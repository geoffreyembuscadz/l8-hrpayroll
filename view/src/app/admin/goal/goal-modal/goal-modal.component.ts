import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response} from '@angular/http';
import { QuestionService } from '../../../services/question.service';
import { GoalService } from '../../../services/goal.service';
import { Configuration } from '../../../app.config';
import { ConfirmModalComponent } from './../../confirm-modal/confirm-modal.component';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { DaterangepickerConfig } from 'ng2-daterangepicker';


@Component({
  selector: 'app-goal-modal',
  templateUrl: './goal-modal.component.html',
  styleUrls: ['./goal-modal.component.css']
})
export class GoalModalComponent extends DialogComponent<null, boolean> {
	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public poststore: any;
	public id: any;

	today = moment().format("YYYY-MM-DD");
	first_month = moment().add(1, 'months');
	edit:any;
	create:any;
	url:any;
	Form : FormGroup;
	dates: any;
	time =false;
	employee_id: any;
	goal: any;
	time_frame: any;
	date_started: any;


	public mainInput = {
		start: moment().subtract(12, 'month'),                              
		end: moment().subtract(11, 'month')
	}

	constructor(
		dialogService: DialogService, 
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute, 
		private _q_service: QuestionService,  
		private _goal_service: GoalService,   
		private _rt: Router,
		private modalService: DialogService,
		private daterangepickerOptions: DaterangepickerConfig ) {
		super(dialogService); 

		this.daterangepickerOptions.settings = {
			locale: { format: 'MM/DD/YYYY' },
			alwaysShowCalendars: false,
			singleDatePicker: true,
			showDropdowns: true,
			opens: "center"
		};  

		this.Form = 
		_fb.group({
			'employee_id':			['null'],
			'goal':					['null'],
			'time_frame':			['null'],
			'date_started':			['null'],
			
		});


	}

	ngOnInit() {
		if (this.edit == true) {
			this.getData();
			this.time = true;
		}
		
	}
	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}
	archive(){
		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
			title:'Archive Data',
			message:'Are you sure you want to archive this data?',
			action:'Delete',
			id:this.id,
			url:this.url
		}).subscribe((isConfirmed)=>{
			setTimeout(() => {
				this.close();
			}, 1000);

		});
	}

 
	onSubmit() {
		this.Form.value.employee_id = this.employee_id;
		this.Form.value.date_started = this.today;
		this.Form.value.time_frame =  this.dates;
		let model = this.Form.value;
		console.log(model);

		if(this.edit == true){
			let id = this.id;
			console.log('a',this.id);
			this.time = true;
			this._goal_service.updateGoal(id, model)
			.subscribe(
				data => {
					this.poststore = Array.from(data); 
					this.success_title = "Success!";
					this.success_message = "Successfully updated";
					setTimeout(() => {
						this.close();
					}, 1000);
				},
				err => this.catchError(err)
				);
		} else if ( this.create = true ) {
			this._goal_service.storeGoal(model)
			.subscribe(
				data => {
					this.poststore = Array.from(data); 
					this.success_title = "Success!";
					this.success_message = "Successfully created";
					setTimeout(() => {
						this.close();
					}, 1000);
				},
				err => this.catchError(err)
				);		
		}		
	}

	getData(){
    	let id = this.id;
    	this._goal_service.getGoal(id).subscribe(
    		data => {
    			this.setType(data);
    		},
    		err => console.error(err)
    		);
    }

    public setType(type: any){
    	this.id = type.id;
    	this.employee_id = type.employee_id;
		this.goal = type.goal;
		this.mainInput.start = type.time_frame;
		this.date_started = type.date_started;

    }

    private selectedDate(value:any, dateInput:any) {
        dateInput.start = value.start;
       	this.dates = moment(dateInput.start).format("YYYY-MM-DD");
    }

    atabs(){
    	this.time = true;
    }


}


