var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { QuestionService } from '../../../services/question.service';
import { GoalService } from '../../../services/goal.service';
import { ConfirmModalComponent } from './../../confirm-modal/confirm-modal.component';
import * as moment from 'moment';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
var GoalModalComponent = /** @class */ (function (_super) {
    __extends(GoalModalComponent, _super);
    function GoalModalComponent(dialogService, _fb, _ar, _q_service, _goal_service, _rt, modalService, daterangepickerOptions) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._q_service = _q_service;
        _this._goal_service = _goal_service;
        _this._rt = _rt;
        _this.modalService = modalService;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this.today = moment().format("YYYY-MM-DD");
        _this.first_month = moment().add(1, 'months');
        _this.time = false;
        _this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        _this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
            showDropdowns: true,
            opens: "center"
        };
        _this.Form =
            _fb.group({
                'employee_id': ['null'],
                'goal': ['null'],
                'time_frame': ['null'],
                'date_started': ['null'],
            });
        return _this;
    }
    GoalModalComponent.prototype.ngOnInit = function () {
        if (this.edit == true) {
            this.getData();
            this.time = true;
        }
    };
    GoalModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    GoalModalComponent.prototype.archive = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: this.id,
            url: this.url
        }).subscribe(function (isConfirmed) {
            setTimeout(function () {
                _this.close();
            }, 1000);
        });
    };
    GoalModalComponent.prototype.onSubmit = function () {
        var _this = this;
        this.Form.value.employee_id = this.employee_id;
        this.Form.value.date_started = this.today;
        this.Form.value.time_frame = this.dates;
        var model = this.Form.value;
        console.log(model);
        if (this.edit == true) {
            var id = this.id;
            console.log('a', this.id);
            this.time = true;
            this._goal_service.updateGoal(id, model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success!";
                _this.success_message = "Successfully updated";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
        else if (this.create = true) {
            this._goal_service.storeGoal(model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success!";
                _this.success_message = "Successfully created";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    GoalModalComponent.prototype.getData = function () {
        var _this = this;
        var id = this.id;
        this._goal_service.getGoal(id).subscribe(function (data) {
            _this.setType(data);
        }, function (err) { return console.error(err); });
    };
    GoalModalComponent.prototype.setType = function (type) {
        this.id = type.id;
        this.employee_id = type.employee_id;
        this.goal = type.goal;
        this.mainInput.start = type.time_frame;
        this.date_started = type.date_started;
    };
    GoalModalComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        this.dates = moment(dateInput.start).format("YYYY-MM-DD");
    };
    GoalModalComponent.prototype.atabs = function () {
        this.time = true;
    };
    GoalModalComponent = __decorate([
        Component({
            selector: 'app-goal-modal',
            templateUrl: './goal-modal.component.html',
            styleUrls: ['./goal-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            QuestionService,
            GoalService,
            Router,
            DialogService,
            DaterangepickerConfig])
    ], GoalModalComponent);
    return GoalModalComponent;
}(DialogComponent));
export { GoalModalComponent };
//# sourceMappingURL=goal-modal.component.js.map