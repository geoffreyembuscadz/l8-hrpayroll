import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild,trigger,state,style,animate,transition,keyframes } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { GoalService } from '../../services/goal.service';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { GoalModalComponent } from '../goal/goal-modal/goal-modal.component';
import { Select2OptionData } from 'ng2-select2';
import { AuthUserService } from '../../services/auth-user.service';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-goal',
  templateUrl: './goal.component.html',
  styleUrls: ['./goal.component.css']
})

@Injectable()
export class GoalComponent implements OnInit, AfterViewInit {
	
	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	public Form: FormGroup;
	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public emp_contract_statuses: any;
	public poststore: any;

	user_id:any;
	employee_id: any;
	jsonParse: any;
	api: String;
	obj: any;
	goal: any;
	petsa: any;
    

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _goal_service: GoalService,
		private _fb: FormBuilder,
		private _auth_service: AuthUserService
		){

		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");

		this.api = this._conf.ServerWithApiUrl + 'goal/';

		this.Form = 
		_fb.group({
			'employee_id':			['null'],
			'goal':					['null'],
			'time_frame':			['null'],
			'date_started':			['null'],
			
		});

		
		
	}

 	ngOnInit() {
		this.get_UserId();
	}
	ngOnDestroy() {
		this.body.classList.remove("skin-blue");
		this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
		this.dtTrigger.next();
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	get_UserId(){
		this._auth_service.getUser().
		subscribe(
			data => {
				let user = data;
				this.user_id = user.id;
				this.getEmployeeId();
			},
			err => console.error(err)
		);
	}

	getEmployeeId(){
		this._auth_service.getUserById(this.user_id).
		subscribe(
			data => {
				let user = data;
				this.employee_id = user.employee_id;
				this.showGoal();
			
			},
			err => console.error(err)
		);

	}

	addGoal() {
		let disposable = this.modalService.addDialog(GoalModalComponent, {
            title:'Goal',
            button:'Add',
            employee_id: this.user_id,
		    create:true
        	}).subscribe((isConfirmed)=>{
        		this.showGoal();
            });
	}

	showGoal(){
    	this.Form.value.employee_id = this.user_id;
		let model = this.Form.value;

        this._goal_service.showGoal(model).subscribe(
          data => {
            this.goal = Array.from(data);
           	for (var i = 0; i < this.goal.length; ++i) {
           		this.petsa = this.goal[i].time_frame;
           	}
          },
          err => console.error(err)
          );
    }

    editGoal(id:any) {
		let disposable = this.modalService.addDialog(GoalModalComponent, {
            title:'Update Goal',
            button:'Update',
		    edit:true,
		    url:'goal',
		    id:id
        	}).subscribe((isConfirmed)=>{
                this.showGoal();
        });
	}

	archiveGoal(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'goal'
        	}, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{

               this.showGoal();
               
               });
	}
}


