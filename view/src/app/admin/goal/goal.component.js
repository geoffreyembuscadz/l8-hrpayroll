var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { GoalService } from '../../services/goal.service';
import { FormBuilder } from '@angular/forms';
import { GoalModalComponent } from '../goal/goal-modal/goal-modal.component';
import { AuthUserService } from '../../services/auth-user.service';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
var GoalComponent = /** @class */ (function () {
    function GoalComponent(modalService, _conf, _goal_service, _fb, _auth_service) {
        this.modalService = modalService;
        this._conf = _conf;
        this._goal_service = _goal_service;
        this._fb = _fb;
        this._auth_service = _auth_service;
        this.dtTrigger = new Subject();
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api = this._conf.ServerWithApiUrl + 'goal/';
        this.Form =
            _fb.group({
                'employee_id': ['null'],
                'goal': ['null'],
                'time_frame': ['null'],
                'date_started': ['null'],
            });
    }
    GoalComponent.prototype.ngOnInit = function () {
        this.get_UserId();
    };
    GoalComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    GoalComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    GoalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    GoalComponent.prototype.get_UserId = function () {
        var _this = this;
        this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
            _this.getEmployeeId();
        }, function (err) { return console.error(err); });
    };
    GoalComponent.prototype.getEmployeeId = function () {
        var _this = this;
        this._auth_service.getUserById(this.user_id).
            subscribe(function (data) {
            var user = data;
            _this.employee_id = user.employee_id;
            _this.showGoal();
        }, function (err) { return console.error(err); });
    };
    GoalComponent.prototype.addGoal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(GoalModalComponent, {
            title: 'Goal',
            button: 'Add',
            employee_id: this.user_id,
            create: true
        }).subscribe(function (isConfirmed) {
            _this.showGoal();
        });
    };
    GoalComponent.prototype.showGoal = function () {
        var _this = this;
        this.Form.value.employee_id = this.user_id;
        var model = this.Form.value;
        this._goal_service.showGoal(model).subscribe(function (data) {
            _this.goal = Array.from(data);
            for (var i = 0; i < _this.goal.length; ++i) {
                _this.petsa = _this.goal[i].time_frame;
            }
        }, function (err) { return console.error(err); });
    };
    GoalComponent.prototype.editGoal = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(GoalModalComponent, {
            title: 'Update Goal',
            button: 'Update',
            edit: true,
            url: 'goal',
            id: id
        }).subscribe(function (isConfirmed) {
            _this.showGoal();
        });
    };
    GoalComponent.prototype.archiveGoal = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'goal'
        }, { backdropColor: 'rgba(238,238,238)' }).subscribe(function (isConfirmed) {
            _this.showGoal();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], GoalComponent.prototype, "dtElement", void 0);
    GoalComponent = __decorate([
        Component({
            selector: 'app-goal',
            templateUrl: './goal.component.html',
            styleUrls: ['./goal.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            GoalService,
            FormBuilder,
            AuthUserService])
    ], GoalComponent);
    return GoalComponent;
}());
export { GoalComponent };
//# sourceMappingURL=goal.component.js.map