import 'rxjs/add/operator/catch';
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { Http, Response, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { EventService } from '../../services/event.service';
import { Event } from '../../model/event';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { CommonService } from '../../services/common.service';
import { AuthUserService } from '../../services/auth-user.service';
import { Select2OptionData } from 'ng2-select2';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})

export class EventComponent extends DialogComponent<null, boolean> {
  public id: string;
  public event_id: any;
  public error_title: string;
  public error_message: string;
  public success_title;
  public success_message;
  public poststore: any;
  // public event = new Event();
  public confirm_archiving: any;
  public form
  eventForm: FormGroup;
  public event = new Event();
  public start_time:any;
  public end_time:any;
  dtOptions: any = {};
  event_rec: any;
  event_type:any;
  event_status:any;
  bodyClasses:string = "skin-blue sidebar-mini";
  body = document.getElementsByTagName('body')[0];
  public mainInput = {
    start: moment().subtract(12, 'month'),
    end: moment().subtract(6, 'month')
  }
  user_id:any;
  validate = false;
  employee_id:any;
  event_data:any;
  edit:any;
  create:any;
  multi:any;
  single:any;
  name:any;
  location:any;
  type_id:any;
  public type : any;
  public type_value : any;
  type_current:any;
  value:any;

  constructor(dialogService: DialogService, 
    private modalService: DialogService, 
    private _ar: ActivatedRoute,
    private _event_service: EventService, 
    private _rt: Router,
    private _fb: FormBuilder, 
    private daterangepickerOptions: DaterangepickerConfig,
    private _common_service: CommonService,
    private _auth_service: AuthUserService
    ) {
    super(dialogService);
    this.eventForm = _fb.group({
      'name':         [null],
      'start_time':   [null],
      'end_time':     [null],
      'type':         [null],
      'location':     [null],
      'employee_id':  [null],
    });   
    this.daterangepickerOptions.settings = {
      locale: { format: 'MM/DD/YYYY' },
      alwaysShowCalendars: false,
      singleDatePicker: true,
      showDropdowns: true,
      opens: "center"
    }; 
  }
  ngOnInit() {
    if(this.edit == true){
      this.getEvent();
    }
    this.getEventType();
    this.get_type();
    this.get_UserId();
  }
  get_UserId(){
    this._auth_service.getUser().
    subscribe(
      data => {
        let user = data; 
        this.user_id = user.id;
      },
      err => console.error(err)
      );
  }
  getEvent(){
    this._event_service.getEvent(this.event_id).subscribe(
      data => {
        this.event_data=(data);
        this.name = this.event_data.name
        this.start_time = this.event_data.start;
        this.end_time = this.event_data.end;
        this.location = this.event_data.location;
        this.type_id = this.event_data.event_type_id;
      },
      err => console.error(err)
      );
  }
  public onSubmit() {
    this.eventForm.value.employee_id = this.employee_id;
    this.eventForm.value.created_by = this.user_id;
    this.eventForm.value.start_time = moment(this.start_time).format("YYYY-MM-DD HH:mm:ss");
    this.eventForm.value.end_time = moment(this.end_time).format("YYYY-MM-DD HH:mm:ss");
    this.eventForm.value.name = this.name;
    this.eventForm.value.location = this.location;
    this.eventForm.value.type = this.type_current;
    if(this.end_time == null){
      this.eventForm.value.end_time = moment(this.start_time).format("YYYY-MM-DD HH:mm:ss");
    }
    let event = this.eventForm.value;

    if(this.create == true){
      this._event_service.storeEventDetails(event)
      .subscribe(
        data => {
          this.poststore = Array.from(data);
          this.success_title = "Success!";
          this.success_message = "New event is added!";
          setTimeout(() => {
            this.close();
          }, 1000);
        },
        err => this.catchError(err)
        ); 
    }
    if(this.edit == true){
      let id = this.event_id;
      this._event_service.updateEvent(id,event)
      .subscribe(
        data => {
          this.poststore = Array.from(data);
          this.success_title = "Success!";
          this.success_message = "Successfully updated";
          setTimeout(() => {
            this.close();
          }, 1000);
        },
        err => this.catchError(err)
        ); 
    }
  }
  ngAfterViewInit(){}
  showMe(events: any){
    this.event_rec = events;
  }
  ngOnDestroy() {
    //remove the the body classes
    // this.body.classList.remove("skin-blue");
    // this.body.classList.remove("sidebar-mini");
  }
  public catchError(error: any) {
    let response_body = error._body;
    let response_status = error.status;
    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'The given data failed to pass validation.';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }
  }
  private selectedDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
    this.start_time = moment(dateInput.start).format("YYYY-MM-DD");
    this.end_time = moment(dateInput.end).format("YYYY-MM-DD"); 
  }
  getEventType(){
    this._event_service.getEventType()
    .subscribe(
      data => {
        this.event_type = Array.from(data);
      },
      err => this.catchError(err)
      );
  }
  get_type(){
    this.type = this._common_service.getEventType().
    subscribe(
      data => {
        this.type = Array.from(data); // fetched record
        this.type_value = [];
        if(this.edit == true) {
          this.type_value = this.type_id;
          this.validate = true;
        }
        this.type_current = this.type_value;
        if(this.create == true){
          let id = 0;
          let text = 'Select Type';
          this.type.unshift({id,text});
          this.type_value = this.value;
        }
      },
      err => console.error(err)
      );
  }
  changedType(data: any) {
    this.type_current = data.value;
    let len = this.type_current.length;
    if(len >= 1 && this.type_current != 0){
      this.validate=true;
    }
  }
  archive(){
    let disposable = this.modalService.addDialog(ConfirmModalComponent, {
      title:'Archive Data',
      message:'Are you sure you want to archive this data?',
      action:'Delete',
      id:this.event_id,
      url:'event'
    }, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
        this.close();
      });
  }
}