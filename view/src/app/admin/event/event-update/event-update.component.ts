import { CommonService } from '../../../services/common.service';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Headers } from '@angular/http';
import { EventService } from '../../../services/event.service';
import { Event } from '../../../model/event';


@Component({
	selector: 'app-event-update',
	templateUrl: './event-update.component.html',
	styleUrls: ['./event-update.component.css']
})
export class EventUpdateComponent extends DialogComponent<null, boolean> {
	
	title: string;
	message: string;
	public id: any;
	public status: any;
	public name: string;
	public success_title;
	public success_message;
	private headers: Headers;
	public poststore: any;
	public event = new Event();
	public event_status: any;
	updateEventForm : FormGroup;

	constructor(
		private _eventservice: EventService, 
		dialogService: DialogService, 
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute, 
		private _common_service: CommonService,
		) {
		super(dialogService);
		// Header Variables
		this.headers = new Headers();
		let authToken = localStorage.getItem('id_token');
		this.headers.append('Authorization', `Bearer ${authToken}`);
		this.headers.append('Content-Type', 'application/json');
		this.headers.append('Accept', 'application/json');

		this.id = this._eventservice.getId();
		
		this._eventservice.getEventStatus(this.id).subscribe(
			data => {
				this.setStatus(data);
			},
			err => console.error(err)
			);

		this.updateEventForm = _fb.group({
			'status_id': [this.event.status_id, [Validators.required]]
		});  
	}
	
	ngOnInit() {
		this.get_status_type();
		this.get_event_status();
	}

	get_status_type(){
		let event_status = this._common_service.getStatusType().
		subscribe(
			data => {
				this.event_status= Array.from(data);
			},
			err => console.error(err)
			);
	}

	get_event_status(){
		this._eventservice.getEvent(this.id).subscribe(
			data => {
				this.setStatus(data);
			},
			err => console.error(err)
			);
	}

	public setStatus(event: any){
		this.id = event.id;
		this.event = event;
	}

	UpdateEventStatus(){
		let event_model = this.updateEventForm.value;
		this._eventservice.updateEventStatus(this.id, event_model)

		.subscribe(
			data => {
				this.poststore = Array.from(data); // fetched the records
				this.success_title = "Success!";
				this.success_message = "The status was successfully updated.";
				setTimeout(() => {
					this.close();
				}, 1000);
				
			},
			err => this.catchError(err)
			);
	}
	
	public catchError(error: any) {

	}
}
