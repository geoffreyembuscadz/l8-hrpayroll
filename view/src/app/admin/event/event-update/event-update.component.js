var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { CommonService } from '../../../services/common.service';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Headers } from '@angular/http';
import { EventService } from '../../../services/event.service';
import { Event } from '../../../model/event';
var EventUpdateComponent = /** @class */ (function (_super) {
    __extends(EventUpdateComponent, _super);
    function EventUpdateComponent(_eventservice, dialogService, _fb, _ar, _common_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._eventservice = _eventservice;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._common_service = _common_service;
        _this.event = new Event();
        // Header Variables
        _this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        _this.headers.append('Authorization', "Bearer " + authToken);
        _this.headers.append('Content-Type', 'application/json');
        _this.headers.append('Accept', 'application/json');
        _this.id = _this._eventservice.getId();
        _this._eventservice.getEventStatus(_this.id).subscribe(function (data) {
            _this.setStatus(data);
        }, function (err) { return console.error(err); });
        _this.updateEventForm = _fb.group({
            'status_id': [_this.event.status_id, [Validators.required]]
        });
        return _this;
    }
    EventUpdateComponent.prototype.ngOnInit = function () {
        this.get_status_type();
        this.get_event_status();
    };
    EventUpdateComponent.prototype.get_status_type = function () {
        var _this = this;
        var event_status = this._common_service.getStatusType().
            subscribe(function (data) {
            _this.event_status = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    EventUpdateComponent.prototype.get_event_status = function () {
        var _this = this;
        this._eventservice.getEvent(this.id).subscribe(function (data) {
            _this.setStatus(data);
        }, function (err) { return console.error(err); });
    };
    EventUpdateComponent.prototype.setStatus = function (event) {
        this.id = event.id;
        this.event = event;
    };
    EventUpdateComponent.prototype.UpdateEventStatus = function () {
        var _this = this;
        var event_model = this.updateEventForm.value;
        this._eventservice.updateEventStatus(this.id, event_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The status was successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 1000);
        }, function (err) { return _this.catchError(err); });
    };
    EventUpdateComponent.prototype.catchError = function (error) {
    };
    EventUpdateComponent = __decorate([
        Component({
            selector: 'app-event-update',
            templateUrl: './event-update.component.html',
            styleUrls: ['./event-update.component.css']
        }),
        __metadata("design:paramtypes", [EventService,
            DialogService,
            FormBuilder,
            ActivatedRoute,
            CommonService])
    ], EventUpdateComponent);
    return EventUpdateComponent;
}(DialogComponent));
export { EventUpdateComponent };
//# sourceMappingURL=event-update.component.js.map