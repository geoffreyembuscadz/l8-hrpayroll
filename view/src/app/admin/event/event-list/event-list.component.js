var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { EventService } from '../../../services/event.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { EventList } from '../../../model/event_list';
import { DialogService } from "ng2-bootstrap-modal";
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
import { PermissionService } from '../../../services/permission.service';
import { EventUpdateComponent } from '../../event/event-update/event-update.component';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { DataTableDirective } from 'angular-datatables';
import { Configuration } from '../../../app.config';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Rx';
var EventListComponent = /** @class */ (function () {
    function EventListComponent(dialogService, _perms_service, modalService, _ar, _rt, _fb, _conf, _event_service, _auth_service) {
        this._perms_service = _perms_service;
        this.modalService = modalService;
        this._ar = _ar;
        this._rt = _rt;
        this._fb = _fb;
        this._conf = _conf;
        this._event_service = _event_service;
        this._auth_service = _auth_service;
        this.dtTrigger = new Subject();
        this.event_list = new EventList();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.api_event = this._conf.ServerWithApiUrl + 'event/';
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }
    EventListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    EventListComponent.prototype.showMe = function (eventlists) {
        this.event_list_rec = eventlists;
    };
    EventListComponent.prototype.ngOnInit = function () {
        this.evenList();
    };
    EventListComponent.prototype.openModal = function () {
        var disposable = this.modalService.addDialog(EventUpdateComponent, {
            title: 'Update Event'
        }).subscribe(function (isConfirmed) {
        });
    };
    EventListComponent.prototype.get_UserId = function () {
        var _this = this;
        this._auth_service.getUser().
            subscribe(function (data) {
            var user = data; // fetched 
            _this.user_id = user.id;
            console.log(_this.user_id);
        }, function (err) { return console.error(err); });
    };
    EventListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    EventListComponent.prototype.evenList = function () {
        var _this = this;
        this._event_service.getEventsList().
            subscribe(function (data) {
            _this.list = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    EventListComponent.prototype.approvedLeave = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Approved Event',
            message: 'Are you sure you want to Approved this Event?',
            action: 'Approved',
            id: id,
            url: 'event'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    EventListComponent.prototype.rejectedLeave = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(RemarksModalComponent, {
            title: 'Reject Event',
            id: id,
            url: 'event',
            button: 'Reject'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], EventListComponent.prototype, "dtElement", void 0);
    EventListComponent = __decorate([
        Component({
            selector: 'app-event-list',
            templateUrl: './event-list.component.html',
            styleUrls: ['./event-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            PermissionService,
            DialogService,
            ActivatedRoute,
            Router,
            FormBuilder,
            Configuration,
            EventService,
            AuthUserService])
    ], EventListComponent);
    return EventListComponent;
}());
export { EventListComponent };
//# sourceMappingURL=event-list.component.js.map