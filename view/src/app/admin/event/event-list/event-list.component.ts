import { Component, OnInit, Injectable, AfterViewInit, ViewChild } from '@angular/core';
import { EventService } from '../../../services/event.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { EventList } from '../../../model/event_list';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
import { PermissionService } from '../../../services/permission.service';
import { EventUpdateComponent } from '../../event/event-update/event-update.component';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { DataTableDirective } from 'angular-datatables';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../../../app.config';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Rx';
@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
@Injectable()
export class EventListComponent implements OnInit, AfterViewInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();

  public id: string;
  public event_list_id: any;
  public error_title: string;
  public error_message: string;
  public success_title;
  public success_message;
  public poststore: any;
  public leave_id: any;
  public event_id: any;
  public user_id: any;
  public event_list = new EventList();
  api_event: String;
  eventListForm: FormGroup;
  public confirm_archiving: any;
  public form
  private headers: Headers;
  dtOptions: any = {};
  event_list_rec: any;
  perm_rec: any;
  list: any;
  bodyClasses:string = "skin-blue sidebar-mini";
  body = document.getElementsByTagName('body')[0];
  constructor(dialogService: DialogService, 
    private _perms_service: PermissionService, 
    private modalService: DialogService, 
    private _ar: ActivatedRoute, 
    private _rt: Router, 
    private _fb: FormBuilder,
    private _conf: Configuration,
    private _event_service: EventService,
    private _auth_service: AuthUserService
    ){
    this.api_event = this._conf.ServerWithApiUrl + 'event/';
    this.body.classList.add("skin-blue");
    this.body.classList.add("sidebar-mini");
    // Header Variables
    this.headers = new Headers();
    let authToken = localStorage.getItem('id_token');
    this.headers.append('Authorization', `Bearer ${authToken}`);
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }
  ngAfterViewInit(){
    this.dtTrigger.next();
  }
  showMe(eventlists: any){
    this.event_list_rec = eventlists;
  }
  ngOnInit() {
    this.evenList();
  }
  openModal() {
    let disposable = this.modalService.addDialog(EventUpdateComponent, {
      title:'Update Event'
    }).subscribe((isConfirmed)=>{

    });

  }
  get_UserId(){
    this._auth_service.getUser().
    subscribe(
      data => {
        let user = data; // fetched 
        this.user_id = user.id;
        console.log(this.user_id);

      },
      err => console.error(err)
      );
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  evenList(){
    this._event_service.getEventsList().
    subscribe(
      data => {
        this.list = Array.from(data);
        this.rerender();      
      },
      err => console.error(err)
      );

  }
  approvedLeave(id:any) {

    let disposable = this.modalService.addDialog(ConfirmModalComponent, {
      title:'Approved Event',
      message:'Are you sure you want to Approved this Event?',
      action:'Approved',
      id:id,
      url:'event'
    }).subscribe((isConfirmed)=>{
      this.rerender();
      this.ngOnInit();

    });
  }

  rejectedLeave(id:any) {

    let disposable = this.modalService.addDialog(RemarksModalComponent, {
      title:'Reject Event',
      id:id,
      url:'event',
      button:'Reject'
    }).subscribe((isConfirmed)=>{
      this.rerender();
      this.ngOnInit();

    });
  }



}