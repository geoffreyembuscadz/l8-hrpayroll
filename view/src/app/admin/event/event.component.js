var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { EventService } from '../../services/event.service';
import { Event } from '../../model/event';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { CommonService } from '../../services/common.service';
import { AuthUserService } from '../../services/auth-user.service';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
var EventComponent = /** @class */ (function (_super) {
    __extends(EventComponent, _super);
    function EventComponent(dialogService, modalService, _ar, _event_service, _rt, _fb, daterangepickerOptions, _common_service, _auth_service) {
        var _this = _super.call(this, dialogService) || this;
        _this.modalService = modalService;
        _this._ar = _ar;
        _this._event_service = _event_service;
        _this._rt = _rt;
        _this._fb = _fb;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this._common_service = _common_service;
        _this._auth_service = _auth_service;
        _this.event = new Event();
        _this.dtOptions = {};
        _this.bodyClasses = "skin-blue sidebar-mini";
        _this.body = document.getElementsByTagName('body')[0];
        _this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(6, 'month')
        };
        _this.validate = false;
        _this.eventForm = _fb.group({
            'name': [null],
            'start_time': [null],
            'end_time': [null],
            'type': [null],
            'location': [null],
            'employee_id': [null],
        });
        _this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
            showDropdowns: true,
            opens: "center"
        };
        return _this;
    }
    EventComponent.prototype.ngOnInit = function () {
        if (this.edit == true) {
            this.getEvent();
        }
        this.getEventType();
        this.get_type();
        this.get_UserId();
    };
    EventComponent.prototype.get_UserId = function () {
        var _this = this;
        this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
        }, function (err) { return console.error(err); });
    };
    EventComponent.prototype.getEvent = function () {
        var _this = this;
        this._event_service.getEvent(this.event_id).subscribe(function (data) {
            _this.event_data = (data);
            _this.name = _this.event_data.name;
            _this.start_time = _this.event_data.start;
            _this.end_time = _this.event_data.end;
            _this.location = _this.event_data.location;
            _this.type_id = _this.event_data.event_type_id;
        }, function (err) { return console.error(err); });
    };
    EventComponent.prototype.onSubmit = function () {
        var _this = this;
        this.eventForm.value.employee_id = this.employee_id;
        this.eventForm.value.created_by = this.user_id;
        this.eventForm.value.start_time = moment(this.start_time).format("YYYY-MM-DD HH:mm:ss");
        this.eventForm.value.end_time = moment(this.end_time).format("YYYY-MM-DD HH:mm:ss");
        this.eventForm.value.name = this.name;
        this.eventForm.value.location = this.location;
        this.eventForm.value.type = this.type_current;
        if (this.end_time == null) {
            this.eventForm.value.end_time = moment(this.start_time).format("YYYY-MM-DD HH:mm:ss");
        }
        var event = this.eventForm.value;
        if (this.create == true) {
            this._event_service.storeEventDetails(event)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success!";
                _this.success_message = "New event is added!";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
        if (this.edit == true) {
            var id = this.event_id;
            this._event_service.updateEvent(id, event)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success!";
                _this.success_message = "Successfully updated";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    EventComponent.prototype.ngAfterViewInit = function () { };
    EventComponent.prototype.showMe = function (events) {
        this.event_rec = events;
    };
    EventComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        // this.body.classList.remove("skin-blue");
        // this.body.classList.remove("sidebar-mini");
    };
    EventComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    EventComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        this.start_time = moment(dateInput.start).format("YYYY-MM-DD");
        this.end_time = moment(dateInput.end).format("YYYY-MM-DD");
    };
    EventComponent.prototype.getEventType = function () {
        var _this = this;
        this._event_service.getEventType()
            .subscribe(function (data) {
            _this.event_type = Array.from(data);
        }, function (err) { return _this.catchError(err); });
    };
    EventComponent.prototype.get_type = function () {
        var _this = this;
        this.type = this._common_service.getEventType().
            subscribe(function (data) {
            _this.type = Array.from(data); // fetched record
            _this.type_value = [];
            if (_this.edit == true) {
                _this.type_value = _this.type_id;
                _this.validate = true;
            }
            _this.type_current = _this.type_value;
            if (_this.create == true) {
                var id = 0;
                var text = 'Select Type';
                _this.type.unshift({ id: id, text: text });
                _this.type_value = _this.value;
            }
        }, function (err) { return console.error(err); });
    };
    EventComponent.prototype.changedType = function (data) {
        this.type_current = data.value;
        var len = this.type_current.length;
        if (len >= 1 && this.type_current != 0) {
            this.validate = true;
        }
    };
    EventComponent.prototype.archive = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: this.event_id,
            url: 'event'
        }, { backdropColor: 'rgba(238,238,238)' }).subscribe(function (isConfirmed) {
            _this.close();
        });
    };
    EventComponent = __decorate([
        Component({
            selector: 'app-event',
            templateUrl: './event.component.html',
            styleUrls: ['./event.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            DialogService,
            ActivatedRoute,
            EventService,
            Router,
            FormBuilder,
            DaterangepickerConfig,
            CommonService,
            AuthUserService])
    ], EventComponent);
    return EventComponent;
}(DialogComponent));
export { EventComponent };
//# sourceMappingURL=event.component.js.map