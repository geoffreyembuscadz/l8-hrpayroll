import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import { Select2OptionData } from 'ng2-select2';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { OnboardingService } from '../../../services/onboarding.service';
import { TypeModalComponent } from '../../type-modal/type-modal.component';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { ChecklistModalComponent } from '../../checklist-modal/checklist-modal.component';


@Component({
  selector: 'app-onboarding-checklist',
  templateUrl: './onboarding-checklist.component.html',
  styleUrls: ['./onboarding-checklist.component.css']
})
export class OnboardingChecklistComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	dtOptions: any = {};

  	filterForm:any;
	user_id:any;
	supervisor_id:any;
	role_id:any;
	employee_id:any;

	onboarding:any;

  	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

  constructor(
  		private modalService: DialogService,
		private _conf: Configuration,
		private daterangepickerOptions: DaterangepickerConfig,
   		private _fb: FormBuilder,
   		private _auth_service: AuthUserService,
   		private _onboarding_service: OnboardingService,
   	) {

  		this.filterForm = _fb.group({
    	'status_id': 		[null],
		'start_date': 		[null],
		'end_date': 		[null],
		'company_id': 		[null],
		'employee_id': 		[null]
   		});

  		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

   	}

  ngOnInit() {
  		this.dateOption();
		this.data();
	}
	
	dateOption(){
    	this.daterangepickerOptions.settings = {
        	singleDatePicker: false
		};
    }

    data(){
		
		let model = this.filterForm.value;

    	this._onboarding_service.getOnBoardingList(model).
	      	subscribe(
	        data => {
	          let dat= data;

	          const result = dat.reduce((acc, elt) => {
			  const {id, name, checker_id, status_id, status, type, val, onboarding_list_id, employee_onboarding_id, employee_id, remarks} = elt;
			 
			  const existingEltIndex = acc.findIndex(e =>
			    e.id === elt.id);
			  const newEvent = {status_id, status, type ,val, onboarding_list_id , checker_id, employee_onboarding_id, employee_id, remarks};
			  
			  if (existingEltIndex < 0) {
			    const newElement = {
			      id,
			      name,
			      checker_id,
			      onboarding: [newEvent]
			    };
			    return [...acc, newElement];
			  } else {
			    acc[existingEltIndex].onboarding.push(newEvent);
			    return acc;
			  }
			}, []);

			this.onboarding = result;
			console.log(this.onboarding)

	          this.rerender();
	        },
	        err => console.error(err)
	      );

	}

	viewAll(x){

		let disposable = this.modalService.addDialog(ChecklistModalComponent, {
            title:'Onboarding',
            tableVal:true,
            onboarding:true,
            data:x.onboarding,
            attend:false,
			holiday:false,
  			clearance:false,
			attendModal:false
        	}).subscribe((isConfirmed)=>{
                this.data();

            });
	}

  	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

}
