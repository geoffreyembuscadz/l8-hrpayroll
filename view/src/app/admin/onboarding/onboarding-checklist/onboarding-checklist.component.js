var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { FormBuilder } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import { OnboardingService } from '../../../services/onboarding.service';
import { ChecklistModalComponent } from '../../checklist-modal/checklist-modal.component';
var OnboardingChecklistComponent = /** @class */ (function () {
    function OnboardingChecklistComponent(modalService, _conf, daterangepickerOptions, _fb, _auth_service, _onboarding_service) {
        this.modalService = modalService;
        this._conf = _conf;
        this.daterangepickerOptions = daterangepickerOptions;
        this._fb = _fb;
        this._auth_service = _auth_service;
        this._onboarding_service = _onboarding_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filterForm = _fb.group({
            'status_id': [null],
            'start_date': [null],
            'end_date': [null],
            'company_id': [null],
            'employee_id': [null]
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    OnboardingChecklistComponent.prototype.ngOnInit = function () {
        this.dateOption();
        this.data();
    };
    OnboardingChecklistComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    OnboardingChecklistComponent.prototype.data = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._onboarding_service.getOnBoardingList(model).
            subscribe(function (data) {
            var dat = data;
            var result = dat.reduce(function (acc, elt) {
                var id = elt.id, name = elt.name, checker_id = elt.checker_id, status_id = elt.status_id, status = elt.status, type = elt.type, val = elt.val, onboarding_list_id = elt.onboarding_list_id, employee_onboarding_id = elt.employee_onboarding_id, employee_id = elt.employee_id, remarks = elt.remarks;
                var existingEltIndex = acc.findIndex(function (e) {
                    return e.id === elt.id;
                });
                var newEvent = { status_id: status_id, status: status, type: type, val: val, onboarding_list_id: onboarding_list_id, checker_id: checker_id, employee_onboarding_id: employee_onboarding_id, employee_id: employee_id, remarks: remarks };
                if (existingEltIndex < 0) {
                    var newElement = {
                        id: id,
                        name: name,
                        checker_id: checker_id,
                        onboarding: [newEvent]
                    };
                    return acc.concat([newElement]);
                }
                else {
                    acc[existingEltIndex].onboarding.push(newEvent);
                    return acc;
                }
            }, []);
            _this.onboarding = result;
            console.log(_this.onboarding);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    OnboardingChecklistComponent.prototype.viewAll = function (x) {
        var _this = this;
        var disposable = this.modalService.addDialog(ChecklistModalComponent, {
            title: 'Onboarding',
            tableVal: true,
            onboarding: true,
            data: x.onboarding,
            attend: false,
            holiday: false,
            clearance: false,
            attendModal: false
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    OnboardingChecklistComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    OnboardingChecklistComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    OnboardingChecklistComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], OnboardingChecklistComponent.prototype, "dtElement", void 0);
    OnboardingChecklistComponent = __decorate([
        Component({
            selector: 'app-onboarding-checklist',
            templateUrl: './onboarding-checklist.component.html',
            styleUrls: ['./onboarding-checklist.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            DaterangepickerConfig,
            FormBuilder,
            AuthUserService,
            OnboardingService])
    ], OnboardingChecklistComponent);
    return OnboardingChecklistComponent;
}());
export { OnboardingChecklistComponent };
//# sourceMappingURL=onboarding-checklist.component.js.map