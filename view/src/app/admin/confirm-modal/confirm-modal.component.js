var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Configuration } from '../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { TaskService } from '../../services/task.service';
var ConfirmModalComponent = /** @class */ (function (_super) {
    __extends(ConfirmModalComponent, _super);
    function ConfirmModalComponent(dialogService, _fb, _ar, _common_service, _conf, _task_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._common_service = _common_service;
        _this._conf = _conf;
        _this._task_service = _task_service;
        _this.validate = true;
        _this.task = false;
        _this.data = [];
        _this.host = _this._conf.socketUrl;
        return _this;
    }
    ConfirmModalComponent.prototype.ngOnInit = function () {
        if (this.request) {
            this.form = this._fb.group({
                'url': [this.url],
                'id': [this.id],
                'remarks': [null]
            });
        }
        this.connectToServer();
    };
    ConfirmModalComponent.prototype.confirm = function () {
        var _this = this;
        var url = this.url;
        var id = this.id;
        if (this.request == true) {
            var action = this.action;
            var status_1 = 0;
            if (action == 'cancel' || action == 'Cancel' || action == 'CANCEL') {
                status_1 = 3;
            }
            else if (action == 'approved' || action == 'Approved' || action == 'APPROVED') {
                status_1 = 4;
            }
            else if (action == 'pending' || action == 'Pending' || action == 'PENDING') {
                this.form.value.supervisor_id = this.supervisor_id;
                status_1 = 1;
            }
            this.form.value.status_id = status_1;
            var model = this.form.value;
            this._common_service.updateRequest(model)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.socket.emit('notify', _this.poststore);
                _this.validate = false;
                _this.result = true;
                _this.success_title = "Success!";
                _this.success_message = "Successfully " + _this.action;
                setTimeout(function () {
                    _this.close();
                }, 2000);
            }, function (err) { return _this.catchError(err); });
        }
        /**
         * for task approval when hit approved button in task-list
         * @param this.task == true [description]
         */
        else if (this.task == true) {
            var model = this.data;
            this._task_service.approvedTask(model)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.socket.emit('notify', _this.poststore);
                _this.validate = false;
                _this.result = true;
                _this.success_title = "Success!";
                _this.success_message = "Successfully " + _this.action;
                setTimeout(function () {
                    _this.close();
                }, 2000);
            }, function (err) { return _this.catchError(err); });
        }
        else if (this.special) {
            this._common_service.specialArchiveData(url, id)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.result = true;
                _this.success_title = "Success!";
                _this.success_message = "Successfully " + _this.action;
                setTimeout(function () {
                    _this.close();
                }, 2000);
            }, function (err) { return _this.catchError(err); });
        }
        else {
            this._common_service.archiveData(url, id)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.result = true;
                _this.success_title = "Success!";
                _this.success_message = "Successfully " + _this.action;
                setTimeout(function () {
                    _this.close();
                }, 2000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    ConfirmModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    ConfirmModalComponent.prototype.cancel = function () {
        this.close();
    };
    ConfirmModalComponent.prototype.connectToServer = function () {
        var _this = this;
        var socketUrl = this.host;
        this.socket = io.connect(socketUrl);
        this.socket.on("connect", function () { return _this.connect(); });
        this.socket.on("disconnect", function () { return _this.disconnect(); });
        this.socket.on("error", function (error) {
            console.log("ERROR: \"" + error + "\" (" + socketUrl + ")");
        });
    };
    // Handle connection opening
    ConfirmModalComponent.prototype.connect = function () {
        // Request initial list when connected
        this.socket.emit("join", 'Client User -> ');
    };
    // Handle connection closing
    ConfirmModalComponent.prototype.disconnect = function () {
        console.log("Disconnected from \"" + this.host + "\"");
    };
    ConfirmModalComponent = __decorate([
        Component({
            selector: 'app-confirm-modal',
            templateUrl: './confirm-modal.component.html',
            styleUrls: ['./confirm-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            CommonService,
            Configuration,
            TaskService])
    ], ConfirmModalComponent);
    return ConfirmModalComponent;
}(DialogComponent));
export { ConfirmModalComponent };
//# sourceMappingURL=confirm-modal.component.js.map