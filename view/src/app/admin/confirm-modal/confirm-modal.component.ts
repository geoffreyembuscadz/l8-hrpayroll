import { Component } from '@angular/core';
import { Configuration } from '../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { TaskService } from '../../services/task.service';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.css']
})
export class ConfirmModalComponent extends DialogComponent<null, boolean> {
  
	title: string;
	public id: any;
	public status: any;
	public name: string;
	public success_title;
	public success_message;
	public error_title;
	public error_message;
	public poststore: any;
	public statuses: any;
	url:any;
	action:any;
	validate=true;
	form:any;
	request:any;
	supervisor_id:any;

	socket: SocketIOClient.Socket;
    host:any;
    task = false;
    data=[];
    special:any;

 constructor( 
   dialogService: DialogService, 
   private _fb: FormBuilder, 
   private _ar: ActivatedRoute,
   private _common_service: CommonService,
   private _conf: Configuration,
   private _task_service: TaskService
   ){
    super(dialogService);
  	this.host = this._conf.socketUrl;
  }
 
	ngOnInit() {

		if (this.request) {
		  	this.form = this._fb.group({
				'url':				[this.url],
				'id':				[this.id],
				'remarks':			[null]
		      
		    }); 

		}
		    this.connectToServer();
	}	

	confirm() {

		let url = this.url;
	    let id = this.id;
	    
		if (this.request == true) {
			
		    let action = this.action;
		    let status = 0;

		    if(action == 'cancel' || action == 'Cancel' || action == 'CANCEL'){
	            status = 3;
	        }
	        else if(action == 'approved' || action == 'Approved' || action == 'APPROVED'){
	            status = 4;
	        }
	        else if(action == 'pending' || action == 'Pending' || action == 'PENDING'){
	        	this.form.value.supervisor_id = this.supervisor_id;
	            status = 1;
	        }

	        this.form.value.status_id = status;

	        let model = this.form.value;

		    this._common_service.updateRequest(model)
		    .subscribe(
		      data => {
		        this.poststore = data;
		        this.socket.emit('notify', this.poststore);
		        this.validate=false;
		        this.result = true;
		        this.success_title = "Success!";
		        this.success_message = "Successfully " + this.action;
				    setTimeout(() => {
		  		   this.close();
		      	}, 2000);
				
		      },
		      err => this.catchError(err)
		    );
		}
		/**
		 * for task approval when hit approved button in task-list
		 * @param this.task == true [description]
		 */
		else if(this.task == true){

			let model = this.data;

			this._task_service.approvedTask(model)
		    .subscribe(
		      data => {
		        this.poststore = data;
		        this.socket.emit('notify', this.poststore);
		        this.validate=false;
		        this.result = true;
		        this.success_title = "Success!";
		        this.success_message = "Successfully " + this.action;
				    setTimeout(() => {
		  		   this.close();
		      	}, 2000);
				
		      },
		      err => this.catchError(err)
		    );
		}
		else if (this.special) {
			this._common_service.specialArchiveData(url,id)
		    .subscribe(
		      data => {
		        this.poststore = data;
		        this.validate=false;
		        this.result = true;
		        this.success_title = "Success!";
		        this.success_message = "Successfully " + this.action;
				    setTimeout(() => {
		  		   this.close();
		      	}, 2000);
				
		      },
		      err => this.catchError(err)
		    );
			
		}
		else{
			this._common_service.archiveData(url,id)
		    .subscribe(
		      data => {
		        this.poststore = data;
		        this.validate=false;
		        this.result = true;
		        this.success_title = "Success!";
		        this.success_message = "Successfully " + this.action;
				    setTimeout(() => {
		  		   this.close();
		      	}, 2000);
				
		      },
		      err => this.catchError(err)
		    );
		}

	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	cancel(){
		this.close();
	}

	connectToServer(){
	    let socketUrl = this.host;
	    this.socket = io.connect(socketUrl);
	    this.socket.on("connect", () => this.connect());
	    this.socket.on("disconnect", () => this.disconnect());
	    
	    this.socket.on("error", (error: string) => {
	      console.log(`ERROR: "${error}" (${socketUrl})`);
	    });

	   
	  }

	// Handle connection opening
	private connect() {
	    // Request initial list when connected
	    this.socket.emit("join", 'Client User -> ');
	}
	// Handle connection closing
	private disconnect() {
	    console.log(`Disconnected from "${this.host}"`);
	}


}
