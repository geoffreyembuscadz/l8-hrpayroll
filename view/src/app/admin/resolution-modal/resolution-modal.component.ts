import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { RemarksModalComponent } from '../remarks-modal/remarks-modal.component';


@Component({
  selector: 'app-resolution-modal',
  templateUrl: './resolution-modal.component.html',
  styleUrls: ['./resolution-modal.component.css']
})
export class ResolutionModalComponent extends DialogComponent<null, boolean> {
  
	title: string;
	public id: any;
	public success_title;
	public success_message;
	public error_title;
	public error_message;
	public poststore: any;
	url:any;
	action: any;
	table_name: any;
	start_time: any;
	end_time: any;
	date:any;
	type: any;
	reason: any;
	name: any;
	data:any;
	start_date: any;
	end_date: any;
	location: any;
	date_from: any;
	date_to: any;
	date_filed: any;
	day: any;
	break_start: any;
	break_end: any;
	time: any;
	total_hours: any;
	remarks: any;
	created_at: any;


	

 constructor( 
   dialogService: DialogService, 
   private _fb: FormBuilder, 
   private _ar: ActivatedRoute,
   private _common_service: CommonService,
   private modalService: DialogService,
   ){
    super(dialogService);
  
  }

	ngOnInit() {

  		if(this.table_name == 'Leave'){
  			this.url = 'leave';
  		}
  		else if(this.table_name == 'COA'){
  			this.url = 'certificate_of_attendance';
		}
		else if(this.table_name == 'OB'){
			this.url = 'official_business';
		}
		else if(this.table_name == 'Schedule Adjustment'){
			this.url = 'schedule_adjustments';
		}

		else if(this.table_name == 'Overtime'){
			this.url = 'overtime';
		}

		else if(this.table_name == 'Undertime'){
			this.url = 'official_undertime';
		}

		this.request_details();
	}

  	request_details(){
  		
		let id = this.id;

  		 this._common_service.resolutionModal(id, this.url)
	    .subscribe(
	      data => {
	        this.data = data; 
	       	this.start_time = data.start_time;
	       	this.end_time = data.end_time;
	       	this.name = data.name;
	       	this.type = data.type;
	       	this.reason = data.reason; 
	       	this.start_date = data.start_date;
	       	this.end_date = data.end_date;
	       	this.location = data.location;
	       	this.date_from = data.date_from;
	       	this.date_to = data.date_to;
	       	this.date_filed = data.date_filed;
	       	this.day = data.day;
	       	this.break_start = data.break_start;
	       	this.break_end = data.break_end;
	       	this.date = data.date;
	       	this.total_hours = data.total_hours;
	       	this.remarks = data.remarks;
	       	this.created_at = data.created_at;


			
	      },
	      err => this.catchError(err)
	    );
  	}
 
	
	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
		this.error_title = 'Error 500';
		this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
		this.error_title = '';
		this.error_message = '';
		}
	}

	cancel(){
		this.close();
	}


	confirm(){
		if (this.action == 'Approve') {
			let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Approve ' + this.table_name,
            message:'Are you sure you want to ' + this.table_name + '?',
            action:'Approve',
            id:this.id,
            url:this.url
        	}).subscribe((isConfirmed)=>{
        		this.close();

            });

			
		}
		else{
			let disposable = this.modalService.addDialog(RemarksModalComponent, {
            title:'Reject' + this.table_name,
            id:this.id,
            url:this.url
        	}).subscribe((isConfirmed)=>{
        		this.close();
            });
		}
	}	

}
