var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { RemarksModalComponent } from '../remarks-modal/remarks-modal.component';
var ResolutionModalComponent = /** @class */ (function (_super) {
    __extends(ResolutionModalComponent, _super);
    function ResolutionModalComponent(dialogService, _fb, _ar, _common_service, modalService) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._common_service = _common_service;
        _this.modalService = modalService;
        return _this;
    }
    ResolutionModalComponent.prototype.ngOnInit = function () {
        if (this.table_name == 'Leave') {
            this.url = 'leave';
        }
        else if (this.table_name == 'COA') {
            this.url = 'certificate_of_attendance';
        }
        else if (this.table_name == 'OB') {
            this.url = 'official_business';
        }
        else if (this.table_name == 'Schedule Adjustment') {
            this.url = 'schedule_adjustments';
        }
        else if (this.table_name == 'Overtime') {
            this.url = 'overtime';
        }
        else if (this.table_name == 'Undertime') {
            this.url = 'official_undertime';
        }
        this.request_details();
    };
    ResolutionModalComponent.prototype.request_details = function () {
        var _this = this;
        var id = this.id;
        this._common_service.resolutionModal(id, this.url)
            .subscribe(function (data) {
            _this.data = data;
            _this.start_time = data.start_time;
            _this.end_time = data.end_time;
            _this.name = data.name;
            _this.type = data.type;
            _this.reason = data.reason;
            _this.start_date = data.start_date;
            _this.end_date = data.end_date;
            _this.location = data.location;
            _this.date_from = data.date_from;
            _this.date_to = data.date_to;
            _this.date_filed = data.date_filed;
            _this.day = data.day;
            _this.break_start = data.break_start;
            _this.break_end = data.break_end;
            _this.date = data.date;
            _this.total_hours = data.total_hours;
            _this.remarks = data.remarks;
            _this.created_at = data.created_at;
        }, function (err) { return _this.catchError(err); });
    };
    ResolutionModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    ResolutionModalComponent.prototype.cancel = function () {
        this.close();
    };
    ResolutionModalComponent.prototype.confirm = function () {
        var _this = this;
        if (this.action == 'Approve') {
            var disposable = this.modalService.addDialog(ConfirmModalComponent, {
                title: 'Approve ' + this.table_name,
                message: 'Are you sure you want to ' + this.table_name + '?',
                action: 'Approve',
                id: this.id,
                url: this.url
            }).subscribe(function (isConfirmed) {
                _this.close();
            });
        }
        else {
            var disposable = this.modalService.addDialog(RemarksModalComponent, {
                title: 'Reject' + this.table_name,
                id: this.id,
                url: this.url
            }).subscribe(function (isConfirmed) {
                _this.close();
            });
        }
    };
    ResolutionModalComponent = __decorate([
        Component({
            selector: 'app-resolution-modal',
            templateUrl: './resolution-modal.component.html',
            styleUrls: ['./resolution-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            CommonService,
            DialogService])
    ], ResolutionModalComponent);
    return ResolutionModalComponent;
}(DialogComponent));
export { ResolutionModalComponent };
//# sourceMappingURL=resolution-modal.component.js.map