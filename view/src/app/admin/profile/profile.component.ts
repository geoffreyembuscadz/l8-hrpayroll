import 'rxjs/add/operator/catch';
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { NgUploaderOptions, UploadedFile, UploadRejected } from 'ngx-uploader';

import { Configuration } from '../../app.config';
import { Employee } from '../../model/employee';
import { EmployeeService } from '../../services/employee.service';
import { AuthUserService } from '../../services/auth-user.service';

const URL = '';
@Component({
  selector: 'admin-user-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})


export class ProfileComponent implements OnInit {
  private poststore: any;
  private post_data: any;
  private error_title: string;
  private error_message: string;
  private success_title: string;
  private success_message: string;
  private route_upload_attachment = 'upload?type=employee_attachment';
  private route_upload_img = 'upload?type=employee_img';
  private show_attachment_btn: any;
  public employee = new Employee();

  public form_genders: any;
  public form_suffixes: any;
  public form_companies: any;
  public form_job_titles: any;
  public form_blood_types: any;
  public form_departments: any;
  public form_marital_statuses: any;
  public form_supervisors: any;
  public form_skills: any;
  public form_employment_status: any;
  public form_emergency_contact = [{ index:0 }];
  public form_emergency_contact_row = 0;
  public form_default_user_placeholder = '';
  public form_user_image = '';
  public errorUploadImageMessage = '';
  public errorUploadAttachmentMessage = '';

  public options: NgUploaderOptions;
  public optionsImg: NgUploaderOptions;
  public response: any;
  public response_image: any;
  public sizeLimit: number = 1000000;
  public hasBaseDropZoneOver: boolean;

  createEmployeeForm : FormGroup;
  emergencyContacts: FormGroup;

  constructor(private _conf: Configuration, private _fb: FormBuilder, private _auth_user_service: AuthUserService, private _ar: ActivatedRoute, private _empservice: EmployeeService, private _zone: NgZone){
    this.form_genders = this._empservice.getGenders();
    this.form_suffixes = this._empservice.getSuffixes();
    this.form_blood_types = this._empservice.getBloodTypes();
    this.form_skills = this._empservice.getEmployeeSkills();
    this.form_marital_statuses = this._empservice.getEmployeeMaritalStatuses();
    this.form_employment_status = this._empservice.getEmploymentStatus();
    this.form_default_user_placeholder = this._conf.frontEndUrl + 'assets/img/user-placeholder.png';

    // this.emergencyContacts = _fb.group({
    //   'emergency_person_name[0]': ['', [Validators.required]],
    //   'emergency_relationship[0]': ['', [Validators.required]],
    //   'emergency_contact_number[0]': ['', [Validators.required]]
    // });

    //   this.createEmployeeForm = _fb.group({
    //     'firstname': ['', [Validators.required]],
    //     'middlename': ['', [Validators.required]],
    //     'lastname': [ '', [Validators.required]],
    //     'suffix': ['', [Validators.required]],
    //     'primary_company': ['', [Validators.required]],
    //     'gender': ['', [Validators.required]],
    //     'birthdate': ['', [Validators.required]],
    //     'blood_type': ['', [Validators.required]],
    //     'citizenship': ['', [Validators.required]],
    //     'employee_code': ['', [Validators.required]],
    //     'department': ['', [Validators.required]],
    //     'position': ['', [Validators.required]],
    //     'tel_no': ['', [Validators.required]],
    //     'cel_no': ['', [Validators.required]],
    //     'email': ['', [Validators.required]],
    //     'current_address': ['', [Validators.required]],
    //     'province_address': ['', [Validators.required]],
    //     'skills': ['', [Validators.required]],
    //     'marital_status': ['', [Validators.required]],
    //     'nationality': ['Filipino', [Validators.required]],
    //     'employment_status': ['', [Validators.required]],
    //     'driving_license_no': ['', [Validators.required]],
    //     'sss_no': ['', [Validators.required]],
    //     'philhealth_no': ['', [Validators.required]],
    //     'tin_no': ['', [Validators.required]],
    //     'supervisor_id': '', //['', [Validators.required]],
    //     'pagibig_no': ['', [Validators.required]],
    //     'emergency_contacts': this.emergencyContacts,
    //     'employee_image': [this._conf.frontEndUrl + 'assets/img/user-placeholder.png', [Validators.required]]
    //   });

      // uploader ngzone
      // this.options = new NgUploaderOptions({
      //   url: this._conf.ServerWithApiUrl + this.route_upload_attachment,
      //   autoUpload: true,
      //   calculateSpeed: true
      // });

      // this.optionsImg = new NgUploaderOptions({
      //   url: this._conf.ServerWithApiUrl + this.route_upload_img,
      //   autoUpload: true,
      //   calculateSpeed: true,
      //   filterExtensions: true,
      //   allowedExtensions: ['jpg', 'png']
      // });
  }

  ngOnInit(){
    // this.form_user_image = this.form_default_user_placeholder; 
  }

  ngAfterViewInit(){
  }
}