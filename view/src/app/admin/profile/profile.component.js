var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Employee } from '../../model/employee';
import { EmployeeService } from '../../services/employee.service';
import { AuthUserService } from '../../services/auth-user.service';
var URL = '';
var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(_conf, _fb, _auth_user_service, _ar, _empservice, _zone) {
        this._conf = _conf;
        this._fb = _fb;
        this._auth_user_service = _auth_user_service;
        this._ar = _ar;
        this._empservice = _empservice;
        this._zone = _zone;
        this.route_upload_attachment = 'upload?type=employee_attachment';
        this.route_upload_img = 'upload?type=employee_img';
        this.employee = new Employee();
        this.form_emergency_contact = [{ index: 0 }];
        this.form_emergency_contact_row = 0;
        this.form_default_user_placeholder = '';
        this.form_user_image = '';
        this.errorUploadImageMessage = '';
        this.errorUploadAttachmentMessage = '';
        this.sizeLimit = 1000000;
        this.form_genders = this._empservice.getGenders();
        this.form_suffixes = this._empservice.getSuffixes();
        this.form_blood_types = this._empservice.getBloodTypes();
        this.form_skills = this._empservice.getEmployeeSkills();
        this.form_marital_statuses = this._empservice.getEmployeeMaritalStatuses();
        this.form_employment_status = this._empservice.getEmploymentStatus();
        this.form_default_user_placeholder = this._conf.frontEndUrl + 'assets/img/user-placeholder.png';
        // this.emergencyContacts = _fb.group({
        //   'emergency_person_name[0]': ['', [Validators.required]],
        //   'emergency_relationship[0]': ['', [Validators.required]],
        //   'emergency_contact_number[0]': ['', [Validators.required]]
        // });
        //   this.createEmployeeForm = _fb.group({
        //     'firstname': ['', [Validators.required]],
        //     'middlename': ['', [Validators.required]],
        //     'lastname': [ '', [Validators.required]],
        //     'suffix': ['', [Validators.required]],
        //     'primary_company': ['', [Validators.required]],
        //     'gender': ['', [Validators.required]],
        //     'birthdate': ['', [Validators.required]],
        //     'blood_type': ['', [Validators.required]],
        //     'citizenship': ['', [Validators.required]],
        //     'employee_code': ['', [Validators.required]],
        //     'department': ['', [Validators.required]],
        //     'position': ['', [Validators.required]],
        //     'tel_no': ['', [Validators.required]],
        //     'cel_no': ['', [Validators.required]],
        //     'email': ['', [Validators.required]],
        //     'current_address': ['', [Validators.required]],
        //     'province_address': ['', [Validators.required]],
        //     'skills': ['', [Validators.required]],
        //     'marital_status': ['', [Validators.required]],
        //     'nationality': ['Filipino', [Validators.required]],
        //     'employment_status': ['', [Validators.required]],
        //     'driving_license_no': ['', [Validators.required]],
        //     'sss_no': ['', [Validators.required]],
        //     'philhealth_no': ['', [Validators.required]],
        //     'tin_no': ['', [Validators.required]],
        //     'supervisor_id': '', //['', [Validators.required]],
        //     'pagibig_no': ['', [Validators.required]],
        //     'emergency_contacts': this.emergencyContacts,
        //     'employee_image': [this._conf.frontEndUrl + 'assets/img/user-placeholder.png', [Validators.required]]
        //   });
        // uploader ngzone
        // this.options = new NgUploaderOptions({
        //   url: this._conf.ServerWithApiUrl + this.route_upload_attachment,
        //   autoUpload: true,
        //   calculateSpeed: true
        // });
        // this.optionsImg = new NgUploaderOptions({
        //   url: this._conf.ServerWithApiUrl + this.route_upload_img,
        //   autoUpload: true,
        //   calculateSpeed: true,
        //   filterExtensions: true,
        //   allowedExtensions: ['jpg', 'png']
        // });
    }
    ProfileComponent.prototype.ngOnInit = function () {
        // this.form_user_image = this.form_default_user_placeholder; 
    };
    ProfileComponent.prototype.ngAfterViewInit = function () {
    };
    ProfileComponent = __decorate([
        Component({
            selector: 'admin-user-profile',
            templateUrl: './profile.component.html',
            styleUrls: ['./profile.component.css']
        }),
        __metadata("design:paramtypes", [Configuration, FormBuilder, AuthUserService, ActivatedRoute, EmployeeService, NgZone])
    ], ProfileComponent);
    return ProfileComponent;
}());
export { ProfileComponent };
//# sourceMappingURL=profile.component.js.map