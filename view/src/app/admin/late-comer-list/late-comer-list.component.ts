import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Rx';
import { Dashboard1Service } from '../../services/dashboard1.service';
import { CommonService } from '../../services/common.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { DataTableDirective } from 'angular-datatables';
import { MomentModule } from 'angular2-moment/moment.module';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng2-select2';

@Component({
	selector: 'app-late-comer-list',
	templateUrl: './late-comer-list.component.html',
	styleUrls: ['./late-comer-list.component.css']
})
export class LateComerListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};
	api: String;
	show_late: any;
	filter_late: any;
	start_date: any;
	end_date: any;
	byDate = false;
	bySort = false;
	byCompany = false;
	filterForm: any;
	late: any;
	public company: any;
	public company_current : any;
	company_value: Array<Select2OptionData>;
	public options: Select2Options;
	value:any;
	no_of_late: any;
	temp: any;
	total_late = false;
	byAsc = false;
	byDesc = false;

	public mainInput = {
		start: moment().subtract(12, 'month'),
		end: moment().subtract(11, 'month')
	}
	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];
	constructor(
		private _dash_service: Dashboard1Service,
		private _common_service: CommonService,
		private daterangepickerOptions: DaterangepickerConfig,
		private _fb: FormBuilder
		){

		this.filterForm = _fb.group({
			'start_date': 		[null],
			'end_date': 		[null],
			'company_id': 		[null],
			'sort':				[null]
		});

		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");
	}

	ngOnInit() {
		this.showEmpLate();
		this.getCompany();
		this.filter();

		var self = this;
		this.dtOptions = {
			dom: 'lfrtip',
			columnDefs: [
             { "targets": [ 4 ], "visible": this.bySort},
             { "targets": [ 4, 5 ], "visible": this.byCompany}
        	]       
		};
	}
	
	showEmpLate(){
		this._dash_service.showEmpLate().
		subscribe(
			data => {
				this.show_late = Array.from(data); 
				this.rerender();
			},
			err => console.error(err)
			);
	}

	choiceFilter(id){

		if(id == 1){
			this.byDate = true;
			this.bySort = true;
			this.byCompany = true;
		} else if(id == 2) {
			this.byDate = true;
		} else if(id==3) {
			this.bySort = true;
			this.ngOnInit();
			this.filter();
		} else if(id==4) {
			this.byCompany=true;
			this.ngOnInit();
		} else {
			this.byDate=false;
			this.bySort=false;
			this.byCompany=false;
			this.ngOnInit();

			this.filterForm.value.company_id = null;
			this.filterForm.value.start_date = null;
			this.filterForm.value.end_date = null;
			this.filterForm.value.sort = null;
			this.filter();
		}
	}

	getCompany(){
		this._common_service.getCompany()
		.subscribe(
			data => {
				this.company = Array.from(data);
				console.log('company', this.company);
				this.company_value = [];
				this.options = {
					multiple: true
				}
				this.company_current = this.company_value;				
				this.company_value = this.value;
			},
			err => console.error(err)
			);
	}

	changedCompany(data: any) {
		this.total_late = true;
		this.company_current = data.value;

		if(this.company_current == 0){
			this.filter();
		}else{
			this.filterForm.value.company_id = this.company_current;
			this.filter();
		}
	} 
	//emp_late = [0,1,2]
	sortFilter(emp_late) {
		let sort = emp_late;	
		this.filterForm.value.sort = sort;
		this.filter();		
	}

	filter(){
		let model = this.filterForm.value;
		this._dash_service.showFilterLate(model).
		subscribe(
			data => {
				this.show_late = Array.from(data);
				this.rerender();
			},
			err => console.error(err)
			);
	}

	filterDate(value:any, dateInput:any) {
		dateInput.start = value.start;
		dateInput.end = value.end;

		let start_date = moment(dateInput.start).format("YYYY-MM-DD");
		let end_date = moment(dateInput.end).format("YYYY-MM-DD");
		
		this.filterForm.value.start_date = start_date;
		this.filterForm.value.end_date = end_date;

		this.filter();
	}

	ngAfterViewInit(): void {
		this.dtTrigger.next();
	}

	rerender(): void {
		this.dtElement.dtInstance.then((dtInstance) => {
			dtInstance.destroy();
			this.dtTrigger.next();
		});
	}

}