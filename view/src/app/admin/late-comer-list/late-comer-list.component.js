var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Rx';
import { Dashboard1Service } from '../../services/dashboard1.service';
import { CommonService } from '../../services/common.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { DataTableDirective } from 'angular-datatables';
import { FormBuilder } from '@angular/forms';
var LateComerListComponent = /** @class */ (function () {
    function LateComerListComponent(_dash_service, _common_service, daterangepickerOptions, _fb) {
        this._dash_service = _dash_service;
        this._common_service = _common_service;
        this.daterangepickerOptions = daterangepickerOptions;
        this._fb = _fb;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.byDate = false;
        this.bySort = false;
        this.byCompany = false;
        this.total_late = false;
        this.byAsc = false;
        this.byDesc = false;
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filterForm = _fb.group({
            'start_date': [null],
            'end_date': [null],
            'company_id': [null],
            'sort': [null]
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    LateComerListComponent.prototype.ngOnInit = function () {
        this.showEmpLate();
        this.getCompany();
        this.filter();
        var self = this;
        this.dtOptions = {
            dom: 'lfrtip',
            columnDefs: [
                { "targets": [4], "visible": this.bySort },
                { "targets": [4, 5], "visible": this.byCompany }
            ]
        };
    };
    LateComerListComponent.prototype.showEmpLate = function () {
        var _this = this;
        this._dash_service.showEmpLate().
            subscribe(function (data) {
            _this.show_late = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    LateComerListComponent.prototype.choiceFilter = function (id) {
        if (id == 1) {
            this.byDate = true;
            this.bySort = true;
            this.byCompany = true;
        }
        else if (id == 2) {
            this.byDate = true;
        }
        else if (id == 3) {
            this.bySort = true;
            this.ngOnInit();
            this.filter();
        }
        else if (id == 4) {
            this.byCompany = true;
            this.ngOnInit();
        }
        else {
            this.byDate = false;
            this.bySort = false;
            this.byCompany = false;
            this.ngOnInit();
            this.filterForm.value.company_id = null;
            this.filterForm.value.start_date = null;
            this.filterForm.value.end_date = null;
            this.filterForm.value.sort = null;
            this.filter();
        }
    };
    LateComerListComponent.prototype.getCompany = function () {
        var _this = this;
        this._common_service.getCompany()
            .subscribe(function (data) {
            _this.company = Array.from(data);
            console.log('company', _this.company);
            _this.company_value = [];
            _this.options = {
                multiple: true
            };
            _this.company_current = _this.company_value;
            _this.company_value = _this.value;
        }, function (err) { return console.error(err); });
    };
    LateComerListComponent.prototype.changedCompany = function (data) {
        this.total_late = true;
        this.company_current = data.value;
        if (this.company_current == 0) {
            this.filter();
        }
        else {
            this.filterForm.value.company_id = this.company_current;
            this.filter();
        }
    };
    //emp_late = [0,1,2]
    LateComerListComponent.prototype.sortFilter = function (emp_late) {
        var sort = emp_late;
        this.filterForm.value.sort = sort;
        this.filter();
    };
    LateComerListComponent.prototype.filter = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._dash_service.showFilterLate(model).
            subscribe(function (data) {
            _this.show_late = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    LateComerListComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.filter();
    };
    LateComerListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    LateComerListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], LateComerListComponent.prototype, "dtElement", void 0);
    LateComerListComponent = __decorate([
        Component({
            selector: 'app-late-comer-list',
            templateUrl: './late-comer-list.component.html',
            styleUrls: ['./late-comer-list.component.css']
        }),
        __metadata("design:paramtypes", [Dashboard1Service,
            CommonService,
            DaterangepickerConfig,
            FormBuilder])
    ], LateComerListComponent);
    return LateComerListComponent;
}());
export { LateComerListComponent };
//# sourceMappingURL=late-comer-list.component.js.map