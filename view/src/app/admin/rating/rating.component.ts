import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { RatingService } from '../../services/rating.service';
import { CommonService } from '../../services/common.service';
import { RatingModalComponent } from '../rating/rating-modal/rating-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';

@Component({
	selector: 'app-rating',
	templateUrl: './rating.component.html',
	styleUrls: ['./rating.component.css']
})

@Injectable()
export class RatingComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};
	public OT_id: any;
	api: String;
	status:any;
	rating:any;
	
	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _common_service: CommonService,
		private _rating_service: RatingService
		
		){

		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");

		this.api = this._conf.ServerWithApiUrl + 'overtime/';
	}

	ngOnInit() {
		this.showRating();
	}

	showRating(){
		this._rating_service.showRating().
		subscribe(
			data => {
				this.rating = data; 
				this.rerender();
			},
			err => console.error(err)
			);
	}

	ngOnDestroy() {
		this.body.classList.remove("skin-blue");
		this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
		this.dtTrigger.next();
	}

	rerender(): void {
		this.dtElement.dtInstance.then((dtInstance) => {
			dtInstance.destroy();
			this.dtTrigger.next();
		});
	}

	archiveRating(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'rating'
        	}, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
		       	this.rerender();
                this.ngOnInit();
        });
	}
	addModal(){
		let disposable = this.modalService.addDialog(RatingModalComponent, {
			title:'Create Rating',
			button:'Add',
			create:true
		}).subscribe((isConfirmed)=>{
			this.rerender();
			this.ngOnInit();
		});
	}

	editModal(id:any){
		let disposable = this.modalService.addDialog(RatingModalComponent, {
			title:'Edit Rating',
			button:'Update',
			edit:true,
			id:id
		}).subscribe((isConfirmed)=>{
			this.rerender();
			this.ngOnInit();
		});
	}

}
