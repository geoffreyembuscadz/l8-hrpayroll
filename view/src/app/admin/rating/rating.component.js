var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { RatingService } from '../../services/rating.service';
import { CommonService } from '../../services/common.service';
import { RatingModalComponent } from '../rating/rating-modal/rating-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
var RatingComponent = /** @class */ (function () {
    function RatingComponent(modalService, _conf, _common_service, _rating_service) {
        this.modalService = modalService;
        this._conf = _conf;
        this._common_service = _common_service;
        this._rating_service = _rating_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api = this._conf.ServerWithApiUrl + 'overtime/';
    }
    RatingComponent.prototype.ngOnInit = function () {
        this.showRating();
    };
    RatingComponent.prototype.showRating = function () {
        var _this = this;
        this._rating_service.showRating().
            subscribe(function (data) {
            _this.rating = data;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    RatingComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    RatingComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    RatingComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    RatingComponent.prototype.archiveRating = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'rating'
        }, { backdropColor: 'rgba(238,238,238)' }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    RatingComponent.prototype.addModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(RatingModalComponent, {
            title: 'Create Rating',
            button: 'Add',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    RatingComponent.prototype.editModal = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(RatingModalComponent, {
            title: 'Edit Rating',
            button: 'Update',
            edit: true,
            id: id
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], RatingComponent.prototype, "dtElement", void 0);
    RatingComponent = __decorate([
        Component({
            selector: 'app-rating',
            templateUrl: './rating.component.html',
            styleUrls: ['./rating.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            CommonService,
            RatingService])
    ], RatingComponent);
    return RatingComponent;
}());
export { RatingComponent };
//# sourceMappingURL=rating.component.js.map