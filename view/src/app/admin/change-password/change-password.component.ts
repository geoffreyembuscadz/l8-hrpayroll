import { Component, Input } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';

import { Select2OptionData } from 'ng2-select2';

import { Users } from '../../model/users';
import { UsersService } from '../../services/users.service';
import { AuthUserService } from '../../services/auth-user.service';

import { matchingPasswords } from '../../validators/validators';

@Component({
  selector: 'admin-change-password',
  templateUrl: './change-password.component.html'
})
export class ChangePasswordComponent extends DialogComponent<null, boolean> {
  roles_rec: any;
  title: string;
  message: string;
  user_id_emp: any;

  public id: any;
  public user_id: any;
  public name: string;
  public display_name: string;
  public description: string;
  public error_title: string;
  public error_message: string;
  public success_title;
  public success_message;
  public poststore: any;
  public users = new Users();

  public RoleData: Array<Select2OptionData>;
  public value: string[];
  public current: any;
  public options: Select2Options;
  public user: any;
  public role_names: string;
  public checked: boolean= false;
  
  passwordChange : FormGroup;

  constructor(private _rt: Router, private _user_service: UsersService, dialogService: DialogService, 
    private _fb: FormBuilder, private _ar: ActivatedRoute, private _auth_service: AuthUserService) {
    super(dialogService);
    
    this.id = this.user_id;
 
    this._user_service.getUser(this.id).subscribe(
      data => {
        this.setUser(data);
        this.ngOnInit();
      },
      err => console.error(err)
    );

    this.passwordChange = _fb.group({
      'current_password': [''],
      'passwords': _fb.group({
        'password': [null, Validators.compose([ Validators.minLength(5), Validators.maxLength(16)])],
        'confirm_password': [null]
      }, { validator: matchingPasswords('password', 'confirm_password')}),
      'updated_by' :[''],
    }); 
  }
   
  public validateUpdate() {

    this.passwordChange.patchValue({
      'updated_by': this.user_id
    });

    let change_password = this.passwordChange.value;

    this._user_service.changePassword(this.user_id_emp, change_password)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The password was successfully updated.";
        setTimeout(() => {
             this.close();
            }, 2000);
        this.clearErrorMsg();

      },
      err => this.catchError(err)
    );
  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;  



    if( response_status == 500 ){
      this.error_title = 'Error';
      this.error_message = 'Email address already exists.';
      window.scrollTo(0, 0);
    } else if (response_status == 400 ) {
      this.error_title =  'Error';
      this.error_message = 'Please enter the correct password.';
    }
  }

  private clearErrorMsg(){
    this.error_title = '';
    this.error_message = '';
  }

  public setUser(users: any){
    this.id = users.id;
    this.users = users;
  }

  ngOnInit() {
    
    let created_by = this._auth_service.getUser().
        subscribe(
          data => {
            let user = data; 
            this.user_id = user.id;
          },
          err => console.error(err)
        );
  }
  


}