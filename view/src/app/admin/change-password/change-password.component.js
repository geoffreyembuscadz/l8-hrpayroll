var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Users } from '../../model/users';
import { UsersService } from '../../services/users.service';
import { AuthUserService } from '../../services/auth-user.service';
import { matchingPasswords } from '../../validators/validators';
var ChangePasswordComponent = /** @class */ (function (_super) {
    __extends(ChangePasswordComponent, _super);
    function ChangePasswordComponent(_rt, _user_service, dialogService, _fb, _ar, _auth_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._user_service = _user_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._auth_service = _auth_service;
        _this.users = new Users();
        _this.checked = false;
        _this.id = _this.user_id;
        _this._user_service.getUser(_this.id).subscribe(function (data) {
            _this.setUser(data);
            _this.ngOnInit();
        }, function (err) { return console.error(err); });
        _this.passwordChange = _fb.group({
            'current_password': [''],
            'passwords': _fb.group({
                'password': [null, Validators.compose([Validators.minLength(5), Validators.maxLength(16)])],
                'confirm_password': [null]
            }, { validator: matchingPasswords('password', 'confirm_password') }),
            'updated_by': [''],
        });
        return _this;
    }
    ChangePasswordComponent.prototype.validateUpdate = function () {
        var _this = this;
        this.passwordChange.patchValue({
            'updated_by': this.user_id
        });
        var change_password = this.passwordChange.value;
        this._user_service.changePassword(this.user_id_emp, change_password)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The password was successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this.clearErrorMsg();
        }, function (err) { return _this.catchError(err); });
    };
    ChangePasswordComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error';
            this.error_message = 'Email address already exists.';
            window.scrollTo(0, 0);
        }
        else if (response_status == 400) {
            this.error_title = 'Error';
            this.error_message = 'Please enter the correct password.';
        }
    };
    ChangePasswordComponent.prototype.clearErrorMsg = function () {
        this.error_title = '';
        this.error_message = '';
    };
    ChangePasswordComponent.prototype.setUser = function (users) {
        this.id = users.id;
        this.users = users;
    };
    ChangePasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        var created_by = this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
        }, function (err) { return console.error(err); });
    };
    ChangePasswordComponent = __decorate([
        Component({
            selector: 'admin-change-password',
            templateUrl: './change-password.component.html'
        }),
        __metadata("design:paramtypes", [Router, UsersService, DialogService,
            FormBuilder, ActivatedRoute, AuthUserService])
    ], ChangePasswordComponent);
    return ChangePasswordComponent;
}(DialogComponent));
export { ChangePasswordComponent };
//# sourceMappingURL=change-password.component.js.map