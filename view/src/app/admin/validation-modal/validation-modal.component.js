var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Configuration } from '../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { CertificateOfAttendanceModalComponent } from '../certificate-of-attendance/certificate-of-attendance-modal/certificate-of-attendance-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { RemarksModalComponent } from '../remarks-modal/remarks-modal.component';
import { LeaveModalComponent } from '../leave/leave-modal/leave-modal.component';
import { OfficialBusinessModalComponent } from '../officialbusiness/official-business-modal/official-business-modal.component';
import { OvertimeModalComponent } from '../overtime/overtime-modal/overtime-modal.component';
import { UndertimeModalComponent } from '../official-undertime/undertime-modal/undertime-modal.component';
import { ScheduleAdjustmentsModalComponent } from '../schedule-adjustments/schedule-adjustments-modal/schedule-adjustments-modal.component';
import { Router, ActivatedRoute } from '@angular/router';
var ValidationModalComponent = /** @class */ (function (_super) {
    __extends(ValidationModalComponent, _super);
    function ValidationModalComponent(modalService, _fb, _ar, _common_service, _conf, _rt) {
        var _this = _super.call(this, modalService) || this;
        _this.modalService = modalService;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._common_service = _common_service;
        _this._conf = _conf;
        _this._rt = _rt;
        _this.data = [];
        _this.userDup = false;
        _this.adminDup = false;
        _this.checkAllValidation = false;
        _this.validate = false;
        _this.user = false;
        _this.logs = false;
        _this.emp_credentials = false;
        _this.empl = [];
        _this.emp_list = [];
        _this.emp_val = false;
        _this.host = _this._conf.socketUrl;
        _this.form = _fb.group({});
        return _this;
    }
    ValidationModalComponent.prototype.ngOnInit = function () {
        if (this.logs) {
            var val = false;
            this.empl = [];
            if (this.user) {
                this.validate = true;
                val = true;
            }
            var len = this.data.length;
            for (var i = 0; i < len; ++i) {
                var name_1 = this.data[i].name;
                var id = this.data[i].id;
                this.empl.push({ id: id, name: name_1, val: val });
            }
        }
        if (this.data.length == 0) {
            this.messages = this.emp_list;
            this.result = this.messages;
            this.close();
        }
        if (this.userDup == true) {
            //for socket.io
            // this.connectToServer();
        }
    };
    ValidationModalComponent.prototype.action = function (id, status, emp_id, i) {
        var _this = this;
        if (status == 'PENDING') {
            var v = [];
            v.push(emp_id);
            var employee = v;
            if (this.message == 'Certificate of Attendance') {
                var disposable = this.modalService.addDialog(CertificateOfAttendanceModalComponent, {
                    title: 'Edit Certificate of Attendance',
                    button: 'Update',
                    edit: true,
                    coa_id: id,
                    single: true,
                    employee_id: employee,
                    emp: this.emp
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.data.splice(i, 1);
                        _this.emp_list.push(emp_id);
                    }
                    _this.ngOnInit();
                });
            }
            else if (this.message == 'Leave') {
                var disposable = this.modalService.addDialog(LeaveModalComponent, {
                    title: 'Update Leave',
                    button: 'Update',
                    edit: true,
                    leave_id: id,
                    single: true,
                    user: this.user,
                    employee_id: employee,
                    emp: this.emp
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.data.splice(i, 1);
                        _this.emp_list.push(emp_id);
                    }
                    _this.ngOnInit();
                });
            }
            else if (this.message == 'Official Business') {
                var disposable = this.modalService.addDialog(OfficialBusinessModalComponent, {
                    title: 'Edit Business',
                    button: 'Update',
                    edit: true,
                    ob_id: id,
                    single: true,
                    employee_id: employee,
                    emp: this.emp
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.data.splice(i, 1);
                        _this.emp_list.push(emp_id);
                    }
                    _this.ngOnInit();
                });
            }
            else if (this.message == 'Overtime') {
                var disposable = this.modalService.addDialog(OvertimeModalComponent, {
                    title: 'Edit Overtime',
                    button: 'Update',
                    edit: true,
                    ot_id: id,
                    single: true,
                    employee_id: employee,
                    emp: this.emp
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.data.splice(i, 1);
                        _this.emp_list.push(emp_id);
                    }
                    _this.ngOnInit();
                });
            }
            else if (this.message == 'Undertime') {
                var disposable = this.modalService.addDialog(UndertimeModalComponent, {
                    title: 'Edit Undertime',
                    button: 'Update',
                    edit: true,
                    ou_id: id,
                    single: true,
                    employee_id: employee,
                    emp: this.emp
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.data.splice(i, 1);
                        _this.emp_list.push(emp_id);
                    }
                    _this.ngOnInit();
                });
            }
            else if (this.message == 'Schedule Adjustment') {
                var disposable = this.modalService.addDialog(ScheduleAdjustmentsModalComponent, {
                    title: 'Edit Schedule Adjustment',
                    edit: true,
                    button: 'Update',
                    sa_id: id,
                    single: true,
                    employee_id: employee,
                    emp: this.emp
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.data.splice(i, 1);
                        _this.emp_list.push(emp_id);
                    }
                    _this.ngOnInit();
                });
            }
        }
        else if (status == 'REJECTED') {
            var disposable = this.modalService.addDialog(ConfirmModalComponent, {
                title: 'Resend ' + this.message,
                message: 'Are you sure you want to Resend this ' + this.message + '?',
                action: 'Pending',
                id: id,
                url: this.url,
                request: true
            }).subscribe(function (message) {
                if (message == true) {
                    _this.data.splice(i, 1);
                    _this.emp_list.push(emp_id);
                }
                _this.ngOnInit();
            });
        }
        else if (status == 'CANCELLED') {
            var disposable = this.modalService.addDialog(ConfirmModalComponent, {
                title: 'Uncancel ' + this.message,
                message: 'Are you sure you want to Uncancel this ' + this.message + '?',
                action: 'Pending',
                id: id,
                url: this.url,
                request: true
            }).subscribe(function (message) {
                if (message == true) {
                    _this.data.splice(i, 1);
                    _this.emp_list.push(emp_id);
                }
                _this.ngOnInit();
            });
        }
        else if (status == 'APPROVED') {
            var disposable = this.modalService.addDialog(RemarksModalComponent, {
                title: 'Cancel ' + this.message + '?',
                id: id,
                url: this.url,
                button: 'Cancel Request'
            }).subscribe(function (message) {
                if (message == true) {
                    _this.data.splice(i, 1);
                    _this.emp_list.push(emp_id);
                }
                _this.ngOnInit();
            });
        }
    };
    ValidationModalComponent.prototype.cancel = function () {
        this.messages = this.emp_list;
        this.result = this.messages;
        this.close();
    };
    ValidationModalComponent.prototype.confirm = function () {
        var _this = this;
        var id = this.data[0].id;
        var status = this.data[0].status;
        var emp_id = this.data[0].emp_id;
        if (status == 'PENDING') {
            if (this.message == 'Certificate of Attendance') {
                var disposable = this.modalService.addDialog(CertificateOfAttendanceModalComponent, {
                    title: 'Edit Certificate of Attendance',
                    button: 'Update',
                    edit: true,
                    coa_id: id,
                    user: true,
                    employee_id: emp_id
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.data.splice(0, 1);
                        _this.emp_list.push(emp_id);
                    }
                    _this.ngOnInit();
                });
            }
            else if (this.message == 'Leave') {
                var disposable = this.modalService.addDialog(LeaveModalComponent, {
                    title: 'Update Leave',
                    button: 'Update',
                    edit: true,
                    leave_id: id,
                    user: this.user,
                    employee_id: emp_id
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.data.splice(0, 1);
                        _this.emp_list.push(emp_id);
                    }
                    _this.ngOnInit();
                });
            }
            else if (this.message == 'Official Business') {
                var disposable = this.modalService.addDialog(OfficialBusinessModalComponent, {
                    title: 'Edit Business',
                    button: 'Update',
                    edit: true,
                    ob_id: id,
                    user: true,
                    employee_id: emp_id
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.data.splice(0, 1);
                        _this.emp_list.push(emp_id);
                    }
                    _this.ngOnInit();
                });
            }
            else if (this.message == 'Overtime') {
                var disposable = this.modalService.addDialog(OvertimeModalComponent, {
                    title: 'Edit Overtime',
                    button: 'Update',
                    edit: true,
                    ot_id: id,
                    user: true,
                    employee_id: emp_id
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.data.splice(0, 1);
                        _this.emp_list.push(emp_id);
                    }
                    _this.ngOnInit();
                });
            }
            else if (this.message == 'Undertime') {
                var disposable = this.modalService.addDialog(UndertimeModalComponent, {
                    title: 'Edit Undertime',
                    button: 'Update',
                    edit: true,
                    ou_id: id,
                    user: true,
                    employee_id: emp_id
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.data.splice(0, 1);
                        _this.emp_list.push(emp_id);
                    }
                    _this.ngOnInit();
                });
            }
            else if (this.message == 'Schedule Adjustment') {
                var disposable = this.modalService.addDialog(ScheduleAdjustmentsModalComponent, {
                    title: 'Edit Schedule Adjustment',
                    edit: true,
                    button: 'Update',
                    sa_id: id,
                    user: true,
                    employee_id: emp_id
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.data.splice(0, 1);
                        _this.emp_list.push(emp_id);
                    }
                    _this.ngOnInit();
                });
            }
        }
        else if (status == 'REJECTED' || status == 'CANCELLED') {
            this.form.value.url = this.url;
            this.form.value.remarks = null;
            this.form.value.status_id = 1;
            this.form.value.id = id;
            var model = this.form.value;
            this._common_service.updateRequest(model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                // socket.io emit
                // this.socket.emit('notify', this.poststore);
                _this.result = true;
                if (_this.message == 'Certificate of Attendance') {
                    _this._rt.navigateByUrl('admin/my-coa-list');
                }
                _this.success_title = "Success!";
                _this.success_message = "Successfully";
                setTimeout(function () {
                    _this.close();
                }, 2000);
            }, function (err) { return _this.catchError(err); });
        }
        // else if(status == 'APPROVED'){
        // 	let disposable = this.modalService.addDialog(RemarksModalComponent, {
        // 	title:'Cancel '+this.message+'?',
        // 	id:id,
        // 	url:this.url,
        // 	button:'Cancel Request'
        // 	}).subscribe((message)=>{
        // 		if(message == true){
        // 			this.result = true;
        //               	this.close();
        //               }
        // 	});
        // }
        else {
            this.messages = this.emp_list;
            this.result = this.messages;
            this.close();
        }
    };
    ValidationModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    ValidationModalComponent.prototype.EmployeeCheckbox = function (index) {
        if (this.empl[index].val == false) {
            this.empl[index].val = true;
        }
        else {
            this.empl[index].val = false;
            this.checkAllValidation = false;
        }
        var len = this.empl.length;
        var i = 0;
        for (var x = 0; x < len; ++x) {
            if (this.empl[x].val == true) {
                i++;
            }
        }
        if (i > 0) {
            this.validate = true;
        }
        else {
            this.validate = false;
        }
    };
    ValidationModalComponent.prototype.checkAll = function () {
        if (this.checkAllValidation == false) {
            var leng = this.empl.length;
            for (var i = 0; i < leng; i++) {
                this.empl[i].val = true;
            }
            this.validate = true;
            this.checkAllValidation = true;
        }
        else {
            var len = this.empl.length;
            for (var i = 0; i < len; i++) {
                this.empl[i].val = false;
            }
            this.validate = false;
            this.checkAllValidation = false;
        }
    };
    ValidationModalComponent.prototype.submitToOB = function () {
        var _this = this;
        var len = this.empl.length;
        var emp_id = [];
        for (var i = 0; i < len; ++i) {
            if (this.empl[i].val) {
                emp_id.push(this.empl[i].id.toString());
            }
        }
        var disposable = this.modalService.addDialog(OfficialBusinessModalComponent, {
            title: 'Create Official Business',
            button: 'Add',
            create: true,
            multi: this.admin,
            logs: true,
            employee_id: emp_id,
            remarks: this.remarks,
            emp: this.emp,
            empStats: true,
            admin: this.admin,
            user: this.user
        }).subscribe(function (message) {
            if (message == true) {
                _this.result = true;
                _this._rt.navigateByUrl('admin/official-business-list');
                _this.close();
            }
        });
    };
    ValidationModalComponent.prototype.connectToServer = function () {
        var _this = this;
        var socketUrl = this.host;
        this.socket = io.connect(socketUrl);
        this.socket.on("connect", function () { return _this.connect(); });
        this.socket.on("disconnect", function () { return _this.disconnect(); });
        this.socket.on("error", function (error) {
            console.log("ERROR: \"" + error + "\" (" + socketUrl + ")");
        });
    };
    // Handle connection opening
    ValidationModalComponent.prototype.connect = function () {
        // Request initial list when connected
        this.socket.emit("join", 'Client User -> ');
    };
    // Handle connection closing
    ValidationModalComponent.prototype.disconnect = function () {
        console.log("Disconnected from \"" + this.host + "\"");
    };
    ValidationModalComponent = __decorate([
        Component({
            selector: 'app-validation-modal',
            templateUrl: './validation-modal.component.html',
            styleUrls: ['./validation-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            CommonService,
            Configuration,
            Router])
    ], ValidationModalComponent);
    return ValidationModalComponent;
}(DialogComponent));
export { ValidationModalComponent };
//# sourceMappingURL=validation-modal.component.js.map