import { Component } from '@angular/core';
import { Configuration } from '../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../../services/common.service';
import { CertificateOfAttendanceModalComponent } from '../certificate-of-attendance/certificate-of-attendance-modal/certificate-of-attendance-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { RemarksModalComponent } from '../remarks-modal/remarks-modal.component';
import { LeaveModalComponent } from '../leave/leave-modal/leave-modal.component';
import { OfficialBusinessModalComponent } from '../officialbusiness/official-business-modal/official-business-modal.component';
import { OvertimeModalComponent } from '../overtime/overtime-modal/overtime-modal.component';
import { UndertimeModalComponent } from '../official-undertime/undertime-modal/undertime-modal.component';
import { ScheduleAdjustmentsModalComponent } from '../schedule-adjustments/schedule-adjustments-modal/schedule-adjustments-modal.component';
import { Router, ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-validation-modal',
  templateUrl: './validation-modal.component.html',
  styleUrls: ['./validation-modal.component.css']
})
export class ValidationModalComponent extends DialogComponent<null, boolean> {
  
	public success_title;
	public success_message;
	public error_title;
	public error_message;
	public poststore: any;
	title: string;
	data=[];
	url:any;
	message:any;

	userDup = false;
	adminDup = false;
	checkAllValidation=false;
	validate = false;
	user = false;
	logs = false;
	emp_credentials = false;

	empl=[];
	date:any;
	remarks:any;
	emp_list=[];
	messages:any;
	emp:any;
	form:any;
	admin:any;

	socket: SocketIOClient.Socket;
    host:any;

    emp_val = false;

 constructor(
   private modalService: DialogService,
   private _fb: FormBuilder, 
   private _ar: ActivatedRoute,
   private _common_service: CommonService,
   private _conf: Configuration,
   private _rt: Router,
   ){
    super(modalService);
    this.host = this._conf.socketUrl;
    this.form = _fb.group({  
    }); 
  
  }
	ngOnInit() {

		if(this.logs){
			let val = false;
			this.empl = [];
			if (this.user) {
				this.validate = true;
				val = true;
			}
			
			let len = this.data.length;
			for (var i = 0; i < len; ++i) {
				let name = this.data[i].name;
				let id =  this.data[i].id;
				this.empl.push({id,name,val});
			}
		}

		if(this.data.length == 0){
			this.messages = this.emp_list;
			this.result = this.messages;
			this.close();
		}

		if (this.userDup == true) {
			//for socket.io
			// this.connectToServer();
		}
	}
 
	action(id,status,emp_id,i) {

		if(status == 'PENDING'){
			let v = [];
			v.push(emp_id);
			let employee = v;
			if (this.message == 'Certificate of Attendance') {
				let disposable = this.modalService.addDialog(CertificateOfAttendanceModalComponent, {
		            title:'Edit Certificate of Attendance',
		            button:'Update',
		            edit:true,
		            coa_id:id,
		            single:true,
		            employee_id:employee,
            		emp:this.emp
		        	}).subscribe((message)=>{
		                if(message == true){
		                	this.data.splice(i,1);
		                	this.emp_list.push(emp_id);
		                }
		                this.ngOnInit();
		        });
			}
			else if (this.message == 'Leave'){
				let disposable = this.modalService.addDialog(LeaveModalComponent, {
		            title:'Update Leave',
		            button:'Update',
		            edit:true,
		            leave_id:id,
		            single:true,
		            user:this.user,
		            employee_id:employee,
            		emp:this.emp
		        	}).subscribe((message)=>{
		                if(message == true){
		                	this.data.splice(i,1);
		                	this.emp_list.push(emp_id);
	                	}
	                this.ngOnInit();
		        });
			}
			else if (this.message == 'Official Business'){
				let disposable = this.modalService.addDialog(OfficialBusinessModalComponent, {
		            title:'Edit Business',
		            button:'Update',
		            edit:true,
		            ob_id:id,
		            single:true,
		            employee_id:employee,
            		emp:this.emp
		        	}).subscribe((message)=>{
		        		if(message == true){
	                		this.data.splice(i,1);
	                		this.emp_list.push(emp_id);
	                	}
	                this.ngOnInit();
		        });
			}
			else if (this.message == 'Overtime'){
				let disposable = this.modalService.addDialog(OvertimeModalComponent, {
		            title:'Edit Overtime',
		            button:'Update',
		            edit:true,
		            ot_id:id,
		            single:true,
		            employee_id:employee,
            		emp:this.emp
		        	}).subscribe((message)=>{
		        		if(message == true){
	                		this.data.splice(i,1);
	                		this.emp_list.push(emp_id);
	                	}
		                this.ngOnInit();
		        });
			}
			else if (this.message == 'Undertime'){
				let disposable = this.modalService.addDialog(UndertimeModalComponent, {
		            title:'Edit Undertime',
		            button:'Update',
		            edit:true,
		            ou_id:id,
		            single:true,
		            employee_id:employee,
            		emp:this.emp
		        	}).subscribe((message)=>{
		        		if(message == true){
	                		this.data.splice(i,1);
	                		this.emp_list.push(emp_id);
	                	}
		                this.ngOnInit();
		        });
			}
			else if (this.message == 'Schedule Adjustment'){
				let disposable = this.modalService.addDialog(ScheduleAdjustmentsModalComponent, {
		            title:'Edit Schedule Adjustment',
		            edit:true,
		            button:'Update',
		            sa_id:id,
		            single:true,
		            employee_id:employee,
            		emp:this.emp
		        	}).subscribe((message)=>{
		        		if(message == true){
	                		this.data.splice(i,1);
	                		this.emp_list.push(emp_id);
	                	}
		                this.ngOnInit();
		        });
			}
		}
		else if(status == 'REJECTED'){
			let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Resend '+this.message,
            message:'Are you sure you want to Resend this '+this.message+'?',
            action:'Pending',
            id:id,
            url:this.url,
            request:true
        	}).subscribe((message)=>{
                if(message == true){
                	this.data.splice(i,1);
                	this.emp_list.push(emp_id);
                }
                this.ngOnInit();
            });
		}
		else if(status == 'CANCELLED'){
			let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Uncancel '+this.message,
            message:'Are you sure you want to Uncancel this '+this.message+'?',
            action:'Pending',
            id:id,
            url:this.url,
            request:true
        	}).subscribe((message)=>{
                if(message == true){
                	this.data.splice(i,1);
                	this.emp_list.push(emp_id);
                }
                this.ngOnInit();
            });
		}
		else if(status == 'APPROVED'){
			let disposable = this.modalService.addDialog(RemarksModalComponent, {
			title:'Cancel '+this.message+'?',
			id:id,
			url:this.url,
			button:'Cancel Request'
			}).subscribe((message)=>{
				if(message == true){
                	this.data.splice(i,1);
                	this.emp_list.push(emp_id);
                }
                this.ngOnInit();
			});
		}
	}

	cancel(){
		this.messages = this.emp_list;
		this.result = this.messages;
		this.close();
	}

	confirm(){
		let id = this.data[0].id;
		let status = this.data[0].status;
		let emp_id = this.data[0].emp_id;

		if(status == 'PENDING'){
			if (this.message == 'Certificate of Attendance') {
				let disposable = this.modalService.addDialog(CertificateOfAttendanceModalComponent, {
		            title:'Edit Certificate of Attendance',
		            button:'Update',
		            edit:true,
		            coa_id:id,
		            user:true,
		            employee_id:emp_id
		        	}).subscribe((message)=>{
		                if(message == true){
		                	this.data.splice(0,1);
		                	this.emp_list.push(emp_id);
		                }
		                this.ngOnInit();
		        });
			}
			else if (this.message == 'Leave'){
				let disposable = this.modalService.addDialog(LeaveModalComponent, {
		            title:'Update Leave',
		            button:'Update',
		            edit:true,
		            leave_id:id,
		            user:this.user,
		            employee_id:emp_id
		        	}).subscribe((message)=>{
		                if(message == true){
		                	this.data.splice(0,1);
		                	this.emp_list.push(emp_id);
		                }
		                this.ngOnInit();
		        });
			}
			else if (this.message == 'Official Business'){
				let disposable = this.modalService.addDialog(OfficialBusinessModalComponent, {
		            title:'Edit Business',
		            button:'Update',
		            edit:true,
		            ob_id:id,
		            user:true,
		            employee_id:emp_id
		        	}).subscribe((message)=>{
		        		if(message == true){
		                	this.data.splice(0,1);
		                	this.emp_list.push(emp_id);
		                }
		                this.ngOnInit();
		        });
			}
			else if (this.message == 'Overtime'){
				let disposable = this.modalService.addDialog(OvertimeModalComponent, {
		            title:'Edit Overtime',
		            button:'Update',
		            edit:true,
		            ot_id:id,
		            user:true,
		            employee_id:emp_id
		        	}).subscribe((message)=>{
		        		if(message == true){
		                	this.data.splice(0,1);
		                	this.emp_list.push(emp_id);
		                }
		                this.ngOnInit();
		        });
			}
			else if (this.message == 'Undertime'){
				let disposable = this.modalService.addDialog(UndertimeModalComponent, {
		            title:'Edit Undertime',
		            button:'Update',
		            edit:true,
		            ou_id:id,
		            user:true,
		            employee_id:emp_id
		        	}).subscribe((message)=>{
		        		if(message == true){
		                	this.data.splice(0,1);
		                	this.emp_list.push(emp_id);
		                }
		                this.ngOnInit();
		        });
			}
			else if (this.message == 'Schedule Adjustment'){
				let disposable = this.modalService.addDialog(ScheduleAdjustmentsModalComponent, {
		            title:'Edit Schedule Adjustment',
		            edit:true,
		            button:'Update',
		            sa_id:id,
		            user:true,
		            employee_id:emp_id
		        	}).subscribe((message)=>{
		        		if(message == true){
	                		this.data.splice(0,1);
	                		this.emp_list.push(emp_id);
	                	}
		                this.ngOnInit();
		        });
			}
		}
		else if(status == 'REJECTED' || status == 'CANCELLED'){

		    this.form.value.url = this.url;
		    this.form.value.remarks = null;
		    this.form.value.status_id = 1;
		    this.form.value.id = id;

		    let model = this.form.value;

		    this._common_service.updateRequest(model)
		    .subscribe(
		      data => {
		        this.poststore = Array.from(data); 
		        // socket.io emit
		        // this.socket.emit('notify', this.poststore);
		        this.result = true;
		        if (this.message == 'Certificate of Attendance') {
		        	this._rt.navigateByUrl('admin/my-coa-list');
		        }
		        this.success_title = "Success!";
		        this.success_message = "Successfully";
				    setTimeout(() => {
		  		   this.close();
		      	}, 2000);
				
		      },
		      err => this.catchError(err)
		    );
		}
		// else if(status == 'APPROVED'){
		// 	let disposable = this.modalService.addDialog(RemarksModalComponent, {
		// 	title:'Cancel '+this.message+'?',
		// 	id:id,
		// 	url:this.url,
		// 	button:'Cancel Request'
		// 	}).subscribe((message)=>{
		// 		if(message == true){
		// 			this.result = true;
  //               	this.close();
  //               }
		// 	});
		// }
		else{
			this.messages = this.emp_list;
			this.result = this.messages;
			this.close();
		}
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	EmployeeCheckbox(index:any){

		if(this.empl[index].val == false){
			this.empl[index].val = true;
			
		}
		else{
			this.empl[index].val = false;
			this.checkAllValidation = false;
		}

		let len = this.empl.length;
		let i = 0;
		for (var x = 0; x < len; ++x) {
			
			if(this.empl[x].val == true){
				i++;
			}
		}

		if(i > 0){
			this.validate = true;
		}
		else{
			this.validate = false;
		}
	}

	checkAll(){

		if(this.checkAllValidation == false){
			let leng = this.empl.length;
			for (var i = 0; i < leng; i++) {
				this.empl[i].val = true;
			}
			this.validate = true;
			this.checkAllValidation = true;
		}
		else{
			let len = this.empl.length;
			for (var i = 0; i < len; i++) {
				this.empl[i].val = false;
			}
			this.validate = false;
			this.checkAllValidation = false;
		}
	}
	submitToOB(){
		let len = this.empl.length;
		let emp_id = [];
    	for (let i = 0; i < len; ++i) {
    		if (this.empl[i].val) {
    			emp_id.push(this.empl[i].id.toString());
    		}
    	}

       	let disposable = this.modalService.addDialog(OfficialBusinessModalComponent, {
		    title:'Create Official Business',
		    button:'Add',
            create:true,
            multi:this.admin,
            logs:true,
            employee_id:emp_id,
            remarks:this.remarks,
	        emp:this.emp,
	        empStats:true,
	        admin:this.admin,
	        user:this.user
			}).subscribe((message)=>{
		        if(message == true){
		        	this.result = true;
		        	this._rt.navigateByUrl('admin/official-business-list');
		            this.close();
		        }
		    });
	}

	connectToServer(){
	    let socketUrl = this.host;
	    this.socket = io.connect(socketUrl);
	    this.socket.on("connect", () => this.connect());
	    this.socket.on("disconnect", () => this.disconnect());
	    
	    this.socket.on("error", (error: string) => {
	      console.log(`ERROR: "${error}" (${socketUrl})`);
	    });

	   
	  }

	// Handle connection opening
	private connect() {
	    // Request initial list when connected
	    this.socket.emit("join", 'Client User -> ');
	}
	// Handle connection closing
	private disconnect() {
	    console.log(`Disconnected from "${this.host}"`);
	}

}
