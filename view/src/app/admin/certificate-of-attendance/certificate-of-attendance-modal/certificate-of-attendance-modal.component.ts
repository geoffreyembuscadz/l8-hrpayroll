import { Component } from '@angular/core';
import { Configuration } from '../../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import { CertificateOfAttendanceService } from '../../../services/certificate-of-attendance.service';
import { CertificateOfAttendance } from '../../../model/certificate-of-attendance';
import { CommonService } from '../../../services/common.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { Select2OptionData } from 'ng2-select2';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TimepickerModule } from 'ngx-bootstrap';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';

@Component({
  selector: 'app-certificate-of-attendance-modal',
  templateUrl: './certificate-of-attendance-modal.component.html',
  styleUrls: ['./certificate-of-attendance-modal.component.css']
})
export class CertificateOfAttendanceModalComponent extends DialogComponent<null, boolean> {

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public options: Select2Options;
	public current: any;
	public type : any;
	public type_value : any;
	public start_date:any;
	public end_date:any;
	public date_in = moment();
	public mainInput = {
        start: moment(),
        end: moment().subtract(6, 'month')
    }
	coa = new CertificateOfAttendance();
	COAForm:any;
	type_current:any;
	emp:any;
	poststore:any;
	user_id:any;
	public ismeridian:boolean = true;
	validate = false;
	empStats=false;
	typeStats=false;
	public time:Date = new Date();
	employee_id:any;
	coa_data:any;
	coa_id:any;
	remarks:any;
    type_id:any;
    edit:any;
    create:any;
    multi:any;
    single:any;
    value:any;
    date:any;
    user=false;
    admin=false;
    dupVal = false;
    coaValidation=false;
    supervisor_id:any;
    role_id:any;

    created_by:any;
    messenger_id:any;

    socket: SocketIOClient.Socket;
    host:any;

  constructor(
  	private _common_service: CommonService,
   	private _coaservice: CertificateOfAttendanceService, 
   	dialogService: DialogService, 
   	private _fb: FormBuilder, 
   	private _ar: ActivatedRoute,
   	private daterangepickerOptions: DaterangepickerConfig,
   	private _auth_service: AuthUserService,
   	private modalService: DialogService,
   	private _rt: Router,
    private _conf: Configuration
  ) {
  	super(dialogService);

  	this.host = this._conf.socketUrl;

    this.COAForm = _fb.group({
		'remarks': 			[this.coa.remarks, [Validators.required]],
		'status_id':		[null],
		'time':				[null],
		'date':				[null]
    }); 

    this.daterangepickerOptions.settings = {
        locale: { format: 'MM/DD/YYYY' },
        alwaysShowCalendars: false,
        singleDatePicker: true,
		showDropdowns: true,
		opens: "center"
	}; 
  	}

  	ngOnInit() {
		
		//use for my-coa-list create button
		if(this.user == true){
    		this.empStats = true;
    		this.get_employee();
    	}

    	//use in createCOA() in attendance-list
    	if(this.coaValidation == true){
    		this.empStats = true;
    		this.get_employee();
	   		this.duplicateEntryValidation();
	   	}

	   	//edit button in my-coa-list and coa-list
		if(this.edit == true){
			this.getCOA();
    		this.validate = true;
    		this.get_employee();
    	}
    	else{
	   		this.get_employee();
    	}
	   	//uncomment this for socket.io
    	// this.connectToServer();
    	
		this.get_type();

	}

	getCOA(){

		this._coaservice.getCOA(this.coa_id).subscribe(
	      data => {
	        this.coa_data=(data);

	        this.date_in = this.coa_data.date;
	        let date = this.coa_data.date + " " + this.coa_data.time;
	        this.time = new Date(date);
	        this.remarks = this.coa_data.reason;
	        this.type_id = this.coa_data.COA_type_id;
	        this.employee_id = this.coa_data.emp_id;
	      },
	      err => console.error(err)
    	);
    	
	}

	get_employee(){

		this.employee = this.emp;
		this.employee_value = [];
		if(this.edit == true) {
			this.employee_value = this.employee_id;
			this.validate = true;
        } 

        if(this.coaValidation == true){
        	this.employee_value = this.employee_id;
        }
		this.options = {
			multiple: true
		}
		this.current = this.employee_value;
	}
	changed(data: any) {

		if(this.single == true){
			let v = [];
			v.push(data.value);
			this.current = v;
		}
		else{
			this.current = data.value;	
		}
		let len = this.current.length;

		if(len >= 1){
			this.empStats = true;
		}
		else{
			this.empStats = false;
		}

	   	this.validation();

	   	if(this.coaValidation != true){
	   		this.duplicateEntryValidation();
	   	}
	}

	validation(){
		if(this.empStats == true && this.typeStats == true && this.dupVal == true|| this.edit == true){
      		this.validate=true;
     	}
     	else{
     		this.validate=false;
     	}
	}

	get_type(){

		this.type = this._common_service.getCOAType().
		subscribe(
			data => {
				this.type = Array.from(data);
				this.type_value = [];

				if(this.edit == true) {
					this.type_value = this.type_id;
					this.validate = true;
	            }

				if(this.create == true && this.coaValidation == false){
					let id = 0;
			        let text = 'Select Type';

			        this.type.unshift({id,text});
			        this.type_value = this.value;
				}

	            if(this.coaValidation == true){
	            	this.type_value = this.type_id;
	            }

				this.type_current = this.type_value;
				
			},
			err => console.error(err)
		);
	}

	changedType(data: any) {
		this.type_current = data.value;
		let len = this.type_current.length;

		if(len >= 1 && this.type_current != 0){
			this.typeStats = true;
		}
		else{
			this.typeStats = false;
		}

		this.validation();
	}

	onSubmit(){

		this.duplicateEntryValidation();

		let  coa = this.COAForm.value;

		//I do you use different routes depending if the user is an admin or an employee for security purposes
		if(this.edit == true && this.user == false){

			this.COAForm.value.id= this.coa_id;
			coa = this.COAForm.value;

			this._coaservice
			.updateCOA(coa)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}
		else if (this.edit == true && this.user == true) {

			this.COAForm.value.id= this.coa_id;
			coa = this.COAForm.value;

			this._coaservice
			.updateMyCOA(coa)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}

		if(this.create == true && this.user == false){
			this._coaservice
			.createCOA(coa)
			.subscribe(
			data => {
				this.poststore = data;
				this.validate = false;
				this.errorCatch();
				
			},
			err => this.catchError(err)
			);
		}
		else if (this.create == true && this.user == true) {
			this._coaservice
			.createMyCOA(coa)
			.subscribe(
			data => {
				this.poststore = data;
				this.validate = false;
				this.errorCatch();
				
			},
			err => this.catchError(err)
			);
		}

	}

	connectToServer(){
	    let socketUrl = this.host;
	    this.socket = io.connect(socketUrl);
	    this.socket.on("connect", () => this.connect());
	    this.socket.on("disconnect", () => this.disconnect());
	    
	    this.socket.on("error", (error: string) => {
	      console.log(`ERROR: "${error}" (${socketUrl})`);
	    });

	   
	  }

	// Handle connection opening
	private connect() {
	    // Request initial list when connected
	    this.socket.emit("join", 'Client User -> ');
	}
	// Handle connection closing
	private disconnect() {
	    console.log(`Disconnected from "${this.host}"`);
	}

	errorCatch(){

		/**
		 * no logIn or LogOut recorded
		 * @param obj this.poststore.data ito yung data na dapat masisave kaso dahil sa validation pinabalik ulit dito
		 * @param boolean logs pang validation lang sa validationmodal
		 * @param string remarks
		 * @param date date
		 * @param obj emp list to ng employee, galing to sa coa-list, nilagay ito para mabilis at hindi na paulit ulit yung pag get ng employee list
		 * @returns pag ang message ay nag true, ibigsabhin nun nag okey sya dun sa validation-modal
		 */
		if(this.poststore.status_code == 404){
			this.error_title = 'Warning';
			this.error_message = "Request doesn't much any logs";

			setTimeout(() => {
				let disposable = this.modalService.addDialog(ValidationModalComponent, {
				title:'Warning',
				message:'Certificate of Attendance',
				url:'certificate_of_attendance',
				data:this.poststore.data,
				logs:true,
				remarks:this.remarks,
				date:this.date,
	            emp:this.emp,
				user:this.user,
				admin:this.admin
				}).subscribe((message)=>{
					if(message == true){
						this.close();
					}
				});
	      	}, 1000);
		}
		else if(this.poststore.status_code == 400){

			this.error_title = 'Warning';
			this.error_message = 'Request is already exist';

			let temp;
			setTimeout(() => {
				let disposable = this.modalService.addDialog(ValidationModalComponent, {
				title:'Warning',
				message:'Certificate of Attendance',
				url:'certificate_of_attendance',
				duplicate:true,
				data:this.poststore.data,
				userDup:this.user,
				adminDup:this.admin,
            	emp:this.emp
				}).subscribe((message)=>{
					if (message == true) { 
						this.error_title = '';
						this.error_message = '';

						setTimeout(() => {
				  		   this.close();
				      	}, 1000);
					}
					else if(message != false){
    					temp = message;

    					for (let x = 0; x < temp.length; ++x) {
							let index = this.current.indexOf(temp[x]);
	    					this.current.splice(index, 1);
    					}

	    				this.employee_value = this.current;

    					if(this.current.length == 0){
	    					this.error_title = '';
							this.error_message = '';
							setTimeout(() => {
					  		   this.close();
					      	}, 1000);
    					}
					}
				});
	      	}, 1000);

		}
		else if(this.poststore.status_code == 200){
			this.success_title = '200 OK';
			this.success_message = 'No Changes Happen';
			setTimeout(() => {
	  		   this.close();
	      	}, 1000);
		}
		else{
			// socket.io emit
			// this.socket.emit('notify', this.poststore);
			this.validate = false;
			this.success_title = "Success";
			this.success_message = "Successfully";
			this.result = true;
			setTimeout(() => {
				if (this.coaValidation == true) {
					this.result = true;
				}
	  		   this.close();
	      	}, 2000);

		}
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        this.date = moment(dateInput.start).format("YYYY-MM-DD");
        this.date_in = this.date;

        this.duplicateEntryValidation();
    }

	public toggleMode():void {
		this.ismeridian = !this.ismeridian;
	}

	duplicateEntryValidation(){

		this.COAForm.value.employee_id = this.current;
		this.COAForm.value.supervisor_id = this.supervisor_id;
		this.COAForm.value.coa_type_id = this.type_current;
		this.COAForm.value.created_by = this.user_id;
		this.COAForm.value.date = this.date;
		let time =  moment(this.time).format("HH:mm");
		this.COAForm.value.time = time;

		if(this.date == null){			
			let date = this.date_in;
			this.COAForm.value.date = moment(date).format("YYYY-MM-DD");
		}

		if(this.user == true  && this.create == true || this.user == true && this.edit == true){
			let v = [];
			v.push(this.employee_id);
			this.COAForm.value.employee_id = v;
		}

		if(this.edit == true){
			this.COAForm.value.edit = this.edit;
			this.COAForm.value.id= this.coa_id;
		}
		else{
			this.COAForm.value.edit = false;
		}

		let coa = this.COAForm.value;

		if(this.COAForm.value.employee_id != 0 && this.COAForm.value.date != null || this.edit == true){

			this._coaservice
			.duplicateEntryValidation(coa)
			.subscribe(
				data => {
					this.poststore = data;

						if(this.poststore.message == "duplicate"){
							this.dupVal = false;
							this.errorCatch();
						}
						else{
							this.dupVal = true;
							this.error_title = '';
							this.error_message = '';
						}

						this.validation();
				},
				err => this.catchError(err)
			);

		}

	}

}
