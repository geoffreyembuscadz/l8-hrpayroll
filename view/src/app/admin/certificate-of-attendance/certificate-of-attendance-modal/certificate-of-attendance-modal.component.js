var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Configuration } from '../../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CertificateOfAttendanceService } from '../../../services/certificate-of-attendance.service';
import { CertificateOfAttendance } from '../../../model/certificate-of-attendance';
import { CommonService } from '../../../services/common.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';
var CertificateOfAttendanceModalComponent = /** @class */ (function (_super) {
    __extends(CertificateOfAttendanceModalComponent, _super);
    function CertificateOfAttendanceModalComponent(_common_service, _coaservice, dialogService, _fb, _ar, daterangepickerOptions, _auth_service, modalService, _rt, _conf) {
        var _this = _super.call(this, dialogService) || this;
        _this._common_service = _common_service;
        _this._coaservice = _coaservice;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this._auth_service = _auth_service;
        _this.modalService = modalService;
        _this._rt = _rt;
        _this._conf = _conf;
        _this.date_in = moment();
        _this.mainInput = {
            start: moment(),
            end: moment().subtract(6, 'month')
        };
        _this.coa = new CertificateOfAttendance();
        _this.ismeridian = true;
        _this.validate = false;
        _this.empStats = false;
        _this.typeStats = false;
        _this.time = new Date();
        _this.user = false;
        _this.admin = false;
        _this.dupVal = false;
        _this.coaValidation = false;
        _this.host = _this._conf.socketUrl;
        _this.COAForm = _fb.group({
            'remarks': [_this.coa.remarks, [Validators.required]],
            'status_id': [null],
            'time': [null],
            'date': [null]
        });
        _this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
            showDropdowns: true,
            opens: "center"
        };
        return _this;
    }
    CertificateOfAttendanceModalComponent.prototype.ngOnInit = function () {
        //use for my-coa-list create button
        if (this.user == true) {
            this.empStats = true;
            this.get_employee();
        }
        //use in createCOA() in attendance-list
        if (this.coaValidation == true) {
            this.empStats = true;
            this.get_employee();
            this.duplicateEntryValidation();
        }
        //edit button in my-coa-list and coa-list
        if (this.edit == true) {
            this.getCOA();
            this.validate = true;
            this.get_employee();
        }
        else {
            this.get_employee();
        }
        //uncomment this for socket.io
        // this.connectToServer();
        this.get_type();
    };
    CertificateOfAttendanceModalComponent.prototype.getCOA = function () {
        var _this = this;
        this._coaservice.getCOA(this.coa_id).subscribe(function (data) {
            _this.coa_data = (data);
            _this.date_in = _this.coa_data.date;
            var date = _this.coa_data.date + " " + _this.coa_data.time;
            _this.time = new Date(date);
            _this.remarks = _this.coa_data.reason;
            _this.type_id = _this.coa_data.COA_type_id;
            _this.employee_id = _this.coa_data.emp_id;
        }, function (err) { return console.error(err); });
    };
    CertificateOfAttendanceModalComponent.prototype.get_employee = function () {
        this.employee = this.emp;
        this.employee_value = [];
        if (this.edit == true) {
            this.employee_value = this.employee_id;
            this.validate = true;
        }
        if (this.coaValidation == true) {
            this.employee_value = this.employee_id;
        }
        this.options = {
            multiple: true
        };
        this.current = this.employee_value;
    };
    CertificateOfAttendanceModalComponent.prototype.changed = function (data) {
        if (this.single == true) {
            var v = [];
            v.push(data.value);
            this.current = v;
        }
        else {
            this.current = data.value;
        }
        var len = this.current.length;
        if (len >= 1) {
            this.empStats = true;
        }
        else {
            this.empStats = false;
        }
        this.validation();
        if (this.coaValidation != true) {
            this.duplicateEntryValidation();
        }
    };
    CertificateOfAttendanceModalComponent.prototype.validation = function () {
        if (this.empStats == true && this.typeStats == true && this.dupVal == true || this.edit == true) {
            this.validate = true;
        }
        else {
            this.validate = false;
        }
    };
    CertificateOfAttendanceModalComponent.prototype.get_type = function () {
        var _this = this;
        this.type = this._common_service.getCOAType().
            subscribe(function (data) {
            _this.type = Array.from(data);
            _this.type_value = [];
            if (_this.edit == true) {
                _this.type_value = _this.type_id;
                _this.validate = true;
            }
            if (_this.create == true && _this.coaValidation == false) {
                var id = 0;
                var text = 'Select Type';
                _this.type.unshift({ id: id, text: text });
                _this.type_value = _this.value;
            }
            if (_this.coaValidation == true) {
                _this.type_value = _this.type_id;
            }
            _this.type_current = _this.type_value;
        }, function (err) { return console.error(err); });
    };
    CertificateOfAttendanceModalComponent.prototype.changedType = function (data) {
        this.type_current = data.value;
        var len = this.type_current.length;
        if (len >= 1 && this.type_current != 0) {
            this.typeStats = true;
        }
        else {
            this.typeStats = false;
        }
        this.validation();
    };
    CertificateOfAttendanceModalComponent.prototype.onSubmit = function () {
        var _this = this;
        this.duplicateEntryValidation();
        var coa = this.COAForm.value;
        //I do you use different routes depending if the user is an admin or an employee for security purposes
        if (this.edit == true && this.user == false) {
            this.COAForm.value.id = this.coa_id;
            coa = this.COAForm.value;
            this._coaservice
                .updateCOA(coa)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        else if (this.edit == true && this.user == true) {
            this.COAForm.value.id = this.coa_id;
            coa = this.COAForm.value;
            this._coaservice
                .updateMyCOA(coa)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        if (this.create == true && this.user == false) {
            this._coaservice
                .createCOA(coa)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        else if (this.create == true && this.user == true) {
            this._coaservice
                .createMyCOA(coa)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
    };
    CertificateOfAttendanceModalComponent.prototype.connectToServer = function () {
        var _this = this;
        var socketUrl = this.host;
        this.socket = io.connect(socketUrl);
        this.socket.on("connect", function () { return _this.connect(); });
        this.socket.on("disconnect", function () { return _this.disconnect(); });
        this.socket.on("error", function (error) {
            console.log("ERROR: \"" + error + "\" (" + socketUrl + ")");
        });
    };
    // Handle connection opening
    CertificateOfAttendanceModalComponent.prototype.connect = function () {
        // Request initial list when connected
        this.socket.emit("join", 'Client User -> ');
    };
    // Handle connection closing
    CertificateOfAttendanceModalComponent.prototype.disconnect = function () {
        console.log("Disconnected from \"" + this.host + "\"");
    };
    CertificateOfAttendanceModalComponent.prototype.errorCatch = function () {
        var _this = this;
        /**
         * no logIn or LogOut recorded
         * @param obj this.poststore.data ito yung data na dapat masisave kaso dahil sa validation pinabalik ulit dito
         * @param boolean logs pang validation lang sa validationmodal
         * @param string remarks
         * @param date date
         * @param obj emp list to ng employee, galing to sa coa-list, nilagay ito para mabilis at hindi na paulit ulit yung pag get ng employee list
         * @returns pag ang message ay nag true, ibigsabhin nun nag okey sya dun sa validation-modal
         */
        if (this.poststore.status_code == 404) {
            this.error_title = 'Warning';
            this.error_message = "Request doesn't much any logs";
            setTimeout(function () {
                var disposable = _this.modalService.addDialog(ValidationModalComponent, {
                    title: 'Warning',
                    message: 'Certificate of Attendance',
                    url: 'certificate_of_attendance',
                    data: _this.poststore.data,
                    logs: true,
                    remarks: _this.remarks,
                    date: _this.date,
                    emp: _this.emp,
                    user: _this.user,
                    admin: _this.admin
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.close();
                    }
                });
            }, 1000);
        }
        else if (this.poststore.status_code == 400) {
            this.error_title = 'Warning';
            this.error_message = 'Request is already exist';
            var temp_1;
            setTimeout(function () {
                var disposable = _this.modalService.addDialog(ValidationModalComponent, {
                    title: 'Warning',
                    message: 'Certificate of Attendance',
                    url: 'certificate_of_attendance',
                    duplicate: true,
                    data: _this.poststore.data,
                    userDup: _this.user,
                    adminDup: _this.admin,
                    emp: _this.emp
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.error_title = '';
                        _this.error_message = '';
                        setTimeout(function () {
                            _this.close();
                        }, 1000);
                    }
                    else if (message != false) {
                        temp_1 = message;
                        for (var x = 0; x < temp_1.length; ++x) {
                            var index = _this.current.indexOf(temp_1[x]);
                            _this.current.splice(index, 1);
                        }
                        _this.employee_value = _this.current;
                        if (_this.current.length == 0) {
                            _this.error_title = '';
                            _this.error_message = '';
                            setTimeout(function () {
                                _this.close();
                            }, 1000);
                        }
                    }
                });
            }, 1000);
        }
        else if (this.poststore.status_code == 200) {
            this.success_title = '200 OK';
            this.success_message = 'No Changes Happen';
            setTimeout(function () {
                _this.close();
            }, 1000);
        }
        else {
            // socket.io emit
            // this.socket.emit('notify', this.poststore);
            this.validate = false;
            this.success_title = "Success";
            this.success_message = "Successfully";
            this.result = true;
            setTimeout(function () {
                if (_this.coaValidation == true) {
                    _this.result = true;
                }
                _this.close();
            }, 2000);
        }
    };
    CertificateOfAttendanceModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    CertificateOfAttendanceModalComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        this.date = moment(dateInput.start).format("YYYY-MM-DD");
        this.date_in = this.date;
        this.duplicateEntryValidation();
    };
    CertificateOfAttendanceModalComponent.prototype.toggleMode = function () {
        this.ismeridian = !this.ismeridian;
    };
    CertificateOfAttendanceModalComponent.prototype.duplicateEntryValidation = function () {
        var _this = this;
        this.COAForm.value.employee_id = this.current;
        this.COAForm.value.supervisor_id = this.supervisor_id;
        this.COAForm.value.coa_type_id = this.type_current;
        this.COAForm.value.created_by = this.user_id;
        this.COAForm.value.date = this.date;
        var time = moment(this.time).format("HH:mm");
        this.COAForm.value.time = time;
        if (this.date == null) {
            var date = this.date_in;
            this.COAForm.value.date = moment(date).format("YYYY-MM-DD");
        }
        if (this.user == true && this.create == true || this.user == true && this.edit == true) {
            var v = [];
            v.push(this.employee_id);
            this.COAForm.value.employee_id = v;
        }
        if (this.edit == true) {
            this.COAForm.value.edit = this.edit;
            this.COAForm.value.id = this.coa_id;
        }
        else {
            this.COAForm.value.edit = false;
        }
        var coa = this.COAForm.value;
        if (this.COAForm.value.employee_id != 0 && this.COAForm.value.date != null || this.edit == true) {
            this._coaservice
                .duplicateEntryValidation(coa)
                .subscribe(function (data) {
                _this.poststore = data;
                if (_this.poststore.message == "duplicate") {
                    _this.dupVal = false;
                    _this.errorCatch();
                }
                else {
                    _this.dupVal = true;
                    _this.error_title = '';
                    _this.error_message = '';
                }
                _this.validation();
            }, function (err) { return _this.catchError(err); });
        }
    };
    CertificateOfAttendanceModalComponent = __decorate([
        Component({
            selector: 'app-certificate-of-attendance-modal',
            templateUrl: './certificate-of-attendance-modal.component.html',
            styleUrls: ['./certificate-of-attendance-modal.component.css']
        }),
        __metadata("design:paramtypes", [CommonService,
            CertificateOfAttendanceService,
            DialogService,
            FormBuilder,
            ActivatedRoute,
            DaterangepickerConfig,
            AuthUserService,
            DialogService,
            Router,
            Configuration])
    ], CertificateOfAttendanceModalComponent);
    return CertificateOfAttendanceModalComponent;
}(DialogComponent));
export { CertificateOfAttendanceModalComponent };
//# sourceMappingURL=certificate-of-attendance-modal.component.js.map