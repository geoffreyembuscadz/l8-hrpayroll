var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { CertificateOfAttendanceModalComponent } from '../../certificate-of-attendance/certificate-of-attendance-modal/certificate-of-attendance-modal.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { CertificateOfAttendanceService } from '../../../services/certificate-of-attendance.service';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
import { FormBuilder } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import * as moment from 'moment';
import { ActionCenterModalComponent } from '../../action-center/action-center-modal/action-center-modal.component';
var CertificateOfAttendanceListComponent = /** @class */ (function () {
    function CertificateOfAttendanceListComponent(modalService, _conf, _coaservice, _common_service, daterangepickerOptions, _fb, _auth_service) {
        this.modalService = modalService;
        this._conf = _conf;
        this._coaservice = _coaservice;
        this._common_service = _common_service;
        this.daterangepickerOptions = daterangepickerOptions;
        this._fb = _fb;
        this._auth_service = _auth_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.coa = [];
        this.byStatus = false;
        this.byDate = false;
        this.byCompany = false;
        this.byEmployee = false;
        this.byType = false;
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.btnCreate = false;
        this.btnEdit = false;
        this.btnApproved = false;
        this.btnAction = false;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filterForm = _fb.group({
            'status_id': [null],
            'start_date': [null],
            'end_date': [null],
            'company_id': [null],
            'employee_id': [null],
            'type_id': [null],
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    CertificateOfAttendanceListComponent.prototype.ngOnInit = function () {
        this.dateOption();
        this.getEmployee();
        this.getPermissionId();
    };
    CertificateOfAttendanceListComponent.prototype.getPermissionId = function () {
        var _this = this;
        this._auth_service.getPermissionId().
            subscribe(function (data) {
            _this.permissionValidation(data);
        }, function (err) { return console.error(err); });
    };
    /**
     * permission of buttons to be display
     * @param array user
     */
    CertificateOfAttendanceListComponent.prototype.permissionValidation = function (user) {
        var len = user.length;
        for (var i = 0; i < len; ++i) {
            if (user[i] == "158") {
                this.btnCreate = true;
            }
            else if (user[i] == "159") {
                this.btnEdit = true;
            }
            else if (user[i] == "160") {
                this.btnApproved = true;
            }
        }
        if (this.btnEdit == false && this.btnApproved == false) {
            this.btnAction = true;
        }
        this.data();
    };
    CertificateOfAttendanceListComponent.prototype.data = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._coaservice.getFilteredCOA(model).
            subscribe(function (data) {
            _this.coa = data;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    CertificateOfAttendanceListComponent.prototype.getEmployee = function () {
        var _this = this;
        this._common_service.getEmployee().
            subscribe(function (data) {
            _this.emp = Array.from(data);
            _this.employee = _this.emp;
            _this.employee_value = [];
            _this.options = {
                multiple: true
            };
            _this.employee_current = _this.employee_value;
        }, function (err) { return console.error(err); });
    };
    CertificateOfAttendanceListComponent.prototype.changedEmployee = function (data) {
        this.employee_current = data.value;
        if (this.employee_current == 0) {
            this.filterForm.value.employee_id = null;
        }
        else {
            this.filterForm.value.employee_id = this.employee_current;
        }
        this.data();
    };
    CertificateOfAttendanceListComponent.prototype.getStatus = function () {
        var _this = this;
        this._common_service.getStatusType().
            subscribe(function (data) {
            _this.sta = Array.from(data);
            _this.status = _this.sta;
            _this.status_value = [];
            _this.options = {
                multiple: true
            };
            _this.status_current = _this.status_value;
        }, function (err) { return console.error(err); });
    };
    CertificateOfAttendanceListComponent.prototype.changedStatus = function (data) {
        this.status_current = data.value;
        if (this.status_current == 0) {
            this.filterForm.value.status_id = null;
        }
        else {
            this.filterForm.value.status_id = this.status_current;
        }
        this.data();
    };
    CertificateOfAttendanceListComponent.prototype.getCompany = function () {
        var _this = this;
        this._common_service.getCompany()
            .subscribe(function (data) {
            _this.company = Array.from(data);
            _this.company_value = [];
            _this.options = {
                multiple: true
            };
            _this.company_current = _this.company_value;
            var id = 0;
            var text = 'Select Company';
            _this.company.unshift({ id: id, text: text });
            _this.company_value = _this.value;
        }, function (err) { return console.error(err); });
    };
    CertificateOfAttendanceListComponent.prototype.changedCompany = function (data) {
        this.company_current = data.value;
        if (this.company_current == 0) {
            this.filterForm.value.company_id = null;
        }
        else {
            this.filterForm.value.company_id = this.company_current;
        }
        this.data();
    };
    CertificateOfAttendanceListComponent.prototype.getType = function () {
        var _this = this;
        this._common_service.getCOAType().
            subscribe(function (data) {
            _this.type = Array.from(data);
            _this.type_value = [];
            _this.options = {
                multiple: true
            };
            _this.type_current = _this.type_value;
        }, function (err) { return console.error(err); });
    };
    CertificateOfAttendanceListComponent.prototype.changedType = function (data) {
        this.type_current = data.value;
        if (this.type_current == 0) {
            this.filterForm.value.type_id = null;
        }
        else {
            this.filterForm.value.type_id = this.type_current;
        }
        this.data();
    };
    CertificateOfAttendanceListComponent.prototype.choiceFilter = function (id) {
        if (id == 1) {
            this.getStatus();
            this.getType();
            this.getCompany();
            this.getEmployee();
            this.byStatus = true;
            this.byDate = true;
            this.byCompany = true;
            this.byEmployee = true;
            this.byType = true;
        }
        else if (id == 2) {
            if (this.byStatus == false) {
                this.getStatus();
                this.byStatus = true;
            }
            else {
                this.byStatus = false;
                this.filterForm.value.status_id = null;
            }
        }
        else if (id == 3) {
            if (this.byDate == false) {
                this.byDate = true;
            }
            else {
                this.byDate = false;
                this.filterForm.value.start_date = null;
                this.filterForm.value.end_date = null;
            }
        }
        else if (id == 4) {
            if (this.byCompany == false) {
                this.getCompany();
                this.byCompany = true;
            }
            else {
                this.byCompany = false;
                this.filterForm.value.company_id = null;
            }
        }
        else if (id == 5) {
            if (this.byEmployee == false) {
                this.getEmployee();
                this.byEmployee = true;
            }
            else {
                this.byEmployee = false;
                this.filterForm.value.employee_id = null;
            }
        }
        else if (id == 6) {
            if (this.byType == false) {
                this.getType();
                this.byType = true;
            }
            else {
                this.byType = false;
                this.filterForm.value.type_id = null;
            }
        }
        else {
            this.byStatus = false;
            this.byDate = false;
            this.byCompany = false;
            this.byEmployee = false;
            this.byType = false;
            this.filterForm.value.status_id = null;
            this.filterForm.value.company_id = null;
            this.filterForm.value.start_date = null;
            this.filterForm.value.end_date = null;
            this.filterForm.value.employee_id = null;
            this.filterForm.value.type_id = null;
            this.data();
        }
    };
    CertificateOfAttendanceListComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.data();
    };
    CertificateOfAttendanceListComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    CertificateOfAttendanceListComponent.prototype.createCOA = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(CertificateOfAttendanceModalComponent, {
            title: 'Create Certificate of Attendance',
            button: 'Add',
            create: true,
            multi: true,
            admin: true,
            emp: this.emp
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    CertificateOfAttendanceListComponent.prototype.approvedCOA = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Approved Certificate of Attendance',
            message: 'Are you sure you want to Approved this Certificate of Attendance?',
            action: 'Approved',
            id: id,
            url: 'certificate_of_attendance',
            request: true
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    CertificateOfAttendanceListComponent.prototype.rejectedCOA = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(RemarksModalComponent, {
            title: 'Reject Certificate of Attendance',
            id: id,
            url: 'certificate_of_attendance',
            button: 'Reject'
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    CertificateOfAttendanceListComponent.prototype.editCOA = function (id, emp_id) {
        var _this = this;
        var v = [];
        v.push(emp_id);
        var employee = v;
        var disposable = this.modalService.addDialog(CertificateOfAttendanceModalComponent, {
            title: 'Edit Certificate of Attendance',
            button: 'Update',
            edit: true,
            coa_id: id,
            single: true,
            admin: true,
            emp: this.emp,
            employee_id: employee
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    CertificateOfAttendanceListComponent.prototype.viewDetails = function (id, status) {
        var _this = this;
        var buttons = true;
        if (status == 'REJECTED' || status == 'APPROVED' || status == 'CANCELLED') {
            buttons = false;
        }
        var disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title: 'Certificate of Attendance',
            id: id,
            url: 'certificate_of_attendance',
            type_and_time: true,
            buttons: buttons,
            created_by: this.user_id,
            messenger_id: this.employee_id
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    CertificateOfAttendanceListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    CertificateOfAttendanceListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    CertificateOfAttendanceListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], CertificateOfAttendanceListComponent.prototype, "dtElement", void 0);
    CertificateOfAttendanceListComponent = __decorate([
        Component({
            selector: 'app-certificate-of-attendance-list',
            templateUrl: './certificate-of-attendance-list.component.html',
            styleUrls: ['./certificate-of-attendance-list.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            CertificateOfAttendanceService,
            CommonService,
            DaterangepickerConfig,
            FormBuilder,
            AuthUserService])
    ], CertificateOfAttendanceListComponent);
    return CertificateOfAttendanceListComponent;
}());
export { CertificateOfAttendanceListComponent };
//# sourceMappingURL=certificate-of-attendance-list.component.js.map