import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { CertificateOfAttendanceModalComponent } from '../../certificate-of-attendance/certificate-of-attendance-modal/certificate-of-attendance-modal.component';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { CertificateOfAttendanceService } from '../../../services/certificate-of-attendance.service';
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
import { AuthUserService } from '../../../services/auth-user.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActionCenterModalComponent } from '../../action-center/action-center-modal/action-center-modal.component';
import { Select2OptionData } from 'ng2-select2';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-my-certificate-of-attendance-list',
  templateUrl: './my-certificate-of-attendance-list.component.html',
  styleUrls: ['./my-certificate-of-attendance-list.component.css']
})
export class MyCertificateOfAttendanceListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};
	coa_id:any;
	coa=[];
	user_id:any;
	employee_id:any;
	supervisor_id:any;
	filterForm:any;
	public options: Select2Options;

	public mainInput = {
        start: moment().subtract(12, 'month'),
        end: moment().subtract(11, 'month')
    }

	public status : Array<Select2OptionData>;
	public status_value: Array<Select2OptionData>;
	public status_current: any;
	sta:any;

	public type : any;
	public type_value: Array<Select2OptionData>;
	public type_current: any;

	buttonVal = false;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

  constructor(
  	private modalService: DialogService,
  	private _conf: Configuration,
	private _auth_service: AuthUserService,
	private _coaservice: CertificateOfAttendanceService,
	private _fb: FormBuilder,
	private _common_service: CommonService,
   	private daterangepickerOptions: DaterangepickerConfig
  ) {

  	this.filterForm = _fb.group({
    	'status_id': 		[null],
		'start_date': 		[null],
		'end_date': 		[null],
		'type_id': 			[null]
   	});

  	this.body.classList.add("skin-blue");
	this.body.classList.add("sidebar-mini");


  }

  	ngOnInit() {
  		this.dateOption();
		this.getListByIdLimited();
		this.getStatus();
		this.getType();
		this.getData();
	}

	getListByIdLimited(){
		this._auth_service.getListByIdLimited().
		subscribe(
			data => {
				let user = data;
				this.employee_id = user.employee_id;
				this.buttonVal = true;
			},
			err => console.error(err)
		);
	}

	dateOption(){
    	this.daterangepickerOptions.settings = {
        	singleDatePicker: false
		};
    }

	getData(){

		let model = this.filterForm.value;

		this._coaservice.getMyCOA(model).
		subscribe(
			data => {
				this.coa  = data;
				this.rerender();
			},
			err => console.error(err)
		);
	}

	createCOA(){

		let disposable = this.modalService.addDialog(CertificateOfAttendanceModalComponent, {
            title:'Create Certificate of Attendance',
            button:'Add',
            create:true,
            employee_id:this.employee_id,
            user:true
        	}).subscribe((isConfirmed)=>{
                this.getData();
                this.dateOption();
        });

	}

	editCOA(id:any){

		let disposable = this.modalService.addDialog(CertificateOfAttendanceModalComponent, {
            title:'Update Certificate of Attendance',
            button:'Update',
            edit:true,
            coa_id:id,
            user:true,
            employee_id:this.employee_id
        	}).subscribe((isConfirmed)=>{
                this.getData();
                this.dateOption();
        });

	}

	cancelCOA(id:any){

        let disposable = this.modalService.addDialog(RemarksModalComponent, {
			title:'Cancel COA',
			id:id,
			url:'certificate_of_attendance',
			button:'Ok'
		}).subscribe((isConfirmed)=>{
			this.getData();
			this.dateOption();

		});

	}

	viewDetails(id:any) {

		let disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title:'Certificate of Attendance',
            id:id,
            url:'certificate_of_attendance',
            type_and_time:true,
            user:true
        	}).subscribe((isConfirmed)=>{
                this.getData();
                this.dateOption();

            });
	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    getStatus(){
		this._common_service.getStatusType().
		subscribe(
			data => {
				this.sta = Array.from(data);
				this.status = this.sta;
				this.status_value = [];
				this.options = {
					multiple: true
				}
				this.status_current = this.status_value;
			},
			err => console.error(err)
		);
	}
	changedStatus(data: any) {
		this.status_current = data.value;

		if(this.status_current == 0){
			this.filterForm.value.status_id = null;
			
		}
		else{
			this.filterForm.value.status_id = this.status_current;
		}

		this.getData();
	}

	getType(){
		this._common_service.getCOAType().
		subscribe(
			data => {
				this.type = Array.from(data); 
				this.type_value = [];
				this.options = {
					multiple: true
				}
				this.type_current = this.type_value;
			},
			err => console.error(err)
		);
	}
	changedType(data: any) {
		this.type_current = data.value;

		if(this.type_current == 0){
			this.filterForm.value.type_id = null;
		}
		else{
			this.filterForm.value.type_id = this.type_current;
		}
			this.getData();
	}

	filterDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;

        let start_date = moment(dateInput.start).format("YYYY-MM-DD");
        let end_date = moment(dateInput.end).format("YYYY-MM-DD");

        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;

        this.getData();

    }

}
