import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';//1
import { CompanyService } from '../../services/company.service';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Company } from '../../model/company';
import { Configuration } from '../../app.config';
import { PermissionService } from '../../services/permission.service';
import { Observable } from 'rxjs/Observable';
import { DataTableDirective } from 'angular-datatables';//2
import { Subject } from 'rxjs/Rx';


@Component({
  selector: 'app-dynasty-loan-list',
  templateUrl: './dynasty-loan-list.component.html',
  styleUrls: ['./dynasty-loan-list.component.css']
})

@Injectable()
export class DynastyLoanListComponent implements OnInit, AfterViewInit {
	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	public id: string;
	public company_id: any;
	public error_title: string;
	public api_employee: string;
	public error_message: string;
	public success_title;
	public success_message;
	public poststore: any;
	public company = new Company();
	companyForm: FormGroup;

	// TEMP variables
	public confirm_archiving: any;
	public form


	private headers: Headers;
	dtOptions: any = {};
	company_rec: any;
	// get permissions list
    perm_rec: any;
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	public _conf: any;

  	constructor(private _perms_service: PermissionService, 
  				private _ar: ActivatedRoute, 
  				private _company_service: CompanyService, 
  				private _rt: Router, 
  				private _fb: FormBuilder,
  				_conf: Configuration){
  		this._conf = _conf;

		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	    this.api_employee = this._conf.ServerWithApiUrl + 'admin_dynasty_loan/';

	      // Header Variables
		this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');

        console.log(authToken);
	}

	ngAfterViewInit(){
		this.dtTrigger.next();
	}

	showMe(companys: any){
		this.company_rec = companys;
	}

	ngOnInit() {
		let authToken = localStorage.getItem('id_token');
		let app_config = this._conf;

		// DataTable Configuration
		this.dtOptions = {
			bFilter: true,
			searching: true,
			ajax: {
				url: this.api_employee,
				type: "GET",
				beforeSend: function(xhr){
                            xhr.setRequestHeader("Authorization",
                            "Bearer " + authToken);
            	},
	        },
			columns: [
			{
				title: 'Employee Num.',
				data: 'employee_code'
			}, {
				title: 'Employee Name',
				data: 'employee_fullname'
			}, {
				title: 'Company',
				data: 'company_name'
			}, {
				title: 'Status',
				data: 'status',
				render: function(data, type, row, meta){
					return data == 1 ? 'Active' : 'Inactive';
				}
			}],
			rowCallback: (nRow: number, data: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
				let self = this;
				// Unbind first in order to avoid any duplicate handler
				// (see https://github.com/l-lin/angular-datatables/issues/87)
				$('td', nRow).unbind('click');
				$('td', nRow).bind('click', () => {
					this._rt.navigate(['admin/dynasty-loan-edit/', data.id]);
				});
				return nRow;
			}
		};
	}

	ngOnDestroy() {
	    //remove the the body classes
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	 }
}
