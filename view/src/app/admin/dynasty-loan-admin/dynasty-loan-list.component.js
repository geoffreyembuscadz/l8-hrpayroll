var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core'; //1
import { CompanyService } from '../../services/company.service';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Company } from '../../model/company';
import { Configuration } from '../../app.config';
import { PermissionService } from '../../services/permission.service';
import { DataTableDirective } from 'angular-datatables'; //2
import { Subject } from 'rxjs/Rx';
var DynastyLoanListComponent = /** @class */ (function () {
    function DynastyLoanListComponent(_perms_service, _ar, _company_service, _rt, _fb, _conf) {
        this._perms_service = _perms_service;
        this._ar = _ar;
        this._company_service = _company_service;
        this._rt = _rt;
        this._fb = _fb;
        this.dtTrigger = new Subject();
        this.company = new Company();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this._conf = _conf;
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api_employee = this._conf.ServerWithApiUrl + 'admin_dynasty_loan/';
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        console.log(authToken);
    }
    DynastyLoanListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    DynastyLoanListComponent.prototype.showMe = function (companys) {
        this.company_rec = companys;
    };
    DynastyLoanListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var authToken = localStorage.getItem('id_token');
        var app_config = this._conf;
        // DataTable Configuration
        this.dtOptions = {
            bFilter: true,
            searching: true,
            ajax: {
                url: this.api_employee,
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + authToken);
                },
            },
            columns: [
                {
                    title: 'Employee Num.',
                    data: 'employee_code'
                }, {
                    title: 'Employee Name',
                    data: 'employee_fullname'
                }, {
                    title: 'Company',
                    data: 'company_name'
                }, {
                    title: 'Status',
                    data: 'status',
                    render: function (data, type, row, meta) {
                        return data == 1 ? 'Active' : 'Inactive';
                    }
                }
            ],
            rowCallback: function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
                var self = _this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', function () {
                    _this._rt.navigate(['admin/dynasty-loan-edit/', data.id]);
                });
                return nRow;
            }
        };
    };
    DynastyLoanListComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], DynastyLoanListComponent.prototype, "dtElement", void 0);
    DynastyLoanListComponent = __decorate([
        Component({
            selector: 'app-dynasty-loan-list',
            templateUrl: './dynasty-loan-list.component.html',
            styleUrls: ['./dynasty-loan-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [PermissionService,
            ActivatedRoute,
            CompanyService,
            Router,
            FormBuilder,
            Configuration])
    ], DynastyLoanListComponent);
    return DynastyLoanListComponent;
}());
export { DynastyLoanListComponent };
//# sourceMappingURL=dynasty-loan-list.component.js.map