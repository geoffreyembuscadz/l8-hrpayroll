var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core'; //1
import { CompanyService } from '../../services/company.service';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Company } from '../../model/company';
import { Employee } from '../../model/employee';
import { EmployeeDynastyLoan } from '../../model/employee_dynasty_loan';
import { Configuration } from '../../app.config';
import { EmployeeService } from '../../services/employee.service';
import { PermissionService } from '../../services/permission.service';
import { DynastyLoanService } from '../../services/dynastyloan.service';
import { DataTableDirective } from 'angular-datatables'; //2
import { Subject } from 'rxjs/Rx';
var DynastyLoanEditComponent = /** @class */ (function () {
    function DynastyLoanEditComponent(_perms_service, _ar, _empservice, _company_service, _dynasty_loan_service, _rt, _fb, _conf) {
        var _this = this;
        this._perms_service = _perms_service;
        this._ar = _ar;
        this._empservice = _empservice;
        this._company_service = _company_service;
        this._dynasty_loan_service = _dynasty_loan_service;
        this._rt = _rt;
        this._fb = _fb;
        this.dtTrigger = new Subject();
        this.employee_dynasty_loan_data = new EmployeeDynastyLoan();
        this.employee_data = new Employee();
        this.company = new Company();
        this.employee = new Employee();
        this.form_user_image = '';
        this.form_job_titles = [];
        this.form_residence_select = [{ name: '' }, { name: 'Rented' }, { name: 'Owned' }, { name: 'Others' }];
        this.form_default_user_placeholder = '';
        this.form_employee_number_of_dependents = 0;
        this.user_thumbnail_employee = 'assets/img/user-placeholder.png';
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this._conf = _conf;
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api_employee = this._conf.ServerWithApiUrl + 'employee/';
        this.form_genders = this._empservice.getGenders();
        this.form_marital_statuses = this._empservice.getEmployeeMaritalStatuses();
        this.form_type_of_residence = this._empservice.getResidencyType();
        this.updateEmployeeDynastyForm = _fb.group({
            'company_id': [this.employee_dynasty_loan_data.company_id, [Validators.required]],
            'employee_fullname': [this.employee_dynasty_loan_data.employee_fullname, [Validators.required]],
            'employee_resident_type': [this.employee_dynasty_loan_data.employee_resident_type, [Validators.required]],
            'payback_mode': [this.employee_dynasty_loan_data.payback_mode, [Validators.required]],
            'term_in_months': [this.employee_dynasty_loan_data.term_in_months, [Validators.required]],
            'type_of_loan': [this.employee_dynasty_loan_data.type_of_loan, [Validators.required]],
            'purpose_of_loan': [this.employee_dynasty_loan_data.purpose_of_loan, [Validators.required]],
            'amount_applied': [this.employee_dynasty_loan_data.amount_applied, [Validators.required]]
        });
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.getCompanies();
        this.getJobTitles();
        this.setPaybackModes();
        this.setTypesOfLoan();
        this.id = this._ar.snapshot.params['dynasty_loan_id'];
        this.dynasty_loan_id = this._ar.snapshot.params['dynasty_loan_id'];
        this._dynasty_loan_service.getDynastyLoanRecord(this.dynasty_loan_id).subscribe(function (data) {
            _this.setDynastyLoanRecord(data);
        }, function (err) { return console.error(err); });
        // this._empservice.getEmployee(this.employee_id).subscribe(
        // 	data => {
        // 		this.setEmployee(data);
        // 	},
        // 	err => console.error(err)
        // );
    }
    DynastyLoanEditComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    DynastyLoanEditComponent.prototype.showMe = function (companys) {
        this.company_rec = companys;
    };
    DynastyLoanEditComponent.prototype.ngOnInit = function () {
        var authToken = localStorage.getItem('id_token');
        var app_config = this._conf;
    };
    // public setEmployee(emp: any){
    // 	this.id = emp.id;
    // 	this.employee = emp;
    // 	this.employee.skills = emp.skills.split(',');
    // 	this.employee.supervisor_id = JSON.parse(emp.supervisor_id);
    // 	let working_schedules = [];
    // 	let fetch_employee_working_schedules = emp.working_schedule;
    // 	this.employee.working_schedule = fetch_employee_working_schedules;
    // 	if(this.employee.employee_image != null || this.employee.employee_image != ''){
    // 		this.form_user_image = this.employee.employee_image;
    // 		this.form_default_user_placeholder = this.employee.employee_image;
    // 	} else if(this.employee.employee_image == ''){
    // 		this.form_user_image = this._conf.frontEndUrl + this.user_thumbnail_employee;
    // 		this.form_default_user_placeholder = this._conf.frontEndUrl + this.user_thumbnail_employee;
    // 	}
    // 	let employee_emergency_contacts = this.employee.emergency_contacts;
    // 	for(let _index_employee_emergency_contacts = 0; _index_employee_emergency_contacts < employee_emergency_contacts.length; _index_employee_emergency_contacts++){
    // 		this.updateEmergencyContacts.addControl('name[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].person_name);
    // 		this.updateEmergencyContacts.addControl('relationship[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].relationship);
    // 		this.updateEmergencyContacts.addControl('contact_number[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].contact_number);
    // 	}
    // 	if(this.employee.primary_company != null){
    // 		this._company_service.getCompany(this.employee.primary_company).subscribe(
    //             data => {
    //             },
    //             err => console.error(err)
    //         );
    // 	}
    // 	if(this.form_user_image == '0'){
    // 		this.form_user_image = this._conf.frontEndUrl + this.user_thumbnail_employee;
    // 	}
    // 	this.employee.ip_address = emp.user_time_setting.ip_address;
    // 	this.employee.mac_address = emp.user_time_setting.mac_address;
    // 	this.employee.ddns = emp.user_time_setting.ddns;
    // 	this.form_employee_number_of_dependents = this.employee.employee_dependents.length;
    // 	console.log(this.employee); // HAkhAkhAk!
    // }
    DynastyLoanEditComponent.prototype.setCompanyBranches = function (data) {
        var company_branches = [];
        var index = 0;
        for (var _a = 0, data_1 = data; _a < data_1.length; _a++) {
            var company_branch = data_1[_a];
            company_branches[index] = company_branch;
            index++;
        }
        this.form_company_branches = company_branches;
    };
    DynastyLoanEditComponent.prototype.setPaybackModes = function () {
        this.form_payback_modes = this._dynasty_loan_service.getPaybackModes();
    };
    DynastyLoanEditComponent.prototype.setTypesOfLoan = function () {
        this.form_types_of_loan = this._dynasty_loan_service.getTypesOfLoan();
        console.log(this.form_types_of_loan);
    };
    DynastyLoanEditComponent.prototype.getCompanies = function () {
        var _this = this;
        this._empservice.getCompanies().subscribe(function (data) {
            _this.setCompanies(data);
        }, function (err) { return _this.catchError(err); });
    };
    DynastyLoanEditComponent.prototype.setCompanies = function (data) {
        var companies = data;
        var fetched_companies = [];
        for (var _i = 0; _i < companies.length; _i++) {
            fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
        }
        this.form_companies = fetched_companies;
    };
    DynastyLoanEditComponent.prototype.getJobTitles = function () {
        var _this = this;
        this._empservice.getJobTitles().subscribe(function (data) {
            _this.fetchJobTitles(data);
        }, function (err) { return _this.catchError(err); });
    };
    DynastyLoanEditComponent.prototype.setDynastyLoanRecord = function (data) {
        var _this = this;
        this.employee_dynasty_loan_data = data;
        console.log(this.employee_dynasty_loan_data);
        this._empservice.getEmployee(data.employee_id).subscribe(function (data) {
            _this.setEmployee(data);
        }, function (err) { return console.error(err); });
    };
    DynastyLoanEditComponent.prototype.setEmployee = function (data) {
        this.employee_data = data;
    };
    DynastyLoanEditComponent.prototype.fetchJobTitles = function (data) {
        var job_titles = [];
        data.forEach(function (values, index) {
            job_titles[index] = { id: values.id, name: values.name };
        });
        this.form_job_titles = job_titles;
    };
    DynastyLoanEditComponent.prototype.validateUpdate = function () {
        var _this = this;
        this.success_title = "Success!";
        this.success_message = "An employee record was successfully updated.";
        this.error_title = null;
        this.error_message = null;
        var employee_dynamic_model = []; //this.updateEmployeeDynastyForm.value;
        employee_dynamic_model = this.updateEmployeeDynastyForm.value;
        console.log(employee_dynamic_model);
        console.log(123123);
        this._dynasty_loan_service.updateDynastyLoan(this.id, employee_dynamic_model).subscribe(function (data) {
            _this.success_title = "Success!";
            _this.success_message = "An employee record was successfully updated.";
            _this.error_title = null;
            _this.error_message = null;
        });
        // let employee_model_keys = Object.keys(employee_dynamic_model);
        // let error_fields = {};
        // let required_fields = [ 'firstname', 'middlename', 'lastname', 'income_rate', 'working_schedule', 'employment_status' ];
        // for(let index = 0; index < employee_model_keys.length; index++){
        // 	if( required_fields.indexOf(employee_model_keys[index]) > -1 && employee_dynamic_model[employee_model_keys[index]] == ''){
        // 		error_fields[employee_model_keys[index]] = "Income Rate field is required.";
        // 	}
        // }
        // if(Object.keys(error_fields).length == 0){
        // 	this._empservice.updateEmployee(this.id, employee_dynamic_model).subscribe(
        // 		data => {
        // 			this.poststore = Array.from(data);
        // 			this.success_title = "Success!";
        // 			this.success_message = "An employee record was successfully updated.";
        // 			this.error_title = null;
        // 			this.error_message = null;
        // 		},
        // 		err => function(err){
        // 			this.error_title = 'Error';
        // 			this.error_message = err;
        // 		}
        // 	);
        // } else {
        // 	let error_message = Object.keys(error_fields).join();
        // 	error_message = error_message.replace(/_/g,' ');
        // 	error_message = error_message.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        // 	this.error_title = 'Oops!';
        // 	this.error_message = 'Seems like there\'s a field you missed. Please check the following fields: ' + error_message;
        // }
    };
    DynastyLoanEditComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    DynastyLoanEditComponent.prototype.catchError = function (error) {
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], DynastyLoanEditComponent.prototype, "dtElement", void 0);
    DynastyLoanEditComponent = __decorate([
        Component({
            selector: 'app-dynasty-loan-edit',
            templateUrl: './dynasty-loan-edit.component.html',
            styleUrls: ['./dynasty-loan-edit.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [PermissionService,
            ActivatedRoute,
            EmployeeService,
            CompanyService,
            DynastyLoanService,
            Router,
            FormBuilder,
            Configuration])
    ], DynastyLoanEditComponent);
    return DynastyLoanEditComponent;
}());
export { DynastyLoanEditComponent };
//# sourceMappingURL=dynasty-loan-edit.component.js.map