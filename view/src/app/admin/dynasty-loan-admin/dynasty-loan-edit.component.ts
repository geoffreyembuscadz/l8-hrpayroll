import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';//1
import { CompanyService } from '../../services/company.service';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Company } from '../../model/company';
import { Employee } from '../../model/employee';
import { EmployeeDynastyLoan } from '../../model/employee_dynasty_loan';
import { Configuration } from '../../app.config';
import { EmployeeService } from '../../services/employee.service';
import { PermissionService } from '../../services/permission.service';
import { DynastyLoanService } from '../../services/dynastyloan.service';
import { Observable } from 'rxjs/Observable';
import { DataTableDirective } from 'angular-datatables';//2
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-dynasty-loan-edit',
  templateUrl: './dynasty-loan-edit.component.html',
  styleUrls: ['./dynasty-loan-edit.component.css']
})

@Injectable()
export class DynastyLoanEditComponent implements OnInit, AfterViewInit {
	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	public id: string;
  	public company_id: any;
  	public employee_id: string;
	public error_title: string;
	public api_employee: string;
	public error_message: string;
	public dynasty_loan_id: string;
	public employee_dynasty_loan_data = new EmployeeDynastyLoan();
	public employee_data = new Employee();
	public success_title;
	public success_message;
	public poststore: any;
	public company = new Company();
	public employee = new Employee();
	companyForm: FormGroup;

	// TEMP variables
	public form_genders: any;
	public form_companies: any;
	public form_user_image = '';
	public form_job_titles = [];
	public confirm_archiving: any;
	public form_residence_select:any = [{ name: '' }, { name: 'Rented' }, { name: 'Owned'}, {name: 'Others'}];
	public form_marital_statuses: any;
	public form_payback_modes: any;
	public form_types_of_loan: any;
	public form_company_branches: any;
	public form_type_of_residence: any;
	public form_default_user_placeholder = '';
	public form_employee_number_of_dependents = 0;
	private user_thumbnail_employee = 'assets/img/user-placeholder.png';

	updateEmployeeDynastyForm : FormGroup;

	private headers: Headers;
	dtOptions: any = {};
	company_rec: any;
	// get permissions list
    perm_rec: any;
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	public _conf: any;

  	constructor(private _perms_service: PermissionService, 
  				private _ar: ActivatedRoute,
  				private _empservice: EmployeeService, 
  				private _company_service: CompanyService, 
  				private _dynasty_loan_service: DynastyLoanService,
  				private _rt: Router, 
  				private _fb: FormBuilder,
  				_conf: Configuration){
  		this._conf = _conf;

		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	    this.api_employee = this._conf.ServerWithApiUrl + 'employee/';
	    this.form_genders = this._empservice.getGenders();
	    this.form_marital_statuses = this._empservice.getEmployeeMaritalStatuses();
	    this.form_type_of_residence = this._empservice.getResidencyType();

	    this.updateEmployeeDynastyForm = _fb.group({
	    	'company_id': [this.employee_dynasty_loan_data.company_id, [Validators.required]],
	    	'employee_fullname': [this.employee_dynasty_loan_data.employee_fullname, [Validators.required]],
	    	'employee_resident_type': [this.employee_dynasty_loan_data.employee_resident_type, [Validators.required]],
	    	'payback_mode': [this.employee_dynasty_loan_data.payback_mode, [Validators.required]],
	    	'term_in_months': [this.employee_dynasty_loan_data.term_in_months, [Validators.required]],
	    	'type_of_loan': [this.employee_dynasty_loan_data.type_of_loan, [Validators.required]],
	    	'purpose_of_loan': [this.employee_dynasty_loan_data.purpose_of_loan, [Validators.required]],
	    	'amount_applied': [this.employee_dynasty_loan_data.amount_applied, [Validators.required]]
	    });

	    // Header Variables
		this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');

        this.getCompanies();
        this.getJobTitles();
        this.setPaybackModes();
		this.setTypesOfLoan();

        this.id = this._ar.snapshot.params['dynasty_loan_id'];
        this.dynasty_loan_id = this._ar.snapshot.params['dynasty_loan_id'];

        this._dynasty_loan_service.getDynastyLoanRecord(this.dynasty_loan_id).subscribe(
        	data => {
        		this.setDynastyLoanRecord(data);
        	},
        	err => console.error(err)
        );
		// this._empservice.getEmployee(this.employee_id).subscribe(
		// 	data => {
		// 		this.setEmployee(data);
		// 	},
		// 	err => console.error(err)
		// );
	}

	ngAfterViewInit(){
		this.dtTrigger.next();
	}

	showMe(companys: any){
		this.company_rec = companys;
	}

	ngOnInit() {
		let authToken = localStorage.getItem('id_token');
		let app_config = this._conf;
		
	}

	// public setEmployee(emp: any){

	// 	this.id = emp.id;
	// 	this.employee = emp;
	// 	this.employee.skills = emp.skills.split(',');
	// 	this.employee.supervisor_id = JSON.parse(emp.supervisor_id);

	// 	let working_schedules = [];
	// 	let fetch_employee_working_schedules = emp.working_schedule;
		
	// 	this.employee.working_schedule = fetch_employee_working_schedules;

	// 	if(this.employee.employee_image != null || this.employee.employee_image != ''){
	// 		this.form_user_image = this.employee.employee_image;
	// 		this.form_default_user_placeholder = this.employee.employee_image;
	// 	} else if(this.employee.employee_image == ''){
	// 		this.form_user_image = this._conf.frontEndUrl + this.user_thumbnail_employee;
	// 		this.form_default_user_placeholder = this._conf.frontEndUrl + this.user_thumbnail_employee;
	// 	}

	// 	let employee_emergency_contacts = this.employee.emergency_contacts;

	// 	for(let _index_employee_emergency_contacts = 0; _index_employee_emergency_contacts < employee_emergency_contacts.length; _index_employee_emergency_contacts++){
	// 		this.updateEmergencyContacts.addControl('name[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].person_name);
	// 		this.updateEmergencyContacts.addControl('relationship[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].relationship);
	// 		this.updateEmergencyContacts.addControl('contact_number[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].contact_number);
	// 	}

	// 	if(this.employee.primary_company != null){
	// 		this._company_service.getCompany(this.employee.primary_company).subscribe(
	//             data => {
	//             },
	//             err => console.error(err)
	//         );
	// 	}

	// 	if(this.form_user_image == '0'){
	// 		this.form_user_image = this._conf.frontEndUrl + this.user_thumbnail_employee;
	// 	}

	// 	this.employee.ip_address = emp.user_time_setting.ip_address;
	// 	this.employee.mac_address = emp.user_time_setting.mac_address;
	// 	this.employee.ddns = emp.user_time_setting.ddns;

	// 	this.form_employee_number_of_dependents = this.employee.employee_dependents.length;

	// 	console.log(this.employee); // HAkhAkhAk!
	// }

	setCompanyBranches(data: any){
		let company_branches = [];
		let index = 0;

		for(let company_branch of data){
			company_branches[index] = company_branch;
			index++;
		}

		this.form_company_branches = company_branches;
	}

	setPaybackModes(){
		this.form_payback_modes = this._dynasty_loan_service.getPaybackModes();
	}

	setTypesOfLoan(){
		this.form_types_of_loan = this._dynasty_loan_service.getTypesOfLoan();
		console.log(this.form_types_of_loan);
	}

	private getCompanies(){
		this._empservice.getCompanies().subscribe(
			data => {
				this.setCompanies(data);
			},
			err => this.catchError(err)
		);
	}

	private setCompanies(data: any){
		let companies = data;
		let fetched_companies = [];

		for(var _i = 0; _i < companies.length; _i++){
			fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
		}

		this.form_companies = fetched_companies;
	}

	private getJobTitles(){
		this._empservice.getJobTitles().subscribe(
			data => {
				this.fetchJobTitles(data);
			},
			err => this.catchError(err)
		);
	}

	private setDynastyLoanRecord(data: any){
		this.employee_dynasty_loan_data = data;
		console.log(this.employee_dynasty_loan_data);

		this._empservice.getEmployee(data.employee_id).subscribe(
			data => {
				this.setEmployee(data);
			},
			err => console.error(err)
		);
	}

	private setEmployee(data: any){
		this.employee_data = data;
	}

	private fetchJobTitles(data: any){
		let job_titles = [];
		data.forEach((values, index) => {
			job_titles[index] = { id: values.id, name: values.name };
		});

		this.form_job_titles = job_titles;
	}

	private validateUpdate(): void {
		this.success_title = "Success!";
		this.success_message = "An employee record was successfully updated.";
		this.error_title = null;
		this.error_message = null;

		let employee_dynamic_model = []; //this.updateEmployeeDynastyForm.value;
		employee_dynamic_model = this.updateEmployeeDynastyForm.value;
		console.log(employee_dynamic_model);
		console.log(123123);

		this._dynasty_loan_service.updateDynastyLoan(this.id, employee_dynamic_model).subscribe(
			data => {
				this.success_title = "Success!";
				this.success_message = "An employee record was successfully updated.";

				this.error_title = null;
				this.error_message = null;
			}
		);

		// let employee_model_keys = Object.keys(employee_dynamic_model);
		// let error_fields = {};
		// let required_fields = [ 'firstname', 'middlename', 'lastname', 'income_rate', 'working_schedule', 'employment_status' ];

		// for(let index = 0; index < employee_model_keys.length; index++){
		// 	if( required_fields.indexOf(employee_model_keys[index]) > -1 && employee_dynamic_model[employee_model_keys[index]] == ''){
		// 		error_fields[employee_model_keys[index]] = "Income Rate field is required.";
		// 	}
		// }
	
		// if(Object.keys(error_fields).length == 0){
		// 	this._empservice.updateEmployee(this.id, employee_dynamic_model).subscribe(
		// 		data => {
		// 			this.poststore = Array.from(data);
		// 			this.success_title = "Success!";
		// 			this.success_message = "An employee record was successfully updated.";

		// 			this.error_title = null;
		// 			this.error_message = null;

		// 		},
		// 		err => function(err){
		// 			this.error_title = 'Error';
		// 			this.error_message = err;
		// 		}
		// 	);
		// } else {
		// 	let error_message = Object.keys(error_fields).join();
		// 	error_message = error_message.replace(/_/g,' ');
		// 	error_message = error_message.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});

		// 	this.error_title = 'Oops!';
		// 	this.error_message = 'Seems like there\'s a field you missed. Please check the following fields: ' + error_message;
		// }

	}

	ngOnDestroy() {
	    //remove the the body classes
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	private catchError(error: any){
	}
}
