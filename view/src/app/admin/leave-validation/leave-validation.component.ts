import { Component } from '@angular/core';
import { Configuration } from '../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { LeaveService } from '../../services/leave.service';
import { ValidationModalComponent } from '../validation-modal/validation-modal.component';

@Component({
  selector: 'app-leave-validation',
  templateUrl: './leave-validation.component.html',
  styleUrls: ['./leave-validation.component.css']
})
export class LeaveValidationComponent extends DialogComponent<null, boolean> {

	data=[];
	public success_title;
	public success_message;
	public error_title;
	public error_message;
	public poststore: any;
	predata=[];
	user:any;
    adminDup:any;
    edit:any;
    emp:any;

    socket: SocketIOClient.Socket;
    host:any;

  constructor(
   private modalService: DialogService,
   private _fb: FormBuilder, 
   private _ar: ActivatedRoute,
   private _common_service: CommonService,
   private _leaveservice: LeaveService,
   private _conf: Configuration
   ){
    super(modalService);
    this.host = this._conf.socketUrl;

   }

  ngOnInit() {

  	//made for computation of leave credits
  	if(this.data.length == 0){
		this.result = true;
		this.close();
	}
	//socket.io
	// this.connectToServer();

  }

  action(id,i,paid){

  	let leave = [];
  	let len = this.predata.length;
  	for (let x = 0; x < len; x++) {
  		if(this.predata[x]['emp_id'] == id){
  			this.predata[x]['total_with_pay'] = paid;
  			let leng = this.predata[x]['details'].length;
  			for (let i = 0; i < leng - paid; ++i) {
  				this.predata[x]['details'][i]['with_pay'] = 0 ;
  			}

  			leave = this.predata[x];
  		}
  	}

  	if(leave.length == 0){

  		leave = this.predata;


  		this._leaveservice
		.leaveUpdate(leave)
		.subscribe(
			data => {
				this.poststore = data;
				this.errorCatch(i);
			},
			err => this.catchError(err)
		);
  	}
  	else{
	  	this._leaveservice
		.leaveCreate(leave)
		.subscribe(
			data => {
				this.poststore = data;
				//emit in socket.io
				// this.socket.emit('notify', this.poststore);
				this.errorCatch(i);
			},
			err => this.catchError(err)
		);
  	}


  }

 	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

  errorCatch(i){
		if(this.poststore.status_code == 200){
			this.data.splice(i,1);
			this.success_title = 'Success';
			this.success_message = 'No Changes Happen';
			setTimeout(() => {
				this.ngOnInit();
	  			this.close();
	      	}, 2000);
		}
		else if(this.poststore.status_code == 400){

			$('html,body').scroll();

			this.error_title = 'Warning';
			this.error_message = 'Request is already exist';

			setTimeout(() => {
				let disposable = this.dialogService.addDialog(ValidationModalComponent, {
				title:'Warning',
				message:'Leave',
				url:'leave',
				duplicate:true,
				data:this.poststore.data,
				userDup:this.user,
				adminDup:this.adminDup,
				user:this.user,
            	emp:this.emp
				}).subscribe((message)=>{
					if(message == true){
						this.close();
					}
				});
	      	}, 2000);

		}
		else if(this.poststore.status_code == 422){

			$('html,body').scroll();

			this.error_title = 'Warning';
			this.error_message = 'Lack of credits'; 

			setTimeout(() => {
				let disposable = this.dialogService.addDialog(LeaveValidationComponent, {
				title:'Warning',
				message:'Leave',
				url:'leave',
				user:this.user,
				data:this.poststore.data,
				predata:this.poststore.predata,
            	emp:this.emp,
				adminDup:this.adminDup,
				edit:this.poststore.updateData
				}).subscribe((message)=>{
					if(message == true){
						this.close();
					}
				});
	      	}, 2000);

		}
		else{

			this.data.splice(i,1);
			this.success_title = "Success";
			this.success_message = "Leave request was successful.";
			
			setTimeout(() => {
	  		   	this.success_title = "";
				this.success_message = "";
	      		this.ngOnInit();
	      	}, 2000);

		}
	}

	cancel(){
		this.close();
		this.result = false;
	}

	submit(){

		let leave = this.predata[0];
		let i = 0;

		if(leave == undefined){

	  		leave = this.predata;

		  	this._leaveservice
			.leaveUpdate(leave)
			.subscribe(
				data => {
					this.poststore = data;
					this.errorCatch(i);
				},
				err => this.catchError(err)
			);
	  	}
	  	else{

	  		this._leaveservice
			.leaveCreate(leave)
			.subscribe(
				data => {
					this.poststore = data;
					this.socket.emit('notify', this.poststore);
					this.errorCatch(i);
				},
				err => this.catchError(err)
			);
	  	}
	}

	connectToServer(){
	    let socketUrl = this.host;
	    this.socket = io.connect(socketUrl);
	    this.socket.on("connect", () => this.connect());
	    this.socket.on("disconnect", () => this.disconnect());
	    
	    this.socket.on("error", (error: string) => {
	      console.log(`ERROR: "${error}" (${socketUrl})`);
	    });

	   
	  }

	// Handle connection opening
	private connect() {
	    // Request initial list when connected
	    this.socket.emit("join", 'Client User -> ');
	}
	// Handle connection closing
	private disconnect() {
	    console.log(`Disconnected from "${this.host}"`);
	}

}
