var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Configuration } from '../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { LeaveService } from '../../services/leave.service';
import { ValidationModalComponent } from '../validation-modal/validation-modal.component';
var LeaveValidationComponent = /** @class */ (function (_super) {
    __extends(LeaveValidationComponent, _super);
    function LeaveValidationComponent(modalService, _fb, _ar, _common_service, _leaveservice, _conf) {
        var _this = _super.call(this, modalService) || this;
        _this.modalService = modalService;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._common_service = _common_service;
        _this._leaveservice = _leaveservice;
        _this._conf = _conf;
        _this.data = [];
        _this.predata = [];
        _this.host = _this._conf.socketUrl;
        return _this;
    }
    LeaveValidationComponent_1 = LeaveValidationComponent;
    LeaveValidationComponent.prototype.ngOnInit = function () {
        //made for computation of leave credits
        if (this.data.length == 0) {
            this.result = true;
            this.close();
        }
        //socket.io
        // this.connectToServer();
    };
    LeaveValidationComponent.prototype.action = function (id, i, paid) {
        var _this = this;
        var leave = [];
        var len = this.predata.length;
        for (var x = 0; x < len; x++) {
            if (this.predata[x]['emp_id'] == id) {
                this.predata[x]['total_with_pay'] = paid;
                var leng = this.predata[x]['details'].length;
                for (var i_1 = 0; i_1 < leng - paid; ++i_1) {
                    this.predata[x]['details'][i_1]['with_pay'] = 0;
                }
                leave = this.predata[x];
            }
        }
        if (leave.length == 0) {
            leave = this.predata;
            this._leaveservice
                .leaveUpdate(leave)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.errorCatch(i);
            }, function (err) { return _this.catchError(err); });
        }
        else {
            this._leaveservice
                .leaveCreate(leave)
                .subscribe(function (data) {
                _this.poststore = data;
                //emit in socket.io
                // this.socket.emit('notify', this.poststore);
                _this.errorCatch(i);
            }, function (err) { return _this.catchError(err); });
        }
    };
    LeaveValidationComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    LeaveValidationComponent.prototype.errorCatch = function (i) {
        var _this = this;
        if (this.poststore.status_code == 200) {
            this.data.splice(i, 1);
            this.success_title = 'Success';
            this.success_message = 'No Changes Happen';
            setTimeout(function () {
                _this.ngOnInit();
                _this.close();
            }, 2000);
        }
        else if (this.poststore.status_code == 400) {
            $('html,body').scroll();
            this.error_title = 'Warning';
            this.error_message = 'Request is already exist';
            setTimeout(function () {
                var disposable = _this.dialogService.addDialog(ValidationModalComponent, {
                    title: 'Warning',
                    message: 'Leave',
                    url: 'leave',
                    duplicate: true,
                    data: _this.poststore.data,
                    userDup: _this.user,
                    adminDup: _this.adminDup,
                    user: _this.user,
                    emp: _this.emp
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.close();
                    }
                });
            }, 2000);
        }
        else if (this.poststore.status_code == 422) {
            $('html,body').scroll();
            this.error_title = 'Warning';
            this.error_message = 'Lack of credits';
            setTimeout(function () {
                var disposable = _this.dialogService.addDialog(LeaveValidationComponent_1, {
                    title: 'Warning',
                    message: 'Leave',
                    url: 'leave',
                    user: _this.user,
                    data: _this.poststore.data,
                    predata: _this.poststore.predata,
                    emp: _this.emp,
                    adminDup: _this.adminDup,
                    edit: _this.poststore.updateData
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.close();
                    }
                });
            }, 2000);
        }
        else {
            this.data.splice(i, 1);
            this.success_title = "Success";
            this.success_message = "Leave request was successful.";
            setTimeout(function () {
                _this.success_title = "";
                _this.success_message = "";
                _this.ngOnInit();
            }, 2000);
        }
    };
    LeaveValidationComponent.prototype.cancel = function () {
        this.close();
        this.result = false;
    };
    LeaveValidationComponent.prototype.submit = function () {
        var _this = this;
        var leave = this.predata[0];
        var i = 0;
        if (leave == undefined) {
            leave = this.predata;
            this._leaveservice
                .leaveUpdate(leave)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.errorCatch(i);
            }, function (err) { return _this.catchError(err); });
        }
        else {
            this._leaveservice
                .leaveCreate(leave)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.socket.emit('notify', _this.poststore);
                _this.errorCatch(i);
            }, function (err) { return _this.catchError(err); });
        }
    };
    LeaveValidationComponent.prototype.connectToServer = function () {
        var _this = this;
        var socketUrl = this.host;
        this.socket = io.connect(socketUrl);
        this.socket.on("connect", function () { return _this.connect(); });
        this.socket.on("disconnect", function () { return _this.disconnect(); });
        this.socket.on("error", function (error) {
            console.log("ERROR: \"" + error + "\" (" + socketUrl + ")");
        });
    };
    // Handle connection opening
    LeaveValidationComponent.prototype.connect = function () {
        // Request initial list when connected
        this.socket.emit("join", 'Client User -> ');
    };
    // Handle connection closing
    LeaveValidationComponent.prototype.disconnect = function () {
        console.log("Disconnected from \"" + this.host + "\"");
    };
    var LeaveValidationComponent_1;
    LeaveValidationComponent = LeaveValidationComponent_1 = __decorate([
        Component({
            selector: 'app-leave-validation',
            templateUrl: './leave-validation.component.html',
            styleUrls: ['./leave-validation.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            CommonService,
            LeaveService,
            Configuration])
    ], LeaveValidationComponent);
    return LeaveValidationComponent;
}(DialogComponent));
export { LeaveValidationComponent };
//# sourceMappingURL=leave-validation.component.js.map