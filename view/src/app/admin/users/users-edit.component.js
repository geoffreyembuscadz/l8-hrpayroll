var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Users } from '../../model/users';
import { UsersService } from '../../services/users.service';
import { RoleService } from '../../services/role.service';
import { AuthUserService } from '../../services/auth-user.service';
import { CompanyService } from '../../services/company.service';
import { matchingPasswords, emailValidator } from '../../validators/validators';
var UsersEditComponent = /** @class */ (function (_super) {
    __extends(UsersEditComponent, _super);
    function UsersEditComponent(_rt, _user_service, dialogService, _fb, _company_serv, _ar, _role_service, _auth_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._user_service = _user_service;
        _this._fb = _fb;
        _this._company_serv = _company_serv;
        _this._ar = _ar;
        _this._role_service = _role_service;
        _this._auth_service = _auth_service;
        _this.users = new Users();
        _this.checked = false;
        _this.selectedCompany = "";
        _this.id = _this._user_service.getId();
        _this._user_service.getUser(_this.id).subscribe(function (data) {
            _this.setUser(data);
            _this.role_names = data.role_id;
            _this.ngOnInit();
        }, function (err) { return console.error(err); });
        _this.updateUserForm = _fb.group({
            'name': ['', Validators.required],
            'email': ['', [
                    Validators.required,
                    emailValidator
                ]],
            'role_id': [null],
            'passwords': _fb.group({
                'password': [null, Validators.compose([Validators.minLength(5), Validators.maxLength(16)])],
                'confirm_password': [null]
            }, { validator: matchingPasswords('password', 'confirm_password') }),
            'checked': [''],
            'created_by': [''],
            'company': [null]
        });
        return _this;
    }
    UsersEditComponent.prototype.isChecked = function () {
        this.checked = !this.checked; // this will change value of it true and false 
    };
    // update
    UsersEditComponent.prototype.validateUpdate = function () {
        // this.updateUserForm.patchValue({
        //   'role_id': this.current, 
        // });
        var _this = this;
        this.updateUserForm.patchValue({
            'created_by': this.user_id
        });
        this.updateUserForm.value.company = this.selectedCompany;
        var users_model = this.updateUserForm.value;
        this.updateUserForm.patchValue({
            role_id: this.value
        });
        this._user_service.updateUser(this.id, users_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The user was successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/user-list']);
            _this.clearErrorMsg();
        }, function (err) { return _this.catchError(err); });
    };
    // delete
    UsersEditComponent.prototype.archiveUser = function (event) {
        var _this = this;
        event.preventDefault();
        var user_id = this.id;
        console.log('prearchive', user_id);
        this._user_service.archiveUser(user_id).subscribe(function (data) {
            alert('Record is successfully archived.');
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigateByUrl('admin/user-list');
        }, function (err) { return console.error(err); });
    };
    UsersEditComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error';
            this.error_message = 'Email address already exists.';
            window.scrollTo(0, 0);
        }
    };
    UsersEditComponent.prototype.clearErrorMsg = function () {
        this.error_title = '';
        this.error_message = '';
    };
    UsersEditComponent.prototype.setUser = function (users) {
        this.id = users.id;
        this.users = users;
    };
    UsersEditComponent.prototype.isBlank = function (control) {
        if (control.value === '') {
            return { blank: true };
        }
    };
    UsersEditComponent.prototype.changed = function (data) {
        this.current = data.value;
    };
    UsersEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        var users_role = this._role_service.getRoles()
            .subscribe(function (data) {
            _this.roles_rec = Array.from(data); // fetched record
            _this.RoleData = _this.roles_rec;
            if (_this.role_names == null) {
                _this.value = [];
            }
            else {
                var role = _this.role_names.split(",");
                _this.value = role;
            }
            _this.options = {
                multiple: true
            };
            _this.current = _this.value;
        }, function (err) { return console.error(err); });
        var created_by = this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
        }, function (err) { return console.error(err); });
        this.getCompanyList();
    };
    UsersEditComponent.prototype.getCompanyList = function () {
        var _this = this;
        var companies = this._company_serv.getCompanys()
            .subscribe(function (data) {
            _this.company_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
    };
    UsersEditComponent.prototype.getCompany = function (value, event) {
        this.selectedCompany = value;
    };
    UsersEditComponent = __decorate([
        Component({
            selector: 'admin-users-edit',
            templateUrl: './users-edit.component.html'
        }),
        __metadata("design:paramtypes", [Router, UsersService, DialogService,
            FormBuilder, CompanyService, ActivatedRoute, RoleService, AuthUserService])
    ], UsersEditComponent);
    return UsersEditComponent;
}(DialogComponent));
export { UsersEditComponent };
//# sourceMappingURL=users-edit.component.js.map