import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Configuration } from './../../app.config';

import { UsersCreateModalComponent } from './users-create-modal.component';
import { UsersEditComponent } from './users-edit.component';

import { UsersService } from '../../services/users.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
 

@Component({
  selector: 'admin-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})

@Injectable()
export class UsersListComponent implements OnInit, AfterViewInit  {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	public id: string;
	public users_id: any;
	public success_title: string;

	public error_title: string;
    public error_message: string;

	dtOptions: any = {};

	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	public reload: Boolean;

  	user_rec: any;


	constructor(
		private _conf: Configuration, 
		private modalService: DialogService, 
		private _user_service: UsersService, 
		private _rt: Router,
		private _ar: ActivatedRoute, 
		){
		//add the the body classes
	    this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	}

	ngOnInit() {

		let user = this._user_service.getUsers().
			subscribe(
				data => {
					 this.user_rec = Array.from(data);
            		 this.rerender();
            		 console.log(data);
				},
				err => console.error(err)
			);
	}	

	clickRow(id) {
	    this._user_service.setId(id);
	    this.OpenEditModal();
	}


	private catchError(error: any){
	    let response_body = error._body;
	    let response_status = error.status;

	    if( response_status == 500 ){
	      this.error_title = 'Error 500';
	      this.error_message = 'Something went wrong';
	    } else if( response_status == 200 ) {
	      this.error_title = '';
	      this.error_message = '';
	    }
	}

	OpenAddModal() {
		let disposable = this.modalService.addDialog(UsersCreateModalComponent, {
            title:'Add Users'
        	}).subscribe((isConfirmed)=>{
        		let user = this._user_service.getUsers().
				subscribe(
				data => {
					 this.user_rec = Array.from(data);
            		 this.rerender();

				},
				err => console.error(err)
			);
        });
	}


	OpenEditModal() {
		let disposable = this.modalService.addDialog(UsersEditComponent, {
            title:'Update Users'
        	}).subscribe((isConfirmed)=>{
        		let user = this._user_service.getUsers().
				subscribe(
				data => {
					 this.user_rec = Array.from(data);
            		 this.rerender();

				},
				err => console.error(err)
			);
                
        });
	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	 }

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }



	
}