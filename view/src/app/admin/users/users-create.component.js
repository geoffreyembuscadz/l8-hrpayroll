var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Users } from '../../model/users';
import { RoleService } from '../../services/role.service';
import { UsersService } from '../../services/users.service';
import { matchingPasswords, emailValidator } from '../../validators/validators';
var UsersCreateComponent = /** @class */ (function () {
    function UsersCreateComponent(_role_service, _fb, _users_service, router) {
        this._role_service = _role_service;
        this._fb = _fb;
        this._users_service = _users_service;
        this.router = router;
        this.users = new Users();
        this.checked = false;
        this.error = false;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.collapse = document.getElementsByClassName('collapse');
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.createUsersForm = _fb.group({
            'name': ['', Validators.required],
            'email': ['', [
                    Validators.required,
                    emailValidator
                ]],
            'passwords': _fb.group({
                'password': ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(16)])],
                'confirm_password': ['']
            }, { validator: matchingPasswords('password', 'confirm_password') }),
            'role_id': [null],
        });
        $(document).ready(function () {
            //Initialize tooltips
            // $('.nav-tabs > li a[title]').tooltip();
            $(".next-step").click(function (e) {
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
            });
            $(".prev-step").click(function (e) {
                var $active = $('.wizard .nav-tabs li.active');
                prevTab($active);
            });
        });
        function nextTab(elem) {
            $(elem).next().find('a[data-toggle="tab"]').click();
        }
        function prevTab(elem) {
            $(elem).prev().find('a[data-toggle="tab"]').click();
        }
    }
    UsersCreateComponent.prototype.onSubmit = function () {
        var _this = this;
        this.createUsersForm.value.role_id = this.current;
        var users_model = this.createUsersForm.value;
        console.log(users_model);
        this._users_service
            .storeUser(users_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            _this.success_title = "Success";
            _this.success_message = "A new user record was successfully added.";
        }, function (err) { return _this.catchError(err); });
    };
    UsersCreateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error';
            this.error_message = 'Email address already exists.';
            window.scrollTo(0, 0);
        }
    };
    UsersCreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        var users_role = this._role_service.getRoles().
            subscribe(function (data) {
            _this.roles_rec = Array.from(data); // fetched record
            _this.RoleData = _this.roles_rec;
            _this.value = [];
            _this.options = {
                multiple: true
            };
            _this.current = _this.value;
        }, function (err) { return console.error(err); });
    };
    UsersCreateComponent.prototype.changed = function (data) {
        this.current = data.value;
    };
    UsersCreateComponent.prototype.isChecked = function () {
        this.checked = !this.checked; // this will change value of it true and false 
    };
    UsersCreateComponent = __decorate([
        Component({
            selector: 'admin-users-create',
            templateUrl: './users-create.component.html',
            styleUrls: ['./users-create.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [RoleService, FormBuilder, UsersService, Router])
    ], UsersCreateComponent);
    return UsersCreateComponent;
}());
export { UsersCreateComponent };
//# sourceMappingURL=users-create.component.js.map