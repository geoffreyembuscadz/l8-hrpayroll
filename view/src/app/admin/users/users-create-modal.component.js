var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Users } from '../../model/users';
import { RoleService } from '../../services/role.service';
import { UsersService } from '../../services/users.service';
import { AuthUserService } from '../../services/auth-user.service';
import { CompanyService } from '../../services/company.service';
import { matchingPasswords, emailValidator } from '../../validators/validators';
var UsersCreateModalComponent = /** @class */ (function (_super) {
    __extends(UsersCreateModalComponent, _super);
    function UsersCreateModalComponent(_auth_service, _company_serv, _rt, dialogService, _fb, _ar, _role_service, _users_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._auth_service = _auth_service;
        _this._company_serv = _company_serv;
        _this._rt = _rt;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._role_service = _role_service;
        _this._users_service = _users_service;
        _this.users = new Users();
        _this.selectedCompany = "";
        _this.createUsersForm = _fb.group({
            'name': ['', [Validators.required]],
            'email': ['', [
                    Validators.required,
                    emailValidator
                ]],
            'passwords': _fb.group({
                'password': ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(16)])],
                'confirm_password': ['']
            }, { validator: matchingPasswords('password', 'confirm_password') }),
            'role_id': [null, Validators.required],
            'created_by': [''],
            'company': [null]
        });
        return _this;
    }
    UsersCreateModalComponent.prototype.onSubmit = function () {
        // this.createUsersForm.patchValue({
        //   'role_id': this.current
        // });
        var _this = this;
        // patchValue update the field value from ts
        // this.user_id the logged in user
        this.createUsersForm.patchValue({
            'created_by': this.user_id
        });
        this.createUsersForm.value.company = this.selectedCompany;
        var users_model = this.createUsersForm.value;
        // let now = Date.now();
        this._users_service
            .storeUser(users_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            _this.success_title = "Success";
            _this.success_message = "A new user record was successfully added.";
            _this._users_service.setReload(true);
            _this.result = true;
            alert('A new user record was successfully added.');
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/user-list']);
        }, function (err) { return _this.catchError(err); });
    };
    UsersCreateModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error';
            this.error_message = 'Email address already exists.';
            window.scrollTo(0, 0);
        }
    };
    UsersCreateModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        var users_role = this._role_service.getRoles().
            subscribe(function (data) {
            _this.roles_rec = Array.from(data); // fetched record
            // this.RoleData = this.roles_rec;
            // this.value = [];
            //   this.options = {
            //     multiple: true
            //   }
            //   this.current = this.value;
        }, function (err) { return console.error(err); });
        this.getCompanyList();
        var created_by = this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
        }, function (err) { return console.error(err); });
    };
    UsersCreateModalComponent.prototype.changed = function (data) {
        this.current = data.value;
    };
    UsersCreateModalComponent.prototype.getCompanyList = function () {
        var _this = this;
        var companies = this._company_serv.getCompanys()
            .subscribe(function (data) {
            _this.company_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
    };
    UsersCreateModalComponent.prototype.getCompany = function (value, event) {
        this.selectedCompany = value;
    };
    UsersCreateModalComponent = __decorate([
        Component({
            selector: 'admin-users-create-modal',
            templateUrl: './users-create-modal.component.html'
        }),
        Injectable(),
        __metadata("design:paramtypes", [AuthUserService, CompanyService, Router, DialogService, FormBuilder, ActivatedRoute, RoleService, UsersService])
    ], UsersCreateModalComponent);
    return UsersCreateModalComponent;
}(DialogComponent));
export { UsersCreateModalComponent };
//# sourceMappingURL=users-create-modal.component.js.map