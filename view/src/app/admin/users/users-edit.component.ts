import { Component, Input } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';

import { Select2OptionData } from 'ng2-select2';

import { Users } from '../../model/users';
import { UsersService } from '../../services/users.service';
import { RoleService } from '../../services/role.service';
import { AuthUserService } from '../../services/auth-user.service';
import { CompanyService } from '../../services/company.service';

import { matchingPasswords, emailValidator } from '../../validators/validators';

@Component({
  selector: 'admin-users-edit',
  templateUrl: './users-edit.component.html'
})
export class UsersEditComponent extends DialogComponent<null, boolean> {
  roles_rec: any;
  title: string;
  message: string;

  public id: any;
  public user_id: any;
  public name: string;
  public display_name: string;
  public description: string;
  public error_title: string;
  public error_message: string;
  public success_title;
  public success_message;
  public poststore: any;
  public users = new Users();

  public RoleData: Array<Select2OptionData>;
  public value: string[];
  public current: any;
  public options: Select2Options;
  public user: any;
  public role_names: string;
  public checked: boolean= false;
  public company_rec: any;
  public selectedCompany: string = "";
  
  updateUserForm : FormGroup;

  constructor(private _rt: Router, private _user_service: UsersService, dialogService: DialogService, 
    private _fb: FormBuilder, private _company_serv: CompanyService, private _ar: ActivatedRoute, private _role_service: RoleService, private _auth_service: AuthUserService) {
    super(dialogService);
    
    this.id = this._user_service.getId();
 
    this._user_service.getUser(this.id).subscribe(
      data => {
        this.setUser(data);
        this.role_names = data.role_id;
        this.ngOnInit();
      },
      err => console.error(err)
    );

    this.updateUserForm = _fb.group({
      'name': ['', Validators.required],
      'email': ['', [
        Validators.required,
        emailValidator
      ]],
      'role_id' : [null],
      'passwords': _fb.group({
        'password': [null, Validators.compose([ Validators.minLength(5), Validators.maxLength(16)])],
        'confirm_password': [null]
      }, { validator: matchingPasswords('password', 'confirm_password')}),
      'checked' :[''],
      'created_by' :[''],
      'company' : [null]


    }); 
  }

  isChecked(){
    this.checked = !this.checked;// this will change value of it true and false 
  }
   
  // update
  public validateUpdate() {
    // this.updateUserForm.patchValue({
    //   'role_id': this.current, 
    // });

    this.updateUserForm.patchValue({
      'created_by': this.user_id
    });
    this.updateUserForm.value.company = this.selectedCompany;
    let users_model = this.updateUserForm.value;
     this.updateUserForm.patchValue({
      role_id: this.value
    });
    this._user_service.updateUser(this.id, users_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The user was successfully updated.";
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/user-list']);
        this.clearErrorMsg();

      },
      err => this.catchError(err)
    );
  }

  // delete
  archiveUser(event: Event){
    event.preventDefault();
    let user_id = this.id;
    console.log('prearchive', user_id);
    this._user_service.archiveUser(user_id).subscribe(
      data => {
        alert('Record is successfully archived.');
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigateByUrl('admin/user-list');
        
      },
      err => console.error(err)
    );

  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error';
      this.error_message = 'Email address already exists.';
      window.scrollTo(0, 0);
    } 
  }

  private clearErrorMsg(){
    this.error_title = '';
    this.error_message = '';
  }

  public setUser(users: any){
    this.id = users.id;
    this.users = users;
  }

  isBlank(control: FormControl): {[s: string]: boolean} {
    if(control.value === '') {
      return {blank: true}
    }

  }
  
  changed(data: any) {
      this.current = data.value;
  }

  ngOnInit() {
    let users_role = this._role_service.getRoles()
        .subscribe(
          data => {
            this.roles_rec = Array.from(data); // fetched record

            this.RoleData = this.roles_rec;
            if(this.role_names == null) {
              this.value = [];
            } else {
              let role = this.role_names.split(",");
              this.value = role;
            }
            
            this.options = {
              multiple: true
            }

            this.current = this.value;
          },
          err => console.error(err)
        );

    let created_by = this._auth_service.getUser().
        subscribe(
          data => {
            let user = data; 
            this.user_id = user.id;
          },
          err => console.error(err)
        );
    this.getCompanyList();
  }
  
  getCompanyList() {
    let companies = this._company_serv.getCompanys()
        .subscribe(
          data => {
            this.company_rec = Array.from(data);
          },
          err => this.catchError(err)
        );
  }

  getCompany(value, event) {
      this.selectedCompany = value;
   }

}