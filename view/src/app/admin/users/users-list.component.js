var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute } from '@angular/router';
import { Configuration } from './../../app.config';
import { UsersCreateModalComponent } from './users-create-modal.component';
import { UsersEditComponent } from './users-edit.component';
import { UsersService } from '../../services/users.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
var UsersListComponent = /** @class */ (function () {
    function UsersListComponent(_conf, modalService, _user_service, _rt, _ar) {
        this._conf = _conf;
        this.modalService = modalService;
        this._user_service = _user_service;
        this._rt = _rt;
        this._ar = _ar;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    UsersListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var user = this._user_service.getUsers().
            subscribe(function (data) {
            _this.user_rec = Array.from(data);
            _this.rerender();
            console.log(data);
        }, function (err) { return console.error(err); });
    };
    UsersListComponent.prototype.clickRow = function (id) {
        this._user_service.setId(id);
        this.OpenEditModal();
    };
    UsersListComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    UsersListComponent.prototype.OpenAddModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(UsersCreateModalComponent, {
            title: 'Add Users'
        }).subscribe(function (isConfirmed) {
            var user = _this._user_service.getUsers().
                subscribe(function (data) {
                _this.user_rec = Array.from(data);
                _this.rerender();
            }, function (err) { return console.error(err); });
        });
    };
    UsersListComponent.prototype.OpenEditModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(UsersEditComponent, {
            title: 'Update Users'
        }).subscribe(function (isConfirmed) {
            var user = _this._user_service.getUsers().
                subscribe(function (data) {
                _this.user_rec = Array.from(data);
                _this.rerender();
            }, function (err) { return console.error(err); });
        });
    };
    UsersListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    UsersListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    UsersListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], UsersListComponent.prototype, "dtElement", void 0);
    UsersListComponent = __decorate([
        Component({
            selector: 'admin-users-list',
            templateUrl: './users-list.component.html',
            styleUrls: ['./users-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration,
            DialogService,
            UsersService,
            Router,
            ActivatedRoute])
    ], UsersListComponent);
    return UsersListComponent;
}());
export { UsersListComponent };
//# sourceMappingURL=users-list.component.js.map