
import { Component, OnInit, Injectable, TemplateRef, Input } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Select2OptionData } from 'ng2-select2';
import { ViewChild } from '@angular/core';
import { Observable } from "rxjs/Rx";

import { Users } from '../../model/users';
import { RoleService } from '../../services/role.service';
import { UsersService } from '../../services/users.service';

import { matchingPasswords, emailValidator } from '../../validators/validators';


@Component({
  selector: 'admin-users-create',
  templateUrl: './users-create.component.html',
  styleUrls: ['./users-create.component.css']
})
@Injectable()
export class UsersCreateComponent implements OnInit {

	roles_rec: any;
	
	public error_title: string;
	public error_message: string;
	public poststore: any;
	public users = new Users();
	private success_title: string;
	private success_message: string;

	public value: string[];
	public current: any;
	public options: Select2Options;
	public RoleData: Array<Select2OptionData>;
	public RoleDatas: any;
	public listRoles: any;
	public checked: boolean= false;

 
	error = false;
	createUsersForm : FormGroup;
	
	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];
	collapse = document.getElementsByClassName('collapse');
	

    constructor(private _role_service: RoleService, private _fb: FormBuilder, private _users_service: UsersService, private router: Router) {
	  	//add the the body classes
	    this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");



	    this.createUsersForm = _fb.group({
	   
		    'name': ['', Validators.required],
		    'email': ['', [
		      Validators.required,
		      emailValidator
		    ]],
		   
		    'passwords': _fb.group({
		    	'password': ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(16)]) ],
			    'confirm_password': ['']
		    }, { validator: matchingPasswords('password', 'confirm_password')}),
		    'role_id' : [null],


		 });

	    $(document).ready(function () {
		    //Initialize tooltips
		    // $('.nav-tabs > li a[title]').tooltip();
		    $(".next-step").click(function (e) {
		        var $active = $('.wizard .nav-tabs li.active');
		        $active.next().removeClass('disabled');
		        nextTab($active);
		    });
		    $(".prev-step").click(function (e) {
		        var $active = $('.wizard .nav-tabs li.active');
		        prevTab($active);
		    });
		});

		function nextTab(elem) {
	      $(elem).next().find('a[data-toggle="tab"]').click();
		}
		function prevTab(elem) {
		    $(elem).prev().find('a[data-toggle="tab"]').click();
		}
    }

	onSubmit() {
		this.createUsersForm.value.role_id = this.current;
		let  users_model = this.createUsersForm.value;
		console.log(users_model);
		this._users_service
		.storeUser(users_model)
		.subscribe(
			data => {
				this.poststore = Array.from(data);
				this.success_title = "Success";
				this.success_message = "A new user record was successfully added.";
				
			},
			err => this.catchError(err)
		);
	}	


	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error';
			this.error_message = 'Email address already exists.';
			window.scrollTo(0, 0);
		} 
	}

    ngOnInit() {

	   let users_role = this._role_service.getRoles().

		subscribe(
			data => {
				this.roles_rec = Array.from(data); // fetched record
				this.RoleData = this.roles_rec;

				this.value = [];

			    this.options = {
			      multiple: true
			    }

			    this.current = this.value;
			},
			err => console.error(err)
		);


    }

    changed(data: any) {
	    this.current = data.value;
	}

	isChecked() {
	    this.checked = !this.checked;// this will change value of it true and false 
	}



}
