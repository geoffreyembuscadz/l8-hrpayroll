import { Component, Input, Injectable } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response } from '@angular/http';

import { Select2OptionData } from 'ng2-select2';

import { Users } from '../../model/users';
import { RoleService } from '../../services/role.service';
import { UsersService } from '../../services/users.service';
import { AuthUserService } from '../../services/auth-user.service';
import { CompanyService } from '../../services/company.service';

import { matchingPasswords, emailValidator } from '../../validators/validators';

@Component({
  selector: 'admin-users-create-modal',
  templateUrl: './users-create-modal.component.html'
})
@Injectable()
export class UsersCreateModalComponent extends DialogComponent<null, boolean> {

  roles_rec: any;
  title: string;
  message: string;
  //create users
  public error_title: string;
  public error_message: string;
  public poststore: any;
  private success_title: string;
  private success_message: string;
  public users = new Users();
  
  public id: any;
  public user_id: any;
  public name: string;
  public display_name: string;
  public description: string;
  public result: any;

  public RoleData: Array<Select2OptionData>;
  public value: string[];
  public current: any;
  public options: Select2Options;
  public company_rec: any;
  public selectedCompany: string = "";


  createUsersForm : FormGroup;

  constructor(private _auth_service: AuthUserService, private _company_serv: CompanyService, private _rt: Router, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute, private _role_service: RoleService, private _users_service: UsersService) {
    super(dialogService);

    this.createUsersForm = _fb.group({
   
      'name': ['', [Validators.required]],
      'email': ['', [
        Validators.required,
        emailValidator
      ]],
      'passwords': _fb.group({
        'password': ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(16)]) ],
        'confirm_password': ['']
      }, { validator: matchingPasswords('password', 'confirm_password')}),
      'role_id' : [null, Validators.required],
      'created_by' : [''],
      'company' : [null]

      

    }); 

  }

  onSubmit() {

    // this.createUsersForm.patchValue({
    //   'role_id': this.current
    // });

    // patchValue update the field value from ts
    // this.user_id the logged in user
    this.createUsersForm.patchValue({
      'created_by': this.user_id
    });
    this.createUsersForm.value.company = this.selectedCompany;
    
    let  users_model = this.createUsersForm.value;
    // let now = Date.now();

    this._users_service
    .storeUser(users_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data);
        this.success_title = "Success";
        this.success_message = "A new user record was successfully added.";
        this._users_service.setReload(true);
        this.result = true;
        alert('A new user record was successfully added.');
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/user-list']);
  
        
        
      },
      err => this.catchError(err)
    );
  }


  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error';
      this.error_message = 'Email address already exists.';
      window.scrollTo(0, 0);
    } 
  }

  ngOnInit() {
    let users_role = this._role_service.getRoles().

    subscribe(
      data => {
        this.roles_rec = Array.from(data); // fetched record
        // this.RoleData = this.roles_rec;

        // this.value = [];

        //   this.options = {
        //     multiple: true
        //   }

        //   this.current = this.value;
      },
      err => console.error(err)
    );
     this.getCompanyList();

    let created_by = this._auth_service.getUser().
        subscribe(
          data => {
            let user = data; 
            this.user_id = user.id;
          },
          err => console.error(err)
        );
  }

  changed(data: any) {
      this.current = data.value;
  }

  getCompanyList() {
    let companies = this._company_serv.getCompanys()
        .subscribe(
          data => {
            this.company_rec = Array.from(data);
          },
          err => this.catchError(err)
        );
  }

  getCompany(value, event) {
      this.selectedCompany = value;
   }

  
}