/* tslint:disable:no-unused-variable */
import { async, TestBed } from '@angular/core/testing';
import { ControlSidebarComponent } from './control-sidebar.component';
describe('ControlSidebarComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [ControlSidebarComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(ControlSidebarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=control-sidebar.component.spec.js.map