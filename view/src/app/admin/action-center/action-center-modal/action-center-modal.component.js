var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
var ActionCenterModalComponent = /** @class */ (function (_super) {
    __extends(ActionCenterModalComponent, _super);
    function ActionCenterModalComponent(modalService, _fb, _ar, _common_service) {
        var _this = _super.call(this, modalService) || this;
        _this.modalService = modalService;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._common_service = _common_service;
        _this.validate = true;
        _this.leaveData = [];
        _this.obData = [];
        _this.saData = [];
        _this.loader = true;
        _this.businesslogs = false;
        _this.dataAtt = [];
        _this.attVal = false;
        return _this;
    }
    ActionCenterModalComponent.prototype.ngOnInit = function () {
        if (this.businesslogs == false) {
            this.requestData();
        }
        else if (this.businesslogs == true) {
            if (this.dataAtt[0].type != null) {
                this.attVal = true;
            }
            this.loader = false;
        }
    };
    ActionCenterModalComponent.prototype.requestData = function () {
        var _this = this;
        var url = this.url;
        var id = this.id;
        this._common_service.getRequest(id, url)
            .subscribe(function (data) {
            var dta = data;
            if (_this.title == "Leave") {
                _this.name = dta[0].name;
                _this.startDate = moment(dta[0].date_from).format("MMMM D, YYYY");
                _this.endDate = moment(dta[0].date_to).format("MMMM D, YYYY");
                _this.type = dta[0].type;
                _this.totalDays = dta[0].total_days;
                _this.totalHours = dta[0].total_hours;
                _this.withPay = dta[0].total_with_pay;
                _this.withOutPay = Math.abs(dta[0].total_with_pay - dta[0].total_days);
                _this.reason = dta[0].reason;
                _this.final_remarks = dta[0].final_remarks;
                _this.status = dta[0].status;
                var len = dta.length;
                for (var x = 0; x < len; x++) {
                    var day = moment(dta[x].date).format("dddd");
                    var length_days = parseFloat(dta[x].length_days) + ' day';
                    var length_hours = parseFloat(dta[x].length_hours) + ' hrs';
                    var time_type = dta[x].time_type;
                    var date = dta[x].date;
                    _this.leaveData.push({ day: day, date: date, length_days: length_days, length_hours: length_hours, time_type: time_type });
                }
            }
            else if (_this.title == "Schedule Adjustment") {
                _this.name = dta[0].name;
                _this.startDate = moment(dta[0].start_date).format("MMMM D, YYYY");
                _this.endDate = moment(dta[0].end_date).format("MMMM D, YYYY");
                _this.type = dta[0].type;
                _this.reason = dta[0].reason;
                _this.final_remarks = dta[0].final_remarks;
                _this.status = dta[0].status;
                var len = dta.length;
                for (var x = 0; x < len; x++) {
                    var day = moment(dta[x].date).format("dddd");
                    var date = dta[x].date;
                    var start_time = moment(dta[x].start_time, ["h:mm:ss"]).format("h:mm A");
                    var end_time = moment(dta[x].end_time, ["h:mm:ss"]).format("h:mm A");
                    var break_start = moment(dta[x].break_start, ["h:mm:ss"]).format("h:mm A");
                    var break_end = moment(dta[x].break_end, ["h:mm:ss"]).format("h:mm A");
                    _this.saData.push({ day: day, date: date, start_time: start_time, end_time: end_time, break_start: break_start, break_end: break_end });
                }
            }
            else if (_this.title == "Official Business") {
                _this.name = dta[0].name;
                _this.startDate = dta[0].start_date;
                _this.endDate = dta[0].end_date;
                _this.reason = dta[0].reason;
                _this.final_remarks = dta[0].final_remarks;
                _this.status = dta[0].status;
                var len = dta.length;
                for (var x = 0; x < len; x++) {
                    var day = moment(dta[x].date).format("dddd");
                    var date = dta[x].date;
                    var start_time = moment(dta[x].start_time, ["h:mm:ss"]).format("h:mm A");
                    var end_time = moment(dta[x].end_time, ["h:mm:ss"]).format("h:mm A");
                    var location_1 = dta[x].location;
                    _this.obData.push({ day: day, date: date, start_time: start_time, end_time: end_time, location: location_1 });
                }
            }
            else {
                _this.name = dta.name;
                _this.date = moment(dta.date).format("MMMM D, YYYY");
                ;
                _this.type = dta.type;
                _this.time = moment(dta.time, ["h:mm:ss"]).format("h:mm A");
                _this.startTime = moment(dta.start_time, ["h:mm:ss"]).format("h:mm A");
                _this.endTime = moment(dta.end_time, ["h:mm:ss"]).format("h:mm A");
                _this.startDate = moment(dta.start_date).format("MMMM D, YYYY");
                _this.endDate = moment(dta.end_date).format("MMMM D, YYYY");
                _this.reason = dta.reason;
                _this.final_remarks = dta.final_remarks;
                _this.status = dta.status;
            }
            _this.loader = false;
        }, function (err) { return _this.catchError(err); });
    };
    ActionCenterModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    ActionCenterModalComponent.prototype.approved = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Approved ' + this.title,
            message: 'Are you sure you want to Approved this Undertime?',
            action: 'Approved',
            id: this.id,
            url: this.url,
            created_by: this.created_by,
            request: true,
            messenger_id: this.messenger_id
        }).subscribe(function (isConfirmed) {
            setTimeout(function () {
                _this.close();
            }, 1000);
        });
    };
    ActionCenterModalComponent.prototype.rejected = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(RemarksModalComponent, {
            title: 'Reject ' + this.title,
            id: this.id,
            url: this.url,
            button: 'Reject',
            created_by: this.created_by,
            messenger_id: this.messenger_id
        }).subscribe(function (isConfirmed) {
            setTimeout(function () {
                _this.close();
            }, 1000);
        });
    };
    ActionCenterModalComponent = __decorate([
        Component({
            selector: 'app-action-center-modal',
            templateUrl: './action-center-modal.component.html',
            styleUrls: ['./action-center-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            CommonService])
    ], ActionCenterModalComponent);
    return ActionCenterModalComponent;
}(DialogComponent));
export { ActionCenterModalComponent };
//# sourceMappingURL=action-center-modal.component.js.map