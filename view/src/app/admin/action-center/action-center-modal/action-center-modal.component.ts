import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';

@Component({
  selector: 'app-action-center-modal',
  templateUrl: './action-center-modal.component.html',
  styleUrls: ['./action-center-modal.component.css']
})
export class ActionCenterModalComponent extends DialogComponent<null, boolean> {
  
	title: string;
	public status: any;
	public success_title;
	public success_message;
	public error_title;
	public error_message;
	url:any;
	validate=true;

	id: any;
	name:any;
	startDate:any;
	endDate:any;
	startTime:any;
	endTime:any;
	type:any;
	date:any;
	reason:any;
	time:any;
	totalDays:any;
	totalHours:any;
	leaveData=[];
	obData=[];
	saData = [];
	final_remarks:any;
	created_by:any;
	messenger_id:any;
	loader = true;
	businesslogs = false;
	dataAtt = [];
	attVal = false;
	withPay:any;
	withOutPay:any;


 constructor( 
   private modalService: DialogService,
   private _fb: FormBuilder, 
   private _ar: ActivatedRoute,
   private _common_service: CommonService,
   ){
    super(modalService);
  
  }
 
	ngOnInit() {

		if (this.businesslogs == false) {
			this.requestData();
		}
		else if (this.businesslogs == true) {
			if (this.dataAtt[0].type != null) {
				this.attVal = true;
			}
			this.loader = false;
		}
	}	
	requestData() {

		let url = this.url;
	    let id = this.id;

	    this._common_service.getRequest(id, url)
	    .subscribe(
	      data => {
	        let dta = data;
	        
	        if(this.title == "Leave"){

	        	this.name = dta[0].name;
	        	this.startDate = moment(dta[0].date_from).format("MMMM D, YYYY");
	        	this.endDate = moment(dta[0].date_to).format("MMMM D, YYYY");
	        	this.type = dta[0].type;
	        	this.totalDays = dta[0].total_days;
	        	this.totalHours = dta[0].total_hours;
	        	this.withPay = dta[0].total_with_pay;
	        	this.withOutPay = Math.abs(dta[0].total_with_pay - dta[0].total_days);
	        	this.reason = dta[0].reason;
	        	this.final_remarks = dta[0].final_remarks;
	        	this.status = dta[0].status;

	        	let len = dta.length;
		        for (let x = 0; x < len; x++) {

					let day = moment(dta[x].date).format("dddd");
					let length_days = parseFloat(dta[x].length_days) + ' day';
					let length_hours = parseFloat(dta[x].length_hours)+ ' hrs';
					let time_type = dta[x].time_type;
					let date = dta[x].date;

					this.leaveData.push({day,date,length_days,length_hours,time_type});

		        }


	        }
	        else if(this.title == "Schedule Adjustment"){

	        	this.name = dta[0].name;
	        	this.startDate = moment(dta[0].start_date).format("MMMM D, YYYY");
	        	this.endDate =  moment(dta[0].end_date).format("MMMM D, YYYY");
	        	this.type = dta[0].type;
	        	this.reason = dta[0].reason;
	        	this.final_remarks = dta[0].final_remarks;
	        	this.status = dta[0].status;

	        	let len = dta.length;
		        for (let x = 0; x < len; x++) {

					let day = moment(dta[x].date).format("dddd");
					let date = dta[x].date;
					let start_time = moment(dta[x].start_time, ["h:mm:ss"]).format("h:mm A");
					let end_time =   moment(dta[x].end_time, ["h:mm:ss"]).format("h:mm A");
					let break_start = moment(dta[x].break_start, ["h:mm:ss"]).format("h:mm A");
					let break_end = moment(dta[x].break_end, ["h:mm:ss"]).format("h:mm A");

					this.saData.push({day,date,start_time,end_time,break_start,break_end});

		        }

	        }
	        else if(this.title == "Official Business"){

	        	this.name = dta[0].name;
	        	this.startDate = dta[0].start_date;
	        	this.endDate = dta[0].end_date;
	        	this.reason = dta[0].reason;
	        	this.final_remarks = dta[0].final_remarks;
	        	this.status = dta[0].status;

	        	let len = dta.length;
		        for (let x = 0; x < len; x++) {

					let day = moment(dta[x].date).format("dddd");
					let date = dta[x].date;
					let start_time = moment(dta[x].start_time, ["h:mm:ss"]).format("h:mm A");
					let end_time =   moment(dta[x].end_time, ["h:mm:ss"]).format("h:mm A");
					let location = dta[x].location;

		        	this.obData.push({day,date,start_time,end_time,location});

		        }

	        }
	        else{

		        this.name = dta.name;
		        this.date =  moment(dta.date).format("MMMM D, YYYY");;
		        this.type = dta.type;
		        this.time = moment(dta.time, ["h:mm:ss"]).format("h:mm A");
		        this.startTime = moment(dta.start_time, ["h:mm:ss"]).format("h:mm A");
				this.endTime =   moment(dta.end_time, ["h:mm:ss"]).format("h:mm A");
		        this.startDate = moment(dta.start_date).format("MMMM D, YYYY");
		        this.endDate =  moment(dta.end_date).format("MMMM D, YYYY");
		        this.reason = dta.reason;
		        this.final_remarks = dta.final_remarks;
		        this.status = dta.status;

	        }

	        this.loader = false;


	      },
	      err => this.catchError(err)
	    );
	}

	 private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	approved() {

		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Approved ' + this.title,
            message:'Are you sure you want to Approved this Undertime?',
            action:'Approved',
            id:this.id,
            url:this.url,
            created_by:this.created_by,
            request:true,
            messenger_id:this.messenger_id
        	}).subscribe((isConfirmed)=>{
                setTimeout(() => {
		  		   this.close();
		      	}, 1000);

            });
	}

	rejected() {

		let disposable = this.modalService.addDialog(RemarksModalComponent, {
            title:'Reject ' + this.title ,
            id:this.id,
            url:this.url,
            button:'Reject',
            created_by:this.created_by,
            messenger_id:this.messenger_id
        	}).subscribe((isConfirmed)=>{
                setTimeout(() => {
		  		   this.close();
		      	}, 1000);

            });
	}

}
