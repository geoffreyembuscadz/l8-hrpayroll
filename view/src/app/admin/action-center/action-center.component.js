var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { CommonService } from '../../services/common.service';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { RemarksModalComponent } from '../remarks-modal/remarks-modal.component';
import { FormBuilder } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { ActionCenterModalComponent } from '../action-center/action-center-modal/action-center-modal.component';
import { AuthUserService } from '../../services/auth-user.service';
var ActionCenterComponent = /** @class */ (function () {
    function ActionCenterComponent(modalService, _conf, _common_service, daterangepickerOptions, _fb, _auth_service) {
        this.modalService = modalService;
        this._conf = _conf;
        this._common_service = _common_service;
        this.daterangepickerOptions = daterangepickerOptions;
        this._fb = _fb;
        this._auth_service = _auth_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.request = [];
        this.byStatus = false;
        this.byDate = false;
        this.byCompany = false;
        this.byEmployee = false;
        this.byType = false;
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.type_value = ['1', '2', '3', '4', '5', '6'];
        this.type = [
            { id: 1, text: 'COA' },
            { id: 2, text: 'Leave' },
            { id: 3, text: 'Official Business' },
            { id: 4, text: 'Overtime' },
            { id: 5, text: 'SA' },
            { id: 6, text: 'Undertime' }
        ];
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.filterForm = _fb.group({
            'status_id': [null],
            'start_date': [null],
            'end_date': [null],
            'company_id': [null],
            'employee_id': [null],
            'type_id': [this.type_value],
        });
        this.dtOptions = {
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            order: [[4, 'desc']],
        };
    }
    ActionCenterComponent.prototype.ngOnInit = function () {
        this.data();
        this.dateOption();
    };
    ActionCenterComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    ActionCenterComponent.prototype.data = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._common_service.getAllRequest(model).
            subscribe(function (data) {
            _this.requestList = Array.from(data);
            _this.requestList.shift();
            _this.request = _this.requestList;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    ActionCenterComponent.prototype.filterStatus = function (event) {
        var type = event;
        if (type == 0) {
            this.data();
        }
        else {
            this.filterForm.value.status_id = type;
            this.data();
        }
    };
    ActionCenterComponent.prototype.getEmployee = function () {
        var _this = this;
        this._common_service.getEmployee().
            subscribe(function (data) {
            _this.emp = Array.from(data);
            _this.employee = _this.emp;
            _this.employee_value = [];
            _this.options = {
                multiple: true
            };
            _this.employee_current = _this.employee_value;
        }, function (err) { return console.error(err); });
    };
    ActionCenterComponent.prototype.changedEmployee = function (data) {
        this.employee_current = data.value;
        if (this.employee_current == 0) {
            this.filterForm.value.employee_id = null;
            this.data();
        }
        else {
            this.filterForm.value.employee_id = this.employee_current;
            this.data();
        }
    };
    ActionCenterComponent.prototype.getCompany = function () {
        var _this = this;
        this._common_service.getCompany()
            .subscribe(function (data) {
            _this.company = Array.from(data);
            _this.company_value = [];
            _this.options = {
                multiple: true
            };
            // this.company_value = ['1'];
            _this.company_current = _this.company_value;
            _this.filterForm.value.company_id = _this.company_current;
        }, function (err) { return console.error(err); });
    };
    ActionCenterComponent.prototype.changedCompany = function (data) {
        this.company_current = data.value;
        if (this.company_current == 0) {
            this.filterForm.value.company_id = null;
            this.data();
        }
        else {
            this.filterForm.value.company_id = this.company_current;
            this.data();
        }
    };
    ActionCenterComponent.prototype.getStatus = function () {
        var _this = this;
        this._common_service.getStatusType().
            subscribe(function (data) {
            _this.sta = Array.from(data);
            _this.status = _this.sta;
            _this.status_value = [];
            // this.status_value = ['1', '2','3','4'];
            _this.options = {
                multiple: true
            };
            _this.status_current = _this.status_value;
        }, function (err) { return console.error(err); });
    };
    ActionCenterComponent.prototype.changedStatus = function (data) {
        this.status_current = data.value;
        if (this.status_current == 0) {
            this.filterForm.value.status_id = null;
            this.data();
        }
        else {
            this.filterForm.value.status_id = this.status_current;
            this.data();
        }
    };
    ActionCenterComponent.prototype.getType = function () {
        this.options = {
            multiple: true
        };
        this.type_current = this.type_value;
    };
    ActionCenterComponent.prototype.changedType = function (data) {
        this.type_current = data.value;
        if (this.type_current == 0) {
            this.filterForm.value.type_id = null;
            this.data();
        }
        else {
            this.filterForm.value.type_id = this.type_current;
            this.data();
        }
    };
    ActionCenterComponent.prototype.choiceFilter = function (id) {
        if (id == 1) {
            this.getType();
            this.getCompany();
            this.getEmployee();
            this.getStatus();
            this.byStatus = true;
            this.byDate = true;
            this.byCompany = true;
            this.byEmployee = true;
            this.byType = true;
        }
        else if (id == 2) {
            if (this.byStatus == false) {
                this.getStatus();
                this.byStatus = true;
            }
            else {
                this.byStatus = false;
                this.filterForm.value.status_id = null;
            }
            // this.byDate=false;
            // this.byCompany=false;
        }
        else if (id == 3) {
            if (this.byDate == false) {
                this.byDate = true;
            }
            else {
                this.byDate = false;
                this.filterForm.value.start_date = null;
                this.filterForm.value.end_date = null;
            }
            // this.byStatus=false;
            // this.byCompany=false;
        }
        else if (id == 4) {
            if (this.byCompany == false) {
                this.getCompany();
                this.byCompany = true;
            }
            else {
                this.byCompany = false;
                this.filterForm.value.company_id = null;
            }
            // this.byDate=false;
            // this.byStatus=false;
        }
        else if (id == 5) {
            if (this.byEmployee == false) {
                this.getEmployee();
                this.byEmployee = true;
            }
            else {
                this.byEmployee = false;
                this.filterForm.value.employee_id = null;
            }
            // this.byDate=false;
            // this.byStatus=false;
        }
        else if (id == 6) {
            if (this.byType == false) {
                this.getType();
                this.byType = true;
            }
            else {
                this.byType = false;
                this.filterForm.value.type_id = null;
            }
            // this.byDate=false;
            // this.byStatus=false;
        }
        else {
            this.byStatus = false;
            this.byDate = false;
            this.byCompany = false;
            this.byEmployee = false;
            this.byType = false;
            this.filterForm.value.status_id = null;
            this.filterForm.value.company_id = null;
            this.filterForm.value.start_date = null;
            this.filterForm.value.end_date = null;
            this.filterForm.value.employee_id = null;
            this.data();
        }
    };
    ActionCenterComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.data();
    };
    ActionCenterComponent.prototype.approved = function (id, url, type) {
        var _this = this;
        var title = type;
        if (type == 'SA') {
            title = 'Schedule Adjustment';
        }
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Approved ' + title,
            message: 'Are you sure you want to Approved this Undertime?',
            action: 'Approved',
            id: id,
            url: url,
            request: true
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    ActionCenterComponent.prototype.rejected = function (id, url, type) {
        var _this = this;
        var title = type;
        if (type == 'SA') {
            title = 'Schedule Adjustment';
        }
        var disposable = this.modalService.addDialog(RemarksModalComponent, {
            title: 'Reject ' + title,
            id: id,
            url: url,
            button: 'Reject'
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    ActionCenterComponent.prototype.viewDetails = function (id, url, type, status) {
        var _this = this;
        var type_and_time = false;
        var date_range = false;
        var leave = false;
        var sa = false;
        var ob = false;
        var start_end_time = false;
        var types = false;
        if (type == 'COA') {
            type = 'Certificate of Attendance';
            type_and_time = true;
        }
        else if (type == 'Undertime' || type == 'Overtime') {
            start_end_time = true;
        }
        else if (type == 'Official Business') {
            date_range = true;
            ob = true;
        }
        else if (type == 'Leave') {
            date_range = true;
            leave = true;
            types = true;
        }
        else if (type == 'SA') {
            type = 'Schedule Adjustment';
            date_range = true;
            sa = true;
        }
        var buttons = true;
        if (status == 'REJECTED' || status == 'APPROVED' || status == 'CANCELLED') {
            buttons = false;
        }
        var disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title: type,
            id: id,
            url: url,
            type_and_time: type_and_time,
            start_end_time: start_end_time,
            date_range: date_range,
            leave: leave,
            sa: sa,
            ob: ob,
            types: types,
            buttons: buttons
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    ActionCenterComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    ActionCenterComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    ActionCenterComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], ActionCenterComponent.prototype, "dtElement", void 0);
    ActionCenterComponent = __decorate([
        Component({
            selector: 'app-action-center',
            templateUrl: './action-center.component.html',
            styleUrls: ['./action-center.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            CommonService,
            DaterangepickerConfig,
            FormBuilder,
            AuthUserService])
    ], ActionCenterComponent);
    return ActionCenterComponent;
}());
export { ActionCenterComponent };
//# sourceMappingURL=action-center.component.js.map