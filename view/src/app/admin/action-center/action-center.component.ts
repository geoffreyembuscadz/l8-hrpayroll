import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { CommonService } from '../../services/common.service';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { RemarksModalComponent } from '../remarks-modal/remarks-modal.component';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Select2OptionData } from 'ng2-select2';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { ActionCenterModalComponent } from '../action-center/action-center-modal/action-center-modal.component';
import { AuthUserService } from '../../services/auth-user.service';

@Component({
  selector: 'app-action-center',
  templateUrl: './action-center.component.html',
  styleUrls: ['./action-center.component.css']
})
export class ActionCenterComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

 	dtOptions: any = {};
	request=[];
	byStatus=false;
	byDate=false;
	byCompany=false;
	byEmployee=false;
	byType=false;
	public company: any;
	public company_current : any;
	company_value: string[];

	public mainInput = {
        start: moment().subtract(12, 'month'),
        end: moment().subtract(11, 'month')
    }
    filterForm:any;

    public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public employee_current: any;
	public options: Select2Options;
	emp:any;

	public status : Array<Select2OptionData>;
	public status_value: string[];
	public status_current: any;
	sta:any;

	public type_current: any;
	type_value = ['1','2','3','4','5','6'];
	type=[
			{id:1,text:'COA'},
			{id:2,text:'Leave'},
			{id:3,text:'Official Business'},
			{id:4,text:'Overtime'},
			{id:5,text:'SA'},
			{id:6,text:'Undertime'}
		];

	requestList:any;
	supervisor_id:any;
	employee_id:any;
	role_id:any;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _common_service: CommonService,
   		private daterangepickerOptions: DaterangepickerConfig,
   		private _fb: FormBuilder,
   		private _auth_service: AuthUserService
		){

		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	    this.filterForm = _fb.group({
    	'status_id': 		[null],
		'start_date': 		[null],
		'end_date': 		[null],
		'company_id': 		[null],
		'employee_id': 		[null],
		'type_id': 			[this.type_value],
    	}); 

    	this.dtOptions = {
	      lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
	      order: [[4, 'desc']],

	    };
	}
	ngOnInit() {
		this.data();
		this.dateOption();
	}

	dateOption(){
    	this.daterangepickerOptions.settings = {
        	singleDatePicker: false
		};
    }


	data(){

		let model = this.filterForm.value;

    	this._common_service.getAllRequest(model).
      	subscribe(
        data => {
          this.requestList = Array.from(data);
          this.requestList.shift();
          this.request = this.requestList;
          this.rerender();
        },
        err => console.error(err)
	    );

	}

	filterStatus(event) {
        let type = event;

        if(type == 0){
        	this.data();
        }
        else{
        	this.filterForm.value.status_id = type;
	        this.data();

        }
         

    }

    getEmployee(){

		this._common_service.getEmployee().
		subscribe(
			data => {
				this.emp = Array.from(data);
				this.employee = this.emp;
				this.employee_value = [];
				this.options = {
					multiple: true
				}
				this.employee_current = this.employee_value;
			},
			err => console.error(err)
		);
	}
	changedEmployee(data: any) {
	  this.employee_current = data.value;
	  if(this.employee_current == 0){
	  		this.filterForm.value.employee_id = null ;
			this.data();
		}
		else{
			this.filterForm.value.employee_id = this.employee_current;
		    this.data();

		}
	}

    getCompany(){
	      this._common_service.getCompany()
	      .subscribe(
	        data => {
	        this.company = Array.from(data);
	        this.company_value = [];
	        this.options = {
				multiple: true
			}
	        // this.company_value = ['1'];
	        this.company_current = this.company_value;
	        this.filterForm.value.company_id = this.company_current;
	        },
	        err => console.error(err)
	    );
	}
  	changedCompany(data: any) {

      	this.company_current = data.value;

		if(this.company_current == 0){
				this.filterForm.value.company_id = null;
				this.data();
		}
		else{
			this.filterForm.value.company_id = this.company_current;
		    this.data();

		}
      
    } 

    getStatus(){
		this._common_service.getStatusType().
		subscribe(
			data => {
				this.sta = Array.from(data);
				this.status = this.sta;
				this.status_value = [];
		        // this.status_value = ['1', '2','3','4'];
				this.options = {
					multiple: true
				}

				this.status_current = this.status_value;
			},
			err => console.error(err)
		);
	}
	changedStatus(data: any) {
	  this.status_current = data.value;
	  	if(this.status_current == 0){
	  			this.filterForm.value.status_id = null;
				this.data();
		}
		else{
			this.filterForm.value.status_id = this.status_current;
		    this.data();
		}
	}

	getType(){

		this.options = {
			multiple: true
		}
		this.type_current = this.type_value;
		
	}
	changedType(data: any) {
	  this.type_current = data.value;

	  	if(this.type_current == 0){
	  			this.filterForm.value.type_id = null;
				this.data();
		}
		else{
			this.filterForm.value.type_id = this.type_current;
		    this.data();
		}
	}

	choiceFilter(id){

		if(id==1){
			this.getType();
			this.getCompany();
			this.getEmployee();
			this.getStatus();
			this.byStatus=true;
			this.byDate=true;
			this.byCompany=true;
			this.byEmployee=true;
			this.byType=true;
		}
		else if(id==2){
			if (this.byStatus==false) { 
				this.getStatus();
				this.byStatus=true;
			} else {
				this.byStatus=false;
				this.filterForm.value.status_id = null;
			}
			// this.byDate=false;
			// this.byCompany=false;
		}
		else if(id==3){
			if (this.byDate==false) { 
				this.byDate=true;
			} else {
				this.byDate=false;
				this.filterForm.value.start_date = null;
        		this.filterForm.value.end_date = null;
			}
			// this.byStatus=false;
			// this.byCompany=false;
		}
		else if(id==4){
			if (this.byCompany==false) {
				this.getCompany();
				this.byCompany=true;
			} else {
				this.byCompany=false;
				this.filterForm.value.company_id = null;
			}
			// this.byDate=false;
			// this.byStatus=false;
		}
		else if(id==5){
			if (this.byEmployee==false) { 
				this.getEmployee();
				this.byEmployee=true;
			} else {
				this.byEmployee=false;
				this.filterForm.value.employee_id = null;
			}
			// this.byDate=false;
			// this.byStatus=false;
		}
		else if(id==6){
			if (this.byType==false) { 
				this.getType();
				this.byType=true;
			} else {
				this.byType=false;
				this.filterForm.value.type_id = null;
			}
			// this.byDate=false;
			// this.byStatus=false;
		}
		else{
			this.byStatus = false;
			this.byDate = false;
			this.byCompany=false;
			this.byEmployee=false;
			this.byType=false;

			this.filterForm.value.status_id = null;
        	this.filterForm.value.company_id = null;
        	this.filterForm.value.start_date = null;
        	this.filterForm.value.end_date = null;
        	this.filterForm.value.employee_id = null;
        	this.data();
		}

	}

	filterDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;

        let start_date = moment(dateInput.start).format("YYYY-MM-DD");
        let end_date = moment(dateInput.end).format("YYYY-MM-DD");

        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;

        this.data();

    }

	approved(id:any,url:any,type:any) {

		let title = type;
		if(type=='SA'){
			title ='Schedule Adjustment';
		}

		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Approved ' + title,
            message:'Are you sure you want to Approved this Undertime?',
            action:'Approved',
            id:id,
            url:url,
            request:true
        	}).subscribe((isConfirmed)=>{
                this.data();

            });
	}

	rejected(id:any,url:any,type:any) {

		let title = type;
		if(type=='SA'){
			title ='Schedule Adjustment';
		}

		let disposable = this.modalService.addDialog(RemarksModalComponent, {
            title:'Reject ' + title ,
            id:id,
            url:url,
            button:'Reject'
        	}).subscribe((isConfirmed)=>{
                this.data();

            });
	}

	viewDetails(id:any,url:any,type:any,status:any) {
		let type_and_time = false;
		let date_range = false;
		let leave = false;
		let sa = false;
		let ob = false;
		let start_end_time = false;
		let types = false;

		if(type=='COA'){
			type = 'Certificate of Attendance';
			type_and_time = true;
		}
		else if(type=='Undertime' || type=='Overtime'){
			start_end_time = true;
		}
		else if (type=='Official Business'){
			date_range = true;
			ob = true;
		}
		else if (type=='Leave'){
			date_range = true;
			leave = true;
			types = true;
		}
		else if(type=='SA'){
			type='Schedule Adjustment';
			date_range = true;
			sa = true;
		}

		let buttons = true;
		if (status == 'REJECTED' || status == 'APPROVED' || status == 'CANCELLED') {
			buttons = false;
		}

		let disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title:type,
            id:id,
            url:url,
            type_and_time:type_and_time,
            start_end_time:start_end_time,
            date_range:date_range,
            leave:leave,
            sa:sa,
            ob:ob,
            types:types,
            buttons:buttons
        	}).subscribe((isConfirmed)=>{
                this.data();

            });
	}


	ngOnDestroy() {

	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }
}
