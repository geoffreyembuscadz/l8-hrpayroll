import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';

// import { PayrollCreateModalComponent } from './payroll-generate.component';
// import { PayrollEditComponent } from './payroll-edit.component';

import { PayrollService } from '../../services/payroll.service';


@Component({
  selector: 'admin-payroll-list',
  templateUrl: './payroll-list.component.html'
  // styleUrls: ['./payroll-list.component.css']
})

@Injectable()
export class PayrollBillingLocationListComponent implements OnInit, AfterViewInit  {

	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	public id: string;
	public payroll_id: any;
	public success_title: string;

	public error_title: string;
    public error_message: string;

	dtOptions: any = {};

	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	payroll_rec: any;

	constructor(
			private _conf: Configuration, 
			private _payroll_service: PayrollService, 
			private modalService: DialogService, 
			private _rt: Router,
			private _ar: ActivatedRoute, 
		) {
	    this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	}

	ngOnInit() {
		let user = this._payroll_service.getPayrolls().
			subscribe(
				data => {
					 this.payroll_id = Array.from(data);
					 this.rerender();
				},
				err => console.error(err)
			);

		this.dtOptions = {
	      order: [[6, 'desc']],
	      "columnDefs": [
		    { "width": "20%", "targets": [0,6] },
		    { "width": "15%", "targets": [1,5] },
		    { "width": "10%", "targets": [2,3,4] },
		  ],
		  "responsive": true
	    };
		
	}	

	clickRow(id) {
		this._payroll_service.setId(id);
		this._rt.navigate(['admin/payroll/', id]);
	}


	private catchError(error: any){
	    let response_body = error._body;
	    let response_status = error.status;

	    if( response_status == 500 ){
	      this.error_title = 'Error 500';
	      this.error_message = 'Something went wrong';
	    } else if( response_status == 200 ) {
	      this.error_title = '';
	      this.error_message = '';
	    }
	}

	generatePayroll() {
        this._rt.navigate(['/admin/payroll-generate']);
	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	}

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }
	
}