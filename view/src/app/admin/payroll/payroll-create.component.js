var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { DepartmentService } from '../../services/department.service';
import { PayrollService } from '../../services/payroll.service';
import { PositionService } from '../../services/position.service';
import { AuthUserService } from '../../services/auth-user.service';
import { AttendanceService } from '../../services/attendance.service';
import { AdjustmentListModalComponent } from '../adjustment/adjustment-list-modal.component';
import { LoanListModalComponent } from '../loan/loan-list-modal.component';
import { PayrollAllowanceModalComponent } from './payroll-allowance.component';
var PayrollCreateComponent = /** @class */ (function () {
    function PayrollCreateComponent(_conf, _payroll_service, modalService, _rt, _fb, _ar, daterangepickerOptions, _company_serv, _emp_service, _department_serv, _position_serv, _ajd_type, _auth_service, _attendance_serv) {
        this._conf = _conf;
        this._payroll_service = _payroll_service;
        this.modalService = modalService;
        this._rt = _rt;
        this._fb = _fb;
        this._ar = _ar;
        this.daterangepickerOptions = daterangepickerOptions;
        this._company_serv = _company_serv;
        this._emp_service = _emp_service;
        this._department_serv = _department_serv;
        this._position_serv = _position_serv;
        this._ajd_type = _ajd_type;
        this._auth_service = _auth_service;
        this._attendance_serv = _attendance_serv;
        this.dtTrigger = new Subject();
        this.mainInput = {
            start: moment(),
            end: moment()
        };
        this.ismeridian = true;
        this.start_date = new Date();
        this.end_date = new Date();
        this.department = false;
        this.position = false;
        this.company = false;
        this.branch = false;
        this.employee = false;
        this.enableDepartment = false;
        this.enablePosition = false;
        this.enableBranch = false;
        this.selectedCompany = "";
        this.selectedDepartment = "";
        this.selectedPosition = "";
        this.selectedBranch = "";
        this.approved_btn = false;
        this.isGenerated = false;
        this.removed_refresh = false;
        this.employee_list_for_payroll = true;
        this.payroll_ready = true;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.isLoading = false;
        this.removed_employee = [];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.createPayrollForm = _fb.group({
            'name': ['', [Validators.required]],
            'start_time': [null],
            'end_time': [null],
            'date': [null],
            'type_of_run': [''],
            'employee_id': [null],
            'company': [''],
            'branch': [''],
            'department': [''],
            'position': [''],
            'prepared_by': [null],
            'payroll_id': [null],
            'approved': [null],
            'approved_by': [null],
            'created_by': [null],
            'removed_employee': [null],
            'cutoff': [null, [Validators.required]]
        });
    }
    PayrollCreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        var role_model = this.createPayrollForm.value;
        var start = this.start_time;
        var end = this.end_time;
        var company = this.selectedCompany;
        var branch = this.selectedBranch;
        var department = this.selectedDepartment;
        var position = this.selectedPosition;
        var run_type = this.run_type;
        var cutoff = this.createPayrollForm.value.cutoff;
        this.getList();
        if (run_type == "undefined") {
            run_type = "0";
        }
        if (this.selectedBranch == '') {
            branch = 'undefined';
        }
        else {
            branch = this.selectedBranch;
        }
        // this._payroll_service
        // .getPayrollList(start, end, run_type, company, branch, department, position, cutoff)
        // .subscribe(
        //   data => {
        //     this.isLoading = false;
        //   },
        //   err => this.catchError(err)
        // );
        this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY h:mm A' },
            alwaysShowCalendars: false,
            timePicker: true
        };
        this.dtOptions = {
            "aLengthMenu": [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
            Sort: true,
            dom: '<"row"<"col-md-12"l>>Bfrtip',
            buttons: [
                {
                    orientation: 'portrait',
                    pageSize: 'LEGAL',
                    extend: 'excelHtml5',
                    footer: true,
                },
                'colvis'
            ],
            "scrollX": true,
            responsive: true,
            paging: false,
            searching: false,
            scrollCollapse: true,
            fixedColumns: {
                leftColumns: 3
            }
        };
        // let payroll_list = this._payroll_service.getPayrollList(start, end, run_type, company, branch, department, position, cutoff)
        //     .subscribe(
        //       data => {
        //         this.payroll_rec = Array.from(data);
        //         this.payrollFooter(data);
        //         this.rerender();
        //       }
        //     );
        var created_by = this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
        }, function (err) { return console.error(err); });
        this.removed_employee = [];
        if (this.removed_refresh == true) {
            this.getPayrollEmployee(this.removed_employee);
        }
    };
    PayrollCreateComponent.prototype.payrollFooter = function (data) {
        this.payroll_rec = Array.from(data);
        if (data != '' && data != 'undefined') {
            var payroll_len = this.payroll_rec.length;
            this.total_bill_rate = 0;
            this.allowance = 0;
            this.total_allowance = 0;
            this.total_asf = 0;
            this.total_thirteen_m = 0;
            this.total_d_cola = 0;
            this.total_d_rate = 0;
            this.total_basic_pay = 0;
            this.total_ecola = 0;
            this.total_late = 0;
            this.total_undertime = 0;
            this.total_pay_reg_ot = 0;
            this.total_pay_restday = 0;
            this.total_pay_rd_ot = 0;
            this.total_pay_special_hol = 0;
            this.total_pay_special_hol_ot = 0;
            this.total_pay_special_hol_rd = 0;
            this.total_pay_special_hol_rd_ot = 0;
            this.total_pay_legal_hol = 0;
            this.total_pay_legal_hol_ot = 0;
            this.total_pay_legal_hol_restday = 0;
            this.total_pay_legal_hol_rd_ot = 0;
            this.total_pay_double_hol = 0;
            this.total_pay_dbl_hol_ot = 0;
            this.total_pay_double_hol_rd = 0;
            this.total_pay_dbl_hol_rd_ot = 0;
            this.total_pay_reg_nd = 0;
            this.total_pay_rd_nd = 0;
            this.total_pay_special_hol_nd = 0;
            this.total_pay_special_hol_rd_nd = 0;
            this.total_pay_legal_hol_nd = 0;
            this.total_pay_legal_hol_rd_nd = 0;
            this.total_pay_dbl_hol_nd = 0;
            this.total_pay_dbl_hol_rd_nd = 0;
            this.total_adjustment = 0;
            this.total_deduction = 0;
            this.total_loan = 0;
            this.total_sss = 0;
            this.total_philhealth = 0;
            this.total_pagibig = 0;
            this.total_gross_pay = 0;
            this.total_net_pay = 0;
            for (var x = 0; x < payroll_len; x++) {
                this.total_bill_rate = this.total_bill_rate + this.payroll_rec[x].bill_rate * 1;
                this.total_asf = this.total_asf + this.payroll_rec[x].asf * 1;
                this.total_thirteen_m = this.total_thirteen_m + this.payroll_rec[x].thirteen_month * 1;
                this.total_d_cola = this.total_d_cola + this.payroll_rec[x].cola * 1;
                this.total_d_rate = this.total_d_rate + this.payroll_rec[x].daily_rate * 1;
                this.total_basic_pay = this.total_basic_pay + this.payroll_rec[x].ttl_basic_pay * 1;
                this.allowance = this.allowance + this.payroll_rec[x].allowance * 1;
                this.total_allowance = this.total_allowance + this.payroll_rec[x].total_allowance * 1;
                this.total_ecola = this.total_ecola + this.payroll_rec[x].ecola * 1;
                this.total_late = this.total_late + this.payroll_rec[x].late_amount * 1;
                this.total_undertime = this.total_undertime + this.payroll_rec[x].undertime_amount * 1;
                this.total_pay_reg_ot = this.total_pay_reg_ot + this.payroll_rec[x].pay_reg_ot * 1;
                this.total_pay_restday = this.total_pay_restday + this.payroll_rec[x].pay_restday * 1;
                this.total_pay_rd_ot = this.total_pay_rd_ot + this.payroll_rec[x].pay_rd_ot * 1;
                this.total_pay_special_hol = this.total_pay_special_hol + this.payroll_rec[x].pay_special_hol * 1;
                this.total_pay_special_hol_ot = this.total_pay_special_hol_ot + this.payroll_rec[x].pay_special_hol_ot * 1;
                this.total_pay_special_hol_rd = this.total_pay_special_hol_rd + this.payroll_rec[x].pay_special_hol_rd * 1;
                this.total_pay_special_hol_rd_ot = this.total_pay_special_hol_rd_ot + this.payroll_rec[x].pay_special_hol_rd_ot * 1;
                this.total_pay_legal_hol = this.total_pay_legal_hol + this.payroll_rec[x].pay_legal_hol * 1;
                this.total_pay_legal_hol_ot = this.total_pay_legal_hol_ot + this.payroll_rec[x].pay_legal_hol_ot * 1;
                this.total_pay_legal_hol_restday = this.total_pay_legal_hol_restday + this.payroll_rec[x].pay_legal_hol_restday * 1;
                this.total_pay_legal_hol_rd_ot = this.total_pay_legal_hol_rd_ot + this.payroll_rec[x].pay_legal_hol_rd_ot * 1;
                this.total_pay_double_hol = this.total_pay_double_hol + this.payroll_rec[x].pay_double_hol * 1;
                this.total_pay_dbl_hol_ot = this.total_pay_dbl_hol_ot + this.payroll_rec[x].pay_dbl_hol_ot * 1;
                this.total_pay_double_hol_rd = this.total_pay_double_hol_rd + this.payroll_rec[x].pay_double_hol_rd * 1;
                this.total_pay_dbl_hol_rd_ot = this.total_pay_dbl_hol_rd_ot + this.payroll_rec[x].pay_dbl_hol_rd_ot * 1;
                this.total_pay_reg_nd = this.total_pay_reg_nd + this.payroll_rec[x].pay_reg_nd * 1;
                this.total_pay_rd_nd = this.total_pay_rd_nd + this.payroll_rec[x].pay_rd_nd * 1;
                this.total_pay_special_hol_nd = this.total_pay_special_hol_nd + this.payroll_rec[x].pay_special_hol_nd * 1;
                this.total_pay_special_hol_rd_nd = this.total_pay_special_hol_rd_nd + this.payroll_rec[x].pay_special_hol_rd_nd * 1;
                this.total_pay_legal_hol_nd = this.total_pay_legal_hol_nd + this.payroll_rec[x].pay_legal_hol_nd * 1;
                this.total_pay_legal_hol_rd_nd = this.total_pay_legal_hol_rd_nd + this.payroll_rec[x].pay_legal_hol_rd_nd * 1;
                this.total_pay_dbl_hol_nd = this.total_pay_dbl_hol_nd + this.payroll_rec[x].pay_dbl_hol_nd * 1;
                this.total_pay_dbl_hol_rd_nd = this.total_pay_dbl_hol_rd_nd + this.payroll_rec[x].pay_dbl_hol_rd_nd * 1;
                this.total_adjustment = this.total_adjustment + this.payroll_rec[x].total_adjustment * 1;
                this.total_deduction = this.total_deduction + this.payroll_rec[x].total_deduction * 1;
                this.total_loan = this.total_loan + this.payroll_rec[x].total_loan * 1;
                this.total_sss = this.total_sss + this.payroll_rec[x].total_sss * 1;
                this.total_philhealth = this.total_philhealth + this.payroll_rec[x].total_philhealth * 1;
                this.total_pagibig = this.total_pagibig + this.payroll_rec[x].total_pagibig * 1;
                this.total_gross_pay = this.total_gross_pay + this.payroll_rec[x].gross_pay * 1;
                this.total_net_pay = this.total_net_pay + this.payroll_rec[x].net_pay * 1;
            }
        }
    };
    PayrollCreateComponent.prototype.deleteRow = function () {
        if (confirm("Do you want to remove the employee on this payroll period?")) {
            this.dtOptions.row('.selected').remove().draw(false);
        }
    };
    PayrollCreateComponent.prototype.generatePayroll = function () {
        var _this = this;
        this.isLoading = true;
        var role_model = this.createPayrollForm.value;
        var start = this.start_time;
        var end = this.end_time;
        var company = this.selectedCompany;
        var department = this.selectedDepartment;
        var position = this.selectedPosition;
        var run_type = this.run_type;
        var cutoff = this.cutoff;
        var branch = '';
        var removed_employee;
        if (run_type == "undefined") {
            run_type = "0";
        }
        if (this.selectedBranch == '') {
            branch = 'undefined';
        }
        else {
            branch = this.selectedBranch;
        }
        if (this.removed_employee.length > 0) {
            removed_employee = this.removed_employee;
        }
        else {
            removed_employee = 'undefined';
        }
        this._payroll_service
            .getPayrollList(start, end, run_type, company, branch, department, position, cutoff, removed_employee)
            .subscribe(function (data) {
            _this.rerender();
            _this.isLoading = false;
            _this.isGenerated = true;
            _this.payrollFooter(data);
        }, function (err) { return _this.catchError(err); });
    };
    PayrollCreateComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_time = moment(dateInput.start).format("YYYY-MM-DD HH:mm:ss");
        this.end_time = moment(dateInput.end).format("YYYY-MM-DD HH:mm:ss");
    };
    PayrollCreateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    PayrollCreateComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    PayrollCreateComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    PayrollCreateComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    PayrollCreateComponent.prototype.runtypeChange = function (value) {
        this.run_type = value;
        this.selectedDepartment = '';
        this.selectedPosition = '';
        if (value == 1) {
            this.company = true;
            this.branch = true;
            this.employee = false;
            this.position = false;
            this.department = false;
        }
        else if (value == 2) {
            this.department = true;
            this.position = false;
            this.company = true;
            this.branch = true;
            this.employee = false;
        }
        else if (value == 3) {
            this.position = true;
            this.department = false;
            this.company = true;
            this.employee = false;
            this.branch = true;
        }
        else {
            this.position = false;
            this.department = false;
            this.company = false;
            this.employee = false;
            this.branch = false;
            this.selectedCompany = '';
            this.selectedDepartment = '';
            this.selectedPosition = '';
            this.selectedBranch = '';
        }
    };
    PayrollCreateComponent.prototype.payrollPeriodChange = function (value) {
        this.cutoff = value;
    };
    PayrollCreateComponent.prototype.getList = function () {
        var _this = this;
        var companies = this._company_serv.getCompanys()
            .subscribe(function (data) {
            _this.company_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var department = this._department_serv.getDepartments()
            .subscribe(function (data) {
            _this.department_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var position = this._position_serv.getPositions()
            .subscribe(function (data) {
            _this.position_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var adjustment = this._ajd_type.getAdjustmentsType()
            .subscribe(function (data) {
            _this.adj_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var company_branch = this._company_serv.getCompanyBranch()
            .subscribe(function (data) {
            _this.company_branch_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
    };
    PayrollCreateComponent.prototype.changed = function (data) {
        this.current = data.value;
    };
    PayrollCreateComponent.prototype.getCompany = function (value, event) {
        this.enableDepartment = true;
        this.enablePosition = true;
        this.enableBranch = true;
        this.selectedCompany = value;
    };
    PayrollCreateComponent.prototype.getDepartment = function (value, event) {
        if (value == '-none-') {
            this.selectedDepartment = '';
        }
        else {
            this.selectedDepartment = value;
        }
    };
    PayrollCreateComponent.prototype.getPosition = function (value, event) {
        if (value == '-none-') {
            this.selectedPosition = '';
        }
        else {
            this.selectedPosition = value;
        }
    };
    PayrollCreateComponent.prototype.getBranch = function (value, event) {
        if (value == '-none-') {
            this.selectedBranch = 'none';
        }
        else {
            this.selectedBranch = value;
        }
    };
    PayrollCreateComponent.prototype.viewLoan = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanListModalComponent, {
            att_id: id,
            start: this.start_time,
            end: this.end_time
        }).subscribe(function (isConfirmed) {
            if (isConfirmed) {
                _this.rerender();
            }
        });
    };
    PayrollCreateComponent.prototype.viewAdjustment = function (id, type, no_of_days) {
        var _this = this;
        var disposable = this.modalService.addDialog(AdjustmentListModalComponent, {
            att_id: id,
            no_of_days: no_of_days,
            start: this.start_time,
            end: this.end_time,
            type: type
        }).subscribe(function (isConfirmed) {
            if (isConfirmed) {
                _this.rerender();
            }
        });
    };
    PayrollCreateComponent.prototype.updateAllowance = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(PayrollAllowanceModalComponent, {
            emp_id: id
        }).subscribe(function (isConfirmed) {
            if (isConfirmed) {
                _this.rerender();
            }
        });
    };
    PayrollCreateComponent.prototype.savePayroll = function () {
        var _this = this;
        var start = this.start_time;
        var end = this.end_time;
        var company = this.selectedCompany;
        var branch = this.selectedBranch;
        var department = this.selectedDepartment;
        var position = this.selectedPosition;
        var run_type = this.run_type;
        var cutoff = this.cutoff;
        var removed_employee = this.removed_employee;
        this.createPayrollForm.patchValue({
            'start_time': this.start_time
        });
        this.createPayrollForm.patchValue({
            'end_time': this.end_time
        });
        this.createPayrollForm.patchValue({
            'company': this.selectedCompany
        });
        this.createPayrollForm.patchValue({
            'branch': this.selectedBranch
        });
        this.createPayrollForm.patchValue({
            'department': this.selectedDepartment
        });
        this.createPayrollForm.patchValue({
            'position': this.selectedPosition
        });
        this.createPayrollForm.patchValue({
            'cutoff': cutoff
        });
        this.createPayrollForm.patchValue({
            'approved': 0
        });
        this.createPayrollForm.patchValue({
            'created_by': this.user_id
        });
        this.createPayrollForm.patchValue({
            'approved_by': 0
        });
        this.createPayrollForm.patchValue({
            'removed_employee': this.removed_employee.join(",")
        });
        this.createPayrollForm.value.prepared_by = this.user_id;
        var payroll_model = this.createPayrollForm.value;
        var storePayroll = this._payroll_service.storePayroll(payroll_model)
            .subscribe(function (data) {
            _this._rt.navigate(['/admin/payroll']);
        }, function (err) { return _this.catchError(err); });
    };
    PayrollCreateComponent.prototype.savePayrollList = function (id) {
        var _this = this;
        this.createPayrollForm.value.payroll_id = id;
        this.payroll_list_model = this.createPayrollForm.value;
        var storePayrollList = this._payroll_service.storePayrollList(this.payroll_list_model).subscribe(function (data) { }, function (err) { return _this.catchError(err); });
    };
    PayrollCreateComponent.prototype.removeEmployeeFromPayrollGeneration = function (value) {
        // check if already exist
        if (this.removed_employee.indexOf(value) == -1) {
            this.removed_employee.push(value);
        }
        this.getPayrollEmployee(this.removed_employee);
    };
    PayrollCreateComponent.prototype.getPayrollEmployee = function (employee) {
        var _this = this;
        this.isLoading = true;
        this.employee_list_for_payroll = false;
        var role_model = this.createPayrollForm.value;
        var start = this.start_time;
        var end = this.end_time;
        var company = this.selectedCompany;
        var department = this.selectedDepartment;
        var position = this.selectedPosition;
        var run_type = this.run_type;
        var cutoff = this.cutoff;
        var branch = '';
        if (run_type == "undefined") {
            run_type = "0";
        }
        if (this.selectedBranch == '') {
            branch = 'undefined';
        }
        else {
            branch = this.selectedBranch;
        }
        var removed_employee = employee;
        this._payroll_service
            .getPayrollEmployee(start, end, run_type, company, branch, department, position, cutoff, removed_employee)
            .subscribe(function (data) {
            _this.isLoading = false;
            _this.isGenerated = true;
            _this.employee_list = Array.from(data);
            // get the count for validation
            _this.getTheCountOfEmployee(_this.employee_list.length);
        }, function (err) { return _this.catchError(err); });
    };
    PayrollCreateComponent.prototype.getTheCountOfEmployee = function (length) {
        if (length == 0) {
            this.employee_list_for_payroll = true;
            this.removed_employee = [];
            this.generatePayroll();
        }
        else {
            this.employee_list_for_payroll = false;
        }
    };
    // not using because angular don't support new tab passing url data; it's here incase 
    PayrollCreateComponent.prototype.goAttendance = function (id) {
        var _this = this;
        var start = moment(this.start_time).format("YYYY-MM-DD");
        var end = moment(this.end_time).format("YYYY-MM-DD");
        var company = this.selectedCompany;
        var employee = id;
        var model;
        model = {};
        model['start_date'] = start;
        model['end_date'] = end;
        model['company_id'] = company;
        model['employee_id'] = employee;
        model['type_id'] = null;
        console.log('model', model);
        this._attendance_serv.getAttendDefault(model).
            subscribe(function (data) {
            _this.att = Array.from(data);
            window.open("https://www.google.com", "_blank");
            console.log('attendance', _this.att);
        }, function (err) { return console.error(err); });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], PayrollCreateComponent.prototype, "dtElement", void 0);
    PayrollCreateComponent = __decorate([
        Component({
            selector: 'admin-payroll-create',
            templateUrl: './payroll-create.component.html',
            styleUrls: ['./payroll.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration,
            PayrollService,
            DialogService,
            Router,
            FormBuilder,
            ActivatedRoute,
            DaterangepickerConfig,
            CompanyService,
            EmployeeService,
            DepartmentService,
            PositionService,
            AdjustmentTypeService,
            AuthUserService,
            AttendanceService])
    ], PayrollCreateComponent);
    return PayrollCreateComponent;
}());
export { PayrollCreateComponent };
//# sourceMappingURL=payroll-create.component.js.map