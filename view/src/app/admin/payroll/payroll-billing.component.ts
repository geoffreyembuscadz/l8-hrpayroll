import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TimepickerModule } from 'ngx-bootstrap';

import { AdjustmentTypeService } from '../../services/adjustment-type.service';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { DepartmentService } from '../../services/department.service';
import { PayrollService } from '../../services/payroll.service';
import { PositionService } from '../../services/position.service';
import { AuthUserService } from '../../services/auth-user.service';

import { AdjustmentListModalComponent } from '../adjustment/adjustment-list-modal.component';
import { LoanListModalComponent } from '../loan/loan-list-modal.component';

import { Select2OptionData } from 'ng2-select2';


@Component({
  selector: 'admin-payroll-create',
  templateUrl: './payroll-billing.component.html',
  styleUrls: ['./payroll.component.css']
})

@Injectable()
export class PayrollBillingComponent implements OnInit, AfterViewInit  {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();

  dtOptions: any;
  persons: any;
  user_id: any;

  public id: string;
  public payroll_id: any;
  public mainInput = {
      start: moment().subtract(12, 'month'),
      end: moment().subtract(11, 'month')
  }
  public ismeridian:boolean = true;
  public start_date:Date = new Date();
  public end_date:Date = new Date();
  public start_time:any;
  public end_time:any;
  public approved:any;
  public length_days:any;
  public length_hours:any;
  public department: boolean = false;
  public position: boolean = false;
  public company: boolean = false;
  public employee: boolean = false;
  public enableDepartment: boolean = false;
  public enablePosition: boolean = false;
  public run_type: any;
  public cutoff: any;
  public selectedCompany: any;
  public selectedBranch: any;
  public selectedDepartment: any;
  public selectedPosition: any;
  public branch_location: any;

  public total_bill_rate:any;
  public total_asf:any;
  public total_thirteen_m: any;
  public total_d_cola: any;
  public total_d_rate: any;
  public total_basic_pay: any;
  public allowance: any;
  public total_allowance: any;
  public total_ecola: any;
  public total_late: any;
  public total_undertime: any;
  public total_pay_reg_ot: any;
  public total_pay_restday: any;
  public total_pay_rd_ot: any;
  public total_pay_special_hol: any;
  public total_pay_special_hol_ot: any;
  public total_pay_special_hol_rd: any;
  public total_pay_special_hol_rd_ot: any;
  public total_pay_legal_hol: any;
  public total_pay_legal_hol_ot: any;
  public total_pay_legal_hol_restday: any;
  public total_pay_legal_hol_rd_ot: any;
  public total_pay_double_hol: any;
  public total_pay_dbl_hol_ot: any;
  public total_pay_double_hol_rd: any;
  public total_pay_dbl_hol_rd_ot: any;
  public total_pay_reg_nd: any;
  public total_pay_rd_nd: any;
  public total_pay_special_hol_nd: any;
  public total_pay_special_hol_rd_nd: any;
  public total_pay_legal_hol_nd: any;
  public total_pay_legal_hol_rd_nd: any;
  public total_pay_dbl_hol_nd: any;
  public total_pay_dbl_hol_rd_nd: any;
  public total_adjustment: any;
  public total_deduction: any;
  public total_loan: any;
  public total_sss: any;
  public total_philhealth: any;
  public total_pagibig: any;
  public total_sss_employer: any;
  public total_philhealth_employer: any;
  public total_pagibig_employer: any;
  public ecc: any;
  public total_mandatory: any;
  public total_mandatory_ee: any;
  public total_mandatory_er: any;
  public total_billable: any;
  public vat: any;
  public total_gross_pay: any;
  public total_net_pay: any;
  public total_admin_fee: any;

  public error_title: string;
  public error_message: string;
  private success_message: string;
  public success_title: string;

  bodyClasses:string = "skin-blue sidebar-mini";
  body = document.getElementsByTagName('body')[0];
  payroll_rec: any;
  payroll_list_rec: any;
  emp_rec: any;
  company_rec: any;
  department_rec: any;
  position_rec: any;
  adj_rec: any;
  prepared_by: any;
  id_payroll: any;

  close: any;

  payroll_list_data: any;


  public PayrollData: Array<0>;
  public value: string[];
  public current: any;
  public options: Select2Options;

  updatePayroll : FormGroup;
  createPayrollList : FormGroup;

  constructor(
      private _conf: Configuration, 
      private _payroll_service: PayrollService, 
      private modalService: DialogService, 
      private _rt: Router,
      private _fb: FormBuilder, 
      private _ar: ActivatedRoute, 
      private daterangepickerOptions: DaterangepickerConfig,
      private _company_serv: CompanyService,
      private _emp_service: EmployeeService,
      private _department_serv: DepartmentService,
      private _position_serv: PositionService,
      private _ajd_type: AdjustmentTypeService,
      private _auth_service: AuthUserService, 
    ) {
      this.body.classList.add("skin-blue");
      this.body.classList.add("sidebar-mini");

      this.id = this._payroll_service.getId();
  }

  ngOnInit() {
    
    let start = this.start_time;
    let end   = this.end_time;
    let company = this.selectedCompany;
    let branch = this.selectedBranch;
    let department = this.selectedDepartment;
    let position = this.selectedPosition;
    let run_type = this.run_type;
    let cutoff = this.cutoff;
    let remove_employee: any;

    if (this.branch_location != 0) {
      
      let payroll_list_approved = this._payroll_service.getPayrollArchieve(this.id)
      .subscribe(
          data => {
            this.payroll_rec = data;
            this.total(data);
            this.rerender();

          }
      );
    } else {
      let billing = this._payroll_service
      .getPayrollBilling(
        start, end, run_type, company,
       branch, department, position, cutoff, remove_employee, this.branch_location)
      .subscribe(
          data => {
            this.payroll_rec = data;
            this.total(data);
            this.rerender();

          }

      );
    }


    this.dtOptions = {
        "aLengthMenu": [[10, 25, 50, 100,500,1000,-1], [10, 25, 50,100,500,1000, "All"]],
          Sort: true,
        dom: '<"row"<"col-md-12"l>>Bfrtip',
        buttons: [
          {
              orientation: 'portrait',
              pageSize: 'LEGAL',
              extend: 'excelHtml5',
              footer: true,
          },
          'colvis'
        ],
        "scrollX": true,
          "fixedColumns":    {
              "leftColumns": 3
          }
    };

  }  

  private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_time = moment(dateInput.start).format("YYYY-MM-DD HH:mm:ss");
        this.end_time = moment(dateInput.end).format("YYYY-MM-DD HH:mm:ss");

  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'Something went wrong';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }
  }

  billingByLocation() {
    
  }

  ngOnDestroy() {
      this.body.classList.remove("skin-blue");
      this.body.classList.remove("sidebar-mini");
   }

  ngAfterViewInit(): void {
      this.dtTrigger.next();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

 viewLoan(id) {
   let disposable = this.modalService.addDialog(LoanListModalComponent, {
      att_id:id,
      start : this.start_time,
      end   : this.end_time
      }).subscribe((isConfirmed)=>{
        if (isConfirmed) {
          this.rerender();
        }
    });
 }
 
 viewAdjustment(id, type, no_of_days) {
    let disposable = this.modalService.addDialog(AdjustmentListModalComponent, {
      att_id:id,
      no_of_days:no_of_days,
      start : this.start_time,
      end   : this.end_time,
      type  : type
      }).subscribe((isConfirmed)=>{
        if (isConfirmed) {
          this.rerender();
        }
    });
 }

 createSOA() {

    this._payroll_service.generateSOA(this.id_payroll).subscribe(data => {
          window.open(data.url, "_blank");

    });
 }

 total(data) {
      if (data != '' && this.payroll_rec.length != 'undefined') {
      let payroll_len = this.payroll_rec.length; this.total_bill_rate = 0; this.total_asf = 0; this.total_allowance = 0; this.allowance = 0; this.total_thirteen_m = 0; this.total_d_cola = 0; this.total_d_rate = 0; this.total_basic_pay = 0; this.total_ecola = 0; this.total_late = 0; this.total_undertime = 0; this.total_pay_reg_ot = 0; this.total_pay_restday = 0; this.total_pay_rd_ot = 0; this.total_pay_special_hol = 0; this.total_pay_special_hol_ot = 0; this.total_pay_special_hol_rd = 0; this.total_pay_special_hol_rd_ot = 0; this.total_pay_legal_hol = 0; this.total_pay_legal_hol_ot = 0; this.total_pay_legal_hol_restday = 0; this.total_pay_legal_hol_rd_ot = 0; this.total_pay_double_hol = 0; this.total_pay_dbl_hol_ot = 0; this.total_pay_double_hol_rd = 0; this.total_pay_dbl_hol_rd_ot = 0; this.total_pay_reg_nd = 0;  this.total_pay_rd_nd = 0;  this.total_pay_special_hol_nd = 0;  this.total_pay_special_hol_rd_nd = 0;  this.total_pay_legal_hol_nd = 0;  this.total_pay_legal_hol_rd_nd = 0;  this.total_pay_dbl_hol_nd = 0;  this.total_pay_dbl_hol_rd_nd = 0;  this.total_adjustment = 0; this.total_deduction = 0;  this.total_loan = 0;  this.total_sss = 0; this.total_sss_employer = 0;  this.total_philhealth = 0; this.total_pagibig = 0; this.ecc = 0;this.total_mandatory = 0; this.total_admin_fee = 0;this.total_billable = 0; this.vat = 0; this.total_gross_pay = 0; this.total_net_pay = 0;
      for (let x = 0; x < payroll_len; x++) {
        this.total_bill_rate = this.total_bill_rate + this.payroll_rec[x].bill_rate * 1;
        this.total_asf = this.total_asf + this.payroll_rec[x].asf * 1;
        this.total_thirteen_m = this.total_thirteen_m + this.payroll_rec[x].thirteen_month * 1;
        this.total_d_cola = this.total_d_cola + this.payroll_rec[x].cola * 1;
        this.total_d_rate = this.total_d_rate + this.payroll_rec[x].daily_rate * 1;
        this.total_basic_pay = this.total_basic_pay + this.payroll_rec[x].ttl_basic_pay * 1;
        this.allowance = this.allowance + this.payroll_rec[x].allowance * 1;
        this.total_allowance = this.total_allowance + this.payroll_rec[x].total_allowance * 1;
        this.total_ecola = this.total_ecola + this.payroll_rec[x].ecola * 1;
        this.total_late = this.total_late + this.payroll_rec[x].late_amount * 1;  
        this.total_undertime = this.total_undertime + this.payroll_rec[x].undertime_amount * 1;
        this.total_pay_reg_ot = this.total_pay_reg_ot + this.payroll_rec[x].pay_reg_ot * 1;
        this.total_pay_restday = this.total_pay_restday + this.payroll_rec[x].pay_restday * 1;
        this.total_pay_rd_ot = this.total_pay_rd_ot + this.payroll_rec[x].pay_rd_ot * 1;
        this.total_pay_special_hol = this.total_pay_special_hol + this.payroll_rec[x].pay_special_hol * 1;
        this.total_pay_special_hol_ot = this.total_pay_special_hol_ot + this.payroll_rec[x].pay_special_hol_ot * 1;
        this.total_pay_special_hol_rd = this.total_pay_special_hol_rd + this.payroll_rec[x].pay_special_hol_rd * 1;
        this.total_pay_special_hol_rd_ot = this.total_pay_special_hol_rd_ot + this.payroll_rec[x].pay_special_hol_rd_ot * 1;
        this.total_pay_legal_hol = this.total_pay_legal_hol + this.payroll_rec[x].pay_legal_hol * 1;
        this.total_pay_legal_hol_ot = this.total_pay_legal_hol_ot + this.payroll_rec[x].pay_legal_hol_ot * 1;
        this.total_pay_legal_hol_restday = this.total_pay_legal_hol_restday + this.payroll_rec[x].pay_legal_hol_restday * 1;
        this.total_pay_legal_hol_rd_ot = this.total_pay_legal_hol_rd_ot + this.payroll_rec[x].pay_legal_hol_rd_ot * 1;
        this.total_pay_double_hol = this.total_pay_double_hol + this.payroll_rec[x].pay_double_hol * 1;
        this.total_pay_dbl_hol_ot = this.total_pay_dbl_hol_ot + this.payroll_rec[x].pay_dbl_hol_ot * 1;
        this.total_pay_double_hol_rd = this.total_pay_double_hol_rd + this.payroll_rec[x].pay_double_hol_rd * 1;
        this.total_pay_dbl_hol_rd_ot = this.total_pay_dbl_hol_rd_ot + this.payroll_rec[x].pay_dbl_hol_rd_ot * 1;
        this.total_pay_reg_nd = this.total_pay_reg_nd + this.payroll_rec[x].pay_reg_nd * 1;
        this.total_pay_rd_nd = this.total_pay_rd_nd + this.payroll_rec[x].pay_rd_nd * 1;
        this.total_pay_special_hol_nd = this.total_pay_special_hol_nd + this.payroll_rec[x].pay_special_hol_nd * 1;
        this.total_pay_special_hol_rd_nd = this.total_pay_special_hol_rd_nd + this.payroll_rec[x].pay_special_hol_rd_nd * 1;
        this.total_pay_legal_hol_nd = this.total_pay_legal_hol_nd + this.payroll_rec[x].pay_legal_hol_nd * 1;
        this.total_pay_legal_hol_rd_nd = this.total_pay_legal_hol_rd_nd + this.payroll_rec[x].pay_legal_hol_rd_nd * 1;
        this.total_pay_dbl_hol_nd = this.total_pay_dbl_hol_nd + this.payroll_rec[x].pay_dbl_hol_nd * 1;
        this.total_pay_dbl_hol_rd_nd = this.total_pay_dbl_hol_rd_nd + this.payroll_rec[x].pay_dbl_hol_rd_nd * 1;
        this.total_adjustment = this.total_adjustment + this.payroll_rec[x].total_adjustment * 1;
        this.total_deduction = this.total_deduction + this.payroll_rec[x].total_deduction * 1;
        this.total_loan = this.total_loan + this.payroll_rec[x].total_loan * 1;
        this.total_admin_fee = this.total_admin_fee + this.payroll_rec[x].admin_fee * 1;
        this.total_sss = this.total_sss + this.payroll_rec[x].total_sss * 1;
        this.total_sss_employer = this.total_sss_employer + this.payroll_rec[x].total_sss_employer * 1;
        this.total_philhealth = this.total_philhealth + this.payroll_rec[x].total_philhealth * 1;
        this.total_philhealth_employer = this.total_philhealth_employer + this.payroll_rec[x].total_philhealth_employer * 1;
        this.total_pagibig = this.total_pagibig + this.payroll_rec[x].total_pagibig * 1;
        this.total_pagibig_employer = this.total_pagibig_employer + this.payroll_rec[x].total_pagibig_employer * 1;
        this.ecc = this.ecc + this.payroll_rec[x].ecc * 1;
        this.total_mandatory = this.total_mandatory + this.payroll_rec[x].total_mandatory * 1;
        this.total_billable = this.total_billable + this.payroll_rec[x].total_billable * 1;
        this.vat = this.vat + this.payroll_rec[x].vat * 1;
        this.total_gross_pay = this.total_gross_pay + this.payroll_rec[x].gross_pay * 1;
        this.total_net_pay = this.total_net_pay + this.payroll_rec[x].net_pay * 1;
        this.id_payroll = this.payroll_rec[x].payroll_id;
        this.total_mandatory_ee = this.total_mandatory_ee + this.payroll_rec[x].total_mandatory_ee * 1;
        this.total_mandatory_er = this.total_mandatory_er + this.payroll_rec[x].total_mandatory_er * 1;



      }

    }
  }

 

}