  
import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TimepickerModule } from 'ngx-bootstrap';

import { AdjustmentTypeService } from '../../services/adjustment-type.service';

import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { DepartmentService } from '../../services/department.service';
import { PayrollService } from '../../services/payroll.service';
import { PositionService } from '../../services/position.service';
import { AuthUserService } from '../../services/auth-user.service';

import { PayrollPayslipComponent } from './payroll-payslip.component';
import { AdjustmentListModalComponent } from '../adjustment/adjustment-list-modal.component';
import { LoanListModalComponent } from '../loan/loan-list-modal.component';
import { PayrollAllowanceModalComponent } from './payroll-allowance.component';

import { Select2OptionData } from 'ng2-select2';


@Component({
  selector: 'admin-payroll-create',
  templateUrl: './payroll-view.component.html',
  styleUrls: ['./payroll.component.css']
})

@Injectable()
export class PayrollViewComponent implements OnInit, AfterViewInit  {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();

  dtOptions: any;
  persons: any;
  user_id: any;

  public id: string;
  public payroll_id: any;
  public mainInput = {
      start: moment().subtract(12, 'month'),
      end: moment().subtract(11, 'month')
  }
  public ismeridian:boolean = true;
  public start_date:Date = new Date();
  public end_date:Date = new Date();
  public start_time:any;
  public end_time:any;
  public approved:any;
  public length_days:any;
  public length_hours:any;
  public department: boolean = false;
  public position: boolean = false;
  public company: boolean = false;
  public branch: boolean = false;
  public employee: boolean = false;
  public enableDepartment: boolean = false;
  public enablePosition: boolean = false;
  public run_type: any;
  public cutoff: any;
  public selectedCompany: any;
  public selectedBranch: any;
  public selectedDepartment: any;
  public selectedPosition: any;

  public total_thirteen_m: any;
  public total_d_cola: any;
  public total_d_rate: any;
  public total_basic_pay: any;
  public allowance: any;
  public total_allowance: any;
  public total_ecola: any;
  public total_late: any;
  public total_undertime: any;
  public total_pay_reg_ot: any;
  public total_pay_restday: any;
  public total_pay_rd_ot: any;
  public total_pay_special_hol: any;
  public total_pay_special_hol_ot: any;
  public total_pay_special_hol_rd: any;
  public total_pay_special_hol_rd_ot: any;
  public total_pay_legal_hol: any;
  public total_pay_legal_hol_ot: any;
  public total_pay_legal_hol_restday: any;
  public total_pay_legal_hol_rd_ot: any;
  public total_pay_double_hol: any;
  public total_pay_dbl_hol_ot: any;
  public total_pay_double_hol_rd: any;
  public total_pay_dbl_hol_rd_ot: any;
  public total_pay_reg_nd: any;
  public total_pay_rd_nd: any;
  public total_pay_special_hol_nd: any;
  public total_pay_special_hol_rd_nd: any;
  public total_pay_legal_hol_nd: any;
  public total_pay_legal_hol_rd_nd: any;
  public total_pay_dbl_hol_nd: any;
  public total_pay_dbl_hol_rd_nd: any;
  public total_adjustment: any;
  public total_deduction: any;
  public total_loan: any;
  public total_sss: any;
  public total_philhealth: any;
  public total_pagibig: any;
  public total_gross_pay: any;
  public total_net_pay: any;

  public error_title: string;
  public error_message: string;
  private success_message: string;
  public success_title: string;

  public approved_btn: boolean = false;

  public removed_employee: any[] = [];

  bodyClasses:string = "skin-blue sidebar-mini";
  body = document.getElementsByTagName('body')[0];
  payroll_rec: any;
  payroll_list_rec: any;
  emp_rec: any;
  company_rec: any;
  department_rec: any;
  position_rec: any;
  adj_rec: any;

  payroll_list_data: any;

  public isLoading: boolean = true;

  public PayrollData: Array<0>;
  public value: string[];
  public current: any;
  public options: Select2Options;

  updatePayroll : FormGroup;
  createPayrollList : FormGroup;

  constructor(
      private _conf: Configuration, 
      private _payroll_service: PayrollService, 
      private modalService: DialogService, 
      private _rt: Router,
      private _fb: FormBuilder, 
      private _ar: ActivatedRoute, 
      private daterangepickerOptions: DaterangepickerConfig,
      private _company_serv: CompanyService,
      private _emp_service: EmployeeService,
      private _department_serv: DepartmentService,
      private _position_serv: PositionService,
      private _ajd_type: AdjustmentTypeService,
      private _auth_service: AuthUserService, 
    ) {
      this.body.classList.add("skin-blue");
      this.body.classList.add("sidebar-mini");

      this.id = this._payroll_service.getId();

      this.updatePayroll = _fb.group({
        'approved': [''],
        'approved_by': [''],
      }); 
      this.createPayrollList = _fb.group({
        'name':     ['',[Validators.required]],
        'start_time':     [null],
        'end_time':     [null],
        'date':       [null],
        'type_of_run': [''],
        'employee_id': [null],
        'company': [''],
        'branch': [''],
        'department': [''],
        'position': [''],
        'payroll_id':[null],
        'created_by':[null],
        'cutoff':[null],
        'removed_employee':[null],

      }); 

      let payroll = this._payroll_service.getPayroll(this.id)
          .subscribe(
            data => {
              this.start_time = data.start_time;
              this.end_time = data.end_time;
              this.selectedCompany = data.company;
              this.selectedBranch = data.branch;
              this.selectedDepartment = data.department;
              this.selectedPosition = data.position;
              this.run_type = data.type_of_run;
              this.cutoff = data.cutoff;
              this.approved = data.approved;
              this.removed_employee = data.removed_employee;
              if (this.approved == 1) { 
                this.approved_btn = true; 
              } else { this.approved_btn = false; }

              this.ngOnInit();
              }
          );

  }

  ngOnInit() {
    let start = this.start_time;
    let end   = this.end_time;
    let company = this.selectedCompany;
    let branch = this.selectedBranch;
    let department = this.selectedDepartment;
    let position = this.selectedPosition;
    let run_type = this.run_type;
    let cutoff = this.cutoff;
    let removed_employee = this.removed_employee;

    this.getList();

    this.dtOptions = {
        "aLengthMenu": [[10, 25, 50, 100,500,1000,-1], [10, 25, 50,100,500,1000, "All"]],
        Sort: true,
        dom: '<"row"<"col-md-12"l>>Bfrtip',
        buttons: [
          {
              orientation: 'portrait',
              pageSize: 'LEGAL',
              extend: 'excelHtml5',
              footer: true,
          },
          'colvis'
        ],
        "scrollX": true,
        responsive: true,
        paging: false,
        searching: false,
        scrollCollapse: true,
        fixedColumns:   {
            leftColumns: 3
        }
    };


    if (this.approved == 0) {
      let removed_employee: any;
      if (this.removed_employee.length > 0) {
         removed_employee = this.removed_employee;
      } else {
        removed_employee = 'undefined';
      }

      let payroll_list = this._payroll_service.getPayrollList(start, end, run_type, company, branch, department, position, cutoff, removed_employee).subscribe(
          data => {
            this.payroll_rec = data;
            this.total(data);
            this.rerender();
            this.isLoading = false;

          }
        );
    } else {
      let payroll_list_approved = this._payroll_service.getPayrollArchieve(this.id)
      .subscribe(
        data => {
          this.payroll_rec = data;
          this.total(data);
          this.rerender();
          this.isLoading = false;

        }
      );
    }
    

    let created_by = this._auth_service.getUser()
    .subscribe(
      data => {
        let user = data; 
        this.user_id = user.id;
      },
      err => console.error(err)
    );
  }  

  

  saveAndApproved() {
    this.updatePayroll.patchValue({
      'approved': 1 
    });
    this.updatePayroll.patchValue({
      'created_by': this.id 
    });
    this.updatePayroll.patchValue({
      'approved_by': this.user_id 
    });

    let payroll_model = this.updatePayroll.value;

    this._payroll_service.updatePayroll(this.id, payroll_model)
    .subscribe(
      data => {
        this.payroll_list_rec = Array.from(data);
        this.savePayrollList();
        this._rt.navigate(['/admin/payroll']);
      },
      err => this.catchError(err)
    );
  }

  onSubmit() {
    
    this._payroll_service
    .getPayrollList(this.start_time, this.end_time, this.run_type, this.selectedCompany, this.selectedBranch, this.selectedDepartment, this.selectedPosition, this.cutoff, this.removed_employee)
    .subscribe(
      data => {
        this.rerender();
        this.ngOnInit();
      },
      err => this.catchError(err)
    );
  }

  savePayrollList() {
    this.createPayrollList.value.payroll_id = this.id;
    this.createPayrollList.value.start_time = this.start_time;
    this.createPayrollList.value.end_time = this.end_time;
    this.createPayrollList.value.company = this.selectedCompany;
    this.createPayrollList.value.branch = this.selectedBranch;
    this.createPayrollList.value.department = this.selectedDepartment;
    this.createPayrollList.value.position = this.selectedPosition;
    this.createPayrollList.value.type_of_run = this.run_type;
    this.createPayrollList.value.cutoff = this.cutoff;
    this.createPayrollList.value.removed_employee = this.removed_employee;
    this.createPayrollList.value.approved_by = this.user_id;
    this.createPayrollList.value.created_by = this.user_id;
    this.payroll_list_data = this.createPayrollList.value;
    
    let storePayrollList = this._payroll_service.storePayrollList(this.payroll_list_data).subscribe(data=>{},err=>this.catchError(err));

  }


  private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_time = moment(dateInput.start).format("YYYY-MM-DD HH:mm:ss");
        this.end_time = moment(dateInput.end).format("YYYY-MM-DD HH:mm:ss");

  }

  deletePayroll() {
    let payroll_id = this.id;

    this._payroll_service.archivePayroll(payroll_id)
    .subscribe(data => {
      this.payroll_list_rec = Array.from(data);
      this._payroll_service.archivePayrollList(payroll_id).subscribe(data=>{});
      this._rt.navigate(['/admin/payroll']);
    }); 
  }


  sendPayslip() {
    this.isLoading = true;
    let payroll_id = this.id;

    this._payroll_service.sendPayslip(payroll_id)
    .subscribe(data => {
        this.isLoading = false;
        alert('Success!');
    }); 
  }


  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'Something went wrong';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }
  }

  ngOnDestroy() {
      this.body.classList.remove("skin-blue");
      this.body.classList.remove("sidebar-mini");
  }

  ngAfterViewInit(): void {
      this.dtTrigger.next();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  runtypeChange(value) {
    this.run_type = value;
    this.selectedDepartment = '';
    this.selectedPosition = '';
    if (value == 1) {
      this.company = true;
      this.branch = true;
      this.employee = false;
      this.position = false;
      this.department = false;
    } else if (value == 2) {
      this.department = true;
      this.position = false;
      this.company = true;
      this.branch = true;
      this.employee = false;
    } else if (value == 3) {
      this.position = true;
      this.department = false;
      this.company = true;
      this.employee = false;
      this.branch = true;

    } else {
      this.position = false;
      this.department = false;
      this.company = false;
      this.employee = false;
      this.branch = false;
      this.selectedCompany = '';
      this.selectedDepartment = '';
      this.selectedPosition = '';
      this.selectedBranch = '';
    }
  }

 generateBilling() {
     this.payroll_id = this.id;
     this._payroll_service.setId(this.payroll_id);
     this._rt.navigate(['admin/payroll/billing/', this.payroll_id]);
 }

 generatePayslip() {
    let disposable = this.modalService.addDialog(PayrollPayslipComponent, {
            title:'Generate Payslip',
            id:this.id,
            start : this.start_time,
            end   : this.end_time
          }).subscribe((isConfirmed)=>{});
    }

 getList() {

    let companies = this._company_serv.getCompanys()
        .subscribe(
          data => {
            this.company_rec = Array.from(data);
          },
          err => this.catchError(err)
        );
   let department = this._department_serv.getDepartments()
        .subscribe(
          data => {
            this.department_rec = Array.from(data);
          },
          err => this.catchError(err)
        );
   let position = this._position_serv.getPositions()
        .subscribe(
          data => {
            this.position_rec = Array.from(data);
          },
          err => this.catchError(err)
        );
   let adjustment = this._ajd_type.getAdjustmentsType()
        .subscribe(
           data => {
             this.adj_rec = Array.from(data);
           },
           err => this.catchError(err)
        );

 }

 
 viewLoan(id) {
   let disposable = this.modalService.addDialog(LoanListModalComponent, {
      att_id:id,
      start : this.start_time,
      end   : this.end_time
      }).subscribe((isConfirmed)=>{
        if (isConfirmed) {
          this.rerender();
        }
    });
 }
 
 viewAdjustment(id, type, no_of_days) {
    let disposable = this.modalService.addDialog(AdjustmentListModalComponent, {
      att_id: id,
      no_of_days:no_of_days,
      start : this.start_time,
      end   : this.end_time,
      type  : type
      }).subscribe((isConfirmed)=>{
        if (isConfirmed) {
          this.rerender();
        }
    });
 }

 updateAllowance(id) {
   let disposable = this.modalService.addDialog(PayrollAllowanceModalComponent, {
     emp_id: id
   }).subscribe((isConfirmed)=>{
      if (isConfirmed) {
        this.rerender();
      }
   });
 
 }

 total(data) {
      if (data != '' && this.payroll_rec.length != 'undefined') {
      let payroll_len = this.payroll_rec.length; this.total_thirteen_m = 0; this.total_d_cola = 0; this.allowance = 0; this.total_allowance = 0; this.total_d_rate = 0; this.total_basic_pay = 0; this.total_ecola = 0; this.total_late = 0; this.total_undertime = 0; this.total_pay_reg_ot = 0; this.total_pay_restday = 0; this.total_pay_rd_ot = 0; this.total_pay_special_hol = 0; this.total_pay_special_hol_ot = 0; this.total_pay_special_hol_rd = 0; this.total_pay_special_hol_rd_ot = 0; this.total_pay_legal_hol = 0; this.total_pay_legal_hol_ot = 0; this.total_pay_legal_hol_restday = 0; this.total_pay_legal_hol_rd_ot = 0; this.total_pay_double_hol = 0; this.total_pay_dbl_hol_ot = 0; this.total_pay_double_hol_rd = 0; this.total_pay_dbl_hol_rd_ot = 0; this.total_pay_reg_nd = 0;  this.total_pay_rd_nd = 0;  this.total_pay_special_hol_nd = 0;  this.total_pay_special_hol_rd_nd = 0;  this.total_pay_legal_hol_nd = 0;  this.total_pay_legal_hol_rd_nd = 0;  this.total_pay_dbl_hol_nd = 0;  this.total_pay_dbl_hol_rd_nd = 0;  this.total_adjustment = 0; this.total_deduction = 0;  this.total_loan = 0;  this.total_sss = 0;  this.total_philhealth = 0; this.total_pagibig = 0; this.total_gross_pay = 0; this.total_net_pay = 0;
      for (let x = 0; x < payroll_len; x++) {
        this.total_thirteen_m = this.total_thirteen_m + this.payroll_rec[x].thirteen_month * 1;
        this.total_d_cola = this.total_d_cola + this.payroll_rec[x].cola * 1;
        this.total_d_rate = this.total_d_rate + this.payroll_rec[x].daily_rate * 1;
        this.total_basic_pay = this.total_basic_pay + this.payroll_rec[x].ttl_basic_pay * 1;
        this.allowance = this.allowance + this.payroll_rec[x].allowance * 1;
        this.total_allowance = this.total_allowance + this.payroll_rec[x].total_allowance * 1;
        this.total_ecola = this.total_ecola + this.payroll_rec[x].ecola * 1;
        this.total_late = this.total_late + this.payroll_rec[x].late_amount * 1;  
        this.total_undertime = this.total_undertime + this.payroll_rec[x].undertime_amount * 1;
        this.total_pay_reg_ot = this.total_pay_reg_ot + this.payroll_rec[x].pay_reg_ot * 1;
        this.total_pay_restday = this.total_pay_restday + this.payroll_rec[x].pay_restday * 1;
        this.total_pay_rd_ot = this.total_pay_rd_ot + this.payroll_rec[x].pay_rd_ot * 1;
        this.total_pay_special_hol = this.total_pay_special_hol + this.payroll_rec[x].pay_special_hol * 1;
        this.total_pay_special_hol_ot = this.total_pay_special_hol_ot + this.payroll_rec[x].pay_special_hol_ot * 1;
        this.total_pay_special_hol_rd = this.total_pay_special_hol_rd + this.payroll_rec[x].pay_special_hol_rd * 1;
        this.total_pay_special_hol_rd_ot = this.total_pay_special_hol_rd_ot + this.payroll_rec[x].pay_special_hol_rd_ot * 1;
        this.total_pay_legal_hol = this.total_pay_legal_hol + this.payroll_rec[x].pay_legal_hol * 1;
        this.total_pay_legal_hol_ot = this.total_pay_legal_hol_ot + this.payroll_rec[x].pay_legal_hol_ot * 1;
        this.total_pay_legal_hol_restday = this.total_pay_legal_hol_restday + this.payroll_rec[x].pay_legal_hol_restday * 1;
        this.total_pay_legal_hol_rd_ot = this.total_pay_legal_hol_rd_ot + this.payroll_rec[x].pay_legal_hol_rd_ot * 1;
        this.total_pay_double_hol = this.total_pay_double_hol + this.payroll_rec[x].pay_double_hol * 1;
        this.total_pay_dbl_hol_ot = this.total_pay_dbl_hol_ot + this.payroll_rec[x].pay_dbl_hol_ot * 1;
        this.total_pay_double_hol_rd = this.total_pay_double_hol_rd + this.payroll_rec[x].pay_double_hol_rd * 1;
        this.total_pay_dbl_hol_rd_ot = this.total_pay_dbl_hol_rd_ot + this.payroll_rec[x].pay_dbl_hol_rd_ot * 1;
        this.total_pay_reg_nd = this.total_pay_reg_nd + this.payroll_rec[x].pay_reg_nd * 1;
        this.total_pay_rd_nd = this.total_pay_rd_nd + this.payroll_rec[x].pay_rd_nd * 1;
        this.total_pay_special_hol_nd = this.total_pay_special_hol_nd + this.payroll_rec[x].pay_special_hol_nd * 1;
        this.total_pay_special_hol_rd_nd = this.total_pay_special_hol_rd_nd + this.payroll_rec[x].pay_special_hol_rd_nd * 1;
        this.total_pay_legal_hol_nd = this.total_pay_legal_hol_nd + this.payroll_rec[x].pay_legal_hol_nd * 1;
        this.total_pay_legal_hol_rd_nd = this.total_pay_legal_hol_rd_nd + this.payroll_rec[x].pay_legal_hol_rd_nd * 1;
        this.total_pay_dbl_hol_nd = this.total_pay_dbl_hol_nd + this.payroll_rec[x].pay_dbl_hol_nd * 1;
        this.total_pay_dbl_hol_rd_nd = this.total_pay_dbl_hol_rd_nd + this.payroll_rec[x].pay_dbl_hol_rd_nd * 1;
        this.total_adjustment = this.total_adjustment + this.payroll_rec[x].total_adjustment * 1;
        this.total_deduction = this.total_deduction + this.payroll_rec[x].total_deduction * 1;
        this.total_loan = this.total_loan + this.payroll_rec[x].total_loan * 1;
        this.total_sss = this.total_sss + this.payroll_rec[x].total_sss * 1;
        this.total_philhealth = this.total_philhealth + this.payroll_rec[x].total_philhealth * 1;
        this.total_pagibig = this.total_pagibig + this.payroll_rec[x].total_pagibig * 1;
        this.total_gross_pay = this.total_gross_pay + this.payroll_rec[x].gross_pay * 1;
        this.total_net_pay = this.total_net_pay + this.payroll_rec[x].net_pay * 1;
      }
    }
  }

 

}