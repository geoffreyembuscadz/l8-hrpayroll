var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { DepartmentService } from '../../services/department.service';
import { PayrollService } from '../../services/payroll.service';
import { PositionService } from '../../services/position.service';
import { AuthUserService } from '../../services/auth-user.service';
import { AdjustmentListModalComponent } from '../adjustment/adjustment-list-modal.component';
import { LoanListModalComponent } from '../loan/loan-list-modal.component';
var PayrollBillingComponent = /** @class */ (function () {
    function PayrollBillingComponent(_conf, _payroll_service, modalService, _rt, _fb, _ar, daterangepickerOptions, _company_serv, _emp_service, _department_serv, _position_serv, _ajd_type, _auth_service) {
        this._conf = _conf;
        this._payroll_service = _payroll_service;
        this.modalService = modalService;
        this._rt = _rt;
        this._fb = _fb;
        this._ar = _ar;
        this.daterangepickerOptions = daterangepickerOptions;
        this._company_serv = _company_serv;
        this._emp_service = _emp_service;
        this._department_serv = _department_serv;
        this._position_serv = _position_serv;
        this._ajd_type = _ajd_type;
        this._auth_service = _auth_service;
        this.dtTrigger = new Subject();
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.ismeridian = true;
        this.start_date = new Date();
        this.end_date = new Date();
        this.department = false;
        this.position = false;
        this.company = false;
        this.employee = false;
        this.enableDepartment = false;
        this.enablePosition = false;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.id = this._payroll_service.getId();
    }
    PayrollBillingComponent.prototype.ngOnInit = function () {
        var _this = this;
        var start = this.start_time;
        var end = this.end_time;
        var company = this.selectedCompany;
        var branch = this.selectedBranch;
        var department = this.selectedDepartment;
        var position = this.selectedPosition;
        var run_type = this.run_type;
        var cutoff = this.cutoff;
        if (this.branch_location != 0) {
            var payroll_list_approved = this._payroll_service.getPayrollArchieve(this.id)
                .subscribe(function (data) {
                _this.payroll_rec = data;
                _this.total(data);
                _this.rerender();
            });
        }
        else {
            var billing = this._payroll_service
                .getPayrollBilling(start, end, run_type, company, branch, department, position, cutoff, this.branch_location)
                .subscribe(function (data) {
                _this.payroll_rec = data;
                _this.total(data);
                _this.rerender();
            });
        }
        this.dtOptions = {
            "aLengthMenu": [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
            Sort: true,
            dom: '<"row"<"col-md-12"l>>Bfrtip',
            buttons: [
                {
                    orientation: 'portrait',
                    pageSize: 'LEGAL',
                    extend: 'excelHtml5',
                    footer: true,
                },
                'colvis'
            ],
            "scrollX": true,
            "fixedColumns": {
                "leftColumns": 3
            }
        };
    };
    PayrollBillingComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_time = moment(dateInput.start).format("YYYY-MM-DD HH:mm:ss");
        this.end_time = moment(dateInput.end).format("YYYY-MM-DD HH:mm:ss");
    };
    PayrollBillingComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    PayrollBillingComponent.prototype.billingByLocation = function () {
    };
    PayrollBillingComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    PayrollBillingComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    PayrollBillingComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    PayrollBillingComponent.prototype.viewLoan = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanListModalComponent, {
            att_id: id,
            start: this.start_time,
            end: this.end_time
        }).subscribe(function (isConfirmed) {
            if (isConfirmed) {
                _this.rerender();
            }
        });
    };
    PayrollBillingComponent.prototype.viewAdjustment = function (id, type, no_of_days) {
        var _this = this;
        var disposable = this.modalService.addDialog(AdjustmentListModalComponent, {
            att_id: id,
            no_of_days: no_of_days,
            start: this.start_time,
            end: this.end_time,
            type: type
        }).subscribe(function (isConfirmed) {
            if (isConfirmed) {
                _this.rerender();
            }
        });
    };
    PayrollBillingComponent.prototype.createSOA = function () {
        this._payroll_service.generateSOA(this.total_billable, this.total_asf, this.vat, this.total_mandatory, this.total_admin_fee, this.total_gross_pay, this.id_payroll).subscribe(function (data) {
            window.open(data.url, "_blank");
        });
    };
    PayrollBillingComponent.prototype.total = function (data) {
        if (data != '' && this.payroll_rec.length != 'undefined') {
            var payroll_len = this.payroll_rec.length;
            this.total_bill_rate = 0;
            this.total_asf = 0;
            this.total_allowance = 0;
            this.allowance = 0;
            this.total_thirteen_m = 0;
            this.total_d_cola = 0;
            this.total_d_rate = 0;
            this.total_basic_pay = 0;
            this.total_ecola = 0;
            this.total_late = 0;
            this.total_undertime = 0;
            this.total_pay_reg_ot = 0;
            this.total_pay_restday = 0;
            this.total_pay_rd_ot = 0;
            this.total_pay_special_hol = 0;
            this.total_pay_special_hol_ot = 0;
            this.total_pay_special_hol_rd = 0;
            this.total_pay_special_hol_rd_ot = 0;
            this.total_pay_legal_hol = 0;
            this.total_pay_legal_hol_ot = 0;
            this.total_pay_legal_hol_restday = 0;
            this.total_pay_legal_hol_rd_ot = 0;
            this.total_pay_double_hol = 0;
            this.total_pay_dbl_hol_ot = 0;
            this.total_pay_double_hol_rd = 0;
            this.total_pay_dbl_hol_rd_ot = 0;
            this.total_pay_reg_nd = 0;
            this.total_pay_rd_nd = 0;
            this.total_pay_special_hol_nd = 0;
            this.total_pay_special_hol_rd_nd = 0;
            this.total_pay_legal_hol_nd = 0;
            this.total_pay_legal_hol_rd_nd = 0;
            this.total_pay_dbl_hol_nd = 0;
            this.total_pay_dbl_hol_rd_nd = 0;
            this.total_adjustment = 0;
            this.total_deduction = 0;
            this.total_loan = 0;
            this.total_sss = 0;
            this.total_sss_employer = 0;
            this.total_philhealth = 0;
            this.total_pagibig = 0;
            this.ecc = 0;
            this.total_mandatory = 0;
            this.total_admin_fee = 0;
            this.total_billable = 0;
            this.vat = 0;
            this.total_gross_pay = 0;
            this.total_net_pay = 0;
            for (var x = 0; x < payroll_len; x++) {
                this.total_bill_rate = this.total_bill_rate + this.payroll_rec[x].bill_rate * 1;
                this.total_asf = this.total_asf + this.payroll_rec[x].asf * 1;
                this.total_thirteen_m = this.total_thirteen_m + this.payroll_rec[x].thirteen_month * 1;
                this.total_d_cola = this.total_d_cola + this.payroll_rec[x].cola * 1;
                this.total_d_rate = this.total_d_rate + this.payroll_rec[x].daily_rate * 1;
                this.total_basic_pay = this.total_basic_pay + this.payroll_rec[x].ttl_basic_pay_billing * 1;
                this.allowance = this.allowance + this.payroll_rec[x].allowance * 1;
                this.total_allowance = this.total_allowance + this.payroll_rec[x].total_allowance * 1;
                this.total_ecola = this.total_ecola + this.payroll_rec[x].ecola * 1;
                this.total_late = this.total_late + this.payroll_rec[x].late_amount * 1;
                this.total_undertime = this.total_undertime + this.payroll_rec[x].undertime_amount * 1;
                this.total_pay_reg_ot = this.total_pay_reg_ot + this.payroll_rec[x].pay_reg_ot * 1;
                this.total_pay_restday = this.total_pay_restday + this.payroll_rec[x].pay_restday * 1;
                this.total_pay_rd_ot = this.total_pay_rd_ot + this.payroll_rec[x].pay_rd_ot * 1;
                this.total_pay_special_hol = this.total_pay_special_hol + this.payroll_rec[x].pay_special_hol * 1;
                this.total_pay_special_hol_ot = this.total_pay_special_hol_ot + this.payroll_rec[x].pay_special_hol_ot * 1;
                this.total_pay_special_hol_rd = this.total_pay_special_hol_rd + this.payroll_rec[x].pay_special_hol_rd * 1;
                this.total_pay_special_hol_rd_ot = this.total_pay_special_hol_rd_ot + this.payroll_rec[x].pay_special_hol_rd_ot * 1;
                this.total_pay_legal_hol = this.total_pay_legal_hol + this.payroll_rec[x].pay_legal_hol * 1;
                this.total_pay_legal_hol_ot = this.total_pay_legal_hol_ot + this.payroll_rec[x].pay_legal_hol_ot * 1;
                this.total_pay_legal_hol_restday = this.total_pay_legal_hol_restday + this.payroll_rec[x].pay_legal_hol_restday * 1;
                this.total_pay_legal_hol_rd_ot = this.total_pay_legal_hol_rd_ot + this.payroll_rec[x].pay_legal_hol_rd_ot * 1;
                this.total_pay_double_hol = this.total_pay_double_hol + this.payroll_rec[x].pay_double_hol * 1;
                this.total_pay_dbl_hol_ot = this.total_pay_dbl_hol_ot + this.payroll_rec[x].pay_dbl_hol_ot * 1;
                this.total_pay_double_hol_rd = this.total_pay_double_hol_rd + this.payroll_rec[x].pay_double_hol_rd * 1;
                this.total_pay_dbl_hol_rd_ot = this.total_pay_dbl_hol_rd_ot + this.payroll_rec[x].pay_dbl_hol_rd_ot * 1;
                this.total_pay_reg_nd = this.total_pay_reg_nd + this.payroll_rec[x].pay_reg_nd * 1;
                this.total_pay_rd_nd = this.total_pay_rd_nd + this.payroll_rec[x].pay_rd_nd * 1;
                this.total_pay_special_hol_nd = this.total_pay_special_hol_nd + this.payroll_rec[x].pay_special_hol_nd * 1;
                this.total_pay_special_hol_rd_nd = this.total_pay_special_hol_rd_nd + this.payroll_rec[x].pay_special_hol_rd_nd * 1;
                this.total_pay_legal_hol_nd = this.total_pay_legal_hol_nd + this.payroll_rec[x].pay_legal_hol_nd * 1;
                this.total_pay_legal_hol_rd_nd = this.total_pay_legal_hol_rd_nd + this.payroll_rec[x].pay_legal_hol_rd_nd * 1;
                this.total_pay_dbl_hol_nd = this.total_pay_dbl_hol_nd + this.payroll_rec[x].pay_dbl_hol_nd * 1;
                this.total_pay_dbl_hol_rd_nd = this.total_pay_dbl_hol_rd_nd + this.payroll_rec[x].pay_dbl_hol_rd_nd * 1;
                this.total_adjustment = this.total_adjustment + this.payroll_rec[x].total_adjustment * 1;
                this.total_deduction = this.total_deduction + this.payroll_rec[x].total_deduction * 1;
                this.total_loan = this.total_loan + this.payroll_rec[x].total_loan * 1;
                this.total_admin_fee = this.total_admin_fee + this.payroll_rec[x].admin_fee * 1;
                this.total_sss = this.total_sss + this.payroll_rec[x].total_sss_employer * 1;
                this.total_sss_employer = this.total_sss + this.payroll_rec[x].total_sss_employer * 1;
                this.total_philhealth = this.total_philhealth + this.payroll_rec[x].total_philhealth_employer * 1;
                this.total_pagibig = this.total_pagibig + this.payroll_rec[x].total_pagibig_employer * 1;
                this.ecc = this.ecc + this.payroll_rec[x].ecc * 1;
                this.total_mandatory = this.total_mandatory + this.payroll_rec[x].total_mandatory * 1;
                this.total_billable = this.total_billable + this.payroll_rec[x].total_billable * 1;
                this.vat = this.vat + this.payroll_rec[x].vat * 1;
                this.total_gross_pay = this.total_gross_pay + this.payroll_rec[x].gross_pay * 1;
                this.total_net_pay = this.total_net_pay + this.payroll_rec[x].net_pay * 1;
                this.id_payroll = this.payroll_rec[x].payroll_id;
            }
        }
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], PayrollBillingComponent.prototype, "dtElement", void 0);
    PayrollBillingComponent = __decorate([
        Component({
            selector: 'admin-payroll-create',
            templateUrl: './payroll-billing.component.html',
            styleUrls: ['./payroll.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration,
            PayrollService,
            DialogService,
            Router,
            FormBuilder,
            ActivatedRoute,
            DaterangepickerConfig,
            CompanyService,
            EmployeeService,
            DepartmentService,
            PositionService,
            AdjustmentTypeService,
            AuthUserService])
    ], PayrollBillingComponent);
    return PayrollBillingComponent;
}());
export { PayrollBillingComponent };
//# sourceMappingURL=payroll-billing.component.js.map