import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';

import { PayrollService } from '../../services/payroll.service';

@Component({
  selector: 'admin-payroll-payslip',
  templateUrl: './payroll-payslip.component.html'
  // styleUrls: ['./role-edit.component.css']
})
export class PayrollPayslipComponent extends DialogComponent<null, boolean> {

  public success_title;
  public success_message;
  public poststore: any;
  private error_title: string;
  private error_message: string;

  id: any;
  start:any;
  end:any

  generatePayroll : FormGroup;

  constructor(private _rt: Router, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute, private _payroll_serv: PayrollService) {
    super(dialogService);

    this.generatePayroll = _fb.group({
      'payroll_id': [''],
      'company': [''],
      'branch': [''],
      'start': [''],
      'end' : [''],
    });  
  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'Role name already exist.';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }
  }

  private clearErrorMsg(){
    this.error_title = '';
    this.error_message = '';
  }

  ngOnInit() {
   
  }

  createPayslip() {
    let company = this.generatePayroll.value.company;
    let branch = this.generatePayroll.value.branch;

    console.log(branch);
    this._payroll_serv.generatePayslip(this.id, company, branch, this.start, this.end).subscribe(data => {
          window.open(data.url, "_blank");
          this.close();
    });
  }
}