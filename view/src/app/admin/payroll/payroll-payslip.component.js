var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PayrollService } from '../../services/payroll.service';
var PayrollPayslipComponent = /** @class */ (function (_super) {
    __extends(PayrollPayslipComponent, _super);
    function PayrollPayslipComponent(_rt, dialogService, _fb, _ar, _payroll_serv) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._payroll_serv = _payroll_serv;
        _this.generatePayroll = _fb.group({
            'payroll_id': [''],
            'company': [''],
            'branch': [''],
            'start': [''],
            'end': [''],
        });
        return _this;
    }
    PayrollPayslipComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Role name already exist.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    PayrollPayslipComponent.prototype.clearErrorMsg = function () {
        this.error_title = '';
        this.error_message = '';
    };
    PayrollPayslipComponent.prototype.ngOnInit = function () {
    };
    PayrollPayslipComponent.prototype.createPayslip = function () {
        var _this = this;
        var company = this.generatePayroll.value.company;
        var branch = this.generatePayroll.value.branch;
        console.log(branch);
        this._payroll_serv.generatePayslip(this.id, company, branch, this.start, this.end).subscribe(function (data) {
            window.open(data.url, "_blank");
            _this.close();
        });
    };
    PayrollPayslipComponent = __decorate([
        Component({
            selector: 'admin-payroll-payslip',
            templateUrl: './payroll-payslip.component.html'
            // styleUrls: ['./role-edit.component.css']
        }),
        __metadata("design:paramtypes", [Router, DialogService, FormBuilder, ActivatedRoute, PayrollService])
    ], PayrollPayslipComponent);
    return PayrollPayslipComponent;
}(DialogComponent));
export { PayrollPayslipComponent };
//# sourceMappingURL=payroll-payslip.component.js.map