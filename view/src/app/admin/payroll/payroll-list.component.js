var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute } from '@angular/router';
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
// import { PayrollCreateModalComponent } from './payroll-generate.component';
// import { PayrollEditComponent } from './payroll-edit.component';
import { PayrollService } from '../../services/payroll.service';
var PayrollListComponent = /** @class */ (function () {
    function PayrollListComponent(_conf, _payroll_service, modalService, _rt, _ar) {
        this._conf = _conf;
        this._payroll_service = _payroll_service;
        this.modalService = modalService;
        this._rt = _rt;
        this._ar = _ar;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    PayrollListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var user = this._payroll_service.getPayrolls().
            subscribe(function (data) {
            _this.payroll_id = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
        this.dtOptions = {
            order: [[6, 'desc']],
            "columnDefs": [
                { "width": "20%", "targets": [0, 6] },
                { "width": "15%", "targets": [1, 5] },
                { "width": "10%", "targets": [2, 3, 4] },
            ],
            "responsive": true
        };
    };
    PayrollListComponent.prototype.clickRow = function (id) {
        this._payroll_service.setId(id);
        this._rt.navigate(['admin/payroll/', id]);
    };
    PayrollListComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    PayrollListComponent.prototype.generatePayroll = function () {
        this._rt.navigate(['/admin/payroll-generate']);
    };
    PayrollListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    PayrollListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    PayrollListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], PayrollListComponent.prototype, "dtElement", void 0);
    PayrollListComponent = __decorate([
        Component({
            selector: 'admin-payroll-list',
            templateUrl: './payroll-list.component.html'
            // styleUrls: ['./payroll-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration,
            PayrollService,
            DialogService,
            Router,
            ActivatedRoute])
    ], PayrollListComponent);
    return PayrollListComponent;
}());
export { PayrollListComponent };
//# sourceMappingURL=payroll-list.component.js.map