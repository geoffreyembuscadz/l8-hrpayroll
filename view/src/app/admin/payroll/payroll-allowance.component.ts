import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';


import { PayrollService } from '../../services/payroll.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'admin-payroll-allowance-list',
  templateUrl: './payroll-allowance.component.html',
  styleUrls: ['./payroll.component.css']
})

@Injectable()
export class PayrollAllowanceModalComponent extends DialogComponent<null, boolean> implements OnInit, AfterViewInit {

  @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();


  updateAllowanceForm : FormGroup;

  public error_title: string;
  public error_message: string;

  public success_title;
  public success_message;
  public poststore: any;

  emp_id: any;
  dtOptions: any = {};
  bodyClasses:string = "skin-blue sidebar-mini";
  body = document.getElementsByTagName('body')[0];

  constructor(
    private _rt: Router, dialogService: DialogService, 
    private _conf: Configuration, 
    private modalService: DialogService,
    private _payrollServ: PayrollService,
    private _fb: FormBuilder,
    ){
    super(dialogService);

    this.updateAllowanceForm = _fb.group({
      'amount': ['', [Validators.required]]
    })

  }


  public validateUpdate() {

    let allowance = this.updateAllowanceForm.value;

    this._payrollServ.updateAllowance(this.emp_id, allowance)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The allowance is successfully updated.";
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/payroll-generate']);

      },
      err => this.catchError(err)
    );
  }


  ngOnInit() {


    //add the the body classes
      this.body.classList.add("skin-blue");
      this.body.classList.add("sidebar-mini");

    
  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'Something went wrong';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }
  }

  onlyDecimalNumberKey(event) {
      let charCode = (event.which) ? event.which : event.keyCode;
      if (charCode != 46 && charCode > 31
          && (charCode < 48 || charCode > 57))
          return false;
      return true;
  }

  ngOnDestroy() {
      
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }
}
