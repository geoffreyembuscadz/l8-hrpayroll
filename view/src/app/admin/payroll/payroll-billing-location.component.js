var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { DepartmentService } from '../../services/department.service';
import { PayrollService } from '../../services/payroll.service';
import { PositionService } from '../../services/position.service';
import { AuthUserService } from '../../services/auth-user.service';
import { AdjustmentListModalComponent } from '../adjustment/adjustment-list-modal.component';
import { LoanListModalComponent } from '../loan/loan-list-modal.component';
var PayrollBillingLocationComponent = /** @class */ (function () {
    function PayrollBillingLocationComponent(_conf, _payroll_service, modalService, _rt, _fb, _ar, daterangepickerOptions, _company_serv, _emp_service, _department_serv, _position_serv, _ajd_type, _auth_service) {
        this._conf = _conf;
        this._payroll_service = _payroll_service;
        this.modalService = modalService;
        this._rt = _rt;
        this._fb = _fb;
        this._ar = _ar;
        this.daterangepickerOptions = daterangepickerOptions;
        this._company_serv = _company_serv;
        this._emp_service = _emp_service;
        this._department_serv = _department_serv;
        this._position_serv = _position_serv;
        this._ajd_type = _ajd_type;
        this._auth_service = _auth_service;
        this.dtTrigger = new Subject();
        this.mainInput = {
            start: moment(),
            end: moment()
        };
        this.ismeridian = true;
        this.start_date = new Date();
        this.end_date = new Date();
        this.department = false;
        this.position = false;
        this.company = false;
        this.branch = false;
        this.employee = false;
        this.enableDepartment = false;
        this.enablePosition = false;
        this.enableBranch = false;
        this.selectedCompany = "";
        this.selectedDepartment = "";
        this.selectedPosition = "";
        this.selectedBranch = "";
        this.branch_location = "";
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.isLoading = false;
        this.isGenerated = false;
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.createPayrollForm = _fb.group({
            'start_time': [null],
            'end_time': [null],
            'date': [null],
            'type_of_run': [''],
            'employee_id': [null],
            'company': [''],
            'branch': [''],
            'department': [''],
            'position': [''],
            'prepared_by': [null],
            'payroll_id': [null],
            'approved': [null],
            'approved_by': [null],
            'created_by': [null],
            'cutoff': [null, [Validators.required]],
            'branch_location': [null, [Validators.required]]
        });
    }
    PayrollBillingLocationComponent.prototype.ngOnInit = function () {
        var _this = this;
        var role_model = this.createPayrollForm.value;
        var start = this.start_time;
        var end = this.end_time;
        var company = this.selectedCompany;
        var branch = this.selectedBranch;
        var department = this.selectedDepartment;
        var position = this.selectedPosition;
        var run_type = this.run_type;
        var cutoff = this.createPayrollForm.value.cutoff;
        var branch_location = this.createPayrollForm.value.branch_location;
        this.getList();
        this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY h:mm A' },
            alwaysShowCalendars: false,
            timePicker: true
        };
        this.dtOptions = {
            "aLengthMenu": [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "All"]],
            Sort: true,
            dom: '<"row"<"col-md-12"l>>Bfrtip',
            buttons: [
                {
                    orientation: 'portrait',
                    pageSize: 'LEGAL',
                    extend: 'excelHtml5',
                    footer: true,
                },
                'colvis'
            ],
            "scrollX": true,
            "fixedColumns": {
                "leftColumns": 1
            }
        };
        var payroll_list = this._payroll_service.getPayrollBilling(start, end, run_type, company, branch, department, position, cutoff, branch_location)
            .subscribe(function (data) {
            _this.payroll_rec = Array.from(data);
            if (data != '' && _this.payroll_rec.length != 'undefined') {
                var payroll_len = _this.payroll_rec.length;
                _this.total_bill_rate = 0;
                _this.total_admin_fee = 0;
                _this.total_asf = 0;
                _this.total_allowance = 0;
                _this.allowance = 0;
                _this.total_thirteen_m = 0;
                _this.total_d_cola = 0;
                _this.total_d_rate = 0;
                _this.total_basic_pay = 0;
                _this.total_ecola = 0;
                _this.total_late = 0;
                _this.total_undertime = 0;
                _this.total_pay_reg_ot = 0;
                _this.total_pay_restday = 0;
                _this.total_pay_rd_ot = 0;
                _this.total_pay_special_hol = 0;
                _this.total_pay_special_hol_ot = 0;
                _this.total_pay_special_hol_rd = 0;
                _this.total_pay_special_hol_rd_ot = 0;
                _this.total_pay_legal_hol = 0;
                _this.total_pay_legal_hol_ot = 0;
                _this.total_pay_legal_hol_restday = 0;
                _this.total_pay_legal_hol_rd_ot = 0;
                _this.total_pay_double_hol = 0;
                _this.total_pay_dbl_hol_ot = 0;
                _this.total_pay_double_hol_rd = 0;
                _this.total_pay_dbl_hol_rd_ot = 0;
                _this.total_pay_reg_nd = 0;
                _this.total_pay_rd_nd = 0;
                _this.total_pay_special_hol_nd = 0;
                _this.total_pay_special_hol_rd_nd = 0;
                _this.total_pay_legal_hol_nd = 0;
                _this.total_pay_legal_hol_rd_nd = 0;
                _this.total_pay_dbl_hol_nd = 0;
                _this.total_pay_dbl_hol_rd_nd = 0;
                _this.total_adjustment = 0;
                _this.total_deduction = 0;
                _this.total_loan = 0;
                _this.total_sss_employer = 0;
                _this.total_sss = 0;
                _this.total_philhealth = 0;
                _this.total_pagibig = 0;
                _this.total_gross_pay = 0;
                _this.total_net_pay = 0;
                _this.ecc = 0;
                _this.total_mandatory = 0;
                _this.total_billable = 0;
                _this.vat = 0;
                for (var x = 0; x < payroll_len; x++) {
                    _this.total_bill_rate = _this.total_bill_rate + _this.payroll_rec[x].bill_rate * 1;
                    _this.total_asf = _this.total_asf + _this.payroll_rec[x].asf * 1;
                    _this.total_thirteen_m = _this.total_thirteen_m + _this.payroll_rec[x].thirteen_month * 1;
                    _this.total_d_cola = _this.total_d_cola + _this.payroll_rec[x].cola * 1;
                    _this.total_d_rate = _this.total_d_rate + _this.payroll_rec[x].daily_rate * 1;
                    _this.total_basic_pay = _this.total_basic_pay + _this.payroll_rec[x].ttl_basic_pay_billing * 1;
                    _this.allowance = _this.allowance + _this.payroll_rec[x].allowance * 1;
                    _this.total_allowance = _this.total_allowance + _this.payroll_rec[x].total_allowance * 1;
                    _this.total_ecola = _this.total_ecola + _this.payroll_rec[x].ecola * 1;
                    _this.total_late = _this.total_late + _this.payroll_rec[x].late_amount * 1;
                    _this.total_undertime = _this.total_undertime + _this.payroll_rec[x].undertime_amount * 1;
                    _this.total_pay_reg_ot = _this.total_pay_reg_ot + _this.payroll_rec[x].pay_reg_ot * 1;
                    _this.total_pay_restday = _this.total_pay_restday + _this.payroll_rec[x].pay_restday * 1;
                    _this.total_pay_rd_ot = _this.total_pay_rd_ot + _this.payroll_rec[x].pay_rd_ot * 1;
                    _this.total_pay_special_hol = _this.total_pay_special_hol + _this.payroll_rec[x].pay_special_hol * 1;
                    _this.total_pay_special_hol_ot = _this.total_pay_special_hol_ot + _this.payroll_rec[x].pay_special_hol_ot * 1;
                    _this.total_pay_special_hol_rd = _this.total_pay_special_hol_rd + _this.payroll_rec[x].pay_special_hol_rd * 1;
                    _this.total_pay_special_hol_rd_ot = _this.total_pay_special_hol_rd_ot + _this.payroll_rec[x].pay_special_hol_rd_ot * 1;
                    _this.total_pay_legal_hol = _this.total_pay_legal_hol + _this.payroll_rec[x].pay_legal_hol * 1;
                    _this.total_pay_legal_hol_ot = _this.total_pay_legal_hol_ot + _this.payroll_rec[x].pay_legal_hol_ot * 1;
                    _this.total_pay_legal_hol_restday = _this.total_pay_legal_hol_restday + _this.payroll_rec[x].pay_legal_hol_restday * 1;
                    _this.total_pay_legal_hol_rd_ot = _this.total_pay_legal_hol_rd_ot + _this.payroll_rec[x].pay_legal_hol_rd_ot * 1;
                    _this.total_pay_double_hol = _this.total_pay_double_hol + _this.payroll_rec[x].pay_double_hol * 1;
                    _this.total_pay_dbl_hol_ot = _this.total_pay_dbl_hol_ot + _this.payroll_rec[x].pay_dbl_hol_ot * 1;
                    _this.total_pay_double_hol_rd = _this.total_pay_double_hol_rd + _this.payroll_rec[x].pay_double_hol_rd * 1;
                    _this.total_pay_dbl_hol_rd_ot = _this.total_pay_dbl_hol_rd_ot + _this.payroll_rec[x].pay_dbl_hol_rd_ot * 1;
                    _this.total_pay_reg_nd = _this.total_pay_reg_nd + _this.payroll_rec[x].pay_reg_nd * 1;
                    _this.total_pay_rd_nd = _this.total_pay_rd_nd + _this.payroll_rec[x].pay_rd_nd * 1;
                    _this.total_pay_special_hol_nd = _this.total_pay_special_hol_nd + _this.payroll_rec[x].pay_special_hol_nd * 1;
                    _this.total_pay_special_hol_rd_nd = _this.total_pay_special_hol_rd_nd + _this.payroll_rec[x].pay_special_hol_rd_nd * 1;
                    _this.total_pay_legal_hol_nd = _this.total_pay_legal_hol_nd + _this.payroll_rec[x].pay_legal_hol_nd * 1;
                    _this.total_pay_legal_hol_rd_nd = _this.total_pay_legal_hol_rd_nd + _this.payroll_rec[x].pay_legal_hol_rd_nd * 1;
                    _this.total_pay_dbl_hol_nd = _this.total_pay_dbl_hol_nd + _this.payroll_rec[x].pay_dbl_hol_nd * 1;
                    _this.total_pay_dbl_hol_rd_nd = _this.total_pay_dbl_hol_rd_nd + _this.payroll_rec[x].pay_dbl_hol_rd_nd * 1;
                    _this.total_adjustment = _this.total_adjustment + _this.payroll_rec[x].total_adjustment * 1;
                    _this.total_deduction = _this.total_deduction + _this.payroll_rec[x].total_deduction * 1;
                    _this.total_loan = _this.total_loan + _this.payroll_rec[x].total_loan * 1;
                    _this.total_sss_employer = _this.total_sss + _this.payroll_rec[x].total_sss_employer * 1;
                    _this.total_philhealth = _this.total_philhealth + _this.payroll_rec[x].total_philhealth_employer * 1;
                    _this.total_pagibig = _this.total_pagibig + _this.payroll_rec[x].total_pagibig_employer * 1;
                    _this.total_admin_fee = _this.total_admin_fee + _this.payroll_rec[x].admin_fee * 1;
                    _this.total_gross_pay = _this.total_gross_pay + _this.payroll_rec[x].gross_pay * 1;
                    _this.total_net_pay = _this.total_net_pay + _this.payroll_rec[x].net_pay * 1;
                    _this.ecc = _this.ecc + _this.payroll_rec[x].ecc * 1;
                    _this.total_mandatory = _this.total_mandatory + _this.payroll_rec[x].total_mandatory * 1;
                    _this.total_billable = _this.total_billable + _this.payroll_rec[x].total_billable * 1;
                    _this.vat = _this.vat + _this.payroll_rec[x].vat * 1;
                    _this.id_payroll = _this.payroll_rec[x].payroll_id;
                }
            }
            _this.rerender();
        });
        var created_by = this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
        }, function (err) { return console.error(err); });
    };
    PayrollBillingLocationComponent.prototype.createSOA = function () {
        this._payroll_service.generateSOAByLocation(this.total_billable, this.total_asf, this.vat, this.total_mandatory, this.total_admin_fee, this.branch_location, this.start_time, this.end_time, this.user_id).subscribe(function (data) {
            window.open(data.url, "_blank");
        });
    };
    PayrollBillingLocationComponent.prototype.onSubmit = function () {
        var _this = this;
        this.isLoading = true;
        var role_model = this.createPayrollForm.value;
        var start = this.start_time;
        var end = this.end_time;
        var company = this.selectedCompany;
        var department = this.selectedDepartment;
        var position = this.selectedPosition;
        var run_type = this.run_type;
        var cutoff = this.cutoff;
        var branch = '';
        var branch_location = this.branch_location;
        if (run_type == "undefined") {
            run_type = "0";
        }
        if (this.selectedBranch == '') {
            branch = 'undefined';
        }
        else {
            branch = this.selectedBranch;
        }
        this._payroll_service
            .getPayrollBilling(start, end, run_type, company, branch, department, position, cutoff, branch_location)
            .subscribe(function (data) {
            _this.ngOnInit();
            _this.isLoading = false;
            _this.isGenerated = true;
        }, function (err) { return _this.catchError(err); });
    };
    PayrollBillingLocationComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_time = moment(dateInput.start).format("YYYY-MM-DD HH:mm:ss");
        this.end_time = moment(dateInput.end).format("YYYY-MM-DD HH:mm:ss");
    };
    PayrollBillingLocationComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    PayrollBillingLocationComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    PayrollBillingLocationComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    PayrollBillingLocationComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    PayrollBillingLocationComponent.prototype.runtypeChange = function (value) {
        this.run_type = value;
        this.selectedDepartment = '';
        this.selectedPosition = '';
        if (value == 1) {
            this.company = true;
            this.branch = true;
            this.employee = false;
            this.position = false;
            this.department = false;
        }
        else if (value == 2) {
            this.department = true;
            this.position = false;
            this.company = true;
            this.branch = true;
            this.employee = false;
        }
        else if (value == 3) {
            this.position = true;
            this.department = false;
            this.company = true;
            this.employee = false;
            this.branch = true;
        }
        else {
            this.position = false;
            this.department = false;
            this.company = false;
            this.employee = false;
            this.branch = false;
            this.selectedCompany = '';
            this.selectedDepartment = '';
            this.selectedPosition = '';
            this.selectedBranch = '';
        }
    };
    PayrollBillingLocationComponent.prototype.payrollPeriodChange = function (value) {
        this.cutoff = value;
    };
    PayrollBillingLocationComponent.prototype.getList = function () {
        var _this = this;
        var companies = this._company_serv.getCompanys()
            .subscribe(function (data) {
            _this.company_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var department = this._department_serv.getDepartments()
            .subscribe(function (data) {
            _this.department_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var position = this._position_serv.getPositions()
            .subscribe(function (data) {
            _this.position_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var adjustment = this._ajd_type.getAdjustmentsType()
            .subscribe(function (data) {
            _this.adj_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var company_branch = this._company_serv.getCompanyBranch()
            .subscribe(function (data) {
            _this.company_branch_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var company_brancehes = this._company_serv.getCompanyBranches()
            .subscribe(function (data) {
            _this.company_branches = Array.from(data);
        }, function (err) { return _this.catchError(err); });
    };
    PayrollBillingLocationComponent.prototype.changed = function (data) {
        this.current = data.value;
    };
    PayrollBillingLocationComponent.prototype.getCompany = function (value, event) {
        this.enableDepartment = true;
        this.enablePosition = true;
        this.enableBranch = true;
        this.selectedCompany = value;
    };
    PayrollBillingLocationComponent.prototype.getDepartment = function (value, event) {
        if (value == '-none-') {
            this.selectedDepartment = '';
        }
        else {
            this.selectedDepartment = value;
        }
    };
    PayrollBillingLocationComponent.prototype.getPosition = function (value, event) {
        if (value == '-none-') {
            this.selectedPosition = '';
        }
        else {
            this.selectedPosition = value;
        }
    };
    PayrollBillingLocationComponent.prototype.getBranch = function (value, event) {
        if (value == '-none-') {
            this.selectedBranch = 'none';
        }
        else {
            this.selectedBranch = value;
        }
    };
    PayrollBillingLocationComponent.prototype.getBranchLocation = function (value, event) {
        if (value == '-none-') {
            this.branch_location = 'none';
        }
        else {
            this.branch_location = value;
        }
    };
    PayrollBillingLocationComponent.prototype.viewLoan = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanListModalComponent, {
            att_id: id,
            start: this.start_time,
            end: this.end_time
        }).subscribe(function (isConfirmed) {
            if (isConfirmed) {
                _this.rerender();
            }
        });
    };
    PayrollBillingLocationComponent.prototype.viewAdjustment = function (id, type, no_of_days) {
        var _this = this;
        var disposable = this.modalService.addDialog(AdjustmentListModalComponent, {
            att_id: id,
            no_of_days: no_of_days,
            start: this.start_time,
            end: this.end_time,
            type: type
        }).subscribe(function (isConfirmed) {
            if (isConfirmed) {
                _this.rerender();
            }
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], PayrollBillingLocationComponent.prototype, "dtElement", void 0);
    PayrollBillingLocationComponent = __decorate([
        Component({
            selector: 'admin-payroll-billing-location',
            templateUrl: './payroll-billing-location.component.html',
            styleUrls: ['./payroll.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration,
            PayrollService,
            DialogService,
            Router,
            FormBuilder,
            ActivatedRoute,
            DaterangepickerConfig,
            CompanyService,
            EmployeeService,
            DepartmentService,
            PositionService,
            AdjustmentTypeService,
            AuthUserService])
    ], PayrollBillingLocationComponent);
    return PayrollBillingLocationComponent;
}());
export { PayrollBillingLocationComponent };
//# sourceMappingURL=payroll-billing-location.component.js.map