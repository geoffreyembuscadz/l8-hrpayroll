var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { PayrollService } from '../../services/payroll.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
var PayrollAllowanceModalComponent = /** @class */ (function (_super) {
    __extends(PayrollAllowanceModalComponent, _super);
    function PayrollAllowanceModalComponent(_rt, dialogService, _conf, modalService, _payrollServ, _fb) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._conf = _conf;
        _this.modalService = modalService;
        _this._payrollServ = _payrollServ;
        _this._fb = _fb;
        _this.dtTrigger = new Subject();
        _this.dtOptions = {};
        _this.bodyClasses = "skin-blue sidebar-mini";
        _this.body = document.getElementsByTagName('body')[0];
        _this.updateAllowanceForm = _fb.group({
            'amount': ['', [Validators.required]]
        });
        return _this;
    }
    PayrollAllowanceModalComponent.prototype.validateUpdate = function () {
        var _this = this;
        var allowance = this.updateAllowanceForm.value;
        this._payrollServ.updateAllowance(this.emp_id, allowance)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The allowance is successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/payroll-generate']);
        }, function (err) { return _this.catchError(err); });
    };
    PayrollAllowanceModalComponent.prototype.ngOnInit = function () {
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    };
    PayrollAllowanceModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    PayrollAllowanceModalComponent.prototype.onlyDecimalNumberKey = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    };
    PayrollAllowanceModalComponent.prototype.ngOnDestroy = function () {
    };
    PayrollAllowanceModalComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    PayrollAllowanceModalComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], PayrollAllowanceModalComponent.prototype, "dtElement", void 0);
    PayrollAllowanceModalComponent = __decorate([
        Component({
            selector: 'admin-payroll-allowance-list',
            templateUrl: './payroll-allowance.component.html',
            styleUrls: ['./payroll.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Router, DialogService,
            Configuration,
            DialogService,
            PayrollService,
            FormBuilder])
    ], PayrollAllowanceModalComponent);
    return PayrollAllowanceModalComponent;
}(DialogComponent));
export { PayrollAllowanceModalComponent };
//# sourceMappingURL=payroll-allowance.component.js.map