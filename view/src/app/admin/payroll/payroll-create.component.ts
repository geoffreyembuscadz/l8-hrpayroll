
import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TimepickerModule } from 'ngx-bootstrap';

import { AdjustmentTypeService } from '../../services/adjustment-type.service';

import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { DepartmentService } from '../../services/department.service';
import { PayrollService } from '../../services/payroll.service';
import { PositionService } from '../../services/position.service';
import { AuthUserService } from '../../services/auth-user.service';
import { AttendanceService } from '../../services/attendance.service';


import { AdjustmentListModalComponent } from '../adjustment/adjustment-list-modal.component';
import { LoanListModalComponent } from '../loan/loan-list-modal.component';
import { PayrollAllowanceModalComponent } from './payroll-allowance.component';

import { Select2OptionData } from 'ng2-select2';


@Component({
  selector: 'admin-payroll-create',
  templateUrl: './payroll-create.component.html',
  styleUrls: ['./payroll.component.css']
})

@Injectable()
export class PayrollCreateComponent implements OnInit, AfterViewInit  {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();

  dtOptions: any;
  persons: any;
  user_id: any;

  public id: string;
  public payroll_id: any;
  public mainInput = {
      start: moment(),
      end: moment()
  }
  public ismeridian:boolean = true;
  public start_date:Date = new Date();
  public end_date:Date = new Date();
  public start_time:any;
  public end_time:any;
  public type:any;
  public length_days:any;
  public length_hours:any;
  public department: boolean = false;
  public position: boolean = false;
  public company: boolean = false;
  public branch: boolean = false;
  public employee: boolean = false;
  public enableDepartment: boolean = false;
  public enablePosition: boolean = false;
  public enableBranch: boolean = false;
  public run_type: any;
  public cutoff: any;
  public selectedCompany: string = "";
  public selectedDepartment: string = "";
  public selectedPosition: string = "";
  public selectedBranch: string = "";

  public total_bill_rate: any;
  public total_asf: any;
  public total_thirteen_m: any;
  public total_d_cola: any;
  public total_d_rate: any;
  public total_basic_pay: any;
  public allowance: any;
  public total_allowance: any;
  public total_ecola: any;
  public total_late: any;
  public total_undertime: any;
  public total_pay_reg_ot: any;
  public total_pay_restday: any;
  public total_pay_rd_ot: any;
  public total_pay_special_hol: any;
  public total_pay_special_hol_ot: any;
  public total_pay_special_hol_rd: any;
  public total_pay_special_hol_rd_ot: any;
  public total_pay_legal_hol: any;
  public total_pay_legal_hol_ot: any;
  public total_pay_legal_hol_restday: any;
  public total_pay_legal_hol_rd_ot: any;
  public total_pay_double_hol: any;
  public total_pay_dbl_hol_ot: any;
  public total_pay_double_hol_rd: any;
  public total_pay_dbl_hol_rd_ot: any;
  public total_pay_reg_nd: any;
  public total_pay_rd_nd: any;
  public total_pay_special_hol_nd: any;
  public total_pay_special_hol_rd_nd: any;
  public total_pay_legal_hol_nd: any;
  public total_pay_legal_hol_rd_nd: any;
  public total_pay_dbl_hol_nd: any;
  public total_pay_dbl_hol_rd_nd: any;
  public total_adjustment: any;
  public total_deduction: any;
  public total_loan: any;
  public total_sss: any;
  public total_philhealth: any;
  public total_pagibig: any;
  public total_gross_pay: any;
  public total_net_pay: any;

  public payroll_list_model: any;

  public att: any;
  public selectedEmployee: any;


  public error_title: string;
  public error_message: string;
  private success_message: string;
  public success_title: string;

  public approved_btn: boolean = false;
  public isGenerated: boolean = false;

  public removed_refresh: boolean = false;
  public employee_list_for_payroll: boolean = true;
  public payroll_ready: boolean = true;


  bodyClasses:string = "skin-blue sidebar-mini";
  body = document.getElementsByTagName('body')[0];
  payroll_rec: any;
  emp_rec: any;
  employee_list: any;
  company_rec: any;
  company_branch_rec: any;
  department_rec: any;
  position_rec: any;
  adj_rec: any;
  createPayrollForm : FormGroup;

  public isLoading: boolean = false;

  public removed_employee: any[] = [];

  public PayrollData: Array<Select2OptionData>;
  public value: any;
  public current: any;
  public options: Select2Options;

  constructor(
      private _conf: Configuration, 
      private _payroll_service: PayrollService, 
      private modalService: DialogService, 
      private _rt: Router,
      private _fb: FormBuilder, 
      private _ar: ActivatedRoute, 
      private daterangepickerOptions: DaterangepickerConfig,
      private _company_serv: CompanyService,
      private _emp_service: EmployeeService,
      private _department_serv: DepartmentService,
      private _position_serv: PositionService,
      private _ajd_type: AdjustmentTypeService,
      private _auth_service: AuthUserService, 
      private _attendance_serv: AttendanceService
    ) {
      this.body.classList.add("skin-blue");
      this.body.classList.add("sidebar-mini");

      this.createPayrollForm = _fb.group({
        'name': ['',[Validators.required]],
        'start_time': [null],
        'end_time': [null],
        'date': [null],
        'type_of_run': [''],
        'employee_id': [null],
        'company': [''],
        'branch': [''],
        'department': [''],
        'position': [''],
        'prepared_by': [null],
        'payroll_id':[null],
        'approved':[null],
        'approved_by':[null],
        'created_by':[null],
        'removed_employee':[null],
        'cutoff':[null,[Validators.required]]

      }); 

  }

  ngOnInit() {
    let role_model = this.createPayrollForm.value;
    let start = this.start_time;
    let end   = this.end_time;
    let company = this.selectedCompany;
    let branch = this.selectedBranch;
    let department = this.selectedDepartment;
    let position = this.selectedPosition;
    let run_type = this.run_type;
    let cutoff = this.createPayrollForm.value.cutoff;

    this.getList();

    if (run_type == "undefined") {
      run_type = "0";
    }
    if (this.selectedBranch == '') {
       branch = 'undefined';
    } else {
       branch = this.selectedBranch;
    }
    // this._payroll_service
    // .getPayrollList(start, end, run_type, company, branch, department, position, cutoff)
    // .subscribe(
    //   data => {
    //     this.isLoading = false;
    //   },
    //   err => this.catchError(err)
    // );

    this.daterangepickerOptions.settings = {
          locale: { format: 'MM/DD/YYYY h:mm A' },
          alwaysShowCalendars: false,
          timePicker: true
      }; 

    this.dtOptions = {
          "aLengthMenu": [[10, 25, 50, 100,500,1000,-1], [10, 25, 50,100,500,1000, "All"]],
          Sort: true,
          dom: '<"row"<"col-md-12"l>>Bfrtip',
          buttons: [
            {
                orientation: 'portrait',
                pageSize: 'LEGAL',
                extend: 'excelHtml5',
                footer: true,
            },
            'colvis'
          ],
          "scrollX": true,
          responsive: true,
          paging: false,
          searching: false,
          scrollCollapse: true,
          fixedColumns:   {
              leftColumns: 3
          }
      };

    // let payroll_list = this._payroll_service.getPayrollList(start, end, run_type, company, branch, department, position, cutoff)
    //     .subscribe(
    //       data => {
    //         this.payroll_rec = Array.from(data);

    //         this.payrollFooter(data);
           
    //         this.rerender();
    //       }
    //     );

    let created_by = this._auth_service.getUser().
        subscribe(
          data => {
            let user = data; 
            this.user_id = user.id;
          },
          err => console.error(err)
        );


        this.removed_employee = [];

        if (this.removed_refresh == true) {
          this.getPayrollEmployee(this.removed_employee);
        }
  }  

  payrollFooter(data) {
    this.payroll_rec = Array.from(data);

    if (data != '' && data != 'undefined') {
          let payroll_len = this.payroll_rec.length; this.total_bill_rate = 0; this.allowance = 0; this.total_allowance = 0; this.total_asf = 0; this.total_thirteen_m = 0; this.total_d_cola = 0; this.total_d_rate = 0; this.total_basic_pay = 0; this.total_ecola = 0; this.total_late = 0; this.total_undertime = 0; this.total_pay_reg_ot = 0; this.total_pay_restday = 0; this.total_pay_rd_ot = 0; this.total_pay_special_hol = 0; this.total_pay_special_hol_ot = 0; this.total_pay_special_hol_rd = 0; this.total_pay_special_hol_rd_ot = 0; this.total_pay_legal_hol = 0; this.total_pay_legal_hol_ot = 0; this.total_pay_legal_hol_restday = 0; this.total_pay_legal_hol_rd_ot = 0; this.total_pay_double_hol = 0; this.total_pay_dbl_hol_ot = 0; this.total_pay_double_hol_rd = 0; this.total_pay_dbl_hol_rd_ot = 0; this.total_pay_reg_nd = 0;  this.total_pay_rd_nd = 0;  this.total_pay_special_hol_nd = 0;  this.total_pay_special_hol_rd_nd = 0;  this.total_pay_legal_hol_nd = 0;  this.total_pay_legal_hol_rd_nd = 0;  this.total_pay_dbl_hol_nd = 0;  this.total_pay_dbl_hol_rd_nd = 0;  this.total_adjustment = 0; this.total_deduction = 0;  this.total_loan = 0;  this.total_sss = 0;  this.total_philhealth = 0; this.total_pagibig = 0; this.total_gross_pay = 0; this.total_net_pay = 0;
          for (let x = 0; x < payroll_len; x++) {
            this.total_bill_rate = this.total_bill_rate + this.payroll_rec[x].bill_rate * 1;
            this.total_asf = this.total_asf + this.payroll_rec[x].asf * 1;
            this.total_thirteen_m = this.total_thirteen_m + this.payroll_rec[x].thirteen_month * 1;
            this.total_d_cola = this.total_d_cola + this.payroll_rec[x].cola * 1;
            this.total_d_rate = this.total_d_rate + this.payroll_rec[x].daily_rate * 1;
            this.total_basic_pay = this.total_basic_pay + this.payroll_rec[x].ttl_basic_pay * 1;
            this.allowance = this.allowance + this.payroll_rec[x].allowance * 1;
            this.total_allowance = this.total_allowance + this.payroll_rec[x].total_allowance * 1;
            this.total_ecola = this.total_ecola + this.payroll_rec[x].ecola * 1;
            this.total_late = this.total_late + this.payroll_rec[x].late_amount * 1;  
            this.total_undertime = this.total_undertime + this.payroll_rec[x].undertime_amount * 1;
            this.total_pay_reg_ot = this.total_pay_reg_ot + this.payroll_rec[x].pay_reg_ot * 1;
            this.total_pay_restday = this.total_pay_restday + this.payroll_rec[x].pay_restday * 1;
            this.total_pay_rd_ot = this.total_pay_rd_ot + this.payroll_rec[x].pay_rd_ot * 1;
            this.total_pay_special_hol = this.total_pay_special_hol + this.payroll_rec[x].pay_special_hol * 1;
            this.total_pay_special_hol_ot = this.total_pay_special_hol_ot + this.payroll_rec[x].pay_special_hol_ot * 1;
            this.total_pay_special_hol_rd = this.total_pay_special_hol_rd + this.payroll_rec[x].pay_special_hol_rd * 1;
            this.total_pay_special_hol_rd_ot = this.total_pay_special_hol_rd_ot + this.payroll_rec[x].pay_special_hol_rd_ot * 1;
            this.total_pay_legal_hol = this.total_pay_legal_hol + this.payroll_rec[x].pay_legal_hol * 1;
            this.total_pay_legal_hol_ot = this.total_pay_legal_hol_ot + this.payroll_rec[x].pay_legal_hol_ot * 1;
            this.total_pay_legal_hol_restday = this.total_pay_legal_hol_restday + this.payroll_rec[x].pay_legal_hol_restday * 1;
            this.total_pay_legal_hol_rd_ot = this.total_pay_legal_hol_rd_ot + this.payroll_rec[x].pay_legal_hol_rd_ot * 1;
            this.total_pay_double_hol = this.total_pay_double_hol + this.payroll_rec[x].pay_double_hol * 1;
            this.total_pay_dbl_hol_ot = this.total_pay_dbl_hol_ot + this.payroll_rec[x].pay_dbl_hol_ot * 1;
            this.total_pay_double_hol_rd = this.total_pay_double_hol_rd + this.payroll_rec[x].pay_double_hol_rd * 1;
            this.total_pay_dbl_hol_rd_ot = this.total_pay_dbl_hol_rd_ot + this.payroll_rec[x].pay_dbl_hol_rd_ot * 1;
            this.total_pay_reg_nd = this.total_pay_reg_nd + this.payroll_rec[x].pay_reg_nd * 1;
            this.total_pay_rd_nd = this.total_pay_rd_nd + this.payroll_rec[x].pay_rd_nd * 1;
            this.total_pay_special_hol_nd = this.total_pay_special_hol_nd + this.payroll_rec[x].pay_special_hol_nd * 1;
            this.total_pay_special_hol_rd_nd = this.total_pay_special_hol_rd_nd + this.payroll_rec[x].pay_special_hol_rd_nd * 1;
            this.total_pay_legal_hol_nd = this.total_pay_legal_hol_nd + this.payroll_rec[x].pay_legal_hol_nd * 1;
            this.total_pay_legal_hol_rd_nd = this.total_pay_legal_hol_rd_nd + this.payroll_rec[x].pay_legal_hol_rd_nd * 1;
            this.total_pay_dbl_hol_nd = this.total_pay_dbl_hol_nd + this.payroll_rec[x].pay_dbl_hol_nd * 1;
            this.total_pay_dbl_hol_rd_nd = this.total_pay_dbl_hol_rd_nd + this.payroll_rec[x].pay_dbl_hol_rd_nd * 1;
            this.total_adjustment = this.total_adjustment + this.payroll_rec[x].total_adjustment * 1;
            this.total_deduction = this.total_deduction + this.payroll_rec[x].total_deduction * 1;
            this.total_loan = this.total_loan + this.payroll_rec[x].total_loan * 1;
            this.total_sss = this.total_sss + this.payroll_rec[x].total_sss * 1;
            this.total_philhealth = this.total_philhealth + this.payroll_rec[x].total_philhealth * 1;
            this.total_pagibig = this.total_pagibig + this.payroll_rec[x].total_pagibig * 1;
            this.total_gross_pay = this.total_gross_pay + this.payroll_rec[x].gross_pay * 1;
            this.total_net_pay = this.total_net_pay + this.payroll_rec[x].net_pay * 1;
          }

        }
  }
  deleteRow() {
    if(confirm("Do you want to remove the employee on this payroll period?")) {
      this.dtOptions.row('.selected').remove().draw( false );
    }
  }

  generatePayroll() {
    this.isLoading = true;
    let role_model = this.createPayrollForm.value;
    let start = this.start_time;
    let end   = this.end_time;
    let company = this.selectedCompany;
    let department = this.selectedDepartment;
    let position = this.selectedPosition;
    let run_type = this.run_type;
    let cutoff = this.cutoff;
    let branch = '';
    let removed_employee: any;
    if (run_type == "undefined") {
      run_type = "0";
    }
    if (this.selectedBranch == '') {
       branch = 'undefined';
    } else {
       branch = this.selectedBranch;
    }


    if (this.removed_employee.length > 0) {
       removed_employee = this.removed_employee;
    } else {
      removed_employee = 'undefined';
    }
    this._payroll_service
    .getPayrollList(start, end, run_type, company, branch, department, position, cutoff, removed_employee)
    .subscribe(
      data => {
        this.rerender();
        this.isLoading = false;
        this.isGenerated = true;
        this.payrollFooter(data);

      },
      err => this.catchError(err)
    );
  }

  private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_time = moment(dateInput.start).format("YYYY-MM-DD HH:mm:ss");
        this.end_time = moment(dateInput.end).format("YYYY-MM-DD HH:mm:ss");

  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'Something went wrong';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }
  }

  ngOnDestroy() {
      this.body.classList.remove("skin-blue");
      this.body.classList.remove("sidebar-mini");
   }

  ngAfterViewInit(): void {
      this.dtTrigger.next();
  }
  rerender(): void {
     this.dtElement.dtInstance.then((dtInstance) => {
       dtInstance.destroy();
       this.dtTrigger.next();
     });
  }

  runtypeChange(value) {
    this.run_type = value;
    this.selectedDepartment = '';
    this.selectedPosition = '';
    if (value == 1) {
      this.company = true;
      this.branch = true;
      this.employee = false;
      this.position = false;
      this.department = false;
    } else if (value == 2) {
      this.department = true;
      this.position = false;
      this.company = true;
      this.branch = true;
      this.employee = false;
    } else if (value == 3) {
      this.position = true;
      this.department = false;
      this.company = true;
      this.employee = false;
      this.branch = true;

    } else {
      this.position = false;
      this.department = false;
      this.company = false;
      this.employee = false;
      this.branch = false;
      this.selectedCompany = '';
      this.selectedDepartment = '';
      this.selectedPosition = '';
      this.selectedBranch = '';
    }
  }


 payrollPeriodChange(value) {
   this.cutoff = value;
 }

 getList() {

    let companies = this._company_serv.getCompanys()
        .subscribe(
          data => {
            this.company_rec = Array.from(data);
          },
          err => this.catchError(err)
        );
   let department = this._department_serv.getDepartments()
        .subscribe(
          data => {
            this.department_rec = Array.from(data);
          },
          err => this.catchError(err)
        );
   let position = this._position_serv.getPositions()
        .subscribe(
          data => {
            this.position_rec = Array.from(data);
          },
          err => this.catchError(err)
        );
   let adjustment = this._ajd_type.getAdjustmentsType()
        .subscribe(
           data => {
             this.adj_rec = Array.from(data);
           },
           err => this.catchError(err)
        );

   let company_branch = this._company_serv.getCompanyBranch()
        .subscribe(
          data => {
            this.company_branch_rec = Array.from(data);
          },
          err => this.catchError(err)
       );
 }

 changed(data: any) {
      this.current = data.value;
 }

 getCompany(value, event) {
    this.enableDepartment = true;
    this.enablePosition = true;
    this.enableBranch = true;
    this.selectedCompany = value;
 }
 getDepartment(value, event) {
   if (value == '-none-') {
     this.selectedDepartment = '';
   } else {
    this.selectedDepartment = value;
   }
 }
 getPosition(value, event) {
   if (value == '-none-') {
     this.selectedPosition = '';
   } else {
    this.selectedPosition = value;
   }
    
 }
 getBranch(value, event) {
   if (value == '-none-') {
     this.selectedBranch = 'none';
   } else {
     this.selectedBranch = value;
   }
 }
 viewLoan(id) {
   let disposable = this.modalService.addDialog(LoanListModalComponent, {
          att_id:id,
          start : this.start_time,
          end   : this.end_time
          }).subscribe((isConfirmed)=>{
            if (isConfirmed) {
              this.rerender();
            }
        });
 }
 
 viewAdjustment(id, type, no_of_days) {
    let disposable = this.modalService.addDialog(AdjustmentListModalComponent, {
          att_id:id,
          no_of_days:no_of_days,
          start : this.start_time,
          end   : this.end_time,
          type  : type
          }).subscribe((isConfirmed)=>{
            if (isConfirmed) {
              this.rerender();
            }
        });
 }


 updateAllowance(id) {
   let disposable = this.modalService.addDialog(PayrollAllowanceModalComponent, {
     emp_id: id
   }).subscribe((isConfirmed)=>{
      if (isConfirmed) {
        this.rerender();
      }
   });
 
 }



 savePayroll() {

    let start = this.start_time;
    let end   = this.end_time;
    let company = this.selectedCompany;
    let branch = this.selectedBranch;
    let department = this.selectedDepartment;
    let position = this.selectedPosition;
    let run_type = this.run_type;
    let cutoff = this.cutoff;
    let removed_employee = this.removed_employee;

    this.createPayrollForm.patchValue({
      'start_time': this.start_time
    });
    this.createPayrollForm.patchValue({
      'end_time': this.end_time
    });
    this.createPayrollForm.patchValue({
      'company': this.selectedCompany
    });
    this.createPayrollForm.patchValue({
      'branch': this.selectedBranch
    });
    this.createPayrollForm.patchValue({
      'department': this.selectedDepartment
    });
    this.createPayrollForm.patchValue({
      'position': this.selectedPosition
    });
    this.createPayrollForm.patchValue({
      'cutoff': cutoff
    });
    this.createPayrollForm.patchValue({
      'approved': 0
    });
    this.createPayrollForm.patchValue({
      'created_by': this.user_id
    });

    this.createPayrollForm.patchValue({
      'approved_by': 0
    });

    this.createPayrollForm.patchValue({
      'removed_employee': this.removed_employee.join(",")
    });

    this.createPayrollForm.value.prepared_by = this.user_id;
    let payroll_model = this.createPayrollForm.value;
    let storePayroll = this._payroll_service.storePayroll(payroll_model)
    .subscribe(
      data => {
        this._rt.navigate(['/admin/payroll']);
      },
      err => this.catchError(err)
    );
  }

  savePayrollList(id) {
    this.createPayrollForm.value.payroll_id = id;
    this.payroll_list_model = this.createPayrollForm.value;
    let storePayrollList = this._payroll_service.storePayrollList(this.payroll_list_model).subscribe(data=>{},err=>this.catchError(err));
  }

  removeEmployeeFromPayrollGeneration(value) {
     // check if already exist
     if (this.removed_employee.indexOf(value) == -1) {
       this.removed_employee.push(value);
     }
    
     this.getPayrollEmployee(this.removed_employee);
  }
  
  getPayrollEmployee(employee?) {

    this.isLoading = true;
    this.employee_list_for_payroll = false;
    let role_model = this.createPayrollForm.value;
    let start = this.start_time;
    let end   = this.end_time;
    let company = this.selectedCompany;
    let department = this.selectedDepartment;
    let position = this.selectedPosition;
    let run_type = this.run_type;
    let cutoff = this.cutoff;
    let branch = '';
    if (run_type == "undefined") {
      run_type = "0";
    }
    if (this.selectedBranch == '') {
       branch = 'undefined';
    } else {
       branch = this.selectedBranch;
    }
    let removed_employee = employee;

    this._payroll_service
    .getPayrollEmployee(start, end, run_type, company, branch, department, position, cutoff, removed_employee)
    .subscribe(
      data => {
        this.isLoading = false;
        this.isGenerated = true;
        this.employee_list = Array.from(data);
        // get the count for validation
        this.getTheCountOfEmployee(this.employee_list.length);
      },
      err => this.catchError(err)
    );

  }  

  getTheCountOfEmployee(length) {
    if (length == 0) {
      this.employee_list_for_payroll = true;
      this.removed_employee = [];
      this.generatePayroll();
    } else {
      this.employee_list_for_payroll = false;
    }
  }

  // not using because angular don't support new tab passing url data; it's here incase 
  goAttendance(id) {

    let start = moment(this.start_time).format("YYYY-MM-DD");
    let end   = moment(this.end_time).format("YYYY-MM-DD");
    let company = this.selectedCompany;
    let employee = id;
    let model: {
      start_date ? : string,
      end_date ? : string,
      company_id ? : string,
      employee_id ? : string 
    };
    model = {};
    model['start_date'] = start;
    model['end_date'] = end;
    model['company_id'] = company;
    model['employee_id'] = employee;
    model['type_id'] = null;


    console.log('model',model);
    this._attendance_serv.getAttendDefault(model).
        subscribe(
        data => {
          this.att = Array.from(data);
          window.open("https://www.google.com", "_blank");
          console.log('attendance', this.att);
        },
        err => console.error(err)
    );
  }


}