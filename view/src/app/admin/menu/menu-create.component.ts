import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { Observable } from "rxjs/Rx";

import { Menu } from '../../model/menu';
import { MenuService } from '../../services/menu.service';
import { ModuleService } from '../../services/module.service';

@Component({
  selector: 'admin-menu-create',
  templateUrl: './menu-create.component.html'
})
export class MenuCreateComponent extends DialogComponent<null, boolean> {

  module_rec: any;
  public error_title: string;
  public error_message: string;
  private success_title: string;
  private success_message: string;
  public poststore: any;
  public menu = new Menu();

  error = false;
  elem: any;
  errorMessage = '';
  createMenuForm : FormGroup;

  constructor(private _rt: Router, dialogService: DialogService, private _fb: FormBuilder, private _module_service: ModuleService,  private _menu_service: MenuService) { 
  	super(dialogService);

  	this.createMenuForm = _fb.group({
	    'name': ['', [Validators.required]],
	    'display_name': ['', [Validators.required]],
	    'description': ['', [Validators.required]],
	    'parent_id' : ['', [Validators.required]],
      'action_url' : ['', [Validators.required]],
    });
  }

  onSubmit() {

	let  menu_model = this.createMenuForm.value;
  console.log(menu_model);
	this._menu_service
	.storeMenu(menu_model)
	.subscribe(
		data => {
			this.poststore = Array.from(data);
			this.success_title = "Success";
      this.success_message = "A new menu record was successfully added.";
      alert('A new menu record was successfully added.');
      setTimeout(() => {
             this.close();
            }, 2000);
      this._rt.navigate(['/admin/menu-list']);

		},
		err => this.catchError(err)
	);
  }

  private catchError(error: any){
  	let response_body = error._body;
  	let response_status = error.status;

  	if( response_status == 500 ){
  		this.error_title = 'Error 500';
  		this.error_message = 'Menu name already exist.';
  	} else if( response_status == 200 ) {
  		this.error_title = '';
  		this.error_message = '';
  	}
  }

  ngOnInit() {
     let module = this._module_service.getModules()
         .subscribe(
            data => {
               this.module_rec = Array.from(data);
               console.log(data);
            },
            err => this.catchError(err)
         );


  }

}
