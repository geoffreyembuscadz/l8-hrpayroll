import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';

import { Menu } from '../../model/menu';
import { MenuService } from '../../services/menu.service';
import { ModuleService } from '../../services/module.service';

@Component({
  selector: 'admin-menu-edit',
  templateUrl: './menu-edit.component.html'
})
export class MenuEditComponent extends DialogComponent<null, boolean> {
  
  title: string;
  message: string;

  public id: any;
  public name: string;
  public display_name: string;
  public description: string;
  public parent_id: string;
  public success_title;
  public success_message;
  private headers: Headers;
  public poststore: any;
  public menu = new Menu();

  module_rec: any;

  updateMenuForm : FormGroup;

  constructor(private _menu_service: MenuService, private _rt: Router, private _module_service: ModuleService, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute) {
    super(dialogService);
   
    this.id = this._menu_service.getId();
    this._menu_service.getMenu(this.id).subscribe(
      data => {
        this.setRole(data);
        console.log(data);
      },
      err => console.error(err)
    );

    this.updateMenuForm = _fb.group({
      'name': [this.menu.name, [Validators.required]],
      'display_name': [this.menu.display_name, [Validators.required]],
      'description': [this.menu.description, [Validators.required]],
      'parent_id': [this.menu.parent_id, [Validators.required]],
      'action_url': [this.menu.action_url, [Validators.required]]
    });  
  }
 
  public validateUpdate() {
    let menu_model = this.updateMenuForm.value;

    this._menu_service.updateMenu(this.id, menu_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The menu is successfully updated.";
        alert('The menu is successfully updated.');
        this.close();
        this._rt.navigate(['/admin/menu-list']);
      },
      err => this.catchError(err)
    );
  }

  archiveMenu(event: Event){
    event.preventDefault();
    let menu_id = this.id;
    this._menu_service.archiveMenu(menu_id).subscribe(
      data => {
      
        alert('Record is successfully archived.');
        this.close();
        this._rt.navigateByUrl('admin/menu-list');
        
      },
      err => console.error(err)
    );
    console.log('record archive');

  }

  public catchError(error: any) {

  }

  public setRole(menu: any){
    this.id = menu.id;
    this.menu = menu;
  }


  ngOnInit() {
    let module = this._module_service.getModules()
         .subscribe(
            data => {
               this.module_rec = Array.from(data);
            },
            err => this.catchError(err)
         );
  }

}