var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Menu } from '../../model/menu';
import { MenuService } from '../../services/menu.service';
import { ModuleService } from '../../services/module.service';
var MenuEditComponent = /** @class */ (function (_super) {
    __extends(MenuEditComponent, _super);
    function MenuEditComponent(_menu_service, _rt, _module_service, dialogService, _fb, _ar) {
        var _this = _super.call(this, dialogService) || this;
        _this._menu_service = _menu_service;
        _this._rt = _rt;
        _this._module_service = _module_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.menu = new Menu();
        _this.id = _this._menu_service.getId();
        _this._menu_service.getMenu(_this.id).subscribe(function (data) {
            _this.setRole(data);
            console.log(data);
        }, function (err) { return console.error(err); });
        _this.updateMenuForm = _fb.group({
            'name': [_this.menu.name, [Validators.required]],
            'display_name': [_this.menu.display_name, [Validators.required]],
            'description': [_this.menu.description, [Validators.required]],
            'parent_id': [_this.menu.parent_id, [Validators.required]],
            'action_url': [_this.menu.action_url, [Validators.required]]
        });
        return _this;
    }
    MenuEditComponent.prototype.validateUpdate = function () {
        var _this = this;
        var menu_model = this.updateMenuForm.value;
        this._menu_service.updateMenu(this.id, menu_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The menu is successfully updated.";
            alert('The menu is successfully updated.');
            _this.close();
            _this._rt.navigate(['/admin/menu-list']);
        }, function (err) { return _this.catchError(err); });
    };
    MenuEditComponent.prototype.archiveMenu = function (event) {
        var _this = this;
        event.preventDefault();
        var menu_id = this.id;
        this._menu_service.archiveMenu(menu_id).subscribe(function (data) {
            alert('Record is successfully archived.');
            _this.close();
            _this._rt.navigateByUrl('admin/menu-list');
        }, function (err) { return console.error(err); });
        console.log('record archive');
    };
    MenuEditComponent.prototype.catchError = function (error) {
    };
    MenuEditComponent.prototype.setRole = function (menu) {
        this.id = menu.id;
        this.menu = menu;
    };
    MenuEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        var module = this._module_service.getModules()
            .subscribe(function (data) {
            _this.module_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
    };
    MenuEditComponent = __decorate([
        Component({
            selector: 'admin-menu-edit',
            templateUrl: './menu-edit.component.html'
        }),
        __metadata("design:paramtypes", [MenuService, Router, ModuleService, DialogService, FormBuilder, ActivatedRoute])
    ], MenuEditComponent);
    return MenuEditComponent;
}(DialogComponent));
export { MenuEditComponent };
//# sourceMappingURL=menu-edit.component.js.map