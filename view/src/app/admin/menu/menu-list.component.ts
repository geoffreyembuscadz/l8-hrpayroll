import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';

import { Menu } from '../../model/menu';
import { MenuCreateComponent } from './menu-create.component';
import { MenuEditComponent } from './menu-edit.component';
import { MenuService } from '../../services/menu.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'admin-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})

@Injectable()
export class MenuListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	public id: string;
	public menu_id: any;
	public menu = new Menu();

	dtOptions: any = {};
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

	constructor(private _conf: Configuration, private modalService: DialogService, private _menu_service: MenuService){
		//add the the body classes
	    this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	}

	ngOnInit() {
		let authToken = localStorage.getItem('id_token');

		// DataTable Configuration
		this.dtOptions = {
			ajax: {
				url: this._conf.ServerWithApiUrl + 'module_menu',
				type: "GET",
				beforeSend: function(xhr){
                            xhr.setRequestHeader("Authorization",
                            "Bearer " + authToken);
                },
			},
			columns: [
			{
				title: 'ID',
				data: 'id'
			}, {
				title: 'Name',
				data: 'name'
			}, {
				title: 'Display Name',
				data: 'display_name'
			}, {
				title: 'Description',
				data: 'description'
			}, {
				title: 'Action URL',
				data: 'action_url'
			}, {
				title: 'Parent',
				data: 'module_name'
			}],
			rowCallback: (nRow: number, data: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
				let self = this;
				// Unbind first in order to avoid any duplicate handler
				// (see https://github.com/l-lin/angular-datatables/issues/87)
				$('td', nRow).unbind('click');
				$('td', nRow).bind('click', () => {
					this.menu_id = data.id;
				    this._menu_service.setId(this.menu_id);
					this.openEditModal();
				});
				return nRow;
			}
		};

		//add the the body classes
	    this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

		
	}

	openEditModal() {
		let disposable = this.modalService.addDialog(MenuEditComponent, {
            title:'Update Menu'
        	}).subscribe((isConfirmed)=>{
        			this.rerender();
                
        });
	}

	openAddModal() {
		let disposable = this.modalService.addDialog(MenuCreateComponent, {
            title:'Add Menu'
        	}).subscribe((isConfirmed)=>{
        			this.rerender();
                
        });
    }

	ngOnDestroy() {
	    //remove the the body classes
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	 }

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      // Destroy the table first
	      dtInstance.destroy();
	      // Call the dtTrigger to rerender again
	      this.dtTrigger.next();
	    });
    }

}
