var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Menu } from '../../model/menu';
import { MenuService } from '../../services/menu.service';
import { ModuleService } from '../../services/module.service';
var MenuCreateComponent = /** @class */ (function (_super) {
    __extends(MenuCreateComponent, _super);
    function MenuCreateComponent(_rt, dialogService, _fb, _module_service, _menu_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._fb = _fb;
        _this._module_service = _module_service;
        _this._menu_service = _menu_service;
        _this.menu = new Menu();
        _this.error = false;
        _this.errorMessage = '';
        _this.createMenuForm = _fb.group({
            'name': ['', [Validators.required]],
            'display_name': ['', [Validators.required]],
            'description': ['', [Validators.required]],
            'parent_id': ['', [Validators.required]],
            'action_url': ['', [Validators.required]],
        });
        return _this;
    }
    MenuCreateComponent.prototype.onSubmit = function () {
        var _this = this;
        var menu_model = this.createMenuForm.value;
        console.log(menu_model);
        this._menu_service
            .storeMenu(menu_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            _this.success_title = "Success";
            _this.success_message = "A new menu record was successfully added.";
            alert('A new menu record was successfully added.');
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/menu-list']);
        }, function (err) { return _this.catchError(err); });
    };
    MenuCreateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Menu name already exist.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    MenuCreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        var module = this._module_service.getModules()
            .subscribe(function (data) {
            _this.module_rec = Array.from(data);
            console.log(data);
        }, function (err) { return _this.catchError(err); });
    };
    MenuCreateComponent = __decorate([
        Component({
            selector: 'admin-menu-create',
            templateUrl: './menu-create.component.html'
        }),
        __metadata("design:paramtypes", [Router, DialogService, FormBuilder, ModuleService, MenuService])
    ], MenuCreateComponent);
    return MenuCreateComponent;
}(DialogComponent));
export { MenuCreateComponent };
//# sourceMappingURL=menu-create.component.js.map