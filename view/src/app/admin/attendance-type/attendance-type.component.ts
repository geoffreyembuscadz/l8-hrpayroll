import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { AttendanceTypeService } from '../../services/attendance-type.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { TypeModalComponent } from '../type-modal/type-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';


@Component({
  selector: 'app-attendance-type',
  templateUrl: './attendance-type.component.html',
  styleUrls: ['./attendance-type.component.css']
})

@Injectable()
export class AttendanceTypeComponent implements OnInit,  AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};

	attendance:any;

	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	constructor(
  		dialogService: DialogService, 
  		private modalService: DialogService, 
  		private _attendance_type_service: AttendanceTypeService
  	){

		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	}

	ngAfterViewInit(){
		this.dtTrigger.next();
	}

	ngOnInit() {
		this.data();
	}
	data(){

    	this._attendance_type_service.getAttendType().
	      	subscribe(
	        data => {
	          this.attendance= Array.from(data);
	          this.rerender();
	        },
	        err => console.error(err)
	     );

	}

	editAttendanceType(id:any) {
		let disposable = this.modalService.addDialog(TypeModalComponent, {
            title:'Update Attendance Type',
            button:'Update',
		    edit:true,
		    url:'attendance_type',
		    id:id
        	}).subscribe((isConfirmed)=>{
                this.rerender();
                this.ngOnInit();
        });
	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	 }

	addAttendanceType() {
		let disposable = this.modalService.addDialog(TypeModalComponent, {
            title:'Add Attendance Type',
            button:'Add',
		    url:'attendance_type',
		    create:true
        	}).subscribe((isConfirmed)=>{
              this.rerender();  
              this.ngOnInit();
            });
		}

	rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    archiveAttendanceType(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'attendance_type'
        	}, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
		       	this.rerender();
                this.ngOnInit();
        });
	}
	
}
