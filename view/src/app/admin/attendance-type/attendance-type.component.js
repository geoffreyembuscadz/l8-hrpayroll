var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { AttendanceTypeService } from '../../services/attendance-type.service';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { TypeModalComponent } from '../type-modal/type-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
var AttendanceTypeComponent = /** @class */ (function () {
    function AttendanceTypeComponent(dialogService, modalService, _attendance_type_service) {
        this.modalService = modalService;
        this._attendance_type_service = _attendance_type_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    AttendanceTypeComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    AttendanceTypeComponent.prototype.ngOnInit = function () {
        this.data();
    };
    AttendanceTypeComponent.prototype.data = function () {
        var _this = this;
        this._attendance_type_service.getAttendType().
            subscribe(function (data) {
            _this.attendance = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    AttendanceTypeComponent.prototype.editAttendanceType = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Update Attendance Type',
            button: 'Update',
            edit: true,
            url: 'attendance_type',
            id: id
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    AttendanceTypeComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    AttendanceTypeComponent.prototype.addAttendanceType = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Add Attendance Type',
            button: 'Add',
            url: 'attendance_type',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    AttendanceTypeComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    AttendanceTypeComponent.prototype.archiveAttendanceType = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'attendance_type'
        }, { backdropColor: 'rgba(238,238,238)' }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], AttendanceTypeComponent.prototype, "dtElement", void 0);
    AttendanceTypeComponent = __decorate([
        Component({
            selector: 'app-attendance-type',
            templateUrl: './attendance-type.component.html',
            styleUrls: ['./attendance-type.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            DialogService,
            AttendanceTypeService])
    ], AttendanceTypeComponent);
    return AttendanceTypeComponent;
}());
export { AttendanceTypeComponent };
//# sourceMappingURL=attendance-type.component.js.map