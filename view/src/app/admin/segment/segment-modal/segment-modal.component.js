var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SegmentService } from '../../../services/segment.service';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from './../../confirm-modal/confirm-modal.component';
var SegmentModalComponent = /** @class */ (function (_super) {
    __extends(SegmentModalComponent, _super);
    function SegmentModalComponent(dialogService, _fb, _ar, _segment_service, _rt, _common_service, modalService) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._segment_service = _segment_service;
        _this._rt = _rt;
        _this._common_service = _common_service;
        _this.modalService = modalService;
        _this.updateSegmentForm = _fb.group({
            'name': [null],
            'description': [null],
        });
        return _this;
    }
    SegmentModalComponent.prototype.ngOnInit = function () {
        if (this.edit == true) {
            this.get_data();
        }
    };
    SegmentModalComponent.prototype.get_data = function () {
        var _this = this;
        var id = this.id;
        this._segment_service.getSegment(id).subscribe(function (data) {
            _this.setType(data);
        }, function (err) { return console.error(err); });
    };
    SegmentModalComponent.prototype.setType = function (type) {
        this.id = type.id;
        this.name = type.name;
        this.description = type.description;
    };
    SegmentModalComponent.prototype.onSubmit = function () {
        var _this = this;
        var model = this.updateSegmentForm.value;
        var url = this.url;
        if (this.edit == true) {
            var id = this.id;
            this._segment_service.updateSegment(id, model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data); // fetched the records
                _this.success_title = "Success!";
                _this.success_message = "Successfully updated";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
        if (this.create == true) {
            this._segment_service.storeSegment(model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data); // fetched the records
                _this.success_title = "Success!";
                _this.success_message = "Successfully created";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    SegmentModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    SegmentModalComponent.prototype.archive = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: this.id,
            url: this.url
        }).subscribe(function (isConfirmed) {
            setTimeout(function () {
                _this.close();
                // this.rerender();
            }, 1000);
        });
    };
    SegmentModalComponent = __decorate([
        Component({
            selector: 'app-segment-modal',
            templateUrl: './segment-modal.component.html',
            styleUrls: ['./segment-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            SegmentService,
            Router,
            CommonService,
            DialogService])
    ], SegmentModalComponent);
    return SegmentModalComponent;
}(DialogComponent));
export { SegmentModalComponent };
//# sourceMappingURL=segment-modal.component.js.map