import 'rxjs/add/operator/catch'
import { Component, OnInit, AfterViewInit, NgZone, Inject } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response} from '@angular/http';
import { SegmentService } from '../../../services/segment.service';
import { Configuration } from '../../../app.config';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from './../../confirm-modal/confirm-modal.component';

@Component({
	selector: 'app-segment-modal',
	templateUrl: './segment-modal.component.html',
	styleUrls: ['./segment-modal.component.css']
})

export class SegmentModalComponent extends DialogComponent<null, boolean> {

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public poststore: any;
	public id: any;
	public segment: any;
	public name: any;
	public description: any;
	
	edit:any;
	create:any;
	url:any;
	updateSegmentForm : FormGroup;
	constructor(
		dialogService: DialogService, 
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute, 
		private _segment_service: SegmentService,  
		private _rt: Router,
		private _common_service: CommonService,
		private modalService: DialogService
		 ) {
		super(dialogService);

		this.updateSegmentForm = _fb.group({
			'name': 	   	 	[null],
			'description': 	 	[null],
		});  
	}

	ngOnInit() {
		if(this.edit == true){
			this.get_data();
		}
	}

	get_data(){
		let id = this.id;
		this._segment_service.getSegment(id).subscribe(
			data => {
				this.setType(data);
			},
			err => console.error(err)
			);
	}

	public setType(type: any){
		this.id = type.id;
		this.name = type.name;    
		this.description = type.description;    
	}

	onSubmit() {
		let model = this.updateSegmentForm.value;
		let url = this.url;
		if(this.edit == true)
		{
			let id = this.id;
			this._segment_service.updateSegment(id, model)
			.subscribe(
				data => {
					this.poststore = Array.from(data); // fetched the records
					this.success_title = "Success!";
					this.success_message = "Successfully updated";
					setTimeout(() => {
						this.close();
					}, 1000);
				},
				err => this.catchError(err)
				);
		}
		if(this.create == true)
		{
			this._segment_service.storeSegment(model)
			.subscribe(
				data => {
					this.poststore = Array.from(data); // fetched the records
					this.success_title = "Success!";
					this.success_message = "Successfully created";
					setTimeout(() => {
						this.close();
					}, 1000);
				},
				err => this.catchError(err)
				);
		}
	}
	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}
	archive(){
		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
			title:'Archive Data',
			message:'Are you sure you want to archive this data?',
			action:'Delete',
			id:this.id,
			url:this.url
		}).subscribe((isConfirmed)=>{
			setTimeout(() => {
				this.close();
				// this.rerender();
			}, 1000);

		});
	}

}