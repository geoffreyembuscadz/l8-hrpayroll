var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { SegmentService } from '../../services/segment.service';
import { CommonService } from '../../services/common.service';
import { SegmentModalComponent } from '../segment/segment-modal/segment-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
var SegmentComponent = /** @class */ (function () {
    function SegmentComponent(modalService, _conf, _common_service, _segment_service) {
        this.modalService = modalService;
        this._conf = _conf;
        this._common_service = _common_service;
        this._segment_service = _segment_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api = this._conf.ServerWithApiUrl + 'overtime/';
    }
    SegmentComponent.prototype.ngOnInit = function () {
        this.data();
    };
    SegmentComponent.prototype.data = function () {
        var _this = this;
        this._segment_service.showSegment().
            subscribe(function (data) {
            _this.segment = data;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    SegmentComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    SegmentComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    SegmentComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    SegmentComponent.prototype.archiveSegment = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'segment'
        }, { backdropColor: 'rgba(238,238,238)' }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    SegmentComponent.prototype.addModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(SegmentModalComponent, {
            title: 'Create Category',
            button: 'Add',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    SegmentComponent.prototype.editModal = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(SegmentModalComponent, {
            title: 'Edit Category',
            button: 'Update',
            edit: true,
            id: id
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], SegmentComponent.prototype, "dtElement", void 0);
    SegmentComponent = __decorate([
        Component({
            selector: 'app-segment',
            templateUrl: './segment.component.html',
            styleUrls: ['./segment.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            CommonService,
            SegmentService])
    ], SegmentComponent);
    return SegmentComponent;
}());
export { SegmentComponent };
//# sourceMappingURL=segment.component.js.map