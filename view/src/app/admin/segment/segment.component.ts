import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { SegmentService } from '../../services/segment.service';
import { CommonService } from '../../services/common.service';
import { SegmentModalComponent } from '../segment/segment-modal/segment-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';

@Component({
	selector: 'app-segment',
	templateUrl: './segment.component.html',
	styleUrls: ['./segment.component.css']
})

@Injectable()
export class SegmentComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};
	public OT_id: any;
	api: String;
	status:any;
	segment:any;
	
	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _common_service: CommonService,
		private _segment_service: SegmentService
		
		){

		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");

		this.api = this._conf.ServerWithApiUrl + 'overtime/';
	}

	ngOnInit() {
		this.data();
	}

	data(){
		this._segment_service.showSegment().
		subscribe(
			data => {
				this.segment = data; 
				this.rerender();
			},
			err => console.error(err)
			);
	}

	ngOnDestroy() {
		this.body.classList.remove("skin-blue");
		this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
		this.dtTrigger.next();
	}

	rerender(): void {
		this.dtElement.dtInstance.then((dtInstance) => {
			dtInstance.destroy();
			this.dtTrigger.next();
		});
	}

	archiveSegment(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'segment'
        	}, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
		       	this.rerender();
                this.ngOnInit();
        });
	}
	addModal(){
		let disposable = this.modalService.addDialog(SegmentModalComponent, {
			title:'Create Category',
			button:'Add',
			create:true
		}).subscribe((isConfirmed)=>{
			this.rerender();
			this.ngOnInit();
		});
	}

	editModal(id:any){
		let disposable = this.modalService.addDialog(SegmentModalComponent, {
			title:'Edit Category',
			button:'Update',
			edit:true,
			id:id
		}).subscribe((isConfirmed)=>{
			this.rerender();
			this.ngOnInit();
		});
	}

}
