var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { HolidayService } from '../../services/holiday.service';
import { CommonService } from '../../services/common.service';
import { HolidayModalComponent } from '../holiday/holiday-modal/holiday-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
var HolidayComponent = /** @class */ (function () {
    function HolidayComponent(modalService, _conf, _common_service, _Holiservice) {
        this.modalService = modalService;
        this._conf = _conf;
        this._common_service = _common_service;
        this._Holiservice = _Holiservice;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api = this._conf.ServerWithApiUrl + 'overtime/';
    }
    HolidayComponent.prototype.ngOnInit = function () {
        this.data();
    };
    HolidayComponent.prototype.data = function () {
        var _this = this;
        this._Holiservice.getHolidays().
            subscribe(function (data) {
            _this.holi = data;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    HolidayComponent.prototype.add = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(HolidayModalComponent, {
            title: 'Create Holiday',
            button: 'Add',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    HolidayComponent.prototype.edit = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(HolidayModalComponent, {
            title: 'Edit Holiday',
            button: 'Update',
            edit: true,
            id: id
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    HolidayComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    HolidayComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    HolidayComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    HolidayComponent.prototype.archive = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'holiday'
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], HolidayComponent.prototype, "dtElement", void 0);
    HolidayComponent = __decorate([
        Component({
            selector: 'app-holiday',
            templateUrl: './holiday.component.html',
            styleUrls: ['./holiday.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            CommonService,
            HolidayService])
    ], HolidayComponent);
    return HolidayComponent;
}());
export { HolidayComponent };
//# sourceMappingURL=holiday.component.js.map