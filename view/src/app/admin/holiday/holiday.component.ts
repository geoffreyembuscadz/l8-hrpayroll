import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { HolidayService } from '../../services/holiday.service';
import { CommonService } from '../../services/common.service';
import { HolidayModalComponent } from '../holiday/holiday-modal/holiday-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-holiday',
  templateUrl: './holiday.component.html',
  styleUrls: ['./holiday.component.css']
})

@Injectable()
export class HolidayComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	dtOptions: any = {};
	public OT_id: any;
	api: String;
	status:any;
	holi:any;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _common_service: CommonService,
   		private _Holiservice: HolidayService
		){

		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

		this.api = this._conf.ServerWithApiUrl + 'overtime/';
	}

	ngOnInit() {
		this.data();
	}

	data(){

		this._Holiservice.getHolidays().
		subscribe(
			data => {
				this.holi = data; 
				this.rerender();
			},
			err => console.error(err)
		);

	}


	add(){
		let disposable = this.modalService.addDialog(HolidayModalComponent, {
            title:'Create Holiday',
            button:'Add',
            create:true
        	}).subscribe((isConfirmed)=>{
                this.data();
            });
	}

	edit(id:any){
		let disposable = this.modalService.addDialog(HolidayModalComponent, {
            title:'Edit Holiday',
            button:'Update',
            edit:true,
            id:id
        	}).subscribe((isConfirmed)=>{
                this.data();
            });
	}


	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

   archive(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'holiday'
        	}).subscribe((isConfirmed)=>{
		       	this.data();
        });
	}

    
	
	
}
