var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Headers } from '@angular/http';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { HolidayTypeService } from '../../../services/holiday-type.service';
var HolidayTypeModalComponent = /** @class */ (function (_super) {
    __extends(HolidayTypeModalComponent, _super);
    function HolidayTypeModalComponent(dialogService, _fb, _ar, _common_service, _holi_service, _rt, modalService) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._common_service = _common_service;
        _this._holi_service = _holi_service;
        _this._rt = _rt;
        _this.modalService = modalService;
        _this.validate = true;
        _this.start_date = new Date();
        // Header Variables
        _this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        _this.headers.append('Authorization', "Bearer " + authToken);
        _this.headers.append('Content-Type', 'application/json');
        _this.headers.append('Accept', 'application/json');
        _this.Form = _fb.group({
            'name': [null],
            'description': [null],
            'computation': [null],
        });
        return _this;
    }
    HolidayTypeModalComponent.prototype.ngOnInit = function () {
        if (this.edit == true) {
            this.validate = true;
            this.get_data();
        }
        this.get_type();
    };
    HolidayTypeModalComponent.prototype.get_data = function () {
        var _this = this;
        var id = this.id;
        this._holi_service.getHolidayType(id).subscribe(function (data) {
            _this.setType(data);
        }, function (err) { return console.error(err); });
    };
    HolidayTypeModalComponent.prototype.setType = function (type) {
        this.id = type.id;
        this.name = type.name;
        this.description = type.description;
        this.computation = type.computation;
    };
    HolidayTypeModalComponent.prototype.get_type = function () {
        var _this = this;
        this.type = this._common_service.getHoliType().
            subscribe(function (data) {
            _this.type = Array.from(data); // fetched record
            _this.type_value = [];
            if (_this.edit == true) {
                _this.type_value = _this.type_id;
                _this.validate = true;
            }
            _this.type_current = _this.type_value;
            if (_this.create == true) {
                var id = 0;
                var text = 'Select Type';
                _this.type.unshift({ id: id, text: text });
                _this.type_value = _this.value;
            }
        }, function (err) { return console.error(err); });
    };
    HolidayTypeModalComponent.prototype.changedType = function (data) {
        this.type_current = data.value;
        var len = this.type_current.length;
        if (len >= 1 && this.type_current != 0) {
            this.validate = true;
        }
        else {
            this.validate = false;
        }
    };
    HolidayTypeModalComponent.prototype.onSubmit = function () {
        var _this = this;
        var model = this.Form.value;
        if (this.edit == true) {
            var id = this.id;
            this._holi_service.updateHolidayType(id, model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data); // fetched the records
                _this.validate = false;
                _this.success_title = "Success!";
                _this.success_message = "Successfully updated";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
        if (this.create == true) {
            this._holi_service.storeHolidayType(model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data); // fetched the records
                _this.success_title = "Success!";
                _this.success_message = "Successfully created";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    HolidayTypeModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    HolidayTypeModalComponent.prototype.archive = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: this.id,
            url: 'holiday_type'
        }, { backdropColor: 'rgba(238,238,238)', closeByClickingOutside: true }).subscribe(function (isConfirmed) {
            _this.close();
        });
    };
    HolidayTypeModalComponent = __decorate([
        Component({
            selector: 'app-holiday-type-modal',
            templateUrl: './holiday-type-modal.component.html',
            styleUrls: ['./holiday-type-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            CommonService,
            HolidayTypeService,
            Router,
            DialogService])
    ], HolidayTypeModalComponent);
    return HolidayTypeModalComponent;
}(DialogComponent));
export { HolidayTypeModalComponent };
//# sourceMappingURL=holiday-type-modal.component.js.map