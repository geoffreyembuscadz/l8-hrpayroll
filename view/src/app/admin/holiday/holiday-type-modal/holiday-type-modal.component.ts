import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { HolidayTypeService } from '../../../services/holiday-type.service';
import { Select2OptionData } from 'ng2-select2';

@Component({
  selector: 'app-holiday-type-modal',
  templateUrl: './holiday-type-modal.component.html',
  styleUrls: ['./holiday-type-modal.component.css']
})
export class HolidayTypeModalComponent extends DialogComponent<null, boolean> {

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	private headers: Headers;
	public poststore: any;
	public id: any;
	public type: any;
	public name: any;
	public location:any;
	edit:any;
	create:any;
  	Form : FormGroup;
  	validate = true;
	public type_value : any;
	type_current:any;
	type_id:any;
	value:any;
	description: any;
	computation: any; 
	public start_date:Date = new Date();


constructor( 
	dialogService: DialogService, 
	private _fb: FormBuilder, 
	private _ar: ActivatedRoute, 
	private _common_service: CommonService,  
	private _holi_service: HolidayTypeService,  
	private _rt: Router,
	private modalService: DialogService,
	) {
	super(dialogService);
	  // Header Variables
	this.headers = new Headers();
	let authToken = localStorage.getItem('id_token');
	this.headers.append('Authorization', `Bearer ${authToken}`);
	this.headers.append('Content-Type', 'application/json');
	this.headers.append('Accept', 'application/json');

	this.Form = _fb.group({
	  'name':		[null],
	  'description': 	[null],
	  'computation': 	[null],
	});  
}

	ngOnInit() {

		if(this.edit == true){
			this.validate = true;
			this.get_data();
		}
			this.get_type();
	}

	get_data(){
		let id = this.id;
		this._holi_service.getHolidayType(id).subscribe(
		  data => {
		    this.setType(data);
		  },
		  err => console.error(err)
		);

	}

	public setType(type: any){
		this.id = type.id;
		this.name = type.name;
		this.description = type.description;
		this.computation = type.computation;
	}

	get_type(){
		this.type = this._common_service.getHoliType().
		subscribe(
			data => {
				this.type = Array.from(data); // fetched record

				this.type_value = [];
				if(this.edit == true) {
					this.type_value = this.type_id;
					this.validate = true;
	            }
				this.type_current = this.type_value;
				
				if(this.create == true){
					let id = 0;
			        let text = 'Select Type';

			        this.type.unshift({id,text});

			        this.type_value = this.value;
					
				}
			},
			err => console.error(err)
		);
	}

	changedType(data: any) {
	    this.type_current = data.value;
	    let len = this.type_current.length;
	      if(len >= 1 && this.type_current != 0){
	      	this.validate=true;
	      }
	      else{
	      	this.validate=false;
	      }
	}
 
  	onSubmit() {
		let model = this.Form.value;

	  	if(this.edit == true){

		    let id = this.id;

		    this._holi_service.updateHolidayType(id, model)
		    .subscribe(
		      data => {
		        this.poststore = Array.from(data); // fetched the records
		        this.validate = false;
		        this.success_title = "Success!";
		       	this.success_message = "Successfully updated";
		        setTimeout(() => {
		       this.close();
		      }, 1000);
		      },
		      err => this.catchError(err)
		    );
	  	}

	  	if(this.create == true){

	  		this._holi_service.storeHolidayType(model)
		    .subscribe(
		      data => {
		        this.poststore = Array.from(data); // fetched the records
		        this.success_title = "Success!";
		        this.success_message = "Successfully created";
		        setTimeout(() => {
		       this.close();
		      }, 1000);
		      },
		      err => this.catchError(err)
		    );
	  	}
  	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}


  	archive(){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:this.id,
            url:'holiday_type'
        	},
        	{ backdropColor: 'rgba(238,238,238)' , closeByClickingOutside:true}
        	).subscribe((isConfirmed)=>{
		       	this.close();                
        });


	}

}
