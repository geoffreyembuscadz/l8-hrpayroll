import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { HolidayTypeService } from '../../../services/holiday-type.service';
import { CommonService } from '../../../services/common.service';
import { HolidayTypeModalComponent } from '../../holiday/holiday-type-modal/holiday-type-modal.component';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-holiday-type',
  templateUrl: './holiday-type.component.html',
  styleUrls: ['./holiday-type.component.css']
})

@Injectable()
export class HolidayTypeComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	dtOptions: any = {};
	public OT_id: any;
	api: String;
	status:any;
	holi:any;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _common_service: CommonService,
   		private _Holiservice: HolidayTypeService
		){

		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

		this.api = this._conf.ServerWithApiUrl + 'holiday_type/';
	}

	ngOnInit() {
		this.data();
	}

	data(){

		this._Holiservice.getHolidayTypes().
		subscribe(
			data => {
				this.holi = data; 
				this.rerender();
			},
			err => console.error(err)
		);

	}


	add(){
		let disposable = this.modalService.addDialog(HolidayTypeModalComponent, {
            title:'Create Holiday Type',
            button:'Add',
            create:true
        	}).subscribe((isConfirmed)=>{
                this.data();
            });
	}

	edit(id:any){
		let disposable = this.modalService.addDialog(HolidayTypeModalComponent, {
            title:'Edit Holiday Type',
            button:'Update',
            edit:true,
            id:id
        	}).subscribe((isConfirmed)=>{
                this.data();
            });
	}


	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    archive(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'holiday'
        	}).subscribe((isConfirmed)=>{
                this.data();
        });
	}

    
	
	
}
