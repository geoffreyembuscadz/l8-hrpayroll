var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { HolidayTypeService } from '../../../services/holiday-type.service';
import { CommonService } from '../../../services/common.service';
import { HolidayTypeModalComponent } from '../../holiday/holiday-type-modal/holiday-type-modal.component';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
var HolidayTypeComponent = /** @class */ (function () {
    function HolidayTypeComponent(modalService, _conf, _common_service, _Holiservice) {
        this.modalService = modalService;
        this._conf = _conf;
        this._common_service = _common_service;
        this._Holiservice = _Holiservice;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api = this._conf.ServerWithApiUrl + 'holiday_type/';
    }
    HolidayTypeComponent.prototype.ngOnInit = function () {
        this.data();
    };
    HolidayTypeComponent.prototype.data = function () {
        var _this = this;
        this._Holiservice.getHolidayTypes().
            subscribe(function (data) {
            _this.holi = data;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    HolidayTypeComponent.prototype.add = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(HolidayTypeModalComponent, {
            title: 'Create Holiday Type',
            button: 'Add',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    HolidayTypeComponent.prototype.edit = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(HolidayTypeModalComponent, {
            title: 'Edit Holiday Type',
            button: 'Update',
            edit: true,
            id: id
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    HolidayTypeComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    HolidayTypeComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    HolidayTypeComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    HolidayTypeComponent.prototype.archive = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'holiday'
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], HolidayTypeComponent.prototype, "dtElement", void 0);
    HolidayTypeComponent = __decorate([
        Component({
            selector: 'app-holiday-type',
            templateUrl: './holiday-type.component.html',
            styleUrls: ['./holiday-type.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            CommonService,
            HolidayTypeService])
    ], HolidayTypeComponent);
    return HolidayTypeComponent;
}());
export { HolidayTypeComponent };
//# sourceMappingURL=holiday-type.component.js.map