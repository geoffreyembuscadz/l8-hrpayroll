import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Select2OptionData } from 'ng2-select2';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { HolidayService } from '../../../services/holiday.service';

@Component({
  selector: 'app-holiday-modal',
  templateUrl: './holiday-modal.component.html',
  styleUrls: ['./holiday-modal.component.css']
})
export class HolidayModalComponent extends DialogComponent<null, boolean> {

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	private headers: Headers;
	public poststore: any;
	public id: any;
	public type: any;
	public name: any;
	public location:any;
	edit:any;
	create:any;
  	Form : FormGroup;
  	validate = true;
	public type_value : any;
	type_current:any;
	type_id:any;
	value:any;
	public mainInput = {
        start: moment(),
        end: moment().subtract(6, 'month')
    }

    date:any;


constructor( 
	dialogService: DialogService, 
	private _fb: FormBuilder, 
	private _ar: ActivatedRoute, 
	private _common_service: CommonService,  
	private _holi_service: HolidayService,  
	private _rt: Router,
	private modalService: DialogService,
	private daterangepickerOptions: DaterangepickerConfig,
	) {
	super(dialogService);

	this.Form = _fb.group({
	  'name':		[null],
	  'location': 	[null],
	  'date':		[null]
	});  

	this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
    		showDropdowns: true,
    		opens: "center"
	}; 
}

	ngOnInit() {

		if(this.edit == true){
			this.validate = true;
			this.get_data();
		}
		this.get_type();
	}

	get_data(){
		let id = this.id;
		this._holi_service.getHoliday(id).subscribe(
		  data => {
		    this.setType(data);
		  },
		  err => console.error(err)
		);

	}

	setType(type: any){
		this.id = type.id;
		this.name = type.name;
		this.location = type.location;
		this.type_id = type.holiday_type_id;
		this.mainInput.start = type.date;
	}

	get_type(){
		this.type = this._common_service.getHoliType().
		subscribe(
			data => {
				this.type = Array.from(data);

				this.type_value = [];
				if(this.edit == true) {
					this.type_value = this.type_id;
					this.validate = true;
	            }

				this.type_current = this.type_value;
				
				if(this.create == true){
					let id = 0;
			        let text = 'Select Type';

			        this.type.unshift({id,text});

			        this.type_value = this.value;
					
				}
			},
			err => console.error(err)
		);
	}

	changedType(data: any) {
	    this.type_current = data.value;
	    let len = this.type_current.length;
	      if(len >= 1 && this.type_current != 0){
	      	this.validate=true;
	      }
	      else{
	      	this.validate=false;
	      }
	}

	private selectedDate(value:any, dateInput:any) {
        dateInput.start = value.start;

       	this.date = moment(dateInput.start).format("YYYY-MM-DD");
    }
 
  	onSubmit() {
  		this.Form.value.holiday_type_id = this.type_current;
  		this.Form.value.date = this.date;

  		if (this.date == null) {
			this.Form.value.date = moment(this.mainInput.start).format("YYYY-MM-DD");
		}
		let model = this.Form.value;

	  	if(this.edit == true){

		    let id = this.id;

		    this._holi_service.updateHoliday(id, model)
		    .subscribe(
		      data => {
		        this.poststore = Array.from(data);
		        this.validate = false;
		        this.success_title = "Success!";
		       	this.success_message = "Successfully updated";
		        setTimeout(() => {
		       this.close();
		      }, 1000);
		      },
		      err => this.catchError(err)
		    );
	  	}

	  	if(this.create == true){

	  		this._holi_service.storeHoliday(model)
		    .subscribe(
		      data => {
		        this.poststore = Array.from(data);
		        this.success_title = "Success!";
		        this.success_message = "Successfully created";
		        setTimeout(() => {
		       this.close();
		      }, 1000);
		      },
		      err => this.catchError(err)
		    );
	  	}
  	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}


  	archive(){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:this.id,
            url:'holiday'
        	}).subscribe((isConfirmed)=>{
		       	this.close();
        });


	}

}
