var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import * as moment from 'moment';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { HolidayService } from '../../../services/holiday.service';
var HolidayModalComponent = /** @class */ (function (_super) {
    __extends(HolidayModalComponent, _super);
    function HolidayModalComponent(dialogService, _fb, _ar, _common_service, _holi_service, _rt, modalService, daterangepickerOptions) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._common_service = _common_service;
        _this._holi_service = _holi_service;
        _this._rt = _rt;
        _this.modalService = modalService;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this.validate = true;
        _this.mainInput = {
            start: moment(),
            end: moment().subtract(6, 'month')
        };
        _this.Form = _fb.group({
            'name': [null],
            'location': [null],
            'date': [null]
        });
        _this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
            showDropdowns: true,
            opens: "center"
        };
        return _this;
    }
    HolidayModalComponent.prototype.ngOnInit = function () {
        if (this.edit == true) {
            this.validate = true;
            this.get_data();
        }
        this.get_type();
    };
    HolidayModalComponent.prototype.get_data = function () {
        var _this = this;
        var id = this.id;
        this._holi_service.getHoliday(id).subscribe(function (data) {
            _this.setType(data);
        }, function (err) { return console.error(err); });
    };
    HolidayModalComponent.prototype.setType = function (type) {
        this.id = type.id;
        this.name = type.name;
        this.location = type.location;
        this.type_id = type.holiday_type_id;
        this.mainInput.start = type.date;
    };
    HolidayModalComponent.prototype.get_type = function () {
        var _this = this;
        this.type = this._common_service.getHoliType().
            subscribe(function (data) {
            _this.type = Array.from(data);
            _this.type_value = [];
            if (_this.edit == true) {
                _this.type_value = _this.type_id;
                _this.validate = true;
            }
            _this.type_current = _this.type_value;
            if (_this.create == true) {
                var id = 0;
                var text = 'Select Type';
                _this.type.unshift({ id: id, text: text });
                _this.type_value = _this.value;
            }
        }, function (err) { return console.error(err); });
    };
    HolidayModalComponent.prototype.changedType = function (data) {
        this.type_current = data.value;
        var len = this.type_current.length;
        if (len >= 1 && this.type_current != 0) {
            this.validate = true;
        }
        else {
            this.validate = false;
        }
    };
    HolidayModalComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        this.date = moment(dateInput.start).format("YYYY-MM-DD");
    };
    HolidayModalComponent.prototype.onSubmit = function () {
        var _this = this;
        this.Form.value.holiday_type_id = this.type_current;
        this.Form.value.date = this.date;
        if (this.date == null) {
            this.Form.value.date = moment(this.mainInput.start).format("YYYY-MM-DD");
        }
        var model = this.Form.value;
        if (this.edit == true) {
            var id = this.id;
            this._holi_service.updateHoliday(id, model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.validate = false;
                _this.success_title = "Success!";
                _this.success_message = "Successfully updated";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
        if (this.create == true) {
            this._holi_service.storeHoliday(model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success!";
                _this.success_message = "Successfully created";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    HolidayModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    HolidayModalComponent.prototype.archive = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: this.id,
            url: 'holiday'
        }).subscribe(function (isConfirmed) {
            _this.close();
        });
    };
    HolidayModalComponent = __decorate([
        Component({
            selector: 'app-holiday-modal',
            templateUrl: './holiday-modal.component.html',
            styleUrls: ['./holiday-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            CommonService,
            HolidayService,
            Router,
            DialogService,
            DaterangepickerConfig])
    ], HolidayModalComponent);
    return HolidayModalComponent;
}(DialogComponent));
export { HolidayModalComponent };
//# sourceMappingURL=holiday-modal.component.js.map