import { Component } from '@angular/core';
import { Configuration } from '../../app.config';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { OnboardingService } from '../../services/onboarding.service';
import { ClearanceService } from '../../services/clearance.service';

export interface PromptModel {
  title:any;
  data:any;
  onboarding:any;
  clearance:any;
  attend:any;
  holiday:any;
  tableVal:any;
  attendModal:any;
}


@Component({
  selector: 'app-checklist-modal',
  templateUrl: './checklist-modal.component.html',
  styleUrls: ['./checklist-modal.component.css']
})
export class ChecklistModalComponent extends DialogComponent<PromptModel, any> implements PromptModel {
  
	title: string;
	public id: any;
	public status: any;
	public name: string;
	public success_title;
	public success_message;
	public error_title;
	public error_message;
	public poststore: any;

	data = [];
	tableVal = false;
	checkAllValidation = false;
	checkAllVal = false;
	btnVal = false;
	loader = false;
	onboarding = false;
	clearance = false;
	attend = false;
	attendModal = false;
	holiday = [];

	constructor( 
		dialogService: DialogService, 
		private _ar: ActivatedRoute,
   		private _onboarding_service: OnboardingService,
   		private _clearance_service: ClearanceService,
	){
    super(dialogService);


  
  }
	ngOnInit() {
		this.validation();
	}

	validation(){
		//checklist in onboarding-checklist and clearance checklist
		if (this.clearance || this.onboarding) {
			let x =0;
			for (let i = 0; i < this.data.length; ++i) {
				if (this.data[i].status != 'DONE') {
					x++;
				}
			}

			if (x==0) {
				this.checkAllVal = false;
			}else{
				this.checkAllVal = true;
			}
			this.loader = false;
		}
		if(this.attend){

			let len = this.data.length;
			let leng = this.holiday.length;
			this.result =[];

			for (let x = 0; x < len; ++x) {
					this.data[x].val = false;
				for (let y = 0; y < leng; ++y) {
					if (this.holiday[y].date == this.data[x].date) {
						this.data[x].val = true;
					}
				}
			}
		}
		//holiday in attendance create day view
		if(this.attendModal){

			let len = this.data.length;
			let leng = this.holiday.length;
			this.result =[];

			for (let x = 0; x < len; ++x) {
					this.data[x].val = false;
				for (let y = 0; y < leng; ++y) {
					if (this.holiday[y] == this.data[x].id) {
						this.data[x].val = true;
					}
				}
			}
		}
	}
 


	 private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	cancel(){
		this.close();
	}

	checkbox(index:any){

		if(this.data[index].val == false){
			this.data[index].val = true;
		}
		else{
			this.data[index].val = false;
		}

		let len = this.data.length;
		let i = 0;
		for (let x = 0; x < len; ++x) {
			
			if(this.data[x].val == true){
				i++;
			}
		}

		if(i > 0){
			this.btnVal = true;
		}
		else{
			this.btnVal = false;
		}
	}

	checkAll(){

		if(this.checkAllValidation == false){
			let leng = this.data.length;
			for (let i = 0; i < leng; i++) {
				this.data[i].val = true;
			}
			this.checkAllValidation = true;
			this.btnVal = true;
		}
		else{
			let len = this.data.length;
			for (let i = 0; i < len; i++) {
				this.data[i].val = false;
			}
			this.checkAllValidation = false;
			this.btnVal = false;
		}
	}

	submit(){

		if (this.onboarding == true) {

			let len = this.data.length;
			let temp = [];
			let remarks = null;
			for (let i = 0; i < len; ++i) {
				if (this.data[i].val == true) {
					let id = this.data[i].onboarding_list_id;
					let remarks = this.data[i].remarks;
					temp.push({id,remarks})
				}
			}

			let model = {
				onboarding: temp,
				employee_id: this.data[0].employee_id,
				table_id: this.data[0].employee_onboarding_id
			};

			this.checkAllVal = false;
			this.tableVal = false;
			this.loader = true;

			this._onboarding_service.updateStatus(model).
			subscribe(
				data => {
					let user = data;
					this.loader = false;

					this.success_title = "Success";
					this.success_message = "Successfully updated.";
					setTimeout(() => {
			  		   this.close();
			      	}, 2000);

				},
				err => console.error(err)
			);
		}
		else if (this.clearance == true) {

			console.log(this.data)

			let len = this.data.length;
			let temp = [];
			for (let i = 0; i < len; ++i) {
				if (this.data[i].val == true) {
					temp.push(this.data[i].clearance_list_id);
				}
			}

			let model = {
				id: temp
			};

			this.checkAllVal = false;
			this.tableVal = false;
			this.loader = true;

			// this._clearance_service.updateStatus(model).
			// subscribe(
			// 	data => {
			// 		let user = data;
			// 		this.loader = false;

			// 		this.success_title = "Success";
			// 		this.success_message = "Successfully updated.";
			// 		setTimeout(() => {
			//   		   this.close();
			//       	}, 2000);

			// 	},
			// 	err => console.error(err)
			// );
		}

		if (this.attend || this.attendModal) {

			let len = this.data.length;
			let temp =[];
			for (let i = 0; i < len; ++i) {

				if (this.data[i].val == true) {
					let date = this.data[i].date;
					let id = this.data[i].id;
					let type = this.data[i].type;
					temp.push({date,type,id});
				}
			}

			this.result = temp;
			this.success_title = "Success";
			this.success_message = "Successfully updated.";
			setTimeout(() => {
	  		   this.close();
	      	}, 2000);
		}
	}

	closes(){
		this.result = 1;
		this.close();
	}

}