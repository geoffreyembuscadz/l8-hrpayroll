var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { ActivatedRoute } from '@angular/router';
import { OnboardingService } from '../../services/onboarding.service';
import { ClearanceService } from '../../services/clearance.service';
var ChecklistModalComponent = /** @class */ (function (_super) {
    __extends(ChecklistModalComponent, _super);
    function ChecklistModalComponent(dialogService, _ar, _onboarding_service, _clearance_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._ar = _ar;
        _this._onboarding_service = _onboarding_service;
        _this._clearance_service = _clearance_service;
        _this.data = [];
        _this.tableVal = false;
        _this.checkAllValidation = false;
        _this.checkAllVal = false;
        _this.btnVal = false;
        _this.loader = false;
        _this.onboarding = false;
        _this.clearance = false;
        _this.attend = false;
        _this.attendModal = false;
        _this.holiday = [];
        return _this;
    }
    ChecklistModalComponent.prototype.ngOnInit = function () {
        this.validation();
    };
    ChecklistModalComponent.prototype.validation = function () {
        //checklist in onboarding-checklist and clearance checklist
        if (this.clearance || this.onboarding) {
            var x = 0;
            for (var i = 0; i < this.data.length; ++i) {
                if (this.data[i].status != 'DONE') {
                    x++;
                }
            }
            if (x == 0) {
                this.checkAllVal = false;
            }
            else {
                this.checkAllVal = true;
            }
            this.loader = false;
        }
        if (this.attend) {
            var len = this.data.length;
            var leng = this.holiday.length;
            this.result = [];
            for (var x = 0; x < len; ++x) {
                this.data[x].val = false;
                for (var y = 0; y < leng; ++y) {
                    if (this.holiday[y].date == this.data[x].date) {
                        this.data[x].val = true;
                    }
                }
            }
        }
        //holiday in attendance create day view
        if (this.attendModal) {
            var len = this.data.length;
            var leng = this.holiday.length;
            this.result = [];
            for (var x = 0; x < len; ++x) {
                this.data[x].val = false;
                for (var y = 0; y < leng; ++y) {
                    if (this.holiday[y] == this.data[x].id) {
                        this.data[x].val = true;
                    }
                }
            }
        }
    };
    ChecklistModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    ChecklistModalComponent.prototype.cancel = function () {
        this.close();
    };
    ChecklistModalComponent.prototype.checkbox = function (index) {
        if (this.data[index].val == false) {
            this.data[index].val = true;
        }
        else {
            this.data[index].val = false;
        }
        var len = this.data.length;
        var i = 0;
        for (var x = 0; x < len; ++x) {
            if (this.data[x].val == true) {
                i++;
            }
        }
        if (i > 0) {
            this.btnVal = true;
        }
        else {
            this.btnVal = false;
        }
    };
    ChecklistModalComponent.prototype.checkAll = function () {
        if (this.checkAllValidation == false) {
            var leng = this.data.length;
            for (var i = 0; i < leng; i++) {
                this.data[i].val = true;
            }
            this.checkAllValidation = true;
            this.btnVal = true;
        }
        else {
            var len = this.data.length;
            for (var i = 0; i < len; i++) {
                this.data[i].val = false;
            }
            this.checkAllValidation = false;
            this.btnVal = false;
        }
    };
    ChecklistModalComponent.prototype.submit = function () {
        var _this = this;
        if (this.onboarding == true) {
            var len = this.data.length;
            var temp = [];
            var remarks = null;
            for (var i = 0; i < len; ++i) {
                if (this.data[i].val == true) {
                    var id = this.data[i].onboarding_list_id;
                    var remarks_1 = this.data[i].remarks;
                    temp.push({ id: id, remarks: remarks_1 });
                }
            }
            var model = {
                onboarding: temp,
                employee_id: this.data[0].employee_id,
                table_id: this.data[0].employee_onboarding_id
            };
            this.checkAllVal = false;
            this.tableVal = false;
            this.loader = true;
            this._onboarding_service.updateStatus(model).
                subscribe(function (data) {
                var user = data;
                _this.loader = false;
                _this.success_title = "Success";
                _this.success_message = "Successfully updated.";
                setTimeout(function () {
                    _this.close();
                }, 2000);
            }, function (err) { return console.error(err); });
        }
        else if (this.clearance == true) {
            console.log(this.data);
            var len = this.data.length;
            var temp = [];
            for (var i = 0; i < len; ++i) {
                if (this.data[i].val == true) {
                    temp.push(this.data[i].clearance_list_id);
                }
            }
            var model = {
                id: temp
            };
            this.checkAllVal = false;
            this.tableVal = false;
            this.loader = true;
            // this._clearance_service.updateStatus(model).
            // subscribe(
            // 	data => {
            // 		let user = data;
            // 		this.loader = false;
            // 		this.success_title = "Success";
            // 		this.success_message = "Successfully updated.";
            // 		setTimeout(() => {
            //   		   this.close();
            //       	}, 2000);
            // 	},
            // 	err => console.error(err)
            // );
        }
        if (this.attend || this.attendModal) {
            var len = this.data.length;
            var temp = [];
            for (var i = 0; i < len; ++i) {
                if (this.data[i].val == true) {
                    var date = this.data[i].date;
                    var id = this.data[i].id;
                    var type = this.data[i].type;
                    temp.push({ date: date, type: type, id: id });
                }
            }
            this.result = temp;
            this.success_title = "Success";
            this.success_message = "Successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 2000);
        }
    };
    ChecklistModalComponent.prototype.closes = function () {
        this.result = 1;
        this.close();
    };
    ChecklistModalComponent = __decorate([
        Component({
            selector: 'app-checklist-modal',
            templateUrl: './checklist-modal.component.html',
            styleUrls: ['./checklist-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            ActivatedRoute,
            OnboardingService,
            ClearanceService])
    ], ChecklistModalComponent);
    return ChecklistModalComponent;
}(DialogComponent));
export { ChecklistModalComponent };
//# sourceMappingURL=checklist-modal.component.js.map