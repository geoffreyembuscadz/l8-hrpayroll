import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { LeaveTypeService } from '../../services/leave-type.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { TypeModalComponent } from '../type-modal/type-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-leave-type',
  templateUrl: './leave-type.component.html',
  styleUrls: ['./leave-type.component.css']
})

@Injectable()
export class LeaveTypeComponent implements OnInit, AfterViewInit {
	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};

	leave:any;

	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	constructor(
  		dialogService: DialogService, 
  		private modalService: DialogService,
  		private _leave_type_service: LeaveTypeService
  		){
		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");


	}

	ngAfterViewInit(){
		this.dtTrigger.next();
	}

	ngOnInit() {
		this.data();
	}
	data(){

    	this._leave_type_service.getLeaves().
	      	subscribe(
	        data => {
	          this.leave= Array.from(data);
	          this.rerender();
	        },
	        err => console.error(err)
	     );

	}

	edit(id:any) {
		let disposable = this.modalService.addDialog(TypeModalComponent, {
            title:'Update Leave Type',
            button:'Update',
		    edit:true,
		    url:'leave_type',
		    id:id
        	}).subscribe((isConfirmed)=>{
                this.data();
        });
	}


	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	 }
	add() {
		let disposable = this.modalService.addDialog(TypeModalComponent, {
            title:'Add Leave Type',
            button:'Add',
		    url:'leave_type',
		    create:true
        	}).subscribe((isConfirmed)=>{
                this.data();
            });
		}
	rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    archive(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'leave_type'
        	}).subscribe((isConfirmed)=>{
		       	this.data();
        });
	}

    
}
