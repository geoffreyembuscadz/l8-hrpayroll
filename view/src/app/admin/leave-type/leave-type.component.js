var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { LeaveTypeService } from '../../services/leave-type.service';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { TypeModalComponent } from '../type-modal/type-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
var LeaveTypeComponent = /** @class */ (function () {
    function LeaveTypeComponent(dialogService, modalService, _leave_type_service) {
        this.modalService = modalService;
        this._leave_type_service = _leave_type_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    LeaveTypeComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    LeaveTypeComponent.prototype.ngOnInit = function () {
        this.data();
    };
    LeaveTypeComponent.prototype.data = function () {
        var _this = this;
        this._leave_type_service.getLeaves().
            subscribe(function (data) {
            _this.leave = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    LeaveTypeComponent.prototype.edit = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Update Leave Type',
            button: 'Update',
            edit: true,
            url: 'leave_type',
            id: id
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    LeaveTypeComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    LeaveTypeComponent.prototype.add = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Add Leave Type',
            button: 'Add',
            url: 'leave_type',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    LeaveTypeComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    LeaveTypeComponent.prototype.archive = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'leave_type'
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], LeaveTypeComponent.prototype, "dtElement", void 0);
    LeaveTypeComponent = __decorate([
        Component({
            selector: 'app-leave-type',
            templateUrl: './leave-type.component.html',
            styleUrls: ['./leave-type.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            DialogService,
            LeaveTypeService])
    ], LeaveTypeComponent);
    return LeaveTypeComponent;
}());
export { LeaveTypeComponent };
//# sourceMappingURL=leave-type.component.js.map