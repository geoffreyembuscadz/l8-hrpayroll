var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Configuration } from '../../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../../services/common.service';
import { OvertimeService } from '../../../services/overtime.service';
import { Overtime } from '../../../model/overtime';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';
var OvertimeModalComponent = /** @class */ (function (_super) {
    __extends(OvertimeModalComponent, _super);
    function OvertimeModalComponent(_common_service, _OTservice, dialogService, _fb, _ar, daterangepickerOptions, _conf) {
        var _this = _super.call(this, dialogService) || this;
        _this._common_service = _common_service;
        _this._OTservice = _OTservice;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this._conf = _conf;
        _this.ot = new Overtime();
        _this.ismeridian = true;
        _this.start_time = new Date();
        _this.end_time = new Date();
        _this.mainInput = {
            start: moment(),
            end: moment().subtract(6, 'month')
        };
        _this.validate = false;
        _this.user = false;
        _this.admin = false;
        _this.empStats = false;
        _this.dupVal = false;
        _this.host = _this._conf.socketUrl;
        _this.OvertimeForm = _fb.group({
            'start_time': [_this.ot.start_time, [Validators.required]],
            'end_time': [_this.ot.end_time, [Validators.required]],
            'status_id': [null],
            'remarks': [_this.ot.remarks, [Validators.required]]
        });
        _this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
            showDropdowns: true,
            opens: "center"
        };
        return _this;
    }
    OvertimeModalComponent.prototype.ngOnInit = function () {
        if (this.user == true) {
            this.empStats = true;
            this.get_employee();
        }
        if (this.edit == true) {
            this.getOT();
            this.validate = true;
            this.get_employee();
        }
        else {
            this.get_employee();
        }
        //uncomment this for socket.io
        // this.connectToServer();
    };
    OvertimeModalComponent.prototype.getOT = function () {
        var _this = this;
        this._OTservice.getOT(this.ot_id).subscribe(function (data) {
            _this.ot_data = (data);
            var start = _this.ot_data.date + " " + _this.ot_data.start_time;
            var end = _this.ot_data.date + " " + _this.ot_data.end_time;
            _this.start_time = new Date(start);
            _this.end_time = new Date(end);
            _this.remarks = _this.ot_data.reason;
            _this.employee_id = _this.ot_data.emp_id;
            _this.mainInput.start = _this.ot_data.date;
        }, function (err) { return console.error(err); });
    };
    OvertimeModalComponent.prototype.get_employee = function () {
        this.employee = this.emp;
        this.employee_value = [];
        if (this.edit == true) {
            this.employee_value = this.employee_id;
            this.validate = true;
        }
        this.options = {
            multiple: true
        };
        this.current = this.employee_value;
    };
    OvertimeModalComponent.prototype.changed = function (data) {
        if (this.single == true) {
            var v = [];
            v.push(data.value);
            this.current = v;
        }
        else {
            this.current = data.value;
        }
        var len = this.current.length;
        if (len >= 1) {
            this.empStats = true;
        }
        else {
            this.empStats = false;
        }
        this.validation();
        this.duplicateEntryValidation();
    };
    OvertimeModalComponent.prototype.validation = function () {
        if (this.empStats == true && this.dupVal == true || this.edit == true) {
            this.validate = true;
        }
        else {
            this.validate = false;
        }
    };
    OvertimeModalComponent.prototype.submit = function () {
        var _this = this;
        this.duplicateEntryValidation();
        var ot = this.OvertimeForm.value;
        if (this.edit == true && this.user == false) {
            this.OvertimeForm.value.id = this.ot_id;
            ot = this.OvertimeForm.value;
            this._OTservice
                .updateOT(ot)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.result = true;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        else if (this.edit == true && this.user == true) {
            this.OvertimeForm.value.id = this.ot_id;
            ot = this.OvertimeForm.value;
            this._OTservice
                .updateMyOT(ot)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.result = true;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        if (this.create == true && this.user == false) {
            this._OTservice
                .createOT(ot)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        else if (this.create == true && this.user == true) {
            this._OTservice
                .createMyOT(ot)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
    };
    OvertimeModalComponent.prototype.connectToServer = function () {
        var _this = this;
        var socketUrl = this.host;
        this.socket = io.connect(socketUrl);
        this.socket.on("connect", function () { return _this.connect(); });
        this.socket.on("disconnect", function () { return _this.disconnect(); });
        this.socket.on("error", function (error) {
            console.log("ERROR: \"" + error + "\" (" + socketUrl + ")");
        });
    };
    // Handle connection opening
    OvertimeModalComponent.prototype.connect = function () {
        // Request initial list when connected
        this.socket.emit("join", 'Client User -> ');
    };
    // Handle connection closing
    OvertimeModalComponent.prototype.disconnect = function () {
        console.log("Disconnected from \"" + this.host + "\"");
    };
    OvertimeModalComponent.prototype.errorCatch = function () {
        var _this = this;
        if (this.poststore.status_code == 200) {
            this.success_title = 'Success';
            this.success_message = 'No Changes Happen';
            setTimeout(function () {
                _this.close();
            }, 1000);
        }
        else if (this.poststore.status_code == 400) {
            this.error_title = 'Warning';
            this.error_message = 'Request is already exist';
            var temp_1;
            setTimeout(function () {
                var disposable = _this.dialogService.addDialog(ValidationModalComponent, {
                    title: 'Warning',
                    message: 'Overtime',
                    url: 'overtime',
                    duplicate: true,
                    data: _this.poststore.data,
                    userDup: _this.user,
                    adminDup: _this.admin,
                    emp: _this.emp
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.error_title = '';
                        _this.error_message = '';
                        setTimeout(function () {
                            _this.close();
                        }, 1000);
                    }
                    else if (message != false) {
                        temp_1 = message;
                        for (var x = 0; x < temp_1.length; ++x) {
                            var index = _this.current.indexOf(temp_1[x]);
                            _this.current.splice(index, 1);
                        }
                        _this.employee_value = _this.current;
                        if (_this.current.length == 0) {
                            _this.error_title = '';
                            _this.error_message = '';
                            setTimeout(function () {
                                _this.close();
                            }, 1000);
                        }
                    }
                });
            }, 1000);
        }
        else {
            // socket.io emit
            // this.socket.emit('notify', this.poststore);
            this.validate = false;
            this.success_title = "Success";
            this.success_message = "Successfully";
            setTimeout(function () {
                _this.close();
            }, 2000);
        }
    };
    OvertimeModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    //time picker
    OvertimeModalComponent.prototype.toggleMode = function () {
        this.ismeridian = !this.ismeridian;
    };
    OvertimeModalComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        this.date = moment(dateInput.start).format("YYYY-MM-DD");
        this.duplicateEntryValidation();
    };
    OvertimeModalComponent.prototype.duplicateEntryValidation = function () {
        var _this = this;
        this.OvertimeForm.value.employee_id = this.current;
        this.OvertimeForm.value.created_by = this.user_id;
        this.OvertimeForm.value.supervisor_id = this.supervisor_id;
        this.OvertimeForm.value.start_time = moment(this.start_time).format("HH:mm");
        this.OvertimeForm.value.end_time = moment(this.end_time).format("HH:mm");
        this.OvertimeForm.value.date = this.date;
        if (this.date == null) {
            this.OvertimeForm.value.date = moment(this.mainInput.start).format("YYYY-MM-DD");
        }
        var hours = moment
            .duration(moment(this.end_time, 'HH:mm')
            .diff(moment(this.start_time, 'HH:mm'))).asHours();
        this.OvertimeForm.value.total_hours = hours;
        if (this.user == true && this.create == true || this.user == true && this.edit == true) {
            var v = [];
            v.push(this.employee_id);
            this.OvertimeForm.value.employee_id = v;
        }
        if (this.edit == true) {
            this.OvertimeForm.value.edit = this.edit;
            this.OvertimeForm.value.id = this.ot_id;
        }
        else {
            this.OvertimeForm.value.edit = false;
        }
        var ot = this.OvertimeForm.value;
        if (this.OvertimeForm.value.employee_id != 0 && this.OvertimeForm.value.date != null || this.edit == true) {
            this._OTservice
                .duplicateEntryValidation(ot)
                .subscribe(function (data) {
                _this.poststore = data;
                if (_this.poststore.message == "duplicate") {
                    _this.dupVal = false;
                    _this.errorCatch();
                }
                else {
                    _this.dupVal = true;
                    _this.error_title = '';
                    _this.error_message = '';
                }
                _this.validation();
            }, function (err) { return _this.catchError(err); });
        }
    };
    OvertimeModalComponent = __decorate([
        Component({
            selector: 'app-overtime-modal',
            templateUrl: './overtime-modal.component.html',
            styleUrls: ['./overtime-modal.component.css']
        }),
        __metadata("design:paramtypes", [CommonService,
            OvertimeService,
            DialogService,
            FormBuilder,
            ActivatedRoute,
            DaterangepickerConfig,
            Configuration])
    ], OvertimeModalComponent);
    return OvertimeModalComponent;
}(DialogComponent));
export { OvertimeModalComponent };
//# sourceMappingURL=overtime-modal.component.js.map