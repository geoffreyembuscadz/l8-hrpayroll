import { Component } from '@angular/core';
import { Configuration } from '../../../app.config';
import { Observable, Subscription } from "rxjs";
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../../services/common.service';
import { OvertimeService } from '../../../services/overtime.service';
import { Overtime } from '../../../model/overtime';
import { Select2OptionData } from 'ng2-select2';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TimepickerModule } from 'ngx-bootstrap';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';

@Component({
  selector: 'app-overtime-modal',
  templateUrl: './overtime-modal.component.html',
  styleUrls: ['./overtime-modal.component.css']
})
export class OvertimeModalComponent extends DialogComponent<null, boolean> {

 	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public options: Select2Options;
	public current: any;
	public type : any;
	public type_value : any;
	ot = new Overtime();
	OvertimeForm:any;
	emp:any;
	poststore:any;
	user_id:any;
	public ismeridian:boolean = true;
	public start_time:Date = new Date();
	public end_time:Date = new Date();
	public mainInput = {
        start: moment(),
        end: moment().subtract(6, 'month')
    }
    validate = false;
    employee_id:any;
    ot_id:any;
    ot_data:any;
    remarks:any;
    edit:any;
    create:any;
    multi:any;
    single:any;
    date:any;
    user=false;
    admin=false;
    empStats=false;
    dupVal=false;
    supervisor_id:any;
    role_id:any;

    socket: SocketIOClient.Socket;
    host:any;

    messenger_id:any;

  constructor(
  	private _common_service: CommonService,
   	private _OTservice: OvertimeService, 
   	dialogService: DialogService, 
   	private _fb: FormBuilder, 
   	private _ar: ActivatedRoute,
   	private daterangepickerOptions: DaterangepickerConfig,
   	private _conf: Configuration
  	){ 
  	
  	super(dialogService);

  	this.host = this._conf.socketUrl;

    this.OvertimeForm = _fb.group({
		'start_time': 		[this.ot.start_time, [Validators.required]],
		'end_time': 		[this.ot.end_time, [Validators.required]],
		'status_id':		[null],
		'remarks': 			[this.ot.remarks, [Validators.required]]
    }); 

    this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
    		showDropdowns: true,
    		opens: "center"
	}; 

  }

	ngOnInit() {
    	if(this.user == true){
    		this.empStats = true;
    		this.get_employee();
    	}
		if(this.edit == true){
			this.getOT();
    		this.validate = true;
    		this.get_employee();
    	}
    	else{
	   		this.get_employee();
    	}
	   		//uncomment this for socket.io
    		// this.connectToServer();
 	}

	getOT(){

		this._OTservice.getOT(this.ot_id).subscribe(
	      data => {
	        this.ot_data=(data);

	        let start = this.ot_data.date + " " + this.ot_data.start_time;
	        let end = this.ot_data.date + " " + this.ot_data.end_time;
	        this.start_time = new Date(start);
	        this.end_time = new Date(end);
	        this.remarks = this.ot_data.reason;
	        this.employee_id= this.ot_data.emp_id;
	        this.mainInput.start = this.ot_data.date;
	      },
	      err => console.error(err)
    	);
    	
	}

	get_employee(){

		this.employee = this.emp;
      	this.employee_value = [];

		if(this.edit == true) {
			this.employee_value = this.employee_id;
			this.validate = true;
	    } 
		this.options = {
			multiple: true
		}
		this.current = this.employee_value;
	}
	
	changed(data: any) {

	  	if(this.single == true){
			let v = [];
			v.push(data.value);
			this.current = v;
		}
		else{
			this.current = data.value;	
		}
	   	let len = this.current.length;

	      if(len >= 1){
	      	this.empStats = true;
	      }
	      else{
	      	this.empStats = false;
	      }

	    this.validation();
	    this.duplicateEntryValidation();
	}	

	validation(){
		
		if(this.empStats == true && this.dupVal == true|| this.edit == true){
      		this.validate=true;
     	}
     	else{
     		this.validate=false;
     	}
	}

	submit(){

		this.duplicateEntryValidation();

		let ot = this.OvertimeForm.value;

		if(this.edit == true && this.user == false){
			this.OvertimeForm.value.id= this.ot_id;
			ot = this.OvertimeForm.value;

			this._OTservice
			.updateOT(ot)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}
		else if(this.edit == true && this.user == true){
			this.OvertimeForm.value.id= this.ot_id;
			ot = this.OvertimeForm.value;

			this._OTservice
			.updateMyOT(ot)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}

		if(this.create == true && this.user == false){
			this._OTservice
			.createOT(ot)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}
		else if(this.create == true && this.user == true){
			this._OTservice
			.createMyOT(ot)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}

	}

	connectToServer(){
	    let socketUrl = this.host;
	    this.socket = io.connect(socketUrl);
	    this.socket.on("connect", () => this.connect());
	    this.socket.on("disconnect", () => this.disconnect());
	    
	    this.socket.on("error", (error: string) => {
	      console.log(`ERROR: "${error}" (${socketUrl})`);
	    });

	   
	  }

	// Handle connection opening
	private connect() {
	    // Request initial list when connected
	    this.socket.emit("join", 'Client User -> ');
	}
	// Handle connection closing
	private disconnect() {
	    console.log(`Disconnected from "${this.host}"`);
	}

	errorCatch(){
		if(this.poststore.status_code == 200){
			this.success_title = 'Success';
			this.success_message = 'No Changes Happen';
			setTimeout(() => {
	  		   this.close();
	      	}, 1000);
		}
		else if(this.poststore.status_code == 400){

			this.error_title = 'Warning';
			this.error_message = 'Request is already exist';

			let temp;
			setTimeout(() => {
				let disposable = this.dialogService.addDialog(ValidationModalComponent, {
				title:'Warning',
				message:'Overtime',
				url:'overtime',
				duplicate:true,
				data:this.poststore.data,
				userDup:this.user,
				adminDup:this.admin,
            	emp:this.emp
				}).subscribe((message)=>{

					if (message == true) { 
						this.error_title = '';
						this.error_message = '';

						setTimeout(() => {
				  		   this.close();
				      	}, 1000);
					}
					else if(message != false){
    					temp = message;

    					for (let x = 0; x < temp.length; ++x) {
							let index = this.current.indexOf(temp[x]);
	    					this.current.splice(index, 1);
    					}

	    				this.employee_value = this.current;

    					if(this.current.length == 0){
	    					this.error_title = '';
							this.error_message = '';
							setTimeout(() => {
					  		   this.close();
					      	}, 1000);
    					}
					}
				});
	      	}, 1000);

		}
		else{
			// socket.io emit
			// this.socket.emit('notify', this.poststore);
			this.validate = false;
			this.success_title = "Success";
			this.success_message = "Successfully";
			setTimeout(() => {
	  		   this.close();
	      	}, 2000);
		}
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}
	//time picker
	public toggleMode():void {
	this.ismeridian = !this.ismeridian;

	}

	private selectedDate(value:any, dateInput:any) {
        dateInput.start = value.start;

       	this.date = moment(dateInput.start).format("YYYY-MM-DD");

       	this.duplicateEntryValidation();
    }

    duplicateEntryValidation(){

    	this.OvertimeForm.value.employee_id = this.current;
		this.OvertimeForm.value.created_by = this.user_id;
		this.OvertimeForm.value.supervisor_id = this.supervisor_id;
		this.OvertimeForm.value.start_time = moment(this.start_time).format("HH:mm");
		this.OvertimeForm.value.end_time = moment(this.end_time).format("HH:mm");
		this.OvertimeForm.value.date = this.date;

		if (this.date == null) {
			this.OvertimeForm.value.date = moment(this.mainInput.start).format("YYYY-MM-DD");
		}

		let hours = moment
        .duration(moment(this.end_time, 'HH:mm')
        .diff(moment(this.start_time, 'HH:mm'))
        ).asHours();

        this.OvertimeForm.value.total_hours = hours;

		if(this.user == true  && this.create == true || this.user == true && this.edit == true){
			let v = [];
			v.push(this.employee_id);
			this.OvertimeForm.value.employee_id = v;
		}

		if(this.edit == true){
			this.OvertimeForm.value.edit = this.edit;
			this.OvertimeForm.value.id= this.ot_id;
		}
		else{
			this.OvertimeForm.value.edit = false;
		}
		let ot = this.OvertimeForm.value;

		if(this.OvertimeForm.value.employee_id != 0 && this.OvertimeForm.value.date != null || this.edit == true){
			this._OTservice
				.duplicateEntryValidation(ot)
				.subscribe(
					data => {
						this.poststore = data;
						if(this.poststore.message == "duplicate"){
							this.dupVal = false;
							this.errorCatch();
						}
						else{
							this.dupVal = true;
							this.error_title = '';
							this.error_message = '';
						}
						this.validation();
					},
					err => this.catchError(err)
				);
		}
    }

}