var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { AuthUserService } from '../../services/auth-user.service';
import { AttendanceService } from '../../services/attendance.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import * as moment from 'moment';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
var DownloadFileModalComponent = /** @class */ (function (_super) {
    __extends(DownloadFileModalComponent, _super);
    function DownloadFileModalComponent(dialogService, _fb, _ar, _common_service, _attendservice, _auth_service, daterangepickerOptions) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._common_service = _common_service;
        _this._attendservice = _attendservice;
        _this._auth_service = _auth_service;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this.value = 0;
        _this.tableValidate = false;
        _this.checkAllValidation = false;
        _this.employeesList = [];
        _this.validate = false;
        _this.mainInput = {
            start: moment(),
            end: moment().add(1, 'days')
        };
        _this.date = [];
        _this.companystats = false;
        _this.deptstats = false;
        _this.loader = false;
        _this.loader2 = true;
        _this.button = false;
        _this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
        _this.searchForm = _fb.group({
            'department': [null],
            'company': [null],
            'supervisor_id': [null],
            'role_id': [null],
            'branch_id': [null],
            'position_id': [null]
        });
        return _this;
    }
    DownloadFileModalComponent.prototype.ngOnInit = function () {
        this.getCompany();
        this.getDepartment();
        this.getBranch();
        this.getPosition();
    };
    DownloadFileModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    DownloadFileModalComponent.prototype.getEmployee = function () {
        var _this = this;
        this.loader = true;
        this.tableValidate = false;
        this.searchForm.value.department = this.department_current;
        this.searchForm.value.company = this.company_current;
        this.searchForm.value.supervisor_id = this.user_id;
        this.searchForm.value.role_id = this.role_id;
        this.searchForm.value.branch_id = this.branch_current;
        this.searchForm.value.position_id = this.position_current;
        var model = this.searchForm.value;
        this._attendservice.getEmployeeData(model).
            subscribe(function (data) {
            _this.employee = (data);
            if (_this.employee.length != 0) {
                _this.tableValidate = true;
            }
            else {
                _this.tableValidate = false;
            }
            if (_this.employeesList != []) {
                _this.employeesList = [];
            }
            var val = false;
            var show = true;
            var len = _this.employee.length;
            for (var i = 0; i < len; ++i) {
                var name_1 = _this.employee[i].name;
                var id = _this.employee[i].id;
                _this.employeesList.push({ id: id, name: name_1, val: val, show: show });
            }
            _this.loader = false;
        }, function (err) { return console.error(err); });
    };
    DownloadFileModalComponent.prototype.cancel = function () {
        this.close();
    };
    DownloadFileModalComponent.prototype.checkAll = function () {
        if (this.checkAllValidation == false) {
            var leng = this.employeesList.length;
            for (var i = 0; i < leng; i++) {
                this.employeesList[i].val = true;
            }
            this.validate = true;
            this.checkAllValidation = true;
        }
        else {
            var len = this.employeesList.length;
            for (var i = 0; i < len; i++) {
                this.employeesList[i].val = false;
            }
            this.validate = false;
            this.checkAllValidation = false;
        }
    };
    DownloadFileModalComponent.prototype.EmployeeCheckbox = function (index) {
        if (this.employeesList[index].val == false) {
            this.employeesList[index].val = true;
        }
        else {
            this.employeesList[index].val = false;
            this.checkAllValidation = false;
        }
        var len = this.employeesList.length;
        var i = 0;
        for (var x = 0; x < len; ++x) {
            if (this.employeesList[x].val == true) {
                i++;
            }
        }
        if (i > 0) {
            this.validate = true;
        }
        else {
            this.validate = false;
        }
    };
    DownloadFileModalComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_date = moment(dateInput.start).format("YYYY-MM-DD");
        this.end_date = moment(dateInput.end).format("YYYY-MM-DD");
    };
    DownloadFileModalComponent.prototype.computeDays = function () {
        //if user didn't click the choice any date
        if (this.start_date == null && this.end_date == null) {
            this.start_date = this.mainInput.start;
            this.end_date = this.mainInput.end;
        }
        var start = this.start_date;
        var end = this.end_date;
        var from = moment(this.start_date, 'YYYY-MM-DD');
        var to = moment(this.end_date, 'YYYY-MM-DD');
        /* using diff */
        this.duration = to.diff(from, 'days');
        var currDate = from.clone().startOf('day');
        var lastDate = to.clone().startOf('day');
        this.date.push(currDate.toDate());
        while (currDate.add(1, 'days').diff(lastDate) < 0) {
            this.date.push(currDate.clone().toDate());
        }
        if (start != end) {
            this.date.push(lastDate.toDate());
        }
    };
    DownloadFileModalComponent.prototype.confirm = function () {
        var _this = this;
        this.computeDays();
        var date = [];
        var n = {};
        for (var i = 0; i < this.date.length; i++) {
            if (!n[this.date[i]]) {
                n[this.date[i]] = true;
                date.push(moment(this.date[i]).format("YYYY-MM-DD"));
            }
        }
        var id = [];
        var len = this.employeesList.length;
        for (var x = 0; x < len; ++x) {
            if (this.employeesList[x].val == true) {
                id.push(this.employeesList[x].id);
            }
        }
        var model = [];
        model.push({ id: id, date: date });
        this._attendservice.getSchedule(model).
            subscribe(function (data) {
            _this.sched = data;
            if (_this.sched != null || _this.sched != []) {
                _this.arrangeData();
            }
        }, function (err) { return console.error(err); });
    };
    DownloadFileModalComponent.prototype.arrangeData = function () {
        var _this = this;
        var data = [
            {
                employee_id: "Employee ID",
                name: "Employee Name",
                time_in: "time_in (YYYY-MM-DD HH:mm:ss)",
                time_out: "time_out (YYYY-MM-DD HH:mm:ss)"
            }
        ];
        var lent = this.employeesList.length;
        var leng = this.sched.length;
        var lengt = this.date.length;
        for (var x = 0; x < lent; ++x) {
            for (var i = 0; i < leng; ++i) {
                for (var y = 0; y < lengt; ++y) {
                    var date = moment(this.date[y]).format("YYYY-MM-DD");
                    var sched = moment(this.sched[i].date).format("YYYY-MM-DD");
                    if (this.employeesList[x].id == this.sched[i].id && date == sched && this.employeesList[x].val == true) {
                        var employee_id = this.employeesList[x].id;
                        var name_2 = this.employeesList[x].name;
                        var time_in = date + ' ' + this.sched[i].start_time;
                        var time_out = date + ' ' + this.sched[i].end_time;
                        data.push({ employee_id: employee_id, name: name_2, time_in: time_in, time_out: time_out });
                    }
                }
            }
        }
        var start = moment(this.start_date).format("YYYY-MM-DD");
        var end = moment(this.end_date).format("YYYY-MM-DD");
        var name = 'Attendance Form of ' + start + ' to ' + end;
        var department = this.searchForm.value.department;
        var company = this.searchForm.value.company;
        var branch = this.searchForm.value.branch_id;
        var position = this.searchForm.value.position_id;
        new Angular2Csv(data, name);
        this.success_title = "Success!";
        this.success_message = "Successfully Downloaded";
        setTimeout(function () {
            _this.close();
        }, 2000);
    };
    DownloadFileModalComponent.prototype.getBranch = function () {
        var _this = this;
        this._common_service.getBranch()
            .subscribe(function (data) {
            _this.branch = Array.from(data);
            _this.branch_value = [];
            _this.branch_current = 0;
            var id = 0;
            var text = 'Select Branch';
            _this.branch.unshift({ id: id, text: text });
            _this.branch_value = _this.value;
        }, function (err) { return _this.catchError(err); });
    };
    DownloadFileModalComponent.prototype.changedBranch = function (data) {
        this.branch_current = data.value;
        this.button = true;
    };
    DownloadFileModalComponent.prototype.getCompany = function () {
        var _this = this;
        this._common_service.getCompany()
            .subscribe(function (data) {
            _this.company = Array.from(data);
            _this.company_value = [];
            _this.company_current = 0;
            var id = 0;
            var text = 'Select Company';
            _this.company.unshift({ id: id, text: text });
            _this.company_value = _this.value;
        }, function (err) { return _this.catchError(err); });
    };
    DownloadFileModalComponent.prototype.changedCompany = function (data) {
        this.company_current = data.value;
        this.button = true;
    };
    DownloadFileModalComponent.prototype.getDepartment = function () {
        var _this = this;
        this._common_service.getDepartment()
            .subscribe(function (data) {
            _this.department = Array.from(data);
            _this.department_value = [];
            _this.department_current = 0;
            var id = 0;
            var text = 'Select Department';
            _this.department.unshift({ id: id, text: text });
            _this.department_value = _this.value;
        }, function (err) { return _this.catchError(err); });
    };
    DownloadFileModalComponent.prototype.changedDepartment = function (data) {
        this.department_current = data.value;
        this.button = true;
    };
    DownloadFileModalComponent.prototype.getPosition = function () {
        var _this = this;
        this._common_service.getPosition()
            .subscribe(function (data) {
            _this.position = data;
            _this.position_value = [];
            _this.position_current = 0;
            var id = 0;
            var text = 'Select Position';
            _this.position.unshift({ id: id, text: text });
            _this.position_value = _this.value;
            _this.loader2 = false;
        }, function (err) { return console.error(err); });
    };
    DownloadFileModalComponent.prototype.changedPosition = function (data) {
        this.position_current = data.value;
        this.button = true;
    };
    DownloadFileModalComponent.prototype.filterEmpInCheckBox = function (name) {
        var len = this.employeesList.length;
        for (var i = 0; i < len; ++i) {
            if (this.employeesList[i].name.toLowerCase().match(name.toLowerCase())) {
                this.employeesList[i].show = true;
            }
            else {
                this.employeesList[i].show = false;
            }
        }
    };
    DownloadFileModalComponent = __decorate([
        Component({
            selector: 'app-download-file-modal',
            templateUrl: './download-file-modal.component.html',
            styleUrls: ['./download-file-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            CommonService,
            AttendanceService,
            AuthUserService,
            DaterangepickerConfig])
    ], DownloadFileModalComponent);
    return DownloadFileModalComponent;
}(DialogComponent));
export { DownloadFileModalComponent };
//# sourceMappingURL=download-file-modal.component.js.map