import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { AuthUserService } from '../../services/auth-user.service';
import { AttendanceService } from '../../services/attendance.service';
import { Select2OptionData } from 'ng2-select2';
import { Angular2Csv } from 'angular2-csv/Angular2-csv'
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { DaterangepickerConfig } from 'ng2-daterangepicker';

@Component({
  selector: 'app-download-file-modal',
  templateUrl: './download-file-modal.component.html',
  styleUrls: ['./download-file-modal.component.css']
})
export class DownloadFileModalComponent extends DialogComponent<null, boolean> {
  
	title: string;
	public id: any;
	public status: any;
	public name: string;
	public success_title;
	public success_message;
	public error_title;
	public error_message;
	public poststore: any;
	public options: Select2Options;

	public company: any;
	public company_current : any;
	company_value: any;
	value=0;
	
	employee:any;
	tableValidate=false;
	checkAllValidation=false;
	employeesList =[];
	validate = false;
	public mainInput = {
        start: moment(),
        end: moment().add(1,'days')
    }
    public start_date:any;
	public end_date:any;
	duration : number;
	date = [];
	sched:any;

	public branch: any;
	public branch_current : any;
	branch_value: any;

	public department_current : any;
	department_value: any;
	public department: any;

	companystats=false;
	deptstats=false;
	role_id:any;
	user_id:any;

	public position: any;
	public position_current : any;
	position_value: any;

	loader = false;
	loader2 = true;
	searchForm:any;

	button = false;

 constructor( 
   dialogService: DialogService, 
   private _fb: FormBuilder, 
   private _ar: ActivatedRoute,
   private _common_service: CommonService,
   private _attendservice: AttendanceService ,
   private _auth_service: AuthUserService,
   private daterangepickerOptions: DaterangepickerConfig
   ){
    super(dialogService);

    this.daterangepickerOptions.settings = {
        singleDatePicker: false
	}; 

	this.searchForm = _fb.group({
    	'department': 	[null],
		'company': 		[null],
		'supervisor_id':[null],
		'role_id': 		[null],
		'branch_id': 	[null],
		'position_id': 	[null]
   	});
  
  }
	ngOnInit() {
		this.getCompany();
		this.getDepartment();
		this.getBranch();
		this.getPosition();
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	getEmployee(){

		this.loader = true;
		this.tableValidate = false;

		this.searchForm.value.department = this.department_current;
		this.searchForm.value.company = this.company_current;
		this.searchForm.value.supervisor_id = this.user_id;
		this.searchForm.value.role_id = this.role_id;
		this.searchForm.value.branch_id = this.branch_current;
		this.searchForm.value.position_id = this.position_current;

		let model = this.searchForm.value;
		
		this._attendservice.getEmployeeData(model).
	      	subscribe(
	        data => {
	        	this.employee = (data);

	          	if(this.employee.length != 0){
			  		this.tableValidate=true;
				}
				else{
					this.tableValidate=false;
				}
				if(this.employeesList != []){
					this.employeesList = [];
				}
	          	let val = false;
	          	let show = true;
				let len = this.employee.length;
				for (let i = 0; i < len; ++i) {
					let name = this.employee[i].name;
					let id =  this.employee[i].id;
					this.employeesList.push({id,name,val,show});
				}

				this.loader = false;
	        },
	        err => console.error(err)
	     );

	}

	cancel(){
		this.close();
	}

	checkAll(){

		if(this.checkAllValidation == false){
			let leng = this.employeesList.length;
			for (let i = 0; i < leng; i++) {
				this.employeesList[i].val = true;
			}
			this.validate = true;
			this.checkAllValidation = true;
		}
		else{
			let len = this.employeesList.length;
			for (let i = 0; i < len; i++) {
				this.employeesList[i].val = false;
			}
			this.validate = false;
			this.checkAllValidation = false;
		}

	}

	EmployeeCheckbox(index:any){

		if(this.employeesList[index].val == false){
			this.employeesList[index].val = true;
			
		}
		else{
			this.employeesList[index].val = false;
			this.checkAllValidation = false;
		}

		let len = this.employeesList.length;
		let i = 0;
		for (let x = 0; x < len; ++x) {
			
			if(this.employeesList[x].val == true){
				i++;
			}
		}

		if(i > 0){
			this.validate = true;
		}
		else{
			this.validate = false;
		}

		
	}

	private selectedDate(value: any, dateInput: any) {
       	
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_date = moment(dateInput.start).format("YYYY-MM-DD");
       	this.end_date = moment(dateInput.end).format("YYYY-MM-DD");
    }

	computeDays(){

		//if user didn't click the choice any date
		if(this.start_date == null && this.end_date == null){
			
			this.start_date = this.mainInput.start;
			this.end_date = this.mainInput.end;
		}

		let start = this.start_date;
		let end = this.end_date;
		let from = moment(this.start_date, 'YYYY-MM-DD');
		let to = moment(this.end_date, 'YYYY-MM-DD');
	  
		/* using diff */
		this.duration = to.diff(from, 'days');

	    let currDate = from.clone().startOf('day');
	    let lastDate = to.clone().startOf('day');

	   	this.date.push(currDate.toDate());

	    while(currDate.add(1 ,'days').diff(lastDate) < 0) {
	        this.date.push(currDate.clone().toDate());
	    }

	    if(start != end)  {
	    	this.date.push(lastDate.toDate());
	    }
	}

	confirm(){

		this.computeDays();
		let date = [];
		let n = {}
		for(let i = 0; i < this.date.length; i++) 
		{
			if (!n[this.date[i]]) 
			{
				n[this.date[i]] = true; 
				date.push(moment(this.date[i]).format("YYYY-MM-DD")); 
			}
		} 
		let id = [];

		let len = this.employeesList.length;
		for (let x = 0; x < len; ++x) {
			if(this.employeesList[x].val == true){
				id.push(this.employeesList[x].id); 
			}
		}

		let model = [];
		model.push({id,date});

		this._attendservice.getSchedule(model).
		subscribe(
			data => {
				this.sched = data;
				if(this.sched != null || this.sched != []){
					this.arrangeData();
				}
			},
			err => console.error(err)
		);
		 
	}

	arrangeData(){
		let data = [
		  {
		    employee_id: "Employee ID",
		    name: "Employee Name",
		    time_in: "time_in (YYYY-MM-DD HH:mm:ss)",
		    time_out: "time_out (YYYY-MM-DD HH:mm:ss)"
		  }
		];


		let lent = this.employeesList.length;
		let leng = this.sched.length;
		let lengt = this.date.length;
		for (let x = 0; x < lent; ++x) {
			for (let i = 0; i < leng; ++i) {
				for (let y = 0; y < lengt; ++y) {
					let date= moment(this.date[y]).format("YYYY-MM-DD");
					let sched= moment(this.sched[i].date).format("YYYY-MM-DD");
					if(this.employeesList[x].id == this.sched[i].id && date == sched && this.employeesList[x].val == true){
						let employee_id = this.employeesList[x].id;
						let name = this.employeesList[x].name;
						let time_in = date + ' ' + this.sched[i].start_time;
						let time_out = date + ' ' + this.sched[i].end_time;
						data.push({employee_id,name,time_in,time_out});
					}
				}
			}
		}

		let start = moment(this.start_date).format("YYYY-MM-DD");
		let end = moment(this.end_date).format("YYYY-MM-DD");

		let name = 'Attendance Form of ' + start + ' to ' + end;

		let department = this.searchForm.value.department;
		let company = this.searchForm.value.company;
		let branch = this.searchForm.value.branch_id;
		let position = this.searchForm.value.position_id;

		new Angular2Csv(data, name);

		this.success_title = "Success!";
        this.success_message = "Successfully Downloaded";
		    setTimeout(() => {
  		   this.close();
      	}, 2000);
	}

	getBranch(){
	      this._common_service.getBranch()
	      .subscribe(
	        data => {
	        this.branch = Array.from(data);
	        this.branch_value = [];
	        this.branch_current = 0;

	        let id = 0;
	        let text = 'Select Branch';

	        this.branch.unshift({id,text});

	        this.branch_value = this.value;
	        
	        },
	        err => this.catchError(err)
	    );
	}
  	changedBranch(data: any) {

      this.branch_current = data.value;

      this.button = true;

    } 

    getCompany(){
	      this._common_service.getCompany()
	      .subscribe(
	        data => {
	        this.company = Array.from(data);
	        this.company_value = [];
	        this.company_current = 0;

	        let id = 0;
	        let text = 'Select Company';

	        this.company.unshift({id,text});

	        this.company_value = this.value;
	        
	        },
	        err => this.catchError(err)
	    );
	}
  	changedCompany(data: any) {

      this.company_current = data.value;

      this.button = true;

    } 

	getDepartment(){
	      this._common_service.getDepartment()
	      .subscribe(
	        data => {
	        this.department = Array.from(data);
	        this.department_value = [];
	        this.department_current = 0;

	        let id = 0;
	        let text = 'Select Department';

	        this.department.unshift({id,text});

	        this.department_value = this.value;
	        
	        },
	        err => this.catchError(err)

	    );
	}
	changedDepartment(data: any) {
		this.department_current = data.value;

		this.button = true;

	} 

    getPosition(){
		this._common_service.getPosition()
			.subscribe(
				data => {
				this.position = data;
				this.position_value = [];
				this.position_current = 0;

				let id = 0;
				let text = 'Select Position';
				this.position.unshift({id,text});

				this.position_value = this.value;

				this.loader2 = false;

			},
			err => console.error(err)
		);
	}
	changedPosition(data) {

		this.position_current = data.value;

		this.button = true;
	}

	filterEmpInCheckBox(name){
		
		let len = this.employeesList.length;

		for (let i = 0; i < len; ++i) {
			if (this.employeesList[i].name.toLowerCase().match(name.toLowerCase())) {
				this.employeesList[i].show = true;
			}else{
				this.employeesList[i].show = false;
			}
		}
	}


}
