import { Component } from '@angular/core';
import { Configuration } from '../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { AttendanceService } from '../../services/attendance.service';

@Component({
  selector: 'app-remarks-modal',
  templateUrl: './remarks-modal.component.html',
  styleUrls: ['./remarks-modal.component.css']
})
export class RemarksModalComponent extends DialogComponent<null, boolean> {
  
	title: string;
	public id: any;
	public status: any;
	public name: string;
	public success_title;
	public success_message;
	public error_title;
	public error_message;
	public poststore: any;
	public statuses: any;
	status_id:any;
	url:any;
	validate=true;
	remarksForm : FormGroup;
	logsRemarksForm : FormGroup;
	button:any;
	messenger_id:any;
	created_by:any;

	socket: SocketIOClient.Socket;
    host:any;

    logs = false;
    data:any;
    message:any;

	constructor( 
		dialogService: DialogService, 
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute,
		private _common_service: CommonService,
		private _conf: Configuration,
		private _attendservice: AttendanceService
	){
    super(dialogService);

    this.host = this._conf.socketUrl;

    this.remarksForm = _fb.group({
		'remarks':			[null, [Validators.required]],
		'status_id':		[null]
      
    }); 

    this.logsRemarksForm = _fb.group({
		'remarks':			[null, [Validators.required]]
      
    }); 
  
  }
	ngOnInit() {


		if (this.logs == false) {
			if(this.button == 'Reject'){
				this.status_id = 2;
			}
			else{
				this.status_id = 3;
			}
			// for socket.io
			// this.connectToServer();
		}

	}	
 
	onSubmit() {
		
	    this.remarksForm.value.messenger_id = this.messenger_id;
	    this.remarksForm.value.status_id = this.status_id;
	    this.remarksForm.value.created_by = this.created_by;
	    this.remarksForm.value.url = this.url;
	    this.remarksForm.value.id = this.id;

	    let model = this.remarksForm.value;

	    this._common_service.updateRequest(model)
	    .subscribe(
	      data => {
	        this.poststore = Array.from(data);
	        // socket.io emit
	        // this.socket.emit('notify', this.poststore);
	        this.validate=false;
	        this.result = true;
	        this.success_title = "Success!";
	        this.success_message = "Successfully Updated";
			    setTimeout(() => {
	  		   this.close();
	      	}, 2000);
			
	      },
	      err => this.catchError(err)
	    );
	}

	 private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	cancel(){
		this.close();
	}

	connectToServer(){
	    let socketUrl = this.host;
	    this.socket = io.connect(socketUrl);
	    this.socket.on("connect", () => this.connect());
	    this.socket.on("disconnect", () => this.disconnect());
	    
	    this.socket.on("error", (error: string) => {
	      console.log(`ERROR: "${error}" (${socketUrl})`);
	    });

	   
	  }

	// Handle connection opening
	private connect() {
	    // Request initial list when connected
	    this.socket.emit("join", 'Client User -> ');
	}
	// Handle connection closing
	private disconnect() {
	    console.log(`Disconnected from "${this.host}"`);
	}

	onSubmitLogs(){
		this.logsRemarksForm.value.branch_id = this.data.branch_id;
		this.logsRemarksForm.value.created_by = this.data.created_by;
		this.logsRemarksForm.value.date = this.data.date;
		this.logsRemarksForm.value.datetime = this.data.datetime;
		this.logsRemarksForm.value.employee_id = this.data.employee_id;
		this.logsRemarksForm.value.id = this.data.id;
		this.logsRemarksForm.value.type = this.data.type;

		let model = this.logsRemarksForm.value;

		this._attendservice.createAttendanceFromWeb(model).
	      	subscribe(
	        data => {
	          let dat = data;
	          	this.validate=false;
	        	this.success_title = "Success!";
	        	this.success_message = "Successfully Save";
				setTimeout(() => {
		  		   this.close();
		      	}, 2000);
	        },
	        err => console.error(err)
	    );


	}


}
