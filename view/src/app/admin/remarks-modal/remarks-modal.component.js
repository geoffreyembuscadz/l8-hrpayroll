var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Configuration } from '../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { AttendanceService } from '../../services/attendance.service';
var RemarksModalComponent = /** @class */ (function (_super) {
    __extends(RemarksModalComponent, _super);
    function RemarksModalComponent(dialogService, _fb, _ar, _common_service, _conf, _attendservice) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._common_service = _common_service;
        _this._conf = _conf;
        _this._attendservice = _attendservice;
        _this.validate = true;
        _this.logs = false;
        _this.host = _this._conf.socketUrl;
        _this.remarksForm = _fb.group({
            'remarks': [null, [Validators.required]],
            'status_id': [null]
        });
        _this.logsRemarksForm = _fb.group({
            'remarks': [null, [Validators.required]]
        });
        return _this;
    }
    RemarksModalComponent.prototype.ngOnInit = function () {
        if (this.logs == false) {
            if (this.button == 'Reject') {
                this.status_id = 2;
            }
            else {
                this.status_id = 3;
            }
            // for socket.io
            // this.connectToServer();
        }
    };
    RemarksModalComponent.prototype.onSubmit = function () {
        var _this = this;
        this.remarksForm.value.messenger_id = this.messenger_id;
        this.remarksForm.value.status_id = this.status_id;
        this.remarksForm.value.created_by = this.created_by;
        this.remarksForm.value.url = this.url;
        this.remarksForm.value.id = this.id;
        var model = this.remarksForm.value;
        this._common_service.updateRequest(model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            // socket.io emit
            // this.socket.emit('notify', this.poststore);
            _this.validate = false;
            _this.result = true;
            _this.success_title = "Success!";
            _this.success_message = "Successfully Updated";
            setTimeout(function () {
                _this.close();
            }, 2000);
        }, function (err) { return _this.catchError(err); });
    };
    RemarksModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    RemarksModalComponent.prototype.cancel = function () {
        this.close();
    };
    RemarksModalComponent.prototype.connectToServer = function () {
        var _this = this;
        var socketUrl = this.host;
        this.socket = io.connect(socketUrl);
        this.socket.on("connect", function () { return _this.connect(); });
        this.socket.on("disconnect", function () { return _this.disconnect(); });
        this.socket.on("error", function (error) {
            console.log("ERROR: \"" + error + "\" (" + socketUrl + ")");
        });
    };
    // Handle connection opening
    RemarksModalComponent.prototype.connect = function () {
        // Request initial list when connected
        this.socket.emit("join", 'Client User -> ');
    };
    // Handle connection closing
    RemarksModalComponent.prototype.disconnect = function () {
        console.log("Disconnected from \"" + this.host + "\"");
    };
    RemarksModalComponent.prototype.onSubmitLogs = function () {
        var _this = this;
        this.logsRemarksForm.value.branch_id = this.data.branch_id;
        this.logsRemarksForm.value.created_by = this.data.created_by;
        this.logsRemarksForm.value.date = this.data.date;
        this.logsRemarksForm.value.datetime = this.data.datetime;
        this.logsRemarksForm.value.employee_id = this.data.employee_id;
        this.logsRemarksForm.value.id = this.data.id;
        this.logsRemarksForm.value.type = this.data.type;
        var model = this.logsRemarksForm.value;
        this._attendservice.createAttendanceFromWeb(model).
            subscribe(function (data) {
            var dat = data;
            _this.validate = false;
            _this.success_title = "Success!";
            _this.success_message = "Successfully Save";
            setTimeout(function () {
                _this.close();
            }, 2000);
        }, function (err) { return console.error(err); });
    };
    RemarksModalComponent = __decorate([
        Component({
            selector: 'app-remarks-modal',
            templateUrl: './remarks-modal.component.html',
            styleUrls: ['./remarks-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            CommonService,
            Configuration,
            AttendanceService])
    ], RemarksModalComponent);
    return RemarksModalComponent;
}(DialogComponent));
export { RemarksModalComponent };
//# sourceMappingURL=remarks-modal.component.js.map