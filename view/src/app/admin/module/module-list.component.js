var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';
import { Module } from '../../model/module';
import { ModuleCreateComponent } from './module-create.component';
import { ModuleEditComponent } from './module-edit.component';
import { ModuleService } from '../../services/module.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
var ModuleListComponent = /** @class */ (function () {
    function ModuleListComponent(_conf, modalService, _module_service) {
        this._conf = _conf;
        this.modalService = modalService;
        this._module_service = _module_service;
        this.dtTrigger = new Subject();
        this.module = new Module();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
    }
    ModuleListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var authToken = localStorage.getItem('id_token');
        // DataTable Configuration
        this.dtOptions = {
            ajax: {
                url: this._conf.ServerWithApiUrl + 'module',
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + authToken);
                },
            },
            columns: [{
                    title: 'ID',
                    data: 'id'
                }, {
                    title: 'Name',
                    data: 'name'
                }, {
                    title: 'Display Name',
                    data: 'display_name'
                }, {
                    title: 'Description',
                    data: 'description'
                }],
            rowCallback: function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
                var self = _this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', function () {
                    _this.module_id = data.id;
                    _this._module_service.setId(_this.module_id);
                    _this.openEditModal();
                });
                return nRow;
            }
        };
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    };
    ModuleListComponent.prototype.openEditModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ModuleEditComponent, {
            title: 'Update Module'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    ModuleListComponent.prototype.openAddModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ModuleCreateComponent, {
            title: 'Add Module'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    ModuleListComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    ModuleListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    ModuleListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], ModuleListComponent.prototype, "dtElement", void 0);
    ModuleListComponent = __decorate([
        Component({
            selector: 'admin-module-list',
            templateUrl: './module-list.component.html',
            styleUrls: ['./module-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration, DialogService, ModuleService])
    ], ModuleListComponent);
    return ModuleListComponent;
}());
export { ModuleListComponent };
//# sourceMappingURL=module-list.component.js.map