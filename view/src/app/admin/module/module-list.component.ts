import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';

import { Module } from '../../model/module';
import { ModuleCreateComponent } from './module-create.component';
import { ModuleEditComponent } from './module-edit.component';
import { ModuleService } from '../../services/module.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'admin-module-list',
  templateUrl: './module-list.component.html',
  styleUrls: ['./module-list.component.css']
})

@Injectable()
export class ModuleListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	public id: string;
	public module_id: any;
	public module = new Module();

	dtOptions: any = {};
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

	constructor(private _conf: Configuration, private modalService: DialogService, private _module_service: ModuleService){

	}

	ngOnInit() {
		let authToken = localStorage.getItem('id_token');

		// DataTable Configuration
		this.dtOptions = {
			ajax: {
				url: this._conf.ServerWithApiUrl + 'module',
				type: "GET",
				beforeSend: function(xhr){
                            xhr.setRequestHeader("Authorization",
                            "Bearer " + authToken);
                },
			},
			columns: [{
				title: 'ID',
				data: 'id'
			}, {
				title: 'Name',
				data: 'name'
			}, {
				title: 'Display Name',
				data: 'display_name'
			}, {
				title: 'Description',
				data: 'description'
			}],
			rowCallback: (nRow: number, data: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
				let self = this;
				// Unbind first in order to avoid any duplicate handler
				// (see https://github.com/l-lin/angular-datatables/issues/87)
				$('td', nRow).unbind('click');
				$('td', nRow).bind('click', () => {
					this.module_id = data.id;
				    this._module_service.setId(this.module_id);
					this.openEditModal();
				});
				return nRow;
			}
		};

		//add the the body classes
	    this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

		
	}

	openEditModal() {
		let disposable = this.modalService.addDialog(ModuleEditComponent, {
            title:'Update Module'
        	}).subscribe((isConfirmed)=>{
        		this.rerender();
                
        });
	}

	openAddModal() {
		let disposable = this.modalService.addDialog(ModuleCreateComponent, {
            title:'Add Module'
        	}).subscribe((isConfirmed)=>{
        		this.rerender();
                
        });
    }

	ngOnDestroy() {
	    //remove the the body classes
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	 }


	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	}

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      // Destroy the table first
	      dtInstance.destroy();
	      // Call the dtTrigger to rerender again
	      this.dtTrigger.next();
	    });
    }
}
