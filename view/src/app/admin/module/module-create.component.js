var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Module } from '../../model/module';
import { ModuleService } from '../../services/module.service';
var ModuleCreateComponent = /** @class */ (function (_super) {
    __extends(ModuleCreateComponent, _super);
    function ModuleCreateComponent(_rt, dialogService, _fb, _module_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._fb = _fb;
        _this._module_service = _module_service;
        _this.module = new Module();
        _this.error = false;
        _this.errorMessage = '';
        _this.createModuleForm = _fb.group({
            'name': ['', [Validators.required]],
            'display_name': ['', [Validators.required]],
            'description': ['', [Validators.required]],
            'icons': ['', [Validators.required]]
        });
        return _this;
    }
    ModuleCreateComponent.prototype.onSubmit = function () {
        var _this = this;
        var module_model = this.createModuleForm.value;
        console.log(module_model);
        this._module_service
            .storeModule(module_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            _this.success_title = "Success";
            _this.success_message = "A new module record was successfully added.";
            alert('A new module record was successfully added.');
            _this.close();
            _this._rt.navigate(['/admin/module-list']);
        }, function (err) { return _this.catchError(err); });
    };
    ModuleCreateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Module name already exist.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    ModuleCreateComponent.prototype.ngOnInit = function () {
    };
    ModuleCreateComponent = __decorate([
        Component({
            selector: 'admin-module-create',
            templateUrl: './module-create.component.html'
        }),
        __metadata("design:paramtypes", [Router, DialogService, FormBuilder, ModuleService])
    ], ModuleCreateComponent);
    return ModuleCreateComponent;
}(DialogComponent));
export { ModuleCreateComponent };
//# sourceMappingURL=module-create.component.js.map