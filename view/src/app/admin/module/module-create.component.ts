import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { Observable } from "rxjs/Rx";

import { Module } from '../../model/module';
import { ModuleService } from '../../services/module.service';

@Component({
  selector: 'admin-module-create',
  templateUrl: './module-create.component.html'
})
export class ModuleCreateComponent extends DialogComponent<null, boolean> {

  public error_title: string;
  public error_message: string;
  private success_title: string;
  private success_message: string;
  public poststore: any;
  public module = new Module();

  error = false;
  elem: any;
  errorMessage = '';
  createModuleForm : FormGroup;

  constructor(private _rt: Router,dialogService: DialogService, private _fb: FormBuilder, private _module_service: ModuleService) { 
    super(dialogService);

    this.createModuleForm = _fb.group({
      'name': ['', [Validators.required]],
      'display_name': ['', [Validators.required]],
      'description': ['', [Validators.required]],
      'icons': ['', [Validators.required]]
    });
  }

  onSubmit() {

  let  module_model = this.createModuleForm.value;
  console.log(module_model);
  this._module_service
  .storeModule(module_model)
  .subscribe(
    data => {
      this.poststore = Array.from(data);
      this.success_title = "Success";
          this.success_message = "A new module record was successfully added.";
      alert('A new module record was successfully added.');
      this.close();
      this._rt.navigate(['/admin/module-list']);
    },
    err => this.catchError(err)
  );
  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'Module name already exist.';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }
  }

  ngOnInit() {
  }

}
