import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';

import { Module } from '../../model/module';
import { ModuleService } from '../../services/module.service';

@Component({
  selector: 'admin-module-edit',
  templateUrl: './module-edit.component.html'
})
export class ModuleEditComponent extends DialogComponent<null, boolean> {
  
  title: string;
  message: string;

  public id: any;
  public name: string;
  public display_name: string;
  public description: string;
  public parent_id: string;
  public success_title;
  public success_message;
  private headers: Headers;
  public poststore: any;
  public module = new Module();

  module_rec: any;

  updateModuleForm : FormGroup;

  constructor(private _rt: Router, private _module_service: ModuleService, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute) {
    super(dialogService);
   
    this.id = this._module_service.getId();
    this._module_service.getModule(this.id).subscribe(
      data => {
        this.setRole(data);
        console.log(data);
      },
      err => console.error(err)
    );

    this.updateModuleForm = _fb.group({
      'name': [this.module.name, [Validators.required]],
      'display_name': [this.module.display_name, [Validators.required]],
      'description': [this.module.description, [Validators.required]]
    });  
  }
 
  public validateUpdate() {
    let module_model = this.updateModuleForm.value;

    this._module_service.updateModule(this.id, module_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The module is successfully updated.";
        alert('The module is successfully updated.');
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/module-list']);
      },
      err => this.catchError(err)
    );
  }

  archiveModule(event: Event){
    event.preventDefault();
    let module_id = this.id;
    console.log('prearchive', module_id);
    this._module_service.archiveModule(module_id).subscribe(
      data => {
      
        alert('Record is successfully archived.');
        this.close();
        this._rt.navigateByUrl('admin/module-list');
        
      },
      err => console.error(err)
    );
    console.log('record archive');

  }

  public catchError(error: any) {

  }

  public setRole(module: any){
    this.id = module.id;
    this.module = module;
  }


  ngOnInit() {
  }

}