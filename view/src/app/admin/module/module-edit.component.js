var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Module } from '../../model/module';
import { ModuleService } from '../../services/module.service';
var ModuleEditComponent = /** @class */ (function (_super) {
    __extends(ModuleEditComponent, _super);
    function ModuleEditComponent(_rt, _module_service, dialogService, _fb, _ar) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._module_service = _module_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.module = new Module();
        _this.id = _this._module_service.getId();
        _this._module_service.getModule(_this.id).subscribe(function (data) {
            _this.setRole(data);
            console.log(data);
        }, function (err) { return console.error(err); });
        _this.updateModuleForm = _fb.group({
            'name': [_this.module.name, [Validators.required]],
            'display_name': [_this.module.display_name, [Validators.required]],
            'description': [_this.module.description, [Validators.required]]
        });
        return _this;
    }
    ModuleEditComponent.prototype.validateUpdate = function () {
        var _this = this;
        var module_model = this.updateModuleForm.value;
        this._module_service.updateModule(this.id, module_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The module is successfully updated.";
            alert('The module is successfully updated.');
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/module-list']);
        }, function (err) { return _this.catchError(err); });
    };
    ModuleEditComponent.prototype.archiveModule = function (event) {
        var _this = this;
        event.preventDefault();
        var module_id = this.id;
        console.log('prearchive', module_id);
        this._module_service.archiveModule(module_id).subscribe(function (data) {
            alert('Record is successfully archived.');
            _this.close();
            _this._rt.navigateByUrl('admin/module-list');
        }, function (err) { return console.error(err); });
        console.log('record archive');
    };
    ModuleEditComponent.prototype.catchError = function (error) {
    };
    ModuleEditComponent.prototype.setRole = function (module) {
        this.id = module.id;
        this.module = module;
    };
    ModuleEditComponent.prototype.ngOnInit = function () {
    };
    ModuleEditComponent = __decorate([
        Component({
            selector: 'admin-module-edit',
            templateUrl: './module-edit.component.html'
        }),
        __metadata("design:paramtypes", [Router, ModuleService, DialogService, FormBuilder, ActivatedRoute])
    ], ModuleEditComponent);
    return ModuleEditComponent;
}(DialogComponent));
export { ModuleEditComponent };
//# sourceMappingURL=module-edit.component.js.map