var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as io from "socket.io-client";
import * as moment from 'moment';
import { Observable } from "rxjs";
import { Configuration } from '../../app.config';
import { AuthUserService } from '../../services/auth-user.service';
import { DialogService } from "ng2-bootstrap-modal";
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { NotificationService } from '../../services/notification.service';
var MainHeaderComponent = /** @class */ (function () {
    function MainHeaderComponent(_auth_user_service, modalService, _rt, _notif_service, _conf) {
        this._auth_user_service = _auth_user_service;
        this.modalService = modalService;
        this._notif_service = _notif_service;
        this._conf = _conf;
        this.today = moment().format('YYYY-MM-DD');
        this.dateList = [];
        this.unreadNotifCount = 0;
        this.notifVal = 0;
        this.host = this._conf.socketUrl;
        this._rt = _rt;
    }
    MainHeaderComponent.prototype.logout = function () {
        localStorage.removeItem('id_token');
        localStorage.removeItem('role');
        this._rt.navigate(['auth']);
    };
    MainHeaderComponent.prototype.changePassword = function () {
        var disposable = this.modalService.addDialog(ChangePasswordComponent, {
            title: 'Change Password',
            user_id_emp: this.id
        }).subscribe(function (isConfirmed) {
        });
    };
    MainHeaderComponent.prototype.ngOnInit = function () {
        this.getUser();
        //uncomment this for socket.io
        // this.connectToServer();
        this.getNotification();
    };
    MainHeaderComponent.prototype.getUser = function () {
        var _this = this;
        var auth_user = this._auth_user_service.getUser()
            .subscribe(function (data) {
            _this.user_rec = data;
            _this.id = data.id;
            _this._auth_user_service.setId(_this.id);
            _this.name = _this.user_rec.name;
            _this.created_date = _this.user_rec.created_at;
        });
    };
    MainHeaderComponent.prototype.connectToServer = function () {
        var _this = this;
        var socketUrl = this.host;
        this.socket = io.connect(socketUrl);
        this.socket.on("connect", function () { return _this.connect(); });
        this.socket.on("disconnect", function () { return _this.disconnect(); });
        /**
         * [on description]
         * @param  "receiveNotif" galing sya sa server.js
         * @param this.insertInNotification(data) data array
         */
        this.socket.on("receiveNotif", function (data) { return _this.insertInNotification(data); });
        this.socket.on("error", function (error) {
            console.log("ERROR: \"" + error + "\" (" + socketUrl + ")");
        });
        // Return observable which follows "create" and "remove" signals from socket stream
        return Observable.create(function (observer) {
            _this.socket.on("create", function (item) { return observer.next({ action: "create", item: item }); });
            _this.socket.on("remove", function (item) { return observer.next({ action: "remove", item: item }); });
            return function () { return _this.socket.close(); };
        });
    };
    // Handle connection opening
    MainHeaderComponent.prototype.connect = function () {
        // Request initial list when connected
        this.socket.emit("join", 'Client User -> ');
    };
    // Handle connection closing
    MainHeaderComponent.prototype.disconnect = function () {
        console.log("Disconnected from \"" + this.host + "\"");
    };
    /**
     * pag nag update yung notificaion, ito yung sasalo para mag display ng bagong data sa notification
     * @param  data array
     */
    MainHeaderComponent.prototype.insertInNotification = function (data) {
        var _this = this;
        this._notif_service.getNotif(data, this.id)
            .subscribe(function (data) {
            var x = data;
            var array_keys = new Array();
            var len = x.length;
            var leng = _this.dateList.length;
            var avail = 0;
            for (var y = 0; y < len; ++y) {
                for (var w = 0; w < leng; ++w) {
                    if (x[y].date_create == _this.dateList[w].date) {
                        avail = 1;
                    }
                }
            }
            if (avail == 0) {
                for (var i = 0; i < x.length; i++) {
                    array_keys[x[i].date_create] = x[i].date_create;
                }
                for (var date in array_keys) {
                    var date1 = moment(date);
                    var date2 = moment(_this.today);
                    var diff = date2.diff(date1, 'days');
                    _this.dateList.unshift({ date: date, diff: diff });
                }
            }
            for (var i = 0; i < len; ++i) {
                _this.notif.unshift(x[i]);
                _this.unreadNotifCount++;
            }
            _this.notifVal = _this.notif.length;
        });
    };
    MainHeaderComponent.prototype.getNotification = function () {
        var _this = this;
        this._notif_service.getNotification()
            .subscribe(function (data) {
            _this.notif = data;
            _this.notifVal = _this.notif.length;
            var array_keys = new Array();
            _this.dateList = [];
            for (var i = 0; i < _this.notif.length; i++) {
                array_keys[_this.notif[i].date_create] = _this.notif[i].date_create;
            }
            for (var date in array_keys) {
                var date1 = moment(date);
                var date2 = moment(_this.today);
                var diff = date2.diff(date1, 'days');
                _this.dateList.push({ date: date, diff: diff });
            }
            var len = _this.notif.length;
            var x = 0;
            for (var i = 0; i < len; ++i) {
                if (_this.notif[i].seen == 0) {
                    x++;
                }
            }
            _this.unreadNotifCount = x;
        });
    };
    /**
     * pag pinindot ng user yng notification, ma seseen nya at mapupunta sya sa page na yun
     * @param details object, galing sya sa html
     */
    MainHeaderComponent.prototype.seeNotif = function (details) {
        var _this = this;
        if (this.unreadNotifCount != 0) {
            this.unreadNotifCount--;
        }
        var route = details.route;
        this._rt.navigateByUrl('admin/' + route);
        if (details.seen != 1) {
            this._notif_service.updateNotif(details)
                .subscribe(function (data) {
                var notif = data;
                _this.getNotification();
            });
        }
    };
    /**
     * pag naclick yung mark all as read
     */
    MainHeaderComponent.prototype.markAll = function () {
        var _this = this;
        this._notif_service.markAllAsRead()
            .subscribe(function (data) {
            var notif = data;
            // socket.io emit
            // this.socket.emit('markAll', notif);
            _this.getNotification();
        });
    };
    MainHeaderComponent = __decorate([
        Component({
            selector: 'admin-main-header',
            templateUrl: './main-header.component.html',
            styleUrls: ['./main-header.component.css']
        }),
        __metadata("design:paramtypes", [AuthUserService,
            DialogService,
            Router,
            NotificationService,
            Configuration])
    ], MainHeaderComponent);
    return MainHeaderComponent;
}());
export { MainHeaderComponent };
//# sourceMappingURL=main-header.component.js.map