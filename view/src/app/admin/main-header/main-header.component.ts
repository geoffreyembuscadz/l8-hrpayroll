import { Component, Optional, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import * as io from "socket.io-client";
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { Observable, Subscription } from "rxjs";
import { Configuration } from '../../app.config';
import { AuthUserService } from '../../services/auth-user.service';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { NotificationService } from '../../services/notification.service';


@Component({
  selector: 'admin-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.css']
})
export class MainHeaderComponent implements OnInit {

  user_rec: any;
  _rt: any;
  public id: string;
  public name: string;
  public created_date: string;
  connection:any;
  messages:any;
  host:any;
  notif:any;
  employee_id:any;

  today = moment().format('YYYY-MM-DD');

  dateList = [];

  unreadNotifCount=0;
  notifVal = 0;

  socket: SocketIOClient.Socket;

  constructor(
    private _auth_user_service: AuthUserService, 
    private modalService: DialogService,
    _rt: Router,
    private _notif_service:NotificationService,
    private _conf: Configuration
    ) {
    this.host = this._conf.socketUrl;
    this._rt = _rt;
  }

  logout() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('role');
    this._rt.navigate([ 'auth' ]); 
  }  


  changePassword() {
    let disposable = this.modalService.addDialog(ChangePasswordComponent, {
      title:'Change Password',
      user_id_emp: this.id
    }).subscribe((isConfirmed)=>{

    });
  }

  ngOnInit() {
    this.getUser();
    //uncomment this for socket.io
    // this.connectToServer();
    this.getNotification();
  }

  getUser(){
    
    let auth_user = this._auth_user_service.getUser()
    .subscribe(
      data => {
        this.user_rec = data;
        this.id = data.id;
        this._auth_user_service.setId(this.id);
        this.name = this.user_rec.name;
        this.created_date = this.user_rec.created_at;

      }
      );
  }

  connectToServer(){
    let socketUrl = this.host;
    this.socket = io.connect(socketUrl);
    this.socket.on("connect", () => this.connect());
    this.socket.on("disconnect", () => this.disconnect());
    /**
     * [on description]
     * @param  "receiveNotif" galing sya sa server.js
     * @param this.insertInNotification(data) data array
     */
    this.socket.on("receiveNotif", (data:any) => this.insertInNotification(data));

    this.socket.on("error", (error: string) => {
      console.log(`ERROR: "${error}" (${socketUrl})`);
    });

    // Return observable which follows "create" and "remove" signals from socket stream
    return Observable.create((observer: any) => {
      this.socket.on("create", (item: any) => observer.next({ action: "create", item: item }) );
      this.socket.on("remove", (item: any) => observer.next({ action: "remove", item: item }) );
      return () => this.socket.close();
    });
  }

  // Handle connection opening
  private connect() {
    // Request initial list when connected
    this.socket.emit("join", 'Client User -> ');
  }

  // Handle connection closing
  private disconnect() {
    console.log(`Disconnected from "${this.host}"`);
  }

  /**
   * pag nag update yung notificaion, ito yung sasalo para mag display ng bagong data sa notification
   * @param  data array
   */
  insertInNotification(data){
    this._notif_service.getNotif(data,this.id)
    .subscribe(
      data => {
        let x = data;

        let array_keys = new Array();

        let len = x.length;
        let leng = this.dateList.length;
        let avail = 0;
        
        for (let y = 0; y < len; ++y) {
          for (let w = 0; w < leng; ++w) {
            if (x[y].date_create == this.dateList[w].date) {
              avail = 1;
            }
          }
        }

        if (avail == 0) {
          for (let i = 0; i < x.length; i++) {
            array_keys[x[i].date_create] = x[i].date_create;
          }

          for (let date in array_keys) {
            let date1 = moment(date);
            let date2 = moment(this.today);
            let diff = date2.diff(date1,'days');
            this.dateList.unshift({date,diff});
          }
        }

        for (let i = 0; i < len; ++i) {
          this.notif.unshift(x[i]);
          this.unreadNotifCount++;
        }

        this.notifVal = this.notif.length;
      }
    );
  }

  getNotification(){
    this._notif_service.getNotification()
    .subscribe(
      data => {
        this.notif = data;

        this.notifVal = this.notif.length;
       
        let array_keys = new Array();
        this.dateList = [];

        for (let i = 0; i < this.notif.length; i++) {
          array_keys[this.notif[i].date_create] = this.notif[i].date_create;
        }

        for (let date in array_keys) {
          let date1 = moment(date);
          let date2 = moment(this.today);
          let diff = date2.diff(date1,'days');
          this.dateList.push({date,diff});
        }

        let len = this.notif.length;
        let x = 0;
        for (let i = 0; i < len; ++i) {
          if (this.notif[i].seen == 0) {
            x++;
          }
        }
        this.unreadNotifCount = x;
      }
    );
  }

  /**
   * pag pinindot ng user yng notification, ma seseen nya at mapupunta sya sa page na yun
   * @param details object, galing sya sa html
   */
  seeNotif(details){

    if(this.unreadNotifCount != 0){
      this.unreadNotifCount--;
    }

    let route = details.route;
    this._rt.navigateByUrl('admin/'+route);

    if(details.seen != 1){

      this._notif_service.updateNotif(details)
      .subscribe(
        data => {
          let notif = data;
          this.getNotification();
        }
      );
    }
  }

  /**
   * pag naclick yung mark all as read
   */
  markAll(){

    this._notif_service.markAllAsRead()
      .subscribe(
        data => {
          let notif = data;
          // socket.io emit
          // this.socket.emit('markAll', notif);
          this.getNotification();
        }
      );
  }

  }
