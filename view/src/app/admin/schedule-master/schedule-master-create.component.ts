import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Configuration } from './../../app.config';
import { EmployeeScheduleMaster } from '../../model/employee-schedule-master';
import { MomentModule } from 'angular2-moment/moment.module';
import { DataTableDirective } from 'angular-datatables';
import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';

import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { CommonService } from '../../services/common.service';

import { PayrollService } from '../../services/payroll.service';

@Component({
  selector: 'schedule-master-list',
  templateUrl: './schedule-master-create.component.html',
  styleUrls: ['./schedule-master-list.component.css']
})

@Injectable()
export class ScheduleMasterCreateComponent implements OnInit, AfterViewInit  {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();

  public id: string;
  public payroll_id: any;
  public company_id: any;
  public success_title: string;

  public form_companies: any;
  public form_company_employees: any;
  public fetched_company_suggested_schedules = [];
  private company_working_schedules = new EmployeeScheduleMaster();
  public form_suggested_schedules: any;
  public form_date_start: any;
  public form_date_end: any;
  public form_suggested_schedule: any;

  // Working Schedule Suggestions
  public work_sched = [];
  public value : any;
  public options: any;
  public ws_value : any;
  public ws_current : any;
  

  public start_date: any;
  public end_date: any;
  public mainInput = {
    start: moment(),
    end: moment().add(1,'days')
  };

  createMasterSchduleForm: FormGroup;

  public message_title: string;
  public message_success: string;
  public error_title: string;
  public error_message: string;
  public form_schedule_creation_forms_array: any;

  dtOptions: any = {};

  bodyClasses:string = "skin-blue sidebar-mini";
    body = document.getElementsByTagName('body')[0];

    payroll_rec: any;

  constructor(
      private _conf: Configuration, 
      private _payroll_service: PayrollService, 
      private modalService: DialogService, 
      private _rt: Router,
      private _fb: FormBuilder, 
      private _ar: ActivatedRoute,
      private daterangepickerOptions: DaterangepickerConfig, 
      private _common_service: CommonService,

      private _company_service: CompanyService,
      private _employee_service: EmployeeService
    ) {
      this.daterangepickerOptions.settings = {}; 

      this.body.classList.add("skin-blue");
      this.body.classList.add("sidebar-mini");

      this.getCompanies();

      this.createMasterSchduleForm = _fb.group({
        'company': ['', [Validators.required]],
        'employees': [null, [Validators.required]],
        'date_range': ['', [Validators.required]],
      });
  }

  ngOnInit() {
    let user = this._payroll_service.getPayrolls().
      subscribe(
        data => {
           this.payroll_id = Array.from(data);
           this.rerender();
        },
        err => console.error(err)
      );

    this.dtOptions = {
      //   order: [[6, 'desc']],
      //   "columnDefs": [
      //   { "width": "20%", "targets": [0,6] },
      //   { "width": "15%", "targets": [1,5] },
      //   { "width": "10%", "targets": [2,3,4] },
      // ],
      "responsive": true
      };
      this.getSuggestedWorkingSchedule();
  }  

  clickRow(id) {
    this._payroll_service.setId(id);
    this._rt.navigate(['admin/payroll/', id]);
  }

  private catchError(error: any){
      let response_body = error._body;
      let response_status = error.status;

      if( response_status == 500 ){
        this.error_title = 'Error 500';
        this.error_message = 'Something went wrong';
      } else if( response_status == 200 ) {
        this.error_title = '';
        this.error_message = '';
      }
  }

  private selectedDate(value:any, dateInput:any) {
    let date_range_val = null;
    dateInput.start = value.start;
    dateInput.end = value.end;
    //format date
    this.start_date = moment(dateInput.start).format("YYYY-MM-DD");
    this.end_date = moment(dateInput.end).format("YYYY-MM-DD");

    date_range_val = [this.start_date, this.end_date];
    date_range_val = date_range_val.join(' - ');

    this.createMasterSchduleForm.get('date_range').setValue(date_range_val);
  }

  generatePayroll() {
    this._rt.navigate(['/admin/payroll-generate']);
  }

  ngOnDestroy() {
      this.body.classList.remove("skin-blue");
      this.body.classList.remove("sidebar-mini");
  }

  ngAfterViewInit(): void {
      this.dtTrigger.next();
  }

  private getCompanies(){
    this._employee_service.getCompanies().subscribe(
      data => {
        this.setCompanies(data);
      },
      err => this.catchError(err)
    );
  }

  private setCompanies(data:any ){
    let companies = data;
    let fetched_companies = [];

    for(var _i = 0; _i < companies.length; _i++){
      fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
    }

    this.form_companies = fetched_companies;
  }

  primaryCompanyChanged(){
    let company_id = this.createMasterSchduleForm.get('company').value;

    if(company_id != null){
      this.company_id = company_id;
      // let company_id = this.createEmployeeForm.get('primary_company').value;
      this._employee_service.getEmployees('company_id=' + company_id).subscribe(
        data => {
          // employee selected
          let employee_array = [];
          let index_company_employee = 0;

          for(let company_employee of data){
            employee_array[index_company_employee] = {
              id: company_employee.id,
              data: company_employee.lastname + ', ' + company_employee.firstname,
              employee_id: company_employee.id,
              company_id: company_id
            };
            index_company_employee++;
          }
          this.form_company_employees = employee_array;
        }
      );

      this._company_service.getCompany(company_id).subscribe(
        data => {
          this.setCompanySchedulesSelection(data.schedules);
        }
      );
    }
  }

  setCompanySchedulesSelection( data: any){
    let schedule_selection = [];
    let index = 0;

    for(let sched of data){
      let days = JSON.parse(sched.days);
      days = days.join();

      schedule_selection[index] = { id: sched.id, name: sched.name, timein: sched.timein, timeout: sched.timeout, days: days };
      this.fetched_company_suggested_schedules[sched.id] = sched;
      index++;
    }

    this.form_suggested_schedules = schedule_selection;
  }

  public renderScheduleCreationForms(){
    let index_count_schedule_grid = 0;
    let index_count_schedule_emp_grid = 0;
    let render_schedule_creation_grid = [];
    let render_schedule_creation_employee = [];
    let raw_company_working_schedules_datas = {
      'employee_id': [],
      'company_id': [],
      'schedule_date': [],
      'timein': [],
      'timeout': [],
      'breakin': [],
      'breakout': [],
      'lunch_mins_break': []
    };
    let index_raw_company_working_schedules_datas = 0;
    let schedule_master_model = this.createMasterSchduleForm.value;
    let split_dates = schedule_master_model.date_range;
    split_dates = split_dates.split(' - ');

    let dates_between_two_dates = this._common_service.getDates(new Date(split_dates[0]), new Date(split_dates[1]));

    // RENDERING OF EMPLOYEES
    for(let company_employee of this.form_company_employees){
      for(let selected_employees of schedule_master_model.employees){
        if( company_employee.id == selected_employees ){
          console.log(company_employee);
          render_schedule_creation_employee[index_count_schedule_emp_grid] = Object.assign(company_employee, { 'schedule': null, 'time-in':null, 'lunch-in':null, 'lunch-out':null, 'time-out':null , 'lunch_mins_break': 60 });
          // console.log(render_schedule_creation_employee[index_count_schedule_emp_grid]);
          index_count_schedule_emp_grid++;
        }
      }
    }

    // RENDERING OF DAYS
    let date1 = new Date(split_dates[0]);
    let date2 = new Date(split_dates[1]);

    let timeDiff = Math.abs(date2.getTime() - date1.getTime());
    let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    diffDays = diffDays + 1;

    // RENDERING FORMS FOR SUBMITION
    for(; diffDays > index_count_schedule_grid; index_count_schedule_grid++){
      render_schedule_creation_grid[index_count_schedule_grid] = {
        'schedule_date': dates_between_two_dates[index_count_schedule_grid].getMonth() + '/' + dates_between_two_dates[index_count_schedule_grid].getDate() + '/' + dates_between_two_dates[index_count_schedule_grid].getFullYear(),
        'day': this._common_service.getDay(dates_between_two_dates[index_count_schedule_grid].getDay()),
        'employees': render_schedule_creation_employee
      };
    }

    this.form_schedule_creation_forms_array = render_schedule_creation_grid;

    for(let raw_company_working_schedules of render_schedule_creation_grid){
      let index_for_for_schedule_master = 0;

      raw_company_working_schedules_datas.employee_id[raw_company_working_schedules.schedule_date] = [];
      raw_company_working_schedules_datas.company_id[raw_company_working_schedules.schedule_date] = [];
      raw_company_working_schedules_datas.schedule_date[raw_company_working_schedules.schedule_date] = [];
      raw_company_working_schedules_datas.timein[raw_company_working_schedules.schedule_date] = [];
      raw_company_working_schedules_datas.timeout[raw_company_working_schedules.schedule_date] = [];
      raw_company_working_schedules_datas.breakin[raw_company_working_schedules.schedule_date] = [];
      raw_company_working_schedules_datas.breakout[raw_company_working_schedules.schedule_date] = [];
      raw_company_working_schedules_datas.lunch_mins_break[raw_company_working_schedules.schedule_date] = [];

      for(let raw_company_working_schedules_emp of raw_company_working_schedules.employees){
        raw_company_working_schedules_datas.employee_id[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules_emp.employee_id;

        raw_company_working_schedules_datas.company_id[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules_emp.company_id;

        raw_company_working_schedules_datas.schedule_date[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date;

        raw_company_working_schedules_datas.timein[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date + ' ' + raw_company_working_schedules_emp.timein;

        raw_company_working_schedules_datas.timeout[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date + ' ' + raw_company_working_schedules_emp.timeout;

        raw_company_working_schedules_datas.breakin[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date + ' ' + raw_company_working_schedules_emp.breakin;

        raw_company_working_schedules_datas.breakout[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date + ' ' + raw_company_working_schedules_emp.breakout;

        raw_company_working_schedules_datas.lunch_mins_break[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date + ' ' + raw_company_working_schedules_emp.lunch_mins_break;
        // raw_company_working_schedules_datas.breakout[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date + ' ' + raw_company_working_schedules_emp.breakout;
        index_for_for_schedule_master++;
      }
      index_raw_company_working_schedules_datas++;
    }

    this.company_working_schedules.employee_master_schedule = raw_company_working_schedules_datas;
  }

  private updateCompanyWorkingSchedules(form_schedule_master_create: any, form_schedule_master_create_valid: any){
    let has_empty_array_values = 0;
    let json_str_form_schedule_master_create = JSON.stringify(form_schedule_master_create);
    json_str_form_schedule_master_create = JSON.parse(json_str_form_schedule_master_create);

    this.error_title = null;
    this.error_message = null;
    this.message_title = null;
    this.message_success = null;

    this._employee_service.updateCompanyMasterSchedules(json_str_form_schedule_master_create)
      .subscribe(
        data => {
          console.log(data);
          // this.putData = Array.from(data); // fetched the records
          this.message_title = 'Oh Yes!';
          this.message_success = 'You have successfully add the following schedules for the selected employees.';

          setTimeout(() => {
            this._rt.navigate(['admin/schedule-master-list']);
          }, 3000);
        },
        err => {
          this.error_title = 'Oops!';
          this.error_message = 'Please check all fields if all values are valid.';
          console.error(err);
        }
      );
    // this._schedule_adj_service.createMySA;
  }

  private getSuggestedWorkingSchedule(){
    this._common_service.getSuggestedWorkingSchedule().
    subscribe(
      data => {
        this.work_sched = data;
        this.options = {
          dropdownAutoWidth: true
        };
        this.ws_value = [];
        this.ws_current = this.ws_value;

        let id = 0;
        let text = 'Suggested Schedule';

        this.work_sched.unshift({id,text});

        this.ws_value = this.value;
        console.log('working schedules suggestion');
        console.log(this.work_sched);
      }
    );
  }

  private changedWorkingSchedule(data: any,i_a: any, i_b: any){
    let select_option = data;
    let index_a = i_a;
    let index_b = i_b;
    console.log(select_option);
    
    let start_time = select_option.data[0].time_in;
    let end_time = select_option.data[0].time_out;
    let break_start = select_option.data[0].break_start;
    let lunch_mins_duration = select_option.data[0].lunch_mins_break;
    
    let start_time_str = this.displayLocalFormatTime(start_time);
    let end_time_str = this.displayLocalFormatTime(end_time);
    let break_time_str = this.displayLocalFormatTime(break_start);
    
    this.company_working_schedules.employee_master_schedule.timein[index_a][index_b]=start_time_str;
    this.company_working_schedules.employee_master_schedule.timeout[index_a][index_b] = end_time_str;
    this.company_working_schedules.employee_master_schedule.breakin[index_a][index_b] = break_time_str;
    this.company_working_schedules.employee_master_schedule.lunch_mins_break[index_a][index_b] = lunch_mins_duration;
  }

  changed(data: any) {
    // if(this.single == true){
    //   let v = [];
    //   v.push(data.value);
    //   this.current = v;
    // }
    // else{
    //   this.current = data.value;
    // }
    
    // let len = this.current.length;

    // if(len >= 1){
    //   this.empVal = true;
    // }
    // else{
    //   this.empVal = false;
    // }

    this.validation();
    this.duplicateEntryValidation();
  }

  validation(){
    // if(this.empVal == true && this.tableVal==true && this.dupVal == true){
    //   this.validate = true;
    // }
    // else{
    //   this.validate = false;
    // }
  }

  duplicateEntryValidation(){
    // this.SAForm.value.employee_id = this.current;
    // this.SAForm.value.supervisor_id = this.supervisor_id;
    // this.SAForm.value.start_date = moment(this.start_date).format("YYYY-MM-DD");
    // this.SAForm.value.end_date = moment(this.end_date).format("YYYY-MM-DD");
    // this.SAForm.value.created_by = this.user_id;
    // this.SAForm.value.details = this.dataStorage;

    // if(this.user == true  && this.create == true || this.user == true && this.edit == true){
    //   let v = [];
    //   v.push(this.employee_id);
    //   this.SAForm.value.employee_id = v;
    // }

    // if(this.edit == true){
    //   this.SAForm.value.edit = this.edit;
    //   this.SAForm.value.id= this.sa_id;
    // }
    // else{
    //   this.SAForm.value.edit = false;
    // }

    // let sa = this.SAForm.value;

    // if(this.SAForm.value.employee_id != 0 && this.SAForm.value.details.length != 0 || this.edit == true){
    //   this._SAservice
    //     .duplicateEntryValidation(sa)
    //     .subscribe(
    //       data => {
    //         this.poststore = data;
    //         if(this.poststore.message == "duplicate"){
    //             this.dupVal = false;
    //             this.errorCatch();
    //         }
    //         else{
    //           this.dupVal = true;
    //           this.error_title = '';
    //           this.error_message = '';
    //         }

    //         this.validation();
    //       },
    //       err => this.catchError(err)
    //     );
    // }
  }

   rerender(): void {
     // this.dtElement.dtInstance.then((dtInstance) => {
     //   dtInstance.destroy();
     //   this.dtTrigger.next();
     // });
   }
   displayLocalFormatTime(time_param: any) {
        let date = new Date("February 04, 2011 " + time_param);
        let options = {
          hour: '2-digit',
          minute: '2-digit',
          hour12: true
        };
        let timeString = date.toLocaleString('en-US', options);
        let time_split = timeString.split(":");
        // console.log(time_split);
        let time_split_hour = (Number(time_split[0]) < 10) ? '0' + time_split[0] : time_split[0];
        let time_split_min = time_split[1].split(' ')[0];
        // let time_split_min = (Number(time_split[1]) < 10) ? '0' + time_split[1] : time_split[1];
        let time_split_ampm = time_split[1].split(" ");
        let new_time_string = time_split_hour + ':' + time_split_min ;//+ ' ' + time_split_ampm[1];
        // console.log(new_time_string);

        return new_time_string;
    }
}