var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute } from '@angular/router';
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { EmployeeService } from '../../services/employee.service';
import { Subject } from 'rxjs/Rx';
import { PayrollService } from '../../services/payroll.service';
var ScheduleMasterListComponent = /** @class */ (function () {
    function ScheduleMasterListComponent(_conf, _payroll_service, _employee_service, modalService, _rt, _ar) {
        this._conf = _conf;
        this._payroll_service = _payroll_service;
        this._employee_service = _employee_service;
        this.modalService = modalService;
        this._rt = _rt;
        this._ar = _ar;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api_employee_master_schedule = this._conf.ServerWithApiUrl + 'employee_schedule_master/';
        console.log(this.api_employee_master_schedule);
    }
    ScheduleMasterListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var auth_token = localStorage.getItem('id_token');
        this.dtOptions = {
            bFilter: true,
            searching: true,
            ajax: {
                url: this.api_employee_master_schedule,
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + auth_token);
                },
            },
            columns: [
                {
                    title: 'Sched. ID',
                    data: 'id'
                },
                {
                    title: 'Employee',
                    data: 'employee_fullname'
                },
                {
                    title: 'Company',
                    data: 'company_name'
                },
                {
                    title: 'Date Schedule',
                    data: 'schedule_date'
                },
                {
                    title: 'Time-In',
                    data: 'timein'
                },
                {
                    title: 'Time-Out',
                    data: 'timeout'
                },
                {
                    title: 'Break-In',
                    data: 'breakin'
                },
                {
                    title: 'Break-Out',
                    data: 'breakout'
                }
            ],
            rowCallback: function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
                var self = _this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', function () {
                    _this._rt.navigate(['admin/schedule-master-edit/', data.id]);
                });
                return nRow;
            }
        };
    };
    ScheduleMasterListComponent.prototype.clickRow = function (id) {
        this._payroll_service.setId(id);
        this._rt.navigate(['admin/payroll/', id]);
    };
    ScheduleMasterListComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    ScheduleMasterListComponent.prototype.createSchedule = function () {
        this._rt.navigate(['/admin/schedule-master-create']);
    };
    ScheduleMasterListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    ScheduleMasterListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], ScheduleMasterListComponent.prototype, "dtElement", void 0);
    ScheduleMasterListComponent = __decorate([
        Component({
            selector: 'schedule-master-list',
            templateUrl: './schedule-master-list.component.html',
            styleUrls: ['./schedule-master-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration,
            PayrollService,
            EmployeeService,
            DialogService,
            Router,
            ActivatedRoute])
    ], ScheduleMasterListComponent);
    return ScheduleMasterListComponent;
}());
export { ScheduleMasterListComponent };
//# sourceMappingURL=schedule-master-list.component.js.map