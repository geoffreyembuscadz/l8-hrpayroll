var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Configuration } from '../../app.config';
import { DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import 'rxjs/add/operator/map';
var ScheduleMasterEditComponent = /** @class */ (function () {
    function ScheduleMasterEditComponent(dialogService, _perms_service, modalService, _ar, _conf, _company_service, _employee_service, _rt, _fb) {
        var _this = this;
        this._perms_service = _perms_service;
        this.modalService = modalService;
        this._ar = _ar;
        this._conf = _conf;
        this._company_service = _company_service;
        this._employee_service = _employee_service;
        this._rt = _rt;
        this._fb = _fb;
        this.working_schedules = [];
        this.form_days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
        this.message_title = '';
        this.message_error = '';
        this.message_success = '';
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.api_company = this._conf.ServerWithApiUrl + 'company/';
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.id = this._ar.snapshot.params['schedule_master_id']; // schedule master id
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this._employee_service.getMasterSchedule(this.id).subscribe(function (data) {
            _this.setMasterSchedule(data);
        }, function (err) { return console.error(err); });
        this.updateScheduleMasterForm = this._fb.group({
            'id': [this.id, Validators.required],
            'employee_fullname': [null, [Validators.required]],
            'company_name': [null, [Validators.required]],
            'company_id': [null, [Validators.required]],
            'schedule_date': [null, [Validators.required]],
            'timein': [null, [Validators.required]],
            'breakin': [null, [Validators.required]],
            'timeout': [null, [Validators.required]],
            'lunch_min_break': [null, [Validators.required]]
        });
    }
    ScheduleMasterEditComponent.prototype.setMasterSchedule = function (data) {
        this.form_schedule_master = data;
        var timein = this.form_schedule_master.timein;
        timein = timein.split(" ")[1];
        var timeout = this.form_schedule_master.timeout;
        timeout = timeout.split(" ")[1];
        var breakin = this.form_schedule_master.breakin;
        breakin = breakin.split(" ")[1];
        var breakout = this.form_schedule_master.breakout;
        breakout = breakout.split(" ")[1];
        var sub_a = new Date(this.form_schedule_master.breakout);
        var sub_b = new Date(this.form_schedule_master.breakin);
        var sub_diff = sub_a.getTime() - sub_b.getTime();
        var lunch_duration = Math.floor(sub_diff / 1000 / 60);
        this.updateScheduleMasterForm = this._fb.group({
            'id': [this.id, [Validators.required]],
            'employee_fullname': [this.form_schedule_master.employee_record.firstname + ' ' + this.form_schedule_master.employee_record.lastname, [Validators.required]],
            'company_name': [this.form_schedule_master.employee_company.name, [Validators.required]],
            'company_id': [this.form_schedule_master.company_id, [Validators.required]],
            'schedule_date': [this.form_schedule_master.schedule_date, [Validators.required]],
            'timein': [timein, [Validators.required]],
            'breakin': [breakin, [Validators.required]],
            'timeout': [timeout, [Validators.required]],
            'lunch_min_break': [lunch_duration, [Validators.required]]
        });
    };
    ScheduleMasterEditComponent.prototype.updateEmployeeWorkingSchedule = function () {
        var _this = this;
        this.message_title = null;
        this.message_error = null;
        this.message_success = null;
        this._employee_service.updateScheduleMaster(this.id, this.updateScheduleMasterForm.value).subscribe(function (data) {
            console.log(data);
            _this.message_title = 'Oh Yes!';
            _this.message_success = 'You have successfully add the following schedules for the selected employees.';
            setTimeout(function () {
                _this._rt.navigate(['admin/schedule-master-list']);
            }, 3000);
        }, function (err) {
            _this.message_title = 'Oops!';
            _this.message_error = 'Please check all fields if all values are valid.';
            console.error(err);
        });
    };
    ScheduleMasterEditComponent.prototype.ngOnInit = function () {
    };
    ScheduleMasterEditComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    ScheduleMasterEditComponent.prototype.clearMsg = function () {
        this.message_title = '';
        this.message_error = '';
        this.message_success = '';
    };
    ScheduleMasterEditComponent = __decorate([
        Component({
            selector: 'app-schedule-master-edit',
            templateUrl: './schedule-master-edit.component.html',
            styleUrls: ['./schedule-master-edit.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService, PermissionService, DialogService, ActivatedRoute, Configuration, CompanyService, EmployeeService, Router, FormBuilder])
    ], ScheduleMasterEditComponent);
    return ScheduleMasterEditComponent;
}());
export { ScheduleMasterEditComponent };
//# sourceMappingURL=schedule-master-edit.component.js.map