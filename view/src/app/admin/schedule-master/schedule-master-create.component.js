var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute } from '@angular/router';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Configuration } from './../../app.config';
import { EmployeeScheduleMaster } from '../../model/employee-schedule-master';
import { DataTableDirective } from 'angular-datatables';
import * as moment from 'moment';
import { Subject } from 'rxjs/Rx';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { CommonService } from '../../services/common.service';
import { PayrollService } from '../../services/payroll.service';
var ScheduleMasterCreateComponent = /** @class */ (function () {
    function ScheduleMasterCreateComponent(_conf, _payroll_service, modalService, _rt, _fb, _ar, daterangepickerOptions, _common_service, _company_service, _employee_service) {
        this._conf = _conf;
        this._payroll_service = _payroll_service;
        this.modalService = modalService;
        this._rt = _rt;
        this._fb = _fb;
        this._ar = _ar;
        this.daterangepickerOptions = daterangepickerOptions;
        this._common_service = _common_service;
        this._company_service = _company_service;
        this._employee_service = _employee_service;
        this.dtTrigger = new Subject();
        this.fetched_company_suggested_schedules = [];
        this.company_working_schedules = new EmployeeScheduleMaster();
        // Working Schedule Suggestions
        this.work_sched = [];
        this.mainInput = {
            start: moment(),
            end: moment().add(1, 'days')
        };
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.daterangepickerOptions.settings = {};
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.getCompanies();
        this.createMasterSchduleForm = _fb.group({
            'company': ['', [Validators.required]],
            'employees': [null, [Validators.required]],
            'date_range': ['', [Validators.required]],
        });
    }
    ScheduleMasterCreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        var user = this._payroll_service.getPayrolls().
            subscribe(function (data) {
            _this.payroll_id = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
        this.dtOptions = {
            //   order: [[6, 'desc']],
            //   "columnDefs": [
            //   { "width": "20%", "targets": [0,6] },
            //   { "width": "15%", "targets": [1,5] },
            //   { "width": "10%", "targets": [2,3,4] },
            // ],
            "responsive": true
        };
        this.getSuggestedWorkingSchedule();
    };
    ScheduleMasterCreateComponent.prototype.clickRow = function (id) {
        this._payroll_service.setId(id);
        this._rt.navigate(['admin/payroll/', id]);
    };
    ScheduleMasterCreateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    ScheduleMasterCreateComponent.prototype.selectedDate = function (value, dateInput) {
        var date_range_val = null;
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_date = moment(dateInput.start).format("YYYY-MM-DD");
        this.end_date = moment(dateInput.end).format("YYYY-MM-DD");
        date_range_val = [this.start_date, this.end_date];
        date_range_val = date_range_val.join(' - ');
        this.createMasterSchduleForm.get('date_range').setValue(date_range_val);
    };
    ScheduleMasterCreateComponent.prototype.generatePayroll = function () {
        this._rt.navigate(['/admin/payroll-generate']);
    };
    ScheduleMasterCreateComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    ScheduleMasterCreateComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    ScheduleMasterCreateComponent.prototype.getCompanies = function () {
        var _this = this;
        this._employee_service.getCompanies().subscribe(function (data) {
            _this.setCompanies(data);
        }, function (err) { return _this.catchError(err); });
    };
    ScheduleMasterCreateComponent.prototype.setCompanies = function (data) {
        var companies = data;
        var fetched_companies = [];
        for (var _i = 0; _i < companies.length; _i++) {
            fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
        }
        this.form_companies = fetched_companies;
    };
    ScheduleMasterCreateComponent.prototype.primaryCompanyChanged = function () {
        var _this = this;
        var company_id = this.createMasterSchduleForm.get('company').value;
        if (company_id != null) {
            this.company_id = company_id;
            // let company_id = this.createEmployeeForm.get('primary_company').value;
            this._employee_service.getEmployees('company_id=' + company_id).subscribe(function (data) {
                // employee selected
                var employee_array = [];
                var index_company_employee = 0;
                for (var _a = 0, data_1 = data; _a < data_1.length; _a++) {
                    var company_employee = data_1[_a];
                    employee_array[index_company_employee] = {
                        id: company_employee.id,
                        data: company_employee.lastname + ', ' + company_employee.firstname,
                        employee_id: company_employee.id,
                        company_id: company_id
                    };
                    index_company_employee++;
                }
                _this.form_company_employees = employee_array;
            });
            this._company_service.getCompany(company_id).subscribe(function (data) {
                _this.setCompanySchedulesSelection(data.schedules);
            });
        }
    };
    ScheduleMasterCreateComponent.prototype.setCompanySchedulesSelection = function (data) {
        var schedule_selection = [];
        var index = 0;
        for (var _a = 0, data_2 = data; _a < data_2.length; _a++) {
            var sched = data_2[_a];
            var days = JSON.parse(sched.days);
            days = days.join();
            schedule_selection[index] = { id: sched.id, name: sched.name, timein: sched.timein, timeout: sched.timeout, days: days };
            this.fetched_company_suggested_schedules[sched.id] = sched;
            index++;
        }
        this.form_suggested_schedules = schedule_selection;
    };
    ScheduleMasterCreateComponent.prototype.renderScheduleCreationForms = function () {
        var index_count_schedule_grid = 0;
        var index_count_schedule_emp_grid = 0;
        var render_schedule_creation_grid = [];
        var render_schedule_creation_employee = [];
        var raw_company_working_schedules_datas = {
            'employee_id': [],
            'company_id': [],
            'schedule_date': [],
            'timein': [],
            'timeout': [],
            'breakin': [],
            'breakout': [],
            'lunch_mins_break': []
        };
        var index_raw_company_working_schedules_datas = 0;
        var schedule_master_model = this.createMasterSchduleForm.value;
        var split_dates = schedule_master_model.date_range;
        split_dates = split_dates.split(' - ');
        var dates_between_two_dates = this._common_service.getDates(new Date(split_dates[0]), new Date(split_dates[1]));
        // RENDERING OF EMPLOYEES
        for (var _a = 0, _b = this.form_company_employees; _a < _b.length; _a++) {
            var company_employee = _b[_a];
            for (var _c = 0, _d = schedule_master_model.employees; _c < _d.length; _c++) {
                var selected_employees = _d[_c];
                if (company_employee.id == selected_employees) {
                    console.log(company_employee);
                    render_schedule_creation_employee[index_count_schedule_emp_grid] = Object.assign(company_employee, { 'schedule': null, 'time-in': null, 'lunch-in': null, 'lunch-out': null, 'time-out': null, 'lunch_mins_break': 60 });
                    // console.log(render_schedule_creation_employee[index_count_schedule_emp_grid]);
                    index_count_schedule_emp_grid++;
                }
            }
        }
        // RENDERING OF DAYS
        var date1 = new Date(split_dates[0]);
        var date2 = new Date(split_dates[1]);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        diffDays = diffDays + 1;
        // RENDERING FORMS FOR SUBMITION
        for (; diffDays > index_count_schedule_grid; index_count_schedule_grid++) {
            render_schedule_creation_grid[index_count_schedule_grid] = {
                'schedule_date': dates_between_two_dates[index_count_schedule_grid].getMonth() + '/' + dates_between_two_dates[index_count_schedule_grid].getDate() + '/' + dates_between_two_dates[index_count_schedule_grid].getFullYear(),
                'day': this._common_service.getDay(dates_between_two_dates[index_count_schedule_grid].getDay()),
                'employees': render_schedule_creation_employee
            };
        }
        this.form_schedule_creation_forms_array = render_schedule_creation_grid;
        for (var _e = 0, render_schedule_creation_grid_1 = render_schedule_creation_grid; _e < render_schedule_creation_grid_1.length; _e++) {
            var raw_company_working_schedules = render_schedule_creation_grid_1[_e];
            var index_for_for_schedule_master = 0;
            raw_company_working_schedules_datas.employee_id[raw_company_working_schedules.schedule_date] = [];
            raw_company_working_schedules_datas.company_id[raw_company_working_schedules.schedule_date] = [];
            raw_company_working_schedules_datas.schedule_date[raw_company_working_schedules.schedule_date] = [];
            raw_company_working_schedules_datas.timein[raw_company_working_schedules.schedule_date] = [];
            raw_company_working_schedules_datas.timeout[raw_company_working_schedules.schedule_date] = [];
            raw_company_working_schedules_datas.breakin[raw_company_working_schedules.schedule_date] = [];
            raw_company_working_schedules_datas.breakout[raw_company_working_schedules.schedule_date] = [];
            raw_company_working_schedules_datas.lunch_mins_break[raw_company_working_schedules.schedule_date] = [];
            for (var _f = 0, _g = raw_company_working_schedules.employees; _f < _g.length; _f++) {
                var raw_company_working_schedules_emp = _g[_f];
                raw_company_working_schedules_datas.employee_id[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules_emp.employee_id;
                raw_company_working_schedules_datas.company_id[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules_emp.company_id;
                raw_company_working_schedules_datas.schedule_date[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date;
                raw_company_working_schedules_datas.timein[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date + ' ' + raw_company_working_schedules_emp.timein;
                raw_company_working_schedules_datas.timeout[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date + ' ' + raw_company_working_schedules_emp.timeout;
                raw_company_working_schedules_datas.breakin[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date + ' ' + raw_company_working_schedules_emp.breakin;
                raw_company_working_schedules_datas.breakout[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date + ' ' + raw_company_working_schedules_emp.breakout;
                raw_company_working_schedules_datas.lunch_mins_break[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date + ' ' + raw_company_working_schedules_emp.lunch_mins_break;
                // raw_company_working_schedules_datas.breakout[raw_company_working_schedules.schedule_date][index_for_for_schedule_master] = raw_company_working_schedules.schedule_date + ' ' + raw_company_working_schedules_emp.breakout;
                index_for_for_schedule_master++;
            }
            index_raw_company_working_schedules_datas++;
        }
        this.company_working_schedules.employee_master_schedule = raw_company_working_schedules_datas;
    };
    ScheduleMasterCreateComponent.prototype.updateCompanyWorkingSchedules = function (form_schedule_master_create, form_schedule_master_create_valid) {
        var _this = this;
        var has_empty_array_values = 0;
        var json_str_form_schedule_master_create = JSON.stringify(form_schedule_master_create);
        json_str_form_schedule_master_create = JSON.parse(json_str_form_schedule_master_create);
        this.error_title = null;
        this.error_message = null;
        this.message_title = null;
        this.message_success = null;
        this._employee_service.updateCompanyMasterSchedules(json_str_form_schedule_master_create)
            .subscribe(function (data) {
            console.log(data);
            // this.putData = Array.from(data); // fetched the records
            _this.message_title = 'Oh Yes!';
            _this.message_success = 'You have successfully add the following schedules for the selected employees.';
            setTimeout(function () {
                _this._rt.navigate(['admin/schedule-master-list']);
            }, 3000);
        }, function (err) {
            _this.error_title = 'Oops!';
            _this.error_message = 'Please check all fields if all values are valid.';
            console.error(err);
        });
        // this._schedule_adj_service.createMySA;
    };
    ScheduleMasterCreateComponent.prototype.getSuggestedWorkingSchedule = function () {
        var _this = this;
        this._common_service.getSuggestedWorkingSchedule().
            subscribe(function (data) {
            _this.work_sched = data;
            _this.options = {
                dropdownAutoWidth: true
            };
            _this.ws_value = [];
            _this.ws_current = _this.ws_value;
            var id = 0;
            var text = 'Suggested Schedule';
            _this.work_sched.unshift({ id: id, text: text });
            _this.ws_value = _this.value;
            console.log('working schedules suggestion');
            console.log(_this.work_sched);
        });
    };
    ScheduleMasterCreateComponent.prototype.changedWorkingSchedule = function (data, i_a, i_b) {
        var select_option = data;
        var index_a = i_a;
        var index_b = i_b;
        console.log(select_option);
        var start_time = select_option.data[0].time_in;
        var end_time = select_option.data[0].time_out;
        var break_start = select_option.data[0].break_start;
        var lunch_mins_duration = select_option.data[0].lunch_mins_break;
        var start_time_str = this.displayLocalFormatTime(start_time);
        var end_time_str = this.displayLocalFormatTime(end_time);
        var break_time_str = this.displayLocalFormatTime(break_start);
        this.company_working_schedules.employee_master_schedule.timein[index_a][index_b] = start_time_str;
        this.company_working_schedules.employee_master_schedule.timeout[index_a][index_b] = end_time_str;
        this.company_working_schedules.employee_master_schedule.breakin[index_a][index_b] = break_time_str;
        this.company_working_schedules.employee_master_schedule.lunch_mins_break[index_a][index_b] = lunch_mins_duration;
    };
    ScheduleMasterCreateComponent.prototype.changed = function (data) {
        // if(this.single == true){
        //   let v = [];
        //   v.push(data.value);
        //   this.current = v;
        // }
        // else{
        //   this.current = data.value;
        // }
        // let len = this.current.length;
        // if(len >= 1){
        //   this.empVal = true;
        // }
        // else{
        //   this.empVal = false;
        // }
        this.validation();
        this.duplicateEntryValidation();
    };
    ScheduleMasterCreateComponent.prototype.validation = function () {
        // if(this.empVal == true && this.tableVal==true && this.dupVal == true){
        //   this.validate = true;
        // }
        // else{
        //   this.validate = false;
        // }
    };
    ScheduleMasterCreateComponent.prototype.duplicateEntryValidation = function () {
        // this.SAForm.value.employee_id = this.current;
        // this.SAForm.value.supervisor_id = this.supervisor_id;
        // this.SAForm.value.start_date = moment(this.start_date).format("YYYY-MM-DD");
        // this.SAForm.value.end_date = moment(this.end_date).format("YYYY-MM-DD");
        // this.SAForm.value.created_by = this.user_id;
        // this.SAForm.value.details = this.dataStorage;
        // if(this.user == true  && this.create == true || this.user == true && this.edit == true){
        //   let v = [];
        //   v.push(this.employee_id);
        //   this.SAForm.value.employee_id = v;
        // }
        // if(this.edit == true){
        //   this.SAForm.value.edit = this.edit;
        //   this.SAForm.value.id= this.sa_id;
        // }
        // else{
        //   this.SAForm.value.edit = false;
        // }
        // let sa = this.SAForm.value;
        // if(this.SAForm.value.employee_id != 0 && this.SAForm.value.details.length != 0 || this.edit == true){
        //   this._SAservice
        //     .duplicateEntryValidation(sa)
        //     .subscribe(
        //       data => {
        //         this.poststore = data;
        //         if(this.poststore.message == "duplicate"){
        //             this.dupVal = false;
        //             this.errorCatch();
        //         }
        //         else{
        //           this.dupVal = true;
        //           this.error_title = '';
        //           this.error_message = '';
        //         }
        //         this.validation();
        //       },
        //       err => this.catchError(err)
        //     );
        // }
    };
    ScheduleMasterCreateComponent.prototype.rerender = function () {
        // this.dtElement.dtInstance.then((dtInstance) => {
        //   dtInstance.destroy();
        //   this.dtTrigger.next();
        // });
    };
    ScheduleMasterCreateComponent.prototype.displayLocalFormatTime = function (time_param) {
        var date = new Date("February 04, 2011 " + time_param);
        var options = {
            hour: '2-digit',
            minute: '2-digit',
            hour12: true
        };
        var timeString = date.toLocaleString('en-US', options);
        var time_split = timeString.split(":");
        // console.log(time_split);
        var time_split_hour = (Number(time_split[0]) < 10) ? '0' + time_split[0] : time_split[0];
        var time_split_min = time_split[1].split(' ')[0];
        // let time_split_min = (Number(time_split[1]) < 10) ? '0' + time_split[1] : time_split[1];
        var time_split_ampm = time_split[1].split(" ");
        var new_time_string = time_split_hour + ':' + time_split_min; //+ ' ' + time_split_ampm[1];
        // console.log(new_time_string);
        return new_time_string;
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], ScheduleMasterCreateComponent.prototype, "dtElement", void 0);
    ScheduleMasterCreateComponent = __decorate([
        Component({
            selector: 'schedule-master-list',
            templateUrl: './schedule-master-create.component.html',
            styleUrls: ['./schedule-master-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration,
            PayrollService,
            DialogService,
            Router,
            FormBuilder,
            ActivatedRoute,
            DaterangepickerConfig,
            CommonService,
            CompanyService,
            EmployeeService])
    ], ScheduleMasterCreateComponent);
    return ScheduleMasterCreateComponent;
}());
export { ScheduleMasterCreateComponent };
//# sourceMappingURL=schedule-master-create.component.js.map