import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Configuration } from '../../app.config';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";

import { PermissionService } from '../../services/permission.service';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component({
    selector: 'app-schedule-master-edit',
    templateUrl: './schedule-master-edit.component.html',
    styleUrls: ['./schedule-master-edit.component.css']
})

@Injectable()
export class ScheduleMasterEditComponent implements OnInit {
    public id: string;
    public poststore: any;
    public putData: any;

    private working_schedules = [];
    private form_schedule_master: any;
    private updateScheduleMasterForm: FormGroup;
    private form_days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    public message_title = '';
    public message_error = '';
    public message_success = '';

    public response: any;

    // TEMP variables
    public confirm_archiving: any;

    private headers: Headers;
    dtOptions: any = {};
    company_rec: any;
    api_company: String;

    // get permissions list
    perm_rec: any;
    bodyClasses:string = "skin-blue sidebar-mini";
    body = document.getElementsByTagName('body')[0];

    constructor(dialogService: DialogService, private _perms_service: PermissionService, private modalService: DialogService, private _ar: ActivatedRoute, private _conf: Configuration, private _company_service: CompanyService, private _employee_service: EmployeeService, private _rt: Router, private _fb: FormBuilder){

        this.api_company = this._conf.ServerWithApiUrl + 'company/';
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.id = this._ar.snapshot.params['schedule_master_id']; // schedule master id

        // Header Variables
        this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');

        this._employee_service.getMasterSchedule(this.id).subscribe(
            data => {
                this.setMasterSchedule(data);
      
            },
            err => console.error(err)
        );
        
        this.updateScheduleMasterForm = this._fb.group({
            'id': [this.id, Validators.required],
            'employee_fullname': [null, [Validators.required]],
            'company_name': [null, [Validators.required]],
            
            'company_id': [null, [Validators.required]],
            'schedule_date': [null, [Validators.required]],
            'timein': [null, [Validators.required]],
            'breakin': [null, [Validators.required]],
            'timeout': [null, [Validators.required]],
            'lunch_min_break': [null, [Validators.required]]
        });    
    }

    private setMasterSchedule(data: any){
        this.form_schedule_master = data;

        let timein = this.form_schedule_master.timein;
        timein = timein.split(" ")[1];

        let timeout = this.form_schedule_master.timeout;
        timeout = timeout.split(" ")[1];

        let breakin = this.form_schedule_master.breakin;
        breakin = breakin.split(" ")[1];

        let breakout = this.form_schedule_master.breakout;
        breakout = breakout.split(" ")[1];

        let sub_a = new Date(this.form_schedule_master.breakout);
        let sub_b = new Date(this.form_schedule_master.breakin);
        let sub_diff = sub_a.getTime() - sub_b.getTime();
        let lunch_duration = Math.floor(sub_diff / 1000 / 60);

        this.updateScheduleMasterForm = this._fb.group({
            'id': [this.id, [Validators.required]],
            'employee_fullname': [this.form_schedule_master.employee_record.firstname + ' ' + this.form_schedule_master.employee_record.lastname , [Validators.required]],
            'company_name': [this.form_schedule_master.employee_company.name, [Validators.required]],
            
            'company_id': [this.form_schedule_master.company_id, [Validators.required]],
            'schedule_date': [this.form_schedule_master.schedule_date, [Validators.required]],
            'timein': [timein, [Validators.required]],
            'breakin': [breakin, [Validators.required]],
            'timeout': [timeout, [Validators.required]],
            'lunch_min_break': [lunch_duration, [Validators.required]]
        });
    }

    private updateEmployeeWorkingSchedule(){
        this.message_title = null;
                  this.message_error = null;
                  this.message_success = null;

        this._employee_service.updateScheduleMaster(this.id, this.updateScheduleMasterForm.value).subscribe(
            data => {
                console.log(data);

                this.message_title = 'Oh Yes!';
                this.message_success = 'You have successfully add the following schedules for the selected employees.';
                
                setTimeout(() => {
                    this._rt.navigate(['admin/schedule-master-list']);
                }, 3000);

            }, err => {
                this.message_title = 'Oops!';
                  this.message_error = 'Please check all fields if all values are valid.';
                  console.error(err);
            }
        );

    }

    ngOnInit() {
    }

    ngOnDestroy() {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    }

    private clearMsg() {
        this.message_title = '';
        this.message_error = '';
        this.message_success = '';        
    }
}
