import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { EmployeeService } from '../../services/employee.service';
import { Subject } from 'rxjs/Rx';

import { PayrollService } from '../../services/payroll.service';


@Component({
  selector: 'schedule-master-list',
  templateUrl: './schedule-master-list.component.html',
  styleUrls: ['./schedule-master-list.component.css']
})

@Injectable()
export class ScheduleMasterListComponent implements OnInit, AfterViewInit  {

	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	public id: string;
	public payroll_id: any;
	public success_title: string;

	public error_title: string;
    public error_message: string;
    public api_employee_master_schedule: string;

	dtOptions: any = {};

	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	payroll_rec: any;

	constructor(
			private _conf: Configuration, 
			private _payroll_service: PayrollService,
			private _employee_service: EmployeeService,
			private modalService: DialogService, 
			private _rt: Router,
			private _ar: ActivatedRoute, 
		) {
		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	    this.api_employee_master_schedule = this._conf.ServerWithApiUrl + 'employee_schedule_master/';
	    console.log(this.api_employee_master_schedule );

	}

	ngOnInit() {
		let auth_token = localStorage.getItem('id_token');

		this.dtOptions = {
			bFilter: true,
			searching: true,
			ajax: {
				url: this.api_employee_master_schedule,
				type: "GET",
				beforeSend: function(xhr){
					xhr.setRequestHeader("Authorization", "Bearer " + auth_token);
            	},
	        },
			columns: [
			{
				title: 'Sched. ID',
				data: 'id'
			},
			{
				title: 'Employee',
				data: 'employee_fullname'
			},
			{
				title: 'Company',
				data: 'company_name'
			},
			{
				title: 'Date Schedule',
				data: 'schedule_date'
			},
			{
				title: 'Time-In',
				data: 'timein'
			},
			{
				title: 'Time-Out',
				data: 'timeout'
			},
			{
				title: 'Break-In',
				data: 'breakin'
			},
			{
				title: 'Break-Out',
				data: 'breakout'

			}],
			rowCallback: (nRow: number, data: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
				let self = this;
				// Unbind first in order to avoid any duplicate handler
				// (see https://github.com/l-lin/angular-datatables/issues/87)
				$('td', nRow).unbind('click');
				$('td', nRow).bind('click', () => {
					this._rt.navigate(['admin/schedule-master-edit/', data.id]);
				});
				return nRow;
			}
		};
		
	}	

	clickRow(id) {
		this._payroll_service.setId(id);
		this._rt.navigate(['admin/payroll/', id]);
	}

	private catchError(error: any){
	    let response_body = error._body;
	    let response_status = error.status;

	    if( response_status == 500 ){
	      this.error_title = 'Error 500';
	      this.error_message = 'Something went wrong';
	    } else if( response_status == 200 ) {
	      this.error_title = '';
	      this.error_message = '';
	    }
	}

	createSchedule() {
        this._rt.navigate(['/admin/schedule-master-create']);
	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	}
	
}