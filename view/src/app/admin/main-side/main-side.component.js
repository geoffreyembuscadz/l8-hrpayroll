var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AuthUserService } from '../../services/auth-user.service';
import { MainSideService } from '../../services/main-side.service';
var MainSideComponent = /** @class */ (function () {
    function MainSideComponent(_auth_user_service, _main_side_service) {
        var _this = this;
        this._auth_user_service = _auth_user_service;
        this._main_side_service = _main_side_service;
        this.module_menu = false;
        this.userHasModule = false;
        this.userRole = Array();
        this.roleString = "";
        var auth_user = this._auth_user_service.getUser()
            .subscribe(function (data) {
            _this.user_rec = data;
            _this.id = data.id;
            _this.ngOnInit();
        });
    }
    MainSideComponent.prototype.hasModule = function (id) {
        if (this.module) {
            for (var i = 0; i < this.module.length; i++) {
                if (id == this.module[i]) {
                    return true;
                }
            }
        }
    };
    MainSideComponent.prototype.hasMenu = function (id) {
        if (this.menu) {
            for (var i = 0; i < this.menu.length; i++) {
                if (id == this.menu[i]) {
                    return true;
                }
            }
        }
    };
    MainSideComponent.prototype.ngOnInit = function () {
        var _this = this;
        var auth_user_id = this._auth_user_service.getUserById(this.id)
            .subscribe(function (data) {
            _this.user_id_rec = data;
            _this.roles = _this.user_id_rec.role_id;
            _this.name = _this.user_id_rec.name;
            if (_this.user_id_rec.role_module == undefined) {
                return false;
            }
            else {
                // split the role_module 
                _this.module = _this.user_id_rec.role_module.split(',');
            }
            if (_this.user_id_rec.role_menu == undefined) {
                return false;
            }
            else {
                _this.menu = _this.user_id_rec.role_menu.split(',');
            }
            _this._auth_user_service.setRoles(_this.roles);
        });
        var main_side = this._main_side_service.getModule()
            .subscribe(function (data) {
            _this.user_side = Array.from(data);
        });
    };
    MainSideComponent = __decorate([
        Component({
            selector: 'admin-main-side',
            templateUrl: './main-side.component.html',
            styleUrls: ['./main-side.component.css']
        }),
        __metadata("design:paramtypes", [AuthUserService, MainSideService])
    ], MainSideComponent);
    return MainSideComponent;
}());
export { MainSideComponent };
//# sourceMappingURL=main-side.component.js.map