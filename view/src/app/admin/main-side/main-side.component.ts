import { Component, OnInit } from '@angular/core';

import { AuthUserService } from '../../services/auth-user.service';
import { MainSideService } from '../../services/main-side.service';

import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';


@Component({
  selector: 'admin-main-side',
  templateUrl: './main-side.component.html',
  styleUrls: ['./main-side.component.css']
})
export class MainSideComponent implements OnInit {
  user_rec: any;
  user_id_rec: any;
  user_side: any;
  user_menu: any;
  module_id: any;
  module_name: any;
  public modulesArr: any;
  public modulesArrVal: any;
  public name: string;
  public id: string;
  public roles: string;
  public module: any;
  public menu: Array<any>;
  public action_url: string;
  public parent_id: string;
  public module_menu: Boolean = false;
  public role_name: Array<any>;
  public userHasModule: Boolean = false;

  private userRole: Array<String> = Array<String>();
  private roleString = "";
 
  constructor(private _auth_user_service: AuthUserService, private _main_side_service: MainSideService ) { 
    let auth_user = this._auth_user_service.getUser()
      .subscribe(
      data => {
        this.user_rec = data;
        this.id = data.id;

        this.ngOnInit();
        
      }
    );
    
  }


  hasModule(id) {
    if(this.module) {
      for (var i = 0; i < this.module.length; i++) {
        if(id == this.module[i]) {
          return true;
        }
      }
    }
  }

  hasMenu(id) {
    if(this.menu) {
      for (var i = 0; i < this.menu.length; i++) {
        if(id == this.menu[i]) {
          return true;
        }
      }
    }
  }

  ngOnInit() {

    let auth_user_id = this._auth_user_service.getUserById(this.id)
        .subscribe(
          data => {
              this.user_id_rec = data;
              this.roles = this.user_id_rec.role_id; 
              this.name = this.user_id_rec.name; 
              if(this.user_id_rec.role_module == undefined) {
                return false;
              } else {
                // split the role_module 
                this.module = this.user_id_rec.role_module.split(',');
              }

              if(this.user_id_rec.role_menu == undefined) {
                return false;
              } else {
                this.menu = this.user_id_rec.role_menu.split(',');
              }
 
              this._auth_user_service.setRoles(this.roles); 
          }
        );

    let main_side = this._main_side_service.getModule()
        .subscribe(
            data => {
                this.user_side = Array.from(data);
            }

         );

   
  }

}
