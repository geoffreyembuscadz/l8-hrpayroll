import 'rxjs/add/operator/catch'
import { Component, OnInit, AfterViewInit, NgZone, Inject } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response} from '@angular/http';
import { QuestionService } from '../../../services/question.service';
import { Configuration } from '../../../app.config';
import {} from 'jasmine';

@Component({
	selector: 'app-question-rating',
	templateUrl: './question-rating.component.html',
	styleUrls: ['./question-rating.component.css']
})

export class QuestionRatingComponent extends DialogComponent<null, boolean> {

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public poststore: any;
	public id: any;
	public question: any;
	
	questionRatingForm : FormGroup;
	edit:any;
	create:any;
	url:any;
	constructor(
		dialogService: DialogService, 
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute, 
		private _question_service: QuestionService,  
		private _rt: Router,
		private modalService: DialogService, ) {
		super(dialogService);

		this.questionRatingForm = _fb.group({
			'question': [null],
		});  
	}

	ngOnInit() {
		
	}
	
	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}
	

}