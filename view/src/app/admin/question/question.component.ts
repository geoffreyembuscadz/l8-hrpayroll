import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { QuestionService } from '../../services/question.service';
import { CommonService } from '../../services/common.service';
import { QuestionModalComponent } from '../question/question-modal/question-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';

@Component({
	selector: 'app-question',
	templateUrl: './question.component.html',
	styleUrls: ['./question.component.css']
})

@Injectable()
export class QuestionComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};
	public OT_id: any;
	api: String;
	status:any;
	question:any;
	
	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _common_service: CommonService,
		private _question_service: QuestionService,
		
		){

		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");

		this.api = this._conf.ServerWithApiUrl + 'question/';



		$(document).ready(function(){

			$('#stars li').on('mouseover', function(){
				var onStar = parseInt($(this).data('value'), 10);

				$(this).parent().children('li.star').each(function(e){
					if (e < onStar) {
						$(this).addClass('hover');
					}
					else {
						$(this).removeClass('hover');
					}
				});

			}).on('mouseout', function(){
				$(this).parent().children('li.star').each(function(e){
					$(this).removeClass('hover');
				});
			});


			$('#stars li').on('click', function(){
				var onStar = parseInt($(this).data('value'), 10);
				var stars = $(this).parent().children('li.star');

				for (var i = 0; i < stars.length; i++) {
					$(stars[i]).removeClass('selected');
				}

				for (i = 0; i < onStar; i++) {
					$(stars[i]).addClass('selected');
				}

				var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
				var msg = "";
				if (ratingValue > 1) {
					msg = "You choose  " + ratingValue + " stars.";
				} 
				responseMessage(msg);

			});


		});


		function responseMessage(msg) {
			$('.success-box').fadeIn(200);  
			$('.success-box div.text-message').html("<span>" + msg + "</span>");
		}
	}

	ngOnInit() {
		this.data();
	}

	data(){
		this._question_service.questionList().
		subscribe(
			data => {
				this.question = data; 
				this.rerender();
			},
			err => console.error(err)
			);
	}

	ngOnDestroy() {
		this.body.classList.remove("skin-blue");
		this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
		this.dtTrigger.next();
	}

	rerender(): void {
		this.dtElement.dtInstance.then((dtInstance) => {
			dtInstance.destroy();
			this.dtTrigger.next();
		});
	}

	archiveQuestion(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'question'
        	}, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
		       	this.rerender();
                this.ngOnInit();
        });
	}
	addModal(){
		let disposable = this.modalService.addDialog(QuestionModalComponent, {
			title:'Create Question',
			button:'Add',
			create:true
		}).subscribe((isConfirmed)=>{
			this.rerender();
			this.ngOnInit();
		});
	}

	editModal(id:any){
		let disposable = this.modalService.addDialog(QuestionModalComponent, {
			title:'Edit Question',
			button:'Update',
			edit:true,
			id:id
		}).subscribe((isConfirmed)=>{
			this.rerender();
			this.ngOnInit();
		});
	}

	

	


}
