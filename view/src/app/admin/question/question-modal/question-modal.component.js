var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { QuestionService } from '../../../services/question.service';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from './../../confirm-modal/confirm-modal.component';
var QuestionModalComponent = /** @class */ (function (_super) {
    __extends(QuestionModalComponent, _super);
    function QuestionModalComponent(dialogService, _fb, _ar, _question_service, _rt, _common_service, modalService) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._question_service = _question_service;
        _this._rt = _rt;
        _this._common_service = _common_service;
        _this.modalService = modalService;
        _this.bituin = false;
        _this.heart = false;
        _this.smile = false;
        _this.flower = false;
        _this.skull = false;
        _this.updateQuestionForm = _fb.group({
            'question': [null],
            'rating_value': [null],
        });
        $("label").click(function () {
            $(this).parent().find("label").css({ "background-color": "#D8D8D8" });
            $(this).css({ "background-color": "#7ED321" });
            $(this).nextAll().css({ "background-color": "#7ED321" });
        });
        //rating
        $(document).ready(function () {
            $('#stars li').on('mouseover', function () {
                var onStar = parseInt($(this).data('value'), 10);
                $(this).parent().children('li.star').each(function (e) {
                    if (e < onStar) {
                        $(this).addClass('hover');
                    }
                    else {
                        $(this).removeClass('hover');
                    }
                });
            }).on('mouseout', function () {
                $(this).parent().children('li.star').each(function (e) {
                    $(this).removeClass('hover');
                });
            });
            $('#stars li').on('click', function () {
                var onStar = parseInt($(this).data('value'), 10);
                var stars = $(this).parent().children('li.star');
                for (var i = 0; i < stars.length; i++) {
                    $(stars[i]).removeClass('selected');
                }
                for (i = 0; i < onStar; i++) {
                    $(stars[i]).addClass('selected');
                }
                var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
                var msg = "";
                if (ratingValue > 1) {
                    msg = "You choose  " + ratingValue + " stars.";
                }
                responseMessage(msg);
            });
        });
        function responseMessage(msg) {
            $('.success-box').fadeIn(200);
            $('.success-box div.text-message').html("<span>" + msg + "</span>");
        }
        return _this;
    }
    QuestionModalComponent.prototype.ngOnInit = function () {
        if (this.edit == true) {
            this.get_data();
        }
    };
    QuestionModalComponent.prototype.get_data = function () {
        var _this = this;
        var id = this.id;
        this._question_service.getQuestion(id).subscribe(function (data) {
            _this.setType(data);
        }, function (err) { return console.error(err); });
    };
    QuestionModalComponent.prototype.setType = function (type) {
        this.id = type.id;
        this.question = type.question;
        if (type.rating_class == 'flower') {
            this.flower = true;
        }
        else if (type.rating_class == 'bituin') {
            this.bituin = true;
        }
        else if (type.rating_class == 'smile') {
            this.smile = true;
        }
        else if (type.rating_class == 'skull') {
            this.skull = true;
        }
        else if (type.rating_class == 'heart') {
            this.heart = true;
        }
    };
    QuestionModalComponent.prototype.onSubmit = function () {
        var _this = this;
        var model = this.updateQuestionForm.value;
        var url = this.url;
        if (this.edit == true) {
            var id = this.id;
            this._question_service.updateQuestion(id, model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success!";
                _this.success_message = "Successfully updated";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
        if (this.create == true) {
            this._question_service.storeQuestion(model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success!";
                _this.success_message = "Successfully created";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    QuestionModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    QuestionModalComponent.prototype.archive = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: this.id,
            url: this.url
        }).subscribe(function (isConfirmed) {
            setTimeout(function () {
                _this.close();
                // this.rerender();
            }, 1000);
        });
    };
    QuestionModalComponent = __decorate([
        Component({
            selector: 'app-question-modal',
            templateUrl: './question-modal.component.html',
            styleUrls: ['./question-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            QuestionService,
            Router,
            CommonService,
            DialogService])
    ], QuestionModalComponent);
    return QuestionModalComponent;
}(DialogComponent));
export { QuestionModalComponent };
//# sourceMappingURL=question-modal.component.js.map