import 'rxjs/add/operator/catch'
import { Component, OnInit, AfterViewInit, NgZone, Inject } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response} from '@angular/http';
import { QuestionService } from '../../../services/question.service';
import { Configuration } from '../../../app.config';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from './../../confirm-modal/confirm-modal.component';
import {} from 'jasmine';


@Component({
	selector: 'app-question-modal',
	templateUrl: './question-modal.component.html',
	styleUrls: ['./question-modal.component.css']
	
})

export class QuestionModalComponent extends DialogComponent<null, boolean> {

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public poststore: any;
	public id: any;
	public question: any;
	
	edit:any;
	create:any;
	url:any;
	bituin = false;
	heart = false;
	smile = false;
	flower = false;
	skull = false;
	
	updateQuestionForm : FormGroup;
	constructor(
		dialogService: DialogService, 
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute, 
		private _question_service: QuestionService,  
		private _rt: Router,
		private _common_service: CommonService,
		private modalService: DialogService, 
		) {
		super(dialogService);
		this.updateQuestionForm = _fb.group({
			'question': 	[null],
			'rating_value': [null],
		});  

		$("label").click(function(){
	    $(this).parent().find("label").css({"background-color": "#D8D8D8"});
	    $(this).css({"background-color": "#7ED321"});
	    $(this).nextAll().css({"background-color": "#7ED321"});
	  	});

	  	//rating

	  	$(document).ready(function(){

	  		$('#stars li').on('mouseover', function(){
	  			var onStar = parseInt($(this).data('value'), 10);

	  			$(this).parent().children('li.star').each(function(e){
	  				if (e < onStar) {
	  					$(this).addClass('hover');
	  				}
	  				else {
	  					$(this).removeClass('hover');
	  				}
	  			});

	  		}).on('mouseout', function(){
	  			$(this).parent().children('li.star').each(function(e){
	  				$(this).removeClass('hover');
	  			});
	  		});


	  		$('#stars li').on('click', function(){
	  			var onStar = parseInt($(this).data('value'), 10);
	  			var stars = $(this).parent().children('li.star');

	  			for (var i = 0; i < stars.length; i++) {
	  				$(stars[i]).removeClass('selected');
	  			}

	  			for (i = 0; i < onStar; i++) {
	  				$(stars[i]).addClass('selected');
	  			}

	  			var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
	  			var msg = "";
	  			if (ratingValue > 1) {
	  				msg = "You choose  " + ratingValue + " stars.";
	  			} 
	  			responseMessage(msg);

	  		});


	  	});


	  	function responseMessage(msg) {
	  		$('.success-box').fadeIn(200);  
	  		$('.success-box div.text-message').html("<span>" + msg + "</span>");
	  	}
	}

	ngOnInit() {
		if(this.edit == true){
			this.get_data();
		}
	}

	get_data(){
		let id = this.id;
		this._question_service.getQuestion(id).subscribe(
			data => {
				this.setType(data);
			},
			err => console.error(err)
			);
	}

	public setType(type: any){
		this.id = type.id;
		this.question = type.question;    

		if (type.rating_class == 'flower') {
			this.flower = true;
		} else if (type.rating_class == 'bituin'){
			this.bituin = true;
		} else if (type.rating_class == 'smile'){
			this.smile = true;
		} else if (type.rating_class == 'skull'){
			this.skull = true;
		} else if (type.rating_class == 'heart'){
			this.heart = true;
		}
	}

	onSubmit() {
		let model = this.updateQuestionForm.value;

		let url = this.url;
		if(this.edit == true)
		{
			let id = this.id;
			this._question_service.updateQuestion(id, model)
			.subscribe(
				data => {
					this.poststore = Array.from(data);
					this.success_title = "Success!";
					this.success_message = "Successfully updated";
					setTimeout(() => {
						this.close();
					}, 1000);
				},
				err => this.catchError(err)
				);
		}
		
		if(this.create == true)
		{
			this._question_service.storeQuestion(model)
			.subscribe(
				data => {
					this.poststore = Array.from(data);
					this.success_title = "Success!";
					this.success_message = "Successfully created";
					setTimeout(() => {
						this.close();
					}, 1000);
				},
				err => this.catchError(err)
				);
		}
	}
	
	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	archive(){
		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
			title:'Archive Data',
			message:'Are you sure you want to archive this data?',
			action:'Delete',
			id:this.id,
			url:this.url
		}).subscribe((isConfirmed)=>{
			setTimeout(() => {
				this.close();
				// this.rerender();
			}, 1000);

		});
	}

}