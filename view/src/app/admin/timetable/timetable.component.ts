import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Component, NgZone, OnInit, Injectable, ElementRef } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { EventService } from '../../services/event.service';
import { EventType } from '../../model/event_type';
import { EventComponent } from '../event/event.component';
import { Event } from '../../model/event';
import * as moment from 'moment';
import { AuthUserService } from '../../services/auth-user.service';
import { Configuration } from '../../app.config';
import { Select2OptionData } from 'ng2-select2';

@Component({
  selector: 'app-timetable',
  templateUrl: './timetable.component.html',
  styleUrls: ['./timetable.component.css']
})

@Injectable()
export class TimetableComponent implements OnInit {
  public error_title: string;
  public error_message: string;
  public success_title;
  public success_message;
  calendarOptions: any = {};
  private headers: Headers;
  private eventsList: any;
  public id: any;
  public poststore: any;
  event = new EventType();
  eventForm: FormGroup;
  employee_id:any;
  user_id:any;
  calendar: any;
  today = moment();
  timetableFilter: any;
  elem: ElementRef;
  temp: any;
  event_type: any;
  events: any;
  bodyClasses:string = "skin-blue sidebar-mini";
  body = document.getElementsByTagName('body')[0];
  public event_current : any;
  event_value: Array<Select2OptionData>;
  event_filter: any;

  public options: any = {
    locale: { format: 'YYYY-MM-DD' },
    alwaysShowCalendars: false,
  };

  constructor(
    private _http: Http,
    private modalService: DialogService,
    _ngZone: NgZone, 
    dialogService: DialogService,
    private _event_service: EventService,
    private _auth_service: AuthUserService,
    private _fb: FormBuilder,
    _conf: Configuration
    ) {
    let app_config = _conf;
    let authToken = localStorage.getItem('id_token');
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Authorization', `Bearer ${authToken}`);

    this.body.classList.add("skin-blue");
    this.body.classList.add("sidebar-mini"); 

    this.id = this._event_service.getId();

    this.eventForm = _fb.group({
      'event_id': [''],
 
    });

    var self = this; 
    this.calendarOptions = {
      height: 'parent', 
      default: "30%",
      fixedWeekCount : false,
      defaultDate: this.today,
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      background: '#007c08',
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaFiveDay',
      },
      views: {
        agendaFiveDay: {
          type: 'agenda',
          duration: { days: 5 },
          buttonText: '5 day'
        }
      },
      events : function(start, end, timezone, callback) {
        $.ajax({
          url: app_config.ServerWithApiUrl + 'event/show',/*'http://localhost:8000/api/event/show', */
          dataType: 'json',
          data: {
            start: start.unix(),
            end: end.unix()
          },
          success: function(data) {
            this.events = data;
            callback(this.events);
          }
        });
      },
      eventDrop: function(event, delta, revertFunc) {
        alert(event.title + " was dropped on " + event.start.format());
        if (!confirm("Are you sure about this change?")) {
          self.validateUpdate();
          revertFunc();
        }
      },
      eventClick: function(event, jsEvent, view, date) {
        this.event_id = event.id;
        self.editModal(this.event_id);
      },
      dayClick: function(date, jsEvent, view) {
        self.addModal();
      },

    }
  } 

  ngAfterViewInit() {

  } 
  
  public catchError(error: any) {
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'The given data failed to pass validation.';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }

  }

  ngOnInit() {
    this.get_UserId();
    this.getEventType();
    this.validateUpdate();  
  }

  addModal() {
    let disposable = this.modalService.addDialog(EventComponent, {
      title:'Add Event',
      button:'Add',
      create:true,
      employee_id:this.employee_id
    }).subscribe((isConfirmed)=>{
      this.events()

    });
  }

  validateUpdate(){
    let event_model = this.eventForm.value;
    this._event_service.updateEvent(this.id, event_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "Event is successfully updated.";        
      },
      err => this.catchError(err)
      );
  }

  get_UserId(){
    this._auth_service.getUser().
    subscribe(
      data => {
        let user = data; // fetched 
        this.user_id = user.id;
        this.getEmployeeId();
      },
      err => console.error(err)
      );
  }

  getEmployeeId(){
    this._auth_service.getUserById(this.user_id).
    subscribe(
      data => {
        let user = data; // fetched 
        this.employee_id = user.employee_id;
      },
      err => console.error(err)
      );
  }

  getEventType(){

    let event_type = this._event_service.getEventTypeLists().
    subscribe(
      data => {
        this.event_type = data;
        let id = 0;
        let text = 'All Event';

        this.event_type.unshift({id,text});
        this.event_current = this.event_value;
      },
      err => console.error(err)
      );
  }

  changedEventType(data: any){
    this.event_current = data.value;
    this.filterEvent();
  }

  filterEvent(){
    this.eventForm.value.event_id = this.event_current;
    let model = this.eventForm.value;
    this._event_service.getEventName(model).
    subscribe(
      data => {
        this.event_filter = Array.from(data);
        console.log(this.event_filter);
      },
      err => console.error(err)
      );
  }

  editModal(id: any) {
    let disposable = this.modalService.addDialog(EventComponent, {
      title:'Edit Event',
      button:'Update',
      edit:true,
      event_id:id,
      employee_id:this.employee_id
    }).subscribe((isConfirmed)=>{

    });
  }

  

}
