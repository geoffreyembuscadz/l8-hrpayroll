var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { FormBuilder } from '@angular/forms';
import { Component, NgZone, Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { DialogService } from "ng2-bootstrap-modal";
import { EventService } from '../../services/event.service';
import { EventType } from '../../model/event_type';
import { EventComponent } from '../event/event.component';
import * as moment from 'moment';
import { AuthUserService } from '../../services/auth-user.service';
import { Configuration } from '../../app.config';
var TimetableComponent = /** @class */ (function () {
    function TimetableComponent(_http, modalService, _ngZone, dialogService, _event_service, _auth_service, _fb, _conf) {
        this._http = _http;
        this.modalService = modalService;
        this._event_service = _event_service;
        this._auth_service = _auth_service;
        this._fb = _fb;
        this.calendarOptions = {};
        this.event = new EventType();
        this.today = moment();
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.options = {
            locale: { format: 'YYYY-MM-DD' },
            alwaysShowCalendars: false,
        };
        var app_config = _conf;
        var authToken = localStorage.getItem('id_token');
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.id = this._event_service.getId();
        this.eventForm = _fb.group({
            'event_id': [''],
        });
        var self = this;
        this.calendarOptions = {
            height: 'parent',
            default: "30%",
            fixedWeekCount: false,
            defaultDate: this.today,
            editable: true,
            eventLimit: true,
            background: '#007c08',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaFiveDay',
            },
            views: {
                agendaFiveDay: {
                    type: 'agenda',
                    duration: { days: 5 },
                    buttonText: '5 day'
                }
            },
            events: function (start, end, timezone, callback) {
                $.ajax({
                    url: app_config.ServerWithApiUrl + 'event/show',
                    dataType: 'json',
                    data: {
                        start: start.unix(),
                        end: end.unix()
                    },
                    success: function (data) {
                        this.events = data;
                        callback(this.events);
                    }
                });
            },
            eventDrop: function (event, delta, revertFunc) {
                alert(event.title + " was dropped on " + event.start.format());
                if (!confirm("Are you sure about this change?")) {
                    self.validateUpdate();
                    revertFunc();
                }
            },
            eventClick: function (event, jsEvent, view, date) {
                this.event_id = event.id;
                self.editModal(this.event_id);
            },
            dayClick: function (date, jsEvent, view) {
                self.addModal();
            },
        };
    }
    TimetableComponent.prototype.ngAfterViewInit = function () {
    };
    TimetableComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    TimetableComponent.prototype.ngOnInit = function () {
        this.get_UserId();
        this.getEventType();
        this.validateUpdate();
    };
    TimetableComponent.prototype.addModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(EventComponent, {
            title: 'Add Event',
            button: 'Add',
            create: true,
            employee_id: this.employee_id
        }).subscribe(function (isConfirmed) {
            _this.events();
        });
    };
    TimetableComponent.prototype.validateUpdate = function () {
        var _this = this;
        var event_model = this.eventForm.value;
        this._event_service.updateEvent(this.id, event_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "Event is successfully updated.";
        }, function (err) { return _this.catchError(err); });
    };
    TimetableComponent.prototype.get_UserId = function () {
        var _this = this;
        this._auth_service.getUser().
            subscribe(function (data) {
            var user = data; // fetched 
            _this.user_id = user.id;
            _this.getEmployeeId();
        }, function (err) { return console.error(err); });
    };
    TimetableComponent.prototype.getEmployeeId = function () {
        var _this = this;
        this._auth_service.getUserById(this.user_id).
            subscribe(function (data) {
            var user = data; // fetched 
            _this.employee_id = user.employee_id;
        }, function (err) { return console.error(err); });
    };
    TimetableComponent.prototype.getEventType = function () {
        var _this = this;
        var event_type = this._event_service.getEventTypeLists().
            subscribe(function (data) {
            _this.event_type = data;
            var id = 0;
            var text = 'All Event';
            _this.event_type.unshift({ id: id, text: text });
            _this.event_current = _this.event_value;
        }, function (err) { return console.error(err); });
    };
    TimetableComponent.prototype.changedEventType = function (data) {
        this.event_current = data.value;
        this.filterEvent();
    };
    TimetableComponent.prototype.filterEvent = function () {
        var _this = this;
        this.eventForm.value.event_id = this.event_current;
        var model = this.eventForm.value;
        this._event_service.getEventName(model).
            subscribe(function (data) {
            _this.event_filter = Array.from(data);
            console.log(_this.event_filter);
        }, function (err) { return console.error(err); });
    };
    TimetableComponent.prototype.editModal = function (id) {
        var disposable = this.modalService.addDialog(EventComponent, {
            title: 'Edit Event',
            button: 'Update',
            edit: true,
            event_id: id,
            employee_id: this.employee_id
        }).subscribe(function (isConfirmed) {
        });
    };
    TimetableComponent = __decorate([
        Component({
            selector: 'app-timetable',
            templateUrl: './timetable.component.html',
            styleUrls: ['./timetable.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Http,
            DialogService,
            NgZone,
            DialogService,
            EventService,
            AuthUserService,
            FormBuilder,
            Configuration])
    ], TimetableComponent);
    return TimetableComponent;
}());
export { TimetableComponent };
//# sourceMappingURL=timetable.component.js.map