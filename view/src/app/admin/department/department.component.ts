import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../services/auth-user.service';
import { Select2OptionData } from 'ng2-select2';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { DepartmentService } from '../../services/department.service';
import { TypeModalComponent } from '../type-modal/type-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';


@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})

@Injectable()
export class DepartmentComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	dtOptions: any = {};

	department:any;

  	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

  constructor(
  		private modalService: DialogService,
		private _conf: Configuration,
		private daterangepickerOptions: DaterangepickerConfig,
   		private _fb: FormBuilder,
   		private _auth_service: AuthUserService,
   		private _dp_service: DepartmentService,
   	) {

  		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

   	}

  ngOnInit() {
  		this.dateOption();
  		this.data();
	}

	dateOption(){
    	this.daterangepickerOptions.settings = {
        	singleDatePicker: false
		};
    }

    data(){

    	this._dp_service.getDepartments().
	      	subscribe(
	        data => {
	          this.department= Array.from(data);
	          this.rerender();
	        },
	        err => console.error(err)
	      );

	}

	createDepartment() {

		let disposable = this.modalService.addDialog(TypeModalComponent, {
            title:'Create Department',
            button:'Add',
		    url:'department',
		    create:true
        	}).subscribe((isConfirmed)=>{
             
                this.ngOnInit();

            });
	}

	editDepartment(id:any) {
		let disposable = this.modalService.addDialog(TypeModalComponent, {
            title:'Update Department',
            button:'Update',
		    edit:true,
		    url:'department',
		    id:id
        	}).subscribe((isConfirmed)=>{
             
                this.ngOnInit();
        });
	}

	archive(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'department'
        	}, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
		     
                this.ngOnInit();
        });
	}

  	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

}