var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { FormBuilder } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../services/auth-user.service';
import { DepartmentService } from '../../services/department.service';
import { TypeModalComponent } from '../type-modal/type-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
var DepartmentComponent = /** @class */ (function () {
    function DepartmentComponent(modalService, _conf, daterangepickerOptions, _fb, _auth_service, _dp_service) {
        this.modalService = modalService;
        this._conf = _conf;
        this.daterangepickerOptions = daterangepickerOptions;
        this._fb = _fb;
        this._auth_service = _auth_service;
        this._dp_service = _dp_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    DepartmentComponent.prototype.ngOnInit = function () {
        this.dateOption();
        this.data();
    };
    DepartmentComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    DepartmentComponent.prototype.data = function () {
        var _this = this;
        this._dp_service.getDepartments().
            subscribe(function (data) {
            _this.department = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    DepartmentComponent.prototype.createDepartment = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Create Department',
            button: 'Add',
            url: 'department',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.ngOnInit();
        });
    };
    DepartmentComponent.prototype.editDepartment = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Update Department',
            button: 'Update',
            edit: true,
            url: 'department',
            id: id
        }).subscribe(function (isConfirmed) {
            _this.ngOnInit();
        });
    };
    DepartmentComponent.prototype.archive = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'department'
        }, { backdropColor: 'rgba(238,238,238)' }).subscribe(function (isConfirmed) {
            _this.ngOnInit();
        });
    };
    DepartmentComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    DepartmentComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    DepartmentComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], DepartmentComponent.prototype, "dtElement", void 0);
    DepartmentComponent = __decorate([
        Component({
            selector: 'app-department',
            templateUrl: './department.component.html',
            styleUrls: ['./department.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            DaterangepickerConfig,
            FormBuilder,
            AuthUserService,
            DepartmentService])
    ], DepartmentComponent);
    return DepartmentComponent;
}());
export { DepartmentComponent };
//# sourceMappingURL=department.component.js.map