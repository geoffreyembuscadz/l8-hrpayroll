import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { EventTypeService } from '../../../services/event-type.service';
import { CommonService } from '../../../services/common.service';


@Component({
	selector: 'app-event-type-modal',
	templateUrl: './event-type-modal.component.html',
	styleUrls: ['./event-type-modal.component.css']
})
export class EventTypeModalComponent extends DialogComponent<null, boolean> {

    public success_title;
    public success_message;
    public error_title: string
    public error_message: string;

    Form : FormGroup;

    id:any;
    name:any;
    description:any;
    color:any;
    postore:any;

    edit = false;
    create = false;

	constructor(
	  dialogService: DialogService, 
	  private _fb: FormBuilder, 
	  private event_service: EventTypeService,  
	  private _common_service: CommonService,
	  private modalService: DialogService) {
		super(dialogService);

	  this.Form = _fb.group({
	     'name':        [null, [Validators.required]],
	     'description':	[null, [Validators.required]],
	     'color':	[null]
	  });  
	}

  	ngOnInit() {
  		if (this.edit) {
			this.getdata();
  		}
	}


	getdata(){
		let id = this.id;
    	this.event_service.getEventTypeById(id).
	      	subscribe(
	        data => {
	          this.assign(data);
	        },
	        err => console.error(err)
	     );
	}

	assign(d){
		this.name = d.name;
		this.description = d.description;
		this.color = d.color;
	}

	onSubmit(){

		if (this.edit) {

			this.Form.value.color = this.color;
			let model = this.Form.value;
			let id = this.id;

	    	this.event_service.updateEventType(id,model).
		      	subscribe(
		        data => {
		          this.postore = data;
		          this.success_title = "Success!";
	            	this.success_message = "Successfully updated";

	            	setTimeout(() => {
	            	this.close();
	            	}, 1000);

		        },
		        err => console.error(err)
		     );
		}

		if (this.create) {

			this.Form.value.color = this.color;
			let model = this.Form.value;

	    	this.event_service.createEventType(model).
		      	subscribe(
		        data => {
		          this.postore = data;
		          this.success_title = "Success!";
	            	this.success_message = "Successfully created";

	            	setTimeout(() => {
	            	this.close();
	            	}, 1000);

		        },
		        err => console.error(err)
		     );
		}
	}

}