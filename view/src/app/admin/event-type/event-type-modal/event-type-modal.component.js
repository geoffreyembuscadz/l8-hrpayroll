var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { EventTypeService } from '../../../services/event-type.service';
import { CommonService } from '../../../services/common.service';
var EventTypeModalComponent = /** @class */ (function (_super) {
    __extends(EventTypeModalComponent, _super);
    function EventTypeModalComponent(dialogService, _fb, event_service, _common_service, modalService) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this.event_service = event_service;
        _this._common_service = _common_service;
        _this.modalService = modalService;
        _this.edit = false;
        _this.create = false;
        _this.Form = _fb.group({
            'name': [null, [Validators.required]],
            'description': [null, [Validators.required]],
            'color': [null]
        });
        return _this;
    }
    EventTypeModalComponent.prototype.ngOnInit = function () {
        if (this.edit) {
            this.getdata();
        }
    };
    EventTypeModalComponent.prototype.getdata = function () {
        var _this = this;
        var id = this.id;
        this.event_service.getEventTypeById(id).
            subscribe(function (data) {
            _this.assign(data);
        }, function (err) { return console.error(err); });
    };
    EventTypeModalComponent.prototype.assign = function (d) {
        this.name = d.name;
        this.description = d.description;
        this.color = d.color;
    };
    EventTypeModalComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.edit) {
            this.Form.value.color = this.color;
            var model = this.Form.value;
            var id = this.id;
            this.event_service.updateEventType(id, model).
                subscribe(function (data) {
                _this.postore = data;
                _this.success_title = "Success!";
                _this.success_message = "Successfully updated";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return console.error(err); });
        }
        if (this.create) {
            this.Form.value.color = this.color;
            var model = this.Form.value;
            this.event_service.createEventType(model).
                subscribe(function (data) {
                _this.postore = data;
                _this.success_title = "Success!";
                _this.success_message = "Successfully created";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return console.error(err); });
        }
    };
    EventTypeModalComponent = __decorate([
        Component({
            selector: 'app-event-type-modal',
            templateUrl: './event-type-modal.component.html',
            styleUrls: ['./event-type-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            EventTypeService,
            CommonService,
            DialogService])
    ], EventTypeModalComponent);
    return EventTypeModalComponent;
}(DialogComponent));
export { EventTypeModalComponent };
//# sourceMappingURL=event-type-modal.component.js.map