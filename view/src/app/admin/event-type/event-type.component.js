var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { EventTypeModalComponent } from './event-type-modal/event-type-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { EventTypeService } from '../../services/event-type.service';
var EventTypeComponent = /** @class */ (function () {
    function EventTypeComponent(dialogService, modalService, event_service) {
        this.modalService = modalService;
        this.event_service = event_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    EventTypeComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    EventTypeComponent.prototype.ngOnInit = function () {
        this.getData();
    };
    EventTypeComponent.prototype.getData = function () {
        var _this = this;
        this.event_service.getEventType().
            subscribe(function (data) {
            _this.data = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    EventTypeComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    EventTypeComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    EventTypeComponent.prototype.archive = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'event_type'
        }).subscribe(function (isConfirmed) {
            _this.getData();
        });
    };
    EventTypeComponent.prototype.edit = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(EventTypeModalComponent, {
            title: 'Update Event Type',
            button: 'Update',
            edit: true,
            id: id
        }).subscribe(function (isConfirmed) {
            _this.getData();
        });
    };
    EventTypeComponent.prototype.add = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(EventTypeModalComponent, {
            title: 'Create Event Type',
            button: 'Create',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.getData();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], EventTypeComponent.prototype, "dtElement", void 0);
    EventTypeComponent = __decorate([
        Component({
            selector: 'app-event-type',
            templateUrl: './event-type.component.html',
            styleUrls: ['./event-type.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            DialogService,
            EventTypeService])
    ], EventTypeComponent);
    return EventTypeComponent;
}());
export { EventTypeComponent };
//# sourceMappingURL=event-type.component.js.map