import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { EventTypeModalComponent } from './event-type-modal/event-type-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { EventTypeService } from '../../services/event-type.service';

@Component({
  selector: 'app-event-type',
  templateUrl: './event-type.component.html',
  styleUrls: ['./event-type.component.css']
})

@Injectable()
export class EventTypeComponent implements OnInit, AfterViewInit {
	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};

	data:any;

	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	constructor(
  		dialogService: DialogService, 
  		private modalService: DialogService,
  		private event_service: EventTypeService
  		){
		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");


	}

	ngAfterViewInit(){
		this.dtTrigger.next();
	}

	ngOnInit() {
		this.getData();
	}
	getData(){

    	this.event_service.getEventType().
	      	subscribe(
	        data => {
	          this.data= Array.from(data);
	          this.rerender();
	        },
	        err => console.error(err)
	     );
	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    archive(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'event_type'
        	}).subscribe((isConfirmed)=>{
		       	this.getData();
        });
	}

	edit(id) {
		let disposable = this.modalService.addDialog(EventTypeModalComponent, {
            title:'Update Event Type',
            button:'Update',
		    edit:true,
		    id:id
        	}).subscribe((isConfirmed)=>{
                this.getData();
        });
	}

	add() {
		let disposable = this.modalService.addDialog(EventTypeModalComponent, {
            title:'Create Event Type',
            button:'Create',
		    create:true
        	}).subscribe((isConfirmed)=>{
                this.getData();
        });
	}

}
