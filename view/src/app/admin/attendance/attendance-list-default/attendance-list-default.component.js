var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, TemplateRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DialogService } from "ng2-bootstrap-modal";
import { AttendanceImportModalComponent } from '../../attendance/attendance-import-modal/attendance-import-modal.component';
import { Router } from '@angular/router';
import { Configuration } from '../../../app.config';
import { CommonService } from '../../../services/common.service';
import { AttendanceService } from '../../../services/attendance.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { AttendanceModalComponent } from '../../attendance/attendance-modal/attendance-modal.component';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { DownloadFileModalComponent } from '../../download-file-modal/download-file-modal.component';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { CertificateOfAttendanceModalComponent } from '../../certificate-of-attendance/certificate-of-attendance-modal/certificate-of-attendance-modal.component';
import { AuthUserService } from '../../../services/auth-user.service';
var AttendanceListDefaultComponent = /** @class */ (function () {
    function AttendanceListDefaultComponent(modalService, _fb, _config, _common_service, _attendservice, _auth_service, _rt, daterangepickerOptions) {
        this.modalService = modalService;
        this._fb = _fb;
        this._config = _config;
        this._common_service = _common_service;
        this._attendservice = _attendservice;
        this._auth_service = _auth_service;
        this._rt = _rt;
        this.daterangepickerOptions = daterangepickerOptions;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.byStatus = false;
        this.byDate = false;
        this.byCompany = false;
        this.byEmployee = false;
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filterForm = _fb.group({
            'type_id': [null],
            'start_date': [null],
            'end_date': [null],
            'company_id': [null],
            'employee_id': [null]
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.dtOptions = {
            lengthMenu: [[50, 100, 150, -1], [50, 100, 150, "All"]]
        };
    }
    AttendanceListDefaultComponent.prototype.ngOnInit = function () {
        this.dateOption();
        this.data();
        this.getEmployee();
    };
    AttendanceListDefaultComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    AttendanceListDefaultComponent.prototype.data = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._attendservice.getAttendDefault(model).
            subscribe(function (data) {
            _this.att = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    AttendanceListDefaultComponent.prototype.getType = function () {
        var _this = this;
        this._common_service.getAttendanceType().
            subscribe(function (data) {
            _this.type = Array.from(data); // fetched record
            _this.type_value = [];
            _this.options = {
                multiple: true
            };
            _this.type_current = _this.type_value;
        }, function (err) { return console.error(err); });
    };
    AttendanceListDefaultComponent.prototype.changedType = function (data) {
        this.type_current = data.value;
        if (this.type_current == 0) {
            this.filterForm.value.type_id = null;
        }
        else {
            this.filterForm.value.type_id = this.type_current;
        }
        this.data();
    };
    AttendanceListDefaultComponent.prototype.getCompany = function () {
        var _this = this;
        this._common_service.getCompany()
            .subscribe(function (data) {
            _this.company = Array.from(data);
            _this.company_value = [];
            _this.options = {
                multiple: true
            };
            _this.company_current = _this.company_value;
        }, function (err) { return console.error(err); });
    };
    AttendanceListDefaultComponent.prototype.changedCompany = function (data) {
        this.company_current = data.value;
        if (this.company_current == 0) {
            this.filterForm.value.company_id = null;
        }
        else {
            this.filterForm.value.company_id = this.company_current;
        }
        this.data();
    };
    AttendanceListDefaultComponent.prototype.getEmployee = function () {
        var _this = this;
        this._common_service.getEmployee().subscribe(function (data) {
            _this.emp = Array.from(data); // fetched record
            _this.employee = _this.emp;
            _this.employee_value = [];
            _this.options = {
                multiple: true
            };
            _this.employee_current = _this.employee_value;
        }, function (err) { return console.error(err); });
    };
    AttendanceListDefaultComponent.prototype.changedEmployee = function (data) {
        this.employee_current = data.value;
        if (this.employee_current == 0) {
            this.filterForm.value.employee_id = null;
        }
        else {
            this.filterForm.value.employee_id = this.employee_current;
        }
        this.data();
    };
    AttendanceListDefaultComponent.prototype.choiceFilter = function (id) {
        if (id == 1) {
            this.getType();
            this.getCompany();
            this.getEmployee();
            this.byStatus = true;
            this.byDate = true;
            this.byCompany = true;
            this.byEmployee = true;
        }
        else if (id == 2) {
            if (this.byStatus == false) {
                this.getType();
                this.byStatus = true;
            }
            else {
                this.byStatus = false;
                this.filterForm.value.status_id = null;
            }
        }
        else if (id == 3) {
            if (this.byDate == false) {
                this.byDate = true;
            }
            else {
                this.byDate = false;
                this.filterForm.value.start_date = null;
                this.filterForm.value.end_date = null;
            }
        }
        else if (id == 4) {
            if (this.byCompany == false) {
                this.getCompany();
                this.byCompany = true;
            }
            else {
                this.byCompany = false;
                this.filterForm.value.company_id = null;
            }
        }
        else if (id == 5) {
            if (this.byEmployee == false) {
                this.getEmployee();
                this.byEmployee = true;
            }
            else {
                this.byEmployee = false;
                this.filterForm.value.employee_id = null;
            }
        }
        else {
            this.byStatus = false;
            this.byDate = false;
            this.byCompany = false;
            this.byEmployee = false;
            this.filterForm.value.type_id = null;
            this.filterForm.value.company_id = null;
            this.filterForm.value.start_date = null;
            this.filterForm.value.end_date = null;
            this.filterForm.value.employee_id = null;
            this.data();
        }
    };
    AttendanceListDefaultComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.data();
    };
    AttendanceListDefaultComponent.prototype.editAttendance = function (id, employee_id) {
        var _this = this;
        var v = [];
        v.push(employee_id);
        var employee = v;
        this.modalService.addDialog(AttendanceModalComponent, {
            title: 'Edit Attendance',
            button: 'Update',
            edit: true,
            single: true,
            att_id: id,
            emp: this.emp,
            user_id: this.user_id,
            employee_id: employee,
            default: true
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    AttendanceListDefaultComponent.prototype.importModal = function () {
        var _this = this;
        this.modalService.addDialog(AttendanceImportModalComponent, {
            title: 'Upload File',
            bio_val: false
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    AttendanceListDefaultComponent.prototype.bioManualUpload = function () {
        var _this = this;
        this.modalService.addDialog(AttendanceImportModalComponent, {
            title: 'Choose Biometics Device',
            bio_val: true
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    AttendanceListDefaultComponent.prototype.exampleValidator = function (control) {
        if (control.value === '') {
            return { example: true };
        }
        return null;
    };
    AttendanceListDefaultComponent.prototype.asyncExampleValidator = function (control) {
        var promise = new Promise(function (resolve, reject) {
            setTimeout(function () {
                if (control.value === '') {
                    resolve({ 'invalid': true });
                }
                else {
                    resolve(null);
                }
            }, 1500);
        });
        return promise;
    };
    AttendanceListDefaultComponent.prototype.onSubmit = function () {
        console.log('FORM Object:', this.attendanceForm);
        console.log('FORM Values:', this.attendanceForm.value);
    };
    // addData(form: NgForm) {
    // 	(<FormArray>this.attendanceForm.controls['hobbies']).push(new FormControl('', Validators.required, this.asyncExampleValidator));
    // }
    AttendanceListDefaultComponent.prototype.downloadAttendanceForm = function () {
        var _this = this;
        this.modalService.addDialog(DownloadFileModalComponent, {
            title: 'Choose Employee'
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    AttendanceListDefaultComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    AttendanceListDefaultComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    AttendanceListDefaultComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    AttendanceListDefaultComponent.prototype.archiveAttendance = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'attendance'
        }, { backdropColor: 'rgba(238,238,238)' }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    AttendanceListDefaultComponent.prototype.createCOA = function (date, type, id) {
        var _this = this;
        var v = [];
        v.push(id);
        var employee = v;
        var dates = moment(date).format("YYYY-MM-DD");
        var disposable = this.modalService.addDialog(CertificateOfAttendanceModalComponent, {
            title: 'Create Certificate of Attendance',
            single: true,
            button: 'Add',
            create: true,
            employee_id: employee,
            type_id: type,
            date_in: dates,
            coaValidation: true,
            emp: this.emp
        }).subscribe(function (message) {
            if (message == true) {
                _this._rt.navigateByUrl('admin/certificate-of-attendance-list');
            }
            else {
                _this.dateOption();
                _this.data();
            }
        });
    };
    __decorate([
        ViewChild("attendanceImport"),
        __metadata("design:type", TemplateRef)
    ], AttendanceListDefaultComponent.prototype, "engineModal", void 0);
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], AttendanceListDefaultComponent.prototype, "dtElement", void 0);
    AttendanceListDefaultComponent = __decorate([
        Component({
            selector: 'app-attendance-list-default',
            templateUrl: './attendance-list-default.component.html',
            styleUrls: ['./attendance-list-default.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            Configuration,
            CommonService,
            AttendanceService,
            AuthUserService,
            Router,
            DaterangepickerConfig])
    ], AttendanceListDefaultComponent);
    return AttendanceListDefaultComponent;
}());
export { AttendanceListDefaultComponent };
//# sourceMappingURL=attendance-list-default.component.js.map