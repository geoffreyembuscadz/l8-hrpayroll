import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild, TemplateRef } from '@angular/core';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from "rxjs/Rx";
import { AttendanceImportModalComponent } from '../../attendance/attendance-import-modal/attendance-import-modal.component';
import { Router } from '@angular/router';
import { Configuration } from '../../../app.config';
import { CommonService } from '../../../services/common.service';
import { AttendanceService } from '../../../services/attendance.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { AttendanceModalComponent } from '../../attendance/attendance-modal/attendance-modal.component';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { DownloadFileModalComponent } from '../../download-file-modal/download-file-modal.component';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Select2OptionData } from 'ng2-select2';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { CertificateOfAttendanceModalComponent } from '../../certificate-of-attendance/certificate-of-attendance-modal/certificate-of-attendance-modal.component';
import { AuthUserService } from '../../../services/auth-user.service';


@Component({
  selector: 'app-attendance-list-default',
  templateUrl: './attendance-list-default.component.html',
  styleUrls: ['./attendance-list-default.component.css']
})
export class AttendanceListDefaultComponent implements OnInit, AfterViewInit {

  	@ViewChild("attendanceImport") private engineModal: TemplateRef<any>;
  	
  	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	private attendance_form_link : string;
  	dtOptions: any = {};
	attendanceForm: FormGroup;
	att_id:any;
	att:any;

	byStatus=false;
	byDate=false;
	byCompany=false;
	byEmployee=false;

	public company: any;
	public company_current : any;
	company_value: Array<Select2OptionData>;
	value:any;

	public mainInput = {
        start: moment().subtract(12, 'month'),
        end: moment().subtract(11, 'month')
    }

   	public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public employee_current: any;
	public options: Select2Options;
	emp:any;

	public type_value: Array<Select2OptionData>;
	public type_current: any;
	public type : any;
	employee_id:any;

    filterForm:any;

    user_id:any;
	supervisor_id:any;
	role_id:any;

	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];
 	
	constructor(
		private modalService: DialogService, 
		private _fb: FormBuilder, 
		private _config: Configuration,
		private _common_service: CommonService,
		private _attendservice: AttendanceService,
		private _auth_service: AuthUserService,
		private _rt: Router,
   		private daterangepickerOptions: DaterangepickerConfig
		){

		this.filterForm = _fb.group({
    	'type_id': 			[null],
		'start_date': 		[null],
		'end_date': 		[null],
		'company_id': 		[null],
		'employee_id': 		[null]
   		});

		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

		this.dtOptions = {
	      lengthMenu: [ [50, 100, 150, -1], [50, 100, 150, "All"] ]
	    };
	}

	ngOnInit() {
		this.dateOption();
		this.data();
		this.getEmployee();
	}

	dateOption(){
    	this.daterangepickerOptions.settings = {
        	singleDatePicker: false
		};
    }

	data(){

		let model = this.filterForm.value;
		this._attendservice.getAttendDefault(model).
		  	subscribe(
		    data => {
		      this.att= Array.from(data);
		      this.rerender();
		    },
		    err => console.error(err)
		);

	}


	getType(){
		this._common_service.getAttendanceType().
		subscribe(
			data => {
				this.type = Array.from(data); // fetched record
				this.type_value = [];
				this.options = {
					multiple: true
				}
				this.type_current = this.type_value;
			},
			err => console.error(err)
		);
	}
	changedType(data: any) {
		this.type_current = data.value;

		if(this.type_current == 0){
			this.filterForm.value.type_id = null;
		}
		else{
			this.filterForm.value.type_id = this.type_current;
		}
			this.data();
	}

	getCompany(){
	      this._common_service.getCompany()
	      .subscribe(
	        data => {
	        this.company = Array.from(data);
	        this.company_value = [];
	        this.options = {
				multiple: true
			}
	        this.company_current = this.company_value;
	        
	        },
	        err => console.error(err)
	    );
	}
	changedCompany(data: any) {

		this.company_current = data.value;

		if(this.company_current == 0){
			this.filterForm.value.company_id = null;
		}
		else{
			this.filterForm.value.company_id = this.company_current;
		}
			this.data();

	} 

    getEmployee(){

		this._common_service.getEmployee().subscribe(
			data => {
				this.emp = Array.from(data); // fetched record
				this.employee = this.emp;
				this.employee_value = [];
				this.options = {
					multiple: true
				}
				this.employee_current = this.employee_value;
			},
			err => console.error(err)
		);
	}
	changedEmployee(data: any) {
		this.employee_current = data.value;

		if(this.employee_current == 0){
			this.filterForm.value.employee_id = null;
		}
		else{
			this.filterForm.value.employee_id = this.employee_current;
		}
			this.data();
	}

	choiceFilter(id){

		if(id==1){
			this.getType();
			this.getCompany();
			this.getEmployee();
			this.byStatus=true;
			this.byDate=true;
			this.byCompany=true;
			this.byEmployee=true;
		}
		else if(id==2){
			if (this.byStatus==false) { 
				this.getType();
				this.byStatus=true;
			} else {
				this.byStatus=false;
				this.filterForm.value.status_id = null;
			}
		}
		else if(id==3){
			if (this.byDate==false) { 
				this.byDate=true;
			} else {
				this.byDate=false;
				this.filterForm.value.start_date = null;
        		this.filterForm.value.end_date = null;
			}
		}
		else if(id==4){
			if (this.byCompany==false) { 
				this.getCompany();
				this.byCompany=true;
			} else {
				this.byCompany=false;
				this.filterForm.value.company_id = null;
			}
		}
		else if(id==5){
			if (this.byEmployee==false) { 
				this.getEmployee();
				this.byEmployee=true;
			} else {
				this.byEmployee=false;
				this.filterForm.value.employee_id = null;
			}
		}
		else{
			this.byStatus = false;
			this.byDate = false;
			this.byCompany=false;
			this.byEmployee=false;

			this.filterForm.value.type_id = null;
        	this.filterForm.value.company_id = null;
        	this.filterForm.value.start_date = null;
        	this.filterForm.value.end_date = null;
        	this.filterForm.value.employee_id = null;
        	this.data();
		}

	}

	filterDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;

        let start_date = moment(dateInput.start).format("YYYY-MM-DD");
        let end_date = moment(dateInput.end).format("YYYY-MM-DD");

        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;

        this.data();

    }


	editAttendance(id,employee_id){

		let v = [];
		v.push(employee_id);
		let employee = v;
		
		this.modalService.addDialog(AttendanceModalComponent, {
		    title:'Edit Attendance',
		    button:'Update',
            edit:true,
            single:true,
            att_id:id,
            emp:this.emp,
            user_id:this.user_id,
            employee_id:employee,
            default:true
			}).subscribe((isConfirmed)=>{
		        this.dateOption();
                this.data();
		    });
	}

	importModal(): void {

		this.modalService.addDialog(AttendanceImportModalComponent, {
            title:'Upload File',
            bio_val:false
        	}).subscribe((isConfirmed)=>{
                this.dateOption();
                this.data();
        });

	}

	bioManualUpload(): void {

		this.modalService.addDialog(AttendanceImportModalComponent, {
            title:'Choose Biometics Device',
            bio_val:true
        	}).subscribe((isConfirmed)=>{
                this.dateOption();
                this.data();
        });

	}
	

	exampleValidator(control: FormControl): {[s: string]: boolean} {
		if (control.value === '') {
			return {example: true};
		}

		return null;
	}
	asyncExampleValidator(control: FormControl): Promise<any> | Observable<any> {
		const promise = new Promise<any>(
		(resolve, reject) => {
			setTimeout(() => {
				if (control.value === '') {
					resolve({'invalid': true});
				} else {
					resolve(null);
				}
			}, 1500);
		});
		
		return promise;
	}

	onSubmit() {
		console.log('FORM Object:', this.attendanceForm);
		console.log('FORM Values:', this.attendanceForm.value);

	}



	// addData(form: NgForm) {
	// 	(<FormArray>this.attendanceForm.controls['hobbies']).push(new FormControl('', Validators.required, this.asyncExampleValidator));
	// }

	downloadAttendanceForm(){

		this.modalService.addDialog(DownloadFileModalComponent, {
            title:'Choose Employee'
        	}).subscribe((isConfirmed)=>{

                this.dateOption();
                this.data();
        });
	}


    ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    archiveAttendance(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'attendance'
        	}, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
                this.dateOption();
                this.data();
        });
	}

	createCOA(date,type,id){

    	let v = [];
		v.push(id);
		let employee = v;

    	let dates = moment(date).format("YYYY-MM-DD");

		let disposable = this.modalService.addDialog(CertificateOfAttendanceModalComponent, {
            title:'Create Certificate of Attendance',
            single:true,
            button:'Add',
            create:true,
            employee_id:employee,
            type_id:type,
            date_in:dates,
            coaValidation:true,
            emp:this.emp
        	}).subscribe((message)=>{
        		if (message == true) {
                	this._rt.navigateByUrl('admin/certificate-of-attendance-list');
       			}else{
	
	                this.dateOption();
                	this.data();
       			}
        });
	}
}
