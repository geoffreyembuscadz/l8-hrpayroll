var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { FormBuilder } from '@angular/forms';
import { Component, Injectable, NgZone, EventEmitter } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { NgUploaderOptions } from 'ngx-uploader'; // ngx-uploader.com
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { Configuration } from '../../../app.config';
import { CommonService } from '../../../services/common.service';
import { AttendanceService } from '../../../services/attendance.service';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';
var AttendanceImportModalComponent = /** @class */ (function (_super) {
    __extends(AttendanceImportModalComponent, _super);
    function AttendanceImportModalComponent(dialogService, modalService, _config, _fb, _route, _zone, _atteservice, http, _common_service, _rt) {
        var _this = _super.call(this, dialogService) || this;
        _this.modalService = modalService;
        _this._config = _config;
        _this._fb = _fb;
        _this._route = _route;
        _this._zone = _zone;
        _this._atteservice = _atteservice;
        _this.http = http;
        _this._common_service = _common_service;
        _this._rt = _rt;
        _this.route_upload = 'upload?type=attendance';
        _this.csvData = [];
        _this.showTable = true;
        _this.fromDown = true;
        _this.fromBio = false;
        _this.fromBio2 = false;
        _this.fromBio3 = false;
        _this.loader = false;
        _this.validate = false;
        // uploader ngzone
        _this.options = new NgUploaderOptions({
            url: _this._config.ServerWithApiUrl + _this.route_upload,
            autoUpload: false,
            calculateSpeed: true,
            allowedExtensions: ['csv']
        });
        _this.inputUploadEvents = new EventEmitter();
        return _this;
    }
    AttendanceImportModalComponent.prototype.ngOnInit = function () {
        if (this.bio_val == true) {
            this.getBioDevice();
        }
    };
    AttendanceImportModalComponent.prototype.ngAfterViewInit = function () {
    };
    //get data from imported file
    AttendanceImportModalComponent.prototype.handleUpload = function (data) {
        var _this = this;
        this.response = [];
        this._zone.run(function () {
            _this.response = data;
            if (data && data.response) {
                _this.response = JSON.parse(data.response);
                _this.showTable = false;
                _this.loader = true;
                // from downloaded excel
                // 		if (this.fromDown == true) {
                //  		this._atteservice.convertFile(this.response).
                // subscribe(
                // 	data => {
                // 		let datas = data;
                // 		if(datas.message == 'Not Exist'){
                // 			this.validateEmpId(datas);
                // 		}
                // 		else if(datas.message == 'Not Allow'){
                // 			this.validateCredential(datas);
                // 		}else{
                // 			this.showTable = true;
                // 			this._atteservice.setData(datas);
                // 			this.loader = false;
                // 	   		this._rt.navigateByUrl('admin/attendance-import-review');
                // 	   		this.close();
                // 		}
                // 	},
                // 	err => console.error(err)
                // );
                // 		}
                if (_this.fromBio == true) {
                    _this._atteservice.attchAttendExcel(_this.response).
                        subscribe(function (data) {
                        var datas = data;
                        _this.loader = false;
                        _this._rt.navigateByUrl('admin/attendance-list-default');
                        _this.success_title = 'Success';
                        _this.success_message = 'New attendance logs was successfully added.';
                        setTimeout(function () {
                            _this.close();
                        }, 2000);
                    }, function (err) { return console.error(err); });
                }
                if (_this.fromBio2 == true) {
                    _this._atteservice.attchAttendExcel2(_this.response).
                        subscribe(function (data) {
                        var datas = data;
                        _this.loader = false;
                        _this._rt.navigateByUrl('admin/attendance-list-default');
                        _this.success_title = 'Success';
                        _this.success_message = 'New attendance logs was successfully added.';
                        setTimeout(function () {
                            _this.close();
                        }, 2000);
                    }, function (err) { return console.error(err); });
                }
                if (_this.fromBio3 == true) {
                    _this._atteservice.attchAttendExcel3(_this.response).
                        subscribe(function (data) {
                        var datas = data;
                        _this.loader = false;
                        _this._rt.navigateByUrl('admin/attendance-list-default');
                        _this.success_title = 'Success';
                        _this.success_message = 'New attendance logs was successfully added.';
                        setTimeout(function () {
                            _this.close();
                        }, 2000);
                    }, function (err) { return console.error(err); });
                }
            }
        });
    };
    AttendanceImportModalComponent.prototype.beforeUpload = function (data) {
    };
    AttendanceImportModalComponent.prototype.startUpload = function () {
        this.inputUploadEvents.emit('startUpload');
    };
    AttendanceImportModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Error in getting data';
            window.scrollTo(0, 0);
        }
    };
    AttendanceImportModalComponent.prototype.typeOfForm = function (event) {
        if (event.target.checked == false) {
            this.fromBio = true;
            this.fromDown = false;
        }
        else {
            this.fromDown = true;
            this.fromBio = false;
        }
    };
    AttendanceImportModalComponent.prototype.getBioDevice = function () {
        var _this = this;
        this._common_service.getBioDevice()
            .subscribe(function (data) {
            _this.bio = Array.from(data);
            _this.bio_value = [];
            _this.bio_current = _this.bio_value;
            var id = 0;
            var text = 'Select Bio Device';
            _this.bio.unshift({ id: id, text: text });
            _this.bio_value = _this.value;
        }, function (err) { return _this.catchError(err); });
    };
    AttendanceImportModalComponent.prototype.changedBio = function (data) {
        this.bio_current = data.value;
        if (this.bio_current != 0) {
            this.validate = true;
        }
        else {
            this.validate = false;
        }
    };
    AttendanceImportModalComponent.prototype.getDataFromBiometrics = function () {
        var _this = this;
        var device_name = this.bio_current;
        this._atteservice.getDataFromBiometrics(device_name).
            subscribe(function (data) {
            var datas = data;
            _this.loader = false;
            _this.success_title = 'Success';
            _this.success_message = 'New attendance logs was successfully added.';
            setTimeout(function () {
                _this.close();
            }, 2000);
        }, function (err) { return console.error(err); });
    };
    AttendanceImportModalComponent.prototype.validateEmpId = function (data) {
        var _this = this;
        var disposable = this.modalService.addDialog(ValidationModalComponent, {
            title: 'Warning',
            message: data.message,
            data: data.data,
            emp_val: true
        }).subscribe(function (isConfirmed) {
            _this.showTable = true;
            _this.fromDown = true;
            _this.fromBio = false;
            _this.loader = false;
            _this.ngOnInit();
        });
    };
    AttendanceImportModalComponent.prototype.validateCredential = function (data) {
        var _this = this;
        var disposable = this.modalService.addDialog(ValidationModalComponent, {
            title: 'Warning',
            message: data.message,
            data: data.data,
            emp_credentials: true
        }).subscribe(function (isConfirmed) {
            _this.showTable = true;
            _this.fromDown = true;
            _this.fromBio = false;
            _this.loader = false;
            _this.ngOnInit();
        });
    };
    AttendanceImportModalComponent.prototype.biometricsChoose = function (value) {
        if (value == 1) {
            this.fromBio = true;
            this.fromBio2 = false;
            this.fromBio3 = false;
        }
        else if (value == 2) {
            this.fromBio = false;
            this.fromBio2 = true;
            this.fromBio3 = false;
        }
        else if (value == 3) {
            this.fromBio = false;
            this.fromBio2 = false;
            this.fromBio3 = true;
        }
        else {
            this.fromBio = false;
            this.fromBio2 = false;
            this.fromBio3 = false;
        }
    };
    AttendanceImportModalComponent = __decorate([
        Component({
            selector: 'app-attendance-import-modal',
            templateUrl: './attendance-import-modal.component.html',
            styleUrls: ['./attendance-import-modal.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            DialogService,
            Configuration,
            FormBuilder,
            Router,
            NgZone,
            AttendanceService,
            Http,
            CommonService,
            Router])
    ], AttendanceImportModalComponent);
    return AttendanceImportModalComponent;
}(DialogComponent));
export { AttendanceImportModalComponent };
//# sourceMappingURL=attendance-import-modal.component.js.map