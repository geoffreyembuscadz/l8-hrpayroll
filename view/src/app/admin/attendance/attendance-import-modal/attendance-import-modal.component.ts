import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Component, OnInit, Injectable, TemplateRef, NgZone, EventEmitter } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgUploaderOptions } from 'ngx-uploader'; // ngx-uploader.com
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from "rxjs/Rx";
import { Http, Response} from '@angular/http';
import { Configuration } from '../../../app.config';
import { CommonService } from '../../../services/common.service';
import { AttendanceService } from '../../../services/attendance.service';
import { Select2OptionData } from 'ng2-select2';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';

@Component({
  selector: 'app-attendance-import-modal',
  templateUrl: './attendance-import-modal.component.html',
  styleUrls: ['./attendance-import-modal.component.css']
})

@Injectable()
export class AttendanceImportModalComponent extends DialogComponent<null, boolean> {

	
	private route_upload = 'upload?type=attendance';
	private error_title: boolean;
	private error_message: string;
	private success_title: string;
	private success_message: string;
	csvUrl: string;
  	csvData: any[] = [];
  	uploadfile:any;
	options: NgUploaderOptions;
	response: any;
	hasBaseDropZoneOver: boolean;
	inputUploadEvents: EventEmitter<string>;
	showTable = true;
	fromDown=true;
	fromBio=false;
	fromBio2=false;
	fromBio3=false;
	loader = false;

	public bio: any;
	public bio_current : any;
	bio_type: any;
	bio_value: Array<Select2OptionData>;
	value:any;
	bio_val:any;
	validate = false;

	constructor(
		dialogService: DialogService,
		private modalService: DialogService, 
		private _config: Configuration, 
		private _fb: FormBuilder, 
		private _route: Router, 
		private _zone: NgZone, 
		private _atteservice: AttendanceService, 
		private http: Http,
		private _common_service: CommonService,
		private _rt: Router
		){
		super(dialogService);	

		// uploader ngzone
	    this.options = new NgUploaderOptions({
	    	url: this._config.ServerWithApiUrl + this.route_upload,
	    	autoUpload: false,
	    	calculateSpeed: true,
	    	allowedExtensions: ['csv']
	    });

	     this.inputUploadEvents = new EventEmitter<string>();
	}

	ngOnInit() {	
		if (this.bio_val == true) {
			this.getBioDevice();
		}
	}

	ngAfterViewInit(){
	}

	//get data from imported file
	handleUpload(data: any) {
		this.response = [];
		this._zone.run(() => {
	    	this.response = data;
	    	if (data && data.response) {
	    		this.response = JSON.parse(data.response);

	    		this.showTable = false;
	    		this.loader = true;
	    		// from downloaded excel
	    // 		if (this.fromDown == true) {
		   //  		this._atteservice.convertFile(this.response).
					// subscribe(
					// 	data => {
					// 		let datas = data;

					// 		if(datas.message == 'Not Exist'){
					// 			this.validateEmpId(datas);
					// 		}
					// 		else if(datas.message == 'Not Allow'){
					// 			this.validateCredential(datas);
					// 		}else{
					// 			this.showTable = true;
					// 			this._atteservice.setData(datas);
					// 			this.loader = false;
					// 	   		this._rt.navigateByUrl('admin/attendance-import-review');
					// 	   		this.close();
					// 		}
					// 	},
					// 	err => console.error(err)
					// );
	    // 		}

	    		if (this.fromBio == true) {
		    		this._atteservice.attchAttendExcel(this.response).
					subscribe(
						data => {
							let datas = data;
							this.loader = false;
							this._rt.navigateByUrl('admin/attendance-list-default');
							this.success_title = 'Success';
							this.success_message = 'New attendance logs was successfully added.';
							setTimeout(() => {
					  		   this.close();
					      	}, 2000);
						},
						err => this.catchError(err)
					);
	    		}

	    		if (this.fromBio2 == true) {
		    		this._atteservice.attchAttendExcel2(this.response).
					subscribe(
						data => {
							let datas = data;
							this.loader = false;
							this._rt.navigateByUrl('admin/attendance-list-default');
							this.success_title = 'Success';
							this.success_message = 'New attendance logs was successfully added.';
							setTimeout(() => {
					  		   this.close();
					      	}, 2000);
						},
						err => console.error(err)
					);
	    		}

	    		if (this.fromBio3 == true) {
		    		this._atteservice.attchAttendExcel3(this.response).
					subscribe(
						data => {
							let datas = data;
							this.loader = false;
							this._rt.navigateByUrl('admin/attendance-list-default');
							this.success_title = 'Success';
							this.success_message = 'New attendance logs was successfully added.';
							setTimeout(() => {
					  		   this.close();
					      	}, 2000);
						},
						err => console.error(err)
					);
	    		}

				
	    	}
	    });
	}

	beforeUpload(data: any){
	}

	startUpload() {
    	this.inputUploadEvents.emit('startUpload');
  	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = true;
			this.error_message = 'Something wen\'t wrong. Please check the file and try againx`.';
			window.scrollTo(0, 0);
		}
	}

	typeOfForm(event){

		if(event.target.checked == false){
			this.fromBio = true;
			this.fromDown = false;
		}else{
			this.fromDown = true;
			this.fromBio = false;
		}
	}

	getBioDevice(){
	      this._common_service.getBioDevice()
	      .subscribe(
	        data => {
	        this.bio = Array.from(data);
	        this.bio_value = [];
	        this.bio_current = this.bio_value;

	        let id = 0;
	        let text = 'Select Bio Device';

	        this.bio.unshift({id,text});

	        this.bio_value = this.value;
	        
	        },
	        err => this.catchError(err)
	    );
	}
  	changedBio(data: any) {
      this.bio_current = data.value;
      if (this.bio_current != 0) {
      	this.validate = true;
      }else{
      	this.validate = false;
      }
    } 

    getDataFromBiometrics(){

    	let device_name = this.bio_current;
    	this._atteservice.getDataFromBiometrics(device_name).
		subscribe(
			data => {
				let datas = data;
				this.loader = false;
				this.success_title = 'Success';
				this.success_message = 'New attendance logs was successfully added.';
				setTimeout(() => {
		  		   this.close();
		      	}, 2000);
			},
			err => console.error(err)
		);
    }

    validateEmpId(data){
    	let disposable = this.modalService.addDialog(ValidationModalComponent, {
            title:'Warning',
			message:data.message,
			data:data.data,
			emp_val:true
        	}).subscribe((isConfirmed)=>{
        		this.showTable = true;
				this.fromDown=true;
				this.fromBio=false;
				this.loader = false;
                this.ngOnInit();
            });
    }

    validateCredential(data){

    	let disposable = this.modalService.addDialog(ValidationModalComponent, {
            title:'Warning',
			message:data.message,
			data:data.data,
			emp_credentials:true
        	}).subscribe((isConfirmed)=>{
        		this.showTable = true;
				this.fromDown=true;
				this.fromBio=false;
				this.loader = false;
                this.ngOnInit();
            });
    }


    biometricsChoose(value) {

	   if (value == 1) {
	   	this.fromBio = true;
	   	this.fromBio2 = false;
	   	this.fromBio3 = false;
	   } else if (value == 2) {
	   	this.fromBio = false;
	   	this.fromBio2 = true;
	   	this.fromBio3 = false;
	   } else if (value == 3) {
	   	this.fromBio = false;
	   	this.fromBio2 = false;
	   	this.fromBio3 = true;
	   } else {
	   	this.fromBio = false;
	   	this.fromBio2 = false;
	   	this.fromBio3 = false;
	   }
	 }




}
