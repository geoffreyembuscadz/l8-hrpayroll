import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AttendanceService } from '../../../services/attendance.service';
import { CommonService } from '../../../services/common.service';
import { Select2OptionData } from 'ng2-select2';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TimepickerModule } from 'ngx-bootstrap';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { ChecklistModalComponent } from '../../checklist-modal/checklist-modal.component';

@Component({
  selector: 'app-attendance-modal',
  templateUrl: './attendance-modal.component.html',
  styleUrls: ['./attendance-modal.component.css']
})
export class AttendanceModalComponent extends DialogComponent<null, boolean>{

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public options: Select2Options;
	public current: any;
	public time_in:any;
	public time_out:any;
	public lunch_in:any;
	public lunch_out:any
	public current_ws: any;
	AttendanceForm:any;
	emp:any;
	poststore:any;
	user_id:any;
	public mainInput = {
        start: moment().subtract(12, 'month'),
        end: moment().subtract(11, 'month')
    }
    singleDate = false;
	public ismeridian:boolean = true;
	empStats = false;
	public start_time:Date = new Date();
	public end_time:Date = new Date();
	public lunch_start_time	:Date = new Date();
	public lunch_end_time:Date = new Date();
	validate = false;
    employee_id:any;
    att_id:any;
    att_data:any;
    edit:any;
    create:any;
    multi:any;
    single:any;
    branch_id:any;
    company_id: any;
    working_schedule: any;
    date=[];
    holiday=[];

    public branch: any;
	public branch_current : any;
	branch_value: any;
	working_schedule_value: any;

	working_schedule_company: any;
	public working_schedule_company_current : any;

	supervisor_id:any;
	number_of_days:any;
	late:any;
	overtime:any;
	night_differential:any;
	restday:any;
	holiday_id:any;
	holiday_ids:any;
	default:any;

  constructor(
  	private _common_service: CommonService,
   	private _attservice: AttendanceService, 
   	private _fb: FormBuilder, 
   	private _ar: ActivatedRoute,
   	private daterangepickerOptions: DaterangepickerConfig,
   	private _auth_service: AuthUserService,
   	private modalService: DialogService
  	){ 
  	
  	super(modalService);

    this.AttendanceForm = _fb.group({
		'time_in': 			[null],
		'time_out': 		[null],
		'lunch_in': 		[null],
		'lunch_out': 		[null],
		'status_id':		[null],
		'date': 			[null],
		'schedule': 		[null],
		'number_of_days': 	[null],
		'late': 			[null],
		'overtime': 		[null],
		'night_differential':[null],
		'restday':[null],
      
    }); 

    
  }

	ngOnInit() {

		if (this.default) {
			this.singleDatePicker();
		}else{
			this.multiDatePicker();
		}

		if(this.employee_id != null){
    		this.validate = true
    	}
		if(this.edit == true){
			this.getAttendance();
			this.get_employee();
    	}
		this.getBranch();
 	}

	getAttendance(){

		if (this.default) {
			this._attservice.getAttendanceDefault(this.att_id).subscribe(
		      data => {
		        this.att_data=(data);
		        this.mainInput.start = this.att_data.time_in;
		        this.start_time = this.att_data.time_in;
		        this.end_time = this.att_data.time_out;
		        this.lunch_start_time = this.att_data.lunch_in;
				this.lunch_end_time = this.att_data.lunch_out;
		        this.employee_id = this.att_data.employee_id;
		        this.branch_id = this.att_data.branch_id;
		        this.company_id = this.att_data.company_id;
		        this.current_ws = this.att_data.working_schedule_id;

		        this.getWorkingSchedulePerCompany(this.company_id);

		        this.getBranch();
		      },
		      err => console.error(err)
	    	);
		}
		else if (!this.default) {
			this._attservice.getAttendance(this.att_id).subscribe(
		      data => {
		        this.att_data=(data);
		        this.mainInput.start = this.att_data.start_date;
		        this.mainInput.end = this.att_data.end_date;
		        this.employee_id = this.att_data.employee_id;
		        this.branch_id = this.att_data.branch_id;
		        this.number_of_days = this.att_data.number_of_days;
		        this.late = this.att_data.late;
		        this.overtime = this.att_data.overtime;
		        this.night_differential = this.att_data.night_differential;
		        this.restday = this.att_data.restday;
		        this.holiday_id = this.att_data.holiday_id;
		        this.holiday_ids = this.att_data.holiday_ids;


		        this.getBranch();
		        this.computeDays();
		      },
		      err => console.error(err)
	    	);
		}

	}

	singleDatePicker(){
		this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
    		showDropdowns: true,
    		opens: "center"
		}; 
	}

	multiDatePicker(){
		this.daterangepickerOptions.settings = {
        	singleDatePicker: false,
        	locale: { format: 'MM/DD/YYYY' },
	        alwaysShowCalendars: true
		};
	}

	get_employee(){
		
		this.employee = this.emp;
		this.employee_value = [];
		if(this.edit == true) {

			this.employee_value = this.employee_id;
			this.validate = true;
        } 
		this.options = {
			multiple: true
		}
		this.current = this.employee_value;
	}
	changed(data: any) {
	  this.current = data.value;
	  let len = this.current.length;
      if(len >= 1){
      	this.empStats = true;
      }
      else{
      	this.empStats = false;
      }

      if(this.empStats == true || this.edit == true){
      	this.validate=true;
      }


	}

	createAttendance(){
		this.AttendanceForm.value.employee_id = this.current;
		this.AttendanceForm.value.time_in = this.time_in;
		this.AttendanceForm.value.time_out = this.time_out;
		this.AttendanceForm.value.lunch_in = this.lunch_in;
		this.AttendanceForm.value.lunch_out = this.lunch_out;
		this.AttendanceForm.value.created_by = this.user_id;
		this.AttendanceForm.value.schedule = this.working_schedule_company_current;
		// if(this.employee_id != null){
		// 	let v = [];	
		// 	v.push(this.employee_id);
		// 	this.AttendanceForm.value.employee_id = v;
		
		// }
		//if user didn't click the range
		// if(this.time_in == null && this.time_out == null){
			let date = moment(this.mainInput.start).format("YYYY-MM-DD");
			let start =  moment(this.start_time).format("HH:mm:ss");
			let end = moment(this.end_time).format("HH:mm:ss");
			let lunch_start =  moment(this.lunch_start_time).format("HH:mm:ss");
			let lunch_end = moment(this.lunch_end_time).format("HH:mm:ss");

			if (this.lunch_start_time == null) {
				this.AttendanceForm.value.lunch_in = null;
			} else {
				this.AttendanceForm.value.lunch_in = date + ' ' + lunch_start;
			}
			if (this.lunch_end_time == null) {
				this.AttendanceForm.value.lunch_out = null;
			} else {
				this.AttendanceForm.value.lunch_out = date + ' ' + lunch_end;
			}

			this.AttendanceForm.value.time_in = date + ' ' + start;
			this.AttendanceForm.value.time_out = date + ' ' + end;
			this.AttendanceForm.value.start_time = start;
			this.AttendanceForm.value.end_time = end;
			this.AttendanceForm.value.branch_id = this.branch_current;

		// }


		let  att = this.AttendanceForm.value;
		if(this.edit == true){
			this.AttendanceForm.value.employee_id = this.employee_id;
			att = this.AttendanceForm.value;
			let id = this.att_id;
			this._attservice
			.updateAttendance(id,att)
			.subscribe(
				data => {
					this.poststore = Array.from(data);
					this.validate = false;
					this.success_title = "Success";
					this.success_message = "Successfully updated.";
					setTimeout(() => {
			  		   this.close();
			      	}, 1000);
				},
				err => this.catchError(err)
			);
		}
		if(this.create == true){
			this._attservice
			.createTimeAttendance(att)
			.subscribe(
				data => {
					this.poststore = Array.from(data);
					this.validate = false;
					this.success_title = "Success";
					this.success_message = "Successfully added.";
					setTimeout(() => {
			  		   this.close();
			      	}, 1000);
				},
				err => this.catchError(err)
			);
		}
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	private selectedDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;

        //format date
        this.time_in = moment(dateInput.start).format("YYYY-MM-DD HH:mm:ss");
       	this.time_out = moment(dateInput.end).format("YYYY-MM-DD HH:mm:ss");

       	this.computeDays()
    }

    SingleDate(event:any){
		this.singleDate = event;

	}

	//time picker option
	public toggleMode():void {
	this.ismeridian = !this.ismeridian;

	}

	archive(){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:this.att_id,
            url:'attendance'
        	}, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
    //     	setTimeout(() => {
		       	this.close();
    //     		// this.rerender();
		  // }, 1000);
                
        });


	}

	getBranch(){
	      this._common_service.getBranch()
	      .subscribe(
	        data => {
	        this.branch = Array.from(data);
	        this.branch_value = [];
	        if(this.edit == true) {
				this.branch_value = this.branch_id;
	        }
	        this.branch_current = this.branch_value;
	        
	        },
	        err => console.error(err)
	    );
	}


	changedBranch(data: any,i:any) {

		this.branch_current = data.value;
	}

	updateAttendanceNotDefault(){

		let sdate = moment(this.mainInput.start).format("YYYY-MM-DD");
		let edate = moment(this.mainInput.end).format("YYYY-MM-DD");

		let a = this.mainInput.start;
		let b = this.mainInput.end;


		let date1 = new Date(sdate);
		let date2 = new Date(edate);
		let timeDiff = Math.abs(date2.getTime() - date1.getTime());
		let len = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

		if (len < this.number_of_days) {
			this.number_of_days = len;
		}


		let model ={
			employee_id:this.current,
			branch_id:this.branch_current,
			number_of_days:this.number_of_days,
			late:this.late,
			overtime:this.overtime,
			night_differential:this.night_differential,
			start_date:sdate + ' ' + "00:00:00",
			end_date:edate + ' ' + "23:59:00",
			holiday_id:this.holiday_id,
			holiday_ids:this.holiday_ids,
			restday:this.restday
		}

		let id = this.att_id;
		this._attservice
		.updateAttendanceNotDefault(id,model)
		.subscribe(
			data => {
				this.poststore = Array.from(data);
				this.validate = false;
				this.success_title = "Success";
				this.success_message = "Successfully added.";
				setTimeout(() => {
		  		   this.close();
		      	}, 1000);
			},
			err => this.catchError(err)
		);
	}

	computeDays(){

		let start_date = this.mainInput.start;
		let end_date = this.mainInput.end;
		let date =[];
		this.date =[];

		let start = start_date;
		let end = end_date;
		let from = moment(start_date, 'YYYY-MM-DD');
		let to = moment(end_date, 'YYYY-MM-DD');


	    let currDate = from.clone().startOf('day');
	    let lastDate = to.clone().startOf('day');

	   	date.push(currDate.toDate());

	    while(currDate.add(1 ,'days').diff(lastDate) < 0) {
	        date.push(currDate.clone().toDate());
	    }

	    if(start != end)  {
	    	date.push(lastDate.toDate());
	    }

	    let n = {}
		for(let i = 0; i < date.length; i++) 
		{
			if (!n[date[i]]) 
			{
				n[date[i]] = true; 
				this.date.push(moment(date[i]).format("YYYY-MM-DD")); 
			}
		}

		this.checkHoliday();
	}

	checkHoliday(){

		let holi = [];

		this._common_service.checkDateHoliday(this.date).
			subscribe(
				data => {
					holi = data;
					this.holiday = holi;
				},
				err => console.error(err)
			);
	}

	checklistOfHoliday(){

		let con = this.holiday_ids.split(",");
		let temp =[];
		for (let x = 0; x < con.length; ++x) {
			temp.push(parseInt(con[x]));
		}
    	let holiday = temp;

    	console.log(holiday)
    	let disposable = this.modalService.addDialog(ChecklistModalComponent, {
            title:'Holiday List',
            tableVal:true,
            attend:false,
            data:this.holiday,
            holiday:holiday,
            clearance:false,
			onboarding:false,
			attendModal:true
        	}).subscribe((message)=>{
        		if (message != 1 || message == []) {

        			let temp_for_type1 = [];
					let temp_for_type2 ='0';
					let temp_for_id1 = [];
					let temp_for_id2 ='0';

					for (let i = 0; i < message.length; ++i) {
					 	temp_for_type1.push(message[i].type);
						temp_for_type2 = temp_for_type1.join();

						temp_for_id1.push(message[i].id);
						temp_for_id2 = temp_for_id1.join();
					}
					this.holiday_ids = temp_for_id2;
					this.holiday_id = temp_for_type2;
        			this.holiday = message;
        		}
            });
    }

    getWorkingSchedulePerCompany(company_id) {
		this._attservice.getWorkingSchedulePerCompany(company_id)
		.subscribe(
			data => {
				
				this.working_schedule = Array.from(data);
		        
				this.working_schedule_value = this.current_ws;

		        this.working_schedule_company_current = this.working_schedule_value;
			},
			err => console.error(err)
		);

	}

	changedWorkingSchedule(id: any) {
		this.working_schedule_company_current = id.value;
	}

}
