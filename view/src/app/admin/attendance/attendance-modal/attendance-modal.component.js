var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AttendanceService } from '../../../services/attendance.service';
import { CommonService } from '../../../services/common.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import * as moment from 'moment';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { ChecklistModalComponent } from '../../checklist-modal/checklist-modal.component';
var AttendanceModalComponent = /** @class */ (function (_super) {
    __extends(AttendanceModalComponent, _super);
    function AttendanceModalComponent(_common_service, _attservice, _fb, _ar, daterangepickerOptions, _auth_service, modalService) {
        var _this = _super.call(this, modalService) || this;
        _this._common_service = _common_service;
        _this._attservice = _attservice;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this._auth_service = _auth_service;
        _this.modalService = modalService;
        _this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        _this.singleDate = false;
        _this.ismeridian = true;
        _this.empStats = false;
        _this.start_time = new Date();
        _this.end_time = new Date();
        _this.lunch_start_time = new Date();
        _this.lunch_end_time = new Date();
        _this.validate = false;
        _this.date = [];
        _this.holiday = [];
        _this.AttendanceForm = _fb.group({
            'time_in': [null],
            'time_out': [null],
            'lunch_in': [null],
            'lunch_out': [null],
            'status_id': [null],
            'date': [null],
            'schedule': [null],
            'number_of_days': [null],
            'late': [null],
            'overtime': [null],
            'night_differential': [null],
            'restday': [null],
        });
        return _this;
    }
    AttendanceModalComponent.prototype.ngOnInit = function () {
        if (this.default) {
            this.singleDatePicker();
        }
        else {
            this.multiDatePicker();
        }
        if (this.employee_id != null) {
            this.validate = true;
        }
        if (this.edit == true) {
            this.getAttendance();
            this.get_employee();
        }
        this.getBranch();
    };
    AttendanceModalComponent.prototype.getAttendance = function () {
        var _this = this;
        if (this.default) {
            this._attservice.getAttendanceDefault(this.att_id).subscribe(function (data) {
                _this.att_data = (data);
                _this.mainInput.start = _this.att_data.time_in;
                _this.start_time = _this.att_data.time_in;
                _this.end_time = _this.att_data.time_out;
                _this.lunch_start_time = _this.att_data.lunch_in;
                _this.lunch_end_time = _this.att_data.lunch_out;
                _this.employee_id = _this.att_data.employee_id;
                _this.branch_id = _this.att_data.branch_id;
                _this.company_id = _this.att_data.company_id;
                _this.current_ws = _this.att_data.working_schedule_id;
                _this.getWorkingSchedulePerCompany(_this.company_id);
                _this.getBranch();
            }, function (err) { return console.error(err); });
        }
        else if (!this.default) {
            this._attservice.getAttendance(this.att_id).subscribe(function (data) {
                _this.att_data = (data);
                _this.mainInput.start = _this.att_data.start_date;
                _this.mainInput.end = _this.att_data.end_date;
                _this.employee_id = _this.att_data.employee_id;
                _this.branch_id = _this.att_data.branch_id;
                _this.number_of_days = _this.att_data.number_of_days;
                _this.late = _this.att_data.late;
                _this.overtime = _this.att_data.overtime;
                _this.night_differential = _this.att_data.night_differential;
                _this.restday = _this.att_data.restday;
                _this.holiday_id = _this.att_data.holiday_id;
                _this.holiday_ids = _this.att_data.holiday_ids;
                _this.getBranch();
                _this.computeDays();
            }, function (err) { return console.error(err); });
        }
    };
    AttendanceModalComponent.prototype.singleDatePicker = function () {
        this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
            showDropdowns: true,
            opens: "center"
        };
    };
    AttendanceModalComponent.prototype.multiDatePicker = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false,
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: true
        };
    };
    AttendanceModalComponent.prototype.get_employee = function () {
        this.employee = this.emp;
        this.employee_value = [];
        if (this.edit == true) {
            this.employee_value = this.employee_id;
            this.validate = true;
        }
        this.options = {
            multiple: true
        };
        this.current = this.employee_value;
    };
    AttendanceModalComponent.prototype.changed = function (data) {
        this.current = data.value;
        var len = this.current.length;
        if (len >= 1) {
            this.empStats = true;
        }
        else {
            this.empStats = false;
        }
        if (this.empStats == true || this.edit == true) {
            this.validate = true;
        }
    };
    AttendanceModalComponent.prototype.createAttendance = function () {
        var _this = this;
        this.AttendanceForm.value.employee_id = this.current;
        this.AttendanceForm.value.time_in = this.time_in;
        this.AttendanceForm.value.time_out = this.time_out;
        this.AttendanceForm.value.lunch_in = this.lunch_in;
        this.AttendanceForm.value.lunch_out = this.lunch_out;
        this.AttendanceForm.value.created_by = this.user_id;
        this.AttendanceForm.value.schedule = this.working_schedule_company_current;
        // if(this.employee_id != null){
        // 	let v = [];	
        // 	v.push(this.employee_id);
        // 	this.AttendanceForm.value.employee_id = v;
        // }
        //if user didn't click the range
        // if(this.time_in == null && this.time_out == null){
        var date = moment(this.mainInput.start).format("YYYY-MM-DD");
        var start = moment(this.start_time).format("HH:mm:ss");
        var end = moment(this.end_time).format("HH:mm:ss");
        var lunch_start = moment(this.lunch_start_time).format("HH:mm:ss");
        var lunch_end = moment(this.lunch_end_time).format("HH:mm:ss");
        if (this.lunch_start_time == null) {
            this.AttendanceForm.value.lunch_in = null;
        }
        else {
            this.AttendanceForm.value.lunch_in = date + ' ' + lunch_start;
        }
        if (this.lunch_end_time == null) {
            this.AttendanceForm.value.lunch_out = null;
        }
        else {
            this.AttendanceForm.value.lunch_out = date + ' ' + lunch_end;
        }
        this.AttendanceForm.value.time_in = date + ' ' + start;
        this.AttendanceForm.value.time_out = date + ' ' + end;
        this.AttendanceForm.value.start_time = start;
        this.AttendanceForm.value.end_time = end;
        this.AttendanceForm.value.branch_id = this.branch_current;
        // }
        var att = this.AttendanceForm.value;
        if (this.edit == true) {
            this.AttendanceForm.value.employee_id = this.employee_id;
            att = this.AttendanceForm.value;
            var id = this.att_id;
            this._attservice
                .updateAttendance(id, att)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.validate = false;
                _this.success_title = "Success";
                _this.success_message = "Successfully updated.";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
        if (this.create == true) {
            this._attservice
                .createTimeAttendance(att)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.validate = false;
                _this.success_title = "Success";
                _this.success_message = "Successfully added.";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    AttendanceModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    AttendanceModalComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.time_in = moment(dateInput.start).format("YYYY-MM-DD HH:mm:ss");
        this.time_out = moment(dateInput.end).format("YYYY-MM-DD HH:mm:ss");
        this.computeDays();
    };
    AttendanceModalComponent.prototype.SingleDate = function (event) {
        this.singleDate = event;
    };
    //time picker option
    AttendanceModalComponent.prototype.toggleMode = function () {
        this.ismeridian = !this.ismeridian;
    };
    AttendanceModalComponent.prototype.archive = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: this.att_id,
            url: 'attendance'
        }, { backdropColor: 'rgba(238,238,238)' }).subscribe(function (isConfirmed) {
            //     	setTimeout(() => {
            _this.close();
            //     		// this.rerender();
            // }, 1000);
        });
    };
    AttendanceModalComponent.prototype.getBranch = function () {
        var _this = this;
        this._common_service.getBranch()
            .subscribe(function (data) {
            _this.branch = Array.from(data);
            _this.branch_value = [];
            if (_this.edit == true) {
                _this.branch_value = _this.branch_id;
            }
            _this.branch_current = _this.branch_value;
        }, function (err) { return console.error(err); });
    };
    AttendanceModalComponent.prototype.changedBranch = function (data, i) {
        this.branch_current = data.value;
    };
    AttendanceModalComponent.prototype.updateAttendanceNotDefault = function () {
        var _this = this;
        var sdate = moment(this.mainInput.start).format("YYYY-MM-DD");
        var edate = moment(this.mainInput.end).format("YYYY-MM-DD");
        var a = this.mainInput.start;
        var b = this.mainInput.end;
        var date1 = new Date(sdate);
        var date2 = new Date(edate);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var len = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (len < this.number_of_days) {
            this.number_of_days = len;
        }
        var model = {
            employee_id: this.current,
            branch_id: this.branch_current,
            number_of_days: this.number_of_days,
            late: this.late,
            overtime: this.overtime,
            night_differential: this.night_differential,
            start_date: sdate + ' ' + "00:00:00",
            end_date: edate + ' ' + "23:59:00",
            holiday_id: this.holiday_id,
            holiday_ids: this.holiday_ids,
            restday: this.restday
        };
        var id = this.att_id;
        this._attservice
            .updateAttendanceNotDefault(id, model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            _this.validate = false;
            _this.success_title = "Success";
            _this.success_m;
            setTimeout(function () {
                _this.close();
            }, 1000);
        }, function (err) { return _this.catchError(err); });
    };
    AttendanceModalComponent.prototype.computeDays = function () {
        var start_date = this.mainInput.start;
        var end_date = this.mainInput.end;
        var date = [];
        this.date = [];
        var start = start_date;
        var end = end_date;
        var from = moment(start_date, 'YYYY-MM-DD');
        var to = moment(end_date, 'YYYY-MM-DD');
        var currDate = from.clone().startOf('day');
        var lastDate = to.clone().startOf('day');
        date.push(currDate.toDate());
        while (currDate.add(1, 'days').diff(lastDate) < 0) {
            date.push(currDate.clone().toDate());
        }
        if (start != end) {
            date.push(lastDate.toDate());
        }
        var n = {};
        for (var i = 0; i < date.length; i++) {
            if (!n[date[i]]) {
                n[date[i]] = true;
                this.date.push(moment(date[i]).format("YYYY-MM-DD"));
            }
        }
        this.checkHoliday();
    };
    AttendanceModalComponent.prototype.checkHoliday = function () {
        var _this = this;
        var holi = [];
        this._common_service.checkDateHoliday(this.date).
            subscribe(function (data) {
            holi = data;
            _this.holiday = holi;
        }, function (err) { return console.error(err); });
    };
    AttendanceModalComponent.prototype.checklistOfHoliday = function () {
        var _this = this;
        var con = this.holiday_ids.split(",");
        var temp = [];
        for (var x = 0; x < con.length; ++x) {
            temp.push(parseInt(con[x]));
        }
        var holiday = temp;
        console.log(holiday);
        var disposable = this.modalService.addDialog(ChecklistModalComponent, {
            title: 'Holiday List',
            tableVal: true,
            attend: false,
            data: this.holiday,
            holiday: holiday,
            clearance: false,
            onboarding: false,
            attendModal: true
        }).subscribe(function (message) {
            if (message != 1 || message == []) {
                var temp_for_type1 = [];
                var temp_for_type2 = '0';
                var temp_for_id1 = [];
                var temp_for_id2 = '0';
                for (var i = 0; i < message.length; ++i) {
                    temp_for_type1.push(message[i].type);
                    temp_for_type2 = temp_for_type1.join();
                    temp_for_id1.push(message[i].id);
                    temp_for_id2 = temp_for_id1.join();
                }
                _this.holiday_ids = temp_for_id2;
                _this.holiday_id = temp_for_type2;
                _this.holiday = message;
            }
        });
    };
    AttendanceModalComponent.prototype.getWorkingSchedulePerCompany = function (company_id) {
        var _this = this;
        this._attservice.getWorkingSchedulePerCompany(company_id)
            .subscribe(function (data) {
            _this.working_schedule = Array.from(data);
            _this.working_schedule_value = _this.current_ws;
            _this.working_schedule_company_current = _this.working_schedule_value;
        }, function (err) { return console.error(err); });
    };
    AttendanceModalComponent.prototype.changedWorkingSchedule = function (id) {
        this.working_schedule_company_current = id.value;
    };
    AttendanceModalComponent = __decorate([
        Component({
            selector: 'app-attendance-modal',
            templateUrl: './attendance-modal.component.html',
            styleUrls: ['./attendance-modal.component.css']
        }),
        __metadata("design:paramtypes", [CommonService,
            AttendanceService,
            FormBuilder,
            ActivatedRoute,
            DaterangepickerConfig,
            AuthUserService,
            DialogService])
    ], AttendanceModalComponent);
    return AttendanceModalComponent;
}(DialogComponent));
export { AttendanceModalComponent };
//# sourceMappingURL=attendance-modal.component.js.map