var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AuthUserService } from '../../../services/auth-user.service';
import { AttendanceService } from '../../../services/attendance.service';
import { CommonService } from '../../../services/common.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { DialogService } from "ng2-bootstrap-modal";
import { AttendanceImportModalComponent } from '../../attendance/attendance-import-modal/attendance-import-modal.component';
var AttendanceImportReviewComponent = /** @class */ (function () {
    function AttendanceImportReviewComponent(_common_service, _atteservice, daterangepickerOptions, _rt, modalService, _auth_service) {
        this._common_service = _common_service;
        this._atteservice = _atteservice;
        this.daterangepickerOptions = daterangepickerOptions;
        this._rt = _rt;
        this.modalService = modalService;
        this._auth_service = _auth_service;
        this.validate = true;
        this.data = [];
        this.final_data = [];
        this.invalid = 0;
        this.tableVal = false;
        this.loader = false;
        this.names = [];
        this.work_sched = [];
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.options2 = {
            dropdownAutoWidth: true
        };
        this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
            showDropdowns: true,
            opens: "center"
        };
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    AttendanceImportReviewComponent.prototype.ngOnInit = function () {
        this.data = this._atteservice.getData();
        if (this.data.length != 0) {
            this.getBranch();
            this.collectName();
            this.getWorkingSchedule();
        }
    };
    AttendanceImportReviewComponent.prototype.collectName = function () {
        this.tableVal = false;
        this.loader = true;
        this.names = [];
        for (var i = 0; i < this.data.length; ++i) {
            var emp_id = this.data[i]['emp_id'];
            var name_1 = this.data[i]['name'];
            this.names.push({ emp_id: emp_id, name: name_1 });
        }
        for (var i = 0; i < this.names.length; i++) {
            for (var j = i + 1; j < this.names.length;) {
                if (this.names[i]['emp_id'] == this.names[j]['emp_id'] && this.names[i]['name'] == this.names[j]['name']) {
                    this.names.splice(j, 1);
                }
                else {
                    j++;
                }
            }
        }
        this.arrangeData();
    };
    AttendanceImportReviewComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    AttendanceImportReviewComponent.prototype.arrangeData = function () {
        var _this = this;
        var temp_data = [];
        var len = this.data.length;
        for (var i = 0; i < len; ++i) {
            var emp_id = this.data[i]['emp_id'];
            var name_2 = this.data[i]['name'];
            var branch_id = this.data[i]['branch_id'];
            // let date = '';
            var date = moment(this.data[i]['time_in']).format("YYYY-MM-DD");
            var start_time = moment(this.data[i]['time_in']).format("HH:mm:ss");
            var end_time = moment(this.data[i]['time_out']).format("HH:mm:ss");
            var y = moment().format("YYYY-MM-DD");
            // let time_in = date + ' ' + start_time;
            // let time_out = date + ' ' + end_time;
            var time_in = moment(this.data[i]['time_in']).format("YYYY-MM-DD HH:mm:ss");
            var time_out = moment(this.data[i]['time_out']).format("YYYY-MM-DD HH:mm:ss");
            if (start_time == 'Invalid date' && end_time == 'Invalid date') {
                this.invalid++;
                this.invalid++;
                date = y;
                start_time = '';
                end_time = '';
                time_in = '';
                time_out = '';
            }
            else if (end_time == 'Invalid date') {
                this.invalid++;
                date = moment(this.data[i]['time_in']).format("YYYY-MM-DD");
                end_time = '';
                time_out = '';
            }
            else if (start_time == 'Invalid date') {
                this.invalid++;
                date = moment(this.data[i]['time_out']).format("YYYY-MM-DD");
                start_time = '';
                time_in = '';
            }
            else {
                date = moment(this.data[i]['time_in']).format("YYYY-MM-DD");
            }
            temp_data.push({ emp_id: emp_id, branch_id: branch_id, date: date, start_time: start_time, end_time: end_time, time_in: time_in, time_out: time_out });
        }
        var dat = temp_data, names = this.names, show = true, map = new Map, result = names.map(function (_a) {
            var emp_id = _a.emp_id, name = _a.name;
            return ({ name: name, show: show, details: map.set(emp_id, []).get(emp_id) });
        });
        dat.forEach(function (_a) {
            var emp_id = _a.emp_id, branch_id = _a.branch_id, date = _a.date, start_time = _a.start_time, end_time = _a.end_time, time_in = _a.time_in, time_out = _a.time_out;
            return map.get(emp_id).push({ emp_id: emp_id, branch_id: branch_id, date: date, start_time: start_time, end_time: end_time, time_in: time_in, time_out: time_out });
        });
        this.getBranch();
        setTimeout(function () {
            _this.final_data = result;
            _this.tableVal = true;
            _this.valid();
            _this.loader = false;
        }, 2000);
        // setTimeout(() => {
        //   		let len = this.final_data.length;
        //   		for(let i=0; i<len;i++){
        //   			let obj = this.final_data[i];
        //   			let val = new String(obj.branch_id);
        //   			// console.log('val: ',"#branch_" + val + " > .select2-hidden-accessible");
        //  		   $("#branch_" + val + " > .select2-hidden-accessible").select2().select2('val',val);
        //   		}
        //      }, 2500);
    };
    AttendanceImportReviewComponent.prototype.valid = function () {
        if (this.invalid > 0) {
            this.validate = false;
        }
        else {
            this.validate = true;
        }
    };
    AttendanceImportReviewComponent.prototype.save = function () {
        var _this = this;
        if (this.validate == true) {
            this.loader = true;
            this.tableVal = false;
            var model = this.final_data;
            this._atteservice.attachAttendanceFiles(model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.validate = false;
                _this.tableVal = false;
                _this.error_title = '';
                _this.error_message = '';
                _this.loader = false;
                _this.success_title = "Success!";
                _this.success_message = "Successfully Imported";
                setTimeout(function () {
                    _this._rt.navigateByUrl('admin/attendance-list-default');
                }, 3000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    AttendanceImportReviewComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data has incomplete details.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    AttendanceImportReviewComponent.prototype.getBranch = function () {
        var _this = this;
        this._common_service.getBranch()
            .subscribe(function (data) {
            _this.branch = Array.from(data);
            _this.branch_value = [];
            // let id = 0;
            // let text = 'Select Branch';
            // this.branch.unshift({id,text});
            // this.branch_current = this.branch_value;
        }, function (err) { return console.error(err); });
    };
    AttendanceImportReviewComponent.prototype.changedBranch = function (datas, i, x) {
        if (datas.value != 0) {
            this.final_data[i].details[x].branch_id = datas.value;
        }
    };
    AttendanceImportReviewComponent.prototype.selectedDate = function (value, i) {
        var date = moment(value.start).format("YYYY-MM-DD");
        this.final_data[i].date = date;
    };
    AttendanceImportReviewComponent.prototype.import = function () {
        var _this = this;
        this.modalService.addDialog(AttendanceImportModalComponent, {
            title: 'Upload File',
            bio_val: false
        }).subscribe(function (isConfirmed) {
            _this.ngOnInit();
        });
    };
    AttendanceImportReviewComponent.prototype.filter = function (name) {
        var len = this.final_data.length;
        for (var i = 0; i < len; ++i) {
            if (this.final_data[i].name.toLowerCase().match(name.toLowerCase())) {
                this.final_data[i].show = true;
            }
            else {
                this.final_data[i].show = false;
            }
        }
    };
    AttendanceImportReviewComponent.prototype.removeInList = function (i, x) {
        var lent = this.final_data.length;
        this.final_data[i].details.splice(x, 1);
        if (this.final_data[i].details.length == 0) {
            this.final_data.splice(i, 1);
        }
        if (this.final_data.length == 0) {
            this.tableVal = false;
            this.validate = false;
            this.data = [];
            this.final_data = [];
        }
        else {
            this.validate = true;
        }
    };
    AttendanceImportReviewComponent.prototype.changeInputTime = function (event, type, i, x) {
        var time = event.target.value;
        var data = this.final_data;
        if (type == 'in') {
            var date = moment(this.final_data[i].details[x].date).format('YYYY-MM-DD');
            this.final_data[i].details[x].start_time = time;
            this.final_data[i].details[x].time_in = date + ' ' + time;
        }
        else if (type == 'out') {
            var date = moment(this.final_data[i].details[x].date).format('YYYY-MM-DD');
            this.final_data[i].details[x].end_time = time;
            this.final_data[i].details[x].time_out = date + ' ' + time;
        }
        if (time == '') {
            this.invalid++;
            this.validate = false;
        }
        else {
            this.invalid--;
            this.valid();
        }
    };
    AttendanceImportReviewComponent.prototype.getWorkingSchedule = function () {
        var _this = this;
        this._common_service.getWorkingScheduleWithoutBreak()
            .subscribe(function (data) {
            _this.work_sched = data;
            _this.ws_value = [];
            _this.ws_current = _this.ws_value;
            var id = 0;
            var text = 'Suggested Schedule';
            _this.work_sched.unshift({ id: id, text: text });
            _this.ws_value = _this.value;
        }, function (err) { return _this.catchError(err); });
    };
    AttendanceImportReviewComponent.prototype.changedWorkingSchedule = function (data, i, x) {
        for (var y = 0; y < this.work_sched.length; ++y) {
            if (this.work_sched[y].id == data.value) {
                var date = moment(this.final_data[i].details[x].date).format('YYYY-MM-DD');
                this.final_data[i].details[x].time_in = date + ' ' + this.work_sched[y].time_in;
                this.final_data[i].details[x].time_out = date + ' ' + this.work_sched[y].time_out;
                this.final_data[i].details[x].start_time = this.work_sched[y].time_in;
                this.final_data[i].details[x].end_time = this.work_sched[y].time_out;
            }
        }
    };
    AttendanceImportReviewComponent = __decorate([
        Component({
            selector: 'app-attendance-import-review',
            templateUrl: './attendance-import-review.component.html',
            styleUrls: ['./attendance-import-review.component.css']
        }),
        __metadata("design:paramtypes", [CommonService,
            AttendanceService,
            DaterangepickerConfig,
            Router,
            DialogService,
            AuthUserService])
    ], AttendanceImportReviewComponent);
    return AttendanceImportReviewComponent;
}());
export { AttendanceImportReviewComponent };
//# sourceMappingURL=attendance-import-review.component.js.map