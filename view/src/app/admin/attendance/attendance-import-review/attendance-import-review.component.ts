
import { Component, OnInit, Injectable, Input} from '@angular/core';
import { MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AuthUserService } from '../../../services/auth-user.service';
import { AttendanceService } from '../../../services/attendance.service';
import { CommonService } from '../../../services/common.service';
import { Select2OptionData } from 'ng2-select2';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { AttendanceImportModalComponent } from '../../attendance/attendance-import-modal/attendance-import-modal.component';

@Component({
  selector: 'app-attendance-import-review',
  templateUrl: './attendance-import-review.component.html',
  styleUrls: ['./attendance-import-review.component.css']
})
export class AttendanceImportReviewComponent implements OnInit{
  
	title: string;
	public id: any;
	public status: any;
	public name: string;
	public success_title;
	public success_message;
	public error_title;
	public error_message;
	public poststore: any;
	public statuses: any;
	url:any;
	action:any;
	validate=true;
	data=[];
	final_data = [];

	public branch: any;
	public branch_current : any;
	branch_value: any;
	
	invalid = 0;
	tableVal = false;
	loader = false;
	names =[];
	user_id:any;

	public ws_current : any;
	ws_value: Array<Select2OptionData>;
	public work_sched=[];
	value:any;
	public options2: Select2Options;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

  constructor(
   	private _common_service: CommonService,
   	private _atteservice: AttendanceService,
   	private daterangepickerOptions: DaterangepickerConfig,
	private _rt: Router,
	private modalService: DialogService,
	private _auth_service: AuthUserService
  ) {

	this.options2 = {
		dropdownAutoWidth: true
	}

  	this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
    		showDropdowns: true,
    		opens: "center"
	}; 

  	this.body.classList.add("skin-blue");
	this.body.classList.add("sidebar-mini");
  }

    ngOnInit() {

  		this.data = this._atteservice.getData();

    	if(this.data.length != 0){ 
	    	this.getBranch();         
	    	this.collectName();
	    	this.getWorkingSchedule();
    	}
  	}

  	collectName(){

  		this.tableVal = false;
		this.loader = true;
  		
	  	this.names = [];
	  	for (let i = 0; i < this.data.length; ++i) {
	  		let emp_id = this.data[i]['emp_id'];
	  		let name = this.data[i]['name'];
	  		this.names.push({emp_id,name});
	  	}

	  	for(let i = 0; i < this.names.length; i++) {
		    for(let j = i + 1; j < this.names.length; ) {
		        if(this.names[i]['emp_id'] == this.names[j]['emp_id'] && this.names[i]['name'] == this.names[j]['name']){
		            this.names.splice(j, 1);
		        }
		        else{
		            j++;
		        }
		    }    
		}


	  	this.arrangeData();

  	}

  	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

    arrangeData(){

    	let temp_data =[];
		let len = this.data.length;
		for (var i = 0; i < len; ++i) {
			let emp_id = this.data[i]['emp_id'];
			let name = this.data[i]['name'];
			let branch_id =  this.data[i]['branch_id'];
			// let date = '';
			let date = moment(this.data[i]['time_in']).format("YYYY-MM-DD");
			let start_time = moment(this.data[i]['time_in']).format("HH:mm:ss");
			let end_time = moment(this.data[i]['time_out']).format("HH:mm:ss");
			let y =  moment().format("YYYY-MM-DD");
			// let time_in = date + ' ' + start_time;
			// let time_out = date + ' ' + end_time;
			let time_in = moment(this.data[i]['time_in']).format("YYYY-MM-DD HH:mm:ss");
			let time_out = moment(this.data[i]['time_out']).format("YYYY-MM-DD HH:mm:ss");
			

			if (start_time == 'Invalid date' && end_time == 'Invalid date') {
				this.invalid++;
				this.invalid++;
				date = y;
				start_time = '';
				end_time = '';
				time_in = '';
				time_out = '';
			}
			else if (end_time == 'Invalid date'){
				this.invalid++;
				date = moment(this.data[i]['time_in']).format("YYYY-MM-DD");
				end_time = '';
				time_out = '';
			}
			else if (start_time == 'Invalid date'){
				this.invalid++;
				date = moment(this.data[i]['time_out']).format("YYYY-MM-DD");
				start_time = '';
				time_in = '';
			}
			else{
				date = moment(this.data[i]['time_in']).format("YYYY-MM-DD");
			}


			temp_data.push({emp_id,branch_id,date,start_time,end_time,time_in,time_out});
		}


		let dat = temp_data,
	    names = this.names,
	    show = true,
	    map = new Map,
	    result = names.map(({ emp_id, name}) => ({ name,show,details: map.set(emp_id, []).get(emp_id) }));

		dat.forEach(({emp_id,branch_id,date,start_time,end_time,time_in,time_out}) => map.get(emp_id).push({emp_id,branch_id,date,start_time,end_time,time_in,time_out}));

		this.getBranch();
		setTimeout(() => {
			this.final_data = result;
			this.tableVal = true;
			this.valid();
			this.loader = false;
      	}, 2000);




		// setTimeout(() => {
  //   		let len = this.final_data.length;
  //   		for(let i=0; i<len;i++){
  //   			let obj = this.final_data[i];
  //   			let val = new String(obj.branch_id);
  //   			// console.log('val: ',"#branch_" + val + " > .select2-hidden-accessible");
	 //  		   $("#branch_" + val + " > .select2-hidden-accessible").select2().select2('val',val);
  //   		}
	 //      }, 2500);
		
	}

	valid(){

		if(this.invalid > 0){
			this.validate = false;
		}
		else{
			this.validate = true;
		}
	}
 
	save() {

		if(this.validate == true){
			this.loader = true;
			this.tableVal = false;

	    	let model = this.final_data;

		    this._atteservice.attachAttendanceFiles(model)
		    .subscribe(
		      data => {
		        this.poststore = Array.from(data);
		        this.validate=false;
		        this.tableVal = false;
		        this.error_title = '';
				this.error_message = '';
				this.loader = false;
		        this.success_title = "Success!";
		        this.success_message = "Successfully Imported";
		        setTimeout(() => {
		        	this._rt.navigateByUrl('admin/attendance-list-default');
		      	}, 3000);
		      },
		      err => this.catchError(err)
		    );
		}
	}

	 private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data has incomplete details.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}


	getBranch(){
	      this._common_service.getBranch()
	      .subscribe(
	        data => {
	        this.branch = Array.from(data);
	        
	        this.branch_value = [];

	        // let id = 0;
        	// let text = 'Select Branch';
        	// this.branch.unshift({id,text});

	        // this.branch_current = this.branch_value;
	        
	        },
	        err => console.error(err)
	    );
	}
	changedBranch(datas: any,i:any,x:any) {

		if(datas.value != 0){
			this.final_data[i].details[x].branch_id = datas.value;
		}
	} 

	private selectedDate(value:any, i:any) {

       	let date = moment(value.start).format("YYYY-MM-DD");
       	this.final_data[i].date = date;
    }

	import(): void {

		this.modalService.addDialog(AttendanceImportModalComponent, {
            title:'Upload File',
            bio_val:false
        	}).subscribe((isConfirmed)=>{
                this.ngOnInit();
        });

	}

	filter(name){
		
		let len = this.final_data.length;

		for (let i = 0; i < len; ++i) {
			if (this.final_data[i].name.toLowerCase().match(name.toLowerCase())) {
				this.final_data[i].show = true;
			}else{
				this.final_data[i].show = false;
			}
		}
	}

	removeInList(i,x){

		let lent = this.final_data.length;

		this.final_data[i].details.splice(x,1);

		if (this.final_data[i].details.length == 0) { 
			this.final_data.splice(i,1);
		}
		if(this.final_data.length == 0){
			this.tableVal = false;
			this.validate = false;
			this.data = [];
			this.final_data =[];
		}else{
			this.validate = true;
		}

	}

	changeInputTime(event:any,type:any,i:any,x:any){

    	let time = event.target.value;
		let data = this.final_data;

		if (type == 'in') { 
			let date = moment(this.final_data[i].details[x].date).format('YYYY-MM-DD');
			this.final_data[i].details[x].start_time = time;
			this.final_data[i].details[x].time_in = date + ' ' + time;
		}
		else if(type == 'out'){
			let date = moment(this.final_data[i].details[x].date).format('YYYY-MM-DD');
			this.final_data[i].details[x].end_time = time;
			this.final_data[i].details[x].time_out = date + ' ' + time;
		}

		if(time == ''){
			this.invalid++;
			this.validate = false;
		}
		else{
			this.invalid--;
			this.valid();
		}

    }

    getWorkingSchedule(){
	      this._common_service.getWorkingScheduleWithoutBreak()
	      .subscribe(
	        data => {
	        this.work_sched = data;
			
	        this.ws_value = [];
	        this.ws_current = this.ws_value;

	        let id = 0;
	        let text = 'Suggested Schedule';

	        this.work_sched.unshift({id,text});

	        this.ws_value = this.value;
	        
	        },
	        err => this.catchError(err)

	    );
	}
	private changedWorkingSchedule(data: any,i:any,x:any) {

  		for (let y = 0; y < this.work_sched.length; ++y) {
  			if (this.work_sched[y].id == data.value) {
  				let date = moment(this.final_data[i].details[x].date).format('YYYY-MM-DD');
  				this.final_data[i].details[x].time_in = date + ' ' + this.work_sched[y].time_in;
				this.final_data[i].details[x].time_out = date + ' ' + this.work_sched[y].time_out;
				this.final_data[i].details[x].start_time = this.work_sched[y].time_in;
				this.final_data[i].details[x].end_time = this.work_sched[y].time_out;
  			}
  		}
    }

}
