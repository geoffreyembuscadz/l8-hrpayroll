var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { DialogService } from "ng2-bootstrap-modal";
import * as moment from 'moment';
import { Attendance } from '../../../model/attendance';
import { AttendanceService } from '../../../services/attendance.service';
import { CommonService } from '../../../services/common.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { AlertModalComponent } from '../../alert-modal/alert-modal.component';
import { ChecklistModalComponent } from '../../checklist-modal/checklist-modal.component';
var AttendanceCreateComponent = /** @class */ (function () {
    function AttendanceCreateComponent(_rt, _fb, _ar, _attendservice, daterangepickerOptions, _common_service, _auth_service, modalService) {
        this._rt = _rt;
        this._fb = _fb;
        this._ar = _ar;
        this._attendservice = _attendservice;
        this.daterangepickerOptions = daterangepickerOptions;
        this._common_service = _common_service;
        this._auth_service = _auth_service;
        this.modalService = modalService;
        this.attendance = new Attendance();
        this.employeecheckbox = [];
        this.employeesList = [];
        this.employee_name = [];
        this.date = [];
        this.dates = [];
        this.input_time = [];
        this.firststep = false;
        this.secondstep = false;
        this.thirdstep = false;
        this.checkAllValidation = false;
        this.sched = [];
        this.compVal = false;
        this.deptVal = false;
        this.branchFil = [];
        this.branch_current = 0;
        this.mainInput = {
            start: moment(),
            end: moment().add(1, 'days')
        };
        this.loader = false;
        this.tableVal = false;
        this.dayView = false;
        this.timeView = true;
        this.invalid = 0;
        this.withFixSched = true;
        this.holiday = [];
        this.dataStorage = [];
        this.work_sched = [];
        this.timeViewArrayId = [];
        this.timeViewArrayData = [];
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.searchForm = _fb.group({
            'branch_id': [null],
            'department': [null],
            'company': [null],
            'position_id': [null]
        });
        this.options = {
            dropdownAutoWidth: true
        };
    }
    AttendanceCreateComponent.prototype.ngAfterViewInit = function () {
    };
    AttendanceCreateComponent.prototype.ngOnInit = function () {
        this.dateOption();
        this.getDepartment();
        this.getPosition();
        this.getCompany();
    };
    AttendanceCreateComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false,
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };
    };
    AttendanceCreateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    AttendanceCreateComponent.prototype.getCompany = function () {
        var _this = this;
        this._common_service.getCompany()
            .subscribe(function (data) {
            _this.company = Array.from(data);
            _this.company_value = [];
            _this.company_current = _this.company_value;
            var id = 0;
            var text = 'Select Company';
            _this.company.unshift({ id: id, text: text });
            _this.company_value = _this.value;
            _this.loader = false;
        }, function (err) { return _this.catchError(err); });
    };
    AttendanceCreateComponent.prototype.changedCompany = function (data) {
        this.company_current = data.value;
        var len = this.company_current.length;
        if (len >= 1 && this.company_current != 0) {
            this.compVal = true;
            this.getBranchFilter();
        }
        else {
            this.compVal = false;
        }
        this.firstStepVal();
    };
    AttendanceCreateComponent.prototype.getDepartment = function () {
        var _this = this;
        this._common_service.getDepartment()
            .subscribe(function (data) {
            _this.department = Array.from(data);
            _this.department_value = [];
            _this.department_current = _this.department_value;
            var id = 0;
            var text = 'Select Department';
            _this.department.unshift({ id: id, text: text });
            _this.department_value = _this.value;
        }, function (err) { return _this.catchError(err); });
    };
    AttendanceCreateComponent.prototype.changedDepartment = function (data) {
        this.department_current = data.value;
        var len = this.department_current.length;
        if (len >= 1 && this.department_current != 0) {
            this.deptVal = true;
        }
        else {
            this.deptVal = false;
        }
        this.firstStepVal();
    };
    AttendanceCreateComponent.prototype.getPosition = function () {
        var _this = this;
        this._common_service.getPosition()
            .subscribe(function (data) {
            _this.position = data;
            _this.position_value = [];
            var id = 0;
            var text = 'Select Position';
            _this.position.unshift({ id: id, text: text });
            _this.position_value = _this.value;
            _this.position_current = _this.position_value;
        }, function (err) { return console.error(err); });
    };
    AttendanceCreateComponent.prototype.changedPosition = function (data) {
        this.position_current = data.value;
    };
    AttendanceCreateComponent.prototype.getBranchFilter = function () {
        var _this = this;
        var id = this.company_current;
        this._common_service.getBranchByCompany(id)
            .subscribe(function (data) {
            _this.branchFil = data;
            _this.branch_value = [];
            var id = 0;
            var text = 'Select Branch';
            _this.branchFil.unshift({ id: id, text: text });
            _this.branch_value = _this.value;
            _this.branch_current = 0;
        }, function (err) { return console.error(err); });
    };
    AttendanceCreateComponent.prototype.changedBranchFilter = function (data) {
        this.branch_current = data.value;
    };
    AttendanceCreateComponent.prototype.firstStepVal = function () {
        if (this.compVal == true || this.deptVal == true) {
            this.firststep = true;
        }
        else {
            this.firststep = false;
        }
    };
    //date range picker
    AttendanceCreateComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_date = moment(dateInput.start).format("YYYY-MM-DD");
        this.end_date = moment(dateInput.end).format("YYYY-MM-DD");
    };
    //step 1
    AttendanceCreateComponent.prototype.FirstStep = function () {
        var _this = this;
        this.secondstep = false;
        this.loader = true;
        this.sched = [];
        this.dataStorage = [];
        this.tableVal = false;
        this.searchForm.value.department = this.department_current;
        this.searchForm.value.company = this.company_current;
        this.searchForm.value.branch_id = this.branch_current;
        this.searchForm.value.position_id = this.position_current;
        var model = this.searchForm.value;
        this._attendservice.getEmployeeData(model).
            subscribe(function (data) {
            _this.employeeData = Array.from(data);
            _this.checkAllValidation = false;
            var val = false;
            var len = _this.employeeData.length;
            var show = true;
            if (_this.employeesList != []) {
                _this.employeesList = [];
            }
            for (var i = 0; i < len; ++i) {
                var name_1 = _this.employeeData[i].name;
                var id = _this.employeeData[i].id;
                var branch_id = _this.employeeData[i].branch_id;
                _this.employeesList.push({ id: id, name: name_1, val: val, branch_id: branch_id, show: show });
            }
            _this.loader = false;
        }, function (err) { return _this.catchError(err); });
        if (this.date != []) {
            this.date = [];
            this.dates = [];
        }
        this.computeDays();
    };
    //compute days
    AttendanceCreateComponent.prototype.computeDays = function () {
        //if user didn't choose any date
        if (this.start_date == null && this.end_date == null) {
            this.start_date = this.mainInput.start;
            this.end_date = this.mainInput.end;
        }
        var start = this.start_date;
        var end = this.end_date;
        var from = moment(this.start_date, 'YYYY-MM-DD');
        var to = moment(this.end_date, 'YYYY-MM-DD');
        /* using diff */
        this.duration = to.diff(from, 'days');
        var currDate = from.clone().startOf('day');
        var lastDate = to.clone().startOf('day');
        this.dates.push(currDate.toDate());
        while (currDate.add(1, 'days').diff(lastDate) < 0) {
            this.dates.push(currDate.clone().toDate());
        }
        if (start != end) {
            this.dates.push(lastDate.toDate());
        }
    };
    AttendanceCreateComponent.prototype.checkAll = function () {
        if (this.checkAllValidation == false) {
            var leng = this.employeesList.length;
            for (var i = 0; i < leng; i++) {
                this.employeesList[i].val = true;
            }
            this.checkAllValidation = true;
            this.secondstep = true;
        }
        else {
            var len = this.employeesList.length;
            for (var i = 0; i < len; i++) {
                this.employeesList[i].val = false;
            }
            this.checkAllValidation = false;
            this.secondstep = false;
        }
    };
    // choice employee in checkbox
    AttendanceCreateComponent.prototype.EmployeeCheckbox = function (index) {
        if (this.employeesList[index].val == false) {
            this.employeesList[index].val = true;
        }
        else {
            this.employeesList[index].val = false;
            this.checkAllValidation = false;
        }
        var len = this.employeesList.length;
        var i = 0;
        for (var x = 0; x < len; ++x) {
            if (this.employeesList[x].val == true) {
                i++;
            }
        }
        if (i > 0) {
            this.secondstep = true;
        }
        else {
            this.secondstep = false;
        }
    };
    AttendanceCreateComponent.prototype.secondStepOption = function (event) {
        if (event.target.checked == false) {
            this.dayView = true;
            this.timeView = false;
        }
        else {
            this.timeView = true;
            this.dayView = false;
        }
    };
    //step two
    AttendanceCreateComponent.prototype.SecondStep = function () {
        var _this = this;
        this.loader = true;
        this.tableVal = false;
        this.backToTop();
        if (this.date != []) {
            this.date = [];
        }
        var n = {};
        for (var i = 0; i < this.dates.length; i++) {
            if (!n[this.dates[i]]) {
                n[this.dates[i]] = true;
                this.date.push(moment(this.dates[i]).format("YYYY-MM-DD"));
            }
        }
        var id = [];
        var len = this.employeesList.length;
        var temp = [];
        for (var x = 0; x < len; x++) {
            if (this.employeesList[x].val == true) {
                id.push(this.employeesList[x].id);
                temp.push(this.employeesList[x]);
            }
        }
        if (this.timeView == true) {
            // this.timeViewArrayId = [];
            // this.timeViewArrayData = [];
            // let index = 5;
            // if (this.date.length > 8 && id.length > 10) {
            // 	index = 3;
            // }
            // if (id.length > 5) {
            // 	this.timeViewArrayId = this.arrangeArray(id, index)
            // 	this.timeViewArrayData = this.arrangeArray(temp, index)
            // }else{
            // 	this.timeViewArrayId.push(id);
            // 	this.timeViewArrayData.push(temp);
            // }
            // let temp_id = this.timeViewArrayId[0];
            // let temp_data = this.timeViewArrayData[0];
            this.timeViewAttend(id, temp);
        }
        if (this.dayView == true) {
            this.checkHoliday();
            setTimeout(function () {
                _this.dayViewAttend(id, temp);
            }, 2000);
            // setTimeout(() => {
            //   		let len = this.dataStorage.length;
            //   		for(let i=0; i<len;i++){
            //   			let obj = this.dataStorage[i];
            //   			let val = new String(obj.branch_id);
            //   			// console.log('val: ',"#branch_" + val + " > .select2-hidden-accessible");
            //  		   $("#branch_" + val + " > .select2-hidden-accessible").select2().select2('val',val);
            //   		}
            //      }, 2500);
        }
    };
    AttendanceCreateComponent.prototype.paginate = function (x) {
        this.loader = true;
        this.tableVal = false;
        var temp_id = this.timeViewArrayId[x];
        var temp_data = this.timeViewArrayData[x];
        this.timeViewAttend(temp_id, temp_data);
        var pagi = 'pagi' + x;
        document.getElementById(pagi).setAttribute("class", "page-item active");
    };
    AttendanceCreateComponent.prototype.arrangeArray = function (myArray, chunk_size) {
        var index = 0;
        var arrayLength = myArray.length;
        var tempArray = [];
        for (index = 0; index < arrayLength; index += chunk_size) {
            var myChunk = myArray.slice(index, index + chunk_size);
            // Do something if you want with the group
            tempArray.push(myChunk);
        }
        return tempArray;
    };
    AttendanceCreateComponent.prototype.timeViewAttend = function (id, tem) {
        var _this = this;
        var date = this.date;
        if (this.sched != null || this.sched != []) {
            this.sched = [];
        }
        var model = [];
        model.push({ id: id, date: date });
        this._attendservice.getSchedule(model).
            subscribe(function (data) {
            var temp_data = data;
            var dat = temp_data, names = tem, show = true, map = new Map, result = names.map(function (_a) {
                var id = _a.id, name = _a.name;
                return ({ name: name, show: show, details: map.set(id, []).get(id) });
            });
            dat.forEach(function (_a) {
                var branch_id = _a.branch_id, date = _a.date, days = _a.days, end_time = _a.end_time, id = _a.id, late = _a.late, start_time = _a.start_time, time_in = _a.time_in, time_out = _a.time_out, undertime = _a.undertime, break_start = _a.break_start, break_end = _a.break_end, lunch_in = _a.lunch_in, lunch_out = _a.lunch_out, sa = _a.sa;
                return map.get(id).push({ branch_id: branch_id, date: date, days: days, end_time: end_time, id: id, late: late, start_time: start_time, time_in: time_in, time_out: time_out, undertime: undertime, break_start: break_start, break_end: break_end, lunch_in: lunch_in, lunch_out: lunch_out, sa: sa });
            });
            _this.sched = result;
            _this.loader = false;
            _this.tableVal = true;
            ///update this
            if (_this.sched != null || _this.sched != []) {
                _this.thirdstep = true;
            }
            else {
                _this.error_title = 'Error 500';
                _this.error_message = 'Please check the working schedule of Employee';
            }
        }, function (err) { return console.error(err); });
        // setTimeout(() => {
        //   		let len = this.sched.length;
        //   		for(let i=0; i<len;i++){
        //   			let obj = this.sched[i];
        //   			let val = new String(obj.branch_id);
        //   			// console.log('val: ',"#branch_" + val + " > .select2-hidden-accessible");
        //  		   $("#branch_" + val + " > .select2-hidden-accessible").select2().select2('val',val);
        //   		}
        //      }, 2500);
        this.getBranch();
        this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
            showDropdowns: true,
            opens: "center"
        };
        setTimeout(function () {
            _this.getWorkingSchedule();
        }, 2000);
    };
    AttendanceCreateComponent.prototype.dayViewAttend = function (id, temp) {
        var _this = this;
        this._attendservice.getEmployeeRate(id).
            subscribe(function (data) {
            var user = data;
            _this.dataStorage = [];
            var days = 0;
            var undertime = 0.0;
            var overtime = 0.0;
            var i = 0;
            var date = _this.date;
            var date2 = [];
            var stime = "00:00:00";
            var etime = "23:59:00";
            var s = moment(_this.start_date).format('YYYY-MM-DD');
            var e = moment(_this.end_date).format('YYYY-MM-DD');
            var start = moment(s + ' ' + stime);
            var end = moment(e + ' ' + etime);
            s = moment(start).format('YYYY-MM-DD HH:mm:ss');
            e = moment(end).format('YYYY-MM-DD HH:mm:ss');
            date2.push(s, e);
            var show = true;
            var holiday = _this.holiday2;
            var night_diff = 0;
            var restday = 0;
            var temp_for_type1 = [];
            var temp_for_type2 = '0';
            var temp_for_id1 = [];
            var temp_for_id2 = '0';
            for (var i_1 = 0; i_1 < _this.holiday2.length; ++i_1) {
                temp_for_type1.push(_this.holiday2[i_1].type);
                temp_for_type2 = temp_for_type1.join();
                temp_for_id1.push(_this.holiday2[i_1].id);
                temp_for_id2 = temp_for_id1.join();
            }
            var holiday2 = temp_for_type2;
            var holiday_ids = temp_for_id2;
            for (var i_2 = 0; i_2 < user.length; ++i_2) {
                for (var x = 0; x < temp.length; ++x) {
                    if (user[i_2].employee_id == temp[x].id) {
                        var emp_id = temp[x].id;
                        var name_2 = temp[x].name;
                        var branch_id = temp[x].branch_id;
                        var daily_rate = user[i_2].daily_rate;
                        var allowance = user[i_2].allowance_amount;
                        var e_cola = user[i_2].e_cola;
                        _this.dataStorage.push({ i: i_2, emp_id: emp_id, name: name_2, branch_id: branch_id, days: days, undertime: undertime, overtime: overtime, date: date, date2: date2, show: show, daily_rate: daily_rate, allowance: allowance, e_cola: e_cola, holiday: holiday, holiday2: holiday2, night_diff: night_diff, restday: restday, holiday_ids: holiday_ids });
                    }
                }
            }
            _this.sortName();
            _this.loader = false;
            _this.tableVal = true;
            _this.getBranch();
            _this.thirdstep = true;
        }, function (err) { return console.error(err); });
    };
    AttendanceCreateComponent.prototype.sortName = function () {
        this.dataStorage.sort(function (a, b) {
            var nameA = a.name.toLowerCase(), nameB = b.name.toLowerCase();
            if (nameA < nameB)
                return -1;
            if (nameA > nameB)
                return 1;
            return 0;
        });
    };
    AttendanceCreateComponent.prototype.checkHoliday = function () {
        var _this = this;
        this.holiday = [];
        var holi = [];
        this._common_service.checkDateHoliday(this.date).
            subscribe(function (data) {
            holi = data;
            if (holi.length != 0) {
                _this.holiday = holi;
                var temp = [];
                _this.holiday2 = [];
                for (var i = 0; i < holi.length; ++i) {
                    var type = holi[i].type;
                    var date = holi[i].date;
                    var id = holi[i].id;
                    _this.holiday2.push({ date: date, type: type, id: id });
                }
            }
            else {
                _this.holiday = null;
                _this.holiday2 = [];
            }
        }, function (err) { return console.error(err); });
    };
    AttendanceCreateComponent.prototype.secondStep = function (type) {
        if (type == 'time') {
            this.timeView = true;
            this.dayView = false;
        }
        else if (type == 'day') {
            this.dayView = true;
            this.timeView = false;
        }
        this.SecondStep();
    };
    AttendanceCreateComponent.prototype.changeInputDay = function (i, type, event) {
        var value = event.target.value;
        var num = this.date.length;
        if (value == '') {
            value = 0;
        }
        if (type == 'day') {
            this.dataStorage[i].days = value;
            if (value <= num && value >= 0) {
                this.dataStorage[i].days = value;
            }
            else {
                this.dataStorage[i].days = num;
            }
        }
        else if (type == 'ut') {
            this.dataStorage[i].undertime = value;
        }
        else if (type == 'ot') {
            this.dataStorage[i].overtime = value;
        }
        else if (type == 'allow') {
            this.dataStorage[i].allowance = value;
        }
        else if (type == 'dr') {
            this.dataStorage[i].daily_rate = value;
        }
        else if (type == 'e_cola') {
            this.dataStorage[i].e_cola = value;
        }
        else if (type == 'night_diff') {
            this.dataStorage[i].night_diff = value;
        }
        else if (type == 'restday') {
            this.dataStorage[i].restday = value;
        }
    };
    AttendanceCreateComponent.prototype.changeInputTime = function (event, type, i, x) {
        var time = moment("2017-11-29 " + event.target.value).format("HH:mm:ss");
        var data = this.sched;
        if (type == 'in') {
            var date = moment(this.sched[i].details[x].date).format('YYYY-MM-DD');
            this.sched[i].details[x].start_time = time;
            this.sched[i].details[x].time_in = date + ' ' + time;
        }
        else if (type == 'out') {
            var date = moment(this.sched[i].details[x].date).format('YYYY-MM-DD');
            this.sched[i].details[x].end_time = time;
            this.sched[i].details[x].time_out = date + ' ' + time;
        }
        else if (type == 'lunch_in') {
            var date = moment(this.sched[i].details[x].date).format('YYYY-MM-DD');
            this.sched[i].details[x].break_start = time;
            this.sched[i].details[x].lunch_in = date + ' ' + time;
        }
        else if (type == 'lunch_out') {
            var date = moment(this.sched[i].details[x].date).format('YYYY-MM-DD');
            this.sched[i].details[x].break_end = time;
            this.sched[i].details[x].lunch_out = date + ' ' + time;
        }
        if (time == '') {
            this.thirdstep = false;
        }
        else {
            this.thirdstep = true;
        }
        if (time == '') {
            this.invalid++;
            this.thirdstep = false;
        }
        else {
            this.invalid--;
            this.valid();
        }
    };
    AttendanceCreateComponent.prototype.valid = function () {
        if (this.invalid > 0) {
            this.thirdstep = false;
        }
        else {
            this.thirdstep = true;
        }
    };
    AttendanceCreateComponent.prototype.removeInListTimeView = function (i, x) {
        this.sched[i].details.splice(x, 1);
        if (this.sched[i].details.length == 0) {
            this.sched.splice(i, 1);
        }
        if (this.sched.length == 0) {
            this.thirdstep = false;
            this.tableVal = false;
        }
    };
    AttendanceCreateComponent.prototype.removeInListDayView = function (x) {
        this.dataStorage.splice(x, 1);
        if (this.dataStorage.length == 0) {
            this.thirdstep = false;
            this.tableVal = false;
        }
    };
    AttendanceCreateComponent.prototype.LastStep = function () {
        var _this = this;
        this.loader = true;
        this.tableVal = false;
        this.backToTop();
        if (this.timeView == true) {
            var data_model = this.sched;
            this._attendservice.createTimeAttendance(data_model).subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.loader = false;
                _this.success_title = "Success";
                _this.success_message = "A new user record was successfully added.";
                setTimeout(function () {
                    _this._rt.navigateByUrl('admin/attendance-list-default');
                }, 3000);
            }, function (err) { return _this.catchError(err); });
        }
        if (this.dayView == true) {
            var data_model = this.dataStorage;
            var len = this.company.length;
            var def = 0;
            for (var i = 1; i < len; ++i) {
                if (this.company[i].id == this.company_current) {
                    def = this.company[i].default;
                }
            }
            if (def == 1) {
                this._attendservice.createDayAttendanceDefault(data_model).subscribe(function (data) {
                    _this.poststore = data;
                    _this.cacheError();
                }, function (err) { return _this.catchError(err); });
            }
            else {
                this._attendservice.createDayAttendance(data_model).subscribe(function (data) {
                    _this.poststore = data;
                    _this.cacheError();
                }, function (err) { return _this.catchError(err); });
            }
        }
    };
    AttendanceCreateComponent.prototype.cacheError = function () {
        var _this = this;
        if (this.poststore.status_code == 422) {
            this.loader = false;
            this.tableVal = true;
            var disposable = this.modalService.addDialog(AlertModalComponent, {
                title: 'Unprocessable Entity 422',
                message: this.poststore.message,
                data: this.poststore.data,
                attValEmp: true,
                model: this.poststore.model
            }).subscribe(function (isConfirmed) {
                _this.loader = false;
                _this.tableVal = true;
                document.getElementById('back').click();
            });
        }
        else {
            this.loader = false;
            this.success_title = "Success";
            this.success_message = "Attendance was successfully added.";
            setTimeout(function () {
                _this._rt.navigateByUrl('admin/attendance-list');
            }, 3000);
        }
    };
    AttendanceCreateComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    AttendanceCreateComponent.prototype.getBranch = function () {
        var _this = this;
        var id = this.company_current;
        this._common_service.getBranchByCompany(id)
            .subscribe(function (data) {
            _this.branch = data;
        }, function (err) { return console.error(err); });
    };
    AttendanceCreateComponent.prototype.changedBranch = function (datas, i, x) {
        if (datas.value != 0) {
            this.sched[i].details[x].branch_id = datas.value;
        }
    };
    AttendanceCreateComponent.prototype.changedBran = function (data, i) {
        if (data.value != 0) {
            this.dataStorage[i].branch_id = data.value;
        }
    };
    AttendanceCreateComponent.prototype.filterEmpInCheckBox = function (name) {
        var len = this.employeesList.length;
        for (var i = 0; i < len; ++i) {
            if (this.employeesList[i].name.toLowerCase().match(name.toLowerCase())) {
                this.employeesList[i].show = true;
            }
            else {
                this.employeesList[i].show = false;
            }
        }
    };
    AttendanceCreateComponent.prototype.filterEmpInTimeView = function (name) {
        var len = this.sched.length;
        for (var i = 0; i < len; ++i) {
            if (this.sched[i].name.toLowerCase().match(name.toLowerCase())) {
                this.sched[i].show = true;
            }
            else {
                this.sched[i].show = false;
            }
        }
    };
    AttendanceCreateComponent.prototype.filterEmpInDayView = function (name) {
        var len = this.dataStorage.length;
        for (var i = 0; i < len; ++i) {
            if (this.dataStorage[i].name.toLowerCase().match(name.toLowerCase())) {
                this.dataStorage[i].show = true;
            }
            else {
                this.dataStorage[i].show = false;
            }
        }
    };
    AttendanceCreateComponent.prototype.fixSched = function (e) {
        this.withFixSched = e.target.checked;
        if (this.withFixSched == false) {
            this.getWorkingSchedule();
        }
    };
    AttendanceCreateComponent.prototype.getWorkingSchedule = function () {
        var _this = this;
        var company = this.company_current;
        this._common_service.getWorkingSchedule(company)
            .subscribe(function (data) {
            _this.work_sched = data;
            _this.ws_value = [];
            _this.ws_current = _this.ws_value;
            var id = 0;
            var text = 'Suggested Schedule';
            _this.work_sched.unshift({ id: id, text: text });
            _this.ws_value = _this.value;
        }, function (err) { return _this.catchError(err); });
    };
    AttendanceCreateComponent.prototype.changedWorkingSchedule = function (data, i, x) {
        for (var y = 0; y < this.work_sched.length; ++y) {
            if (this.work_sched[y].id == data.value) {
                this.sched[i].details[x].start_time = this.work_sched[y].time_in;
                this.sched[i].details[x].end_time = this.work_sched[y].time_out;
                this.sched[i].details[x].time_in = this.sched[i].details[x].date + ' ' + this.work_sched[y].time_in;
                this.sched[i].details[x].time_out = this.sched[i].details[x].date + ' ' + this.work_sched[y].time_out;
                this.sched[i].details[x].sa = data.value;
                this.sched[i].details[x].break_start = this.work_sched[y].break_start;
                this.sched[i].details[x].break_end = this.work_sched[y].break_end;
                this.sched[i].details[x].lunch_in = this.sched[i].details[x].date + ' ' + this.work_sched[y].break_start;
                this.sched[i].details[x].lunch_out = this.sched[i].details[x].date + ' ' + this.work_sched[y].break_end;
            }
        }
    };
    AttendanceCreateComponent.prototype.checklistOfHoliday = function (x) {
        var _this = this;
        var holiday = this.dataStorage[x].holiday;
        var disposable = this.modalService.addDialog(ChecklistModalComponent, {
            title: 'Holiday List',
            tableVal: true,
            attend: true,
            data: this.holiday,
            holiday: holiday,
            clearance: false,
            onboarding: false,
            attendModal: false
        }).subscribe(function (message) {
            if (message != 1 || message == []) {
                var temp = [];
                var temp2 = '0';
                for (var i = 0; i < message.length; ++i) {
                    temp.push(message[i].type);
                    temp2 = temp.join();
                }
                _this.dataStorage[x].holiday2 = temp2;
                _this.dataStorage[x].holiday = message;
            }
        });
    };
    AttendanceCreateComponent.prototype.backToTop = function () {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    };
    AttendanceCreateComponent = __decorate([
        Component({
            selector: 'app-attendance-create',
            templateUrl: './attendance-create.component.html',
            styleUrls: ['./attendance-create.component.css']
        }),
        __metadata("design:paramtypes", [Router,
            FormBuilder,
            ActivatedRoute,
            AttendanceService,
            DaterangepickerConfig,
            CommonService,
            AuthUserService,
            DialogService])
    ], AttendanceCreateComponent);
    return AttendanceCreateComponent;
}());
export { AttendanceCreateComponent };
//# sourceMappingURL=attendance-create.component.js.map