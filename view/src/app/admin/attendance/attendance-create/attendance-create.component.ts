import 'rxjs/add/operator/catch'
import { Component, OnInit } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Select2OptionData } from 'ng2-select2';
import { TimepickerModule } from 'ngx-bootstrap';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { Attendance } from '../../../model/attendance';
import { AttendanceService } from '../../../services/attendance.service';
import { CommonService } from '../../../services/common.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { AlertModalComponent } from '../../alert-modal/alert-modal.component';
import { ChecklistModalComponent } from '../../checklist-modal/checklist-modal.component';

@Component({
  selector: 'app-attendance-create',
  templateUrl: './attendance-create.component.html',
  styleUrls: ['./attendance-create.component.css']
})
export class AttendanceCreateComponent implements OnInit {

 	public error_title: string
	public error_message: string;
	public success_title;
	public success_message;
	public poststore: any;
	public employeeData: any;
	public attendance = new Attendance();
	public employee: any;
	duration : number;
	employeecheckbox=[];
	public employees: any;
	employeesList =[];
	employee_name =[];
	date = [];
	dates = [];
	public input_time=[];
	range:number;
	public start_date:any;
	public end_date:any;
	public department_current : any;
	department_value: Array<Select2OptionData>;
	public department: any;

	public company: any;
	public company_current : any;
	company_value: Array<Select2OptionData>;
	user_id:any;
	firststep =false;
	secondstep = false;
	thirdstep = false;
	value:any;
	checkAllValidation=false;
	sched=[];
	compVal = false;
	deptVal = false;

	public branch: any;
	branchFil = [];
	public branch_current=0;
	branch_value: any;

	public position: any;
	public position_current : any;
	position_value: any;

    public mainInput = {
        start: moment(),
        end: moment().add(1,'days')
    }

    loader = false;
    tableVal = false;

    role_id:any;
    dayView = false;
    timeView = true;
    searchForm:any;
    invalid = 0;
    withFixSched = true;
    holiday = [];
    holiday2:any;
    dataStorage = [];
    public options: Select2Options;

    public ws_current : any;
	ws_value: Array<Select2OptionData>;
	public work_sched=[];
	timeViewArrayId = [];
	timeViewArrayData = [];

    bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];
	

	constructor(
		private _rt: Router,
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute, 
		private _attendservice: AttendanceService, 
		private daterangepickerOptions: DaterangepickerConfig,
		private _common_service: CommonService,
		private _auth_service: AuthUserService,
		private modalService: DialogService,
		
	){

		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	    this.searchForm = _fb.group({
			'branch_id': 	[null],
	    	'department': 	[null],
			'company': 		[null],
			'position_id': 	[null]
   		});

		this.options = {
			dropdownAutoWidth: true
		}

	}

	ngAfterViewInit(){
	}

	ngOnInit(){
		this.dateOption();
		this.getDepartment();
		this.getPosition();
		this.getCompany();
	}

	dateOption(){
    	this.daterangepickerOptions.settings = {
	        singleDatePicker: false,
	        locale: { format: 'MM/DD/YYYY' },
	        alwaysShowCalendars: true,
	        ranges: {
	           'Today': [moment(), moment()],
	           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	           'This Month': [moment().startOf('month'), moment().endOf('month')],
	           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	        }
		};
    }

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	getCompany(){
	      this._common_service.getCompany()
	      .subscribe(
	        data => {
	        this.company = Array.from(data);
	        this.company_value = [];
	        this.company_current = this.company_value;

	        let id = 0;
	        let text = 'Select Company';

	        this.company.unshift({id,text});

	        this.company_value = this.value;

	        this.loader = false;
	        
	        },
	        err => this.catchError(err)
	    );
	}
  	changedCompany(data: any) {

      this.company_current = data.value;
      let len = this.company_current.length;

      if(len >= 1 && this.company_current != 0){
      	this.compVal=true;
      	this.getBranchFilter();
      }
      else{
      	this.compVal=false;
      }
      this.firstStepVal();
    } 

	getDepartment(){
	      this._common_service.getDepartment()
	      .subscribe(
	        data => {
	        this.department = Array.from(data);
	        this.department_value = [];
	        this.department_current = this.department_value;

	        let id = 0;
	        let text = 'Select Department';

	        this.department.unshift({id,text});

	        this.department_value = this.value;
	        
	        },
	        err => this.catchError(err)

	    );
	}
	changedDepartment(data: any) {
      this.department_current = data.value;
      let len = this.department_current.length;
      if(len >= 1 && this.department_current != 0){
      	this.deptVal=true;
      }
      else{
      	this.deptVal=false;
      }
      this.firstStepVal();
    }

    getPosition(){
	      this._common_service.getPosition()
	      .subscribe(
	        data => {
	        this.position = data;
	        this.position_value = [];

	        let id = 0;
        	let text = 'Select Position';
        	this.position.unshift({id,text});

        	this.position_value = this.value;
	        
	        this.position_current = this.position_value;

	        },
	        err => console.error(err)
	    );
	}
	changedPosition(data) {

		this.position_current = data.value;
	}

	getBranchFilter(){
	      let id = this.company_current;
	      this._common_service.getBranchByCompany(id)
	      .subscribe(
	        data => {
	        this.branchFil = data;
	        this.branch_value = [];

	        let id = 0;
        	let text = 'Select Branch';
        	this.branchFil.unshift({id,text});

        	this.branch_value = this.value;
	        this.branch_current = 0;
	        
	        },
	        err => console.error(err)
	    );
	}

	changedBranchFilter(data: any) {

		this.branch_current = data.value;
	}

    firstStepVal(){
    	if (this.compVal == true || this.deptVal == true) { 
    		this.firststep=true;
    	} else {
    		this.firststep=false;
    	}
    }

    //date range picker
	private selectedDate(value: any, dateInput: any) {
       	
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_date = moment(dateInput.start).format("YYYY-MM-DD");
       	this.end_date = moment(dateInput.end).format("YYYY-MM-DD");
    }

    
	//step 1
	public FirstStep() {

		this.secondstep = false;
		this.loader = true;
		this.sched = [];
		this.dataStorage = [];
	    this.tableVal = false;

		this.searchForm.value.department = this.department_current;
		this.searchForm.value.company = this.company_current;
		this.searchForm.value.branch_id = this.branch_current;
		this.searchForm.value.position_id = this.position_current;

		let model = this.searchForm.value;
		
		this._attendservice.getEmployeeData(model).
		subscribe(
			data => {
				this.employeeData = Array.from(data);

				this.checkAllValidation = false;
				let val = false;
				let len = this.employeeData.length;
				let show = true;
				if(this.employeesList != []){
					this.employeesList = [];
				}
				for (let i = 0; i < len; ++i) {
					let name = this.employeeData[i].name;
					let id =  this.employeeData[i].id;
					let branch_id = this.employeeData[i].branch_id;
					this.employeesList.push({id,name,val,branch_id,show});
				}

				this.loader = false;
			},
			err => this.catchError(err)
		);

		if(this.date != []){
       		this.date = [];
       		this.dates = [];
       	}
		
		this.computeDays();

	}

	//compute days
	computeDays(){

		//if user didn't choose any date
		if(this.start_date == null && this.end_date == null){
			
			this.start_date = this.mainInput.start;
			this.end_date = this.mainInput.end;
		}

		let start = this.start_date;
		let end = this.end_date;
		let from = moment(this.start_date, 'YYYY-MM-DD');
		let to = moment(this.end_date, 'YYYY-MM-DD');

		/* using diff */
		this.duration = to.diff(from, 'days');

	    let currDate = from.clone().startOf('day');
	    let lastDate = to.clone().startOf('day');

	   	this.dates.push(currDate.toDate());

	    while(currDate.add(1 ,'days').diff(lastDate) < 0) {
	        this.dates.push(currDate.clone().toDate());
	    }

	    if(start != end)  {
	    	this.dates.push(lastDate.toDate());
	    }
	    
	}

	checkAll(){

		if(this.checkAllValidation == false){
			let leng = this.employeesList.length;
			for (let i = 0; i < leng; i++) {
				this.employeesList[i].val = true;
			}

			this.checkAllValidation = true;
			this.secondstep = true;
		}
		else{
			let len = this.employeesList.length;
			for (let i = 0; i < len; i++) {
				this.employeesList[i].val = false;
			}
			this.checkAllValidation = false;
			this.secondstep = false;
		}

	}
	
	// choice employee in checkbox
	EmployeeCheckbox(index:any){


		if(this.employeesList[index].val == false){
			this.employeesList[index].val = true;
			
		}
		else{
			this.employeesList[index].val = false;
			this.checkAllValidation = false;
		}

		let len = this.employeesList.length;
		let i = 0;
		for (let x = 0; x < len; ++x) {
			
			if(this.employeesList[x].val == true){
				i++;
			}
		}

		if(i > 0){
			this.secondstep = true;
		}
		else{
			this.secondstep = false;
		}
	}

	secondStepOption(event){

		if(event.target.checked == false){
			this.dayView = true;
			this.timeView = false;
		}else{
			this.timeView = true;
			this.dayView = false;
		}
	}

	//step two
	public SecondStep() {

		this.loader = true;
		this.tableVal = false;
		
		this.backToTop();

		if (this.date != []) {
			this.date = [];
		}

		let n = {}
		for(let i = 0; i < this.dates.length; i++) 
		{
			if (!n[this.dates[i]]) 
			{
				n[this.dates[i]] = true; 
				this.date.push(moment(this.dates[i]).format("YYYY-MM-DD")); 
			}
		} 

		let id = [];

		let len = this.employeesList.length;
		let temp = [];

		for (let x = 0; x < len; x++) {
			if(this.employeesList[x].val == true){
				id.push(this.employeesList[x].id); 
				temp.push(this.employeesList[x]); 
			}
		}

		if(this.timeView == true){

			// this.timeViewArrayId = [];
			// this.timeViewArrayData = [];
			// let index = 5;

			// if (this.date.length > 8 && id.length > 10) {
			// 	index = 3;
			// }

			// if (id.length > 5) {
			// 	this.timeViewArrayId = this.arrangeArray(id, index)
			// 	this.timeViewArrayData = this.arrangeArray(temp, index)
			// }else{
			// 	this.timeViewArrayId.push(id);
			// 	this.timeViewArrayData.push(temp);
			// }

			// let temp_id = this.timeViewArrayId[0];
			// let temp_data = this.timeViewArrayData[0];
			this.timeViewAttend(id,temp);

		}

		if(this.dayView == true){
			this.checkHoliday();

			setTimeout(() => {
				this.dayViewAttend(id,temp);
			}, 2000);

			
			// setTimeout(() => {
	  //   		let len = this.dataStorage.length;
	  //   		for(let i=0; i<len;i++){
	  //   			let obj = this.dataStorage[i];
	  //   			let val = new String(obj.branch_id);
	  //   			// console.log('val: ',"#branch_" + val + " > .select2-hidden-accessible");
		 //  		   $("#branch_" + val + " > .select2-hidden-accessible").select2().select2('val',val);
	  //   		}
		 //      }, 2500);
			
		}
	}



	paginate(x){
		this.loader = true;
		this.tableVal = false;
		let temp_id = this.timeViewArrayId[x];
		let temp_data = this.timeViewArrayData[x];
		this.timeViewAttend(temp_id,temp_data);

		let pagi = 'pagi'+ x;
		document.getElementById(pagi).setAttribute("class", "page-item active");
	}

	arrangeArray(myArray, chunk_size){
	    let index = 0;
	    let arrayLength = myArray.length;
	    let tempArray = [];
	    
	    for (index = 0; index < arrayLength; index += chunk_size) {
	        let myChunk = myArray.slice(index, index+chunk_size);
	        // Do something if you want with the group
	        tempArray.push(myChunk);
	    }

	    return tempArray;
	}

	timeViewAttend(id,tem){
		let date = this.date;

		if(this.sched != null || this.sched != []){
			this.sched = [];
		}

		let model = [];
		model.push({id,date});

		this._attendservice.getSchedule(model).
		subscribe(
			data => {
				let temp_data = data;

				let dat = temp_data,
			    names = tem,
			    show = true,
			    map = new Map,
			    result = names.map(({ id, name}) => ({ name,show,details: map.set(id, []).get(id) }));

				dat.forEach(({branch_id,date,days,end_time,id,late,start_time,time_in,time_out,undertime,break_start,break_end,lunch_in,lunch_out,sa}) => map.get(id).push({branch_id,date,days,end_time,id,late,start_time,time_in,time_out,undertime,break_start,break_end,lunch_in,lunch_out,sa}));

				this.sched = result;
				this.loader = false;
				this.tableVal = true;

				///update this
				if(this.sched != null || this.sched != []){
					this.thirdstep = true;
				}
				else{
					this.error_title = 'Error 500';
					this.error_message = 'Please check the working schedule of Employee';
				}

			},
			err => console.error(err)
		);

		// setTimeout(() => {
  //   		let len = this.sched.length;
  //   		for(let i=0; i<len;i++){
  //   			let obj = this.sched[i];
  //   			let val = new String(obj.branch_id);
  //   			// console.log('val: ',"#branch_" + val + " > .select2-hidden-accessible");
	 //  		   $("#branch_" + val + " > .select2-hidden-accessible").select2().select2('val',val);
  //   		}
	 //      }, 2500);

		this.getBranch();

		this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
    		showDropdowns: true,
    		opens: "center"
		};

		setTimeout(() => {
			this.getWorkingSchedule(); 
      	}, 2000);
	}

	dayViewAttend(id,temp){

		this._attendservice.getEmployeeRate(id).
		subscribe(
			data => {
				let user = data;

				this.dataStorage = [];
				let days = 0;
				let undertime = 0.0;
				let overtime = 0.0;
				let i = 0;
				let date = this.date;
				let date2 = [];

				let stime = "00:00:00";
				let etime = "23:59:00";

				let s = moment(this.start_date).format('YYYY-MM-DD');
				let e = moment(this.end_date).format('YYYY-MM-DD');

				let start = moment(s+' '+stime);
				let end = moment(e+' '+etime);

				s = moment(start).format('YYYY-MM-DD HH:mm:ss');
				e =moment(end).format('YYYY-MM-DD HH:mm:ss');

				date2.push(s,e);
				let show = true;
				let holiday = this.holiday2;
				let night_diff = 0;
				let restday = 0;
				let temp_for_type1 = [];
				let temp_for_type2 = '0';
				let temp_for_id1 = [];
				let temp_for_id2 = '0';

				for (let i = 0; i < this.holiday2.length; ++i) {
					temp_for_type1.push(this.holiday2[i].type);
					temp_for_type2 = temp_for_type1.join();

					temp_for_id1.push(this.holiday2[i].id);
					temp_for_id2 = temp_for_id1.join();
				}
				let holiday2 = temp_for_type2;
				let holiday_ids = temp_for_id2;

				for (let i = 0; i < user.length; ++i) {

					for (let x = 0; x < temp.length; ++x) {
						if (user[i].employee_id == temp[x].id) {
							let emp_id = temp[x].id;
							let name = temp[x].name;
							let branch_id = temp[x].branch_id;
							let daily_rate = user[i].daily_rate;
							let allowance = user[i].allowance_amount;
							let e_cola = user[i].e_cola;
							this.dataStorage.push({i,emp_id,name,branch_id,days,undertime,overtime,date,date2,show,daily_rate,allowance,e_cola,holiday,holiday2,night_diff,restday,holiday_ids});
						}
					}
				}

				this.sortName();

				this.loader = false;
				this.tableVal = true;
				this.getBranch();
				this.thirdstep = true;

			},
			err => console.error(err)
		);

	}

	sortName(){
		this.dataStorage.sort(function(a, b){
		    var nameA=a.name.toLowerCase(), nameB=b.name.toLowerCase()
		    if (nameA < nameB)
		        return -1 
		    if (nameA > nameB)
		        return 1
		    return 0
		});
	}

	checkHoliday(){

		this.holiday = [];
		let holi = [];

		this._common_service.checkDateHoliday(this.date).
			subscribe(
				data => {
					holi = data;

					if (holi.length != 0) {
						this.holiday = holi;
						let temp = [];
						this.holiday2 = [];
						for (let i = 0; i < holi.length; ++i) {
							let type = holi[i].type;
							let date = holi[i].date;
							let id = holi[i].id;
							this.holiday2.push({date,type,id});
						}
					}else{
						this.holiday = null;
						this.holiday2 = [];
					}
				},
				err => console.error(err)
			);
	}

	secondStep(type){

		if(type == 'time'){
			this.timeView = true;
			this.dayView = false;
		}else if(type == 'day'){
			this.dayView = true;
			this.timeView = false;
		}

		this.SecondStep();

	}

	changeInputDay(i,type,event){

		let value = event.target.value;
		let num = this.date.length;

		if (value == '') {
			value = 0;
		}

		if(type == 'day'){
			this.dataStorage[i].days = value;	
			if(value <= num && value >= 0){
				this.dataStorage[i].days = value;	
			}else{
				this.dataStorage[i].days = num;
			}
		}
		else if(type == 'ut'){
			this.dataStorage[i].undertime = value;
		}
		else if(type == 'ot'){
			this.dataStorage[i].overtime  = value;
		}
		else if(type == 'allow'){
			this.dataStorage[i].allowance  = value;
		}
		else if(type == 'dr'){
			this.dataStorage[i].daily_rate  = value;
		}
		else if(type == 'e_cola'){
			this.dataStorage[i].e_cola  = value;
		}
		else if(type == 'night_diff'){
			this.dataStorage[i].night_diff  = value;
		}
		else if(type == 'restday'){
			this.dataStorage[i].restday  = value;
		}
	}
	
    changeInputTime(event:any,type:any,i:any,x:any){

		let time =  moment("2017-11-29 " + event.target.value).format("HH:mm:ss");
		let data = this.sched;

		if (type == 'in') { 
			let date = moment(this.sched[i].details[x].date).format('YYYY-MM-DD');
			this.sched[i].details[x].start_time = time;
			this.sched[i].details[x].time_in = date + ' ' + time;
		}
		else if(type == 'out'){
			let date = moment(this.sched[i].details[x].date).format('YYYY-MM-DD');
			this.sched[i].details[x].end_time = time;
			this.sched[i].details[x].time_out = date + ' ' + time;
		}
		else if(type == 'lunch_in'){
			let date = moment(this.sched[i].details[x].date).format('YYYY-MM-DD');
			this.sched[i].details[x].break_start = time;
			this.sched[i].details[x].lunch_in = date + ' ' + time;
		}
		else if(type == 'lunch_out'){
			let date = moment(this.sched[i].details[x].date).format('YYYY-MM-DD');
			this.sched[i].details[x].break_end = time;
			this.sched[i].details[x].lunch_out = date + ' ' + time;
		}

		if(time == ''){
			this.thirdstep = false;
		}
		else{
			this.thirdstep = true;
		}

		if(time == ''){
			this.invalid++;
			this.thirdstep = false;
		}
		else{
			this.invalid--;
			this.valid();
		}

    }

    valid(){

		if(this.invalid > 0){
			this.thirdstep = false;
		}
		else{
			this.thirdstep = true;
		}
	}

    removeInListTimeView(i,x){

		this.sched[i].details.splice(x,1);

		if (this.sched[i].details.length == 0) { 
			this.sched.splice(i,1);
		}
		if(this.sched.length == 0){
			this.thirdstep = false;
			this.tableVal = false;
		}

	}
    removeInListDayView(x){

		this.dataStorage.splice(x,1);

		if(this.dataStorage.length == 0){
			this.thirdstep = false;
			this.tableVal = false;
		}

	}
    

    public LastStep(){

    	this.loader = true;
		this.tableVal = false;
		this.backToTop();

    	if(this.timeView == true){

	    	let data_model = this.sched;

	   		this._attendservice.createTimeAttendance(data_model).subscribe(
				data => {
					this.poststore = Array.from(data); 
					this.loader = false;
					this.success_title = "Success";
					this.success_message = "A new user record was successfully added.";
					setTimeout(() => {
			   		this._rt.navigateByUrl('admin/attendance-list-default');
	    			}, 3000);
				},
				err => this.catchError(err)
			);
    	}
    	if(this.dayView == true){

    		let data_model = this.dataStorage;
    		let len = this.company.length;
    		let def = 0;

    		for (let i = 1; i < len; ++i) {
	    		if (this.company[i].id == this.company_current ) {
	    			def = this.company[i].default;
	    		}
    		}

    		if (def == 1) {
	    		this._attendservice.createDayAttendanceDefault(data_model).subscribe(
					data => {
						this.poststore = data; 
						this.cacheError();
						
					},
					err => this.catchError(err)
				);	
    		}else{
    			this._attendservice.createDayAttendance(data_model).subscribe(
					data => {
						this.poststore = data; 
						this.cacheError();
						
					},
					err => this.catchError(err)
				);
    		}

    	}

    }

    cacheError(){
    	if (this.poststore.status_code == 422) {
    		this.loader = false;
			this.tableVal = true;
    		let disposable = this.modalService.addDialog(AlertModalComponent, {
            title:'Unprocessable Entity 422',
			message:this.poststore.message,
			data:this.poststore.data,
			attValEmp:true,
			model:this.poststore.model
        	}).subscribe((isConfirmed)=>{
                this.loader = false;
				this.tableVal = true;
        		document.getElementById('back').click();
            });
    	}else{
    		this.loader = false;
			this.success_title = "Success";
			this.success_message = "Attendance was successfully added.";
			setTimeout(() => {
	   		this._rt.navigateByUrl('admin/attendance-list');
			}, 3000);
    	}
    }

    ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	getBranch(){
	      let id = this.company_current;
	      this._common_service.getBranchByCompany(id)
	      .subscribe(
	        data => {
	        this.branch = data;	        
	        },
	        err => console.error(err)
	    );
	}

	changedBranch(datas: any,i:any,x:any) {

		if(datas.value != 0){
			this.sched[i].details[x].branch_id = datas.value;
		}
	} 
	changedBran(data,i) {

		if(data.value != 0){
			this.dataStorage[i].branch_id = data.value;
		}
	}
	

	filterEmpInCheckBox(name){
		
		let len = this.employeesList.length;

		for (let i = 0; i < len; ++i) {
			if (this.employeesList[i].name.toLowerCase().match(name.toLowerCase())) {
				this.employeesList[i].show = true;
			}else{
				this.employeesList[i].show = false;
			}
		}
	}

	filterEmpInTimeView(name){
		
		let len = this.sched.length;

		for (let i = 0; i < len; ++i) {
			if (this.sched[i].name.toLowerCase().match(name.toLowerCase())) {
				this.sched[i].show = true;
			}else{
				this.sched[i].show = false;
			}
		}
	}

	filterEmpInDayView(name){
		
		let len = this.dataStorage.length;

		for (let i = 0; i < len; ++i) {
			if (this.dataStorage[i].name.toLowerCase().match(name.toLowerCase())) {
				this.dataStorage[i].show = true;
			}else{
				this.dataStorage[i].show = false;
			}
		}
	}

	fixSched(e){
		this.withFixSched = e.target.checked;
		if (this.withFixSched == false) {
			this.getWorkingSchedule();
		}

	}

	getWorkingSchedule(){

		  let company = this.company_current;
	      this._common_service.getWorkingSchedule(company)
	      .subscribe(
	        data => {
	        this.work_sched = data;
	        this.ws_value = [];
	        this.ws_current = this.ws_value;

	        let id = 0;
	        let text = 'Suggested Schedule';

	        this.work_sched.unshift({id,text});

	        this.ws_value = this.value;
	        
	        },
	        err => this.catchError(err)

	    );
	}
	private changedWorkingSchedule(data: any,i:any,x:any) {

  		for (let y = 0; y < this.work_sched.length; ++y) {
  			if (this.work_sched[y].id == data.value) {
				this.sched[i].details[x].start_time = this.work_sched[y].time_in;
				this.sched[i].details[x].end_time = this.work_sched[y].time_out;
				this.sched[i].details[x].time_in = this.sched[i].details[x].date + ' ' + this.work_sched[y].time_in;
				this.sched[i].details[x].time_out = this.sched[i].details[x].date + ' ' + this.work_sched[y].time_out;
				this.sched[i].details[x].sa = data.value;
		  		this.sched[i].details[x].break_start = this.work_sched[y].break_start;
				this.sched[i].details[x].break_end = this.work_sched[y].break_end;
				this.sched[i].details[x].lunch_in = this.sched[i].details[x].date + ' ' + this.work_sched[y].break_start;
				this.sched[i].details[x].lunch_out = this.sched[i].details[x].date + ' ' + this.work_sched[y].break_end;
  			}
  		}
    } 

    checklistOfHoliday(x){
    	let holiday = this.dataStorage[x].holiday;

    	let disposable = this.modalService.addDialog(ChecklistModalComponent, {
            title:'Holiday List',
            tableVal:true,
            attend:true,
            data:this.holiday,
            holiday:holiday,
            clearance:false,
			onboarding:false,
			attendModal:false
        	}).subscribe((message)=>{
        		if (message != 1 || message == []) {

        			let temp = [];
					let temp2 ='0';

					for (let i = 0; i < message.length; ++i) {
						temp.push(message[i].type);
						temp2 = temp.join();
					}
					this.dataStorage[x].holiday2 = temp2;
        			this.dataStorage[x].holiday = message;
        		}
            });
    }

	backToTop(){
		document.body.scrollTop = 0;
		document.documentElement.scrollTop = 0;
	}

}