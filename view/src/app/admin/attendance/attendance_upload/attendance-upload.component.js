var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute } from '@angular/router';
import { Configuration } from './../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { AttendanceService } from './../../../services/attendance.service';
var AttendanceUploadComponent = /** @class */ (function () {
    function AttendanceUploadComponent(_conf, _attendance_serv, modalService, _rt, _ar) {
        this._conf = _conf;
        this._attendance_serv = _attendance_serv;
        this.modalService = modalService;
        this._rt = _rt;
        this._ar = _ar;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    AttendanceUploadComponent.prototype.ngOnInit = function () {
        var _this = this;
        var attendance = this._attendance_serv.getUploadHistoryFromAttendance().
            subscribe(function (data) {
            _this.attendance_id = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
        this.dtOptions = {
            order: [[1, 'desc']],
        };
    };
    AttendanceUploadComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    AttendanceUploadComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    AttendanceUploadComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    AttendanceUploadComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    AttendanceUploadComponent.prototype.bulkAttendanceDelete = function (id) {
        var _this = this;
        var attendace_id = id;
        if (confirm("Are you sure you want to delete this upload?")) {
            this._attendance_serv.deleteAttendanceByBatch(attendace_id).subscribe(function (data) {
                alert('Record is successfully deleted.');
                _this._rt.navigate(['/admin/attendance-upload-logs']);
                _this.ngOnInit();
            }, function (err) { return console.error(err); });
        }
        else {
            this._rt.navigate(['/admin/attendance-upload-logs']);
        }
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], AttendanceUploadComponent.prototype, "dtElement", void 0);
    AttendanceUploadComponent = __decorate([
        Component({
            selector: 'attendance-upload-list',
            templateUrl: './attendance-upload.component.html'
            // styleUrls: ['./payroll-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration,
            AttendanceService,
            DialogService,
            Router,
            ActivatedRoute])
    ], AttendanceUploadComponent);
    return AttendanceUploadComponent;
}());
export { AttendanceUploadComponent };
//# sourceMappingURL=attendance-upload.component.js.map