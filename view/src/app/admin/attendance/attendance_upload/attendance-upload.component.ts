import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Configuration } from './../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';


import { AttendanceService } from './../../../services/attendance.service';


@Component({
  selector: 'attendance-upload-list',
  templateUrl: './attendance-upload.component.html'
  // styleUrls: ['./payroll-list.component.css']
})

@Injectable()
export class AttendanceUploadComponent implements OnInit, AfterViewInit  {

	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	public id: string;
	public attendance_id: any;
	public success_title: string;

	public error_title: string;
    public error_message: string;

	dtOptions: any = {};

	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	payroll_rec: any;

	constructor(
			private _conf: Configuration, 
			private _attendance_serv: AttendanceService, 
			private modalService: DialogService, 
			private _rt: Router,
			private _ar: ActivatedRoute, 
		) {
	    this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	}

	ngOnInit() {
		let attendance = this._attendance_serv.getUploadHistoryFromAttendance().
			subscribe(
				data => {
					 this.attendance_id = Array.from(data);
					 this.rerender();
				},
				err => console.error(err)
			);
		this.dtOptions = {
	      order: [[1, 'desc']],
	    };
	}	

	private catchError(error: any){
	    let response_body = error._body;
	    let response_status = error.status;

	    if( response_status == 500 ){
	      this.error_title = 'Error 500';
	      this.error_message = 'Something went wrong';
	    } else if( response_status == 200 ) {
	      this.error_title = '';
	      this.error_message = '';
	    }
	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	}

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }


    bulkAttendanceDelete(id){
    let attendace_id = id;

   if (confirm("Are you sure you want to delete this upload?")) {
	   	this._attendance_serv.deleteAttendanceByBatch(attendace_id).subscribe(
	      data => {
	        alert('Record is successfully deleted.');
	        this._rt.navigate(['/admin/attendance-upload-logs']);
	        this.ngOnInit();
	      },
	      err => console.error(err)
	    );
   } else {
	  this._rt.navigate(['/admin/attendance-upload-logs']);
   }


    

  }
	
}