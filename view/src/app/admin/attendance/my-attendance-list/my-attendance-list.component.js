var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { AuthUserService } from '../../../services/auth-user.service';
import { Subject } from 'rxjs/Rx';
import { AttendanceService } from '../../../services/attendance.service';
import { CertificateOfAttendanceModalComponent } from '../../certificate-of-attendance/certificate-of-attendance-modal/certificate-of-attendance-modal.component';
import * as moment from 'moment';
import { CommonService } from '../../../services/common.service';
var MyAttendanceListComponent = /** @class */ (function () {
    function MyAttendanceListComponent(modalService, _conf, _auth_service, _attendservice, _fb, _common_service, _rt) {
        this.modalService = modalService;
        this._conf = _conf;
        this._auth_service = _auth_service;
        this._attendservice = _attendservice;
        this._fb = _fb;
        this._common_service = _common_service;
        this._rt = _rt;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.att = [];
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.dtOptions = {
            info: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            processing: true,
            language: {
                paginate: {
                    previous: '‹',
                    next: '›'
                },
                aria: {
                    paginate: {
                        previous: 'Previous',
                        next: 'Next'
                    }
                }
            }
        };
        this.filterForm = _fb.group({
            'type_id': [null],
            'start_date': [null],
            'end_date': [null]
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    MyAttendanceListComponent.prototype.ngOnInit = function () {
        this.Data();
        this.getType();
    };
    MyAttendanceListComponent.prototype.Data = function () {
        var _this = this;
        this.filterForm.value.employee_id = this.employee_id;
        var model = this.filterForm.value;
        this._attendservice.getMyAttendance(model).
            subscribe(function (data) {
            _this.att = data;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    MyAttendanceListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    MyAttendanceListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    MyAttendanceListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    MyAttendanceListComponent.prototype.createCOA = function (date, type, emp_id) {
        var _this = this;
        var dates = moment(date).format("YYYY-MM-DD");
        var disposable = this.modalService.addDialog(CertificateOfAttendanceModalComponent, {
            title: 'Create Certificate of Attendance',
            button: 'Add',
            create: true,
            employee_id: emp_id,
            user: true,
            type_id: type,
            date_in: dates,
            coaValidation: true
        }).subscribe(function (message) {
            if (message == true) {
                _this._rt.navigateByUrl('admin/my-coa-list');
            }
            else {
                _this.rerender();
                _this.ngOnInit();
            }
        });
    };
    MyAttendanceListComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.Data();
    };
    MyAttendanceListComponent.prototype.getType = function () {
        var _this = this;
        this._common_service.getAttendanceType().
            subscribe(function (data) {
            _this.type = Array.from(data);
            _this.type_value = [];
            _this.options = {
                multiple: true
            };
            _this.type_current = _this.type_value;
        }, function (err) { return console.error(err); });
    };
    MyAttendanceListComponent.prototype.changedType = function (data) {
        this.type_current = data.value;
        if (this.type_current == 0) {
            this.filterForm.value.type_id = null;
        }
        else {
            this.filterForm.value.type_id = this.type_current;
        }
        this.Data();
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], MyAttendanceListComponent.prototype, "dtElement", void 0);
    MyAttendanceListComponent = __decorate([
        Component({
            selector: 'app-my-attendance-list',
            templateUrl: './my-attendance-list.component.html',
            styleUrls: ['./my-attendance-list.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            AuthUserService,
            AttendanceService,
            FormBuilder,
            CommonService,
            Router])
    ], MyAttendanceListComponent);
    return MyAttendanceListComponent;
}());
export { MyAttendanceListComponent };
//# sourceMappingURL=my-attendance-list.component.js.map