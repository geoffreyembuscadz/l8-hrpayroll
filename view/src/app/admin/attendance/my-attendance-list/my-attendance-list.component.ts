import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { AuthUserService } from '../../../services/auth-user.service';
import { Subject } from 'rxjs/Rx';
import { AttendanceService } from '../../../services/attendance.service';
import { CertificateOfAttendanceModalComponent } from '../../certificate-of-attendance/certificate-of-attendance-modal/certificate-of-attendance-modal.component';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Select2OptionData } from 'ng2-select2';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-my-attendance-list',
  templateUrl: './my-attendance-list.component.html',
  styleUrls: ['./my-attendance-list.component.css']
})
export class MyAttendanceListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	dtOptions: any = {};
	public OT_id: any;
	status:any;
	att=[];
	user_id:any;
	employee_id:any;
	supervisor_id:any;

	public mainInput = {
        start: moment().subtract(12, 'month'),
        end: moment().subtract(11, 'month')
    }

    filterForm:any;
    public options: Select2Options;

    public type_value: Array<Select2OptionData>;
	public type_current: any;
	public type : any;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _auth_service: AuthUserService,
		private _attendservice: AttendanceService,
		private _fb: FormBuilder, 
		private _common_service: CommonService,
		private _rt: Router
		){

		this.dtOptions = {
	      info: false,
	      lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
	      processing: true,
	      language: {
		        paginate: {
		            previous: '‹',
		            next:     '›'
		        },
		        aria: {
		            paginate: {
		                previous: 'Previous',
		                next:     'Next'
		            }
		        }
		    }
	    };

		this.filterForm = _fb.group({
    	'type_id': 			[null],
		'start_date': 		[null],
		'end_date': 		[null]
   		});

		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");
	}

	ngOnInit() {
		this.Data();
		this.getType();
	}

	Data(){

		this.filterForm.value.employee_id = this.employee_id;
		let model = this.filterForm.value;

		this._attendservice.getMyAttendance(model).
		subscribe(
			data => {
				this.att = data;
				this.rerender();

			},
			err => console.error(err)
		);
	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	}

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    createCOA(date,type,emp_id){

    	let dates = moment(date).format("YYYY-MM-DD");

		let disposable = this.modalService.addDialog(CertificateOfAttendanceModalComponent, {
            title:'Create Certificate of Attendance',
            button:'Add',
            create:true,
            employee_id:emp_id,
            user:true,
            type_id:type,
            date_in:dates,
            coaValidation:true
        	}).subscribe((message)=>{
       			if (message == true) {
                	this._rt.navigateByUrl('admin/my-coa-list');
       			}else{
	                this.rerender();
	                this.ngOnInit();
       			}
        });
	}

	filterDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;

        let start_date = moment(dateInput.start).format("YYYY-MM-DD");
        let end_date = moment(dateInput.end).format("YYYY-MM-DD");

        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;

        this.Data();

    }

    getType(){
		this._common_service.getAttendanceType().
		subscribe(
			data => {
				this.type = Array.from(data);
				this.type_value = [];
				this.options = {
					multiple: true
				}
				this.type_current = this.type_value;
			},
			err => console.error(err)
		);
	}
	changedType(data: any) {
		this.type_current = data.value;

		if(this.type_current == 0){
			this.filterForm.value.type_id = null;
		}
		else{
			this.filterForm.value.type_id = this.type_current;
		}
			this.Data();
	}
}
