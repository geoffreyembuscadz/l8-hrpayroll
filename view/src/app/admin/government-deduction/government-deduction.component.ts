import { Component, OnInit } from '@angular/core';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { GovernmentDeductionService } from '../../services/government-deduction.service';
import { PhilhealthService } from '../../services/philhealth.service';
import { SSSService } from '../../services/sss.service';

@Component({
  selector: 'app-government-deduction',
  templateUrl: './government-deduction.component.html',
  styleUrls: ['./government-deduction.component.css']
})
export class GovernmentDeductionComponent implements OnInit {
	public pagibig: any;
	public philhealth: any;
	public sss: any;
	public deduction: any;
	public totalDeduction: any;
	public netPay:any;
	basicPayForm : FormGroup;
	basicPay : number;

  	constructor(private _govdec_service: GovernmentDeductionService, private _fb: FormBuilder) {

  	this.basicPayForm = _fb.group({
      'basicPay': [this.basicPay, [Validators.required]],
      
    }); 

  	}  	

	ngOnInit() {
	
	}

	computeSalary(){

		let basicPay_model = this.basicPayForm.value;
		let basicPay = basicPay_model.basicPay;
		let deduction_obj = this._govdec_service.computeDeduction(basicPay).
			subscribe(
				data => {
					
					Array.from(data); // fetched record
					this.deduction = data;
					
					this.philhealth = this.deduction.philhealth.employee_share;
					this.pagibig = this.deduction.pagibig;
					this.sss = this.deduction.sss[0].ee;
					this.computeTotal();
						},
				err => console.error(err)
			);
		
	}

	computeTotal(){
		let pagibig = parseInt(this.pagibig);
		let philhealth =parseInt(this.philhealth);
		let sss = parseInt(this.sss);

		this.totalDeduction = pagibig + philhealth + sss;
		this.netPay = this.basicPay - this.totalDeduction;
		

	}


}