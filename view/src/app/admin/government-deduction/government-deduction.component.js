var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { GovernmentDeductionService } from '../../services/government-deduction.service';
var GovernmentDeductionComponent = /** @class */ (function () {
    function GovernmentDeductionComponent(_govdec_service, _fb) {
        this._govdec_service = _govdec_service;
        this._fb = _fb;
        this.basicPayForm = _fb.group({
            'basicPay': [this.basicPay, [Validators.required]],
        });
    }
    GovernmentDeductionComponent.prototype.ngOnInit = function () {
    };
    GovernmentDeductionComponent.prototype.computeSalary = function () {
        var _this = this;
        var basicPay_model = this.basicPayForm.value;
        var basicPay = basicPay_model.basicPay;
        var deduction_obj = this._govdec_service.computeDeduction(basicPay).
            subscribe(function (data) {
            Array.from(data); // fetched record
            _this.deduction = data;
            _this.philhealth = _this.deduction.philhealth.employee_share;
            _this.pagibig = _this.deduction.pagibig;
            _this.sss = _this.deduction.sss[0].ee;
            _this.computeTotal();
        }, function (err) { return console.error(err); });
    };
    GovernmentDeductionComponent.prototype.computeTotal = function () {
        var pagibig = parseInt(this.pagibig);
        var philhealth = parseInt(this.philhealth);
        var sss = parseInt(this.sss);
        this.totalDeduction = pagibig + philhealth + sss;
        this.netPay = this.basicPay - this.totalDeduction;
    };
    GovernmentDeductionComponent = __decorate([
        Component({
            selector: 'app-government-deduction',
            templateUrl: './government-deduction.component.html',
            styleUrls: ['./government-deduction.component.css']
        }),
        __metadata("design:paramtypes", [GovernmentDeductionService, FormBuilder])
    ], GovernmentDeductionComponent);
    return GovernmentDeductionComponent;
}());
export { GovernmentDeductionComponent };
//# sourceMappingURL=government-deduction.component.js.map