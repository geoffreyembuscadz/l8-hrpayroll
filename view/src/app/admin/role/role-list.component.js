var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from './../../app.config';
import { Role } from '../../model/role';
import { RoleCreateComponent } from './role-create.component';
import { RoleEditComponent } from './role-edit.component';
import { PermissionService } from '../../services/permission.service';
import { RoleService } from '../../services/role.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
var RoleListComponent = /** @class */ (function () {
    function RoleListComponent(_conf, _perms_service, modalService, _ar, _role_service, _rt, _fb) {
        this._conf = _conf;
        this._perms_service = _perms_service;
        this.modalService = modalService;
        this._ar = _ar;
        this._role_service = _role_service;
        this._rt = _rt;
        this._fb = _fb;
        this.dtTrigger = new Subject();
        this.role = new Role();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    RoleListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var perms = this._perms_service.getPermissions().
            subscribe(function (data) {
            _this.perm_rec = Array.from(data); // fetched record
        }, function (err) { return console.error(err); });
        var roles = this._role_service.getRoles()
            .subscribe(function (data) {
            _this.ad_roles = Array.from(data);
            _this.rerender();
        }, function (err) { return _this.catchError(err); });
    };
    RoleListComponent.prototype.clickRow = function (id, name) {
        this._role_service.setName(name);
        this._role_service.setId(id);
        this.openEditModal();
    };
    RoleListComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    RoleListComponent.prototype.openEditModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(RoleEditComponent, {
            title: 'Update Role'
        }).subscribe(function (isConfirmed) {
            var roles = _this._role_service.getRoles()
                .subscribe(function (data) {
                _this.ad_roles = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    RoleListComponent.prototype.openAddModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(RoleCreateComponent, {
            title: 'Add Role'
        }).subscribe(function (isConfirmed) {
            var roles = _this._role_service.getRoles()
                .subscribe(function (data) {
                _this.ad_roles = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    RoleListComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    RoleListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    RoleListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], RoleListComponent.prototype, "dtElement", void 0);
    RoleListComponent = __decorate([
        Component({
            selector: 'admin-role-list',
            templateUrl: './role-list.component.html',
            styleUrls: ['./role-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration,
            PermissionService,
            DialogService,
            ActivatedRoute,
            RoleService,
            Router,
            FormBuilder])
    ], RoleListComponent);
    return RoleListComponent;
}());
export { RoleListComponent };
//# sourceMappingURL=role-list.component.js.map