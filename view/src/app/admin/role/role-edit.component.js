var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Role } from '../../model/role';
import { RoleService } from '../../services/role.service';
import { PermissionService } from '../../services/permission.service';
import { AuthUserService } from '../../services/auth-user.service';
import { MainSideService } from '../../services/main-side.service';
var RoleEditComponent = /** @class */ (function (_super) {
    __extends(RoleEditComponent, _super);
    function RoleEditComponent(_auth_service, _main_side_service, _rt, _perms_service, dialogService, _fb, _ar, _role_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._auth_service = _auth_service;
        _this._main_side_service = _main_side_service;
        _this._rt = _rt;
        _this._perms_service = _perms_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._role_service = _role_service;
        _this.role = new Role();
        _this.permission_value = false;
        _this.current_permission = [];
        _this.current_module = [];
        _this.current_menu = [];
        _this.checkModule = [];
        _this.demoChkModule = [];
        _this.checkMenu = [];
        _this.demoChkMenu = [];
        _this.checkPerms = [];
        _this.demoChkPerms = [];
        _this.updateRoleForm = _fb.group({
            'name': [_this.role.name, [Validators.required]],
            'display_name': [_this.role.display_name, [Validators.required]],
            'description': [_this.role.description],
            'permission_id': [null],
            'module_id': [null],
            'menu_id': [null],
            'created_by': ['']
        });
        return _this;
    }
    RoleEditComponent.prototype.validateUpdate = function () {
        var _this = this;
        this.updateRoleForm.patchValue({
            'permission_id': this.current_permission
        });
        this.updateRoleForm.patchValue({
            'module_id': this.current_module
        });
        this.updateRoleForm.patchValue({
            'menu_id': this.current_menu
        });
        this.updateRoleForm.patchValue({
            'created_by': this.user_id
        });
        var role_model = this.updateRoleForm.value;
        this.updateRoleForm.patchValue({
            permission_id: this.value
        });
        this._role_service.updateRole(this.id, role_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The role is successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 1000);
            _this._rt.navigate(['/admin/role-list']);
        }, function (err) { return _this.catchError(err); });
    };
    RoleEditComponent.prototype.archiveRole = function (event) {
        var _this = this;
        event.preventDefault();
        var role_id = this.id;
        this._role_service.archiveRole(role_id).subscribe(function (data) {
            //this.setEmployee(data);
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigateByUrl('admin/role-list');
        }, function (err) { return console.error(err); });
    };
    RoleEditComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Role name already exist.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    RoleEditComponent.prototype.clearErrorMsg = function () {
        this.error_title = '';
        this.error_message = '';
    };
    RoleEditComponent.prototype.setRole = function (role) {
        role.name = this.nameText;
        this.id = role.id;
        this.role = role;
    };
    RoleEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.id = this._role_service.getId();
        this.nameText = this._role_service.getName();
        this._role_service.getRole(this.id).subscribe(function (data) {
            _this.setRole(data);
            if (data.module_id == null) {
                return false;
            }
            else if (!_this.module_names) {
                _this.module_names = Array.from(data.module_id.split(','));
                for (var i = 0; i < _this.module_names.length; i++) {
                    _this.demoChkModule.push(_this.module_names[i]);
                }
                _this.current_module = _this.demoChkModule;
            }
            if (data.permission_id == null) {
                return false;
            }
            else if (!_this.permission_names) {
                _this.permission_names = Array.from(data.permission_id.split(','));
                for (var i = 0; i < _this.permission_names.length; i++) {
                    _this.demoChkPerms.push(_this.permission_names[i]);
                }
                _this.current_permission = _this.demoChkPerms;
            }
            if (data.menu_id == null) {
                return false;
            }
            else if (!_this.menu_names) {
                _this.menu_names = Array.from(data.menu_id.split(','));
                for (var i = 0; i < _this.menu_names.length; i++) {
                    _this.demoChkMenu.push(_this.menu_names[i]);
                }
                _this.current_menu = _this.demoChkMenu;
            }
        }, function (err) { return console.error(err); });
        var main_side = this._main_side_service.getModule()
            .subscribe(function (data) {
            _this.user_side = Array.from(data);
        });
        var created_by = this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
        }, function (err) { return console.error(err); });
    };
    RoleEditComponent.prototype.addCheckModule = function (value, event) {
        if (event.target.checked) {
            this.demoChkModule.push(value);
            var length_1 = this.user_side.length;
            for (var i = 0; i < length_1; i++) {
                var lengthx = this.user_side[i].menu.length;
                for (var x = 0; x < lengthx; x++) {
                    if (value == this.user_side[i].menu[x].parent_id) {
                        this.demoChkMenu.push(this.user_side[i].menu[x].id);
                        var lengthy = this.user_side[i].menu[x].permission.length;
                        for (var y = 0; y < lengthy; y++) {
                            if (this.user_side[i].menu[x].id == this.user_side[i].menu[x].permission[y].parent_id) {
                                this.demoChkPerms.push(this.user_side[i].menu[x].permission[y].id);
                            }
                        }
                    }
                }
            }
        }
        else if (!event.target.checked) {
            // if (this.module_names == 'undefined') {
            var arr_len = this.module_names.length;
            this.demoChkModule = [];
            for (var i_1 = 0; i_1 < arr_len; i_1++) {
                if (this.module_names[i_1] != value) {
                    this.demoChkModule.push(this.module_names[i_1]);
                }
            }
            this.module_names = this.demoChkModule;
            var length_2 = this.user_side.length;
            for (var i = 0; i < length_2; i++) {
                var lengthx = this.user_side[i].menu.length;
                for (var x = 0; x < lengthx; x++) {
                    if (value == this.user_side[i].menu[x].parent_id) {
                        var arr_len_1 = this.menu_names.length;
                        this.demoChkMenu = [];
                        for (var z = 0; z < arr_len_1; z++) {
                            if (this.menu_names[z] != this.user_side[i].menu[x].id) {
                                this.demoChkMenu.push(this.menu_names[z]);
                            }
                        }
                        this.menu_names = this.demoChkMenu;
                        var lengthy = this.user_side[i].menu[x].permission.length;
                        for (var y = 0; y < lengthy; y++) {
                            if (this.user_side[i].menu[x].id == this.user_side[i].menu[x].permission[y].parent_id) {
                                var perms_len = this.permission_names.length;
                                this.demoChkPerms = [];
                                for (var p = 0; p < arr_len_1; p++) {
                                    if (this.permission_names[p] != this.user_side[i].menu[x].permission[y].id) {
                                        this.demoChkPerms.push(this.permission_names[p]);
                                    }
                                }
                                this.permission_names = this.demoChkPerms;
                            }
                        }
                    }
                }
            }
            // } else {
            // let indexx = this.demoChkModule.indexOf(value);
            // this.demoChkModule.splice(indexx,1);
            // let length = this.user_side.length;
            // for (var i = 0; i < length; i++) {
            //   let lengthx = this.user_side[i].menu.length;
            //   for (var x = 0; x < lengthx; x++) {
            //     if (value == this.user_side[i].menu[x].parent_id) {
            //       let indexxx = this.demoChkModule.indexOf(value);
            //       this.demoChkMenu.splice(indexxx, 1);
            //       let lengthy = this.user_side[i].menu[x].permission.length;
            //       for (var y = 0; y < lengthy; y++) {
            //         if (this.user_side[i].menu[x].id == this.user_side[i].menu[x].permission[y].parent_id) {
            //           let indexxxx = this.demoChkPerms.indexOf(value);
            //           this.demoChkPerms.splice(indexxxx, 1);
            //         }
            //       }
            //     }
            //   }
            // }
            // }
        }
        this.current_permission = this.demoChkPerms;
        this.current_menu = this.demoChkMenu;
        this.current_module = this.demoChkModule;
    };
    RoleEditComponent.prototype.addCheckMenu = function (value, event) {
        if (event.target.checked) {
            this.demoChkMenu.push(value);
            var length_3 = this.user_side.length;
            for (var i = 0; i < length_3; i++) {
                var lengthx = this.user_side[i].menu.length;
                for (var x = 0; x < lengthx; x++) {
                    var lengthy = this.user_side[i].menu[x].permission.length;
                    for (var y = 0; y < lengthy; y++) {
                        if (value == this.user_side[i].menu[x].permission[y].parent_id) {
                            this.demoChkPerms.push(this.user_side[i].menu[x].permission[y].id);
                        }
                    }
                }
            }
        }
        else if (!event.target.checked) {
            if (this.menu_names == 'undefined') {
                var arr_len = this.menu_names.length;
                this.demoChkMenu = [];
                for (var i_2 = 0; i_2 < arr_len; i_2++) {
                    if (this.menu_names[i_2] != value) {
                        this.demoChkMenu.push(this.menu_names[i_2]);
                    }
                }
                this.menu_names = this.demoChkMenu;
            }
            else {
                var indexx = this.demoChkMenu.indexOf(value);
                this.demoChkMenu.splice(indexx, 1);
                var length_4 = this.user_side.length;
                for (var i = 0; i < length_4; i++) {
                    var lengthx = this.user_side[i].menu.length;
                    for (var x = 0; x < lengthx; x++) {
                        var lengthy = this.user_side[i].menu[x].permission.length;
                        for (var y = 0; y < lengthy; y++) {
                            if (value == this.user_side[i].menu[x].permission[y].parent_id) {
                                var indexxxx = this.demoChkPerms.indexOf(this.user_side[i].menu[x].permission[y].id);
                                this.demoChkPerms.splice(indexxxx, 1);
                            }
                        }
                        this.permission_names = this.demoChkPerms;
                    }
                }
            }
        }
        this.current_permission = this.demoChkPerms;
        this.current_menu = this.demoChkMenu;
    };
    RoleEditComponent.prototype.addCheckPermission = function (value, event) {
        if (event.target.checked) {
            this.demoChkPerms.push(value);
        }
        else if (!event.target.checked) {
            if (this.permission_names == 'undefined') {
                var arr_len = this.permission_names.length;
                this.demoChkPerms = [];
                for (var i = 0; i < arr_len; i++) {
                    if (this.permission_names[i] != value) {
                        this.demoChkPerms.push(this.permission_names[i]);
                    }
                }
                this.permission_names = this.demoChkPerms;
            }
            else {
                var indexx = this.demoChkPerms.indexOf(value);
                this.demoChkPerms.splice(indexx, 1);
            }
        }
        this.current_permission = this.demoChkPerms;
    };
    RoleEditComponent.prototype.getModuleId = function (id) {
        return this.checkedId(this.module_names, id);
    };
    RoleEditComponent.prototype.getMenuId = function (id) {
        return this.checkedId(this.menu_names, id);
    };
    RoleEditComponent.prototype.getPermsId = function (id) {
        return this.checkedId(this.permission_names, id);
    };
    RoleEditComponent.prototype.getModuleIds = function (id) {
        return this.checkedId(this.demoChkModule, id);
    };
    RoleEditComponent.prototype.getMenuIds = function (id) {
        return this.checkedId(this.demoChkMenu, id);
    };
    RoleEditComponent.prototype.getPermsIds = function (id) {
        return this.checkedId(this.demoChkPerms, id);
    };
    RoleEditComponent.prototype.checkedId = function (arr_names, id) {
        if (!arr_names) {
            return false;
        }
        else {
            var len = arr_names.length;
            for (var i = 0; i < len; i++) {
                if (id == arr_names[i]) {
                    return true;
                }
            }
        }
    };
    RoleEditComponent = __decorate([
        Component({
            selector: 'admin-role-edit',
            templateUrl: './role-edit.component.html',
            styleUrls: ['./role-edit.component.css']
        }),
        __metadata("design:paramtypes", [AuthUserService, MainSideService, Router, PermissionService, DialogService, FormBuilder, ActivatedRoute, RoleService])
    ], RoleEditComponent);
    return RoleEditComponent;
}(DialogComponent));
export { RoleEditComponent };
//# sourceMappingURL=role-edit.component.js.map