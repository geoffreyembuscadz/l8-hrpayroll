import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';

import { Select2OptionData } from 'ng2-select2';

import { Role } from '../../model/role';
import { RoleService } from '../../services/role.service';
import { PermissionService } from '../../services/permission.service';
import { AuthUserService } from '../../services/auth-user.service';
import { MainSideService } from '../../services/main-side.service';

@Component({
  selector: 'admin-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.css']
})
export class RoleEditComponent extends DialogComponent<null, boolean> {
  perm_rec: any;
  user_side: any;
  title: string;
  message: string;

  public id: any;
  public name: string;
  public display_name: string;
  public description: string;
  public nameText: string;
  public success_title;
  public success_message;
  public poststore: any;
  public role = new Role();
  private error_title: string;
  private error_message: string;
  public permission_value: boolean = false;
  public current_permission = [];
  public current_module = [];
  public current_menu = [];

  public PermissionData: Array<Select2OptionData>;
  public value: any;
  public current: any;
  public options: Select2Options;
  public permission_names:any;
  public module_names:any;
  public menu_names:any;

  public module_index: any;
  user_id: any;
  checkModule = []
  demoChkModule= [];
  checkMenu = []
  demoChkMenu= [];
  checkPerms = []
  demoChkPerms= [];

  updateRoleForm : FormGroup;

  constructor(private _auth_service: AuthUserService, private _main_side_service: MainSideService,private _rt: Router, private _perms_service: PermissionService, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute, private _role_service: RoleService) {
    super(dialogService);

    this.updateRoleForm = _fb.group({

      'name': [this.role.name, [Validators.required]],
      'display_name': [this.role.display_name, [Validators.required]],
      'description': [this.role.description],
      'permission_id': [null],
      'module_id' : [null],
      'menu_id' : [null],
      'created_by' :['']

    });  
  }

  
 
  public validateUpdate() {

    this.updateRoleForm.patchValue({
      'permission_id': this.current_permission
    });

    this.updateRoleForm.patchValue({
      'module_id': this.current_module
    });

    this.updateRoleForm.patchValue({
      'menu_id': this.current_menu
    });

    this.updateRoleForm.patchValue({
        'created_by': this.user_id
      });

    let role_model = this.updateRoleForm.value; 

    this.updateRoleForm.patchValue({
      permission_id: this.value
    });

    this._role_service.updateRole(this.id, role_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The role is successfully updated.";
        setTimeout(() => {
             this.close();
            }, 1000);
        this._rt.navigate(['/admin/role-list']);
      },
      err => this.catchError(err)
    );
  }

  archiveRole(event: Event){
    event.preventDefault();
    let role_id = this.id;
    this._role_service.archiveRole(role_id).subscribe(
      data => {
        //this.setEmployee(data);
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigateByUrl('admin/role-list');
        
      },
      err => console.error(err)
    );
  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'Role name already exist.';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }
  }

  private clearErrorMsg(){
    this.error_title = '';
    this.error_message = '';
  }

  public setRole(role: any){
    role.name =  this.nameText ;
    this.id = role.id;
    this.role = role;
  }


  ngOnInit() {

    this.id = this._role_service.getId();
    this.nameText = this._role_service.getName();

    this._role_service.getRole(this.id).subscribe(
      data => {
        this.setRole(data);


        if(data.module_id == null) {
          return false;
        } else if(!this.module_names) {
          this.module_names = Array.from(data.module_id.split(','));
           for (let i = 0; i < this.module_names.length; i++) {
              this.demoChkModule.push(this.module_names[i]);  
           }
           this.current_module = this.demoChkModule;
        } 
        if (data.permission_id == null) {
          return false;
        } else if(!this.permission_names) {
          this.permission_names = Array.from(data.permission_id.split(','));
          for (let i = 0; i < this.permission_names.length; i++) {
              this.demoChkPerms.push(this.permission_names[i]);  
           }
           this.current_permission = this.demoChkPerms;
        } 
        if (data.menu_id == null) {
          return false;
        } else if(!this.menu_names) {
          this.menu_names = Array.from(data.menu_id.split(','));
          for (let i = 0; i < this.menu_names.length; i++) {
              this.demoChkMenu.push(this.menu_names[i]);  
           }
           this.current_menu = this.demoChkMenu;
        }
        
      },
      err => console.error(err)
    );
  
   
      let main_side = this._main_side_service.getModule()
        .subscribe(
            data => {
                this.user_side = Array.from(data);
            }

         );

      let created_by = this._auth_service.getUser().
        subscribe(
          data => {
            let user = data; 
            this.user_id = user.id;
          },
          err => console.error(err)
        );
  }
   

  addCheckModule(value,event) {
    if(event.target.checked){
      this.demoChkModule.push(value);
      let length = this.user_side.length;
      for (var i = 0; i < length; i++) {
        let lengthx = this.user_side[i].menu.length;
        for (var x = 0; x < lengthx; x++) {
          if (value == this.user_side[i].menu[x].parent_id) {
            this.demoChkMenu.push(this.user_side[i].menu[x].id);
            let lengthy = this.user_side[i].menu[x].permission.length;
            for (var y = 0; y < lengthy; y++) {
              if (this.user_side[i].menu[x].id == this.user_side[i].menu[x].permission[y].parent_id) {
                this.demoChkPerms.push(this.user_side[i].menu[x].permission[y].id)
              }
            }
          }
        }
      }
    }
    else if (!event.target.checked){
      // if (this.module_names == 'undefined') {
        let arr_len = this.module_names.length;
        this.demoChkModule = [];
        for (let i = 0; i < arr_len; i++) {
            if (this.module_names[i] != value) {
                this.demoChkModule.push(this.module_names[i]);  
            }
        }
        this.module_names = this.demoChkModule;

        let length = this.user_side.length;
        for (var i = 0; i < length; i++) {
          let lengthx = this.user_side[i].menu.length;
          for (var x = 0; x < lengthx; x++) {
            if (value == this.user_side[i].menu[x].parent_id) {
             let arr_len = this.menu_names.length;
             this.demoChkMenu = [];
              for (let z= 0; z< arr_len; z++) {
                  if (this.menu_names[z] != this.user_side[i].menu[x].id) {
                      this.demoChkMenu.push(this.menu_names[z]);  
                  }
              }
              this.menu_names = this.demoChkMenu;
              let lengthy = this.user_side[i].menu[x].permission.length;
              for (var y = 0; y < lengthy; y++) {
                if (this.user_side[i].menu[x].id == this.user_side[i].menu[x].permission[y].parent_id) {
                  let perms_len = this.permission_names.length;
                  this.demoChkPerms = [];
                  for (let p= 0; p< arr_len; p++) {
                  if (this.permission_names[p] != this.user_side[i].menu[x].permission[y].id) {
                          this.demoChkPerms.push(this.permission_names[p]);  
                      }
                  }
                  this.permission_names = this.demoChkPerms;
                }
              }
            }
          }
        }
      // } else {

        // let indexx = this.demoChkModule.indexOf(value);
        // this.demoChkModule.splice(indexx,1);
        // let length = this.user_side.length;
        // for (var i = 0; i < length; i++) {
        //   let lengthx = this.user_side[i].menu.length;
        //   for (var x = 0; x < lengthx; x++) {
        //     if (value == this.user_side[i].menu[x].parent_id) {
        //       let indexxx = this.demoChkModule.indexOf(value);
        //       this.demoChkMenu.splice(indexxx, 1);
        //       let lengthy = this.user_side[i].menu[x].permission.length;
        //       for (var y = 0; y < lengthy; y++) {
        //         if (this.user_side[i].menu[x].id == this.user_side[i].menu[x].permission[y].parent_id) {
        //           let indexxxx = this.demoChkPerms.indexOf(value);
        //           this.demoChkPerms.splice(indexxxx, 1);
        //         }
        //       }
        //     }
        //   }
        // }

      // }
    }

    this.current_permission = this.demoChkPerms;
    this.current_menu = this.demoChkMenu;
    this.current_module = this.demoChkModule;
  }

  addCheckMenu(value,event){
    if(event.target.checked){
      this.demoChkMenu.push(value);
      let length = this.user_side.length;
      for (var i = 0; i < length; i++) {
        let lengthx = this.user_side[i].menu.length;
        for (var x = 0; x < lengthx; x++) {
          let lengthy = this.user_side[i].menu[x].permission.length;
          for (var y = 0; y < lengthy; y++) {
            if (value == this.user_side[i].menu[x].permission[y].parent_id) {
              this.demoChkPerms.push(this.user_side[i].menu[x].permission[y].id)
            }
          }
        }
      }
    }
    else if (!event.target.checked){
      if (this.menu_names == 'undefined') {
         let arr_len = this.menu_names.length;
         this.demoChkMenu = [];
          for (let i = 0; i < arr_len; i++) {
              if (this.menu_names[i] != value) {
                  this.demoChkMenu.push(this.menu_names[i]);  
              }
          }
          this.menu_names = this.demoChkMenu;
      } else {
        let indexx = this.demoChkMenu.indexOf(value);
        this.demoChkMenu.splice(indexx,1);
        let length = this.user_side.length;
        for (var i = 0; i < length; i++) {
          let lengthx = this.user_side[i].menu.length;
          for (var x = 0; x < lengthx; x++) {
            let lengthy = this.user_side[i].menu[x].permission.length;
            for (var y = 0; y < lengthy; y++) {
              if (value == this.user_side[i].menu[x].permission[y].parent_id) {
                let indexxxx = this.demoChkPerms.indexOf(this.user_side[i].menu[x].permission[y].id);
                this.demoChkPerms.splice(indexxxx, 1);

              }
            }
            this.permission_names = this.demoChkPerms;
          }
        }
      }
    }


    this.current_permission = this.demoChkPerms;
    this.current_menu = this.demoChkMenu;

 
 
  }
 
  addCheckPermission(value,event){
    if(event.target.checked){
      this.demoChkPerms.push(value);
    }
    else if (!event.target.checked){
      if (this.permission_names == 'undefined') {
        
        let arr_len = this.permission_names.length;
        this.demoChkPerms = [];
          for (let i = 0; i < arr_len; i++) {
              if (this.permission_names[i] != value) {
                  this.demoChkPerms.push(this.permission_names[i]);  
              }
        }
        this.permission_names = this.demoChkPerms;
      } else {
        let indexx = this.demoChkPerms.indexOf(value);
        this.demoChkPerms.splice(indexx,1);
      }
    }
    this.current_permission = this.demoChkPerms;
  }

  getModuleId(id) {
    return this.checkedId(this.module_names, id);
  }

  getMenuId(id) {
    return this.checkedId(this.menu_names, id);
  }

  getPermsId(id) {
    return this.checkedId(this.permission_names, id);
  }

  getModuleIds(id) {
    return this.checkedId(this.demoChkModule, id);
  }

  getMenuIds(id) {
    return this.checkedId(this.demoChkMenu, id);
  }

  getPermsIds(id) {
    return this.checkedId(this.demoChkPerms, id);
  }

  checkedId(arr_names, id) {
    if(!arr_names) {
      return false;
    } else {
      let len = arr_names.length;
      for(let i = 0; i < len; i++) {
        if(id == arr_names[i]) {
          return true;
        }           
      }
    }
  }

  

}