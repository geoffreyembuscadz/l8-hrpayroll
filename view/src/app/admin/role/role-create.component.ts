import { Component, OnInit, Injectable, TemplateRef, Input } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { Observable } from "rxjs/Rx";
import { Response, Headers } from '@angular/http';

import { Role } from '../../model/role';
import { RoleService } from '../../services/role.service';
import { PermissionService } from '../../services/permission.service';
import { MainSideService } from '../../services/main-side.service';
import { AuthUserService } from '../../services/auth-user.service';

import { Select2OptionData } from 'ng2-select2';

@Component({
  selector: 'admin-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: ['./role-create.component.css']
})
export class RoleCreateComponent extends DialogComponent<null, boolean>  {
 
  perm_rec: any;

  public error_title: string;
  public error_message: string;
  public poststore: any;
  private success_title: string;
  private success_message: string;
  public roles = new Role();

  error = false;
  elem: any;
  errorMessage = '';
  createRoleForm : FormGroup;

  public PermissionData: Array<Select2OptionData>;
  public value: string[];
  public current_permission: any;
  public current_module: any;
  public current_menu: any;


  public options: Select2Options;
  public user_id: any;
  public checkedModule: boolean= false;
  public checkedMenu: boolean= false;
  public module: boolean= false;
  public mod_id: string;
  public parent_id: string;
  public module_names: any;
  public menu_names: any;
  public permission_names: any;

  user_side: any;
  user_menu: any;

  checkModule = []
  demoChkModule= [];
  checkMenu = []
  demoChkMenu= [];
  checkPerms = []
  demoChkPerms= [];

  menuId = [];
  setMenuId = [];
  module_id: any;
  menu_id: any;


  checkedModuleId: string;
  checkedMenuId: string;
  checkedPermsId: string;



  bodyClasses:string = "skin-blue sidebar-mini";
  body = document.getElementsByTagName('body')[0];
  checkboxes = document.getElementsByName('permission_id');

  constructor(private _auth_service: AuthUserService, private _rt: Router, private _main_side_service: MainSideService, dialogService: DialogService, private _role_service: RoleService, private _fb: FormBuilder, private _perms_service: PermissionService) { 
  	super(dialogService);
  	
  	this.createRoleForm = _fb.group({
	    'name': ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(32)])],
	    'display_name': ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(32)])],
	    'description': ['', Validators.compose([ Validators.maxLength(64)])],
	    'permission_id' : [''],
      'module_id' : [''],
      'menu_id' : [''],
      'checked' :[''],
      'created_by' :[''],
	    });
  }

  onSubmit() {
    if (this.current_permission != undefined && this.current_module != undefined && this.current_menu != undefined) {
      this.createRoleForm.patchValue({
        'permission_id': this.current_permission
      });

      this.createRoleForm.patchValue({
        'module_id': this.current_module
      });

      this.createRoleForm.patchValue({
        'menu_id': this.current_menu
      });

      this.createRoleForm.patchValue({
        'created_by': this.user_id
      });

    	let  role_model = this.createRoleForm.value;

    	this._role_service
    	.storeRole(role_model)
    	.subscribe(
    		data => {
    			this.poststore = Array.from(data);
    			this.success_title = "Success";
          this.success_message = "A new role record was successfully added.";
          setTimeout(() => {
               this.close();
              }, 2000);
          this._rt.navigate(['/admin/role-list']);
    		},
    		err => this.catchError(err)
    	);
    } else {
      this.error_title = 'Error 400';
      this.error_message = 'Please select atleast one Permission.';
    }
  }

  private catchError(error: any){
  	let response_body = error._body;
  	let response_status = error.status;

  	if( response_status == 500 ){
  		this.error_title = 'Error 500';
  		this.error_message = 'Role name already exist.';
  	} else if( response_status == 200 ) {
  		this.error_title = '';
  		this.error_message = '';
  	} 
  }
  
  ngOnInit() {
    let main_side = this._main_side_service.getModule()
        .subscribe(
            data => {
                this.user_side = Array.from(data);
            }

         );

     let created_by = this._auth_service.getUser().
        subscribe(
          data => {
            let user = data; 
            this.user_id = user.id;
          },
          err => console.error(err)
        );
  }

  private clearErrorMsg(){
    this.error_title = '';
    this.error_message = '';
  }

  addCheckModule(value,event) {
     // check the action 
    if(event.target.checked){ // switch on
      // push the module (value) to demoChkModule array
      this.demoChkModule.push(value);
      // get the length of the module on the database
      let length = this.user_side.length;
      // loop thru
      for (var i = 0; i < length; i++) {
        let lengthx = this.user_side[i].menu.length;
        for (var x = 0; x < lengthx; x++) {
          // if (value)module.id == menu.parent_id
          if (value == this.user_side[i].menu[x].parent_id) {
            // include the menu id on the demoChkMenu array
            this.demoChkMenu.push(this.user_side[i].menu[x].id);
             // check the menu on front end
            this.checkedMenu = true;
            // get the length of the permissions from db
            let lengthy = this.user_side[i].menu[x].permission.length;
            // loop 
            for (var y = 0; y < lengthy; y++) {
              // if menu.id == permission.parent_id
              if (this.user_side[i].menu[x].id == this.user_side[i].menu[x].permission[y].parent_id) {
                // include the permissions ids on demoChkPerms array
                this.demoChkPerms.push(this.user_side[i].menu[x].permission[y].id)
              }
            }
          }
        }
      }
    }
    else if (!event.target.checked){ // switch off
      // get the index of the unclicked module
      let indexx = this.demoChkModule.indexOf(value);
      // remove from array
      this.demoChkModule.splice(indexx,1);
      // get the length on db
      let length = this.user_side.length;
      for (var i = 0; i < length; i++) {
        // get the menu length
        let lengthx = this.user_side[i].menu.length;
        for (var x = 0; x < lengthx; x++) {
          // if module.id == menu.parent_id
          if (value == this.user_side[i].menu[x].parent_id) {
            // get the index of the module
            let indexxx = this.demoChkMenu.indexOf(this.user_side[i].menu[x].id);
            // remove from array
            this.demoChkMenu.splice(indexxx, 1);
            // get the length of permission on DB
            let lengthy = this.user_side[i].menu[x].permission.length;
            // loop
            for (var y = 0; y < lengthy; y++) {
              // if menu.id == permission.parent_id
              if (this.user_side[i].menu[x].id == this.user_side[i].menu[x].permission[y].parent_id) {
                // get the index of the permission
                let indexxxx = this.demoChkPerms.indexOf(this.user_side[i].menu[x].permission[y].id);
                this.demoChkPerms.splice(indexxxx, 1);
              }
            }
          }
        }
      }
    }
    this.current_permission = this.demoChkPerms;
    this.current_menu = this.demoChkMenu;
    this.current_module = this.demoChkModule;
  
  }

  addCheckMenu(value,event){
    // get action
    if(event.target.checked){ // if switched on
      // push the menu value
      this.demoChkMenu.push(value);
      let length = this.user_side.length;
      for (var i = 0; i < length; i++) {
        let lengthx = this.user_side[i].menu.length;
        for (var x = 0; x < lengthx; x++) {
          // get the length
          let lengthy = this.user_side[i].menu[x].permission.length;
          for (var y = 0; y < lengthy; y++) {
            // if menu.id == permission.parent_id
            if (value == this.user_side[i].menu[x].permission[y].parent_id) {
              // push the permissions idss
              this.demoChkPerms.push(this.user_side[i].menu[x].permission[y].id)
            }
          }
        }
      }
    }
    else if (!event.target.checked){
      let indexx = this.demoChkMenu.indexOf(value);
      this.demoChkMenu.splice(indexx,1);
      let length = this.user_side.length;
      for (var i = 0; i < length; i++) {
        let lengthx = this.user_side[i].menu.length;
        for (var x = 0; x < lengthx; x++) {
          let lengthy = this.user_side[i].menu[x].permission.length;
          for (var y = 0; y < lengthy; y++) {
            if (value == this.user_side[i].menu[x].permission[y].parent_id) {
              let indexxxx = this.demoChkPerms.indexOf(this.user_side[i].menu[x].permission[y].id);
              this.demoChkPerms.splice(indexxxx, 1);

            }
          }
        }
      }
    }
    this.current_permission = this.demoChkPerms;
    this.current_menu = this.demoChkMenu;
  
  }

  mustBeChecked(control: FormControl): {[key: string]: string} {
    if (!control.value) {
      return {mustBeCheckedError: 'Must be checked'};
    } else {
      return null;
    }
  }
 
  addCheckPermission(value,event){
    if(event.target.checked){
      this.demoChkPerms.push(value);
      
    }
    else if (!event.target.checked){
      let indexx = this.demoChkPerms.indexOf(value);
      this.demoChkPerms.splice(indexx,1);
    }

    this.current_permission = this.demoChkPerms;
    
  }


  getModuleId(id) {
    return this.checkedId(this.demoChkModule, id);
  }

  getMenuId(id) {
    return this.checkedId(this.demoChkMenu, id);
  }

  getPermsId(id) {
    return this.checkedId(this.demoChkPerms, id);
  }

  checkedId(arr_names, id) {
    if(!arr_names) {
      return false;
    } else {
      let len = arr_names.length;
      for(let i = 0; i < len; i++) {
        if(id == arr_names[i]) {
          return true;
        }           
      }
    }
  }
  
}
