import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from './../../app.config';

import { Role } from '../../model/role';
import { RoleCreateComponent } from './role-create.component';
import { RoleEditComponent } from './role-edit.component';
import { PermissionService } from '../../services/permission.service';
import { RoleService } from '../../services/role.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';


@Component({
  selector: 'admin-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})

@Injectable()
export class RoleListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();

	public id: string;
	public role_id: any;
	public role_name: any;
	public role = new Role();

	public confirm_archiving: any;

	public error_title: string;
    public error_message: string;
    ad_roles: any;

	dtOptions: any = {};
	roles_rec: any;
    perm_rec: any;
    
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];


	constructor(
		private _conf: Configuration, 
		private _perms_service: PermissionService, 
		private modalService: DialogService, 
		private _ar: ActivatedRoute, 
		private _role_service: RoleService, 
		private _rt: Router, 
		private _fb: FormBuilder
		){
		//add the the body classes
	    this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");
    }
    
	ngOnInit() {


		let perms = this._perms_service.getPermissions().
		subscribe(
			data => {
				this.perm_rec = Array.from(data); // fetched record
				
			},
			err => console.error(err)
		);

		let roles = this._role_service.getRoles()
			.subscribe(data => {
				this.ad_roles = Array.from(data);
            	this.rerender();
			},
			err => this.catchError(err)
		);
		
	}

	clickRow(id, name) {
		this._role_service.setName(name);
	    this._role_service.setId(id);
	    this.openEditModal();
	}


	private catchError(error: any){
	    let response_body = error._body;
	    let response_status = error.status;

	    if( response_status == 500 ){
	      this.error_title = 'Error 500';
	      this.error_message = 'Something went wrong';
	    } else if( response_status == 200 ) {
	      this.error_title = '';
	      this.error_message = '';
	    }
	}

	openEditModal() {
		let disposable = this.modalService.addDialog(RoleEditComponent, {
            title:'Update Role'
        	}).subscribe((isConfirmed)=>{
                let roles = this._role_service.getRoles()
					.subscribe(data => {
						this.ad_roles = Array.from(data);
		            	this.rerender();
					},
					err => this.catchError(err)
				);
            });
    }

    openAddModal() {
		let disposable = this.modalService.addDialog(RoleCreateComponent, {
            title:'Add Role'
        	}).subscribe((isConfirmed)=>{
                let roles = this._role_service.getRoles()
					.subscribe(data => {
						this.ad_roles = Array.from(data);
		            	this.rerender();
					},
					err => this.catchError(err)
				);
            });
    }

	ngOnDestroy() {
	    //remove the the body classes
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	 }

	 ngAfterViewInit(): void {
    	this.dtTrigger.next();
     }
	 rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      // Destroy the table first
	      dtInstance.destroy();
	      // Call the dtTrigger to rerender again
	      this.dtTrigger.next();
	    });
    }

}





