var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Role } from '../../model/role';
import { RoleService } from '../../services/role.service';
import { PermissionService } from '../../services/permission.service';
import { MainSideService } from '../../services/main-side.service';
import { AuthUserService } from '../../services/auth-user.service';
var RoleCreateComponent = /** @class */ (function (_super) {
    __extends(RoleCreateComponent, _super);
    function RoleCreateComponent(_auth_service, _rt, _main_side_service, dialogService, _role_service, _fb, _perms_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._auth_service = _auth_service;
        _this._rt = _rt;
        _this._main_side_service = _main_side_service;
        _this._role_service = _role_service;
        _this._fb = _fb;
        _this._perms_service = _perms_service;
        _this.roles = new Role();
        _this.error = false;
        _this.errorMessage = '';
        _this.checkedModule = false;
        _this.checkedMenu = false;
        _this.module = false;
        _this.checkModule = [];
        _this.demoChkModule = [];
        _this.checkMenu = [];
        _this.demoChkMenu = [];
        _this.checkPerms = [];
        _this.demoChkPerms = [];
        _this.menuId = [];
        _this.setMenuId = [];
        _this.bodyClasses = "skin-blue sidebar-mini";
        _this.body = document.getElementsByTagName('body')[0];
        _this.checkboxes = document.getElementsByName('permission_id');
        _this.createRoleForm = _fb.group({
            'name': ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(32)])],
            'display_name': ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(32)])],
            'description': ['', Validators.compose([Validators.maxLength(64)])],
            'permission_id': [''],
            'module_id': [''],
            'menu_id': [''],
            'checked': [''],
            'created_by': [''],
        });
        return _this;
    }
    RoleCreateComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.current_permission != undefined && this.current_module != undefined && this.current_menu != undefined) {
            this.createRoleForm.patchValue({
                'permission_id': this.current_permission
            });
            this.createRoleForm.patchValue({
                'module_id': this.current_module
            });
            this.createRoleForm.patchValue({
                'menu_id': this.current_menu
            });
            this.createRoleForm.patchValue({
                'created_by': this.user_id
            });
            var role_model = this.createRoleForm.value;
            this._role_service
                .storeRole(role_model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success";
                _this.success_message = "A new role record was successfully added.";
                setTimeout(function () {
                    _this.close();
                }, 2000);
                _this._rt.navigate(['/admin/role-list']);
            }, function (err) { return _this.catchError(err); });
        }
        else {
            this.error_title = 'Error 400';
            this.error_message = 'Please select atleast one Permission.';
        }
    };
    RoleCreateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Role name already exist.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    RoleCreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        var main_side = this._main_side_service.getModule()
            .subscribe(function (data) {
            _this.user_side = Array.from(data);
        });
        var created_by = this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
        }, function (err) { return console.error(err); });
    };
    RoleCreateComponent.prototype.clearErrorMsg = function () {
        this.error_title = '';
        this.error_message = '';
    };
    RoleCreateComponent.prototype.addCheckModule = function (value, event) {
        // check the action 
        if (event.target.checked) { // switch on
            // push the module (value) to demoChkModule array
            this.demoChkModule.push(value);
            // get the length of the module on the database
            var length_1 = this.user_side.length;
            // loop thru
            for (var i = 0; i < length_1; i++) {
                var lengthx = this.user_side[i].menu.length;
                for (var x = 0; x < lengthx; x++) {
                    // if (value)module.id == menu.parent_id
                    if (value == this.user_side[i].menu[x].parent_id) {
                        // include the menu id on the demoChkMenu array
                        this.demoChkMenu.push(this.user_side[i].menu[x].id);
                        // check the menu on front end
                        this.checkedMenu = true;
                        // get the length of the permissions from db
                        var lengthy = this.user_side[i].menu[x].permission.length;
                        // loop 
                        for (var y = 0; y < lengthy; y++) {
                            // if menu.id == permission.parent_id
                            if (this.user_side[i].menu[x].id == this.user_side[i].menu[x].permission[y].parent_id) {
                                // include the permissions ids on demoChkPerms array
                                this.demoChkPerms.push(this.user_side[i].menu[x].permission[y].id);
                            }
                        }
                    }
                }
            }
        }
        else if (!event.target.checked) { // switch off
            // get the index of the unclicked module
            var indexx = this.demoChkModule.indexOf(value);
            // remove from array
            this.demoChkModule.splice(indexx, 1);
            // get the length on db
            var length_2 = this.user_side.length;
            for (var i = 0; i < length_2; i++) {
                // get the menu length
                var lengthx = this.user_side[i].menu.length;
                for (var x = 0; x < lengthx; x++) {
                    // if module.id == menu.parent_id
                    if (value == this.user_side[i].menu[x].parent_id) {
                        // get the index of the module
                        var indexxx = this.demoChkMenu.indexOf(this.user_side[i].menu[x].id);
                        // remove from array
                        this.demoChkMenu.splice(indexxx, 1);
                        // get the length of permission on DB
                        var lengthy = this.user_side[i].menu[x].permission.length;
                        // loop
                        for (var y = 0; y < lengthy; y++) {
                            // if menu.id == permission.parent_id
                            if (this.user_side[i].menu[x].id == this.user_side[i].menu[x].permission[y].parent_id) {
                                // get the index of the permission
                                var indexxxx = this.demoChkPerms.indexOf(this.user_side[i].menu[x].permission[y].id);
                                this.demoChkPerms.splice(indexxxx, 1);
                            }
                        }
                    }
                }
            }
        }
        this.current_permission = this.demoChkPerms;
        this.current_menu = this.demoChkMenu;
        this.current_module = this.demoChkModule;
    };
    RoleCreateComponent.prototype.addCheckMenu = function (value, event) {
        // get action
        if (event.target.checked) { // if switched on
            // push the menu value
            this.demoChkMenu.push(value);
            var length_3 = this.user_side.length;
            for (var i = 0; i < length_3; i++) {
                var lengthx = this.user_side[i].menu.length;
                for (var x = 0; x < lengthx; x++) {
                    // get the length
                    var lengthy = this.user_side[i].menu[x].permission.length;
                    for (var y = 0; y < lengthy; y++) {
                        // if menu.id == permission.parent_id
                        if (value == this.user_side[i].menu[x].permission[y].parent_id) {
                            // push the permissions idss
                            this.demoChkPerms.push(this.user_side[i].menu[x].permission[y].id);
                        }
                    }
                }
            }
        }
        else if (!event.target.checked) {
            var indexx = this.demoChkMenu.indexOf(value);
            this.demoChkMenu.splice(indexx, 1);
            var length_4 = this.user_side.length;
            for (var i = 0; i < length_4; i++) {
                var lengthx = this.user_side[i].menu.length;
                for (var x = 0; x < lengthx; x++) {
                    var lengthy = this.user_side[i].menu[x].permission.length;
                    for (var y = 0; y < lengthy; y++) {
                        if (value == this.user_side[i].menu[x].permission[y].parent_id) {
                            var indexxxx = this.demoChkPerms.indexOf(this.user_side[i].menu[x].permission[y].id);
                            this.demoChkPerms.splice(indexxxx, 1);
                        }
                    }
                }
            }
        }
        this.current_permission = this.demoChkPerms;
        this.current_menu = this.demoChkMenu;
    };
    RoleCreateComponent.prototype.mustBeChecked = function (control) {
        if (!control.value) {
            return { mustBeCheckedError: 'Must be checked' };
        }
        else {
            return null;
        }
    };
    RoleCreateComponent.prototype.addCheckPermission = function (value, event) {
        if (event.target.checked) {
            this.demoChkPerms.push(value);
        }
        else if (!event.target.checked) {
            var indexx = this.demoChkPerms.indexOf(value);
            this.demoChkPerms.splice(indexx, 1);
        }
        this.current_permission = this.demoChkPerms;
    };
    RoleCreateComponent.prototype.getModuleId = function (id) {
        return this.checkedId(this.demoChkModule, id);
    };
    RoleCreateComponent.prototype.getMenuId = function (id) {
        return this.checkedId(this.demoChkMenu, id);
    };
    RoleCreateComponent.prototype.getPermsId = function (id) {
        return this.checkedId(this.demoChkPerms, id);
    };
    RoleCreateComponent.prototype.checkedId = function (arr_names, id) {
        if (!arr_names) {
            return false;
        }
        else {
            var len = arr_names.length;
            for (var i = 0; i < len; i++) {
                if (id == arr_names[i]) {
                    return true;
                }
            }
        }
    };
    RoleCreateComponent = __decorate([
        Component({
            selector: 'admin-role-create',
            templateUrl: './role-create.component.html',
            styleUrls: ['./role-create.component.css']
        }),
        __metadata("design:paramtypes", [AuthUserService, Router, MainSideService, DialogService, RoleService, FormBuilder, PermissionService])
    ], RoleCreateComponent);
    return RoleCreateComponent;
}(DialogComponent));
export { RoleCreateComponent };
//# sourceMappingURL=role-create.component.js.map