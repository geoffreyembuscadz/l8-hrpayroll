import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import { Select2OptionData } from 'ng2-select2';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { ClearanceService } from '../../../services/clearance.service';
import { ChecklistModalComponent } from '../../checklist-modal/checklist-modal.component';
import { CommonService } from '../../../services/common.service';
import { ClearanceModalComponent } from '../../clearance/clearance-modal/clearance-modal.component';



@Component({
  selector: 'app-clearance-checklist',
  templateUrl: './clearance-checklist.component.html',
  styleUrls: ['./clearance-checklist.component.css']
})
export class ClearanceChecklistComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	dtOptions: any = {};

  	filterForm:any;
	user_id:any;
	supervisor_id:any;
	role_id:any;
	employee_id:any;

	public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public employee_current: any;
	public options: Select2Options;
	emp:any;

	clearance:any;

  	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

  constructor(
  		private modalService: DialogService,
		private _conf: Configuration,
		private daterangepickerOptions: DaterangepickerConfig,
   		private _fb: FormBuilder,
   		private _auth_service: AuthUserService,
   		private _clearance_service: ClearanceService,
   		private _common_service: CommonService,
   	) {

  		this.filterForm = _fb.group({
    	'status_id': 		[null],
		'start_date': 		[null],
		'end_date': 		[null],
		'company_id': 		[null],
		'employee_id': 		[null],
		'supervisor_id': 	[null],
		'role_id': 			[null]
   		});

  		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

   	}

  ngOnInit() {
  		this.dateOption();
		this.getEmployee();
		this.data();
	}


	getEmployee(){

		this._common_service.getEmployee().
		subscribe(
			data => {
				this.emp = Array.from(data);
				this.employee = this.emp;
				this.employee_value = [];
				this.options = {
					multiple: true
				}
				this.employee_current = this.employee_value;
			},
			err => console.error(err)
		);
	}
	changedEmployee(data: any) {
		this.employee_current = data.value;

		if(this.employee_current == 0){
			this.filterForm.value.employee_id = null;
		}
		else{
			this.filterForm.value.employee_id = this.employee_current;
		}
	}

	dateOption(){
    	this.daterangepickerOptions.settings = {
        	singleDatePicker: false
		};
    }

    data(){
		
		let model = this.filterForm.value;

    	this._clearance_service.getClearanceList(model).
	      	subscribe(
	        data => {
	           let dat= data;

	          const result = dat.reduce((acc, elt) => {
			  const {id, name, status, type, val, clearance_list_id, employee_clearance_id, employee_id,remarks,checker_id,clearance_id
} = elt;
			 
			  const existingEltIndex = acc.findIndex(e =>
			    e.id === elt.id);
			  const newEvent = {status, type ,val, clearance_list_id, employee_clearance_id
 , employee_id , remarks,checker_id,clearance_id};
			  
			  if (existingEltIndex < 0) {
			    const newElement = {
			      id,
			      name,
			      employee_id,
			      status,
			      clearance: [newEvent]
			    };
			    return [...acc, newElement];
			  } else {
			    acc[existingEltIndex].clearance.push(newEvent);
			    return acc;
			  }
			}, []);

			let len = result.length;
			let y = 0;
			for (let i = 0; i < len; ++i) {
				for (let x = 0; x < result[i].clearance.length; ++x) {
					
					if (result[i].clearance[x].status != 'DONE') {
						y++;
					}
				}

				if (y!=0) {
					result[i].status = true;
				}else{
					result[i].status = false;
				}
			}


			this.clearance = result;
	        this.rerender();

	        },
	        err => console.error(err)
	      );

	}

	viewAll(x){
		
		let disposable = this.modalService.addDialog(ChecklistModalComponent, {
            title:'Clearance',
            tableVal:true,
            clearance:true,
            data:x.clearance,
            attend:false,
			holiday:false,
  			onboarding:false,
			attendModal:false
        	}).subscribe((isConfirmed)=>{
                this.data();

            });
	}


	createClearance(){
	
		let disposable = this.modalService.addDialog(ClearanceModalComponent, {
            title:'Create Clearance',
            emp:this.emp,
            create:true,
            multi:true
        	}).subscribe((isConfirmed)=>{
        		this.emp = [];
        		this.getEmployee();
                this.data();
            });
	}

	editClearance(id,emp_id,x){
		let v = [];
		v.push(emp_id);
		let employee = v;
	
		let disposable = this.modalService.addDialog(ClearanceModalComponent, {
            title:'Update Clearance',
            emp:this.emp,
            edit:true,
            single:true,
            employee_id:employee,
            clearance_id:id,
            tempData:x.clearance,
        	}).subscribe((isConfirmed)=>{
        		this.emp = [];
        		this.getEmployee();
                this.data();

            });
	}

  	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

}
