var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { FormBuilder } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import { ClearanceService } from '../../../services/clearance.service';
import { ChecklistModalComponent } from '../../checklist-modal/checklist-modal.component';
import { CommonService } from '../../../services/common.service';
import { ClearanceModalComponent } from '../../clearance/clearance-modal/clearance-modal.component';
var ClearanceChecklistComponent = /** @class */ (function () {
    function ClearanceChecklistComponent(modalService, _conf, daterangepickerOptions, _fb, _auth_service, _clearance_service, _common_service) {
        this.modalService = modalService;
        this._conf = _conf;
        this.daterangepickerOptions = daterangepickerOptions;
        this._fb = _fb;
        this._auth_service = _auth_service;
        this._clearance_service = _clearance_service;
        this._common_service = _common_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filterForm = _fb.group({
            'status_id': [null],
            'start_date': [null],
            'end_date': [null],
            'company_id': [null],
            'employee_id': [null],
            'supervisor_id': [null],
            'role_id': [null]
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    ClearanceChecklistComponent.prototype.ngOnInit = function () {
        this.dateOption();
        this.getEmployee();
        this.data();
    };
    ClearanceChecklistComponent.prototype.getEmployee = function () {
        var _this = this;
        this._common_service.getEmployee().
            subscribe(function (data) {
            _this.emp = Array.from(data);
            _this.employee = _this.emp;
            _this.employee_value = [];
            _this.options = {
                multiple: true
            };
            _this.employee_current = _this.employee_value;
        }, function (err) { return console.error(err); });
    };
    ClearanceChecklistComponent.prototype.changedEmployee = function (data) {
        this.employee_current = data.value;
        if (this.employee_current == 0) {
            this.filterForm.value.employee_id = null;
        }
        else {
            this.filterForm.value.employee_id = this.employee_current;
        }
    };
    ClearanceChecklistComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    ClearanceChecklistComponent.prototype.data = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._clearance_service.getClearanceList(model).
            subscribe(function (data) {
            var dat = data;
            var result = dat.reduce(function (acc, elt) {
                var id = elt.id, name = elt.name, status = elt.status, type = elt.type, val = elt.val, clearance_list_id = elt.clearance_list_id, employee_clearance_id = elt.employee_clearance_id, employee_id = elt.employee_id, remarks = elt.remarks, checker_id = elt.checker_id, clearance_id = elt.clearance_id;
                var existingEltIndex = acc.findIndex(function (e) {
                    return e.id === elt.id;
                });
                var newEvent = { status: status, type: type, val: val, clearance_list_id: clearance_list_id, employee_clearance_id: employee_clearance_id,
                    employee_id: employee_id, remarks: remarks, checker_id: checker_id, clearance_id: clearance_id };
                if (existingEltIndex < 0) {
                    var newElement = {
                        id: id,
                        name: name,
                        employee_id: employee_id,
                        status: status,
                        clearance: [newEvent]
                    };
                    return acc.concat([newElement]);
                }
                else {
                    acc[existingEltIndex].clearance.push(newEvent);
                    return acc;
                }
            }, []);
            var len = result.length;
            var y = 0;
            for (var i = 0; i < len; ++i) {
                for (var x = 0; x < result[i].clearance.length; ++x) {
                    if (result[i].clearance[x].status != 'DONE') {
                        y++;
                    }
                }
                if (y != 0) {
                    result[i].status = true;
                }
                else {
                    result[i].status = false;
                }
            }
            _this.clearance = result;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    ClearanceChecklistComponent.prototype.viewAll = function (x) {
        var _this = this;
        var disposable = this.modalService.addDialog(ChecklistModalComponent, {
            title: 'Clearance',
            tableVal: true,
            clearance: true,
            data: x.clearance,
            attend: false,
            holiday: false,
            onboarding: false,
            attendModal: false
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    ClearanceChecklistComponent.prototype.createClearance = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ClearanceModalComponent, {
            title: 'Create Clearance',
            emp: this.emp,
            create: true,
            multi: true
        }).subscribe(function (isConfirmed) {
            _this.emp = [];
            _this.getEmployee();
            _this.data();
        });
    };
    ClearanceChecklistComponent.prototype.editClearance = function (id, emp_id, x) {
        var _this = this;
        var v = [];
        v.push(emp_id);
        var employee = v;
        var disposable = this.modalService.addDialog(ClearanceModalComponent, {
            title: 'Update Clearance',
            emp: this.emp,
            edit: true,
            single: true,
            employee_id: employee,
            clearance_id: id,
            tempData: x.clearance,
        }).subscribe(function (isConfirmed) {
            _this.emp = [];
            _this.getEmployee();
            _this.data();
        });
    };
    ClearanceChecklistComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    ClearanceChecklistComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    ClearanceChecklistComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], ClearanceChecklistComponent.prototype, "dtElement", void 0);
    ClearanceChecklistComponent = __decorate([
        Component({
            selector: 'app-clearance-checklist',
            templateUrl: './clearance-checklist.component.html',
            styleUrls: ['./clearance-checklist.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            DaterangepickerConfig,
            FormBuilder,
            AuthUserService,
            ClearanceService,
            CommonService])
    ], ClearanceChecklistComponent);
    return ClearanceChecklistComponent;
}());
export { ClearanceChecklistComponent };
//# sourceMappingURL=clearance-checklist.component.js.map