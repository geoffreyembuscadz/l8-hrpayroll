var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Configuration } from '../../../app.config';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../../../services/common.service';
import { ClearanceService } from '../../../services/clearance.service';
var ClearanceModalComponent = /** @class */ (function (_super) {
    __extends(ClearanceModalComponent, _super);
    function ClearanceModalComponent(dialogService, _fb, _conf, _rt, _common_service, _clearance_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._conf = _conf;
        _this._rt = _rt;
        _this._common_service = _common_service;
        _this._clearance_service = _clearance_service;
        _this.single = false;
        _this.multi = false;
        _this.emp = [];
        _this.validate = false;
        _this.empStats = false;
        _this.edit = false;
        _this.create = false;
        _this.dupVal = false;
        _this.user = false;
        _this.clearanceVal = false;
        _this.clearance = [];
        _this.checkAllValClearance = false;
        _this.empVal = false;
        _this.tempData = [];
        _this.OBForm = _fb.group({
            'employee_id': [null],
            'checker_id': [null],
            'clearance': [null],
            'created_by': [null]
        });
        return _this;
    }
    ClearanceModalComponent.prototype.ngOnInit = function () {
        this.get_employee();
        this.getClearance();
        this.getEmployeeIndivi();
        if (this.edit == true) {
            this.getEmployeeIndivi();
        }
    };
    ClearanceModalComponent.prototype.getEmpClearance = function () {
        if (this.clearance != null && this.tempData != null) {
            var y = 0;
            for (var i = 0; i < this.clearance.length; ++i) {
                for (var x = 0; x < this.tempData.length; ++x) {
                    if (this.clearance[i].id == this.tempData[x].clearance_id) {
                        this.clearance[i].status = this.tempData[x].status;
                        this.clearance[i].checker_id = this.tempData[x].checker_id;
                        this.clearance[i].val = true;
                        y++;
                    }
                }
            }
            if (y == this.clearance.length) {
                this.checkAllValClearance = true;
            }
            else {
                this.checkAllValClearance = false;
            }
            this.clearanceVal = true;
        }
    };
    ClearanceModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    ClearanceModalComponent.prototype.get_employee = function () {
        this.employee = this.emp;
        this.employee_value = [];
        if (this.edit == true) {
            this.employee_value = this.employee_id;
            this.validate = true;
        }
        this.options = {
            multiple: true
        };
        this.employee_current = this.employee_value;
    };
    ClearanceModalComponent.prototype.changed = function (data) {
        if (this.single == true) {
            var v = [];
            v.push(data.value);
            this.employee_current = v;
        }
        else {
            this.employee_current = data.value;
        }
        var len = this.employee_current.length;
        if (len >= 1) {
            this.empStats = true;
            this.duplicateEntryValidation();
        }
        else {
            this.empStats = false;
        }
    };
    ClearanceModalComponent.prototype.getEmployeeIndivi = function () {
        this.employeeIndiv = [];
        this.employeeIndiv = this.emp;
        this.employeeValueIndiv = [];
        if (this.edit == true) {
            this.employeeValueIndiv = this.employee_id;
            this.validate = true;
        }
        // if(this.create == true){
        var id = 0;
        var text = 'Select Checker';
        this.employeeIndiv.unshift({ id: id, text: text });
        this.employeeValueIndiv = 0;
        // }
    };
    ClearanceModalComponent.prototype.changedIndiv = function (data, i) {
        if (data.value != 0) {
            this.clearance[i].checker_id = data.value;
            var len = this.clearance.length;
            var x = 0;
            var y = 0;
            for (var i_1 = 0; i_1 < len; ++i_1) {
                if (this.clearance[i_1].val == true) {
                    x++;
                }
                if (this.clearance[i_1].checker_id != 0) {
                    y++;
                }
            }
            if (x == y) {
                this.empVal = true;
            }
            else {
                this.empVal = false;
            }
            this.validation();
        }
        else {
            this.validate = false;
        }
    };
    ClearanceModalComponent.prototype.validation = function () {
        if (this.empStats == true && this.dupVal == true && this.clearanceVal == true && this.empVal == true || this.edit == true && this.clearanceVal == true) {
            this.validate = true;
        }
        else {
            this.validate = false;
        }
    };
    ClearanceModalComponent.prototype.getClearance = function () {
        var _this = this;
        this._common_service.getClearance().
            subscribe(function (data) {
            _this.clearance = Array.from(data);
            if (_this.edit == true) {
                _this.getEmpClearance();
            }
        }, function (err) { return console.error(err); });
    };
    ClearanceModalComponent.prototype.clearanceCheckbox = function (index) {
        if (this.clearance[index].val == false) {
            this.clearance[index].val = true;
        }
        else {
            this.clearance[index].val = false;
            this.checkAllValClearance = false;
        }
        var len = this.clearance.length;
        var i = 0;
        for (var x = 0; x < len; ++x) {
            if (this.clearance[x].val == true) {
                i++;
            }
        }
        if (i > 0) {
            this.clearanceVal = true;
            this.duplicateEntryValidation();
        }
        else {
            this.clearanceVal = false;
        }
        this.validation();
    };
    ClearanceModalComponent.prototype.checkAllClearance = function () {
        if (this.checkAllValClearance == false) {
            var leng = this.clearance.length;
            for (var i = 0; i < leng; i++) {
                this.clearance[i].val = true;
            }
            this.checkAllValClearance = true;
            this.clearanceVal = true;
        }
        else {
            var len = this.clearance.length;
            for (var i = 0; i < len; i++) {
                this.clearance[i].val = false;
            }
            this.checkAllValClearance = false;
            this.clearanceVal = false;
        }
        this.validation();
        this.duplicateEntryValidation();
    };
    ClearanceModalComponent.prototype.clearanceCheck = function () {
        var len = this.clearance.length;
        var temp = [];
        for (var i = 0; i < len; ++i) {
            if (this.clearance[i].val == true) {
                temp.push(this.clearance[i].id);
            }
        }
    };
    ClearanceModalComponent.prototype.submit = function () {
        var _this = this;
        if (this.create == true) {
            var len = this.clearance.length;
            var temp = [];
            for (var i = 0; i < len; ++i) {
                if (this.clearance[i].val == true) {
                    var id = this.clearance[i].id;
                    var checker_id = this.clearance[i].checker_id;
                    temp.push({ id: id, checker_id: checker_id });
                }
            }
            var model = {
                clearance: temp,
                employee_id: this.employee_current
            };
            this._clearance_service
                .createClearance(model)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.success_title = "Success";
                _this.success_message = "New clearance request was successfully added.";
                setTimeout(function () {
                    _this.close();
                }, 2000);
            }, function (err) { return _this.catchError(err); });
        }
        if (this.edit == true) {
            var len = this.clearance.length;
            var temp = [];
            for (var i = 0; i < len; ++i) {
                if (this.clearance[i].val == true) {
                    var id = this.clearance[i].id;
                    var checker_id = this.clearance[i].checker_id;
                    temp.push({ id: id, checker_id: checker_id });
                }
            }
            var model = {
                clearance: temp,
                id: this.tempData[0].employee_clearance_id,
                employee_id: this.employee_current
            };
            this._clearance_service
                .updateClearance(model)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.success_title = "Success";
                _this.success_message = "Clearance request was successfully updated.";
                setTimeout(function () {
                    _this.close();
                }, 2000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    ClearanceModalComponent.prototype.duplicateEntryValidation = function () {
        var _this = this;
        this.OBForm.value.employee_id = this.employee_current;
        this.OBForm.value.checker_id = this.checker_id;
        this.OBForm.value.created_by = this.created_by;
        if (this.user == true && this.create == true || this.user == true && this.edit == true) {
            var v = [];
            v.push(this.employee_id);
            this.OBForm.value.employee_id = v;
        }
        if (this.edit == true) {
            this.OBForm.value.edit = this.edit;
            this.OBForm.value.id = this.clearance_id;
        }
        else {
            this.OBForm.value.edit = false;
        }
        var ob = this.OBForm.value;
        if (this.OBForm.value.employee_id != 0 || this.edit == true && this.OBForm.value.employee_id != 0) {
            this._clearance_service
                .duplicateEntryValidation(ob)
                .subscribe(function (data) {
                _this.poststore = data;
                if (_this.poststore.message == "duplicate") {
                    _this.dupVal = false;
                    _this.errorCatch();
                }
                else {
                    _this.dupVal = true;
                    _this.error_title = '';
                    _this.error_message = '';
                }
                _this.validation();
            }, function (err) { return _this.catchError(err); });
        }
    };
    ClearanceModalComponent.prototype.errorCatch = function () {
        var _this = this;
        if (this.poststore.status_code == 200) {
            this.success_title = '200 OK';
            this.success_message = 'No Changes Happen';
            setTimeout(function () {
                _this.close();
            }, 2000);
        }
        else if (this.poststore.status_code == 400) {
            this.error_title = '400 Bad Request';
            this.error_message = 'Request is already exist';
        }
        else {
            this.validate = false;
            this.success_title = "Success";
            this.success_message = "New official business request was successfully added.";
            setTimeout(function () {
                _this.close();
            }, 2000);
        }
    };
    ClearanceModalComponent = __decorate([
        Component({
            selector: 'app-clearance-modal',
            templateUrl: './clearance-modal.component.html',
            styleUrls: ['./clearance-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            Configuration,
            Router,
            CommonService,
            ClearanceService])
    ], ClearanceModalComponent);
    return ClearanceModalComponent;
}(DialogComponent));
export { ClearanceModalComponent };
//# sourceMappingURL=clearance-modal.component.js.map