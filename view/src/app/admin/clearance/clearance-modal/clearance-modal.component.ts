import { Component, OnInit } from '@angular/core';
import { Configuration } from '../../../app.config';
import { Select2OptionData } from 'ng2-select2';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommonService } from '../../../services/common.service';
import { ClearanceService } from '../../../services/clearance.service';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';

@Component({
  selector: 'app-clearance-modal',
  templateUrl: './clearance-modal.component.html',
  styleUrls: ['./clearance-modal.component.css']
})
export class ClearanceModalComponent extends DialogComponent<null, boolean> {
  
	title: string;
	public id: any;
	public status: any;
	public name: string;
	public success_title;
	public success_message;
	public error_title;
	public error_message;
	public poststore: any;

	public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public options: Select2Options;
	public employee_current: any;
	single=false;
	multi=false;
	emp=[];
	employee_id:any;
	validate = false;
	empStats=false;
	edit=false;
	create = false;
	created_by:any;
	checker_id:any;
	dupVal=false;
	OBForm:any;
	user=false;
	clearanceVal = false;
	clearance_id:any;

	public clearance=[];
	checkAllValClearance = false;
	value:any;
	employeeIndiv:any;
	employeeValueIndiv:any;
	empVal = false;
	tempData = [];


 constructor( 
   dialogService: DialogService, 
   private _fb: FormBuilder, 
   private _conf: Configuration,
   private _rt: Router,
   private _common_service: CommonService,
   private _clearance_service: ClearanceService,
   ){
    super(dialogService);

    this.OBForm = _fb.group({
		'employee_id': 		[null],
		'checker_id': 		[null],
		'clearance': 		[null],
		'created_by': 		[null]
    }); 

  }
 
	ngOnInit() {

		this.get_employee();
		this.getClearance();
		this.getEmployeeIndivi();

		if (this.edit == true) {
			this.getEmployeeIndivi();
		}
	}

	getEmpClearance(){

        if (this.clearance != null && this.tempData != null) {
        	let y = 0;
			for (let i = 0; i < this.clearance.length; ++i) {
				for (let x = 0; x < this.tempData.length; ++x) {
					if (this.clearance[i].id == this.tempData[x].clearance_id) {
						this.clearance[i].status = this.tempData[x].status;
						this.clearance[i].checker_id = this.tempData[x].checker_id;
						this.clearance[i].val = true;
						y++;
					}
				}
			}
			if (y==this.clearance.length) {
				this.checkAllValClearance = true;
			}else{
				this.checkAllValClearance = false;
			}

			this.clearanceVal = true;
		}
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	get_employee(){

		this.employee = this.emp;
      	this.employee_value = [];

		if(this.edit == true) {
			this.employee_value = this.employee_id;
			this.validate = true;
	    } 
		this.options = {
			multiple: true
		}
		this.employee_current = this.employee_value;
	}
	
	changed(data: any) {

		if(this.single == true){
			let v = [];
			v.push(data.value);
			this.employee_current = v;
		}
		else{
	  		this.employee_current = data.value;	
		}


	   	let len = this.employee_current.length;

		if(len >= 1){
			this.empStats = true;
			this.duplicateEntryValidation();
		}
		else{
			this.empStats = false;
		}

	}

	getEmployeeIndivi(){

		this.employeeIndiv = [];

		this.employeeIndiv = this.emp;
      	this.employeeValueIndiv = [];

		if(this.edit == true) {
			this.employeeValueIndiv = this.employee_id;
			this.validate = true;
	    }
	    // if(this.create == true){
			let id = 0;
	        let text = 'Select Checker';

	        this.employeeIndiv.unshift({id,text});

	        this.employeeValueIndiv = 0;
		// }
	}

	changedIndiv(data: any,i) {

		if (data.value != 0) {
			
	  		this.clearance[i].checker_id = data.value;

	  		let len = this.clearance.length;
	  		let x = 0;
	  		let y = 0;
	  		for (let i = 0; i < len; ++i) {
	  			if (this.clearance[i].val == true) {
	  				x++;
	  			}
	  			if (this.clearance[i].checker_id != 0) {
	  				y++;
	  			}
	  		}

	  		if (x==y) {
	  			this.empVal = true;
	  		}else{
	  			this.empVal = false;
	  		}

	  		this.validation();
		}else{
			this.validate=false;
		}

	}

	validation(){
		
		if(this.empStats == true && this.dupVal == true && this.clearanceVal == true && this.empVal == true|| this.edit == true && this.clearanceVal == true){
      		this.validate=true;
     	}
     	else{
     		this.validate=false;
     	}
	}

	getClearance(){
		
		this._common_service.getClearance().
		subscribe(
			data => {
				this.clearance = Array.from(data);
				if (this.edit == true) {
					this.getEmpClearance();
				}
			},
			err => console.error(err)
		);
	}

	clearanceCheckbox(index:any){

		if(this.clearance[index].val == false){
			this.clearance[index].val = true;
		}
		else{
			this.clearance[index].val = false;
			this.checkAllValClearance = false;
		}

		let len = this.clearance.length;
		let i = 0;
		for (let x = 0; x < len; ++x) {
			
			if(this.clearance[x].val == true){
				i++;
			}
		}

		if(i > 0){
			this.clearanceVal = true;
			this.duplicateEntryValidation();
		}
		else{
			this.clearanceVal = false;
		}

		this.validation();
	}

	checkAllClearance(){

		if(this.checkAllValClearance == false){
			let leng = this.clearance.length;
			for (let i = 0; i < leng; i++) {
				this.clearance[i].val = true;
			}

			this.checkAllValClearance = true;
			this.clearanceVal = true;
		}
		else{
			let len = this.clearance.length;
			for (let i = 0; i < len; i++) {
				this.clearance[i].val = false;
			}
			this.checkAllValClearance = false;
			this.clearanceVal = false;
		}

		this.validation();
		this.duplicateEntryValidation();
	}

	clearanceCheck(){
		let len = this.clearance.length;
		let temp = [];
		for (let i = 0; i < len; ++i) {
			if (this.clearance[i].val == true) {
				temp.push(this.clearance[i].id);
			}
		}
	}

	submit(){

		if (this.create == true) {
			let len = this.clearance.length;
			let temp = [];
			for (let i = 0; i < len; ++i) {
				if (this.clearance[i].val == true) {
					let id = this.clearance[i].id;
					let checker_id = this.clearance[i].checker_id;
					temp.push({id,checker_id});
				}
			}

			let model = {
				clearance:temp,
				employee_id:this.employee_current
			}

			this._clearance_service
			.createClearance(model)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.success_title = "Success";
					this.success_message = "New clearance request was successfully added.";
					setTimeout(() => {
			  		   this.close();
			      	}, 2000);
				},
				err => this.catchError(err)
			);
		}

		if (this.edit == true) {
			let len = this.clearance.length;
			let temp = [];
			for (let i = 0; i < len; ++i) {
				if (this.clearance[i].val == true) {
					let id = this.clearance[i].id;
					let checker_id = this.clearance[i].checker_id
					temp.push({id,checker_id});
				}
			}

			let model = {
				clearance:temp,
				id:this.tempData[0].employee_clearance_id,
				employee_id:this.employee_current
			}

			this._clearance_service
			.updateClearance(model)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.success_title = "Success";
					this.success_message = "Clearance request was successfully updated.";
					setTimeout(() => {
			  		   this.close();
			      	}, 2000);
				},
				err => this.catchError(err)
			);
		}
	}

	duplicateEntryValidation(){
		this.OBForm.value.employee_id = this.employee_current;
		this.OBForm.value.checker_id = this.checker_id;
		this.OBForm.value.created_by = this.created_by;
		
		if(this.user == true  && this.create == true || this.user == true && this.edit == true){
			let v = [];
			v.push(this.employee_id);
			this.OBForm.value.employee_id = v;
		}

		if(this.edit == true){
			this.OBForm.value.edit = this.edit;
			this.OBForm.value.id = this.clearance_id;
		}
		else{
			this.OBForm.value.edit = false;
		}

		let ob = this.OBForm.value;

		if(this.OBForm.value.employee_id != 0 || this.edit == true && this.OBForm.value.employee_id != 0){
			
			this._clearance_service
			.duplicateEntryValidation(ob)
			.subscribe(
				data => {
					this.poststore = data;
					if(this.poststore.message == "duplicate"){
							this.dupVal = false;
							this.errorCatch();
					}
					else{
						this.dupVal = true;
						this.error_title = '';
						this.error_message = '';
					}

					this.validation();
				},
				err => this.catchError(err)
			);
		}
	}

	k:any;
	errorCatch(){
		if(this.poststore.status_code == 200){
			this.success_title = '200 OK';
			this.success_message = 'No Changes Happen';
			setTimeout(() => {
	  		   this.close();
	      	}, 2000);
		}
		else if(this.poststore.status_code == 400){
			this.error_title = '400 Bad Request';
			this.error_message = 'Request is already exist';
		}
		else{
			this.validate = false;
			this.success_title = "Success";
			this.success_message = "New official business request was successfully added.";
			setTimeout(() => {
	  		   this.close();
	      	}, 2000);
		}
	}



}
