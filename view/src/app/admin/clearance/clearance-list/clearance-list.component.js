var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { FormBuilder } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import { ClearanceService } from '../../../services/clearance.service';
import { TypeModalComponent } from '../../type-modal/type-modal.component';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
var ClearanceListComponent = /** @class */ (function () {
    function ClearanceListComponent(modalService, _conf, daterangepickerOptions, _fb, _auth_service, _clearance_service) {
        this.modalService = modalService;
        this._conf = _conf;
        this.daterangepickerOptions = daterangepickerOptions;
        this._fb = _fb;
        this._auth_service = _auth_service;
        this._clearance_service = _clearance_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filterForm = _fb.group({
            'status_id': [null],
            'start_date': [null],
            'end_date': [null],
            'company_id': [null],
            'employee_id': [null],
            'supervisor_id': [null],
            'role_id': [null]
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    ClearanceListComponent.prototype.ngOnInit = function () {
        this.dateOption();
        this.data();
    };
    ClearanceListComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    ClearanceListComponent.prototype.data = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._clearance_service.getClearance().
            subscribe(function (data) {
            _this.clearance = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    ClearanceListComponent.prototype.createClearance = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Create Clearance',
            button: 'Add',
            url: 'clearance',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    ClearanceListComponent.prototype.editClearance = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Update Clearance',
            button: 'Update',
            edit: true,
            url: 'clearance',
            id: id
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    ClearanceListComponent.prototype.archive = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'clearance'
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    ClearanceListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    ClearanceListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    ClearanceListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], ClearanceListComponent.prototype, "dtElement", void 0);
    ClearanceListComponent = __decorate([
        Component({
            selector: 'app-clearance-list',
            templateUrl: './clearance-list.component.html',
            styleUrls: ['./clearance-list.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            DaterangepickerConfig,
            FormBuilder,
            AuthUserService,
            ClearanceService])
    ], ClearanceListComponent);
    return ClearanceListComponent;
}());
export { ClearanceListComponent };
//# sourceMappingURL=clearance-list.component.js.map