import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import { Select2OptionData } from 'ng2-select2';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { ClearanceService } from '../../../services/clearance.service';
import { TypeModalComponent } from '../../type-modal/type-modal.component';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';


@Component({
  selector: 'app-clearance-list',
  templateUrl: './clearance-list.component.html',
  styleUrls: ['./clearance-list.component.css']
})
export class ClearanceListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	dtOptions: any = {};

  	filterForm:any;
	user_id:any;
	supervisor_id:any;
	role_id:any;
	employee_id:any;

	clearance:any;

  	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

  constructor(
  		private modalService: DialogService,
		private _conf: Configuration,
		private daterangepickerOptions: DaterangepickerConfig,
   		private _fb: FormBuilder,
   		private _auth_service: AuthUserService,
   		private _clearance_service: ClearanceService,
   	) {

  		this.filterForm = _fb.group({
    	'status_id': 		[null],
		'start_date': 		[null],
		'end_date': 		[null],
		'company_id': 		[null],
		'employee_id': 		[null],
		'supervisor_id': 	[null],
		'role_id': 			[null]
   		});

  		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

   	}

  	ngOnInit() {
  		this.dateOption();
  		this.data();
	}

	dateOption(){
    	this.daterangepickerOptions.settings = {
        	singleDatePicker: false
		};
    }

    data(){
		
		let model = this.filterForm.value;

    	this._clearance_service.getClearance().
	      	subscribe(
	        data => {
	          this.clearance= Array.from(data);
	          this.rerender();
	        },
	        err => console.error(err)
	      );

	}

	createClearance() {

		let disposable = this.modalService.addDialog(TypeModalComponent, {
            title:'Create Clearance',
            button:'Add',
		    url:'clearance',
		    create:true
        	}).subscribe((isConfirmed)=>{
                this.data();

            });
	}

	editClearance(id:any) {

		let disposable = this.modalService.addDialog(TypeModalComponent, {
            title:'Update Clearance',
            button:'Update',
		    edit:true,
		    url:'clearance',
		    id:id
        	}).subscribe((isConfirmed)=>{
                this.data();
        });
	}

	archive(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'clearance'
        	}).subscribe((isConfirmed)=>{
		       	this.data();
        });
	}

  	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

}
