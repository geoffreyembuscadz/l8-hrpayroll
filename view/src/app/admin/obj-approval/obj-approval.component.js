var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core'; //1
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Configuration } from '../../app.config';
import { DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import { AppraisalService } from '../../services/appraisal.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { ObjRateComponent } from '../obj-approval/obj-rate/obj-rate.component';
import { AlertModalComponent } from '../alert-modal/alert-modal.component';
import { AuthUserService } from '../../services/auth-user.service';
import * as moment from 'moment';
var ObjApprovalComponent = /** @class */ (function () {
    function ObjApprovalComponent(dialogService, _perms_service, _auth_service, _appraisal_service, modalService, _ar, _rt, _fb, _conf) {
        this._perms_service = _perms_service;
        this._auth_service = _auth_service;
        this._appraisal_service = _appraisal_service;
        this.modalService = modalService;
        this._ar = _ar;
        this._rt = _rt;
        this._fb = _fb;
        this._conf = _conf;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.selectedEmployee = null;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.today = moment().format("YYYY-MM-DD");
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.api_position = this._conf.ServerWithApiUrl + 'position/';
        this.Form =
            _fb.group({
                'supervisor_id': ['null'],
                'employee_id': ['null']
            });
    }
    ObjApprovalComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    ObjApprovalComponent.prototype.ngOnInit = function () {
        this.getEmployee();
        this.get_UserId();
    };
    ObjApprovalComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    ObjApprovalComponent.prototype.getEmployee = function () {
        var _this = this;
        this._appraisal_service.getEmployee().
            subscribe(function (data) {
            _this.employee = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    ObjApprovalComponent.prototype.setActiveEmployee = function (employee) {
        this.selectedEmployee = employee;
        var myJSON = JSON.stringify(this.selectedEmployee);
        this.getObjectives();
        this.getObjNow();
    };
    ObjApprovalComponent.prototype.getObjectives = function () {
        var _this = this;
        this.Form.value.employee_id = this.selectedEmployee.id;
        var model = this.Form.value;
        this._appraisal_service.getObjectives(model).subscribe(function (data) {
            _this.obj = Array.from(data);
            for (var x = 0; x < _this.obj.length; ++x) {
                _this.dates = _this.obj[x].due;
            }
        }, function (err) { return console.error(err); });
    };
    ObjApprovalComponent.prototype.approvedObj = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(AlertModalComponent, {
            title: 'Objective',
            message: 'Are you sure to approve this objective',
            id: id,
            attValEmp: false,
            approved: true,
            updated_by: this.user_id,
            status: 4
        }).subscribe(function (isConfirmed) {
            _this.getObjectives();
        });
    };
    ObjApprovalComponent.prototype.get_UserId = function () {
        var _this = this;
        this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
        }, function (err) { return console.error(err); });
    };
    ObjApprovalComponent.prototype.rejectObj = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(AlertModalComponent, {
            title: 'Objective',
            message: 'Are you sure to reject this objective',
            id: id,
            attValEmp: false,
            approved: true,
            updated_by: this.user_id,
            status: 2
        }).subscribe(function (isConfirmed) {
            _this.getObjectives();
        });
    };
    ObjApprovalComponent.prototype.getObjNow = function () {
        var _this = this;
        this.Form.value.employee_id = this.selectedEmployee.id;
        var model = this.Form.value;
        this._appraisal_service.getObjNow(model).subscribe(function (data) {
            _this.objectives = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    ObjApprovalComponent.prototype.viewObj = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ObjRateComponent, {
            title: 'Objective',
            button: 'Done',
            view: true,
            url: 'appraisal',
            employee_id: this.selectedEmployee.id,
            id: id
        }).subscribe(function (isConfirmed) {
            _this.ngOnInit();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], ObjApprovalComponent.prototype, "dtElement", void 0);
    ObjApprovalComponent = __decorate([
        Component({
            selector: 'app-obj-approval',
            templateUrl: './obj-approval.component.html',
            styleUrls: ['./obj-approval.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            PermissionService,
            AuthUserService,
            AppraisalService,
            DialogService,
            ActivatedRoute,
            Router,
            FormBuilder,
            Configuration])
    ], ObjApprovalComponent);
    return ObjApprovalComponent;
}());
export { ObjApprovalComponent };
//# sourceMappingURL=obj-approval.component.js.map