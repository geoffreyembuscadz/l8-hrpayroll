var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { QuestionService } from '../../../services/question.service';
import { AppraisalService } from '../../../services/appraisal.service';
var ObjRateComponent = /** @class */ (function (_super) {
    __extends(ObjRateComponent, _super);
    function ObjRateComponent(dialogService, _fb, _ar, _q_service, _appraisal_service, _rt, modalService) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._q_service = _q_service;
        _this._appraisal_service = _appraisal_service;
        _this._rt = _rt;
        _this.modalService = modalService;
        _this.Form = _fb.group({
            'id': ['null'],
            'employee_id': ['null'],
            'feedback': ['null'],
            'rate_value': ['null']
        });
        return _this;
    }
    ObjRateComponent.prototype.ngOnInit = function () {
        this.getObjRate();
    };
    ObjRateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    ObjRateComponent.prototype.onSubmit = function () {
        var _this = this;
        var id = this.id;
        this.Form.value.obj_id = id;
        this.Form.value.employee_id = this.employee_id;
        var model = this.Form.value;
        console.log(model);
        if (this.view == true) {
            this._appraisal_service.saveRateObj(model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success!";
                _this.success_message = "Successfully updated";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    ObjRateComponent.prototype.getObjRate = function () {
        var _this = this;
        var id = this.id;
        this.Form.value.employee_id = this.employee_id;
        this.Form.value.id = this.id;
        var model = this.Form.value;
        this._appraisal_service.getObjRate(model).subscribe(function (data) {
            _this.obj_rate = Array.from(data);
            for (var x = 0; x < _this.obj_rate.length; x++) {
                _this.dues = _this.obj_rate[x].due;
            }
        }, function (err) { return console.error(err); });
    };
    ObjRateComponent = __decorate([
        Component({
            selector: 'app-obj-rate',
            templateUrl: './obj-rate.component.html',
            styleUrls: ['./obj-rate.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            QuestionService,
            AppraisalService,
            Router,
            DialogService])
    ], ObjRateComponent);
    return ObjRateComponent;
}(DialogComponent));
export { ObjRateComponent };
//# sourceMappingURL=obj-rate.component.js.map