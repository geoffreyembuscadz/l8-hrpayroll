import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response} from '@angular/http';
import { QuestionService } from '../../../services/question.service';
import { AppraisalService } from '../../../services/appraisal.service';
import { CommonService } from '../../../services/common.service';
import { Configuration } from '../../../app.config';
import { ConfirmModalComponent } from './../../confirm-modal/confirm-modal.component';


@Component({
  selector: 'app-obj-rate',
  templateUrl: './obj-rate.component.html',
  styleUrls: ['./obj-rate.component.css']
})

export class ObjRateComponent extends DialogComponent<null, boolean> {
	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public poststore: any;
	public id: any;

	Form : FormGroup;
	obj_rate: any; 
	view:any;
	create:any;
	url:any;
	employee_id: any;
	obj_id: any;
	dues: any;

	constructor(
		dialogService: DialogService, 
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute, 
		private _q_service: QuestionService,  
		private _appraisal_service: AppraisalService,  
		private _rt: Router,
		private modalService: DialogService,) {
		super(dialogService);

		this.Form = _fb.group({
			'id':				['null'],
			'employee_id':		['null'],
			'feedback':			['null'],
			'rate_value':		['null']
		}); 
	}

	ngOnInit() {
		this.getObjRate();
		
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}


	onSubmit() {
		let id = this.id;
		this.Form.value.obj_id = id;
		this.Form.value.employee_id = this.employee_id;
		let model = this.Form.value;
		console.log(model);
		if(this.view == true){
			this._appraisal_service.saveRateObj(model)
			.subscribe(
				data => {
					this.poststore = Array.from(data); 
					this.success_title = "Success!";
					this.success_message = "Successfully updated";
					setTimeout(() => {
						this.close();
					}, 1000);
				},
				err => this.catchError(err)
				);
		} 
	}

	getObjRate(){
		let id = this.id;
		this.Form.value.employee_id = this.employee_id;
		this.Form.value.id = this.id;
		let model = this.Form.value;

		this._appraisal_service.getObjRate(model).subscribe(
			data => {
				this.obj_rate = Array.from(data);
				for (let x = 0; x < this.obj_rate.length; x++) {
					this.dues = this.obj_rate[x].due;
				}

			},
			err => console.error(err)
			);
	}
    

    

}
