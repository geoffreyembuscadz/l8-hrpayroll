import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';//1
import { PositionService } from '../../services/position.service';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Configuration } from '../../app.config';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import { AppraisalService } from '../../services/appraisal.service';
import { Observable } from 'rxjs/Observable';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { ObjRateComponent } from '../obj-approval/obj-rate/obj-rate.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { AlertModalComponent } from '../alert-modal/alert-modal.component'; 
import {MomentModule} from 'angular2-moment/moment.module';
import { AuthUserService } from '../../services/auth-user.service';
import * as moment from 'moment';

@Component({
  selector: 'app-obj-approval',
  templateUrl: './obj-approval.component.html',
  styleUrls: ['./obj-approval.component.css']
})


@Injectable()
export class ObjApprovalComponent implements OnInit,  AfterViewInit {

	public Form: FormGroup;

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	public id: string;
	public error_title: string;
	public error_message: string;
	public success_title;
	public success_message;
	public poststore: any;
	positionForm: FormGroup;
	pos: any;
	public confirm_archiving: any;
	public form
	private headers: Headers;
	dtOptions: any = {};
    perm_rec: any;
    employee: any;
    selectedEmployee: any = null;
    obj : any;
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];
  	user_id: any;
  	dates: any;
  	today = moment().format("YYYY-MM-DD"); 
  	objectives: any;
  	api_position: String;
  	

  	constructor(
  				dialogService: DialogService, 
  				private _perms_service: PermissionService, 
  				private _auth_service: AuthUserService, 
  				private _appraisal_service: AppraisalService, 
  				private modalService: DialogService, 
  				private _ar: ActivatedRoute,  
  				private _rt: Router, 
  				private _fb: FormBuilder, 
  				private _conf: Configuration,

  				){
		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	      // Header Variables
		this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.api_position = this._conf.ServerWithApiUrl + 'position/';

        this.Form = 
			_fb.group({
				'supervisor_id': 	['null'],
				'employee_id':		['null']
		});
	}

	ngAfterViewInit(){
		this.dtTrigger.next();
	}

	ngOnInit() {
		this.getEmployee();
		this.get_UserId();
		
	}
	

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	getEmployee(){
		this._appraisal_service.getEmployee().
		subscribe(
			data => {
				this.employee = Array.from(data);	
			},
			err => console.error(err)
			);
	}

	setActiveEmployee(employee: any) {
		this.selectedEmployee = employee;
		let myJSON = JSON.stringify(this.selectedEmployee);
		this.getObjectives();
		this.getObjNow();
	}

	getObjectives(){
		this.Form.value.employee_id = this.selectedEmployee.id;
		let model = this.Form.value;
        this._appraisal_service.getObjectives(model).subscribe(
          data => {
            this.obj = Array.from(data);
            for (let x = 0; x < this.obj.length; ++x) {
            	this.dates = this.obj[x].due;
            }
          },
          err => console.error(err)
          );
    }

    approvedObj(id){
    	let disposable = this.modalService.addDialog(AlertModalComponent, {
            title:'Objective',
            message:'Are you sure to approve this objective',
			id: id,
			attValEmp:false,
			approved: true,
			updated_by: this.user_id, 
			status: 4
        	}).subscribe((isConfirmed)=>{
                this.getObjectives();
            });
    }

    get_UserId(){
		this._auth_service.getUser().
		subscribe(
			data => {
				let user = data;
				this.user_id = user.id;
			},
			err => console.error(err)
		);
	}

	rejectObj(id){
    	let disposable = this.modalService.addDialog(AlertModalComponent, {
            title:'Objective',
            message:'Are you sure to reject this objective',
			id: id,
			attValEmp:false,
			approved: true,
			updated_by: this.user_id,
			status: 2
        	}).subscribe((isConfirmed)=>{
        		this.getObjectives();
                
            });
    }

    getObjNow(){
		this.Form.value.employee_id = this.selectedEmployee.id;
		let model = this.Form.value;
        this._appraisal_service.getObjNow(model).subscribe(
          data => {
            this.objectives = Array.from(data);    
          },
          err => console.error(err)
          );
    }

    viewObj(id:any) {
		let disposable = this.modalService.addDialog(ObjRateComponent, {
            title:'Objective',
            button:'Done',
            view:true,
		    url:'appraisal',
		    employee_id: this.selectedEmployee.id,
		    id:id
        	}).subscribe((isConfirmed)=>{
                this.ngOnInit();
        });


	}

}



