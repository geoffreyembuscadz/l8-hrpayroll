var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Configuration } from '../../../app.config';
import { TypeModalComponent } from '../../type-modal/type-modal.component';
import { CertificateOfAttendanceTypeService } from '../../../services/certificate-of-attendance-type.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
var CertificateOfAttendanceTypeListComponent = /** @class */ (function () {
    function CertificateOfAttendanceTypeListComponent(_conf, modalService, _COAtypeservice) {
        this._conf = _conf;
        this.modalService = modalService;
        this._COAtypeservice = _COAtypeservice;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api = this._conf.ServerWithApiUrl + 'certificate_of_attendance_type/';
    }
    CertificateOfAttendanceTypeListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var authToken = localStorage.getItem('id_token');
        this.dtOptions = {
            ajax: {
                url: this.api,
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + authToken);
                },
            },
            columns: [{
                    title: 'ID',
                    data: 'id'
                }, {
                    title: 'Name',
                    data: 'name'
                }, {
                    title: 'Description',
                    data: 'description'
                }, {
                    title: 'Action',
                    defaultContent: '<button class="btn btn-primary">Edit</button>'
                }],
            rowCallback: function (nRow, data, aData, iDisplayIndex, iDisplayIndexFull) {
                var self = _this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', function () {
                    _this.id = data.id;
                    _this.editCOAtype();
                });
                return nRow;
            },
        };
    };
    CertificateOfAttendanceTypeListComponent.prototype.editCOAtype = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Edit Certificate of Attendance Type',
            button: 'Update',
            edit: true,
            url: 'certificate_of_attendance_type',
            id: this.id
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    CertificateOfAttendanceTypeListComponent.prototype.createCOAType = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Create Certificate of Attendance Type',
            button: 'Add',
            url: 'certificate_of_attendance_type',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    CertificateOfAttendanceTypeListComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    CertificateOfAttendanceTypeListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    CertificateOfAttendanceTypeListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], CertificateOfAttendanceTypeListComponent.prototype, "dtElement", void 0);
    CertificateOfAttendanceTypeListComponent = __decorate([
        Component({
            selector: 'app-certificate-of-attendance-type-list',
            templateUrl: './certificate-of-attendance-type-list.component.html',
            styleUrls: ['./certificate-of-attendance-type-list.component.css']
        }),
        __metadata("design:paramtypes", [Configuration,
            DialogService,
            CertificateOfAttendanceTypeService])
    ], CertificateOfAttendanceTypeListComponent);
    return CertificateOfAttendanceTypeListComponent;
}());
export { CertificateOfAttendanceTypeListComponent };
//# sourceMappingURL=certificate-of-attendance-type-list.component.js.map