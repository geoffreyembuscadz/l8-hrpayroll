import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from '../../../app.config';
import { TypeModalComponent } from '../../type-modal/type-modal.component';
import { CertificateOfAttendanceTypeService } from '../../../services/certificate-of-attendance-type.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-certificate-of-attendance-type-list',
  templateUrl: './certificate-of-attendance-type-list.component.html',
  styleUrls: ['./certificate-of-attendance-type-list.component.css']
})
export class CertificateOfAttendanceTypeListComponent implements OnInit, AfterViewInit  {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	dtOptions: any = {};
	api: String;
	id:any;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

  constructor(
  	private _conf: Configuration,
  	private modalService: DialogService,
  	private _COAtypeservice: CertificateOfAttendanceTypeService
  	) { 

  	this.body.classList.add("skin-blue");
	this.body.classList.add("sidebar-mini");

	this.api = this._conf.ServerWithApiUrl + 'certificate_of_attendance_type/';
  }

  ngOnInit() {

  	let authToken = localStorage.getItem('id_token');

		this.dtOptions = {
			ajax:{
				url: this.api,
				type: "GET",
				beforeSend: function(xhr){
                            xhr.setRequestHeader("Authorization",
                            "Bearer " + authToken);
            	},

	        },
			columns: [{
				title: 'ID',
				data: 'id'
			},{
				title: 'Name',
				data: 'name'
			},{
				title: 'Description',
				data: 'description'
			},{
				title: 'Action',
				defaultContent: '<button class="btn btn-primary">Edit</button>'
			}],
			rowCallback: (nRow: number, data: any, aData: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
				let self = this;
				// Unbind first in order to avoid any duplicate handler
				// (see https://github.com/l-lin/angular-datatables/issues/87)
				$('td', nRow).unbind('click');
				$('td', nRow).bind('click', () => {

					this.id = data.id;
					this.editCOAtype();
				});
				return nRow;
			},
			
		};

  }

	editCOAtype() {
		let disposable = this.modalService.addDialog(TypeModalComponent, {
		    title:'Edit Certificate of Attendance Type',
		    button:'Update',
		    edit:true,
		    url:'certificate_of_attendance_type',
		    id:this.id
			}).subscribe((isConfirmed)=>{
		        this.rerender();
		    });
	}

	createCOAType(){
		let disposable = this.modalService.addDialog(TypeModalComponent, {
		    title:'Create Certificate of Attendance Type',
		    button:'Add',
		    url:'certificate_of_attendance_type',
		    create:true
			}).subscribe((isConfirmed)=>{
		        this.rerender();
		    });
	}

	ngOnDestroy() {
	    //remove the the body classes
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {//7
	    this.dtTrigger.next();
	  }

    rerender(): void {//8
	    this.dtElement.dtInstance.then((dtInstance) => {
	      // Destroy the table first
	      dtInstance.destroy();
	      // Call the dtTrigger to rerender again
	      this.dtTrigger.next();
	    });
    }

}
