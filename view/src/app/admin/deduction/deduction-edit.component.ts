import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Deduction } from '../../model/deduction';
import { Response, Headers } from '@angular/http';
import { DeductionService } from '../../services/deduction.service';
import { PermissionService } from '../../services/permission.service';
import { Configuration } from '../../app.config';


@Component({
  selector: 'confirm',
  templateUrl: './deduction-edit.component.html'
})
export class DeductionEditComponent extends DialogComponent<null, boolean> {
  perm_rec: any;

  title: string;
  message: string;
  public id: any;
  public name: string;
  public display_name: string;
  public description: string;
  public success_title;
  public success_message;
  private headers: Headers;
  public poststore: any;
  public deduction = new Deduction();

  updateDeductionForm : FormGroup;
  public confirm_archiving: any;

  constructor(private _perms_service: PermissionService, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute, private _deduction_service: DeductionService,  private _rt: Router) {
    super(dialogService);
      // Header Variables
    this.headers = new Headers();
    let authToken = localStorage.getItem('id_token');
    this.headers.append('Authorization', `Bearer ${authToken}`);
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');

    this.id = this._deduction_service.getId();
    console.log(this.id);
    this._deduction_service.getDeduction(this.id).subscribe(
      data => {
        this.setDeduction(data);
      },
      err => console.error(err)
    );

    this.updateDeductionForm = _fb.group({
      'type': [this.deduction.type, [Validators.required]],
      'description': [this.deduction.description, [Validators.required]],
      
      
    });  
  }
 
  public validateUpdate() {
    let deduction_model = this.updateDeductionForm.value;

    this._deduction_service.updateDeduction(this.id, deduction_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The Deduction is successfully updated.";
        setTimeout(() => {
       this.close();
      }, 1000);
      },
      err => this.catchError(err)
    );
  }

  public catchError(error: any) {

  }

  public setDeduction(deduction: any){
    this.id = deduction.id;
    this.deduction = deduction;
    console.log(this.deduction);
  }
  public archiveDeductionRecord(){
		console.log(this.id);
	}
public confirmArchived(){
		this.confirm_archiving = 1;
	}

	public dismissConfirmArchive(){
		this.confirm_archiving = null;
	}

	public archiveEmployeeRecord(){
		console.log(this.id);
	}

	ngAfterViewInit(){
	}

	archiveDeduction(event: Event){
		event.preventDefault();
		let deduction_id = this.id;
		console.log('prearchive', deduction_id);
		this._deduction_service.archiveDeduction(deduction_id).subscribe(
			data => {
				this.success_title = "Success!";
        this.success_message = "The Deduction Type is successfully updated.";
        setTimeout(() => {
           this.close();
        }, 1000);
			},
			err => console.error(err)
		);
		

	}

  ngOnInit() {
    // display permission
    let companies = this._perms_service.getPermissions().
      subscribe(
        data => {
          this.perm_rec = Array.from(data); // fetched record
        },
        err => console.error(err)
      );
    }

}