var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Deduction } from '../../model/deduction';
import { DeductionService } from '../../services/deduction.service';
import { Headers } from '@angular/http';
import { PermissionService } from '../../services/permission.service';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
var DeductionAddComponent = /** @class */ (function (_super) {
    __extends(DeductionAddComponent, _super);
    function DeductionAddComponent(_perms_service, dialogService, _fb, _ar, _deduction_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._perms_service = _perms_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._deduction_service = _deduction_service;
        _this.deduction = new Deduction();
        // Header Variables
        _this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        _this.headers.append('Authorization', "Bearer " + authToken);
        _this.headers.append('Content-Type', 'application/json');
        _this.headers.append('Accept', 'application/json');
        _this.addDeductionForm = _fb.group({
            'type': [_this.deduction.type, [Validators.required]],
            'description': [_this.deduction.description, [Validators.required]]
        });
        return _this;
    }
    DeductionAddComponent.prototype.catchError = function (error) {
    };
    DeductionAddComponent.prototype.onSubmit = function () {
        var _this = this;
        var deduction_model = this.addDeductionForm.value;
        this._deduction_service.storeDeduction(deduction_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            _this.success_title = "Success!";
            _this.success_message = "New deduction is added!";
            setTimeout(function () {
                _this.close();
            }, 1000);
        }, function (err) { return _this.catchError(err); });
    };
    DeductionAddComponent = __decorate([
        Component({
            selector: 'app-deduction-add',
            templateUrl: './deduction-add.component.html',
            styleUrls: ['./deduction-add.component.css']
        }),
        __metadata("design:paramtypes", [PermissionService, DialogService, FormBuilder, ActivatedRoute, DeductionService])
    ], DeductionAddComponent);
    return DeductionAddComponent;
}(DialogComponent));
export { DeductionAddComponent };
//# sourceMappingURL=deduction-add.component.js.map