var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DeductionService } from '../../services/deduction.service';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Deduction } from '../../model/deduction';
import { DialogService } from "ng2-bootstrap-modal";
import { DeductionEditComponent } from './deduction-edit.component';
import { DeductionAddComponent } from './deduction-add.component';
import { PermissionService } from '../../services/permission.service';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables'; //2
import { Subject } from 'rxjs/Rx';
var DeductionComponent = /** @class */ (function () {
    function DeductionComponent(dialogService, _perms_service, modalService, _ar, _deduction_service, _rt, _fb, _conf) {
        this._perms_service = _perms_service;
        this.modalService = modalService;
        this._ar = _ar;
        this._deduction_service = _deduction_service;
        this._rt = _rt;
        this._fb = _fb;
        this._conf = _conf;
        this.dtTrigger = new Subject();
        this.deduction = new Deduction();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.api_deduction = this._conf.ServerWithApiUrl + 'deduction/';
    }
    DeductionComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    DeductionComponent.prototype.showMe = function (deductions) {
        this.deduction_rec = deductions;
    };
    DeductionComponent.prototype.ngOnInit = function () {
        var _this = this;
        var deductions = this._perms_service.getPermissions().
            subscribe(function (data) {
            _this.perm_rec = Array.from(data); // fetched record
        }, function (err) { return console.error(err); });
        var authToken = localStorage.getItem('id_token');
        // DataTable Configuration
        this.dtOptions = {
            ajax: {
                url: this.api_deduction,
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + authToken);
                },
            },
            columns: [{
                    title: 'Deduction',
                    data: 'type'
                }, {
                    title: 'Description',
                    data: 'description'
                }],
            rowCallback: function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
                var self = _this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', function () {
                    // this._rt.navigate(['admin/role-edit/', data.id]);
                    // console.log(oData.id);
                    _this.deduction_id = data.id;
                    // console.log(this.deduction_id);
                    _this._deduction_service.setId(_this.deduction_id);
                    _this.openModal();
                });
                return nRow;
            }
        };
    };
    DeductionComponent.prototype.openModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(DeductionEditComponent, {
            title: 'Update Deduction'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
        //We can close dialog calling disposable.unsubscribe();
        //If dialog was not closed manually close it by timeout
    };
    DeductionComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    DeductionComponent.prototype.addModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(DeductionAddComponent, {
            title: 'Add Deduction Type'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    DeductionComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], DeductionComponent.prototype, "dtElement", void 0);
    DeductionComponent = __decorate([
        Component({
            selector: 'app-deduction',
            templateUrl: './deduction.component.html',
            styleUrls: ['./deduction.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService, PermissionService, DialogService, ActivatedRoute, DeductionService, Router, FormBuilder, Configuration])
    ], DeductionComponent);
    return DeductionComponent;
}());
export { DeductionComponent };
//# sourceMappingURL=deduction.component.js.map