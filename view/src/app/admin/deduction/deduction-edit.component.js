var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Deduction } from '../../model/deduction';
import { Headers } from '@angular/http';
import { DeductionService } from '../../services/deduction.service';
import { PermissionService } from '../../services/permission.service';
var DeductionEditComponent = /** @class */ (function (_super) {
    __extends(DeductionEditComponent, _super);
    function DeductionEditComponent(_perms_service, dialogService, _fb, _ar, _deduction_service, _rt) {
        var _this = _super.call(this, dialogService) || this;
        _this._perms_service = _perms_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._deduction_service = _deduction_service;
        _this._rt = _rt;
        _this.deduction = new Deduction();
        // Header Variables
        _this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        _this.headers.append('Authorization', "Bearer " + authToken);
        _this.headers.append('Content-Type', 'application/json');
        _this.headers.append('Accept', 'application/json');
        _this.id = _this._deduction_service.getId();
        console.log(_this.id);
        _this._deduction_service.getDeduction(_this.id).subscribe(function (data) {
            _this.setDeduction(data);
        }, function (err) { return console.error(err); });
        _this.updateDeductionForm = _fb.group({
            'type': [_this.deduction.type, [Validators.required]],
            'description': [_this.deduction.description, [Validators.required]],
        });
        return _this;
    }
    DeductionEditComponent.prototype.validateUpdate = function () {
        var _this = this;
        var deduction_model = this.updateDeductionForm.value;
        this._deduction_service.updateDeduction(this.id, deduction_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The Deduction is successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 1000);
        }, function (err) { return _this.catchError(err); });
    };
    DeductionEditComponent.prototype.catchError = function (error) {
    };
    DeductionEditComponent.prototype.setDeduction = function (deduction) {
        this.id = deduction.id;
        this.deduction = deduction;
        console.log(this.deduction);
    };
    DeductionEditComponent.prototype.archiveDeductionRecord = function () {
        console.log(this.id);
    };
    DeductionEditComponent.prototype.confirmArchived = function () {
        this.confirm_archiving = 1;
    };
    DeductionEditComponent.prototype.dismissConfirmArchive = function () {
        this.confirm_archiving = null;
    };
    DeductionEditComponent.prototype.archiveEmployeeRecord = function () {
        console.log(this.id);
    };
    DeductionEditComponent.prototype.ngAfterViewInit = function () {
    };
    DeductionEditComponent.prototype.archiveDeduction = function (event) {
        var _this = this;
        event.preventDefault();
        var deduction_id = this.id;
        console.log('prearchive', deduction_id);
        this._deduction_service.archiveDeduction(deduction_id).subscribe(function (data) {
            _this.success_title = "Success!";
            _this.success_message = "The Deduction Type is successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 1000);
        }, function (err) { return console.error(err); });
    };
    DeductionEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        // display permission
        var companies = this._perms_service.getPermissions().
            subscribe(function (data) {
            _this.perm_rec = Array.from(data); // fetched record
        }, function (err) { return console.error(err); });
    };
    DeductionEditComponent = __decorate([
        Component({
            selector: 'confirm',
            templateUrl: './deduction-edit.component.html'
        }),
        __metadata("design:paramtypes", [PermissionService, DialogService, FormBuilder, ActivatedRoute, DeductionService, Router])
    ], DeductionEditComponent);
    return DeductionEditComponent;
}(DialogComponent));
export { DeductionEditComponent };
//# sourceMappingURL=deduction-edit.component.js.map