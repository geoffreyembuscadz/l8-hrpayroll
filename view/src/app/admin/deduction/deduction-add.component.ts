import 'rxjs/add/operator/catch';
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Deduction } from '../../model/deduction';
import { DeductionService } from '../../services/deduction.service';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { PermissionService } from '../../services/permission.service';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";

@Component({
  selector: 'app-deduction-add',
  templateUrl: './deduction-add.component.html',
  styleUrls: ['./deduction-add.component.css']
})
export class DeductionAddComponent extends DialogComponent<null, boolean> {
  

  private poststore: any;
  private post_data: any;
  private error_title: string;
  private error_message: string;
  private success_title: string;
  private success_message: string;
  private headers: Headers;
  public deduction = new Deduction();

  addDeductionForm : FormGroup;
  
  constructor(private _perms_service: PermissionService, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute, private _deduction_service: DeductionService) {
    super(dialogService);
      // Header Variables
    this.headers = new Headers();
    let authToken = localStorage.getItem('id_token');
    this.headers.append('Authorization', `Bearer ${authToken}`);
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');

    

    this.addDeductionForm = _fb.group({
      'type': [this.deduction.type, [Validators.required]],
      'description': [this.deduction.description, [Validators.required]]
      
    });  
  }

  public catchError(error: any) {

  }
 public onSubmit() {
    let deduction_model = this.addDeductionForm.value;

    this._deduction_service.storeDeduction(deduction_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data);
        this.success_title = "Success!";
        this.success_message = "New deduction is added!";
        setTimeout(() => {
         this.close();
        }, 1000);
      },
      err => this.catchError(err)
    );

    
  }

  
}