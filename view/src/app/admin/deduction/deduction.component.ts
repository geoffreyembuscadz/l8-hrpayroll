import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DeductionService } from '../../services/deduction.service';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Deduction } from '../../model/deduction';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { DeductionEditComponent } from './deduction-edit.component';
import { DeductionAddComponent } from './deduction-add.component';
import { PermissionService } from '../../services/permission.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';//2
import { Subject } from 'rxjs/Rx';


@Component({
  selector: 'app-deduction',
  templateUrl: './deduction.component.html',
  styleUrls: ['./deduction.component.css']
})

@Injectable()
export class DeductionComponent implements OnInit, AfterViewInit {
	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	public id: string;
	public deduction_id: any;
	public error_title: string;
	public error_message: string;
	public success_title;
	public success_message;
	public poststore: any;
	public deduction = new Deduction();
	deductionForm: FormGroup;

	// TEMP variables
	public confirm_archiving: any;
	public form


	private headers: Headers;
	dtOptions: any = {};
	deduction_rec: any;
	// get permissions list
    perm_rec: any;
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	api_deduction: String;

  	constructor(dialogService: DialogService, private _perms_service: PermissionService, private modalService: DialogService, private _ar: ActivatedRoute, private _deduction_service: DeductionService, private _rt: Router, private _fb: FormBuilder, private _conf: Configuration){
		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	      // Header Variables
		this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');

        this.api_deduction = this._conf.ServerWithApiUrl + 'deduction/';



	}

	ngAfterViewInit(){
		this.dtTrigger.next();
	}

	showMe(deductions: any){
		this.deduction_rec = deductions;
	}

	ngOnInit() {


		let deductions = this._perms_service.getPermissions().
		subscribe(
			data => {
				this.perm_rec = Array.from(data); // fetched record
			},
			err => console.error(err)
		);

		let authToken = localStorage.getItem('id_token');


		// DataTable Configuration
		this.dtOptions = {
			ajax:
			{
				url: this.api_deduction,
				type: "GET",
				beforeSend: function(xhr){
                            xhr.setRequestHeader("Authorization",
                            "Bearer " + authToken);
                },
			},
			columns: [{
				
				title: 'Deduction',
				data: 'type'
				  },{
				title: 'Description',
				data: 'description'
				
				  }],
			rowCallback: (nRow: number, data: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
				let self = this;
				// Unbind first in order to avoid any duplicate handler
				// (see https://github.com/l-lin/angular-datatables/issues/87)
				$('td', nRow).unbind('click');
				$('td', nRow).bind('click', () => {
					// this._rt.navigate(['admin/role-edit/', data.id]);
				    // console.log(oData.id);
				    this.deduction_id = data.id;
				    // console.log(this.deduction_id);
				    this._deduction_service.setId(this.deduction_id);
					this.openModal();

				});
				return nRow;
			}
		};
		
	}
	openModal() {
		let disposable = this.modalService.addDialog(DeductionEditComponent, {
            title:'Update Deduction'
        	}).subscribe((isConfirmed)=>{
                this.rerender();
            });
        //We can close dialog calling disposable.unsubscribe();
        //If dialog was not closed manually close it by timeout
	}


	ngOnDestroy() {
	    //remove the the body classes
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	 }
	addModal() {
		let disposable = this.modalService.addDialog(DeductionAddComponent, {
            title:'Add Deduction Type'
        	}).subscribe((isConfirmed)=>{
                this.rerender();
            });
		}
	rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      // Destroy the table first
	      dtInstance.destroy();
	      // Call the dtTrigger to rerender again
	      this.dtTrigger.next();
	    });
    }


	
}
