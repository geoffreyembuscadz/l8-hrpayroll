import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommonService } from '../../services/common.service';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';


@Component({
  selector: 'app-type-modal',
  templateUrl: './type-modal.component.html',
  styleUrls: ['./type-modal.component.css']
})
export class TypeModalComponent extends DialogComponent<null, boolean> {

    public success_title;
    public success_message;
    public error_title: string
    public error_message: string;
    public poststore: any;
    public id: any;
    public type: string;
    public name: any;
    public description:any;
    edit:any;
    create:any;
    url:any;

    Form : FormGroup;

    constructor(
      dialogService: DialogService, 
      private _fb: FormBuilder, 
      private _ar: ActivatedRoute, 
      private _common_service: CommonService,  
      private _rt: Router,
      private modalService: DialogService
    ) {

      super(dialogService);

      this.Form = _fb.group({
         'name':          [null],
         'description':   [null]
      });  

    }

    ngOnInit() {
      if(this.edit == true){
        this.get_data();
      }
    }

    get_data(){
      let id = this.id;
      let url = this.url;

      this._common_service.getTypeData(id,url).subscribe(
        data => {
          this.setType(data);
          },
          err => console.error(err)
      );
    }

    public setType(type: any){
      this.id = type.id;
      this.name = type.name;
      this.description = type.description;

      this.Form.value.name = type.name;
      this.Form.value.description = type.description;
    }

    onSubmit() {

      let model = this.Form.value;
      let url = this.url;

      if(this.edit == true){

        let id = this.id;

        this._common_service.updateTypeData(id, model,url)
        .subscribe(
          data => {
            this.poststore = Array.from(data);
            this.success_title = "Success!";
            this.success_message = "Successfully updated";
            setTimeout(() => {
            this.close();
            }, 1000);
          },
          err => this.catchError(err)
        );
      }

      if(this.create == true){

        this._common_service.createTypeData(model,url)
        .subscribe(
          data => {
            this.poststore = Array.from(data);
            this.success_title = "Success!";
            this.success_message = "Successfully created";
            setTimeout(() => {
            this.close();
            }, 1000);
          },
          err => this.catchError(err)
        );
      }
    }

    private catchError(error: any){
      let response_body = error._body;
      let response_status = error.status;

      if( response_status == 500 ){
        this.error_title = 'Error 500';
        this.error_message = 'The given data failed to pass validation.';
      } else if( response_status == 200 ) {
        this.error_title = '';
        this.error_message = '';
      }
    }

    archive(){
 
      let disposable = this.modalService.addDialog(ConfirmModalComponent, {
        title:'Archive Data',
        message:'Are you sure you want to archive this data?',
        action:'Delete',
        id:this.id,
        url:this.url
        }).subscribe((message)=>{
          if (message == true) {
            setTimeout(() => {
              this.close();
            }, 1000);
          }
         
      });
    }
}