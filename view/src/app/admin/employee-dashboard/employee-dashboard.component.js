var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable } from '@angular/core';
import * as moment from 'moment';
import * as io from "socket.io-client";
import { Configuration } from '../../app.config';
import { DialogService } from "ng2-bootstrap-modal";
import { AuthUserService } from '../../services/auth-user.service';
import { AttendanceService } from '../../services/attendance.service';
import { FormBuilder } from '@angular/forms';
import { PayrollService } from '../../services/payroll.service';
import { RemarksModalComponent } from '../remarks-modal/remarks-modal.component';
import { ActionCenterModalComponent } from '../action-center/action-center-modal/action-center-modal.component';
import { TaskService } from '../../services/task.service';
import { OnboardingService } from '../../services/onboarding.service';
import { ClearanceService } from '../../services/clearance.service';
var EmployeeDashboardComponent = /** @class */ (function () {
    function EmployeeDashboardComponent(_auth_service, _attendservice, _fb, _payroll_serv, modalService, _task_service, _onboarding_service, _clearance_service, _conf) {
        this._auth_service = _auth_service;
        this._attendservice = _attendservice;
        this._fb = _fb;
        this._payroll_serv = _payroll_serv;
        this.modalService = modalService;
        this._task_service = _task_service;
        this._onboarding_service = _onboarding_service;
        this._clearance_service = _clearance_service;
        this._conf = _conf;
        this.logs = [];
        this.tableVal = true;
        this.today = moment().format("YYYY-MM-DD");
        this.datetime = moment().format("YYYY-MM-DD HH:mm");
        this.loaderForAttend = true;
        this.loaderForTask = true;
        this.loaderForOnboarding = true;
        this.loaderForClearance = true;
        this.btnLogIn = false;
        this.btnLogOut = false;
        this.btnLunchIn = false;
        this.btnLunchOut = false;
        this.btnMeetingIn = false;
        this.btnMeetingOut = false;
        this.btnOutOfOfficeIn = false;
        this.btnOutOfOfficeOut = false;
        this.btnSeminarIn = false;
        this.btnSeminarOut = false;
        this.button = false;
        this.seminar = 0;
        this.meeting = 0;
        this.office = 0;
        this.lunch = 0;
        this.timeIn = 0;
        this.timeOut = 0;
        this.task = [];
        this.onboarding = [];
        this.btnOnboarding = false;
        this.checkAllValOnboarding = false;
        this.checkAllOnboard = false;
        this.clearance = [];
        this.btnClearance = false;
        this.checkAllValClearance = false;
        this.checkAllClear = false;
        this.btnPrint = false;
        this.grey = 0;
        this.log = 0;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.host = this._conf.socketUrl;
        this.AttendanceForm = _fb.group({
            'type': [null],
            'date': [this.today],
            'datetime': [this.datetime],
            'branch_id': [1],
            'start_date': [null],
            'end_date': [null],
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    EmployeeDashboardComponent.prototype.ngOnInit = function () {
        this.getTime();
        this.logsValidation();
        this.getTask();
        this.getOnboarding();
        this.getClearance();
    };
    EmployeeDashboardComponent.prototype.getTime = function () {
        var _this = this;
        setInterval(function () {
            _this.day = moment().format('dddd');
            _this.date = moment().format('MMMM D, YYYY');
            _this.time = moment().format('h:mm:ss a');
        }, 1000);
    };
    EmployeeDashboardComponent.prototype.btn = function () {
        if (!this.btnLogIn && this.btnLogOut) {
            var a = moment("00:00:00", "h:mm:ss a");
            var b = moment();
            var temp = Math.abs(a.diff(b, 'hours') * 4.16);
            if (this.btnMeetingOut) {
                this.meeting = temp;
            }
            else if (this.btnSeminarOut) {
            }
            else if (this.btnOutOfOfficeOut) {
            }
            else {
                this.grey = temp;
            }
        }
    };
    EmployeeDashboardComponent.prototype.getLogs = function () {
        var _this = this;
        this._attendservice.getMyLogs().
            subscribe(function (data) {
            _this.arrangingDataInArray(data);
        }, function (err) { return console.error(err); });
    };
    EmployeeDashboardComponent.prototype.arrangingDataInArray = function (dat) {
        var result = dat.reduce(function (acc, elt) {
            var id = elt.id, time_in = elt.time_in, time_out = elt.time_out, employee_id = elt.employee_id, location = elt.location, lunch_in = elt.lunch_in, lunch_out = elt.lunch_out, types = elt.types, typeValue = elt.type, log_in = elt.log_in, log_out = elt.log_out, remarks = elt.remarks;
            var existingEltIndex = acc.findIndex(function (e) {
                return e.id === elt.id && e.time_in === elt.time_in;
            });
            var newEvent = { type: typeValue, log_in: log_in, log_out: log_out, remarks: remarks };
            if (existingEltIndex < 0) {
                var newElement = {
                    id: id, time_in: time_in, time_out: time_out, employee_id: employee_id, location: location, lunch_in: lunch_in, lunch_out: lunch_out, types: types, business_logs: [newEvent]
                };
                return acc.concat([newElement]);
            }
            else {
                acc[existingEltIndex].business_logs.push(newEvent);
                return acc;
            }
        }, []);
        this.logs = result;
        if (this.logs.length != 0) {
            this.tableVal = true;
        }
        this.loaderForAttend = false;
        this.btnLogIn = false;
        this.btnLogOut = false;
        this.btnLunchIn = false;
        this.btnLunchOut = false;
        this.btnMeetingIn = false;
        this.btnMeetingOut = false;
        this.btnOutOfOfficeIn = false;
        this.btnOutOfOfficeOut = false;
        this.btnSeminarIn = false;
        this.btnSeminarOut = false;
        if (this.logs.length != 0 && this.button == true) {
            this.validationForButton();
        }
        else {
            this.btnLogIn = true;
        }
    };
    EmployeeDashboardComponent.prototype.validationForButton = function () {
        this.id = this.logs[0].id;
        if (this.logs[0].time_in != null && this.logs[0].time_out != null) {
            var start = moment(this.logs[0].time_in).format("YYYY-MM-DD");
            if (start != this.today) {
                this.btnLogIn = true;
            }
            else {
                this.log = 100;
            }
        }
        else if (this.logs[0].time_in != null) {
            this.validationForTimeInNotNull();
        }
        else {
            this.btnLogIn = true;
        }
    };
    EmployeeDashboardComponent.prototype.validationForTimeInNotNull = function () {
        // this.log = 10;
        if (this.logs[0].lunch_out != null && this.logs[0].lunch_in != null) {
            this.btnLunchIn = false;
            this.btnLunchOut = false;
            // this.lunch = 20;
        }
        else if (this.logs[0].lunch_in != null) {
            this.btnLunchOut = true;
            // this.lunch = 10;
        }
        else {
            this.btnLunchIn = true;
        }
        this.validationForLunchOutNotNull();
    };
    EmployeeDashboardComponent.prototype.validationForLunchOutNotNull = function () {
        if (this.btnLunchOut != true) {
            this.btnMeetingIn = true;
            this.btnSeminarIn = true;
            this.btnOutOfOfficeIn = true;
            this.btnLogOut = true;
            var len = this.logs[0].business_logs.length;
            this.validationForBusinessLogsLogInNotNullLogOutNull(len);
            this.validationForBusinessLogsLogInLogOutNotNull(len);
        }
    };
    EmployeeDashboardComponent.prototype.validationForBusinessLogsLogInNotNullLogOutNull = function (len) {
        for (var i = len - 1; i < len; ++i) {
            if (this.logs[0].business_logs[i].log_in != null && this.logs[0].business_logs[i].log_out == null) {
                this.btnMeetingIn = false;
                this.btnSeminarIn = false;
                this.btnOutOfOfficeIn = false;
                this.btnLunchIn = false;
                this.btnLogOut = false;
                if (this.logs[0].business_logs[i].type == 'Meeting') {
                    this.btnMeetingIn = false;
                    this.btnMeetingOut = true;
                }
                else if (this.logs[0].business_logs[i].type == 'Seminar') {
                    this.btnSeminarIn = false;
                    this.btnSeminarOut = true;
                }
                else if (this.logs[0].business_logs[i].type == 'Out of the Office') {
                    this.btnOutOfOfficeIn = false;
                    this.btnOutOfOfficeOut = true;
                }
            }
        }
    };
    EmployeeDashboardComponent.prototype.validationForBusinessLogsLogInLogOutNotNull = function (len) {
        var temp = 0;
        for (var i = 0; i < len; ++i) {
            var a = moment(this.logs[0].business_logs[i].log_in);
            var b = moment(this.logs[0].business_logs[i].log_out);
            temp = Math.abs(a.diff(b, 'hours') * 4.16);
            if (this.logs[0].business_logs[i].log_in != null && this.logs[0].business_logs[i].log_out != null) {
                if (this.logs[0].business_logs[i].type == 'Meeting') {
                    this.btnMeetingIn = false;
                    this.btnMeetingOut = false;
                    this.meeting = temp;
                }
                else if (this.logs[0].business_logs[i].type == 'Seminar') {
                    this.btnSeminarIn = false;
                    this.btnSeminarOut = false;
                    this.seminar = temp;
                }
                else if (this.logs[0].business_logs[i].type == 'Out of the Office') {
                    this.btnOutOfOfficeIn = false;
                    this.btnOutOfOfficeOut = false;
                    this.office = temp;
                }
            }
        }
    };
    EmployeeDashboardComponent.prototype.submit = function (type) {
        var _this = this;
        var today = moment().format("YYYY-MM-DD");
        var datetime = moment().format("YYYY-MM-DD HH:mm");
        this.AttendanceForm.value.type = type;
        this.AttendanceForm.value.datetime = datetime;
        this.AttendanceForm.value.date = today;
        this.AttendanceForm.value.id = this.id;
        var model = this.AttendanceForm.value;
        this._attendservice.createAttendanceFromWeb(model).
            subscribe(function (data) {
            var dat = data;
            _this.loaderForAttend = true;
            _this.getLogs();
        }, function (err) { return console.error(err); });
    };
    EmployeeDashboardComponent.prototype.exchange = function () {
        if (this.btnLogIn == true) {
            this.btnLogOut = true;
            this.btnLogIn = false;
            this.btnLunchIn = false;
            this.btnLunchOut = false;
            this.btnMeetingIn = false;
            this.btnMeetingOut = false;
            this.btnOutOfOfficeIn = false;
            this.btnOutOfOfficeOut = false;
            this.btnSeminarIn = false;
            this.btnSeminarOut = false;
        }
        else if (this.btnLogOut == true) {
            this.btnLogOut = false;
            this.btnLogIn = true;
            this.btnLunchIn = false;
            this.btnLunchOut = false;
            this.btnMeetingIn = false;
            this.btnMeetingOut = false;
            this.btnOutOfOfficeIn = false;
            this.btnOutOfOfficeOut = false;
            this.btnSeminarIn = false;
            this.btnSeminarOut = false;
            // this.log = 0;
            this.lunch = 0;
            this.meeting = 0;
            this.seminar = 0;
            this.office = 0;
        }
    };
    EmployeeDashboardComponent.prototype.generatePayroll = function () {
        this._payroll_serv.generatePayrollslip(this.employee_id).
            subscribe(function (data) {
            window.open(data.url, "_blank");
        });
    };
    EmployeeDashboardComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    EmployeeDashboardComponent.prototype.addBusinessLogs = function (type) {
        var _this = this;
        var today = moment().format("YYYY-MM-DD");
        var datetime = moment().format("YYYY-MM-DD HH:mm");
        this.AttendanceForm.value.type = type;
        this.AttendanceForm.value.datetime = datetime;
        this.AttendanceForm.value.date = today;
        this.AttendanceForm.value.id = this.id;
        var model = this.AttendanceForm.value;
        var message = '';
        if (type == 'meetingIn') {
            message = 'Agenda of the meeting:';
        }
        else if (type == 'seminarIn') {
            message = 'Purpose of Seminar:';
        }
        else if (type == 'outOffice') {
            message = 'Where are you going and what things you will do there:';
        }
        var disposable = this.modalService.addDialog(RemarksModalComponent, {
            title: 'Business Logs',
            logs: true,
            data: model,
            message: message
        }).subscribe(function (isConfirmed) {
            _this.loaderForAttend = true;
            _this.getLogs();
        });
    };
    EmployeeDashboardComponent.prototype.viewBusinessLogs = function (x) {
        var _this = this;
        var disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title: 'Attendance',
            businesslogs: true,
            dataAtt: x.business_logs
        }).subscribe(function (isConfirmed) {
            _this.getLogs();
        });
    };
    EmployeeDashboardComponent.prototype.logsValidation = function () {
        var _this = this;
        this._attendservice.logsValidation().
            subscribe(function (data) {
            var user = data;
            if (user.length != 0) {
                _this.button = true;
            }
            _this.getLogs();
        }, function (err) { return console.error(err); });
    };
    EmployeeDashboardComponent.prototype.getTask = function () {
        var _this = this;
        this._task_service.getMyTaskForDashboard().
            subscribe(function (data) {
            _this.task = data;
            _this.loaderForTask = false;
        }, function (err) { return console.error(err); });
    };
    EmployeeDashboardComponent.prototype.getOnboarding = function () {
        var _this = this;
        this.AttendanceForm.value.employee_id = this.employee_id;
        var model = this.AttendanceForm.value;
        var temp = [];
        this._onboarding_service.getMyOnBoardingList(model).
            subscribe(function (data) {
            temp = data;
            var x = 0;
            if (temp != []) {
                var len = temp.length;
                for (var i = 0; i < len; ++i) {
                    if (temp[i].status == 'PENDING') {
                        x++;
                    }
                }
            }
            if (x != 0) {
                _this.checkAllOnboard = true;
                _this.btnOnboarding = true;
            }
            _this.onboarding = temp;
            _this.loaderForOnboarding = false;
        }, function (err) { return console.error(err); });
    };
    EmployeeDashboardComponent.prototype.checkOnboarding = function (index) {
        if (this.onboarding[index].val == false) {
            this.onboarding[index].val = true;
        }
        else {
            this.onboarding[index].val = false;
            this.checkAllValOnboarding = false;
        }
        var len = this.onboarding.length;
        var i = 0;
        for (var x = 0; x < len; ++x) {
            if (this.onboarding[x].val == true) {
                i++;
            }
        }
        if (i > 0) {
            this.btnOnboarding = true;
        }
        else {
            this.btnOnboarding = false;
        }
    };
    EmployeeDashboardComponent.prototype.checkAllOnboarding = function () {
        if (this.checkAllValOnboarding == false) {
            var leng = this.onboarding.length;
            for (var i = 0; i < leng; i++) {
                if (this.onboarding[i].status == 'PENDING') {
                    this.onboarding[i].val = true;
                }
            }
            this.checkAllValOnboarding = true;
            this.btnOnboarding = true;
        }
        else {
            var len = this.onboarding.length;
            for (var i = 0; i < len; i++) {
                if (this.onboarding[i].status == 'PENDING') {
                    this.onboarding[i].val = false;
                }
            }
            this.checkAllValOnboarding = false;
            this.btnOnboarding = false;
        }
    };
    EmployeeDashboardComponent.prototype.submitOnboarding = function () {
        var _this = this;
        this.connectToServer();
        var len = this.onboarding.length;
        var temp = [];
        for (var i = 0; i < len; ++i) {
            if (this.onboarding[i].val == true) {
                temp.push(this.onboarding[i].id);
            }
        }
        var model = {
            data: temp,
            table_id: this.onboarding[0].employee_onboarding_id,
        };
        this._onboarding_service.updateMyStatus(model).
            subscribe(function (data) {
            _this.postore = data;
            _this.socket.emit('notify', _this.postore);
            _this.loaderForOnboarding = true;
            _this.checkAllOnboard = false;
            _this.checkAllValOnboarding = false;
            _this.getOnboarding();
        }, function (err) { return console.error(err); });
    };
    EmployeeDashboardComponent.prototype.getClearance = function () {
        var _this = this;
        var temp = [];
        this._clearance_service.getMyClearanceList().
            subscribe(function (data) {
            temp = data;
            var x = 0;
            var y = 0;
            if (temp != []) {
                var len = temp.length;
                for (var i = 0; i < len; ++i) {
                    if (temp[i].status == 'PENDING') {
                        x++;
                    }
                    if (temp[i].status == 'PENDING' || temp[i].status == 'CHECKING') {
                        y++;
                    }
                }
            }
            if (x != 0) {
                _this.checkAllClear = true;
                _this.btnClearance = true;
            }
            if (y != 0) {
                _this.btnPrint = false;
            }
            else {
                _this.btnPrint = true;
            }
            _this.clearance = temp;
            _this.loaderForClearance = false;
        }, function (err) { return console.error(err); });
    };
    EmployeeDashboardComponent.prototype.checkClearance = function (index) {
        if (this.clearance[index].val == false) {
            this.clearance[index].val = true;
        }
        else {
            this.clearance[index].val = false;
            this.checkAllValClearance = false;
        }
        var len = this.clearance.length;
        var i = 0;
        for (var x = 0; x < len; ++x) {
            if (this.clearance[x].val == true) {
                i++;
            }
        }
        if (i > 0) {
            this.btnClearance = true;
        }
        else {
            this.btnClearance = false;
        }
    };
    EmployeeDashboardComponent.prototype.checkAllClearance = function () {
        if (this.checkAllValClearance == false) {
            var leng = this.clearance.length;
            for (var i = 0; i < leng; i++) {
                if (this.clearance[i].status == 'PENDING') {
                    this.clearance[i].val = true;
                }
            }
            this.checkAllValClearance = true;
            this.btnClearance = true;
        }
        else {
            var len = this.clearance.length;
            for (var i = 0; i < len; i++) {
                if (this.clearance[i].status == 'PENDING') {
                    this.clearance[i].val = false;
                }
            }
            this.checkAllValClearance = false;
            this.btnClearance = false;
        }
    };
    EmployeeDashboardComponent.prototype.submitClearance = function () {
        var _this = this;
        this.connectToServer();
        var len = this.clearance.length;
        var temp = [];
        for (var i = 0; i < len; ++i) {
            if (this.clearance[i].val == true) {
                temp.push(this.clearance[i].id);
            }
        }
        var model = {
            data: temp,
            created_by: this.user_id,
            receiver_id: this.supervisor_id,
            messenger_id: this.employee_id,
            table_id: this.clearance[0].employee_clearance_id,
        };
        this._clearance_service.updateMyStatus(model).
            subscribe(function (data) {
            _this.postore = data;
            _this.socket.emit('notify', _this.postore);
            _this.loaderForClearance = true;
            _this.checkAllClear = false;
            _this.checkAllValClearance = false;
            _this.getClearance();
        }, function (err) { return console.error(err); });
    };
    EmployeeDashboardComponent.prototype.connectToServer = function () {
        var _this = this;
        var socketUrl = this.host;
        this.socket = io.connect(socketUrl);
        this.socket.on("connect", function () { return _this.connect(); });
        this.socket.on("disconnect", function () { return _this.disconnect(); });
        this.socket.on("error", function (error) {
            console.log("ERROR: \"" + error + "\" (" + socketUrl + ")");
        });
    };
    // Handle connection opening
    EmployeeDashboardComponent.prototype.connect = function () {
        // Request initial list when connected
        this.socket.emit("join", 'Client User -> ');
    };
    // Handle connection closing
    EmployeeDashboardComponent.prototype.disconnect = function () {
        console.log("Disconnected from \"" + this.host + "\"");
    };
    EmployeeDashboardComponent.prototype.printClearance = function () {
        var id = this.clearance[0].employee_id;
        this._clearance_service.printClearance(id).subscribe(function (data) {
            window.open(data.url, "_blank");
        });
    };
    EmployeeDashboardComponent = __decorate([
        Component({
            selector: 'app-employee-dashboard',
            templateUrl: './employee-dashboard.component.html',
            styleUrls: ['./employee-dashboard.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [AuthUserService,
            AttendanceService,
            FormBuilder,
            PayrollService,
            DialogService,
            TaskService,
            OnboardingService,
            ClearanceService,
            Configuration])
    ], EmployeeDashboardComponent);
    return EmployeeDashboardComponent;
}());
export { EmployeeDashboardComponent };
//# sourceMappingURL=employee-dashboard.component.js.map