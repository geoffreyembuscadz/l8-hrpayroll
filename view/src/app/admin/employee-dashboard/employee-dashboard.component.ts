import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import * as io from "socket.io-client";
import { Configuration } from '../../app.config';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { AuthUserService } from '../../services/auth-user.service';
import { AttendanceService } from '../../services/attendance.service';
import {MomentModule} from 'angular2-moment/moment.module';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { PayrollService } from '../../services/payroll.service';
import { RemarksModalComponent } from '../remarks-modal/remarks-modal.component';
import { ActionCenterModalComponent } from '../action-center/action-center-modal/action-center-modal.component';
import { TaskService } from '../../services/task.service';
import { OnboardingService } from '../../services/onboarding.service';
import { ClearanceService } from '../../services/clearance.service';

@Component({
  selector: 'app-employee-dashboard',
  templateUrl: './employee-dashboard.component.html',
  styleUrls: ['./employee-dashboard.component.css']
})

@Injectable()
export class EmployeeDashboardComponent implements OnInit {

	user_id:any;
	employee_id:any;
	logs=[];
	tableVal=true;
	today = moment().format("YYYY-MM-DD");
	datetime = moment().format("YYYY-MM-DD HH:mm");
	AttendanceForm:any;
	id:any;

	loaderForAttend = true;
	loaderForTask = true;
	loaderForOnboarding = true;
	loaderForClearance = true;


	day:any;
	time:any;
	date:any;

	btnLogIn = false;
	btnLogOut = false;
	btnLunchIn = false;
	btnLunchOut = false;
	btnMeetingIn = false;
	btnMeetingOut = false;
	btnOutOfOfficeIn = false;
	btnOutOfOfficeOut = false;
	btnSeminarIn = false;
	btnSeminarOut = false;
	button = false;

	seminar = 0;
	meeting = 0;
	office = 0;
	lunch = 0;
	timeIn = 0;
	timeOut = 0;
	task = [];
	onboarding = [];
	supervisor_id:any;

	btnOnboarding = false;
	checkAllValOnboarding = false;
	checkAllOnboard = false;

	clearance = [];

	btnClearance = false;
	checkAllValClearance = false;
	checkAllClear = false;
	btnPrint = false;
	postore:any;
	grey = 0;
	log = 0;

	host:any;
	socket: SocketIOClient.Socket;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

  	constructor(
   		private _auth_service: AuthUserService,
		private _attendservice: AttendanceService,
		private _fb: FormBuilder,
		private _payroll_serv: PayrollService,
		private modalService: DialogService,
		private _task_service: TaskService,
   		private _onboarding_service: OnboardingService,
   		private _clearance_service: ClearanceService,
   		private _conf: Configuration,
   	) { 
   		this.host = this._conf.socketUrl;

   		this.AttendanceForm = _fb.group({
			'type': 		[null],
			'date': 		[this.today],
			'datetime': 	[this.datetime],
			'branch_id': 	[1],
			'start_date': 	[null],
			'end_date': 	[null],
    	}); 

	  	this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");
  	}

 	ngOnInit() {
		this.getTime();
		this.logsValidation();
		this.getTask();
		this.getOnboarding();
		this.getClearance();
	}

	getTime(){
		setInterval(() => {
		  this.day = moment().format('dddd');
		  this.date = moment().format('MMMM D, YYYY');
		  this.time = moment().format('h:mm:ss a');

		
		}, 1000);
 

	}

	btn(){
		if (!this.btnLogIn && this.btnLogOut) {
			let a = moment("00:00:00", "h:mm:ss a");
			let b = moment();
			let temp = Math.abs(a.diff(b, 'hours') * 4.16);

			if (this.btnMeetingOut) {
				this.meeting = temp;
			}
			else if(this.btnSeminarOut){

			}
			else if(this.btnOutOfOfficeOut){

			}else{
				this.grey = temp;
			}
		}
	}

	getLogs(){

		this._attendservice.getMyLogs().
	      	subscribe(
	        data => {
	          this.arrangingDataInArray(data);
	        },
	        err => console.error(err)
	    );
	}

	arrangingDataInArray(dat){
		const result = dat.reduce((acc, elt) => {
      	const {id,time_in,time_out,employee_id,location,lunch_in,lunch_out,types,type:typeValue,log_in,log_out,remarks} = elt;
      	const existingEltIndex = acc.findIndex(e =>
       		 e.id === elt.id && e.time_in === elt.time_in);
      	const newEvent = {type: typeValue, log_in, log_out, remarks};
      
		if (existingEltIndex < 0) { 
			const newElement = {
		  	id,time_in,time_out,employee_id,location,lunch_in,lunch_out,types,business_logs: [newEvent]
		};
		return [...acc, newElement];
		} else { 
			acc[existingEltIndex].business_logs.push(newEvent);
		return acc;
		}
    	}, []);

    	this.logs = result;

      	if (this.logs.length != 0) {
	        this.tableVal=true;
      	}

	    this.loaderForAttend = false;
      	this.btnLogIn = false;
		this.btnLogOut = false;
		this.btnLunchIn = false;
		this.btnLunchOut = false;
		this.btnMeetingIn = false;
		this.btnMeetingOut = false;
		this.btnOutOfOfficeIn = false;
		this.btnOutOfOfficeOut = false;
		this.btnSeminarIn = false;
		this.btnSeminarOut = false;


		if (this.logs.length != 0 && this.button == true) {
        	this.validationForButton();
    	}else{
			this.btnLogIn = true;
		}
	}

	validationForButton(){

  		this.id = this.logs[0].id;
  		if(this.logs[0].time_in != null && this.logs[0].time_out != null){
          	let start =  moment(this.logs[0].time_in).format("YYYY-MM-DD");
          	if(start != this.today){
          		this.btnLogIn = true;
          	}else{
          		this.log = 100;
          	}
      	}
		else if(this.logs[0].time_in != null){

			this.validationForTimeInNotNull();
      	}
		else{
			this.btnLogIn = true;
		}
	}

	validationForTimeInNotNull(){
		// this.log = 10;

		if(this.logs[0].lunch_out != null && this.logs[0].lunch_in != null){
			this.btnLunchIn = false;
			this.btnLunchOut = false;
			// this.lunch = 20;
      	}
		else if (this.logs[0].lunch_in != null) {
			this.btnLunchOut = true;
			// this.lunch = 10;
		}else{
			this.btnLunchIn = true;
		}

		this.validationForLunchOutNotNull();
	}

	validationForLunchOutNotNull(){
		if (this.btnLunchOut != true) {
			this.btnMeetingIn = true;
			this.btnSeminarIn = true;
			this.btnOutOfOfficeIn = true;
			this.btnLogOut = true;

			let len = this.logs[0].business_logs.length;

			this.validationForBusinessLogsLogInNotNullLogOutNull(len);
			this.validationForBusinessLogsLogInLogOutNotNull(len);
		}
	}

	validationForBusinessLogsLogInNotNullLogOutNull(len){
		for (let i = len - 1; i < len; ++i) {

			if (this.logs[0].business_logs[i].log_in != null && this.logs[0].business_logs[i].log_out == null) {
				this.btnMeetingIn = false;
				this.btnSeminarIn = false;
				this.btnOutOfOfficeIn = false;
				this.btnLunchIn = false;
				this.btnLogOut = false;
				

				if (this.logs[0].business_logs[i].type == 'Meeting') {
					this.btnMeetingIn = false;
					this.btnMeetingOut = true;
				}
				else if (this.logs[0].business_logs[i].type == 'Seminar') {
					this.btnSeminarIn = false;
					this.btnSeminarOut = true;
				}
				else if (this.logs[0].business_logs[i].type == 'Out of the Office') {
					this.btnOutOfOfficeIn = false;
					this.btnOutOfOfficeOut = true;
				}
			}
		}
	}

	validationForBusinessLogsLogInLogOutNotNull(len){
		let temp = 0;

		for (let i = 0; i < len; ++i) {

			let a = moment(this.logs[0].business_logs[i].log_in);
			let b = moment(this.logs[0].business_logs[i].log_out);
			temp = Math.abs(a.diff(b, 'hours') * 4.16);

			if (this.logs[0].business_logs[i].log_in != null && this.logs[0].business_logs[i].log_out != null) {
				if (this.logs[0].business_logs[i].type == 'Meeting') {
					this.btnMeetingIn = false;
					this.btnMeetingOut = false;
					this.meeting = temp;
				}
				else if (this.logs[0].business_logs[i].type == 'Seminar') {
					this.btnSeminarIn = false;
					this.btnSeminarOut = false;
					this.seminar = temp;
				}
				else if (this.logs[0].business_logs[i].type == 'Out of the Office') {
					this.btnOutOfOfficeIn = false;
					this.btnOutOfOfficeOut = false;
					this.office = temp;
				}
			}
		}
	}

	submit(type){

		let today = moment().format("YYYY-MM-DD");
		let datetime = moment().format("YYYY-MM-DD HH:mm");


		this.AttendanceForm.value.type = type;
		this.AttendanceForm.value.datetime = datetime;
		this.AttendanceForm.value.date = today;
		this.AttendanceForm.value.id = this.id;

		let model = this.AttendanceForm.value;

		this._attendservice.createAttendanceFromWeb(model).
	      	subscribe(
	        data => {
	          let dat =data;
	          this.loaderForAttend = true;

	          this.getLogs();
	          
	        },
	        err => console.error(err)
	    );

	}

	exchange(){
		if(this.btnLogIn == true){
			this.btnLogOut = true;
			this.btnLogIn = false;
			this.btnLunchIn = false;
			this.btnLunchOut = false;
			this.btnMeetingIn = false;
			this.btnMeetingOut = false;
			this.btnOutOfOfficeIn = false;
			this.btnOutOfOfficeOut = false;
			this.btnSeminarIn = false;
			this.btnSeminarOut = false;
		}
		else if (this.btnLogOut == true) {
			this.btnLogOut = false;
			this.btnLogIn = true;
			this.btnLunchIn = false;
			this.btnLunchOut = false;
			this.btnMeetingIn = false;
			this.btnMeetingOut = false;
			this.btnOutOfOfficeIn = false;
			this.btnOutOfOfficeOut = false;
			this.btnSeminarIn = false;
			this.btnSeminarOut = false;
			// this.log = 0;
			this.lunch = 0;
			this.meeting = 0;
			this.seminar = 0;
			this.office = 0;
		}

	}

	generatePayroll(){
		this._payroll_serv.generatePayrollslip(this.employee_id).
		subscribe(
			data => {
	          window.open(data.url, "_blank");
    	});
	}

  	ngOnDestroy() {
	    //remove the the body classes
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	addBusinessLogs(type) {

		let today = moment().format("YYYY-MM-DD");
		let datetime = moment().format("YYYY-MM-DD HH:mm");


		this.AttendanceForm.value.type = type;
		this.AttendanceForm.value.datetime = datetime;
		this.AttendanceForm.value.date = today;
		this.AttendanceForm.value.id = this.id;

		let model = this.AttendanceForm.value;
		let message = '';

		if (type == 'meetingIn') {
			message = 'Agenda of the meeting:';
		}
		else if(type == 'seminarIn'){
			message = 'Purpose of Seminar:';
		}
		else if(type == 'outOffice'){
			message = 'Where are you going and what things you will do there:';
		}

		let disposable = this.modalService.addDialog(RemarksModalComponent, {
            title:'Business Logs',
            logs:true,
            data:model,
            message:message
        	}).subscribe((isConfirmed)=>{
	          this.loaderForAttend = true;
	          this.getLogs();
            });
	}

	viewBusinessLogs(x){
		
		let disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title:'Attendance',
            businesslogs:true,
            dataAtt:x.business_logs
        	}).subscribe((isConfirmed)=>{
                this.getLogs();

            });
	}

	logsValidation(){
		
		this._attendservice.logsValidation().
		subscribe(
			data => {
				let user = data;
				if (user.length != 0) {
					this.button = true;
				}
				this.getLogs();

				
			},
			err => console.error(err)
		);
	}

	getTask(){

		this._task_service.getMyTaskForDashboard().
		subscribe(
			data => {
				this.task = data;
				this.loaderForTask = false;
			},
			err => console.error(err)
		);
	}

	getOnboarding(){

		this.AttendanceForm.value.employee_id = this.employee_id;
  		let model = this.AttendanceForm.value;

  		let temp =[];

		this._onboarding_service.getMyOnBoardingList(model).
		subscribe(
			data => {

				 temp = data;
				 let x = 0;

				if (temp != []) {
					let len = temp.length
					for (let i = 0; i < len; ++i) {
						if (temp[i].status == 'PENDING') {
							x++;
						}
					}
				}

				if (x != 0) {
					this.checkAllOnboard = true;
					this.btnOnboarding = true;
				}

				this.onboarding = temp;
				this.loaderForOnboarding = false;
			},
			err => console.error(err)
		);
	}

	checkOnboarding(index){

		if(this.onboarding[index].val == false){
			this.onboarding[index].val = true;

		}
		else{
			this.onboarding[index].val = false;
			this.checkAllValOnboarding = false;
		}

		let len = this.onboarding.length;
		let i = 0;
		for (let x = 0; x < len; ++x) {
			
			if(this.onboarding[x].val == true){
				i++;
			}
		}

		if(i > 0){
			this.btnOnboarding = true;
		}
		else{
			this.btnOnboarding = false;
		}

	}

	checkAllOnboarding(){

		if(this.checkAllValOnboarding == false){
			let leng = this.onboarding.length;
			for (let i = 0; i < leng; i++) {
				if (this.onboarding[i].status == 'PENDING') {
					this.onboarding[i].val = true;
				}
			}

			this.checkAllValOnboarding = true;
			this.btnOnboarding = true;
		}
		else{
			let len = this.onboarding.length;
			for (let i = 0; i < len; i++) {
				if (this.onboarding[i].status == 'PENDING') {
					this.onboarding[i].val = false;
				}
			}
			this.checkAllValOnboarding = false;
			this.btnOnboarding = false;
		}
	}

	submitOnboarding(){

		this.connectToServer();

		let len = this.onboarding.length;
		let temp = [];
		for (let i = 0; i < len; ++i) {
			if (this.onboarding[i].val == true) {
				temp.push(this.onboarding[i].id);
			}
		}

		let model ={
			data:temp,
			table_id:this.onboarding[0].employee_onboarding_id,
		}

		this._onboarding_service.updateMyStatus(model).
		subscribe(
			data => {
				this.postore = data;
				this.socket.emit('notify', this.postore);
				this.loaderForOnboarding = true;
				this.checkAllOnboard = false;
				this.checkAllValOnboarding = false;
				this.getOnboarding();
			},
			err => console.error(err)
		);
	}

	getClearance(){

  		let temp =[];

		this._clearance_service.getMyClearanceList().
		subscribe(
			data => {

				temp = data;
				let x = 0;
				let y = 0;

				if (temp != []) {
					let len = temp.length
					for (let i = 0; i < len; ++i) {
						if (temp[i].status == 'PENDING') {
							x++;
						}
						if (temp[i].status == 'PENDING' || temp[i].status == 'CHECKING') {
							y++;
						}
					}
				}

				if (x != 0) {
					this.checkAllClear = true;
					this.btnClearance = true;
				}
				if (y != 0) {
					this.btnPrint = false;
				}else{
					this.btnPrint = true;
				}

				this.clearance = temp;
				this.loaderForClearance = false;
			},
			err => console.error(err)
		);
	}

	checkClearance(index){

		if(this.clearance[index].val == false){
			this.clearance[index].val = true;

		}
		else{
			this.clearance[index].val = false;
			this.checkAllValClearance = false;
		}

		let len = this.clearance.length;
		let i = 0;
		for (let x = 0; x < len; ++x) {
			
			if(this.clearance[x].val == true){
				i++;
			}
		}

		if(i > 0){
			this.btnClearance = true;
		}
		else{
			this.btnClearance = false;
		}

	}

	checkAllClearance(){

		if(this.checkAllValClearance == false){
			let leng = this.clearance.length;
			for (let i = 0; i < leng; i++) {

				if (this.clearance[i].status == 'PENDING') {
					this.clearance[i].val = true;
				}
			}

			this.checkAllValClearance = true;
			this.btnClearance = true;
		}
		else{
			let len = this.clearance.length;
			for (let i = 0; i < len; i++) {
				if (this.clearance[i].status == 'PENDING') {
					this.clearance[i].val = false;
				}
			}
			this.checkAllValClearance = false;
			this.btnClearance = false;
		}
	}

	submitClearance(){

		this.connectToServer();

		let len = this.clearance.length;
		let temp = [];
		for (let i = 0; i < len; ++i) {
			if (this.clearance[i].val == true) {
				temp.push(this.clearance[i].id);
			}
		}

		let model ={
			data:temp,
			created_by:this.user_id,
			receiver_id:this.supervisor_id,
			messenger_id:this.employee_id,
			table_id:this.clearance[0].employee_clearance_id,
		}

		this._clearance_service.updateMyStatus(model).
		subscribe(
			data => {
				this.postore = data;
				this.socket.emit('notify', this.postore);
				this.loaderForClearance = true;
				this.checkAllClear = false;
				this.checkAllValClearance = false;
				this.getClearance();
			},
			err => console.error(err)
		);
	}

	connectToServer(){
	    let socketUrl = this.host;
	    this.socket = io.connect(socketUrl);
	    this.socket.on("connect", () => this.connect());
	    this.socket.on("disconnect", () => this.disconnect());
	    
	    this.socket.on("error", (error: string) => {
	      console.log(`ERROR: "${error}" (${socketUrl})`);
	    });
	}

	// Handle connection opening
	private connect() {
	    // Request initial list when connected
	    this.socket.emit("join", 'Client User -> ');
	}
	// Handle connection closing
	private disconnect() {
	    console.log(`Disconnected from "${this.host}"`);
	}

	printClearance(){
		let id = this.clearance[0].employee_id;

		this._clearance_service.printClearance(id).subscribe(data => {
          window.open(data.url, "_blank");
    	});

	}

}
