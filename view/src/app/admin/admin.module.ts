import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, Validators, FormsModule, NgForm, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule  } from '@angular/platform-browser';
import { ChartsModule } from 'ng2-charts';
import { CalendarComponent } from "angular2-fullcalendar/src/calendar/calendar";    
import { DataTablesModule } from 'angular-datatables';
import { NgUploaderModule } from 'ngx-uploader';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { Select2Module } from 'ng2-select2';
import { TimepickerModule } from 'ngx-bootstrap';
import { MomentModule } from 'angular2-moment';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { MainSideComponent } from './main-side/main-side.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { FooterComponent } from './footer/footer.component';
import { ControlSidebarComponent } from './control-sidebar/control-sidebar.component';
import { Dashboard1Component } from './dashboard1/dashboard1.component';
import { DashboardComponent } from './dashboard1/dashboard.component';
import { EmployeeMassUploadComponent } from './employee/employee-mass-upload.component';
import { EmployeeListComponent } from './employee/employee-list.component';
import { EmployeeRemarksListComponent } from './employee/employee-remarks-list.component';
import { EmployeeRemarksViewComponent } from './employee/employee-remarks-view.component';
import { EmployeeRemarksCreateComponent } from './employee/employee-remarks-create.component';
import { EmployeeCreateComponent } from './employee/employee-create.component';
import { EmployeeEditComponent } from './employee/employee-edit.component';
import { EmployeeAuditListComponent } from './employee/employee-audit-list.component';
import { EmployeeAuditLogsComponent } from './employee/employee-audit-logs.component';
import { Configuration } from '../app.config';
import { SSSService } from '../services/sss.service';
import { AppraisalService } from '../services/appraisal.service';
import { DevelopmentService } from '../services/development.service';
import { QuestionService } from '../services/question.service';
import { SegmentService } from '../services/segment.service';
import { CategoryService } from '../services/category.service';
import { MemoService } from '../services/memo.service';
import { RatingService } from '../services/rating.service';
import { HolidayTypeService } from '../services/holiday-type.service';
import { UsersService } from '../services/users.service';
import { UsersListComponent } from './users/users-list.component';
import { UsersCreateComponent } from './users/users-create.component';
import { UsersEditComponent } from './users/users-edit.component';
import { UsersCreateModalComponent } from './users/users-create-modal.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { RoleService } from '../services/role.service';
import { RoleListComponent } from './role/role-list.component';
import { RoleCreateComponent } from './role/role-create.component';
import { RoleEditComponent } from './role/role-edit.component';
import { PermissionService } from '../services/permission.service';
import { PermissionListComponent } from './permission/permission-list.component';
import { PermissionCreateComponent } from './permission/permission-create.component';
import { PermissionEditComponent } from './permission/permission-edit.component';
import { AdjustmentTypeService } from '../services/adjustment-type.service';
import { AdjustmentTypeListComponent } from './adjustment/adjustment-type-list.component';
import { AdjustmentTypeCreateComponent } from './adjustment/adjustment-type-create.component';
import { AdjustmentTypeEditComponent } from './adjustment/adjustment-type-edit.component';
import { AdjustmentService } from '../services/adjustment.service';
import { AdjustmentListComponent } from './adjustment/adjustment-list.component';
import { AdjustmentListModalComponent } from './adjustment/adjustment-list-modal.component';
import { AdjustmentCreateEmployeeComponent } from './adjustment/adjustment-create-employee.component';
import { AdjustmentCreateCompanyComponent } from './adjustment/adjustment-create-company.component';
import { AdjustmentCreateDepartmentComponent } from './adjustment/adjustment-create-department.component';
import { AdjustmentCreatePositionComponent } from './adjustment/adjustment-create-position.component';
import { AdjustmentEditComponent } from './adjustment/adjustment-edit.component';
import { LoanTypeService } from '../services/loan-type.service';
import { LoanTypeListComponent } from './loan/loan-type-list.component';
import { LoanTypeCreateComponent } from './loan/loan-type-create.component';
import { LoanTypeEditComponent } from './loan/loan-type-edit.component';
import { LoanService } from '../services/loan.service';
import { LoanListComponent } from './loan/loan-list.component';
import { LoanListModalComponent } from './loan/loan-list-modal.component';
import { LoanCreateEmployeeComponent } from './loan/loan-create-employee.component';
import { LoanCreateCompanyComponent } from './loan/loan-create-company.component';
import { LoanCreateDepartmentComponent } from './loan/loan-create-department.component';
import { LoanCreatePositionComponent } from './loan/loan-create-position.component';
import { LoanEditComponent } from './loan/loan-edit.component';
import { DynastyLoanService } from '../services/dynastyloan.service';
import { DynastyLoanListComponent } from './dynasty-loan-admin/dynasty-loan-list.component';
import { DynastyLoanEditComponent } from './dynasty-loan-admin/dynasty-loan-edit.component';
import { DynastyLoanCreateComponent } from './dynasty-loan-user/dynasty-loan-create.component';
import { MenuService } from '../services/menu.service';
import { MenuListComponent } from './menu/menu-list.component';
import { MenuCreateComponent } from './menu/menu-create.component';
import { MenuEditComponent } from './menu/menu-edit.component';
import { BillingRateService } from '../services/billing-rate.service';
import { BillingRateListComponent } from './billing_rate/billing-rate-list.component';
import { BillingRateCreateComponent } from './billing_rate/billing-rate-create.component';
import { BillingRateEditComponent } from './billing_rate/billing-rate-edit.component';
import { ModuleService } from '../services/module.service';
import { ModuleListComponent } from './module/module-list.component';
import { ModuleCreateComponent } from './module/module-create.component';
import { ModuleEditComponent } from './module/module-edit.component';
import { AuthService } from '../services/auth.service';
import { AuthGuard } from './../services/auth-guard.service';
import { HttpClient } from './../services/http-extended.service';
import { Request, XHRBackend, RequestOptions, Response, Http, RequestOptionsArgs, Headers, HttpModule } from '@angular/http';
import { ExtendedHttpService } from './../services/error-handler.service';
import { PayrollListComponent } from './payroll/payroll-list.component';
import { PayrollViewComponent } from './payroll/payroll-view.component';
import { PayrollCreateComponent } from './payroll/payroll-create.component';
import { PayrollBillingComponent } from './payroll/payroll-billing.component';
import { PayrollBillingLocationComponent } from './payroll/payroll-billing-location.component';
import { PayrollBillingLocationListComponent } from './payroll/payroll-billing-location-list.component';
import { PayrollPayslipComponent } from './payroll/payroll-payslip.component';
import { PayrollAllowanceModalComponent } from './payroll/payroll-allowance.component';
import { PayrollService } from '../services/payroll.service';
import { LeaveService } from '../services/leave.service'; 
import { EmployeeService } from '../services/employee.service';
import { Dashboard1Service } from '../services/dashboard1.service';
import { LeaveListComponent } from './leave/leave-list/leave-list.component';
import { AttendanceListComponent } from './attendance/attendance-list/attendance-list.component';
import { AttendanceCreateComponent } from './attendance/attendance-create/attendance-create.component';
import { AttendanceUploadComponent } from './attendance/attendance_upload/attendance-upload.component';
import { AttendanceService } from '../services/attendance.service';
import { LeaveTypeComponent } from './leave-type/leave-type.component';
import { LeaveTypeService } from '../services/leave-type.service';
import { AttendanceTypeComponent } from './attendance-type/attendance-type.component';
import { AttendanceTypeService } from '../services/attendance-type.service';
import { CompanyComponent } from './company/company.component';
import { CompanyBranchComponent } from './company/company-branch-list.component';
import { CompanyBranchEditComponent } from './company/company-branch-edit.component';
import { CompanyWorkingScheduleComponent } from './company/company-working-schedule.component';
import { CompanyWorkingScheduleEditComponent } from './company/company-working-schedule-edit.component';
import { CompanyPolicyComponent } from './company/company-policy.component';
import { CompanyPolicyEditComponent } from './company/company-policy-edit.component';
import { CompanyService } from '../services/company.service';
import { DeductionComponent } from './deduction/deduction.component';
import { DeductionService } from '../services/deduction.service';
import { EventService } from '../services/event.service';
import { DepartmentComponent } from './department/department.component';
import { DepartmentService } from '../services/department.service';
import { HolidayComponent } from './holiday/holiday.component';
import { HolidayService } from '../services/holiday.service';
import { PagibigComponent } from './pagibig/pagibig.component';
import { PagibigService } from '../services/pagibig.service';
import { AttendanceReportService } from '../services/attendance-report.service';
import { PhilhealthComponent } from './philhealth/philhealth.component';
import { PhilhealthService } from '../services/philhealth.service';
import { PositionComponent } from './position/position.component';
import { PositionService } from '../services/position.service';
import { RankTypeComponent } from './rank-type/rank-type.component';
import { RankTypeService } from '../services/rank-type.service';
import { GovernmentDeductionService } from '../services/government-deduction.service';
import { GovernmentDeductionComponent } from './government-deduction/government-deduction.component';
import { ConfirmComponent } from './attendance/confirm.component';
import { ProfileComponent } from './profile/profile.component';
import { DeductionEditComponent } from './deduction/deduction-edit.component';
import { DeductionAddComponent } from './deduction/deduction-add.component';
import { FileDropDirective, FileUploader, FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { ChatBotComponent } from './chat-bot/chat-bot.component'; // Chat Bot
import { SSSComponent } from './sss/sss.component';
import { CalendarEditComponent } from './dashboard1/calendar-edit.component';
import { EventComponent } from './event/event.component';
import { CommonService } from '../services/common.service';
import { OfficialBusinessListComponent } from './officialbusiness/official-business-list/official-business-list.component';
import { OfficialBusinessService } from '../services/official-business.service';
import { CertificateOfAttendanceListComponent } from './certificate-of-attendance/certificate-of-attendance-list/certificate-of-attendance-list.component';
import { CertificateOfAttendanceService } from '../services/certificate-of-attendance.service';
import { Daterangepicker } from 'ng2-daterangepicker';
import { EventTypeComponent } from './event-type/event-type.component';
import { EventTypeService } from '../services/event-type.service';
import { OfficialUndertimeListComponent } from './official-undertime/official-undertime-list/official-undertime-list.component';
import { OfficialUndertimeService } from '../services/official-undertime.service';
import { ScheduleAdjustmentsListComponent } from './schedule-adjustments/schedule-adjustments-list/schedule-adjustments-list.component';
import { ScheduleAdjustmentsService } from '../services/schedule-adjustments.service';
import { OvertimeListComponent } from './overtime/overtime-list/overtime-list.component';
import { OvertimeService } from '../services/overtime.service';
import { EventListComponent } from './event/event-list/event-list.component';
import { TimetableComponent } from './timetable/timetable.component';
import { AttendanceReportComponent } from './attendance-report/attendance-report.component';
import { EventUpdateComponent } from './event/event-update/event-update.component';
import { OfficialBusinessTypeListComponent } from './official-business-type/official-business-type-list/official-business-type-list.component';
import { OfficialBusinessTypeService } from '../services/official-business-type.service';
import { CertificateOfAttendanceTypeListComponent } from './certificate-of-attendance-type/certificate-of-attendance-type-list/certificate-of-attendance-type-list.component';
import { CertificateOfAttendanceTypeService } from '../services/certificate-of-attendance-type.service';
import { MyLeaveListComponent } from './leave/my-leave-list/my-leave-list.component';
import { MyCertificateOfAttendanceListComponent } from './certificate-of-attendance/my-certificate-of-attendance-list/my-certificate-of-attendance-list.component';
import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';
import { MyUndertimeListComponent } from './official-undertime/my-undertime-list/my-undertime-list.component';
import { MyOfficialBusinessListComponent } from './officialbusiness/my-official-business-list/my-official-business-list.component';
import { MyOvertimeListComponent } from './overtime/my-overtime-list/my-overtime-list.component';
import { MyScheduleAdjustmentsListComponent } from './schedule-adjustments/my-schedule-adjustments-list/my-schedule-adjustments-list.component';
import { OvertimeModalComponent } from './overtime/overtime-modal/overtime-modal.component';
import { ScheduleAdjustmentsModalComponent } from './schedule-adjustments/schedule-adjustments-modal/schedule-adjustments-modal.component';
import { UndertimeModalComponent } from './official-undertime/undertime-modal/undertime-modal.component';
import { OfficialBusinessModalComponent } from './officialbusiness/official-business-modal/official-business-modal.component';
import { CertificateOfAttendanceModalComponent } from './certificate-of-attendance/certificate-of-attendance-modal/certificate-of-attendance-modal.component';
import { LeaveModalComponent } from './leave/leave-modal/leave-modal.component';
import { TypeModalComponent } from './type-modal/type-modal.component';
import { EventTypeModalComponent } from './event-type/event-type-modal/event-type-modal.component';
import { AttendanceModalComponent } from './attendance/attendance-modal/attendance-modal.component';
import { MyAttendanceListComponent } from './attendance/my-attendance-list/my-attendance-list.component';
import { HolidayTypeComponent } from './holiday/holiday-type/holiday-type.component';
import { RemarksModalComponent } from './remarks-modal/remarks-modal.component';
import { HolidayModalComponent } from './holiday/holiday-modal/holiday-modal.component';
import { LateComerListComponent } from './late-comer-list/late-comer-list.component';
import { ResolutionModalComponent } from './resolution-modal/resolution-modal.component';
import { ActionCenterComponent } from './action-center/action-center.component';
import { MemoComponent } from './memo/memo.component';
import { ActionCenterModalComponent } from './action-center/action-center-modal/action-center-modal.component';
import { MemoModalComponent } from './memo/memo-modal/memo-modal.component';
import { ReportsComponent } from './reports/reports.component';
import { ValidationModalComponent } from './validation-modal/validation-modal.component';
import { DownloadFileModalComponent } from './download-file-modal/download-file-modal.component';
import { LeaveValidationComponent } from './leave-validation/leave-validation.component';
import { EmployeeDashboardComponent } from './employee-dashboard/employee-dashboard.component';
import { CategoryComponent } from './category/category.component';
import { CategoryModalComponent } from './category/category-modal/category-modal.component';
import { QuestionComponent } from './question/question.component';
import { QuestionModalComponent } from './question/question-modal/question-modal.component';
import { EvaluationComponent } from './evaluation/evaluation.component';
import { EvaluationModalComponent } from './evaluation/evaluation-modal/evaluation-modal.component';
import { SegmentComponent } from './segment/segment.component';
import { SegmentModalComponent } from './segment/segment-modal/segment-modal.component';
import { RatingComponent } from './rating/rating.component';
import { RatingModalComponent } from './rating/rating-modal/rating-modal.component';
import { SubEvaluationComponent } from './evaluation/sub-evaluation/sub-evaluation.component';
import { NotificationService } from '../services/notification.service';
import { EvaluationFormComponent } from './evaluation/evaluation-form/evaluation-form.component';
import { NotificationListComponent } from './notification-list/notification-list.component';
import { AttendanceImportReviewComponent } from './attendance/attendance-import-review/attendance-import-review.component';
import { AttendanceImportModalComponent } from './attendance/attendance-import-modal/attendance-import-modal.component';
import { EvaluationEmployeeComponent } from './evaluation/evaluation-employee/evaluation-employee.component';
import { EmployeeRateComponent } from './evaluation/employee-rate/employee-rate.component';
import { HolidayTypeModalComponent } from './holiday/holiday-type-modal/holiday-type-modal.component';
import { TaskService } from '../services/task.service';
import { TaskListComponent } from './task-list/task-list/task-list.component';
import { TaskModalComponent } from './task-list/task-modal/task-modal.component';
import { MyTaskListComponent } from './task-list/my-task-list/my-task-list.component';
import { ObjApprovalComponent } from './obj-approval/obj-approval.component';
import { AppraisalComponent } from './appraisal/appraisal.component';
import { GoalComponent } from './goal/goal.component';
import { DevelopmentModalComponent } from './appraisal/development-modal/development-modal.component';
import { ObjectiveModalComponent } from './appraisal/objective-modal/objective-modal.component';
import { AlertModalComponent } from './alert-modal/alert-modal.component';
import { ObjRateComponent } from './obj-approval/obj-rate/obj-rate.component';
import { OnboardingListComponent } from './onboarding/onboarding-list/onboarding-list.component';
import { OnboardingService } from '../services/onboarding.service';
import { OnboardingChecklistComponent } from './onboarding/onboarding-checklist/onboarding-checklist.component';
import { ClearanceListComponent } from './clearance/clearance-list/clearance-list.component';
import { ClearanceService } from '../services/clearance.service';
import { GoalService } from '../services/goal.service';
import { ClearanceChecklistComponent } from './clearance/clearance-checklist/clearance-checklist.component';
import { ChecklistModalComponent } from './checklist-modal/checklist-modal.component';
import { GoalModalComponent } from './goal/goal-modal/goal-modal.component';
import { ClearanceModalComponent } from './clearance/clearance-modal/clearance-modal.component';
import { ArchwizardModule } from 'ng2-archwizard';
import { AttendanceListDefaultComponent } from './attendance/attendance-list-default/attendance-list-default.component';
import { BiometricsListComponent } from './biometrics/biometrics-list/biometrics-list.component';
import { BiometricsService } from '../services/biometrics.service';
import { BiometricsModalComponent } from './biometrics/biometrics-modal/biometrics-modal.component';
import { CompanyPolicyTypeListComponent } from './company/company-policy-type-list/company-policy-type-list.component';
import { CompanyPolicyTypeModalComponent } from './company/company-policy-type-modal/company-policy-type-modal.component';

import { ScheduleMasterCreateComponent } from './schedule-master/schedule-master-create.component';
import { ScheduleMasterEditComponent } from './schedule-master/schedule-master-edit.component';
import { ScheduleMasterListComponent } from './schedule-master/schedule-master-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    Select2Module,
    AdminRoutingModule,
    DataTablesModule,
    ReactiveFormsModule,
    BrowserModule,
    BootstrapModalModule,
    NgUploaderModule,
    ChartsModule,
    MomentModule,
    HttpModule,
    Daterangepicker,
    ArchwizardModule,
    TimepickerModule.forRoot()
  ],
  declarations: [
  	GoalModalComponent,
  	ChecklistModalComponent,
  	ClearanceChecklistComponent,
  	ClearanceListComponent,
  	OnboardingChecklistComponent,
  	OnboardingListComponent,
    CalendarComponent,
    AdminComponent, 
    MainSideComponent, 
    AlertModalComponent,
    MainHeaderComponent, 
    FooterComponent, 
    ControlSidebarComponent, 
    Dashboard1Component,
    ChatBotComponent,
    LateComerListComponent,
    ResolutionModalComponent,
    DevelopmentModalComponent,
    EmployeeListComponent,
    EmployeeEditComponent,
    EmployeeCreateComponent,
    EmployeeMassUploadComponent,
    EmployeeRemarksListComponent,
    EmployeeRemarksViewComponent,
    EmployeeRemarksCreateComponent,
    EmployeeAuditListComponent,
    EmployeeAuditLogsComponent,
    UsersListComponent,
    UsersCreateModalComponent,
    UsersCreateComponent,
    UsersEditComponent,
    ChangePasswordComponent,
    ProfileComponent,
    RoleListComponent,
    RoleCreateComponent,
    RoleEditComponent,
    PermissionListComponent,
    PermissionCreateComponent,
    PermissionEditComponent,
    AdjustmentTypeListComponent,
    AdjustmentListModalComponent,
    AdjustmentTypeCreateComponent,
    AdjustmentTypeEditComponent,
    AdjustmentListComponent,
    AdjustmentCreateEmployeeComponent,
    AdjustmentCreateCompanyComponent,
    AdjustmentCreateDepartmentComponent,
    AdjustmentCreatePositionComponent,
    AdjustmentEditComponent,
    LoanTypeListComponent,
    LoanTypeCreateComponent,
    LoanTypeEditComponent,
    LoanListComponent,
    LoanListModalComponent,
    LoanCreateEmployeeComponent,
    LoanCreateCompanyComponent,
    LoanCreateDepartmentComponent,
    LoanCreatePositionComponent,
    LoanEditComponent,
    DynastyLoanCreateComponent,
    DynastyLoanListComponent,
    DynastyLoanEditComponent,
    MenuListComponent,
    MenuCreateComponent,
    MenuEditComponent,
    ModuleListComponent,
    ModuleCreateComponent,
    ModuleEditComponent,
    BillingRateListComponent,
	BillingRateCreateComponent,
	BillingRateEditComponent,
    PayrollListComponent,
    PayrollViewComponent,
    PayrollCreateComponent,
    PayrollPayslipComponent,
    PayrollBillingComponent,
    PayrollBillingLocationComponent,
    PayrollBillingLocationListComponent,
    ObjectiveModalComponent,
    PayrollAllowanceModalComponent,
    MemoComponent,
    LeaveTypeComponent,
    AttendanceTypeComponent,
    CompanyComponent,
    CompanyBranchComponent,
    CompanyBranchEditComponent,
    CompanyPolicyComponent,
    CompanyPolicyEditComponent,
    CompanyWorkingScheduleComponent,
    CompanyWorkingScheduleEditComponent,
    DeductionComponent,
    DepartmentComponent,
    HolidayComponent,
    PagibigComponent,
    PhilhealthComponent,
    PositionComponent,
    RankTypeComponent,
    ConfirmComponent,
    LeaveListComponent,
    AttendanceListComponent,
    AttendanceCreateComponent,
    HolidayTypeComponent,
    GovernmentDeductionComponent,
    DeductionEditComponent,
    DeductionAddComponent,
    SSSComponent,
    CalendarEditComponent,
    EventComponent,
    SSSComponent,
    OfficialBusinessListComponent,
    CertificateOfAttendanceListComponent,
    EventTypeComponent,
    OfficialUndertimeListComponent,
    ScheduleAdjustmentsListComponent,
    OvertimeListComponent,
    // EventTypeComponent
    EventListComponent,
    GoalComponent,
    // EventTypeComponent
    
    TimetableComponent,
    AppraisalComponent,
    AttendanceReportComponent,
    
    EventUpdateComponent,
    OfficialBusinessTypeListComponent,
    CertificateOfAttendanceTypeListComponent,
    MyLeaveListComponent,
    MyCertificateOfAttendanceListComponent,
    ConfirmModalComponent,
    MyUndertimeListComponent,
    MyOfficialBusinessListComponent,
    MyOvertimeListComponent,
    MyScheduleAdjustmentsListComponent,
    OvertimeModalComponent,
    ScheduleAdjustmentsModalComponent,
    UndertimeModalComponent,
    OfficialBusinessModalComponent,
    CertificateOfAttendanceModalComponent,
    LeaveModalComponent,
    TypeModalComponent,
    EventTypeModalComponent,
    AttendanceModalComponent,
    AttendanceUploadComponent,
    MyAttendanceListComponent,
    RemarksModalComponent,
    HolidayModalComponent,
    ActionCenterComponent,
    ActionCenterModalComponent,
    MemoModalComponent,
    ReportsComponent,
    ValidationModalComponent,
    DownloadFileModalComponent,
    LeaveValidationComponent,
    EmployeeDashboardComponent,
    NotificationListComponent,
    AttendanceImportReviewComponent,
    AttendanceImportModalComponent,
    CategoryComponent,
    CategoryModalComponent,
    QuestionComponent,
    QuestionModalComponent,
    EvaluationComponent,
    EvaluationModalComponent,
    SegmentComponent,
    SegmentModalComponent,
    RatingComponent,
    RatingModalComponent,
    SubEvaluationComponent,
    EvaluationFormComponent,
    EvaluationEmployeeComponent,
    EmployeeRateComponent,
    HolidayTypeModalComponent,
    TaskListComponent,
    TaskModalComponent,
    MyTaskListComponent,
    ObjApprovalComponent,
    ObjRateComponent,
    ClearanceModalComponent,
    AttendanceListDefaultComponent,
    BiometricsListComponent,
    BiometricsModalComponent,
    CompanyPolicyTypeListComponent,
    CompanyPolicyTypeModalComponent,
    DashboardComponent,

    ScheduleMasterCreateComponent,
    ScheduleMasterEditComponent,
    ScheduleMasterListComponent
  ],
  entryComponents: [
        ConfirmComponent
   ],
  providers: [
    { provide: Http, useClass: ExtendedHttpService },
    Configuration,
    AuthGuard,
    OnboardingService,
    ClearanceService,
    AuthService,
    EmployeeService,
    RatingService,
    FormBuilder,
    RoleService ,
    UsersService,
    PermissionService,  
    AdjustmentTypeService,
    LeaveTypeService, 
    AttendanceTypeService, 
    CompanyService, 
    DeductionService, 
    DepartmentService, 
    HolidayService, 
    PagibigService,
    PhilhealthService,
    PositionService,
    RankTypeService,
    HolidayTypeService,
    AttendanceService,
    GovernmentDeductionService,
    LeaveService,
    HttpClient,
    SSSService,
    Dashboard1Service,
    AppraisalService,
    EventService,
    EventTypeService,
    QuestionService,
    SegmentService,
    CommonService,
    MenuService,
    ModuleService,
    AdjustmentService,
    AdjustmentTypeService,
    LoanService,
    LoanTypeService,
    PayrollService,
    BillingRateService,
    OfficialBusinessService,
    CertificateOfAttendanceService,
    OfficialUndertimeService,
    ScheduleAdjustmentsService,
    DevelopmentService,
    OvertimeService,
    OfficialBusinessTypeService,
    CertificateOfAttendanceTypeService,
    MemoService,
    AttendanceReportService,
    CategoryService,
    NotificationService,
    TaskService,
    GoalService,
    DynastyLoanService,
    BiometricsService
  ], // Define services here
  exports: [
    AdminComponent,
    MainSideComponent, 
    MainHeaderComponent, 
    FooterComponent, 
    ControlSidebarComponent    
  ]
})
export class AdminModule { }