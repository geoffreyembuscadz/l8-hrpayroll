import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild,trigger,state,style,animate,transition,keyframes } from '@angular/core';import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { QuestionService } from '../../../services/question.service';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng2-select2';
import { AuthUserService } from '../../../services/auth-user.service';
import { EvaluationModalComponent } from '../../evaluation/evaluation-modal/evaluation-modal.component';

@Component({
  selector: 'app-employee-rate',
  templateUrl: './employee-rate.component.html',
  styleUrls: ['./employee-rate.component.css'],

  animations: [
    trigger('hideShowAnimator', [
        state('true' , style({ opacity: 1 })), 
        state('false', style({ opacity: 0 })),
        transition('0 => 1', animate('.5s')),
        transition('1 => 0', animate('.9s'))
    	])
  	]
})
export class EmployeeRateComponent implements OnInit, AfterViewInit {
	hideShowAnimator:boolean = true;
    hideShowAnimation(){
        this.hideShowAnimator = !this.hideShowAnimator;
    }

	public evaluationForm: FormGroup;
	@Input() items: Array<any> = [];
	
	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public emp_contract_statuses: any;
	public poststore: any;
    private hidden:string="hidden";

	api: String;
	user_id:any;
	supervisor_id: any;
	all_emp: any;
	selectedEmployee: any = null;
	evaluation_form: any;
	rate: any
	emp_id: any;
	temp: any;
	filter_employee: any;
	view_evaluattion: any;
	edit : any;
	rates: any;
	val : any;
	avg: any;
	questions: Array<string> = [];
	selectedIndex: number;
	curPage : number;
  	pageSize : number;
    parttwo = false;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _q_service: QuestionService,
		private _fb: FormBuilder,
		private _auth_service: AuthUserService
		){

		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");

		this.api = this._conf.ServerWithApiUrl + 'evaluation/';

		this.evaluationForm = 
		_fb.group({
			'employee_id':				['null'],
			'emp_name':					['null'],
			'position_id':				['null'],
			'segment_id':				['null'],
			'question_id':				['null'],
			'rating_value':				['null'],
			'type':						['null'],
			'primary_strengths':    	['null'],
			'improvement_needs':    	['null'],
			'action_taken':    			['null'],
			'comments_recommendation':  ['null'],

		});

		 this.selectedIndex = 0;
		 this.curPage = 1;
    	 this.pageSize = 1;

	}

 	ngOnInit() {
 		let id = 0;
 		this.showEmployeeRate(id); 			 		
	}
	
	ngOnDestroy() {
		this.body.classList.remove("skin-blue");
		this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
		this.dtTrigger.next();
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}


	getEmployeeEvaluation(){
		this.evaluationForm.value.id = this.selectedEmployee.id;
		let model = this.evaluationForm.value;		
		this._q_service.getEmployeeEvaluation(model).
		subscribe(
			data => {
				let temp_data = data;
				this.evaluation_form  = temp_data; 


				 for (let i = 0; i < temp_data.length; i++) {
					for (let x = 0; x < temp_data[i].segments.length; ++x) {
						for (let y = 0; y < temp_data[i].segments[x].question_collection.length; ++y) {
						 this.questions = temp_data[i].segments[x].question_collection[y].question;
						}
						
					}
				}	
			},
			err => console.error(err)
			);
	}
	data = [];

	onSubmit() {
	
		let model = this.evaluation_form;
		this._q_service.createEvaluationForm(model)
		.subscribe(
			data => {
				this.poststore = Array.from(data); 
				let disposable = this.modalService.addDialog(EvaluationModalComponent, {
						title:'Evaluation Form',
						create:true
					}).subscribe((isConfirmed)=>{
						
					});
			},
			err => this.catchError(err)
			);
	}


	changeRatingValue(i,j,o,value){
		let obj = this.evaluation_form[i].segments[j];
		obj.question_collection[o].rating_value = value;
	}


	

	setActiveEmployee(employee: any) {
		this.selectedEmployee = employee;	
		let myJSON = JSON.stringify(this.selectedEmployee);
		if( this.selectedEmployee.rate > 0){
			this.viewEmployeeEvaluation();
		}  else {
			this.getEmployeeEvaluation();
		}	
	}

	resetSelectedEmployee(emp) {
		emp = null;
	}


	quickFilter(id){
    	this.evaluationForm.value.type = id;
		let model = this.evaluationForm.value;
		this._q_service.filterEmployee(model).
		subscribe(
			data => {
				this.rate = Array.from(data);
				for (var x = 0; x < this.rate.length; x++) {
					this.avg = this.rate[x].rate;
					if (this.avg >= '4.50') {
						this.rate[x].remarks = "Consistenly exceeds requirements";
					} else if (this.avg >= '3.50' && this.avg <= '4.49') {
						this.rate[x].remarks = "Occasionally exceeds requirements";
					}  else if (this.avg >= '2.50' && this.avg <= '3.49') {
						this.rate[x].remarks = "Meet requirements"
					} else if (this.avg >= '1.50' && this.avg <= '2.49') {
						this.rate[x].remarks = "Occasionally does not meet requirements"
					} else if (this.avg >= '1.00' && this.avg <= '1.49') {
						this.rate[x].remarks = "Consistently does not meet requirements"
					} else if (this.avg = '0.00'){
						this.rate[x].remarks = "Not yet rated"
					}
				}
			},
			err => console.error(err)
			);
	}

	showEmployeeRate(id){		
		this.quickFilter(id);
		this._q_service.showEmployeeRate().
		subscribe(
			data => {
				this.rate = data;	
				for (var x = 0; x < this.rate.length; x++) {
					this.avg = this.rate[x].rate;
					if (this.avg >= '4.50') {
						this.rate[x].remarks = "Consistenly exceeds requirements";
					} else if (this.avg >= '3.50' && this.avg <= '4.49') {
						this.rate[x].remarks = "Occasionally exceeds requirements";
					}  else if (this.avg >= '2.50' && this.avg <= '3.49') {
						this.rate[x].remarks = "Meet requirements"
					} else if (this.avg >= '1.50' && this.avg <= '2.49') {
						this.rate[x].remarks = "Occasionally does not meet requirements"
					} else if (this.avg >= '1.00' && this.avg <= '1.49') {
						this.rate[x].remarks = "Consistently does not meet requirements"
					} else if (this.avg = '0.00'){
						this.rate[x].remarks = "Not yet rated"
					}
				}



			},
			err => console.error(err)
		);
	}

	viewEmployeeEvaluation(){
		this.evaluationForm.value.id = this.selectedEmployee.id;
		let model = this.evaluationForm.value;		
		this._q_service.viewEmployeeEvaluation(model).
		subscribe(
			data => {
				this.view_evaluattion = JSON.parse(JSON.stringify(data || null ));
			},
			err => console.error(err)
			);
	}

	next() {
       ++this.selectedIndex;
    }

    previous() {
        --this.selectedIndex;
    }

    numberOfPages(){
    	this.getEmployeeEvaluation();
    	return Math.ceil(this.evaluation_form.length / this.pageSize);
  	};

  	part2(){
  		this.parttwo = true;
  	}

	

}

