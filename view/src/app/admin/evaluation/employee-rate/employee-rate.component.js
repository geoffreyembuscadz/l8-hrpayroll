var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, ViewChild, trigger, state, style, animate, transition } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { QuestionService } from '../../../services/question.service';
import { FormBuilder } from '@angular/forms';
import { AuthUserService } from '../../../services/auth-user.service';
import { EvaluationModalComponent } from '../../evaluation/evaluation-modal/evaluation-modal.component';
var EmployeeRateComponent = /** @class */ (function () {
    function EmployeeRateComponent(modalService, _conf, _q_service, _fb, _auth_service) {
        this.modalService = modalService;
        this._conf = _conf;
        this._q_service = _q_service;
        this._fb = _fb;
        this._auth_service = _auth_service;
        this.hideShowAnimator = true;
        this.items = [];
        this.dtTrigger = new Subject();
        this.hidden = "hidden";
        this.selectedEmployee = null;
        this.questions = [];
        this.parttwo = false;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.data = [];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api = this._conf.ServerWithApiUrl + 'evaluation/';
        this.evaluationForm =
            _fb.group({
                'employee_id': ['null'],
                'emp_name': ['null'],
                'position_id': ['null'],
                'segment_id': ['null'],
                'question_id': ['null'],
                'rating_value': ['null'],
                'type': ['null'],
                'primary_strengths': ['null'],
                'improvement_needs': ['null'],
                'action_taken': ['null'],
                'comments_recommendation': ['null'],
            });
        this.selectedIndex = 0;
        this.curPage = 1;
        this.pageSize = 1;
    }
    EmployeeRateComponent.prototype.hideShowAnimation = function () {
        this.hideShowAnimator = !this.hideShowAnimator;
    };
    EmployeeRateComponent.prototype.ngOnInit = function () {
        var id = 0;
        this.showEmployeeRate(id);
    };
    EmployeeRateComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    EmployeeRateComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    EmployeeRateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    EmployeeRateComponent.prototype.getEmployeeEvaluation = function () {
        var _this = this;
        this.evaluationForm.value.id = this.selectedEmployee.id;
        var model = this.evaluationForm.value;
        this._q_service.getEmployeeEvaluation(model).
            subscribe(function (data) {
            var temp_data = data;
            _this.evaluation_form = temp_data;
            for (var i = 0; i < temp_data.length; i++) {
                for (var x = 0; x < temp_data[i].segments.length; ++x) {
                    for (var y = 0; y < temp_data[i].segments[x].question_collection.length; ++y) {
                        _this.questions = temp_data[i].segments[x].question_collection[y].question;
                    }
                }
            }
        }, function (err) { return console.error(err); });
    };
    EmployeeRateComponent.prototype.onSubmit = function () {
        var _this = this;
        var model = this.evaluation_form;
        this._q_service.createEvaluationForm(model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            var disposable = _this.modalService.addDialog(EvaluationModalComponent, {
                title: 'Evaluation Form',
                create: true
            }).subscribe(function (isConfirmed) {
            });
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeRateComponent.prototype.changeRatingValue = function (i, j, o, value) {
        var obj = this.evaluation_form[i].segments[j];
        obj.question_collection[o].rating_value = value;
    };
    EmployeeRateComponent.prototype.setActiveEmployee = function (employee) {
        this.selectedEmployee = employee;
        var myJSON = JSON.stringify(this.selectedEmployee);
        if (this.selectedEmployee.rate > 0) {
            this.viewEmployeeEvaluation();
        }
        else {
            this.getEmployeeEvaluation();
        }
    };
    EmployeeRateComponent.prototype.resetSelectedEmployee = function (emp) {
        emp = null;
    };
    EmployeeRateComponent.prototype.quickFilter = function (id) {
        var _this = this;
        this.evaluationForm.value.type = id;
        var model = this.evaluationForm.value;
        this._q_service.filterEmployee(model).
            subscribe(function (data) {
            _this.rate = Array.from(data);
            for (var x = 0; x < _this.rate.length; x++) {
                _this.avg = _this.rate[x].rate;
                if (_this.avg >= '4.50') {
                    _this.rate[x].remarks = "Consistenly exceeds requirements";
                }
                else if (_this.avg >= '3.50' && _this.avg <= '4.49') {
                    _this.rate[x].remarks = "Occasionally exceeds requirements";
                }
                else if (_this.avg >= '2.50' && _this.avg <= '3.49') {
                    _this.rate[x].remarks = "Meet requirements";
                }
                else if (_this.avg >= '1.50' && _this.avg <= '2.49') {
                    _this.rate[x].remarks = "Occasionally does not meet requirements";
                }
                else if (_this.avg >= '1.00' && _this.avg <= '1.49') {
                    _this.rate[x].remarks = "Consistently does not meet requirements";
                }
                else if (_this.avg = '0.00') {
                    _this.rate[x].remarks = "Not yet rated";
                }
            }
        }, function (err) { return console.error(err); });
    };
    EmployeeRateComponent.prototype.showEmployeeRate = function (id) {
        var _this = this;
        this.quickFilter(id);
        this._q_service.showEmployeeRate().
            subscribe(function (data) {
            _this.rate = data;
            for (var x = 0; x < _this.rate.length; x++) {
                _this.avg = _this.rate[x].rate;
                if (_this.avg >= '4.50') {
                    _this.rate[x].remarks = "Consistenly exceeds requirements";
                }
                else if (_this.avg >= '3.50' && _this.avg <= '4.49') {
                    _this.rate[x].remarks = "Occasionally exceeds requirements";
                }
                else if (_this.avg >= '2.50' && _this.avg <= '3.49') {
                    _this.rate[x].remarks = "Meet requirements";
                }
                else if (_this.avg >= '1.50' && _this.avg <= '2.49') {
                    _this.rate[x].remarks = "Occasionally does not meet requirements";
                }
                else if (_this.avg >= '1.00' && _this.avg <= '1.49') {
                    _this.rate[x].remarks = "Consistently does not meet requirements";
                }
                else if (_this.avg = '0.00') {
                    _this.rate[x].remarks = "Not yet rated";
                }
            }
        }, function (err) { return console.error(err); });
    };
    EmployeeRateComponent.prototype.viewEmployeeEvaluation = function () {
        var _this = this;
        this.evaluationForm.value.id = this.selectedEmployee.id;
        var model = this.evaluationForm.value;
        this._q_service.viewEmployeeEvaluation(model).
            subscribe(function (data) {
            _this.view_evaluattion = JSON.parse(JSON.stringify(data || null));
        }, function (err) { return console.error(err); });
    };
    EmployeeRateComponent.prototype.next = function () {
        ++this.selectedIndex;
    };
    EmployeeRateComponent.prototype.previous = function () {
        --this.selectedIndex;
    };
    EmployeeRateComponent.prototype.numberOfPages = function () {
        this.getEmployeeEvaluation();
        return Math.ceil(this.evaluation_form.length / this.pageSize);
    };
    ;
    EmployeeRateComponent.prototype.part2 = function () {
        this.parttwo = true;
    };
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], EmployeeRateComponent.prototype, "items", void 0);
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], EmployeeRateComponent.prototype, "dtElement", void 0);
    EmployeeRateComponent = __decorate([
        Component({
            selector: 'app-employee-rate',
            templateUrl: './employee-rate.component.html',
            styleUrls: ['./employee-rate.component.css'],
            animations: [
                trigger('hideShowAnimator', [
                    state('true', style({ opacity: 1 })),
                    state('false', style({ opacity: 0 })),
                    transition('0 => 1', animate('.5s')),
                    transition('1 => 0', animate('.9s'))
                ])
            ]
        }),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            QuestionService,
            FormBuilder,
            AuthUserService])
    ], EmployeeRateComponent);
    return EmployeeRateComponent;
}());
export { EmployeeRateComponent };
//# sourceMappingURL=employee-rate.component.js.map