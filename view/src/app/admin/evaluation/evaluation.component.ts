import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { QuestionService } from '../../services/question.service';
import { CommonService } from '../../services/common.service';
import { EmployeeService } from '../../services/employee.service';
import { EvaluationModalComponent } from '../evaluation/evaluation-modal/evaluation-modal.component';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng2-select2';
import { Evaluation } from '../../model/evaluation';

@Component({
	selector: 'app-evaluation',
	templateUrl: './evaluation.component.html',
	styleUrls: ['./evaluation.component.css']
})

@Injectable()
export class EvaluationComponent implements OnInit, AfterViewInit {
	public evaluationForm: FormGroup;

	ratingOptions = [
    {name:'Overall Rating', value:'1'},
    {name:'Per Segment Rating', value:'2'}
  	]

	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public emp_contract_statuses: any;
	public poststore: any;

	public segment_value: Array<Select2OptionData>;
	public category_value: any;
	public subcategory_value: any;
	public emp_value: Array<Select2OptionData>;
	public position_value: Array<Select2OptionData>;
	public employment_value: Array<Select2OptionData>;
	public question_value: Array<Select2OptionData>;
	public options: Select2Options;
	public segment_current: any;
	public question_current: any;
	public position_current: any;
	public category_current: any;
	public subcategory_current: any;
	public collection_category = [];
	public collection_subcategory = [];
	public emp_current: any;
	public employment_current: any;
	dtOptions: any = {};
	api: String;

	emp_status: any;
	category: any;
	data: any;
	sub_cat: any;
	cat_id: any;
	id: any;
	title: string;
	supervisor_id: any;
	emp: any;
	select_employment_status: any;
	add_segment: any;
	add_category: any;
	add_subcategory: any;
	value: any;
	rating: any;
	add_position: any;
	add_question: any;
	add_form = false;
	add_cat = false;
	get_category: any;
	get_subcategory: any;
	get_question: any;
	cat_value = false;
	sub_value = false;
	forms = [];
	index = 1;
	query = false;
	subcat = false;
	rating_number_desc = [];
	get_last_rating: any;
	last_element;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _common_service: CommonService,
		private _q_service: QuestionService,
		private _emp_service: EmployeeService,
		private _fb: FormBuilder,
		
		){

		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");

		this.api = this._conf.ServerWithApiUrl + 'evaluation/';

		this.evaluationForm = 
		_fb.group({
			'name':					[null],	
			'position':				[null],
			'rating_count':			[null],
			'segments': this._fb.array([
				this.initSegment()])
		});
	}

    initSegment() {
        return this._fb.group({
        	segment:	 [''],
            question: 	 [''],
        });
    }

	ngOnInit() {
		this.getEmploymentStatus();
		this.getRating();
		this.addPosition();
		this.rating_number();
		}
		ngOnDestroy() {
		this.body.classList.remove("skin-blue");
		this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
		this.dtTrigger.next();
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	onSubmit() {

		this.evaluationForm.value.position = this.position_current;
		this.evaluationForm.value.rating_count = this.last_element.id;
		let model = this.evaluationForm.value;
		{
			this._q_service.storeEvaluationTemplate(model)
			.subscribe(
				data => {
					this.poststore = Array.from(data);
					let disposable = this.modalService.addDialog(EvaluationModalComponent, {
						title:'Evaluation Form',
						create:true
					}).subscribe((isConfirmed)=>{
						this.ngOnInit();

						this.evaluationForm.reset();

					});
				},
				err => this.catchError(err)
				);
		}
	}

	getEmploymentStatus(){
		this.emp_contract_statuses 
		= 
		this._emp_service.getEmploymentStatus();

	}
	
	
	getRating(){
		this._q_service.rating().
		subscribe(
			data => {
				this.rating = Array.from(data);	
			},
			err => console.error(err)
			);
	}

	addFormRow(){
		const control = <FormArray>this.evaluationForm.controls['segments'];
        control.push(this.initSegment());
	}

	addCat(){
		this.add_cat = true;

	}

	addPosition(){
		this._q_service.addPosition().
		subscribe(
			data => {
				this.add_position = Array.from(data);
				this.position_value = [];
				this.position_current = this.position_value;			
			},
			err => console.error(err)
			);
	}

	changedPosition(data: any) {
		this.position_current = data.value;
	}

	addQuestions(){
		this.query = true;
	}

	addSubCat(){
		this.subcat = true;
	}

	removeSegments(i: number){
		const control = <FormArray>this.evaluationForm.controls['segments'];
		control.removeAt(i);

	}

	addCategory(){
		this._q_service.addCategory().
		subscribe(
			data => {
				this.add_category = Array.from(data);
			},
			err => console.error(err)
			);
	}

	rating_number(){
		let description = [
				"Unsatisfactory", 
				"Improvement needed", 
				"Meets expectations", 
				"Improvement needed", 
				" Unsatisfactory"
			];
		for (let x = 1; x <= 5; x++) {
			let obj_rating = {
				"id": x,
				"text": description[x - 1]
			}
		  	this.rating_number_desc.push(obj_rating);		  	
		}
		this.last_element = this.rating_number_desc[this.rating_number_desc.length - 1];
	}

	resetDescription(x){
		// x.text = "";
		let id = this.rating_number_desc.map(e => function(e) { return e.text} ).indexOf(x.text);
		this.rating_number_desc.splice(id, 1);
		this.last_element = this.rating_number_desc[this.rating_number_desc.length - 1];

	}

	add_rating(x){
		let obj_rating = {
				"id": x + 1,
				"text": ""
			}
		this.rating_number_desc.push(obj_rating);
		this.last_element = this.rating_number_desc[this.rating_number_desc.length - 1];
	}
	
}
