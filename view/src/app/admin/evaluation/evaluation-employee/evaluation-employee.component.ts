import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild,trigger,state,style,animate,transition,keyframes } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { QuestionService } from '../../../services/question.service';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng2-select2';
import { AuthUserService } from '../../../services/auth-user.service';
import { EvaluationModalComponent } from '../../evaluation/evaluation-modal/evaluation-modal.component';

@Component({
	selector: 'app-evaluation-employee',
	templateUrl: './evaluation-employee.component.html',
	styleUrls: ['./evaluation-employee.component.css'],

	animations: [
    trigger('hideShowAnimator', [
        state('true' , style({ opacity: 1 })), 
        state('false', style({ opacity: 0 })),
        transition('0 => 1', animate('.5s')),
        transition('1 => 0', animate('.9s'))
    	])
  	]
})

@Injectable()
export class EvaluationEmployeeComponent implements OnInit, AfterViewInit {
	hideShowAnimator:boolean = true;
    hideShowAnimation(){
        this.hideShowAnimator = !this.hideShowAnimator;
    }

	public evaluationForm: FormGroup;
	@Input() items: Array<any> = [];
	
	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public emp_contract_statuses: any;
	public poststore: any;

	user_id:any;
	evaluation_form:any;
	employee_id: any;
	jsonParse: any;
	api: String;
	selectedRow : Number;
    setClickedRow : Function;
    private hidden:string="hidden";
    question: any[];
    temp: any;
    position: any;
    selectedEmployee: any = null;
    hahahaha: Boolean = false;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _q_service: QuestionService,
		private _fb: FormBuilder,
		private _auth_service: AuthUserService
		){

		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");

		this.api = this._conf.ServerWithApiUrl + 'evaluation/';

		this.evaluationForm = 
		_fb.group({
			'employee_id':			['null'],
			'position_id':			['null'],
			'question_id':			['null'],
			'rating_value':			['null']
		});

	}

 	ngOnInit() {
		this.get_UserId();
	}
	ngOnDestroy() {
		this.body.classList.remove("skin-blue");
		this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
		this.dtTrigger.next();
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	get_UserId(){
		this._auth_service.getUser().
		subscribe(
			data => {
				let user = data;
				this.user_id = user.id;
				this.getEmployeeId();
			},
			err => console.error(err)
		);
	}

	getEmployeeId(){
		this._auth_service.getUserById(this.user_id).
		subscribe(
			data => {
				let user = data;
				this.employee_id = user.employee_id;
				this.getEmployeeEvaluation();
			
			},
			err => console.error(err)
		);

	}

	getEmployeeEvaluation(){
		this.evaluationForm.value.id = this.employee_id;
		let model = this.evaluationForm.value;
	
		this._q_service.getEmployeeEvaluation(model).
		subscribe(
			data => {
				let temp_data= data;
				this.evaluation_form  = temp_data;
					// for (let i = 0; i < this.evaluation_form.length; i++) {
				// 	this.position = this.evaluation_form[i].p_id;
				// 	console.log(this.position);
					
				// }

				// this.name = temp_data[0].emp_name;
				// this.positions = temp_data[0].position;

				// let new_data = [];
				// for (let i = 0; i < temp_data.length; i++) {
				// 	for (let x = 0; x < temp_data[i].segments.length; ++x) {
				// 		for (let y = 0; y < temp_data[i].segments[x].question_collection.length; ++y) {
				// 			let employee_id = temp_data[i].emp_id;
				// 			let emp_name = temp_data[i].emp_name;
				// 			let position_id = temp_data[i].position_id;
				// 			let position = temp_data[i].position;
				// 			let question_id = temp_data[i].segments[x].question_collection[y].q_id;
				// 			let question = temp_data[i].segments[x].question_collection[y].question;
				// 			let segment_name = temp_data[i].segments[x].segment_name;
				// 			let rating_value = 0;

				// 			new_data.push({employee_id,emp_name,position_id,question_id,question,position,segment_name,rating_value});
				// 		}
						
				// 	}
				// }

				
			},
			err => console.error(err)
			);
	}
	data = [];

	onSubmit() {

		let model = this.evaluation_form;
		this._q_service.createEvaluationForm(model)
		.subscribe(
			data => {
				this.poststore = Array.from(data); 
				let disposable = this.modalService.addDialog(EvaluationModalComponent, {
						title:'Evaluation Form',
						create:true
					}).subscribe((isConfirmed)=>{
						
					});
			},
			err => this.catchError(err)
			);
	}


	changeRatingValue(i,j,o,value){
		console.log('i:',i);
		console.log('j:',j);
		console.log('o:',o);
		let obj = this.evaluation_form[0].segments[0];
		obj.question_collection[o].rating_value = value;
		console.log(obj.question_collection[o]);

	}

	




}
