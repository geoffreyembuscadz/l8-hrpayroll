var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, Input, ViewChild, trigger, state, style, animate, transition } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { QuestionService } from '../../../services/question.service';
import { FormBuilder } from '@angular/forms';
import { AuthUserService } from '../../../services/auth-user.service';
import { EvaluationModalComponent } from '../../evaluation/evaluation-modal/evaluation-modal.component';
var EvaluationEmployeeComponent = /** @class */ (function () {
    function EvaluationEmployeeComponent(modalService, _conf, _q_service, _fb, _auth_service) {
        this.modalService = modalService;
        this._conf = _conf;
        this._q_service = _q_service;
        this._fb = _fb;
        this._auth_service = _auth_service;
        this.hideShowAnimator = true;
        this.items = [];
        this.dtTrigger = new Subject();
        this.hidden = "hidden";
        this.selectedEmployee = null;
        this.hahahaha = false;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.data = [];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api = this._conf.ServerWithApiUrl + 'evaluation/';
        this.evaluationForm =
            _fb.group({
                'employee_id': ['null'],
                'position_id': ['null'],
                'question_id': ['null'],
                'rating_value': ['null']
            });
    }
    EvaluationEmployeeComponent.prototype.hideShowAnimation = function () {
        this.hideShowAnimator = !this.hideShowAnimator;
    };
    EvaluationEmployeeComponent.prototype.ngOnInit = function () {
        this.get_UserId();
    };
    EvaluationEmployeeComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    EvaluationEmployeeComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    EvaluationEmployeeComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    EvaluationEmployeeComponent.prototype.get_UserId = function () {
        var _this = this;
        this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
            _this.getEmployeeId();
        }, function (err) { return console.error(err); });
    };
    EvaluationEmployeeComponent.prototype.getEmployeeId = function () {
        var _this = this;
        this._auth_service.getUserById(this.user_id).
            subscribe(function (data) {
            var user = data;
            _this.employee_id = user.employee_id;
            _this.getEmployeeEvaluation();
        }, function (err) { return console.error(err); });
    };
    EvaluationEmployeeComponent.prototype.getEmployeeEvaluation = function () {
        var _this = this;
        this.evaluationForm.value.id = this.employee_id;
        var model = this.evaluationForm.value;
        this._q_service.getEmployeeEvaluation(model).
            subscribe(function (data) {
            var temp_data = data;
            _this.evaluation_form = temp_data;
            // for (let i = 0; i < this.evaluation_form.length; i++) {
            // 	this.position = this.evaluation_form[i].p_id;
            // 	console.log(this.position);
            // }
            // this.name = temp_data[0].emp_name;
            // this.positions = temp_data[0].position;
            // let new_data = [];
            // for (let i = 0; i < temp_data.length; i++) {
            // 	for (let x = 0; x < temp_data[i].segments.length; ++x) {
            // 		for (let y = 0; y < temp_data[i].segments[x].question_collection.length; ++y) {
            // 			let employee_id = temp_data[i].emp_id;
            // 			let emp_name = temp_data[i].emp_name;
            // 			let position_id = temp_data[i].position_id;
            // 			let position = temp_data[i].position;
            // 			let question_id = temp_data[i].segments[x].question_collection[y].q_id;
            // 			let question = temp_data[i].segments[x].question_collection[y].question;
            // 			let segment_name = temp_data[i].segments[x].segment_name;
            // 			let rating_value = 0;
            // 			new_data.push({employee_id,emp_name,position_id,question_id,question,position,segment_name,rating_value});
            // 		}
            // 	}
            // }
        }, function (err) { return console.error(err); });
    };
    EvaluationEmployeeComponent.prototype.onSubmit = function () {
        var _this = this;
        var model = this.evaluation_form;
        this._q_service.createEvaluationForm(model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            var disposable = _this.modalService.addDialog(EvaluationModalComponent, {
                title: 'Evaluation Form',
                create: true
            }).subscribe(function (isConfirmed) {
            });
        }, function (err) { return _this.catchError(err); });
    };
    EvaluationEmployeeComponent.prototype.changeRatingValue = function (i, j, o, value) {
        console.log('i:', i);
        console.log('j:', j);
        console.log('o:', o);
        var obj = this.evaluation_form[0].segments[0];
        obj.question_collection[o].rating_value = value;
        console.log(obj.question_collection[o]);
    };
    __decorate([
        Input(),
        __metadata("design:type", Array)
    ], EvaluationEmployeeComponent.prototype, "items", void 0);
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], EvaluationEmployeeComponent.prototype, "dtElement", void 0);
    EvaluationEmployeeComponent = __decorate([
        Component({
            selector: 'app-evaluation-employee',
            templateUrl: './evaluation-employee.component.html',
            styleUrls: ['./evaluation-employee.component.css'],
            animations: [
                trigger('hideShowAnimator', [
                    state('true', style({ opacity: 1 })),
                    state('false', style({ opacity: 0 })),
                    transition('0 => 1', animate('.5s')),
                    transition('1 => 0', animate('.9s'))
                ])
            ]
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            QuestionService,
            FormBuilder,
            AuthUserService])
    ], EvaluationEmployeeComponent);
    return EvaluationEmployeeComponent;
}());
export { EvaluationEmployeeComponent };
//# sourceMappingURL=evaluation-employee.component.js.map