import { Component, Input, EventEmitter, Output  } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { QuestionService } from '../../../services/question.service';

@Component({

  selector: 'app-sub-evaluation',
  templateUrl: './sub-evaluation.component.html',
  styleUrls: ['./sub-evaluation.component.css']
})
export class SubEvaluationComponent  {

 	 @Input('group')

 	 
 	 // @Output() onChange: EventEmitter<any> = new EventEmitter<any>();


 	public evaluationForm: FormGroup;
 	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public emp_contract_statuses: any;
	public poststore: any;
	public collection_category = [];

	add_category: any;
	add_subcategory: any;
	add_question: any;
	category_current: any;
	question_current: any;
	segment_current: any;
	subcategory_current: any;
	get_category: any;
	get_subcategory: any;
	get_question: any;
	add_segment: any;
	category_field = [];
	subcategory_field = [];
	cat_value = false;
	sub_value = false;
	query = false;
	subcat = false;
	counter = 0;
	array = [];

 	constructor( 
		private _q_service: QuestionService,
		private _fb: FormBuilder,
		
	){
 		
	}

	ngOnInit() {
		this.addSegment();
		this.addCategory();
		this.addQuestion();

	}

	addCategoryField(){
  		let new_form = {	
        	question: ''
        };

        let new_category = {
        	question: ''
        }

        this.collection_category.push(new_category);
		this.category_field.push(new_form);
  	}

	addCategory(){
		this._q_service.addCategory().
		subscribe(
			data => {
				this.add_category = Array.from(data);
			},
			err => console.error(err)
			);
	}

	changedCategory(data, form_index) {
		this.category_current = data;

		if(this.category_current.length > 0){
			let len = this.add_category.length;
			for (let i = 0; i < len; ++i) {
				if (this.add_category[i].id == this.category_current) {
					this.get_category = this.add_category[i].text;	
					this.collection_category[form_index].category = this.add_category[i].text;	
				}		
			}
			
			this.cat_value = true;
		}
			this.addSubCategory();
	}

	addSubCategory(){
		this._q_service.addSubCategory().
		subscribe(
			data => {
				this.add_subcategory = Array.from(data);				
			},
			err => console.error(err)
			);
	}

	changedSubcategory( data: any, x: any ) {
		this.subcategory_current = data.value;
		for (var i = 0; i < this.add_subcategory.length; ++i) {
			if (this.add_subcategory[i] == this.question_current) {

				this.collection_category[x].subcategory = this.add_subcategory[i].text;
			}
		}

	}

	addQuestion(){
		this._q_service.questionList().
		subscribe(
			data => {
				this.add_question = Array.from(data);	
			},
			err => console.error(err)
			);
	}

	changedQuestion(data: any, x: any) {
		this.question_current = data.value;
		
		if(this.question_current != 0){
			let len = this.add_question.length;
			for (let i = 0; i < len; ++i) {
				if (this.add_question[i].id == this.question_current) {
					this.get_question = this.add_question[i].text;

					this.collection_category[x].question = this.add_question[i].text;
				}
			}
		}
	}

	addQuestions(){
	  this.query = true;
	}

  	addSubCat(){
  		let new_form ={	
        	subcategory: '',
        	question: ''
        };

        let new_category = {
        	subcategory_increment: '',
        	question: ''
        }

        this.collection_category.push(new_category);
		this.subcategory_field.push(new_form);


		this.sub_value = true;
  	}

  	onChangeValue(data: any){
  		// this.onChange.emit(data);
  	}

  	addSegment(){
		this._q_service.addSegment().
		subscribe(
			data => {
				this.add_segment = Array.from(data); 
			},
			err => console.error(err)
			);
	}

	changedSegment(data) {
		this.segment_current = data;

	}



}


