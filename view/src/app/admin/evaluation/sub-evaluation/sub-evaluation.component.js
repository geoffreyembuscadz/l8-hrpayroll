var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { QuestionService } from '../../../services/question.service';
var SubEvaluationComponent = /** @class */ (function () {
    function SubEvaluationComponent(_q_service, _fb) {
        this._q_service = _q_service;
        this._fb = _fb;
        this.collection_category = [];
        this.category_field = [];
        this.subcategory_field = [];
        this.cat_value = false;
        this.sub_value = false;
        this.query = false;
        this.subcat = false;
        this.counter = 0;
        this.array = [];
    }
    SubEvaluationComponent.prototype.ngOnInit = function () {
        this.addSegment();
        this.addCategory();
        this.addQuestion();
    };
    SubEvaluationComponent.prototype.addCategoryField = function () {
        var new_form = {
            question: ''
        };
        var new_category = {
            question: ''
        };
        this.collection_category.push(new_category);
        this.category_field.push(new_form);
    };
    SubEvaluationComponent.prototype.addCategory = function () {
        var _this = this;
        this._q_service.addCategory().
            subscribe(function (data) {
            _this.add_category = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    SubEvaluationComponent.prototype.changedCategory = function (data, form_index) {
        this.category_current = data;
        if (this.category_current.length > 0) {
            var len = this.add_category.length;
            for (var i = 0; i < len; ++i) {
                if (this.add_category[i].id == this.category_current) {
                    this.get_category = this.add_category[i].text;
                    this.collection_category[form_index].category = this.add_category[i].text;
                }
            }
            this.cat_value = true;
        }
        this.addSubCategory();
    };
    SubEvaluationComponent.prototype.addSubCategory = function () {
        var _this = this;
        this._q_service.addSubCategory().
            subscribe(function (data) {
            _this.add_subcategory = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    SubEvaluationComponent.prototype.changedSubcategory = function (data, x) {
        this.subcategory_current = data.value;
        for (var i = 0; i < this.add_subcategory.length; ++i) {
            if (this.add_subcategory[i] == this.question_current) {
                this.collection_category[x].subcategory = this.add_subcategory[i].text;
            }
        }
    };
    SubEvaluationComponent.prototype.addQuestion = function () {
        var _this = this;
        this._q_service.questionList().
            subscribe(function (data) {
            _this.add_question = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    SubEvaluationComponent.prototype.changedQuestion = function (data, x) {
        this.question_current = data.value;
        if (this.question_current != 0) {
            var len = this.add_question.length;
            for (var i = 0; i < len; ++i) {
                if (this.add_question[i].id == this.question_current) {
                    this.get_question = this.add_question[i].text;
                    this.collection_category[x].question = this.add_question[i].text;
                }
            }
        }
    };
    SubEvaluationComponent.prototype.addQuestions = function () {
        this.query = true;
    };
    SubEvaluationComponent.prototype.addSubCat = function () {
        var new_form = {
            subcategory: '',
            question: ''
        };
        var new_category = {
            subcategory_increment: '',
            question: ''
        };
        this.collection_category.push(new_category);
        this.subcategory_field.push(new_form);
        this.sub_value = true;
    };
    SubEvaluationComponent.prototype.onChangeValue = function (data) {
        // this.onChange.emit(data);
    };
    SubEvaluationComponent.prototype.addSegment = function () {
        var _this = this;
        this._q_service.addSegment().
            subscribe(function (data) {
            _this.add_segment = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    SubEvaluationComponent.prototype.changedSegment = function (data) {
        this.segment_current = data;
    };
    __decorate([
        Input('group')
        // @Output() onChange: EventEmitter<any> = new EventEmitter<any>();
        ,
        __metadata("design:type", FormGroup)
    ], SubEvaluationComponent.prototype, "evaluationForm", void 0);
    SubEvaluationComponent = __decorate([
        Component({
            selector: 'app-sub-evaluation',
            templateUrl: './sub-evaluation.component.html',
            styleUrls: ['./sub-evaluation.component.css']
        }),
        __metadata("design:paramtypes", [QuestionService,
            FormBuilder])
    ], SubEvaluationComponent);
    return SubEvaluationComponent;
}());
export { SubEvaluationComponent };
//# sourceMappingURL=sub-evaluation.component.js.map