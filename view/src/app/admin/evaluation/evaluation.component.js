var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { QuestionService } from '../../services/question.service';
import { CommonService } from '../../services/common.service';
import { EmployeeService } from '../../services/employee.service';
import { EvaluationModalComponent } from '../evaluation/evaluation-modal/evaluation-modal.component';
import { FormBuilder } from '@angular/forms';
var EvaluationComponent = /** @class */ (function () {
    function EvaluationComponent(modalService, _conf, _common_service, _q_service, _emp_service, _fb) {
        this.modalService = modalService;
        this._conf = _conf;
        this._common_service = _common_service;
        this._q_service = _q_service;
        this._emp_service = _emp_service;
        this._fb = _fb;
        this.ratingOptions = [
            { name: 'Overall Rating', value: '1' },
            { name: 'Per Segment Rating', value: '2' }
        ];
        this.dtTrigger = new Subject();
        this.collection_category = [];
        this.collection_subcategory = [];
        this.dtOptions = {};
        this.add_form = false;
        this.add_cat = false;
        this.cat_value = false;
        this.sub_value = false;
        this.forms = [];
        this.index = 1;
        this.query = false;
        this.subcat = false;
        this.rating_number_desc = [];
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api = this._conf.ServerWithApiUrl + 'evaluation/';
        this.evaluationForm =
            _fb.group({
                'name': [null],
                'position': [null],
                'rating_count': [null],
                'segments': this._fb.array([
                    this.initSegment()
                ])
            });
    }
    EvaluationComponent.prototype.initSegment = function () {
        return this._fb.group({
            segment: [''],
            question: [''],
        });
    };
    EvaluationComponent.prototype.ngOnInit = function () {
        this.getEmploymentStatus();
        this.getRating();
        this.addPosition();
        this.rating_number();
    };
    EvaluationComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    EvaluationComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    EvaluationComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    EvaluationComponent.prototype.onSubmit = function () {
        var _this = this;
        this.evaluationForm.value.position = this.position_current;
        this.evaluationForm.value.rating_count = this.last_element.id;
        var model = this.evaluationForm.value;
        {
            this._q_service.storeEvaluationTemplate(model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                var disposable = _this.modalService.addDialog(EvaluationModalComponent, {
                    title: 'Evaluation Form',
                    create: true
                }).subscribe(function (isConfirmed) {
                    _this.ngOnInit();
                    _this.evaluationForm.reset();
                });
            }, function (err) { return _this.catchError(err); });
        }
    };
    EvaluationComponent.prototype.getEmploymentStatus = function () {
        this.emp_contract_statuses
            =
                this._emp_service.getEmploymentStatus();
    };
    EvaluationComponent.prototype.getRating = function () {
        var _this = this;
        this._q_service.rating().
            subscribe(function (data) {
            _this.rating = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    EvaluationComponent.prototype.addFormRow = function () {
        var control = this.evaluationForm.controls['segments'];
        control.push(this.initSegment());
    };
    EvaluationComponent.prototype.addCat = function () {
        this.add_cat = true;
    };
    EvaluationComponent.prototype.addPosition = function () {
        var _this = this;
        this._q_service.addPosition().
            subscribe(function (data) {
            _this.add_position = Array.from(data);
            _this.position_value = [];
            _this.position_current = _this.position_value;
        }, function (err) { return console.error(err); });
    };
    EvaluationComponent.prototype.changedPosition = function (data) {
        this.position_current = data.value;
    };
    EvaluationComponent.prototype.addQuestions = function () {
        this.query = true;
    };
    EvaluationComponent.prototype.addSubCat = function () {
        this.subcat = true;
    };
    EvaluationComponent.prototype.removeSegments = function (i) {
        var control = this.evaluationForm.controls['segments'];
        control.removeAt(i);
    };
    EvaluationComponent.prototype.addCategory = function () {
        var _this = this;
        this._q_service.addCategory().
            subscribe(function (data) {
            _this.add_category = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    EvaluationComponent.prototype.rating_number = function () {
        var description = [
            "Unsatisfactory",
            "Improvement needed",
            "Meets expectations",
            "Improvement needed",
            " Unsatisfactory"
        ];
        for (var x = 1; x <= 5; x++) {
            var obj_rating = {
                "id": x,
                "text": description[x - 1]
            };
            this.rating_number_desc.push(obj_rating);
        }
        this.last_element = this.rating_number_desc[this.rating_number_desc.length - 1];
    };
    EvaluationComponent.prototype.resetDescription = function (x) {
        // x.text = "";
        var id = this.rating_number_desc.map(function (e) { return function (e) { return e.text; }; }).indexOf(x.text);
        this.rating_number_desc.splice(id, 1);
        this.last_element = this.rating_number_desc[this.rating_number_desc.length - 1];
    };
    EvaluationComponent.prototype.add_rating = function (x) {
        var obj_rating = {
            "id": x + 1,
            "text": ""
        };
        this.rating_number_desc.push(obj_rating);
        this.last_element = this.rating_number_desc[this.rating_number_desc.length - 1];
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], EvaluationComponent.prototype, "dtElement", void 0);
    EvaluationComponent = __decorate([
        Component({
            selector: 'app-evaluation',
            templateUrl: './evaluation.component.html',
            styleUrls: ['./evaluation.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            CommonService,
            QuestionService,
            EmployeeService,
            FormBuilder])
    ], EvaluationComponent);
    return EvaluationComponent;
}());
export { EvaluationComponent };
//# sourceMappingURL=evaluation.component.js.map