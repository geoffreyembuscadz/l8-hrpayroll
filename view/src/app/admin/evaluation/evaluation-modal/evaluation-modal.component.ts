import 'rxjs/add/operator/catch'
import { Component, OnInit, AfterViewInit, NgZone, Inject } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response} from '@angular/http';
import { QuestionService } from '../../../services/question.service';
import { CommonService } from '../../../services/common.service';
import { Configuration } from '../../../app.config';
import { ConfirmModalComponent } from './../../confirm-modal/confirm-modal.component';

@Component({
	selector: 'app-evaluation-modal',
	templateUrl: './evaluation-modal.component.html',
	styleUrls: ['./evaluation-modal.component.css']
})

export class EvaluationModalComponent extends DialogComponent<null, boolean> {

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public poststore: any;
	public id: any;
	
	evaluationForm : FormGroup;
	edit:any;
	create:any;
	data:any;
	cat_id:any;

	constructor(
		dialogService: DialogService, 
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute, 
		private _q_service: QuestionService,  
		private _common_service: CommonService,  
		private _rt: Router,
		private modalService: DialogService
		 ) {
		super(dialogService);

		this.evaluationForm = _fb.group({
			'emp_type': 	   	[null],
			'emp': 	 	[null],
		});  
	}

	ngOnInit() {
		
	}

	changeSession(id){

	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}
}