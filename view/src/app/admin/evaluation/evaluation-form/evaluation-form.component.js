var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, } from '@angular/core';
import { QuestionService } from '../../../services/question.service';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import * as moment from 'moment';
import { EvaluationModalComponent } from '../../evaluation/evaluation-modal/evaluation-modal.component';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
var EvaluationFormComponent = /** @class */ (function () {
    function EvaluationFormComponent(modalService, _q_service, _ar, _rt, _fb, daterangepickerOptions) {
        this.modalService = modalService;
        this._q_service = _q_service;
        this._ar = _ar;
        this._rt = _rt;
        this._fb = _fb;
        this.daterangepickerOptions = daterangepickerOptions;
        this.objectKeys = Object.keys;
        this.isLoading = false;
        this.title = false;
        this.position = true;
        this.all_evaluation = [];
        this.question = false;
        this.save = false;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.evaluationForm = _fb.group({
            'evaluation_id': [null],
            'employee': [null],
            'date': [null],
        });
        this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
            showDropdowns: true,
            opens: "center"
        };
    }
    EvaluationFormComponent.prototype.ngOnInit = function () {
        this.addPosition();
    };
    EvaluationFormComponent.prototype.addPosition = function () {
        var _this = this;
        this._q_service.addPosition().
            subscribe(function (data) {
            _this.add_position = Array.from(data);
            _this.position_value = [];
            _this.position_current = _this.position_value;
        }, function (err) { return console.error(err); });
    };
    EvaluationFormComponent.prototype.changedPosition = function (data) {
        if (data.value != 0) {
            this.position_current = data.value;
            this.getTitle();
            this.position = false;
        }
        else {
            this.title_current = null;
        }
    };
    EvaluationFormComponent.prototype.getTitle = function () {
        var _this = this;
        var position = this.position_current;
        this._q_service.getEvaluationTitle(position).
            subscribe(function (data) {
            _this.get_title = Array.from(data);
            _this.title_value = [];
            _this.title_current = _this.title_value;
        }, function (err) { return console.error(err); });
    };
    EvaluationFormComponent.prototype.changedEvaluationTitle = function (data) {
        this.title_current = data.value;
        this.getEmployeeByPosition();
    };
    EvaluationFormComponent.prototype.getEmployeeByPosition = function () {
        var _this = this;
        var position = this.position_current;
        this._q_service.getEmployeeByPosition(position).
            subscribe(function (data) {
            _this.get_emp_by_pos = Array.from(data);
            _this.emp_pos_value = [];
            _this.options = {
                multiple: true
            };
            _this.emp_pos_current = _this.emp_pos_value;
        }, function (err) { return console.error(err); });
    };
    EvaluationFormComponent.prototype.changedEmployeePosition = function (data) {
        this.emp_pos_current = data.value;
        this.getEvaluationData();
    };
    EvaluationFormComponent.prototype.getEvaluationData = function () {
        var _this = this;
        var model = this.evaluationForm.value;
        this.evaluationForm.value.id = this.title_current;
        this.evaluationForm.value.position = this.position_current;
        this._q_service.getEvaluationData(model).
            subscribe(function (data) {
            _this.evaluation_data = data;
        }, function (err) { return console.error(err); });
    };
    EvaluationFormComponent.prototype.onSubmit = function () {
        var _this = this;
        this.evaluationForm.value.evaluation_id = this.title_current;
        this.evaluationForm.value.employee = this.emp_pos_current;
        this.evaluationForm.value.date = this.date;
        var model = this.evaluationForm.value;
        console.log(model);
        this._q_service.postEvaluationForm(model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            var disposable = _this.modalService.addDialog(EvaluationModalComponent, {
                title: 'Evaluation Form',
                create: true
            }).subscribe(function (isConfirmed) {
            });
        }, function (err) { return _this.catchError(err); });
    };
    EvaluationFormComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    EvaluationFormComponent.prototype.selectedDate = function (value, dateInput) {
        this.save = true;
        dateInput.start = value.start;
        this.date = moment(dateInput.start).format("YYYY-MM-DD");
        this.onSubmit();
    };
    EvaluationFormComponent = __decorate([
        Component({
            selector: 'app-evaluation-form',
            templateUrl: './evaluation-form.component.html',
            styleUrls: ['./evaluation-form.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            QuestionService,
            ActivatedRoute,
            Router,
            FormBuilder,
            DaterangepickerConfig])
    ], EvaluationFormComponent);
    return EvaluationFormComponent;
}());
export { EvaluationFormComponent };
//# sourceMappingURL=evaluation-form.component.js.map