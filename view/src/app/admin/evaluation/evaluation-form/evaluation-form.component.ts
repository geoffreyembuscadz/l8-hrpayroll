import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild, } from '@angular/core';
import { QuestionService } from '../../../services/question.service';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { DataTableDirective } from 'angular-datatables';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Rx';
import {MomentModule} from 'angular2-moment/moment.module';
import { Select2OptionData } from 'ng2-select2';
import * as moment from 'moment';
import { EvaluationModalComponent } from '../../evaluation/evaluation-modal/evaluation-modal.component';
import { DaterangepickerConfig } from 'ng2-daterangepicker';

@Component({
  selector: 'app-evaluation-form',
  templateUrl: './evaluation-form.component.html',
  styleUrls: ['./evaluation-form.component.css']
})


export class EvaluationFormComponent implements OnInit {
	objectKeys = Object.keys;

	public options: Select2Options;
	public position_value: Array<Select2OptionData>;
	public title_value: Array<Select2OptionData>;
	public emp_pos_value: Array<Select2OptionData>;
	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public emp_pos_current: any;
	public position_current: any;
	public title_current: any;
	public poststore: any;
	public id: any;
	isLoading = false;
	title = false;
	position = true;
	evaluationForm: any;
	add_position: any;
	get_title: any;
	get_emp_by_pos: any;
	evaluation_data: any;
	all_evaluation = [];
	question = false;
	object: any;
	date:any;
	save = false;

	private headers: Headers;
	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	public mainInput = {
		start: moment().subtract(12, 'month'),
		end: moment().subtract(11, 'month')
	}

  constructor(
  	private modalService: DialogService,
	private _q_service: QuestionService,
	private _ar: ActivatedRoute,
	private _rt: Router,
	private _fb: FormBuilder,
	private daterangepickerOptions: DaterangepickerConfig,
  	) {
	  	this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");

		this.headers = new Headers();
		let authToken = localStorage.getItem('id_token');
		this.headers.append('Authorization', `Bearer ${authToken}`);
		this.headers.append('Content-Type', 'application/json');
		this.headers.append('Accept', 'application/json');

		this.evaluationForm = _fb.group({
			'evaluation_id':[null],
			'employee':		[null],
			'date':  		[null],
		});

		this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
    		showDropdowns: true,
    		opens: "center"
		}; 
  	 }

  ngOnInit() {
  	this.addPosition();
  	
  }

  	addPosition(){
		this._q_service.addPosition().
		subscribe(
			data => {
				this.add_position = Array.from(data);
				this.position_value = [];
				this.position_current = this.position_value;			
			},
			err => console.error(err)
			);
	}

	changedPosition(data: any) {
		if (data.value != 0) {
			this.position_current = data.value;
			this.getTitle();
			this.position = false;
			
		} else {
			this.title_current = null;
		}
	}

	getTitle(){
		let position = this.position_current;
		this._q_service.getEvaluationTitle(position).
		subscribe(
			data => {
				this.get_title = Array.from(data);
				this.title_value = [];
				this.title_current = this.title_value;			
			},
			err => console.error(err)
			);
	}

	changedEvaluationTitle(data: any) {
		this.title_current = data.value;
		this.getEmployeeByPosition();
		
	}
	getEmployeeByPosition(){
				
		let position = this.position_current;
		this._q_service.getEmployeeByPosition(position).
		subscribe(
			data => {
				this.get_emp_by_pos = Array.from(data);
				this.emp_pos_value = [];
				this.options = {
					multiple: true
				}
				this.emp_pos_current = this.emp_pos_value;	

			},
			err => console.error(err)
			);
	}


	changedEmployeePosition(data: any) {
		this.emp_pos_current = data.value;
		this.getEvaluationData();
		
	}

	getEvaluationData(){
		let model = this.evaluationForm.value;
		this.evaluationForm.value.id = this.title_current;
		this.evaluationForm.value.position = this.position_current;
		this._q_service.getEvaluationData(model).
		subscribe(
			data => {
				this.evaluation_data = data;
			},
			err => console.error(err)
			);
	}

	onSubmit() {
		this.evaluationForm.value.evaluation_id = this.title_current;
		this.evaluationForm.value.employee = this.emp_pos_current;
		this.evaluationForm.value.date =  this.date;

		let model = this.evaluationForm.value;
		console.log(model);
		this._q_service.postEvaluationForm(model)
		.subscribe(
			data => {
				this.poststore = Array.from(data); 
				let disposable = this.modalService.addDialog(EvaluationModalComponent, {
						title:'Evaluation Form',
						create:true
					}).subscribe((isConfirmed)=>{
						
					});
			},
			err => this.catchError(err)
			);
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	private selectedDate(value:any, dateInput:any) {
       	this.save = true;
        dateInput.start = value.start;
       	this.date = moment(dateInput.start).format("YYYY-MM-DD");

       	this.onSubmit();
    }

}

