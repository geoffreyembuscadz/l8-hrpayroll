var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Configuration } from '../../../app.config';
import { OfficialBusinessModalComponent } from '../../officialbusiness/official-business-modal/official-business-modal.component';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { OfficialBusinessService } from '../../../services/official-business.service';
import { CommonService } from '../../../services/common.service';
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
import { AuthUserService } from '../../../services/auth-user.service';
import { FormBuilder } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { ActionCenterModalComponent } from '../../action-center/action-center-modal/action-center-modal.component';
var OfficialBusinessListComponent = /** @class */ (function () {
    function OfficialBusinessListComponent(_conf, modalService, _common_service, _OBservice, daterangepickerOptions, _fb, _auth_service) {
        this._conf = _conf;
        this.modalService = modalService;
        this._common_service = _common_service;
        this._OBservice = _OBservice;
        this.daterangepickerOptions = daterangepickerOptions;
        this._fb = _fb;
        this._auth_service = _auth_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.ob = [];
        this.byStatus = false;
        this.byDate = false;
        this.byCompany = false;
        this.byEmployee = false;
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.btnCreate = false;
        this.btnEdit = false;
        this.btnApproved = false;
        this.btnAction = false;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filterForm = _fb.group({
            'status_id': [null],
            'start_date': [null],
            'end_date': [null],
            'company_id': [null],
            'employee_id': [null]
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    OfficialBusinessListComponent.prototype.ngOnInit = function () {
        this.dateOption();
        this.getEmployee();
        this.getPermissionId();
    };
    OfficialBusinessListComponent.prototype.getPermissionId = function () {
        var _this = this;
        this._auth_service.getPermissionId().
            subscribe(function (data) {
            _this.permissionValidation(data);
        }, function (err) { return console.error(err); });
    };
    OfficialBusinessListComponent.prototype.permissionValidation = function (user) {
        var len = user.length;
        for (var i = 0; i < len; ++i) {
            if (user[i] == "155") {
                this.btnCreate = true;
            }
            else if (user[i] == "156") {
                this.btnEdit = true;
            }
            else if (user[i] == "157") {
                this.btnApproved = true;
            }
        }
        if (this.btnEdit == false && this.btnApproved == false) {
            this.btnAction = true;
        }
        this.data();
    };
    OfficialBusinessListComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    OfficialBusinessListComponent.prototype.data = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._OBservice.getOBList(model).
            subscribe(function (data) {
            _this.ob = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    OfficialBusinessListComponent.prototype.getEmployee = function () {
        var _this = this;
        this._common_service.getEmployee().
            subscribe(function (data) {
            _this.emp = Array.from(data);
            _this.employee = _this.emp;
            _this.employee_value = [];
            _this.options = {
                multiple: true
            };
            _this.employee_current = _this.employee_value;
        }, function (err) { return console.error(err); });
    };
    OfficialBusinessListComponent.prototype.changedEmployee = function (data) {
        this.employee_current = data.value;
        if (this.employee_current == 0) {
            this.filterForm.value.employee_id = null;
        }
        else {
            this.filterForm.value.employee_id = this.employee_current;
        }
        this.data();
    };
    OfficialBusinessListComponent.prototype.getStatus = function () {
        var _this = this;
        this._common_service.getStatusType().
            subscribe(function (data) {
            _this.sta = Array.from(data);
            _this.status = _this.sta;
            _this.status_value = [];
            _this.options = {
                multiple: true
            };
            _this.status_current = _this.status_value;
        }, function (err) { return console.error(err); });
    };
    OfficialBusinessListComponent.prototype.changedStatus = function (data) {
        this.status_current = data.value;
        if (this.status_current == 0) {
            this.filterForm.value.status_id = null;
        }
        else {
            this.filterForm.value.status_id = this.status_current;
        }
        this.data();
    };
    OfficialBusinessListComponent.prototype.getCompany = function () {
        var _this = this;
        this._common_service.getCompany()
            .subscribe(function (data) {
            _this.company = Array.from(data);
            _this.company_value = [];
            _this.options = {
                multiple: true
            };
            _this.company_current = _this.company_value;
            var id = 0;
            var text = 'Select Company';
            _this.company.unshift({ id: id, text: text });
            _this.company_value = _this.value;
        }, function (err) { return console.error(err); });
    };
    OfficialBusinessListComponent.prototype.changedCompany = function (data) {
        this.company_current = data.value;
        if (this.company_current == 0) {
            this.filterForm.value.company_id = null;
        }
        else {
            this.filterForm.value.company_id = this.company_current;
        }
        this.data();
    };
    OfficialBusinessListComponent.prototype.choiceFilter = function (id) {
        if (id == 1) {
            this.getStatus();
            this.getCompany();
            this.getEmployee();
            this.byStatus = true;
            this.byDate = true;
            this.byCompany = true;
            this.byEmployee = true;
        }
        else if (id == 2) {
            if (this.byStatus == false) {
                this.getStatus();
                this.byStatus = true;
            }
            else {
                this.byStatus = false;
                this.filterForm.value.status_id = null;
            }
        }
        else if (id == 3) {
            if (this.byDate == false) {
                this.byDate = true;
            }
            else {
                this.byDate = false;
                this.filterForm.value.start_date = null;
                this.filterForm.value.end_date = null;
            }
        }
        else if (id == 4) {
            if (this.byCompany == false) {
                this.getCompany();
                this.byCompany = true;
            }
            else {
                this.byCompany = false;
                this.filterForm.value.company_id = null;
            }
        }
        else if (id == 5) {
            if (this.byEmployee == false) {
                this.getEmployee();
                this.byEmployee = true;
            }
            else {
                this.byEmployee = false;
                this.filterForm.value.employee_id = null;
            }
        }
        else {
            this.byStatus = false;
            this.byDate = false;
            this.byCompany = false;
            this.byEmployee = false;
            this.filterForm.value.status_id = null;
            this.filterForm.value.company_id = null;
            this.filterForm.value.start_date = null;
            this.filterForm.value.end_date = null;
            this.filterForm.value.employee_id = null;
            this.data();
        }
    };
    OfficialBusinessListComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.data();
    };
    OfficialBusinessListComponent.prototype.approvedOB = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Approved Official Business',
            message: 'Are you sure you want to Approved this Official Business?',
            action: 'Approved',
            id: id,
            url: 'official_business',
            created_by: this.user_id,
            request: true,
            messenger_id: this.employee_id
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    OfficialBusinessListComponent.prototype.rejectedOB = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(RemarksModalComponent, {
            title: 'Reject Official Business',
            id: id,
            url: 'official_business',
            button: 'Reject',
            created_by: this.user_id,
            messenger_id: this.employee_id
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    OfficialBusinessListComponent.prototype.createOfficialBusiness = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(OfficialBusinessModalComponent, {
            title: 'Create Official Business',
            button: 'Add',
            create: true,
            multi: true,
            admin: true,
            emp: this.emp
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    OfficialBusinessListComponent.prototype.viewDetails = function (id, status) {
        var _this = this;
        var buttons = true;
        if (status == 'REJECTED' || status == 'APPROVED' || status == 'CANCELLED') {
            buttons = false;
        }
        var disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title: 'Official Business',
            id: id,
            url: 'official_business',
            date_range: true,
            ob: true,
            buttons: buttons
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    OfficialBusinessListComponent.prototype.editOB = function (id, emp_id) {
        var _this = this;
        var v = [];
        v.push(emp_id);
        var employee = v;
        var disposable = this.modalService.addDialog(OfficialBusinessModalComponent, {
            title: 'Edit Official Business',
            button: 'Update',
            edit: true,
            ob_id: id,
            single: true,
            admin: true,
            emp: this.emp,
            employee_id: employee
        }).subscribe(function (isConfirmed) {
            _this.dateOption();
            _this.data();
        });
    };
    OfficialBusinessListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    OfficialBusinessListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    OfficialBusinessListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], OfficialBusinessListComponent.prototype, "dtElement", void 0);
    OfficialBusinessListComponent = __decorate([
        Component({
            selector: 'app-official-business-list',
            templateUrl: './official-business-list.component.html',
            styleUrls: ['./official-business-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration,
            DialogService,
            CommonService,
            OfficialBusinessService,
            DaterangepickerConfig,
            FormBuilder,
            AuthUserService])
    ], OfficialBusinessListComponent);
    return OfficialBusinessListComponent;
}());
export { OfficialBusinessListComponent };
//# sourceMappingURL=official-business-list.component.js.map