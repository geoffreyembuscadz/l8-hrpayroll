var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, NgZone, EventEmitter } from '@angular/core';
import { Configuration } from '../../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { OfficialBusinessService } from '../../../services/official-business.service';
import { CommonService } from '../../../services/common.service';
import { OfficialBusiness } from '../../../model/official-business';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import * as moment from 'moment';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';
import { NgUploaderOptions } from 'ngx-uploader';
var OfficialBusinessModalComponent = /** @class */ (function (_super) {
    __extends(OfficialBusinessModalComponent, _super);
    function OfficialBusinessModalComponent(_common_service, _OBservice, dialogService, _fb, _ar, daterangepickerOptions, _auth_service, _config, _zone, _rt) {
        var _this = _super.call(this, dialogService) || this;
        _this._common_service = _common_service;
        _this._OBservice = _OBservice;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this._auth_service = _auth_service;
        _this._config = _config;
        _this._zone = _zone;
        _this.ob = new OfficialBusiness();
        _this.mainInput = {
            start: moment(),
            end: moment().add(1, 'days')
        };
        _this.ismeridian = true;
        _this.empStats = false;
        _this.typeStats = false;
        _this.branchStats = false;
        _this.start_time = new Date("2017-08-08 08:00:00");
        _this.end_time = new Date("2017-08-08 17:00:00");
        _this.validate = false;
        _this.dataStorage = [];
        _this.tableVal = false;
        _this.user = false;
        _this.admin = false;
        _this.dupVal = false;
        _this.csvData = [];
        _this.route_upload = 'upload?type=officialbusiness';
        _this.work_sched = [];
        _this.host = _this._config.socketUrl;
        _this.action_url = _this._config.Server;
        _this.attachment = _this.action_url + 'files/image2.jpg';
        // uploader ngzone
        _this.option = new NgUploaderOptions({
            url: _this._config.ServerWithApiUrl + _this.route_upload,
            autoUpload: false,
            calculateSpeed: true,
            allowedExtensions: ['csv']
        });
        _this.inputUploadEvents = new EventEmitter();
        _this.OBForm = _fb.group({
            'start_time': [null],
            'end_time': [null],
            'remarks': [_this.ob.remarks, [Validators.required]],
            'status_id': [null],
            'date': [null],
            'location': [null],
            'attachment': [null]
        });
        _this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY h:mm A' },
            alwaysShowCalendars: false,
            dateLimit: {
                "days": 5
            }
        };
        return _this;
    }
    OfficialBusinessModalComponent.prototype.ngOnInit = function () {
        if (this.user == true) {
            this.empStats = true;
            this.get_employee();
        }
        if (this.edit == true) {
            this.getOB();
            this.getBranch();
            this.validate = true;
            this.get_employee();
            this.getWorkingSchedule();
        }
        else {
            this.get_employee();
            this.getBranch();
        }
        //uncomment this for socket.io
        // this.connectToServer();
        this.select2Option();
        // this.get_type();
    };
    OfficialBusinessModalComponent.prototype.select2Option = function () {
        this.options2 = {
            dropdownAutoWidth: true
        };
    };
    OfficialBusinessModalComponent.prototype.getOB = function () {
        var _this = this;
        this._OBservice.getOB(this.ob_id).subscribe(function (data) {
            _this.ob_data = (data);
            _this.mainInput.start = _this.ob_data[0].start_date;
            _this.mainInput.end = _this.ob_data[0].end_date;
            _this.remarks = _this.ob_data[0].reason;
            _this.employee_id = _this.ob_data[0].emp_id;
            _this.location = _this.ob_data[0].branch_id;
            // this.attachment= this.ob_data[0].attachment;
            var start = _this.ob_data[0].start_date + " " + _this.ob_data[0].start_time;
            var end = _this.ob_data[0].end_date + " " + _this.ob_data[0].end_time;
            _this.start_time = new Date(start);
            _this.end_time = new Date(end);
            var len = _this.ob_data.length;
            var i = 1;
            for (var x = 0; x < len; x++) {
                var day = moment(_this.ob_data[x].date).format("dddd");
                var date = _this.ob_data[x].date;
                var start_time = _this.ob_data[x].start_time;
                var end_time = _this.ob_data[x].end_time;
                var location_1 = _this.ob_data[x].branch_id;
                var request_id = _this.ob_data[x].request_id;
                _this.dataStorage.push({ i: i, day: day, date: date, start_time: start_time, end_time: end_time, location: location_1, request_id: request_id });
                i++;
            }
            _this.tableVal = true;
            // this.type_id = this.ob_data.OB_type_id;
        }, function (err) { return console.error(err); });
    };
    OfficialBusinessModalComponent.prototype.get_employee = function () {
        this.employee = this.emp;
        this.employee_value = [];
        if (this.edit == true) {
            this.employee_value = this.employee_id;
            this.validate = true;
        }
        if (this.logs == true) {
            this.employee_value = this.employee_id;
        }
        this.options = {
            multiple: true
        };
        this.current = this.employee_value;
    };
    OfficialBusinessModalComponent.prototype.changed = function (data) {
        if (this.single == true) {
            var v = [];
            v.push(data.value);
            this.current = v;
        }
        else {
            this.current = data.value;
        }
        var len = this.current.length;
        if (len >= 1) {
            this.empStats = true;
        }
        else {
            this.empStats = false;
        }
        this.validation();
        this.duplicateEntryValidation();
    };
    OfficialBusinessModalComponent.prototype.getBranch = function () {
        var _this = this;
        this._common_service.getBranch()
            .subscribe(function (data) {
            _this.branch = Array.from(data);
            _this.branch_value = [];
            if (_this.edit == true) {
                _this.branch_value = _this.location;
                _this.validate = true;
            }
            if (_this.create == true) {
                var id = 0;
                var text = 'Select Branch';
                _this.branch.unshift({ id: id, text: text });
                _this.branch_value = ['0'];
            }
            _this.branch_current = _this.branch_value;
        }, function (err) { return console.error(err); });
    };
    OfficialBusinessModalComponent.prototype.changedBranch = function (data, i) {
        if (data.value != 0) {
            if (i != -1) {
                this.dataStorage[i].location = data.value;
            }
            else {
                var len_1 = this.dataStorage.length;
                for (var x = 0; x < len_1; ++x) {
                    this.dataStorage[x].location = data.value;
                }
                this.branch_current = data.value;
            }
        }
        var len = this.branch_current.length;
        if (len >= 1 && data.value != 0) {
            this.branchStats = true;
        }
        else {
            this.branchStats = false;
        }
        this.validation();
        this.duplicateEntryValidation();
    };
    OfficialBusinessModalComponent.prototype.validation = function () {
        if (this.empStats == true && this.branchStats == true && this.dupVal == true || this.edit == true) {
            this.validate = true;
        }
        else {
            this.validate = false;
        }
    };
    // get_type(){
    // 	this.type = this._common_service.getOBType().
    // 	subscribe(
    // 		data => {
    // 			this.type = Array.from(data); // fetched record
    // 			this.type_value = [];
    // 			if(this.edit == true) {
    // 				this.type_value = this.type_id;
    // 				this.validate = true;
    //             }
    // 			this.type_current = this.type_value;
    // 			if(this.create == true){
    // 				let id = 0;
    // 		        let text = 'Select Type';
    // 		        this.type.unshift({id,text});
    // 		        this.type_value = this.value;
    // 			}
    // 		},
    // 		err => console.error(err)
    // 	);
    // }
    // changedType(data: any) {
    //     this.type_current = data.value;
    //     let len = this.type_current.length;
    //       if(len >= 1 && this.type_current != 0){
    //       	this.typeStats = true;
    //       }
    //       else{
    //       	this.typeStats = false;
    //       }
    //       if(this.empStats == true || this.edit == true){
    //       	this.validate=true;
    //       }
    // }
    OfficialBusinessModalComponent.prototype.createOB = function () {
        var _this = this;
        this.validate = false;
        this.startUpload();
        this.duplicateEntryValidation();
        var ob = this.OBForm.value;
        var i = ob.details.length - 1;
        ob.start_date = ob.details[0].date;
        ob.end_date = ob.details[i].date;
        if (this.edit == true && this.user == false) {
            this.OBForm.value.id = this.ob_id;
            ob = this.OBForm.value;
            this._OBservice
                .updateOB(ob)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.result = true;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        else if (this.edit == true && this.user == true) {
            this.OBForm.value.id = this.ob_id;
            ob = this.OBForm.value;
            this._OBservice
                .updateMyOB(ob)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.result = true;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        if (this.create == true && this.user == false) {
            this._OBservice
                .createOB(ob)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.result = true;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        else if (this.create == true && this.user == true) {
            this._OBservice
                .createMyOB(ob)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.result = true;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
    };
    OfficialBusinessModalComponent.prototype.connectToServer = function () {
        var _this = this;
        var socketUrl = this.host;
        this.socket = io.connect(socketUrl);
        this.socket.on("connect", function () { return _this.connect(); });
        this.socket.on("disconnect", function () { return _this.disconnect(); });
        this.socket.on("error", function (error) {
            console.log("ERROR: \"" + error + "\" (" + socketUrl + ")");
        });
    };
    // Handle connection opening
    OfficialBusinessModalComponent.prototype.connect = function () {
        // Request initial list when connected
        this.socket.emit("join", 'Client User -> ');
    };
    // Handle connection closing
    OfficialBusinessModalComponent.prototype.disconnect = function () {
        console.log("Disconnected from \"" + this.host + "\"");
    };
    OfficialBusinessModalComponent.prototype.errorCatch = function () {
        var _this = this;
        if (this.poststore.status_code == 200) {
            this.success_title = 'Success';
            this.success_message = 'No Changes Happen';
            setTimeout(function () {
                _this.close();
            }, 1000);
        }
        else if (this.poststore.status_code == 400) {
            this.error_title = 'Warning';
            this.error_message = 'Request is already exist';
            var temp_1;
            setTimeout(function () {
                var disposable = _this.dialogService.addDialog(ValidationModalComponent, {
                    title: 'Warning',
                    message: 'Official Business',
                    url: 'official_business',
                    duplicate: true,
                    data: _this.poststore.data,
                    userDup: _this.user,
                    adminDup: _this.admin,
                    emp: _this.emp
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.error_title = '';
                        _this.error_message = '';
                        setTimeout(function () {
                            _this.close();
                        }, 1000);
                    }
                    else if (message != false) {
                        temp_1 = message;
                        for (var x = 0; x < temp_1.length; ++x) {
                            var index = _this.current.indexOf(temp_1[x]);
                            _this.current.splice(index, 1);
                        }
                        _this.employee_value = _this.current;
                        if (_this.current.length == 0) {
                            _this.error_title = '';
                            _this.error_message = '';
                            setTimeout(function () {
                                _this.close();
                            }, 1000);
                        }
                    }
                });
            }, 1000);
        }
        else {
            // socket.io emit
            // this.socket.emit('notify', this.poststore);
            if (this.logs && this.user) {
                this._rt.navigateByUrl('admin/my-official-business-list');
            }
            if (this.logs && this.admin) {
                this._rt.navigateByUrl('admin/official-business-list');
            }
            this.validate = false;
            this.success_title = "Success";
            this.success_message = "Successfully";
            setTimeout(function () {
                _this.close();
            }, 2000);
        }
    };
    OfficialBusinessModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    OfficialBusinessModalComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_date = moment(dateInput.start).format("YYYY-MM-DD");
        this.end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.tableData();
    };
    OfficialBusinessModalComponent.prototype.toggleMode = function () {
        this.ismeridian = !this.ismeridian;
    };
    OfficialBusinessModalComponent.prototype.tableData = function () {
        this.getWorkingSchedule();
        this.tableVal = true;
        if (this.dataStorage != null) {
            this.dataStorage = [];
        }
        if (this.start_date == null && this.end_date == null) {
            this.start_date = this.mainInput.start;
            this.end_date = this.mainInput.end;
        }
        var start = this.start_date;
        var end = this.end_date;
        var from = moment(this.start_date, 'YYYY-MM-DD');
        var to = moment(this.end_date, 'YYYY-MM-DD');
        var i = 0;
        var start_time = moment(this.start_time).format("HH:mm");
        var end_time = moment(this.end_time).format("HH:mm");
        var request_id = 0;
        var location = this.branch_current;
        /* using diff */
        var currDate = from.clone().startOf('day');
        var lastDate = to.clone().startOf('day');
        var date = moment(currDate.toDate()).format("YYYY-MM-DD");
        var day = moment(date).format("dddd");
        this.dataStorage.push({ i: i, day: day, date: date, start_time: start_time, end_time: end_time, location: location, request_id: request_id });
        while (currDate.add(1, 'days').diff(lastDate) < 0) {
            var date_1 = moment(currDate.clone().toDate()).format("YYYY-MM-DD");
            var day_1 = moment(date_1).format("dddd");
            this.dataStorage.push({ i: i, day: day_1, date: date_1, start_time: start_time, end_time: end_time, location: location, request_id: request_id });
        }
        if (start != end) {
            var date_2 = moment(lastDate.toDate()).format("YYYY-MM-DD");
            var day_2 = moment(date_2).format("dddd");
            this.dataStorage.push({ i: i, day: day_2, date: date_2, start_time: start_time, end_time: end_time, location: location, request_id: request_id });
        }
        var len = this.dataStorage.length;
        var t = 1;
        for (var x = 0; x < len; ++x) {
            this.dataStorage[x].i = t;
            t++;
        }
        this.duplicateEntryValidation();
    };
    OfficialBusinessModalComponent.prototype.removeDay = function (id) {
        this.dataStorage.splice(id, 1);
        if (this.dataStorage.length == 0) {
            this.tableVal = false;
        }
    };
    OfficialBusinessModalComponent.prototype.duplicateEntryValidation = function () {
        var _this = this;
        this.OBForm.value.employee_id = this.current;
        this.OBForm.value.location = this.branch_current;
        // this.OBForm.value.OB_type_id = this.type_current;
        this.OBForm.value.start_date = moment(this.start_date).format("YYYY-MM-DD");
        this.OBForm.value.end_date = moment(this.end_date).format("YYYY-MM-DD");
        this.OBForm.value.details = this.dataStorage;
        if (this.user == true && this.create == true || this.user == true && this.edit == true) {
            var v = [];
            v.push(this.employee_id);
            this.OBForm.value.employee_id = v;
        }
        if (this.logs) {
            this.OBForm.value.employee_id = this.employee_id;
        }
        if (this.start_date == null && this.end_date == null) {
            var start = this.mainInput.start;
            var end = this.mainInput.end;
            this.OBForm.value.start_date = moment(start).format("YYYY-MM-DD");
            this.OBForm.value.end_date = moment(end).format("YYYY-MM-DD");
        }
        if (this.edit == true) {
            this.OBForm.value.edit = this.edit;
            this.OBForm.value.id = this.ob_id;
        }
        else {
            this.OBForm.value.edit = false;
        }
        var ob = this.OBForm.value;
        if (this.OBForm.value.employee_id != 0 && this.OBForm.value.details.length != 0 || this.edit == true) {
            this._OBservice
                .duplicateEntryValidation(ob)
                .subscribe(function (data) {
                _this.poststore = data;
                if (_this.poststore.message == "duplicate") {
                    _this.dupVal = false;
                    _this.errorCatch();
                }
                else {
                    _this.dupVal = true;
                    _this.error_title = '';
                    _this.error_message = '';
                }
                _this.validation();
            }, function (err) { return _this.catchError(err); });
        }
    };
    OfficialBusinessModalComponent.prototype.handleUpload = function (data) {
        var _this = this;
        this._zone.run(function () {
            _this.response = data;
            if (data && data.response) {
                _this.response = JSON.parse(data.response);
                _this.attachment = _this.response.file_url;
                _this.OBForm.value.attachment = _this.response.file_url;
            }
        });
    };
    OfficialBusinessModalComponent.prototype.beforeUpload = function (data) {
    };
    OfficialBusinessModalComponent.prototype.startUpload = function () {
        this.inputUploadEvents.emit('startUpload');
    };
    OfficialBusinessModalComponent.prototype.getWorkingSchedule = function () {
        var _this = this;
        this._common_service.getWorkingScheduleWithoutBreak()
            .subscribe(function (data) {
            _this.work_sched = data;
            _this.ws_value = [];
            _this.ws_current = _this.ws_value;
            var id = 0;
            var text = 'Suggested Schedule';
            _this.work_sched.unshift({ id: id, text: text });
            _this.ws_value = _this.value;
        }, function (err) { return _this.catchError(err); });
    };
    OfficialBusinessModalComponent.prototype.changedWorkingSchedule = function (data, i) {
        for (var y = 0; y < this.work_sched.length; ++y) {
            if (this.work_sched[y].id == data.value) {
                // this.dataStorage[i].break_start = this.work_sched[y].break_start;
                // this.dataStorage[i].break_end = this.work_sched[y].break_end;
                this.dataStorage[i].start_time = this.work_sched[y].time_in;
                this.dataStorage[i].end_time = this.work_sched[y].time_out;
            }
        }
    };
    OfficialBusinessModalComponent.prototype.updateTime = function (i, type, event) {
        var time = moment("2017-11-29 " + event.target.value).format("HH:mm:ss");
        if (type == 'start') {
            this.dataStorage[i].start_time = time;
        }
        else if (type == 'end') {
            this.dataStorage[i].end_time = time;
        }
    };
    OfficialBusinessModalComponent = __decorate([
        Component({
            selector: 'app-official-business-modal',
            templateUrl: './official-business-modal.component.html',
            styleUrls: ['./official-business-modal.component.css']
        }),
        __metadata("design:paramtypes", [CommonService,
            OfficialBusinessService,
            DialogService,
            FormBuilder,
            ActivatedRoute,
            DaterangepickerConfig,
            AuthUserService,
            Configuration,
            NgZone,
            Router])
    ], OfficialBusinessModalComponent);
    return OfficialBusinessModalComponent;
}(DialogComponent));
export { OfficialBusinessModalComponent };
//# sourceMappingURL=official-business-modal.component.js.map