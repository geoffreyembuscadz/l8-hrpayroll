import { Component, OnInit, Injectable, TemplateRef, NgZone, EventEmitter } from '@angular/core';
import { Configuration } from '../../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router } from '@angular/router';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { OfficialBusinessService } from '../../../services/official-business.service';
import { CommonService } from '../../../services/common.service';
import { OfficialBusiness } from '../../../model/official-business';
import { Select2OptionData } from 'ng2-select2';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TimepickerModule } from 'ngx-bootstrap';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';
import { NgUploaderOptions } from 'ngx-uploader';


@Component({
  selector: 'app-official-business-modal',
  templateUrl: './official-business-modal.component.html',
  styleUrls: ['./official-business-modal.component.css']
})
export class OfficialBusinessModalComponent extends DialogComponent<null, boolean>{

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public options: Select2Options;
	public options2: Select2Options;
	public current: any;
	public type : any;
	public type_value : any;
	public start_date:any;
	public end_date:any;
	ob = new OfficialBusiness();
	OBForm:any;
	type_current:any;
	emp:any;
	poststore:any;
	user_id:any;
	public mainInput = {
        start: moment(),
        end: moment().add(1,'days')
    }
	public ismeridian:boolean = true;
	empStats = false;
	typeStats = false;
	branchStats = false;

	public start_time:Date = new Date("2017-08-08 08:00:00");
	public end_time:Date = new Date("2017-08-08 17:00:00");

	validate = false;
    employee_id:any;
    ob_id:any;
    ob_data:any;
    remarks:any;
    type_id:any;
    edit:any;
    create:any;
    multi:any;
    single:any;
    value:any;
    location:any;
    dataStorage=[];
    tableVal = false;
    user=false;
    admin=false;
    logs:any;
    dupVal=false;
    supervisor_id:any;
    role_id:any;
    attachment:any;
    _rt:any;

    public branch: any;
	public branch_current : any;
	branch_value: any;

	csvUrl: string;  // URL to web API
  	csvData: any[] = [];
  	uploadfile:any;
	option: NgUploaderOptions;
	response: any;
	hasBaseDropZoneOver: boolean;
	inputUploadEvents: EventEmitter<string>;
	private route_upload = 'upload?type=officialbusiness';

	private action_url: string;
	messenger_id:any;

	public ws_current : any;
	ws_value: Array<Select2OptionData>;
	public work_sched=[];

	socket: SocketIOClient.Socket;
    host:any;

  constructor(
  	private _common_service: CommonService,
   	private _OBservice: OfficialBusinessService, 
   	dialogService: DialogService, 
   	private _fb: FormBuilder, 
   	private _ar: ActivatedRoute,
   	private daterangepickerOptions: DaterangepickerConfig,
   	private _auth_service: AuthUserService,
   	private _config: Configuration, 
   	private _zone: NgZone,
   	_rt: Router,
  	){ 
  	
  	super(dialogService);

  	this.host = this._config.socketUrl;

  	this.action_url = this._config.Server;

  	this.attachment = this.action_url + 'files/image2.jpg';

  	// uploader ngzone
    this.option = new NgUploaderOptions({
    	url: this._config.ServerWithApiUrl + this.route_upload,
    	autoUpload: false,
    	calculateSpeed: true,
    	allowedExtensions: ['csv']
    });

     this.inputUploadEvents = new EventEmitter<string>();

    this.OBForm = _fb.group({
		'start_time': 		[null],
		'end_time': 		[null],
		'remarks': 			[this.ob.remarks, [Validators.required]],
		'status_id':		[null],
		'date': 			[null],
		'location': 		[null],
		'attachment': 		[null]
    }); 

    this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY h:mm A' },
            alwaysShowCalendars: false,
            dateLimit: {
	        	"days": 5
	    	}
	}; 
  }

	ngOnInit() {

		if(this.user == true){
    		this.empStats = true;
    		this.get_employee();
    	}
		if(this.edit == true){
			this.getOB();
			this.getBranch();
    		this.validate = true;
    		this.get_employee();
    		this.getWorkingSchedule();
    	}
    	else{
	   		this.get_employee();
	   		this.getBranch();
    	}
	   		//uncomment this for socket.io
    		// this.connectToServer();
    	this.select2Option();
		// this.get_type();
		
 	}

 	select2Option(){
 		this.options2 = {
		dropdownAutoWidth: true
		}
 	}

	getOB(){

		this._OBservice.getOB(this.ob_id).subscribe(
	      data => {
	        this.ob_data=(data);

	        this.mainInput.start = this.ob_data[0].start_date;
	        this.mainInput.end = this.ob_data[0].end_date;
	        this.remarks = this.ob_data[0].reason;
	        this.employee_id= this.ob_data[0].emp_id;
	        this.location= this.ob_data[0].branch_id;
	        // this.attachment= this.ob_data[0].attachment;
	        
	        let start = this.ob_data[0].start_date + " " + this.ob_data[0].start_time;
	        let end = this.ob_data[0].end_date + " " + this.ob_data[0].end_time;

	        this.start_time = new Date(start);
	        this.end_time = new Date(end);

	        let len = this.ob_data.length;
	        let i = 1;
	        for (let x = 0; x < len; x++) {

				let day = moment(this.ob_data[x].date).format("dddd");
				let date = this.ob_data[x].date;
				let start_time = this.ob_data[x].start_time;
				let end_time =  this.ob_data[x].end_time;
				let location = this.ob_data[x].branch_id;
				let request_id = this.ob_data[x].request_id;

	        	this.dataStorage.push({i,day,date,start_time,end_time,location,request_id});
				i++;

	        }
	        
	        this.tableVal = true;

	        // this.type_id = this.ob_data.OB_type_id;

	      },
	      err => console.error(err)
    	);
	}

	get_employee(){
		
		this.employee = this.emp;
		this.employee_value = [];
		if(this.edit == true) {
			this.employee_value = this.employee_id;
			this.validate = true;
        } 
        
        if(this.logs == true){
        	this.employee_value = this.employee_id;
        }

		this.options = {
			multiple: true
		}
		this.current = this.employee_value;
	}
	changed(data: any) {
		if(this.single == true){
			let v = [];
			v.push(data.value);
			this.current = v;
		}
		else{
			this.current = data.value;
		}
		let len = this.current.length;
		if(len >= 1){
			this.empStats = true;
		}
		else{
			this.empStats = false;
		}

		this.validation();
		this.duplicateEntryValidation();
	}

	getBranch(){
	      this._common_service.getBranch()
	      .subscribe(
	        data => {
	        this.branch = Array.from(data);
	        
	        this.branch_value = [];

	        if(this.edit == true) {
				this.branch_value = this.location;
				this.validate = true;
        	}

        	if (this.create == true) {
	    		let id = 0;
	        	let text = 'Select Branch';
	        	this.branch.unshift({id,text});

	        	this.branch_value = ['0'];

        	}
        	
	        this.branch_current = this.branch_value;

	        
	        },
	        err => console.error(err)
	    );
	}
	changedBranch(data: any,i) {
		
		if(data.value != 0){
			if(i != -1){
				this.dataStorage[i].location = data.value;
			}
			else{
				let len = this.dataStorage.length;
				for (let x = 0; x < len; ++x) {
					this.dataStorage[x].location = data.value
				}
				this.branch_current = data.value;
			}
		}

		let len = this.branch_current.length;
		if(len >= 1 && data.value != 0){
			this.branchStats = true;
		}
		else{
			this.branchStats = false;
		}

		this.validation();
		this.duplicateEntryValidation();
	} 

	validation(){

		if(this.empStats == true && this.branchStats == true && this.dupVal == true|| this.edit == true){
	      	this.validate=true;
	    }
     	else{
     		this.validate=false;
     	}
	}

	// get_type(){
	// 	this.type = this._common_service.getOBType().
	// 	subscribe(
	// 		data => {
	// 			this.type = Array.from(data); // fetched record
	// 			this.type_value = [];
	// 			if(this.edit == true) {
	// 				this.type_value = this.type_id;
	// 				this.validate = true;
	//             }
	// 			this.type_current = this.type_value;
				
	// 			if(this.create == true){
	// 				let id = 0;
	// 		        let text = 'Select Type';

	// 		        this.type.unshift({id,text});

	// 		        this.type_value = this.value;
					
	// 			}
	// 		},
	// 		err => console.error(err)
	// 	);
	// }

	// changedType(data: any) {
	//     this.type_current = data.value;
	//     let len = this.type_current.length;
	//       if(len >= 1 && this.type_current != 0){
	//       	this.typeStats = true;
	//       }
	//       else{
	//       	this.typeStats = false;
	//       }

	//       if(this.empStats == true || this.edit == true){
	//       	this.validate=true;
	//       }
	// }

	

	createOB(){
		
		this.validate = false;
		this.startUpload();
		this.duplicateEntryValidation();

  		let  ob = this.OBForm.value;
  		let i = ob.details.length -1;
		ob.start_date = ob.details[0].date;
		ob.end_date = ob.details[i].date;
	
		if(this.edit == true && this.user == false){
			this.OBForm.value.id= this.ob_id;
			ob = this.OBForm.value;

			this._OBservice
			.updateOB(ob)
			.subscribe(
				data => {
					this.poststore = data;
					
					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}
		else if(this.edit == true && this.user == true){
			this.OBForm.value.id= this.ob_id;
			ob = this.OBForm.value;

			this._OBservice
			.updateMyOB(ob)
			.subscribe(
				data => {
					this.poststore = data;
					
					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}
		if(this.create == true && this.user == false){

			this._OBservice
			.createOB(ob)
			.subscribe(
				data => {
					this.poststore = data;
					
					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}
		else if(this.create == true && this.user == true){

			this._OBservice
			.createMyOB(ob)
			.subscribe(
				data => {
					this.poststore = data;
					
					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}

	}

	connectToServer(){
	    let socketUrl = this.host;
	    this.socket = io.connect(socketUrl);
	    this.socket.on("connect", () => this.connect());
	    this.socket.on("disconnect", () => this.disconnect());
	    
	    this.socket.on("error", (error: string) => {
	      console.log(`ERROR: "${error}" (${socketUrl})`);
	    });

	   
	  }

	// Handle connection opening
	private connect() {
	    // Request initial list when connected
	    this.socket.emit("join", 'Client User -> ');
	}
	// Handle connection closing
	private disconnect() {
	    console.log(`Disconnected from "${this.host}"`);
	}

	errorCatch(){
		if(this.poststore.status_code == 200){
			this.success_title = 'Success';
			this.success_message = 'No Changes Happen';
			setTimeout(() => {
	  		   this.close();
	      	}, 1000);
		}
		else if(this.poststore.status_code == 400){

			this.error_title = 'Warning';
			this.error_message = 'Request is already exist';

			let temp;
			setTimeout(() => {
				let disposable = this.dialogService.addDialog(ValidationModalComponent, {
				title:'Warning',
				message:'Official Business',
				url:'official_business',
				duplicate:true,
				data:this.poststore.data,
				userDup:this.user,
				adminDup:this.admin,
            	emp:this.emp
				}).subscribe((message)=>{
					if (message == true) { 
						this.error_title = '';
						this.error_message = '';

						setTimeout(() => {
				  		   this.close();
				      	}, 1000);
					}
					else if(message != false){
    					temp = message;

    					for (let x = 0; x < temp.length; ++x) {
							let index = this.current.indexOf(temp[x]);
	    					this.current.splice(index, 1);
    					}

	    				this.employee_value = this.current;

    					if(this.current.length == 0){
	    					this.error_title = '';
							this.error_message = '';
							setTimeout(() => {
					  		   this.close();
					      	}, 1000);
    					}
					}
				});
	      	}, 1000);

		}
		else{
			// socket.io emit
			// this.socket.emit('notify', this.poststore);
			if (this.logs && this.user) {
   				this._rt.navigateByUrl('admin/my-official-business-list');
			}
			if (this.logs && this.admin) {
				this._rt.navigateByUrl('admin/official-business-list');
			}
			this.validate = false;
			this.success_title = "Success";
			this.success_message = "Successfully";
			setTimeout(() => {
	  		   this.close();
	      	}, 2000);
		}
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	private selectedDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_date = moment(dateInput.start).format("YYYY-MM-DD");
       	this.end_date = moment(dateInput.end).format("YYYY-MM-DD");

       	this.tableData();
    }

	public toggleMode():void {
		this.ismeridian = !this.ismeridian;
	}


	tableData(){


	    this.getWorkingSchedule();

		this.tableVal = true;

		if(this.dataStorage != null){
			this.dataStorage = [];
		}

		if(this.start_date == null && this.end_date==null){
			this.start_date = this.mainInput.start;
			this.end_date = this.mainInput.end;
		}


		let start = this.start_date;
		let end = this.end_date;
		let from = moment(this.start_date, 'YYYY-MM-DD'); 
		let to = moment(this.end_date, 'YYYY-MM-DD');
		let i = 0;
		let start_time = moment(this.start_time).format("HH:mm");
		let end_time =  moment(this.end_time).format("HH:mm");
		let request_id = 0;
		let location = this.branch_current;

	  
		/* using diff */
	    let currDate = from.clone().startOf('day');
	    let lastDate = to.clone().startOf('day');

	    let date = moment(currDate.toDate()).format("YYYY-MM-DD");
	    let day = moment(date).format("dddd");

	   	this.dataStorage.push({i,day,date,start_time,end_time,location,request_id});

	    while(currDate.add(1 ,'days').diff(lastDate) < 0) {
	    	let date = moment(currDate.clone().toDate()).format("YYYY-MM-DD");
	    	let day = moment(date).format("dddd");
	        this.dataStorage.push({i,day,date,start_time,end_time,location,request_id});
	    }

	    if(start != end)  {
	    	let date = moment(lastDate.toDate()).format("YYYY-MM-DD");
	    	let day = moment(date).format("dddd");
	    	this.dataStorage.push({i,day,date,start_time,end_time,location,request_id});
	    }

	    let len = this.dataStorage.length;

	    let t = 1;
	    for (let x = 0; x < len; ++x) {
	    	this.dataStorage[x].i = t;
	    	t++;
	    }

	    this.duplicateEntryValidation();
	}

	removeDay(id){
		this.dataStorage.splice(id, 1);

		if(this.dataStorage.length == 0){
			this.tableVal = false;
		}
	}

	duplicateEntryValidation(){
		this.OBForm.value.employee_id = this.current;
		this.OBForm.value.location = this.branch_current;
		// this.OBForm.value.OB_type_id = this.type_current;
		this.OBForm.value.start_date = moment(this.start_date).format("YYYY-MM-DD");
		this.OBForm.value.end_date = moment(this.end_date).format("YYYY-MM-DD");
		this.OBForm.value.details = this.dataStorage;
		
		if(this.user == true  && this.create == true || this.user == true && this.edit == true){
			let v = [];
			v.push(this.employee_id);
			this.OBForm.value.employee_id = v;
		}

		if (this.logs) {
			this.OBForm.value.employee_id = this.employee_id
		}

		if(this.start_date == null && this.end_date == null){
			
			let start = this.mainInput.start;
			let end = this.mainInput.end;
			this.OBForm.value.start_date = moment(start).format("YYYY-MM-DD");
			this.OBForm.value.end_date = moment(end).format("YYYY-MM-DD");
		}

		if(this.edit == true){
			this.OBForm.value.edit = this.edit;
			this.OBForm.value.id= this.ob_id;
		}
		else{
			this.OBForm.value.edit = false;
		}

		let ob = this.OBForm.value;

		if(this.OBForm.value.employee_id != 0 && this.OBForm.value.details.length != 0 || this.edit == true){
			
			this._OBservice
			.duplicateEntryValidation(ob)
			.subscribe(
				data => {
					this.poststore = data;
					if(this.poststore.message == "duplicate"){
							this.dupVal = false;
							this.errorCatch();
					}
					else{
						this.dupVal = true;
						this.error_title = '';
						this.error_message = '';
					}

					this.validation();
				},
				err => this.catchError(err)
			);
		}
	}

	handleUpload(data: any) {
		this._zone.run(() => {
	    	this.response = data;
	    	if (data && data.response) {
	    		this.response = JSON.parse(data.response);
	    		this.attachment = this.response.file_url;
	    		this.OBForm.value.attachment = this.response.file_url;

	    	}
	    });
	}

	beforeUpload(data: any){
	}

	startUpload() {
    	this.inputUploadEvents.emit('startUpload');
  	}

  	getWorkingSchedule(){
	      this._common_service.getWorkingScheduleWithoutBreak()
	      .subscribe(
	        data => {
	        this.work_sched = data;
			
	        this.ws_value = [];
	        this.ws_current = this.ws_value;

	        let id = 0;
	        let text = 'Suggested Schedule';

	        this.work_sched.unshift({id,text});

	        this.ws_value = this.value;
	        
	        },
	        err => this.catchError(err)

	    );
	}
	private changedWorkingSchedule(data: any,i:any) {

  		for (let y = 0; y < this.work_sched.length; ++y) {
  			if (this.work_sched[y].id == data.value) {
				// this.dataStorage[i].break_start = this.work_sched[y].break_start;
				// this.dataStorage[i].break_end = this.work_sched[y].break_end;
				this.dataStorage[i].start_time = this.work_sched[y].time_in;
				this.dataStorage[i].end_time = this.work_sched[y].time_out;
  			}
  		}
    }

    updateTime(i,type,event) {

    	let time =  moment("2017-11-29 " + event.target.value).format("HH:mm:ss");

    	if (type == 'start') {
    		this.dataStorage[i].start_time = time;
    	}
    	else if (type == 'end') {
			this.dataStorage[i].end_time = time;
    	}
    }


}
