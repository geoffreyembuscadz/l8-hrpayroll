var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Configuration } from '../../../app.config';
import { OfficialBusinessModalComponent } from '../../officialbusiness/official-business-modal/official-business-modal.component';
import { OfficialBusinessService } from '../../../services/official-business.service';
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
import { AuthUserService } from '../../../services/auth-user.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { ActionCenterModalComponent } from '../../action-center/action-center-modal/action-center-modal.component';
import { FormBuilder } from '@angular/forms';
import { CommonService } from '../../../services/common.service';
import * as moment from 'moment';
var MyOfficialBusinessListComponent = /** @class */ (function () {
    function MyOfficialBusinessListComponent(_conf, modalService, _auth_service, _OBservice, _fb, _common_service, daterangepickerOptions) {
        this._conf = _conf;
        this.modalService = modalService;
        this._auth_service = _auth_service;
        this._OBservice = _OBservice;
        this._fb = _fb;
        this._common_service = _common_service;
        this.daterangepickerOptions = daterangepickerOptions;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.ob = [];
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.buttonVal = false;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filterForm = _fb.group({
            'status_id': [null],
            'start_date': [null],
            'end_date': [null]
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    MyOfficialBusinessListComponent.prototype.ngOnInit = function () {
        this.dateOption();
        this.getListByIdLimited();
        this.getStatus();
        this.getData();
    };
    MyOfficialBusinessListComponent.prototype.getListByIdLimited = function () {
        var _this = this;
        this._auth_service.getListByIdLimited().
            subscribe(function (data) {
            var user = data;
            _this.employee_id = user.employee_id;
            _this.buttonVal = true;
        }, function (err) { return console.error(err); });
    };
    MyOfficialBusinessListComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    MyOfficialBusinessListComponent.prototype.getData = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._OBservice.getMyOB(model).
            subscribe(function (data) {
            _this.ob = data;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    MyOfficialBusinessListComponent.prototype.createOfficialBusiness = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(OfficialBusinessModalComponent, {
            title: 'Create Official Business',
            button: 'Add',
            create: true,
            employee_id: this.employee_id,
            single: false,
            user: true
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyOfficialBusinessListComponent.prototype.editOB = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(OfficialBusinessModalComponent, {
            title: 'Edit Official Business',
            button: 'Update',
            edit: true,
            ob_id: id,
            user: true,
            employee_id: this.employee_id
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyOfficialBusinessListComponent.prototype.cancelOB = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(RemarksModalComponent, {
            title: 'Cancel Official Business',
            id: id,
            url: 'official_business',
            button: 'Ok'
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyOfficialBusinessListComponent.prototype.viewDetails = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title: 'Official Official Business',
            id: id,
            url: 'official_business',
            date_range: true,
            ob: true,
            user: true
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyOfficialBusinessListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    MyOfficialBusinessListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    MyOfficialBusinessListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    MyOfficialBusinessListComponent.prototype.getStatus = function () {
        var _this = this;
        this._common_service.getStatusType().
            subscribe(function (data) {
            _this.sta = Array.from(data);
            _this.status = _this.sta;
            _this.status_value = [];
            _this.options = {
                multiple: true
            };
            _this.status_current = _this.status_value;
        }, function (err) { return console.error(err); });
    };
    MyOfficialBusinessListComponent.prototype.changedStatus = function (data) {
        this.status_current = data.value;
        if (this.status_current == 0) {
            this.filterForm.value.status_id = null;
        }
        else {
            this.filterForm.value.status_id = this.status_current;
        }
        this.getData();
    };
    MyOfficialBusinessListComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.getData();
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], MyOfficialBusinessListComponent.prototype, "dtElement", void 0);
    MyOfficialBusinessListComponent = __decorate([
        Component({
            selector: 'app-my-official-business-list',
            templateUrl: './my-official-business-list.component.html',
            styleUrls: ['./my-official-business-list.component.css']
        }),
        __metadata("design:paramtypes", [Configuration,
            DialogService,
            AuthUserService,
            OfficialBusinessService,
            FormBuilder,
            CommonService,
            DaterangepickerConfig])
    ], MyOfficialBusinessListComponent);
    return MyOfficialBusinessListComponent;
}());
export { MyOfficialBusinessListComponent };
//# sourceMappingURL=my-official-business-list.component.js.map