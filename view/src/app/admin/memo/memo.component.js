var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { MemoService } from '../../services/memo.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { MemoModalComponent } from '../memo/memo-modal/memo-modal.component';
import { AuthUserService } from '../../services/auth-user.service';
import { CommonService } from '../../services/common.service';
import * as moment from 'moment';
var MemoComponent = /** @class */ (function () {
    function MemoComponent(dialogService, modalService, _ar, _memo_service, _rt, _fb, _common_service, _auth_service) {
        this.modalService = modalService;
        this._ar = _ar;
        this._memo_service = _memo_service;
        this._rt = _rt;
        this._fb = _fb;
        this._common_service = _common_service;
        this._auth_service = _auth_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.byDate = false;
        this.byCompany = false;
        this.byEmployee = false;
        this.emp_filter = false;
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filterForm = _fb.group({
            'emp_id': [''],
            'start_date': [null],
            'end_date': [null],
            'company_id': [''],
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    MemoComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    MemoComponent.prototype.ngOnInit = function () {
        this.getMemo();
        this.filter();
        this.get_UserId();
        this.getCompany();
        var self = this;
        this.dtOptions = {
            dom: 'lfrtip',
            columnDefs: [
                { "targets": [1], "visible": this.byEmployee },
                { "targets": [3], "visible": this.byCompany }
            ]
        };
    };
    MemoComponent.prototype.get_UserId = function () {
        var _this = this;
        this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
            _this.getEmployeeId();
        }, function (err) { return console.error(err); });
    };
    MemoComponent.prototype.choiceFilter = function (id) {
        if (id == 1) {
            this.byDate = true;
            this.byCompany = true;
            this.byEmployee = true;
        }
        else if (id == 2) {
            this.byDate = true;
        }
        else if (id == 3) {
            this.byCompany = true;
            this.filterForm.value.type = 2;
            this.filter();
            this.ngOnInit();
        }
        else if (id == 4) {
            this.byEmployee = true;
            this.filterForm.value.type = 1;
            this.filter();
            this.ngOnInit();
        }
        else {
            this.byDate = false;
            this.byCompany = false;
            this.byEmployee = false;
            this.emp_filter = false;
            this.ngOnInit();
            this.filterForm.value.emp_filter = null;
            this.filterForm.value.start_date = null;
            this.filterForm.value.end_date = null;
            this.filterForm.value.byCompany = null;
            this.filterForm.value.byEmployee = null;
            // this.filter();
        }
    };
    MemoComponent.prototype.getCompany = function () {
        var _this = this;
        this._common_service.getCompany()
            .subscribe(function (data) {
            _this.company = Array.from(data);
            _this.company_value = [];
            _this.company_current = _this.company_value;
        }, function (err) { return console.error(err); });
    };
    MemoComponent.prototype.changedCompany = function (data) {
        this.company_current = data.value;
        if (this.company_current == 2) {
            this.filter();
        }
        else {
            this.filterForm.value.company = this.company_current;
            this.filter();
        }
    };
    MemoComponent.prototype.getEmployee = function () {
        var _this = this;
        var id = this.employee_id;
        this._common_service.getEmployees(id).
            subscribe(function (data) {
            _this.emp = Array.from(data);
            _this.employees = _this.emp;
            _this.employee_value = [];
            _this.current = _this.employee_value;
        }, function (err) { return console.error(err); });
    };
    MemoComponent.prototype.changedEmployee = function (data) {
        this.current = data.value;
        if (this.current == 1) {
            this.filter();
        }
        else {
            this.filterForm.value.emp_id = this.current;
            this.filter();
        }
    };
    MemoComponent.prototype.getEmployeeId = function () {
        var _this = this;
        this._auth_service.getUserById(this.user_id).
            subscribe(function (data) {
            var user = data; // fetched 
            _this.employee_id = user.employee_id;
            _this.getEmployee();
        }, function (err) { return console.error(err); });
    };
    MemoComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    MemoComponent.prototype.getMemo = function () {
        var _this = this;
        this._memo_service.showMemo().subscribe(function (data) {
            _this.memo_data = Array.from(data);
            _this.rerender();
        }, function (err) { });
    };
    MemoComponent.prototype.filter = function () {
        var _this = this;
        this.filterForm.value.emp_id = this.current;
        this.filterForm.value.company_id = this.company_current;
        var model = this.filterForm.value;
        this._memo_service.showFilterMemo(model).
            subscribe(function (data) {
            _this.memo = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    MemoComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.filter();
    };
    MemoComponent.prototype.addModal = function (id) {
        var disposable = this.modalService.addDialog(MemoModalComponent, {
            title: 'Add Memo',
            button: 'Add',
            create: true,
            employee_id: this.employee_id
        }).subscribe(function (isConfirmed) {
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], MemoComponent.prototype, "dtElement", void 0);
    MemoComponent = __decorate([
        Component({
            selector: 'app-memo',
            templateUrl: './memo.component.html',
            styleUrls: ['./memo.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            DialogService,
            ActivatedRoute,
            MemoService,
            Router,
            FormBuilder,
            CommonService,
            AuthUserService])
    ], MemoComponent);
    return MemoComponent;
}());
export { MemoComponent };
//# sourceMappingURL=memo.component.js.map