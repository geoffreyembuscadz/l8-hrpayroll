import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild  } from '@angular/core';
import { MemoService } from '../../services/memo.service';
import { Http, Response, Jsonp, Headers, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { MemoModalComponent } from '../memo/memo-modal/memo-modal.component';
import { AuthUserService } from '../../services/auth-user.service';
import { CommonService } from '../../services/common.service';
import { Select2OptionData } from 'ng2-select2';
import * as moment from 'moment';

@Component({

  selector: 'app-memo',
  templateUrl: './memo.component.html',
  styleUrls: ['./memo.component.css']
  
})
export class MemoComponent implements OnInit, AfterViewInit {

@ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();

  	public id: string;
	public error_title: string;
	public error_message: string;
	public success_title;
	public success_message;
	public poststore: any;
	public confirm_archiving: any;
	public form
	dtOptions: any = {};
	memo: any;
	filterForm: any;
    perm_rec: any;
    byDate = false;
    byCompany = false;
    byEmployee = false;
    show_memo: any;
    emp_filter = false;
    employee_id:any;
    user_id:any;
    company: any;
    empMemo: any;
    public options: Select2Options;
    public company_current : any;
	company_value: Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public employees : Array<Select2OptionData>;
	emp: any;
	value:any;
	memo_data: any;
	public current: any;

    public mainInput = {
		start: moment().subtract(12, 'month'),
		end: moment().subtract(11, 'month')
	}
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	constructor(dialogService: DialogService, 
  				private modalService: DialogService, 
  				private _ar: ActivatedRoute, 
  				private _memo_service: MemoService, 
  				private _rt: Router, 
  				private _fb: FormBuilder,
  				private _common_service: CommonService,
  				private _auth_service: AuthUserService){

  		this.filterForm = _fb.group({
			'emp_id': 		[''],
			'start_date':	[null],
			'end_date':		[null],
			'company_id':	[''],
		});

		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");
	   
	}
	ngAfterViewInit(){
		this.dtTrigger.next();
	}
	ngOnInit() {
		this.getMemo();
		this.filter();
		this.get_UserId();
		this.getCompany();

		var self = this;
		this.dtOptions = {
			dom: 'lfrtip',
			columnDefs: [         
            { "targets": [ 1 ],"visible": this.byEmployee },
            { "targets": [ 3 ],"visible": this.byCompany }
        	]       
		};
	}

	get_UserId(){
		this._auth_service.getUser().
		subscribe(
			data => {
				let user = data;
				this.user_id = user.id;
				this.getEmployeeId();
			},
			err => console.error(err)
		);
	}

	choiceFilter(id){

		if(id==1){
			this.byDate = true;
			this.byCompany = true;
			this.byEmployee = true;
		} else if(id==2) {
			this.byDate=true;

		} else if(id==3) {
			this.byCompany = true;
			this.filterForm.value.type = 2;
			this.filter();
			this.ngOnInit();		
		} else if(id==4) {
			this.byEmployee = true;
			this.filterForm.value.type = 1;
			this.filter();	
			this.ngOnInit();
		} else {
			this.byDate = false;
			this.byCompany = false;
			this.byEmployee = false;
			this.emp_filter = false;
			this.ngOnInit();

			this.filterForm.value.emp_filter = null;
			this.filterForm.value.start_date = null;
			this.filterForm.value.end_date = null;
			this.filterForm.value.byCompany = null;
			this.filterForm.value.byEmployee = null;
			// this.filter();
		}
	}
	getCompany(){
		this._common_service.getCompany()
		.subscribe(
			data => {
				this.company = Array.from(data);
				this.company_value = [];
				
				this.company_current = this.company_value;
			},
			err => console.error(err)
			);
	}

	changedCompany(data: any) {
		this.company_current = data.value;

		if(this.company_current == 2){
			this.filter();
		}else{
			this.filterForm.value.company = this.company_current;
			this.filter();
		}
	} 
	getEmployee(){
		let id = this.employee_id;

		this._common_service.getEmployees(id).
		subscribe(
			data => {
				this.emp = Array.from(data);
				this.employees = this.emp;
				this.employee_value = [];	
				this.current = this.employee_value;
			},
			err => console.error(err)
			);
	}

	changedEmployee(data: any) {
		this.current = data.value;

		if(this.current == 1){
			this.filter();
		}else{
			this.filterForm.value.emp_id = this.current;
			this.filter();
		}		
	}

	getEmployeeId(){
      this._auth_service.getUserById(this.user_id).
      subscribe(
        data => {
          let user = data; // fetched 
          this.employee_id = user.employee_id;
          this.getEmployee();
        },
        err => console.error(err)
        );
    }
	
	rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    getMemo(){
    	this._memo_service.showMemo().subscribe(
    		data => {
    			this.memo_data = Array.from(data); 
    			this.rerender();
    		},
    		err =>{}
    		);
    }

    filter(){
    	this.filterForm.value.emp_id = this.current;
    	this.filterForm.value.company_id = this.company_current;
		let model = this.filterForm.value;
		this._memo_service.showFilterMemo(model).
		subscribe(
			data => {
				this.memo = Array.from(data);
				this.rerender();
			},
			err => console.error(err)
			);
	}
	
	filterDate(value:any, dateInput:any) {
		dateInput.start = value.start;
		dateInput.end = value.end;

		let start_date = moment(dateInput.start).format("YYYY-MM-DD");
		let end_date = moment(dateInput.end).format("YYYY-MM-DD");
		
		this.filterForm.value.start_date = start_date;
		this.filterForm.value.end_date = end_date;

		this.filter();
	}

	addModal(id: any) {
      let disposable = this.modalService.addDialog(MemoModalComponent, {
        title:'Add Memo',
        button:'Add',
        create:true,
        employee_id:this.employee_id
      }).subscribe((isConfirmed)=>{
      });
    }

}