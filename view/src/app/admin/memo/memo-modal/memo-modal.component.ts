import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MemoService } from '../../../services/memo.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import {MomentModule} from 'angular2-moment/moment.module';
import { AuthUserService } from '../../../services/auth-user.service';
import { CommonService } from '../../../services/common.service';
import * as moment from 'moment';
import { Select2OptionData } from 'ng2-select2';
import {Router} from '@angular/router';

@Component({
	selector: 'app-memo-modal',
	templateUrl: './memo-modal.component.html',
	styleUrls: ['./memo-modal.component.css']
})

export class MemoModalComponent extends DialogComponent<null, boolean> {

	public location = '' ;
	public id: string;
	public error_title: string;
	public error_message: string;
	public success_title;
	public success_message;
	public poststore: any;
	public confirm_archiving: any;
	public form
	memoForm: FormGroup;
	user_id: any;
	employee_id: any;
	date:any;
	name:any;
	type:any;
	emp_id:any;
	comp_id: any;
	create: any;
	memo_type_id: any;
	memo_type:any;
	public memo_type_current: any;
	public memo_type_value: Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	company_value: Array<Select2OptionData>;
	public company_current : any;
	memoType: any;
	emp: any;
	employee: any;
	public current: any;
	emp_memo = false;
	company: any;
	comp_memo = false;
	com_all = [];
	public options: Select2Options;
	public mainInput = {
		start: moment().subtract(12, 'month'),
		end: moment().subtract(6, 'month')
	}
	supervisor_id:any;
	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];
	constructor(   
		dialogService: DialogService, 
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute,
		private _memo_service: MemoService,
		private modalService: DialogService,
		private _common_service: CommonService,
		private daterangepickerOptions: DaterangepickerConfig,
		private _auth_service: AuthUserService,
		private  _router : Router
		) { 
		super(dialogService); 
		this.memoForm = _fb.group({
			'name':      		[null, [Validators.required]],
			'date':	   			[null, [Validators.required]],
			'memo_type_id':  	[null, [Validators.required]],
			'emp_id':  			[''],
			'comp_id':  		[''],
		}); 
		this.daterangepickerOptions.settings = {
			locale: { format: 'MM/DD/YYYY' },
			alwaysShowCalendars: false,
			singleDatePicker: true,
			showDropdowns: true,
			opens: "center"
		}; 

		this.location = _router.url;
	}
	ngOnInit() {
		this.getMemoType();
		this.get_UserId();
		this.getCompany();
	}
	cancel(){
		this.close();
	}
	public catchError(error: any) {
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}
	get_UserId(){
		this._auth_service.getUser().
		subscribe(
			data => {
				let user = data;
				this.user_id = user.id;
				this.getEmployeeId();
			},
			err => console.error(err)
		);
	}

	getEmployeeId(){
		this._auth_service.getUserById(this.user_id).
		subscribe(
			data => {
				let user = data;
				this.supervisor_id = user.employee_id;
				this.getEmpMemo();
			},
			err => console.error(err)
		);
	}

	public onSubmit() {														
		this.memoForm.value.name = this.name;
		this.memoForm.value.date = moment(this.date).format("YYYY-MM-DD HH:mm:ss");
		this.memoForm.value.created_by = this.user_id;
		if(this.emp_memo == true){
			this.memoForm.value.emp_id = this.current;
		}
		if(this.comp_memo == true){
			this.memoForm.value.comp_id = this.company_current;
		}

		this.memoForm.value.memo_type_id = this.memo_type_current;
		let memo = this.memoForm.value;
		console.log('mem', memo);
		if(this.create == true){
			this._memo_service.storeMemo(memo)
			.subscribe(
				data => {
					this.poststore = Array.from(data);
					this.success_title = "Success!";
					this.success_message = "New memo is added!";
					setTimeout(() => {
						this.close();
					}, 1000);
				},
				err => this.catchError(err)
				); 
		}
	}
	getMemoType(){
		this._common_service.getMemoType().
		subscribe(
			data => {
				this.memo_type = data;
				this.memo_type_value = [];
				let id = 0;
				let text = 'Specify Type of Memo';
				this.memo_type.unshift({id,text});
				this.memo_type_current = this.memo_type_value;
			},
			err => console.error(err)
			);
	}
	changedMemoType(data: any) {
		this.memo_type_current = data.value;
		if (this.memo_type_current == 2){
			this.emp_memo = true;
			this.getEmpMemo();	  
		}else if (this.memo_type_current == 1) {
			this.comp_memo = true;
			
		}
	}
	getCompany(){
		this._common_service.getCompany()
		.subscribe(
			data => {
				this.company = Array.from(data);
				this.company_value = [];
				this.options = {
					multiple: true
				}
				let companies = this.company.length;
				for(var i=0; i < companies; i++){
					this.com_all.push(this.company[i].id);
				}		
				let id = 0;
				let text = 'All Company';
				this.company.unshift({id,text});
				this.company_current = this.company_value;
			},
			err => console.error(err)
			);
	}
	changedCompany(data: any) {

		this.company_current = data.value;	
		if(this.company_current == 0){
			this.company_current = this.com_all;
		}else{
			let v = [];
			v.push(parseInt(this.company_current));
			this.company_current = v;
		}
	} 
	getEmpMemo(){
		let id = this.supervisor_id;
		this._common_service.getEmployees(id).
		subscribe(
			data => {
				this.emp = Array.from(data); 
				this.employee_value = [];
				this.current = this.employee_value;
			},
			err => console.error(err)
			);
	}
	changed(data: any) {
		this.current = data.value;
	}	
	private selectedDate(value:any, dateInput:any) {
		dateInput.start = value.start;
		this.date = moment(dateInput.start).format("YYYY-MM-DD");
	}
}


