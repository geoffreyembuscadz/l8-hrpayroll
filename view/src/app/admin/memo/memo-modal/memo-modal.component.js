var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MemoService } from '../../../services/memo.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import { CommonService } from '../../../services/common.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
var MemoModalComponent = /** @class */ (function (_super) {
    __extends(MemoModalComponent, _super);
    function MemoModalComponent(dialogService, _fb, _ar, _memo_service, modalService, _common_service, daterangepickerOptions, _auth_service, _router) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._memo_service = _memo_service;
        _this.modalService = modalService;
        _this._common_service = _common_service;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this._auth_service = _auth_service;
        _this._router = _router;
        _this.location = '';
        _this.emp_memo = false;
        _this.comp_memo = false;
        _this.com_all = [];
        _this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(6, 'month')
        };
        _this.bodyClasses = "skin-blue sidebar-mini";
        _this.body = document.getElementsByTagName('body')[0];
        _this.memoForm = _fb.group({
            'name': [null, [Validators.required]],
            'date': [null, [Validators.required]],
            'memo_type_id': [null, [Validators.required]],
            'emp_id': [''],
            'comp_id': [''],
        });
        _this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
            showDropdowns: true,
            opens: "center"
        };
        _this.location = _router.url;
        return _this;
    }
    MemoModalComponent.prototype.ngOnInit = function () {
        this.getMemoType();
        this.get_UserId();
        this.getCompany();
    };
    MemoModalComponent.prototype.cancel = function () {
        this.close();
    };
    MemoModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    MemoModalComponent.prototype.get_UserId = function () {
        var _this = this;
        this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
            _this.getEmployeeId();
        }, function (err) { return console.error(err); });
    };
    MemoModalComponent.prototype.getEmployeeId = function () {
        var _this = this;
        this._auth_service.getUserById(this.user_id).
            subscribe(function (data) {
            var user = data;
            _this.supervisor_id = user.employee_id;
            _this.getEmpMemo();
        }, function (err) { return console.error(err); });
    };
    MemoModalComponent.prototype.onSubmit = function () {
        var _this = this;
        this.memoForm.value.name = this.name;
        this.memoForm.value.date = moment(this.date).format("YYYY-MM-DD HH:mm:ss");
        this.memoForm.value.created_by = this.user_id;
        if (this.emp_memo == true) {
            this.memoForm.value.emp_id = this.current;
        }
        if (this.comp_memo == true) {
            this.memoForm.value.comp_id = this.company_current;
        }
        this.memoForm.value.memo_type_id = this.memo_type_current;
        var memo = this.memoForm.value;
        console.log('mem', memo);
        if (this.create == true) {
            this._memo_service.storeMemo(memo)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success!";
                _this.success_message = "New memo is added!";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    MemoModalComponent.prototype.getMemoType = function () {
        var _this = this;
        this._common_service.getMemoType().
            subscribe(function (data) {
            _this.memo_type = data;
            _this.memo_type_value = [];
            var id = 0;
            var text = 'Specify Type of Memo';
            _this.memo_type.unshift({ id: id, text: text });
            _this.memo_type_current = _this.memo_type_value;
        }, function (err) { return console.error(err); });
    };
    MemoModalComponent.prototype.changedMemoType = function (data) {
        this.memo_type_current = data.value;
        if (this.memo_type_current == 2) {
            this.emp_memo = true;
            this.getEmpMemo();
        }
        else if (this.memo_type_current == 1) {
            this.comp_memo = true;
        }
    };
    MemoModalComponent.prototype.getCompany = function () {
        var _this = this;
        this._common_service.getCompany()
            .subscribe(function (data) {
            _this.company = Array.from(data);
            _this.company_value = [];
            _this.options = {
                multiple: true
            };
            var companies = _this.company.length;
            for (var i = 0; i < companies; i++) {
                _this.com_all.push(_this.company[i].id);
            }
            var id = 0;
            var text = 'All Company';
            _this.company.unshift({ id: id, text: text });
            _this.company_current = _this.company_value;
        }, function (err) { return console.error(err); });
    };
    MemoModalComponent.prototype.changedCompany = function (data) {
        this.company_current = data.value;
        if (this.company_current == 0) {
            this.company_current = this.com_all;
        }
        else {
            var v = [];
            v.push(parseInt(this.company_current));
            this.company_current = v;
        }
    };
    MemoModalComponent.prototype.getEmpMemo = function () {
        var _this = this;
        var id = this.supervisor_id;
        this._common_service.getEmployees(id).
            subscribe(function (data) {
            _this.emp = Array.from(data);
            _this.employee_value = [];
            _this.current = _this.employee_value;
        }, function (err) { return console.error(err); });
    };
    MemoModalComponent.prototype.changed = function (data) {
        this.current = data.value;
    };
    MemoModalComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        this.date = moment(dateInput.start).format("YYYY-MM-DD");
    };
    MemoModalComponent = __decorate([
        Component({
            selector: 'app-memo-modal',
            templateUrl: './memo-modal.component.html',
            styleUrls: ['./memo-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            MemoService,
            DialogService,
            CommonService,
            DaterangepickerConfig,
            AuthUserService,
            Router])
    ], MemoModalComponent);
    return MemoModalComponent;
}(DialogComponent));
export { MemoModalComponent };
//# sourceMappingURL=memo-modal.component.js.map