import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';//1
import { RankTypeService } from '../../services/rank-type.service';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';//2
import { Subject } from 'rxjs/Rx';
import { TypeModalComponent } from '../type-modal/type-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-rank-type',
  templateUrl: './rank-type.component.html',
  styleUrls: ['./rank-type.component.css']
})

@Injectable()
export class RankTypeComponent implements OnInit, AfterViewInit {
	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	public id: string;
	public rank_type_id: any;
	public error_title: string;
	public error_message: string;
	public success_title;
	public success_message;
	public poststore: any;

	dtOptions: any = {};
	rank_type_rec: any;
	rank_type: any;

	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	constructor(
  		dialogService: DialogService, 
  		private modalService: DialogService,
  		private rank_service: RankTypeService,
  		){
		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");
	}

	ngAfterViewInit(){
		this.dtTrigger.next();
	}

	showMe(ranks: any){
		this.rank_type_rec = ranks;
	}

	ngOnInit() {
		this.data();

	}

	editModal(id:any){
		let disposable = this.modalService.addDialog(TypeModalComponent, {
            title:'Edit Rank Type',
            button:'Update',
            edit:true,
            id:id,
            url:'rank_type',
        	}).subscribe((isConfirmed)=>{
                this.rerender();
                this.ngOnInit();
            });
	}


	ngOnDestroy() {
	    //remove the the body classes
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}
	addModal() {
		let disposable = this.modalService.addDialog(TypeModalComponent, {
            title:'Add Rank Type',
            button:'Add',
		    url:'rank_type',
		    create:true
        	}).subscribe((isConfirmed)=>{
                this.ngOnInit();
                this.rerender();
            });
	}

	rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }


    data(){
		this.rank_service.getRankTypes().
		subscribe(
			data => {
				this.rank_type = data; 
				this.rerender();
			},
			err => console.error(err)
		);

	}


	archiveHoliday(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'rank_type'
        	}).subscribe((isConfirmed)=>{
		       	this.rerender();
                this.ngOnInit();
        });
	}
}
