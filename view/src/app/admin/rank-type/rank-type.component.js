var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core'; //1
import { RankTypeService } from '../../services/rank-type.service';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables'; //2
import { Subject } from 'rxjs/Rx';
import { TypeModalComponent } from '../type-modal/type-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
var RankTypeComponent = /** @class */ (function () {
    function RankTypeComponent(dialogService, modalService, rank_service) {
        this.modalService = modalService;
        this.rank_service = rank_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    RankTypeComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    RankTypeComponent.prototype.showMe = function (ranks) {
        this.rank_type_rec = ranks;
    };
    RankTypeComponent.prototype.ngOnInit = function () {
        this.data();
    };
    RankTypeComponent.prototype.editModal = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Edit Rank Type',
            button: 'Update',
            edit: true,
            id: id,
            url: 'rank_type',
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    RankTypeComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    RankTypeComponent.prototype.addModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Add Rank Type',
            button: 'Add',
            url: 'rank_type',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.ngOnInit();
            _this.rerender();
        });
    };
    RankTypeComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    RankTypeComponent.prototype.data = function () {
        var _this = this;
        this.rank_service.getRankTypes().
            subscribe(function (data) {
            _this.rank_type = data;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    RankTypeComponent.prototype.archiveHoliday = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'rank_type'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], RankTypeComponent.prototype, "dtElement", void 0);
    RankTypeComponent = __decorate([
        Component({
            selector: 'app-rank-type',
            templateUrl: './rank-type.component.html',
            styleUrls: ['./rank-type.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            DialogService,
            RankTypeService])
    ], RankTypeComponent);
    return RankTypeComponent;
}());
export { RankTypeComponent };
//# sourceMappingURL=rank-type.component.js.map