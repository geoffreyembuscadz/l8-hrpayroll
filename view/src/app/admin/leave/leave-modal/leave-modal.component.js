var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, NgZone, EventEmitter } from '@angular/core';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { LeaveService } from '../../../services/leave.service';
import { CommonService } from '../../../services/common.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { Leave } from '../../../model/leave';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';
import { LeaveValidationComponent } from '../../leave-validation/leave-validation.component';
import { Configuration } from '../../../app.config';
import { NgUploaderOptions } from 'ngx-uploader';
var LeaveModalComponent = /** @class */ (function (_super) {
    __extends(LeaveModalComponent, _super);
    function LeaveModalComponent(_common_service, _leaveservice, dialogService, _fb, _ar, daterangepickerOptions, _auth_service, _config, _zone) {
        var _this = _super.call(this, dialogService) || this;
        _this._common_service = _common_service;
        _this._leaveservice = _leaveservice;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this._auth_service = _auth_service;
        _this._config = _config;
        _this._zone = _zone;
        _this.leave = new Leave();
        _this.leave_payment_options = [{ value: 1, name: 'Leave w/ Pay' }, { value: 0, name: 'Leave Without Pay' }];
        _this.leave_remaining_leaves = 0;
        _this.mainInput = {
            start: moment(),
            end: moment().add(1, 'days')
        };
        _this.empStats = false;
        _this.typeStats = false;
        _this.start_date = new Date();
        _this.end_date = new Date();
        _this.validate = false;
        _this.radioData = [];
        _this.tableVal = false;
        _this.user = false;
        _this.admin = false;
        _this.dupVal = false;
        _this.attachment = null;
        _this.csvData = [];
        _this.route_upload = 'upload?type=leave';
        _this.host = _this._config.socketUrl;
        _this.action_url = _this._config.Server;
        // this.attachment = this.action_url + 'files/image.png';
        // uploader ngzone
        _this.option = new NgUploaderOptions({
            url: _this._config.ServerWithApiUrl + _this.route_upload,
            autoUpload: false,
            calculateSpeed: true,
            allowedExtensions: ['csv']
        });
        _this.inputUploadEvents = new EventEmitter();
        _this.leaveForm = _fb.group({
            'reason': [_this.leave.reason, [Validators.required]],
            'status_id': [null],
            'date_from': [null],
            'date_to': [null],
            'attachment': [null],
            'leave_payment_option': [null]
        });
        return _this;
    }
    LeaveModalComponent.prototype.ngOnInit = function () {
        //use for my-leave-list create button
        if (this.user == true) {
            this.empStats = true;
            this.get_employee();
        }
        //edit button in my-leave-list and leave-list
        if (this.edit == true) {
            this.getleave();
            this.validate = true;
            this.get_employee();
        }
        else {
            this.get_employee();
        }
        //uncomment this for socket.io
        // this.connectToServer();
        this.get_leave_type();
    };
    LeaveModalComponent.prototype.getleave = function () {
        var _this = this;
        this._leaveservice.getLeave(this.leave_id).subscribe(function (data) {
            _this.leave_data = (data);
            _this.mainInput.start = _this.leave_data[0].date_from;
            _this.mainInput.end = _this.leave_data[0].date_to;
            _this.date_from = _this.leave_data[0].date_from;
            _this.date_to = _this.leave_data[0].date_to;
            _this.total_days = _this.leave_data[0].total_days;
            _this.total_days = _this.leave_data[0].total_days;
            _this.total_hours = _this.leave_data[0].total_hours;
            _this.reason = _this.leave_data[0].reason;
            _this.type_id = _this.leave_data[0].leave_type_id;
            _this.employee_id = _this.leave_data[0].emp_id;
            _this.leave_id = _this.leave_data[0].id;
            if (_this.leave_data[0].attachment != null) {
                _this.attachment = _this.leave_data[0].attachment;
            }
            var len = _this.leave_data.length;
            var i = 1;
            for (var x = 0; x < len; x++) {
                var num = false;
                var length_days = parseFloat(_this.leave_data[x].length_days);
                var length_hours = parseFloat(_this.leave_data[x].length_hours);
                var time_type = _this.leave_data[x].time_type;
                var date = _this.leave_data[x].date;
                var half = false;
                var Fhalf = false;
                var Shalf = false;
                var whole = true;
                var with_pay = _this.leave_data[x].with_pay;
                var request_id = _this.leave_data[x].request_id;
                if (_this.leave_data[x].time_type == "First half") {
                    half = true;
                    Fhalf = true;
                }
                else if (_this.leave_data[x].time_type == "Second half") {
                    half = true;
                    Shalf = true;
                }
                _this.radioData.push({ date: date, i: i, half: half, Fhalf: Fhalf, Shalf: Shalf, whole: whole, length_days: length_days, length_hours: length_hours, time_type: time_type, request_id: request_id, with_pay: with_pay });
                i++;
            }
            _this.tableVal = true;
        }, function (err) { return console.error(err); });
    };
    LeaveModalComponent.prototype.get_employee = function () {
        this.employee = this.emp;
        console.log(this.employee);
        this.employee_value = [];
        if (this.edit == true) {
            this.employee_value = this.employee_id;
            this.validate = true;
        }
        this.options = {
            multiple: true
        };
        this.current = this.employee_value;
        this.getRemainingLeaves(this.current);
    };
    LeaveModalComponent.prototype.changedEmployee = function (data) {
        if (this.single == true) {
            var v = [];
            v.push(data.value);
            this.current = v;
        }
        else {
            this.current = data.value;
        }
        var len = this.current.length;
        if (len >= 1) {
            this.empStats = true;
        }
        else {
            this.empStats = false;
        }
        this.validations();
        this.duplicateEntryValidation();
    };
    LeaveModalComponent.prototype.validations = function () {
        if (this.empStats == true && this.typeStats == true && this.tableVal == true && this.dupVal == true || this.edit == true) {
            this.validate = true;
        }
        else {
            this.validate = false;
        }
    };
    LeaveModalComponent.prototype.get_leave_type = function () {
        var _this = this;
        this.leavetype = this._common_service.getLeaveType().
            subscribe(function (data) {
            _this.leavetype = Array.from(data);
            _this.leave_type_value = [];
            if (_this.edit == true) {
                _this.leave_type_value = _this.type_id;
                _this.validate = true;
            }
            _this.leave_type_current = _this.leave_type_value;
            if (_this.create == true) {
                var id = 0;
                var text = 'Select Type';
                _this.leavetype.unshift({ id: id, text: text });
                _this.leave_type_value = _this.value;
            }
        }, function (err) { return console.error(err); });
    };
    LeaveModalComponent.prototype.getRemainingLeaves = function (employee_id) {
    };
    LeaveModalComponent.prototype.changedLeaveType = function (data) {
        this.leave_type_current = data.value;
        var len = this.leave_type_current.length;
        if (len >= 1 && this.leave_type_current != 0) {
            this.typeStats = true;
        }
        else {
            this.typeStats = false;
        }
        this.validations();
    };
    LeaveModalComponent.prototype.onSubmit = function () {
        var _this = this;
        this.validate = false;
        this.duplicateEntryValidation();
        var leave = this.leaveForm.value;
        var i = leave.details.length - 1;
        leave.date_from = leave.details[0].date;
        leave.date_to = leave.details[i].date;
        if (this.edit == true && this.user == false) {
            this.leaveForm.value.id = this.leave_id;
            leave = this.leaveForm.value;
            this._leaveservice
                .updateLeave(leave)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.result = true;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        else if (this.edit == true && this.user == true) {
            this.leaveForm.value.id = this.leave_id;
            leave = this.leaveForm.value;
            this._leaveservice
                .updateMyLeave(leave)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.result = true;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        if (this.create == true && this.user == false) {
            this._leaveservice
                .createLeave(leave)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        else if (this.create == true && this.user == true) {
            this._leaveservice
                .createMyLeave(leave)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
    };
    LeaveModalComponent.prototype.connectToServer = function () {
        var _this = this;
        var socketUrl = this.host;
        this.socket = io.connect(socketUrl);
        this.socket.on("connect", function () { return _this.connect(); });
        this.socket.on("disconnect", function () { return _this.disconnect(); });
        this.socket.on("error", function (error) {
            console.log("ERROR: \"" + error + "\" (" + socketUrl + ")");
        });
    };
    // Handle connection opening
    LeaveModalComponent.prototype.connect = function () {
        // Request initial list when connected
        this.socket.emit("join", 'Client User -> ');
    };
    // Handle connection closing
    LeaveModalComponent.prototype.disconnect = function () {
        console.log("Disconnected from \"" + this.host + "\"");
    };
    LeaveModalComponent.prototype.errorCatch = function () {
        var _this = this;
        if (this.poststore.status_code == 200) {
            this.success_title = 'Success';
            this.success_message = 'No Changes Happen';
            setTimeout(function () {
                _this.close();
            }, 1000);
        }
        else if (this.poststore.status_code == 400) {
            this.error_title = 'Warning';
            this.error_message = 'Request is already exist';
            var temp_1;
            setTimeout(function () {
                var disposable = _this.dialogService.addDialog(ValidationModalComponent, {
                    title: 'Warning',
                    message: 'Leave',
                    url: 'leave',
                    duplicate: true,
                    data: _this.poststore.data,
                    userDup: _this.user,
                    adminDup: _this.admin,
                    emp: _this.emp,
                    user: _this.user
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.error_title = '';
                        _this.error_message = '';
                        setTimeout(function () {
                            _this.close();
                        }, 1000);
                    }
                    else if (message != false) {
                        temp_1 = message;
                        for (var x = 0; x < temp_1.length; ++x) {
                            var index = _this.current.indexOf(temp_1[x]);
                            _this.current.splice(index, 1);
                        }
                        _this.employee_value = _this.current;
                        if (_this.current.length == 0) {
                            _this.error_title = '';
                            _this.error_message = '';
                            setTimeout(function () {
                                _this.close();
                            }, 1000);
                        }
                    }
                });
            }, 1000);
        }
        //for lack of credits
        else if (this.poststore.status_code == 422) {
            if (this.multi == true) {
                this.single = false;
            }
            this.error_title = 'Warning';
            this.error_message = 'Lack of credits';
            setTimeout(function () {
                var disposable = _this.dialogService.addDialog(LeaveValidationComponent, {
                    title: 'Warning',
                    message: 'Leave',
                    url: 'leave',
                    data: _this.poststore.data,
                    predata: _this.poststore.predata,
                    adminDup: _this.admin,
                    user: _this.user,
                    edit: _this.poststore.updateData,
                    emp: _this.emp
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.close();
                    }
                    else {
                        _this.error_title = '';
                        _this.error_message = '';
                        _this.current = [];
                        _this.leave_type_current = [];
                        _this.employee_value = [];
                        _this.leave_type_value = [];
                        if (_this.edit == true) {
                            _this.leave_type_value = _this.type_id;
                            _this.employee_value = _this.employee_id;
                        }
                        else if (_this.create == true) {
                            _this.leave_type_value = _this.type_id;
                        }
                    }
                });
            }, 1000);
        }
        else {
            // socket.io emit
            // this.socket.emit('notify', this.poststore);
            this.validate = false;
            this.success_title = "Success";
            this.success_message = "Successfully";
            setTimeout(function () {
                _this.close();
            }, 2000);
        }
    };
    LeaveModalComponent.prototype.computeSingleDate = function () {
        var date = this.leaveForm.value.date;
        var start = moment(this.start_date).format("HH:mm");
        var end = moment(this.end_date).format("HH:mm");
        this.leaveForm.value.start_time = date + ' ' + start;
        this.leaveForm.value.end_time = date + ' ' + end;
        this.leaveForm.value.length_days = 0.5;
        if (this.leaveForm.value.length_hours >= 8) {
            this.leaveForm.value.length_days = 1;
        }
    };
    LeaveModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    LeaveModalComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        this.date_from = moment(dateInput.start).format("YYYY-MM-DD");
        this.date_to = moment(dateInput.end).format("YYYY-MM-DD");
        this.computeDays();
    };
    LeaveModalComponent.prototype.computeDays = function () {
        this.tableVal = true;
        if (this.radioData != []) {
            this.radioData = [];
        }
        if (this.date_from == null && this.date_to == null) {
            this.date_from = this.mainInput.start;
            this.date_to = this.mainInput.end;
            this.leaveForm.value.date_from = moment(this.date_from).format("YYYY-MM-DD");
            this.leaveForm.value.date_to = moment(this.date_to).format("YYYY-MM-DD");
        }
        this.validations();
        var start = this.date_from;
        var end = this.date_to;
        var from = moment(this.date_from, 'YYYY-MM-DD');
        var to = moment(this.date_to, 'YYYY-MM-DD');
        var i = 0;
        var half = false;
        var Fhalf = false;
        var Shalf = false;
        var whole = true;
        var length_days = 1;
        var length_hours = 8;
        var time_type = 'Whole Day';
        var request_id = 0;
        var with_pay = 1;
        /* using diff */
        var currDate = from.clone().startOf('day');
        var lastDate = to.clone().startOf('day');
        var date = moment(currDate.toDate()).format("YYYY-MM-DD");
        this.radioData.push({ date: date, i: i, half: half, Fhalf: Fhalf, Shalf: Shalf, whole: whole, length_days: length_days, length_hours: length_hours, time_type: time_type, request_id: request_id, with_pay: with_pay });
        while (currDate.add(1, 'days').diff(lastDate) < 0) {
            var date_1 = moment(currDate.clone().toDate()).format("YYYY-MM-DD");
            this.radioData.push({ date: date_1, i: i, half: half, Fhalf: Fhalf, Shalf: Shalf, whole: whole, length_days: length_days, length_hours: length_hours, time_type: time_type, request_id: request_id, with_pay: with_pay });
        }
        if (start != end) {
            var date_2 = moment(lastDate.toDate()).format("YYYY-MM-DD");
            this.radioData.push({ date: date_2, i: i, half: half, Fhalf: Fhalf, Shalf: Shalf, whole: whole, length_days: length_days, length_hours: length_hours, time_type: time_type, request_id: request_id, with_pay: with_pay });
        }
        var len = this.radioData.length;
        var t = 1;
        for (var x = 0; x < len; ++x) {
            this.radioData[x].i = t;
            t++;
        }
        this.computeLengthDay();
    };
    LeaveModalComponent.prototype.halfDay = function (id) {
        this.radioData[id].half = true;
        this.radioData[id].whole = false;
        this.radioData[id].Shalf = false;
        this.radioData[id].Fhalf = true;
        this.radioData[id].length_days = 0.5;
        this.radioData[id].length_hours = 4;
        this.radioData[id].time_type = 'First half';
        this.computeLengthDay();
    };
    LeaveModalComponent.prototype.wholeDay = function (id) {
        this.radioData[id].whole = true;
        this.radioData[id].half = false;
        this.radioData[id].length_days = 1;
        this.radioData[id].length_hours = 8;
        this.radioData[id].time_type = 'Whole Day';
        this.computeLengthDay();
    };
    LeaveModalComponent.prototype.computeLengthDay = function () {
        var len = this.radioData.length;
        var days = 0;
        var hours = 0;
        for (var i = 0; i < len; ++i) {
            days = this.radioData[i].length_days + days;
            hours = this.radioData[i].length_hours + hours;
        }
        this.total_days = days;
        this.total_hours = hours;
        this.duplicateEntryValidation();
    };
    LeaveModalComponent.prototype.choice = function (id, event) {
        this.radioData[id].time_type = event;
        if (event == 'First Half') {
            this.radioData[id].Fhalf = true;
            this.radioData[id].Shalf = false;
        }
        else if (event == 'Second Half') {
            this.radioData[id].Fhalf = false;
            this.radioData[id].Shalf = true;
        }
    };
    LeaveModalComponent.prototype.removeDay = function (id) {
        this.radioData.splice(id, 1);
        if (this.radioData.length == 0) {
            this.tableVal = false;
        }
        this.computeLengthDay();
    };
    LeaveModalComponent.prototype.duplicateEntryValidation = function () {
        var _this = this;
        this.leaveForm.value.employee_id = this.current;
        this.leaveForm.value.supervisor_id = this.supervisor_id;
        this.leaveForm.value.type = this.leave_type_current;
        this.leaveForm.value.total_days = this.total_days;
        this.leaveForm.value.total_with_pay = this.total_days;
        this.leaveForm.value.total_hours = this.total_hours;
        this.leaveForm.value.created_by = this.user_id;
        this.leaveForm.value.details = this.radioData;
        this.leaveForm.value.date_from = moment(this.date_from).format("YYYY-MM-DD");
        this.leaveForm.value.date_to = moment(this.date_to).format("YYYY-MM-DD");
        if (this.radioData.length == 1) {
            this.leaveForm.value.date_from = moment(this.radioData[0].date).format("YYYY-MM-DD");
            this.leaveForm.value.date_to = moment(this.radioData[0].date).format("YYYY-MM-DD");
        }
        if (this.user == true && this.create == true || this.user == true && this.edit == true) {
            var v = [];
            v.push(this.employee_id);
            this.leaveForm.value.employee_id = v;
        }
        if (this.edit == true) {
            this.leaveForm.value.edit = this.edit;
            this.leaveForm.value.ids = this.leave_id;
        }
        else {
            this.leaveForm.value.edit = false;
        }
        this.leaveForm.value.id = -1;
        if (this.leaveForm.value.employee_id != null && this.leaveForm.value.details.length != 0 || this.edit == true) {
            var leave = this.leaveForm.value;
            this._leaveservice
                .duplicateEntryValidation(leave)
                .subscribe(function (data) {
                _this.poststore = data;
                if (_this.poststore.message == "duplicate" && _this.multi == true) {
                    _this.errorCatch();
                }
                else if (_this.poststore.message == "duplicate") {
                    _this.dupVal = false;
                    _this.errorCatch();
                }
                else {
                    _this.dupVal = true;
                    _this.error_title = '';
                    _this.error_message = '';
                }
                _this.validations();
            }, function (err) { return _this.catchError(err); });
        }
    };
    LeaveModalComponent.prototype.handleUpload = function (data) {
        var _this = this;
        this._zone.run(function () {
            _this.response = data;
            if (data && data.response) {
                _this.response = JSON.parse(data.response);
                _this.attachment = _this.response.file_url;
                _this.leaveForm.value.attachment = _this.response.file_url;
            }
        });
    };
    LeaveModalComponent.prototype.beforeUpload = function (data) {
    };
    LeaveModalComponent.prototype.startUpload = function () {
        this.inputUploadEvents.emit('startUpload');
    };
    LeaveModalComponent = __decorate([
        Component({
            selector: 'app-leave-modal',
            templateUrl: './leave-modal.component.html',
            styleUrls: ['./leave-modal.component.css']
        }),
        __metadata("design:paramtypes", [CommonService,
            LeaveService,
            DialogService,
            FormBuilder,
            ActivatedRoute,
            DaterangepickerConfig,
            AuthUserService,
            Configuration,
            NgZone])
    ], LeaveModalComponent);
    return LeaveModalComponent;
}(DialogComponent));
export { LeaveModalComponent };
//# sourceMappingURL=leave-modal.component.js.map