import { Component, OnInit, Injectable, TemplateRef, NgZone, EventEmitter } from '@angular/core';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { LeaveService } from '../../../services/leave.service';
import { CommonService } from '../../../services/common.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { Leave } from '../../../model/leave';
import { Select2OptionData } from 'ng2-select2';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';
import { LeaveValidationComponent } from '../../leave-validation/leave-validation.component';
import { Configuration } from '../../../app.config';
import { NgUploaderOptions } from 'ngx-uploader';


@Component({
  selector: 'app-leave-modal',
  templateUrl: './leave-modal.component.html',
  styleUrls: ['./leave-modal.component.css']
})
export class LeaveModalComponent  extends DialogComponent<null, boolean> {


	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public poststore: any;
	public leave = new Leave();
	public lastname: any;
	public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public leave_type_value: Array<Select2OptionData>;
	public leave_payment_options = [{ value: 1, name: 'Leave w/ Pay'}, { value: 0, name: 'Leave Without Pay'}];
	public current: any;
	public options: Select2Options;
	public leavetype : any;
	public leave_type_current : any;
	public leave_remaining_leaves = 0;
	public date_from:any;
	public date_to:any;
	public total_days:any;
	public total_hours:any;
	emp:any;
	public user_id: any;
    public mainInput = {
        start: moment(),
        end: moment().add(1,'days')
    }
    user_employee_id:any;

	leaveForm : FormGroup;
	empStats = false;
	typeStats = false;
	public start_date:Date = new Date();
	public end_date:Date = new Date();
	validate = false;
	employee_id:any;
	leave_id:any;
	leave_data:any;
	reason:any;
    type_id:any;
    edit:any;
    create:any;
    multi:any;
    single:any;
    value:any;
    radioData=[];
    tableVal = false;
    user = false;
    admin = false;
    dupVal = false;
    supervisor_id:any;
    role_id:any;
    messenger_id:any;

    private action_url: string;
    attachment=null;

    csvUrl: string;  // URL to web API
  	csvData: any[] = [];
  	uploadfile:any;
	option: NgUploaderOptions;
	response: any;
	hasBaseDropZoneOver: boolean;
	inputUploadEvents: EventEmitter<string>;
	private route_upload = 'upload?type=leave';

	socket: SocketIOClient.Socket;
    host:any;

   constructor(
   	private _common_service: CommonService,
   	private _leaveservice: LeaveService, 
   	dialogService: DialogService, 
   	private _fb: FormBuilder, 
   	private _ar: ActivatedRoute,
   	private daterangepickerOptions: DaterangepickerConfig,
   	private _auth_service: AuthUserService,
   	private _config: Configuration, 
   	private _zone: NgZone,
   	) { 

   super(dialogService);

   	this.host = this._config.socketUrl;

   	this.action_url = this._config.Server;

  	// this.attachment = this.action_url + 'files/image.png';

  	// uploader ngzone
    this.option = new NgUploaderOptions({
    	url: this._config.ServerWithApiUrl + this.route_upload,
    	autoUpload: false,
    	calculateSpeed: true,
    	allowedExtensions: ['csv']
    });

    this.inputUploadEvents = new EventEmitter<string>();
    
    this.leaveForm = _fb.group({
		'reason':			[this.leave.reason, [Validators.required]],
		'status_id':		[null],
		'date_from':		[null],
		'date_to':			[null],
		'attachment': 		[null],
		'leave_payment_option': [null]
      
    }); 


   }


  	ngOnInit() {

  		//use for my-leave-list create button
  		if(this.user == true){
    		this.empStats = true;
    		this.get_employee();
    	}

    	//edit button in my-leave-list and leave-list
		if(this.edit == true){
			this.getleave();
    		this.validate = true;
    		this.get_employee();
    	}
    	else{
	   		this.get_employee();
    	}
	   		//uncomment this for socket.io
    		// this.connectToServer();
  		this.get_leave_type();
  		
 	}

	getleave(){
		this._leaveservice.getLeave(this.leave_id).subscribe(
	      data => {
	        this.leave_data=(data);
      		this.mainInput.start = this.leave_data[0].date_from;
        	this.mainInput.end = this.leave_data[0].date_to;
      		this.date_from = this.leave_data[0].date_from;
        	this.date_to = this.leave_data[0].date_to;
        	this.total_days = this.leave_data[0].total_days;
        	this.total_days = this.leave_data[0].total_days;
        	this.total_hours = this.leave_data[0].total_hours;
        	this.reason = this.leave_data[0].reason;
        	this.type_id=this.leave_data[0].leave_type_id;
        	this.employee_id = this.leave_data[0].emp_id;
        	this.leave_id = this.leave_data[0].id;

        	if(this.leave_data[0].attachment != null){
        		this.attachment = this.leave_data[0].attachment;
        	}

	        let len = this.leave_data.length;
	        let i = 1;
	        for (let x = 0; x < len; x++) {

				let num = false;
				let length_days = parseFloat(this.leave_data[x].length_days);
				let length_hours = parseFloat(this.leave_data[x].length_hours);
				let time_type = this.leave_data[x].time_type;
				let date = this.leave_data[x].date;
				let half = false;
				let Fhalf = false;
				let Shalf = false;
				let whole = true;
				let with_pay = this.leave_data[x].with_pay;
				let request_id = this.leave_data[x].request_id;
				if(this.leave_data[x].time_type == "First half"){
					half = true;
					Fhalf = true;
				}
				else if(this.leave_data[x].time_type == "Second half"){
					half = true;
					Shalf = true;
				}

				this.radioData.push({date,i,half,Fhalf,Shalf,whole,length_days,length_hours,time_type,request_id,with_pay});
				i++;

	        }

	        this.tableVal = true;
	      },
	      err => console.error(err)
    	);
    	
	}


	get_employee(){

		this.employee = this.emp;
		console.log(this.employee);
		this.employee_value = [];
		if(this.edit == true) {
			this.employee_value = this.employee_id;
			this.validate = true;
        } 

		this.options = {
			multiple: true
		}
		this.current = this.employee_value;

		this.getRemainingLeaves(this.current);
	}

	changedEmployee(data: any) {
		
		if(this.single == true){
			let v = [];
			v.push(data.value);
			this.current = v;
		}
		else{
			this.current = data.value;
		}

		let len = this.current.length;
		if(len >= 1){
			this.empStats = true;
		}
		else{
			this.empStats = false;
		}

		this.validations();
		this.duplicateEntryValidation();

	}

	validations(){
		if(this.empStats == true && this.typeStats == true && this.tableVal == true && this.dupVal == true || this.edit == true){
      		this.validate=true;
      	}
     	else{
     		this.validate=false;
     	}
	}

	get_leave_type(){
		this.leavetype = this._common_service.getLeaveType().
		subscribe(
			data => {
				this.leavetype = Array.from(data);
				this.leave_type_value = [];
				if(this.edit == true) {
					this.leave_type_value = this.type_id;
					this.validate = true;
	            }
				this.leave_type_current = this.leave_type_value;

				if(this.create == true){
					let id = 0;
			        let text = 'Select Type';

			        this.leavetype.unshift({id,text});

			        this.leave_type_value = this.value;
					
				}
				
			},
			err => console.error(err)
		);
	}

	getRemainingLeaves(employee_id: any){

	}

	changedLeaveType(data: any) {
      this.leave_type_current = data.value;
      let len = this.leave_type_current.length;
		if(len >= 1 && this.leave_type_current != 0){
			this.typeStats = true;

		}
		else{
			this.typeStats = false;
		}

		this.validations();
  	}


	onSubmit(){

		this.validate = false;
		this.duplicateEntryValidation();

		let  leave = this.leaveForm.value;
		let i = leave.details.length -1;
		leave.date_from = leave.details[0].date;
		leave.date_to = leave.details[i].date;

		if(this.edit == true && this.user == false){
			this.leaveForm.value.id= this.leave_id;
			leave = this.leaveForm.value;
			
			this._leaveservice
			.updateLeave(leave)
			.subscribe(
				data => {
					this.poststore = data;
					
					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}
		else if(this.edit == true && this.user == true){
			this.leaveForm.value.id= this.leave_id;
			leave = this.leaveForm.value;
			
			this._leaveservice
			.updateMyLeave(leave)
			.subscribe(
				data => {
					this.poststore = data;
					
					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}

		if(this.create == true && this.user == false){
			this._leaveservice
			.createLeave(leave)
			.subscribe(
				data => {
					this.poststore = data;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}
		else if(this.create == true && this.user == true){
			this._leaveservice
			.createMyLeave(leave)
			.subscribe(
				data => {
					this.poststore = data;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}
	}

	connectToServer(){
	    let socketUrl = this.host;
	    this.socket = io.connect(socketUrl);
	    this.socket.on("connect", () => this.connect());
	    this.socket.on("disconnect", () => this.disconnect());
	    
	    this.socket.on("error", (error: string) => {
	      console.log(`ERROR: "${error}" (${socketUrl})`);
	    });

	   
	  }

	// Handle connection opening
	private connect() {
	    // Request initial list when connected
	    this.socket.emit("join", 'Client User -> ');
	}
	// Handle connection closing
	private disconnect() {
	    console.log(`Disconnected from "${this.host}"`);
	}

	errorCatch(){
		if(this.poststore.status_code == 200){
			this.success_title = 'Success';
			this.success_message = 'No Changes Happen';
			setTimeout(() => {
	  		   this.close();
	      	}, 1000);
		}
		else if(this.poststore.status_code == 400){

			this.error_title = 'Warning';
			this.error_message = 'Request is already exist';

			let temp;
			setTimeout(() => {
				let disposable = this.dialogService.addDialog(ValidationModalComponent, {
				title:'Warning',
				message:'Leave',
				url:'leave',
				duplicate:true,
				data:this.poststore.data,
				userDup:this.user,
				adminDup:this.admin,
            	emp:this.emp,
            	user:this.user
				}).subscribe((message)=>{
					if (message == true) { 
						this.error_title = '';
						this.error_message = '';

						setTimeout(() => {
				  		   this.close();
				      	}, 1000);
					}
					else if(message != false){
    					temp = message;

    					for (let x = 0; x < temp.length; ++x) {
							let index = this.current.indexOf(temp[x]);
	    					this.current.splice(index, 1);
    					}

	    				this.employee_value = this.current;

    					if(this.current.length == 0){
	    					this.error_title = '';
							this.error_message = '';
							setTimeout(() => {
					  		   this.close();
					      	}, 1000);
    					}
					}
				});
	      	}, 1000);

		}
		//for lack of credits
		else if(this.poststore.status_code == 422){
			if(this.multi == true){
    			this.single = false;
    		}
			this.error_title = 'Warning';
			this.error_message = 'Lack of credits'; 
			setTimeout(() => {
				let disposable = this.dialogService.addDialog(LeaveValidationComponent, {
				title:'Warning',
				message:'Leave',
				url:'leave',
				data:this.poststore.data,
				predata:this.poststore.predata,
				adminDup:this.admin,
				user:this.user,
				edit:this.poststore.updateData,
            	emp:this.emp
				}).subscribe((message)=>{
					if(message == true){
						this.close();
					}
					else{
						this.error_title = '';
						this.error_message = ''; 
						this.current = [];
						this.leave_type_current = [];
						this.employee_value = [];
						this.leave_type_value = [];

						if(this.edit == true){
							this.leave_type_value = this.type_id;
							this.employee_value = this.employee_id;
						}
						else if (this.create == true) {
							this.leave_type_value = this.type_id;
						}
					}
				});
	      	}, 1000);

		}
		else{
			// socket.io emit
			// this.socket.emit('notify', this.poststore);
			this.validate = false;
			this.success_title = "Success";
			this.success_message = "Successfully";
			setTimeout(() => {
	  		   this.close();
	      	}, 2000);
		}
	}

	computeSingleDate(){
		let date = this.leaveForm.value.date;
		let start =  moment(this.start_date).format("HH:mm");
		let end = moment(this.end_date).format("HH:mm");

		this.leaveForm.value.start_time = date + ' ' + start;
		this.leaveForm.value.end_time = date + ' ' + end;
		this.leaveForm.value.length_days = 0.5;

		if(this.leaveForm.value.length_hours >= 8){
			this.leaveForm.value.length_days = 1;
		}

	}


  	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

    private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        dateInput.end = value.end;

        this.date_from = moment(dateInput.start).format("YYYY-MM-DD");
       	this.date_to = moment(dateInput.end).format("YYYY-MM-DD");

		this.computeDays();
    }

	computeDays(){

		this.tableVal = true;

		if(this.radioData != []){
			this.radioData = [];
		}

		if(this.date_from == null && this.date_to == null){
			this.date_from = this.mainInput.start;
			this.date_to = this.mainInput.end;
			this.leaveForm.value.date_from = moment(this.date_from).format("YYYY-MM-DD");
			this.leaveForm.value.date_to = moment(this.date_to).format("YYYY-MM-DD");
		}
		
		this.validations();

		let start = this.date_from;
		let end = this.date_to;
		let from = moment(this.date_from, 'YYYY-MM-DD'); 
		let to = moment(this.date_to, 'YYYY-MM-DD');    
		let i = 0;
		let half = false;
		let Fhalf = false;
		let Shalf = false;
		let whole = true;
		let length_days = 1;
		let length_hours = 8;
		let time_type = 'Whole Day';
		let request_id = 0;
		let with_pay = 1;
	  
		/* using diff */
	    let currDate = from.clone().startOf('day');
	    let lastDate = to.clone().startOf('day');

	    let date = moment(currDate.toDate()).format("YYYY-MM-DD" );

	   	this.radioData.push({date,i,half,Fhalf,Shalf,whole,length_days,length_hours,time_type,request_id,with_pay});

	    while(currDate.add(1 ,'days').diff(lastDate) < 0) {
	    	let date = moment(currDate.clone().toDate()).format("YYYY-MM-DD");
	        this.radioData.push({date,i,half,Fhalf,Shalf,whole,length_days,length_hours,time_type,request_id,with_pay});
	    }

	    if(start != end)  {
	    	let date = moment(lastDate.toDate()).format("YYYY-MM-DD");
	    	this.radioData.push({date,i,half,Fhalf,Shalf,whole,length_days,length_hours,time_type,request_id,with_pay});
	    }

	    let len = this.radioData.length;

	    let t = 1;
	    for (let x = 0; x < len; ++x) {
	    	this.radioData[x].i = t;
	    	t++;
	    }

	    this.computeLengthDay();
	    
	}
	halfDay(id){
		this.radioData[id].half = true;
		this.radioData[id].whole = false;
		this.radioData[id].Shalf = false;
		this.radioData[id].Fhalf = true;
		this.radioData[id].length_days = 0.5;
		this.radioData[id].length_hours = 4;
		this.radioData[id].time_type = 'First half';
		this.computeLengthDay();
	}
	wholeDay(id){
		this.radioData[id].whole = true;
		this.radioData[id].half = false;
		this.radioData[id].length_days = 1;
		this.radioData[id].length_hours = 8;
		this.radioData[id].time_type = 'Whole Day';
		this.computeLengthDay();
	}

	computeLengthDay(){
		let len = this.radioData.length;
		let days = 0;
		let hours = 0;
		for (let i = 0; i < len; ++i) {
			days = this.radioData[i].length_days + days;
			hours = this.radioData[i].length_hours + hours;
		}
		this.total_days = days;
		this.total_hours = hours;

		this.duplicateEntryValidation();
	}

	choice(id:any,event:any){
		this.radioData[id].time_type = event;

		if(event == 'First Half'){
			this.radioData[id].Fhalf = true;
			this.radioData[id].Shalf = false;
		}
		else if(event == 'Second Half'){
			this.radioData[id].Fhalf = false;
			this.radioData[id].Shalf = true;
		}

	}

	removeDay(id){
		this.radioData.splice(id, 1);

		if(this.radioData.length == 0){
			this.tableVal = false;
		}

		this.computeLengthDay();
	}

	duplicateEntryValidation(){

		this.leaveForm.value.employee_id = this.current;
		this.leaveForm.value.supervisor_id = this.supervisor_id;
		this.leaveForm.value.type = this.leave_type_current;
		this.leaveForm.value.total_days = this.total_days;
		this.leaveForm.value.total_with_pay = this.total_days;
		this.leaveForm.value.total_hours = this.total_hours;
		this.leaveForm.value.created_by = this.user_id;
		this.leaveForm.value.details = this.radioData;
		this.leaveForm.value.date_from = moment(this.date_from).format("YYYY-MM-DD");
		this.leaveForm.value.date_to = moment(this.date_to).format("YYYY-MM-DD");

		if(this.radioData.length == 1){
			this.leaveForm.value.date_from = moment(this.radioData[0].date).format("YYYY-MM-DD");
			this.leaveForm.value.date_to = moment(this.radioData[0].date).format("YYYY-MM-DD");
		}

		if(this.user == true  && this.create == true || this.user == true && this.edit == true){
			let v = [];
			v.push(this.employee_id);
			this.leaveForm.value.employee_id = v;
		}

		if(this.edit == true){
			this.leaveForm.value.edit = this.edit;
			this.leaveForm.value.ids= this.leave_id;
		}
		else{
			this.leaveForm.value.edit = false;
		}

		this.leaveForm.value.id = -1;

		if(this.leaveForm.value.employee_id != null && this.leaveForm.value.details.length != 0 || this.edit == true){

			let  leave = this.leaveForm.value;
			this._leaveservice
				.duplicateEntryValidation(leave)
				.subscribe(
					data => {
						this.poststore = data;

						if(this.poststore.message == "duplicate" && this.multi == true){
							this.errorCatch();
						}
						else if(this.poststore.message == "duplicate"){
							this.dupVal = false;
							this.errorCatch();
						}
						else{
							this.dupVal = true;
							this.error_title = '';
							this.error_message = '';
						}

						this.validations();
					},
					err => this.catchError(err)
				);	
		}
	}

	handleUpload(data: any) {
		this._zone.run(() => {
	    	this.response = data;
	    	if (data && data.response) {
	    		this.response = JSON.parse(data.response);
	    		this.attachment = this.response.file_url;
	    		this.leaveForm.value.attachment = this.response.file_url;

	    	}
	    });
	}

	beforeUpload(data: any){
	}

	startUpload() {
    	this.inputUploadEvents.emit('startUpload');
  	}

}