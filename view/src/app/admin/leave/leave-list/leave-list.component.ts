import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { LeaveModalComponent } from '../../leave/leave-modal/leave-modal.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
import { CommonService } from '../../../services/common.service';
import { LeaveService } from '../../../services/leave.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Select2OptionData } from 'ng2-select2';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { AuthUserService } from '../../../services/auth-user.service';
import { ActionCenterModalComponent } from '../../action-center/action-center-modal/action-center-modal.component';


@Component({
	selector: 'app-leave',
	templateUrl: './leave-list.component.html',
	styleUrls: ['./leave-list.component.css']
})

@Injectable()
export class LeaveListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};
	public leave_id: any;
	api: String;
	leave=[];

	byStatus=false;
	byDate=false;
	byCompany=false;
	byEmployee=false;
	byType=false;

	public company: any;
	public company_current : any;
	company_value: Array<Select2OptionData>;

	public mainInput = {
        start: moment().subtract(12, 'month'),
        end: moment().subtract(11, 'month')
    }
    filterForm:any;

    public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public employee_current: any;
	public options: Select2Options;
	emp:any;

	public status : Array<Select2OptionData>;
	public status_value: any;
	public status_current: any;
	sta:any;

	public type: any;
	public type_current : any;
	type_value: Array<Select2OptionData>;

	user_id:any;
	supervisor_id:any;
	role_id:any;
	employee_id:any;

	btnCreate = false;
	btnEdit = false;
	btnApproved = false;
	btnAction = false;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _leave_service: LeaveService,
		private _common_service: CommonService,
		private daterangepickerOptions: DaterangepickerConfig,
		private _fb: FormBuilder,
   		private _auth_service: AuthUserService
		){

		this.filterForm = _fb.group({
			'status_id': 		[null],
			'start_date': 		[null],
			'end_date': 		[null],
			'company_id': 		[null],
			'employee_id': 		[null],
			'type_id': 			[null]
		});

		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");
	}

	ngOnInit() {
		this.dateOption();
		this.getEmployee();
		this.getPermissionId();
	}

	getPermissionId(){
		this._auth_service.getPermissionId().
		subscribe(
			data => {
				this.permissionValidation(data);

				
			},
			err => console.error(err)
		);

	}

	permissionValidation(user){
		let len = user.length;
		for (let i = 0; i < len; ++i) {
			if(user[i] == "152"){
				this.btnCreate = true;
			}
			else if(user[i] == "153"){
				this.btnEdit = true;
			}
			else if(user[i] == "154"){
				this.btnApproved = true;
			}
		}

		if(this.btnEdit == false && this.btnApproved == false){
			this.btnAction = true;
		}

		this.data();
	}

	dateOption(){
    	this.daterangepickerOptions.settings = {
        	singleDatePicker: false
		};
    }

	data(){

		let model = this.filterForm.value;

		this._leave_service.getLeaves(model).
		subscribe(
			data => {
				this.leave= Array.from(data);
				this.rerender();
			},
			err => console.error(err)
			);

	}

	getEmployee(){

		this._common_service.getEmployee().
		subscribe(
			data => {
				this.emp = Array.from(data);
				this.employee = this.emp;
				this.employee_value = [];
				this.options = {
					multiple: true
				}
				this.employee_current = this.employee_value;
			},
			err => console.error(err)
		);
	}
	changedEmployee(data: any) {
		this.employee_current = data.value;

		if(this.employee_current == 0){
			this.filterForm.value.employee_id = null;
		}
		else{
			this.filterForm.value.employee_id = this.employee_current;
		}
			this.data();
	}

	getStatus(){
		this._common_service.getStatusType().
		subscribe(
			data => {
				this.sta = Array.from(data);
				this.status = this.sta;
				this.status_value = [];
				this.status_value = ['1', '2','3','4'];
				this.options = {
					multiple: true
				}
				this.status_current = this.status_value;
			},
			err => console.error(err)
		);
	}
	changedStatus(data: any) {
		this.status_current = data.value;

		if(this.status_current == 0){
			this.filterForm.value.status_id = null;
		}
		else{
			this.filterForm.value.status_id = this.status_current;
		}
			this.data();
	}

    getCompany(){
	      this._common_service.getCompany()
	      .subscribe(
	        data => {
	        this.company = Array.from(data);
	        this.company_value = [];
	        this.options = {
				multiple: true
			}
	        this.company_current = this.company_value;
	        
	        },
	        err => console.error(err)
	    );
	}
	changedCompany(data: any) {

		this.company_current = data.value;

		if(this.company_current == 0){
			this.filterForm.value.company_id = null;
		}
		else{
			this.filterForm.value.company_id = this.company_current;
		}
			this.data();

	} 

    getType(){
	      this._common_service.getLeaveType()
	      .subscribe(
	        data => {
	        this.type = Array.from(data);
	        this.type_value = [];
	        this.options = {
				multiple: true
			}
	        this.type_current = this.type_value;
	        
	        },
	        err => console.error(err)
	    );
	}
	changedType(data: any) {

		this.type_current = data.value;

		if(this.type_current == 0){
			this.filterForm.value.type_id = null;
		}
		else{
			this.filterForm.value.type_id = this.type_current;
		}
			this.data();

	} 

	choiceFilter(id){

		if(id==1){
			this.getStatus();
			this.getCompany();
			this.getEmployee();
			this.getType();
			this.byStatus=true;
			this.byDate=true;
			this.byCompany=true;
			this.byEmployee=true;
			this.byType=true;
		}
		else if(id==2){
			if (this.byStatus==false) { 
				this.getStatus();
				this.byStatus=true;
			} else {
				this.byStatus=false;
				this.filterForm.value.status_id = null;
			}
		}
		else if(id==3){
			if (this.byDate==false) { 
				this.byDate=true;
			} else {
				this.byDate=false;
				this.filterForm.value.start_date = null;
        		this.filterForm.value.end_date = null;
			}
		}
		else if(id==4){
			if (this.byCompany==false) { 
				this.getCompany();
				this.byCompany=true;
			} else {
				this.byCompany=false;
				this.filterForm.value.company_id = null;
			}
		}
		else if(id==5){
			if (this.byEmployee==false) { 
				this.getEmployee();
				this.byEmployee=true;
			} else {
				this.byEmployee=false;
				this.filterForm.value.employee_id = null;
			}
		}
		else if(id==6){
			if (this.byType==false) { 
				this.getType();
				this.byType=true;
			} else {
				this.byType=false;
				this.filterForm.value.type_id = null;
			}
		}
		else{
			this.byStatus = false;
			this.byDate = false;
			this.byCompany=false;
			this.byEmployee=false;
			this.byType=false;

			this.filterForm.value.status_id = null;
        	this.filterForm.value.company_id = null;
        	this.filterForm.value.start_date = null;
        	this.filterForm.value.end_date = null;
        	this.filterForm.value.employee_id = null;
        	this.filterForm.value.type_id = null;
        	this.data();
		}

	}

	filterDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;

        let start_date = moment(dateInput.start).format("YYYY-MM-DD");
        let end_date = moment(dateInput.end).format("YYYY-MM-DD");

        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;

        this.data();

    }

	approvedLeave(id:any) {

		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
			title:'Approved Leave',
			message:'Are you sure you want to Approved this Leave?',
			action:'Approved',
			id:id,
			url:'leave',
            request:true
		}).subscribe((isConfirmed)=>{
			this.dateOption();
            this.data();

		});
	}

	rejectedLeave(id:any) {

		let disposable = this.modalService.addDialog(RemarksModalComponent, {
			title:'Reject Leave',
			id:id,
			url:'leave',
			button:'Reject'
		}).subscribe((isConfirmed)=>{
			this.dateOption();
            this.data();

		});
	}


	createLeave() {
		let disposable = this.modalService.addDialog(LeaveModalComponent, {
			title:'Create Leave',
			button:'Add',
			create:true,
			multi:true,
			admin:true,
            emp:this.emp
		}).subscribe((isConfirmed)=>{
			this.dateOption();
           	this.data();
		});
	}


	viewDetails(id:any,status:any) {
		let buttons = true;
		if (status == 'REJECTED' || status == 'APPROVED' || status == 'CANCELLED') {
			buttons = false;
		}

		let disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title:'Leave',
            id:id,
            url:'leave',
            date_range:true,
            leave:true,
            types:true,
            buttons:buttons
        	}).subscribe((isConfirmed)=>{
                this.dateOption();
                this.data();

            });
	}

	editLeave(id,emp_id){

		let v = [];
		v.push(emp_id);
		let employee = v;
		
		let disposable = this.modalService.addDialog(LeaveModalComponent, {
            title:'Update Leave',
            button:'Update',
            edit:true,
            leave_id:id,
            single:true,
            true:true,
            emp:this.emp,
            employee_id:employee
        	}).subscribe((isConfirmed)=>{
                this.dateOption();
                this.data();
            });
	}

	ngOnDestroy() {
		this.body.classList.remove("skin-blue");
		this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
		this.dtTrigger.next();
	}

	rerender(): void {
		this.dtElement.dtInstance.then((dtInstance) => {
			
			dtInstance.destroy();
			
			this.dtTrigger.next();
		});
	}
}
