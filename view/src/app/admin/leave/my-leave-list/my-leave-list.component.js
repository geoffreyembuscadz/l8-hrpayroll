var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import 'rxjs/add/operator/map';
import { LeaveService } from '../../../services/leave.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { Configuration } from '../../../app.config';
import { LeaveModalComponent } from '../../leave/leave-modal/leave-modal.component';
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { ActionCenterModalComponent } from '../../action-center/action-center-modal/action-center-modal.component';
import { CommonService } from '../../../services/common.service';
import { FormBuilder } from '@angular/forms';
import * as moment from 'moment';
var MyLeaveListComponent = /** @class */ (function () {
    function MyLeaveListComponent(_leaservice, modalService, _conf, _auth_service, _common_service, _fb, daterangepickerOptions) {
        this._leaservice = _leaservice;
        this.modalService = modalService;
        this._conf = _conf;
        this._auth_service = _auth_service;
        this._common_service = _common_service;
        this._fb = _fb;
        this.daterangepickerOptions = daterangepickerOptions;
        this.dtTrigger = new Subject();
        this.leave = [];
        this.buttonVal = false;
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filterForm = _fb.group({
            'status_id': [null],
            'start_date': [null],
            'end_date': [null],
            'type_id': [null]
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    MyLeaveListComponent.prototype.ngOnInit = function () {
        this.dateOption();
        this.getListByIdLimited();
        this.getStatus();
        this.getType();
        this.getData();
    };
    MyLeaveListComponent.prototype.getListByIdLimited = function () {
        var _this = this;
        this._auth_service.getListByIdLimited().
            subscribe(function (data) {
            var user = data;
            _this.employee_id = user.employee_id;
            _this.buttonVal = true;
        }, function (err) { return console.error(err); });
    };
    MyLeaveListComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    MyLeaveListComponent.prototype.getData = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._leaservice.getMyleave(model).
            subscribe(function (data) {
            _this.leave = data;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    MyLeaveListComponent.prototype.editLeave = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(LeaveModalComponent, {
            title: 'Update Leave',
            button: 'Update',
            edit: true,
            leave_id: id,
            user: true,
            employee_id: this.employee_id
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyLeaveListComponent.prototype.createLeave = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(LeaveModalComponent, {
            title: 'Create Leave',
            button: 'Add',
            create: true,
            employee_id: this.employee_id,
            user: true
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyLeaveListComponent.prototype.cancelLeave = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(RemarksModalComponent, {
            title: 'Cancel Leave',
            id: id,
            url: 'leave',
            button: 'Ok'
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyLeaveListComponent.prototype.viewDetails = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title: 'Leave',
            id: id,
            url: 'leave',
            date_range: true,
            leave: true,
            types: true,
            user: true
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyLeaveListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    MyLeaveListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    MyLeaveListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    MyLeaveListComponent.prototype.getStatus = function () {
        var _this = this;
        this._common_service.getStatusType().
            subscribe(function (data) {
            _this.sta = Array.from(data);
            _this.status = _this.sta;
            _this.status_value = [];
            _this.options = {
                multiple: true
            };
            _this.status_current = _this.status_value;
        }, function (err) { return console.error(err); });
    };
    MyLeaveListComponent.prototype.changedStatus = function (data) {
        this.status_current = data.value;
        if (this.status_current == 0) {
            this.filterForm.value.status_id = null;
        }
        else {
            this.filterForm.value.status_id = this.status_current;
        }
        this.getData();
    };
    MyLeaveListComponent.prototype.getType = function () {
        var _this = this;
        this._common_service.getLeaveType()
            .subscribe(function (data) {
            _this.type = Array.from(data);
            _this.type_value = [];
            _this.options = {
                multiple: true
            };
            _this.type_current = _this.type_value;
        }, function (err) { return console.error(err); });
    };
    MyLeaveListComponent.prototype.changedType = function (data) {
        this.type_current = data.value;
        if (this.type_current == 0) {
            this.filterForm.value.type_id = null;
        }
        else {
            this.filterForm.value.type_id = this.type_current;
        }
        this.getData();
    };
    MyLeaveListComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.getData();
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], MyLeaveListComponent.prototype, "dtElement", void 0);
    MyLeaveListComponent = __decorate([
        Component({
            selector: 'app-my-leave-list',
            templateUrl: './my-leave-list.component.html',
            styleUrls: ['./my-leave-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [LeaveService,
            DialogService,
            Configuration,
            AuthUserService,
            CommonService,
            FormBuilder,
            DaterangepickerConfig])
    ], MyLeaveListComponent);
    return MyLeaveListComponent;
}());
export { MyLeaveListComponent };
//# sourceMappingURL=my-leave-list.component.js.map