import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { LeaveService } from '../../../services/leave.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { Configuration } from '../../../app.config';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { LeaveModalComponent } from '../../leave/leave-modal/leave-modal.component';
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { ActionCenterModalComponent } from '../../action-center/action-center-modal/action-center-modal.component';
import { CommonService } from '../../../services/common.service';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng2-select2';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';

@Component({
  selector: 'app-my-leave-list',
  templateUrl: './my-leave-list.component.html',
  styleUrls: ['./my-leave-list.component.css']
})

@Injectable()
export class MyLeaveListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	public leave_id: any;
	user_id:any;
	leave=[];
	employee_id:any;
	supervisor_id:any
	buttonVal = false;

	public mainInput = {
        start: moment().subtract(12, 'month'),
        end: moment().subtract(11, 'month')
    }
    filterForm:any;
    public options: Select2Options;

	public status : Array<Select2OptionData>;
	public status_value: any;
	public status_current: any;
	sta:any;

	public type: any;
	public type_current : any;
	type_value: Array<Select2OptionData>;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor(
		private _leaservice: LeaveService, 
		private modalService: DialogService,
		private _conf: Configuration,
		private _auth_service: AuthUserService,
		private _common_service: CommonService,
		private _fb: FormBuilder,
   		private daterangepickerOptions: DaterangepickerConfig
		){

		this.filterForm = _fb.group({
			'status_id': 		[null],
			'start_date': 		[null],
			'end_date': 		[null],
			'type_id': 			[null]
		});

		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	}

	ngOnInit() {
		this.dateOption();
		this.getListByIdLimited();
		this.getStatus();
		this.getType();
		this.getData();
	}

	getListByIdLimited(){
		this._auth_service.getListByIdLimited().
		subscribe(
			data => {
				let user = data;
				this.employee_id = user.employee_id;
				this.buttonVal = true;
			},
			err => console.error(err)
		);
	}

	dateOption(){
    	this.daterangepickerOptions.settings = {
        	singleDatePicker: false
		};
    }

	getData(){

		let model = this.filterForm.value;
		this._leaservice.getMyleave(model).
		subscribe(
			data => {	
				this.leave  = data;
				this.rerender();
			},
			err => console.error(err)
		);
	}
	

	editLeave(id:any) {
		let disposable = this.modalService.addDialog(LeaveModalComponent, {
            title:'Update Leave',
            button:'Update',
            edit:true,
            leave_id:id,
            user:true,
            employee_id:this.employee_id
        	}).subscribe((isConfirmed)=>{
                this.getData();
                this.dateOption();
            });
	}

	createLeave() {

		let disposable = this.modalService.addDialog(LeaveModalComponent, {
            title:'Create Leave',
            button:'Add',
            create:true,
            employee_id:this.employee_id,
            user:true
        	}).subscribe((isConfirmed)=>{
                this.getData();
                this.dateOption();
            });
	}

	cancelLeave(id:any){

		let disposable = this.modalService.addDialog(RemarksModalComponent, {
			title:'Cancel Leave',
			id:id,
			url:'leave',
			button:'Ok'
		}).subscribe((isConfirmed)=>{
			this.getData();
			this.dateOption();

		});

	}

	viewDetails(id:any) {

		let disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title:'Leave',
            id:id,
            url:'leave',
            date_range:true,
            leave:true,
            types:true,
            user:true
        	}).subscribe((isConfirmed)=>{
                this.getData();
                this.dateOption();

            });
	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    getStatus(){
		this._common_service.getStatusType().
		subscribe(
			data => {
				this.sta = Array.from(data);
				this.status = this.sta;
				this.status_value = [];
				this.options = {
					multiple: true
				}
				this.status_current = this.status_value;
			},
			err => console.error(err)
		);
	}
	changedStatus(data: any) {
		this.status_current = data.value;

		if(this.status_current == 0){
			this.filterForm.value.status_id = null;
		}
		else{
			this.filterForm.value.status_id = this.status_current;
		}
			this.getData();
	}

	getType(){
	      this._common_service.getLeaveType()
	      .subscribe(
	        data => {
	        this.type = Array.from(data);
	        this.type_value = [];
	        this.options = {
				multiple: true
			}
	        this.type_current = this.type_value;
	        
	        },
	        err => console.error(err)
	    );
	}
	changedType(data: any) {

		this.type_current = data.value;

		if(this.type_current == 0){
			this.filterForm.value.type_id = null;
		}
		else{
			this.filterForm.value.type_id = this.type_current;
		}
			this.getData();
	} 

	filterDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;

        let start_date = moment(dateInput.start).format("YYYY-MM-DD");
        let end_date = moment(dateInput.end).format("YYYY-MM-DD");

        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;

        this.getData();

    }

}
