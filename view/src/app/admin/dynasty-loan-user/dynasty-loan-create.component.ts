import { Router } from '@angular/router';
import { Component, OnInit, Injectable, NgZone } from '@angular/core';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';

import { NgUploaderOptions, UploadedFile, UploadRejected } from 'ngx-uploader';
import { Subject } from 'rxjs/Rx';
import { Configuration } from '../../app.config';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { CommonService } from '../../services/common.service';

@Component({
	selector: 'dynasty-loan-create',
	templateUrl: './dynasty-loan-create.component.html',
	styleUrls: ['./dynasty-loan-create.component.css']
})

@Injectable()
export class DynastyLoanCreateComponent implements OnInit {
	api_employee: String;
	private post_data: any;
	private route_upload_attachment = 'upload?type=employee_mass_upload';
	private error_title: any;
	private form_companies: any;
	private success_title: any;
	private error_message: any;

	private message_title: any;
	private message_success: any;
	private message_error: any;
	
	private form_employee:any;
	private success_message: any;

	public response: any;
	public response_csv: any;
	public options: NgUploaderOptions;
	public sizeLimit: number = 1000000;
	public hasBaseDropZoneOver: boolean;
	public optionsCsv: NgUploaderOptions;

	public residence_ownership_selection = [
		{ value: 'owned', text: 'Owned' },
		{ value: 'rented', text: 'Rented' }
	];

	public employee_response_upload = {
		'firstname': null,
		'middlename': null,
		'lastname': null,
		'current_address': null,
		'provincial_address': null
	};

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	createDynastyLoan: FormGroup;

	constructor(private _commonserv: CommonService, private _company_service: CompanyService, private _emp_service: EmployeeService, private _route: Router, private _fb: FormBuilder, private _conf: Configuration, private _zone: NgZone){
		this.getCompanies();
		
		//add the the body classes
		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");
		
		this.createDynastyLoan = _fb.group({
	    	'employer': ['', [Validators.required]],
	    	'employee': ['', [Validators.required]],
	    	'no_of_dependents': ['', [Validators.required]],
	    	'residence_ownership_status': ['', [Validators.required]],
	    	'spouse_name': ['', [Validators.required]],
	    	'spouse_employer_name': ['', [Validators.required]],
	    	'spouse_address': ['', [Validators.required]],
	    	'reference_name': ['', [Validators.required]],
	    	'reference_contact': ['', [Validators.required]],
			'reference_address': ['', [Validators.required]],

			'loan_amount': ['', [Validators.required]],
			'loan_term_months': ['', [Validators.required]],
			'loan_purpose_of_loan': ['', [Validators.required]],
			'loan_type_of_loan': ['', [Validators.required]],
			'loan_payback_mode': ['', [Validators.required]],

			'comaker_type_of_loan': ['', [Validators.required]],
			'comaker_number': ['', [Validators.required]],
			'comaker_address': ['', [Validators.required]],
			'comaker_job_position': ['', [Validators.required]]
	    });
	}

	ngOnInit() {
	}

	ngAfterViewInit() {
	}

	public validateCreation(): void {
		let application_form = this.createDynastyLoan.value;

		this.message_title = '';
        this.message_success = '';
        this.message_error = '';

        console.log(application_form);

        // this._company_service.updateCompanyPolicy(application_form).subscribe(
        //     data => {
        //         this.fetchUpdateReponse(data);
                
        //     },
        //     err => {
        //         this.message_title = 'Invalid Entry';
        //         this.message_error = 'Please check the following fields if the entries have filled.';
        //     }
        // );
	}

	getEmployeesByCompany(company_id: any){
		let uri = 'company_id=' + company_id;

		this._emp_service.getEmployees(uri).subscribe(
			data => {
				this.setEmployeeSelection(data);
			},
			err => this.catchError(err)
		);

	}

	private getEmployees(){
		// getemployees
		this._emp_service.getEmployees().subscribe(
			data => {
				this.setEmployeeSelection(data);
			},
			err => this.catchError(err)
		);
	}

	private setEmployeeSelection(data: any){
		// setSuperVisors
		this.form_employee = data;

		let employees = data;
		let fetched_employees = [];

		for(var _i = 0; _i < employees.length; _i++){
			fetched_employees[_i] = { id: employees[_i].id, name: employees[_i].lastname + ', ' + employees[_i].firstname + ' - Employee Code: ' + employees[_i].employee_code };
		}

		this.form_employee = fetched_employees;

	}

	private getEmployee(employee_id: any){
		this._emp_service.getEmployee(employee_id).subscribe(
			data => {
				this.setEmployeeData(data);
			},
			err => console.error(err)
		);
	}

	private setEmployeeData(data: any){
		console.log('employee data');
		console.log(data);
		this.employee_response_upload = data;
	}

	private getCompanies(){
		this._emp_service.getCompanies().subscribe(
			data => {
				this.setCompanies(data);
			},
			err => this.catchError(err)
		);
	}

	private setCompanies(data: any){
		let companies = data;
		let fetched_companies = [];

		for(var _i = 0; _i < companies.length; _i++){
			fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
		}

		this.form_companies = fetched_companies;
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
			window.scrollTo(0, 0);
		}
	}
}
