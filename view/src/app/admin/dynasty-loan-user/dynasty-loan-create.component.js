var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Router } from '@angular/router';
import { Component, Injectable, NgZone } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Configuration } from '../../app.config';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { CommonService } from '../../services/common.service';
var DynastyLoanCreateComponent = /** @class */ (function () {
    function DynastyLoanCreateComponent(_commonserv, _company_service, _emp_service, _route, _fb, _conf, _zone) {
        this._commonserv = _commonserv;
        this._company_service = _company_service;
        this._emp_service = _emp_service;
        this._route = _route;
        this._fb = _fb;
        this._conf = _conf;
        this._zone = _zone;
        this.route_upload_attachment = 'upload?type=employee_mass_upload';
        this.sizeLimit = 1000000;
        this.residence_ownership_selection = [
            { value: 'owned', text: 'Owned' },
            { value: 'rented', text: 'Rented' }
        ];
        this.employee_response_upload = {
            'firstname': null,
            'middlename': null,
            'lastname': null,
            'current_address': null,
            'provincial_address': null
        };
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.getCompanies();
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.createDynastyLoan = _fb.group({
            'employer': ['', [Validators.required]],
            'employee': ['', [Validators.required]],
            'no_of_dependents': ['', [Validators.required]],
            'residence_ownership_status': ['', [Validators.required]],
            'spouse_name': ['', [Validators.required]],
            'spouse_employer_name': ['', [Validators.required]],
            'spouse_address': ['', [Validators.required]],
            'reference_name': ['', [Validators.required]],
            'reference_contact': ['', [Validators.required]],
            'reference_address': ['', [Validators.required]],
            'loan_amount': ['', [Validators.required]],
            'loan_term_months': ['', [Validators.required]],
            'loan_purpose_of_loan': ['', [Validators.required]],
            'loan_type_of_loan': ['', [Validators.required]],
            'loan_payback_mode': ['', [Validators.required]],
            'comaker_type_of_loan': ['', [Validators.required]],
            'comaker_number': ['', [Validators.required]],
            'comaker_address': ['', [Validators.required]],
            'comaker_job_position': ['', [Validators.required]]
        });
    }
    DynastyLoanCreateComponent.prototype.ngOnInit = function () {
    };
    DynastyLoanCreateComponent.prototype.ngAfterViewInit = function () {
    };
    DynastyLoanCreateComponent.prototype.validateCreation = function () {
        var application_form = this.createDynastyLoan.value;
        this.message_title = '';
        this.message_success = '';
        this.message_error = '';
        console.log(application_form);
        // this._company_service.updateCompanyPolicy(application_form).subscribe(
        //     data => {
        //         this.fetchUpdateReponse(data);
        //     },
        //     err => {
        //         this.message_title = 'Invalid Entry';
        //         this.message_error = 'Please check the following fields if the entries have filled.';
        //     }
        // );
    };
    DynastyLoanCreateComponent.prototype.getEmployeesByCompany = function (company_id) {
        var _this = this;
        var uri = 'company_id=' + company_id;
        this._emp_service.getEmployees(uri).subscribe(function (data) {
            _this.setEmployeeSelection(data);
        }, function (err) { return _this.catchError(err); });
    };
    DynastyLoanCreateComponent.prototype.getEmployees = function () {
        var _this = this;
        // getemployees
        this._emp_service.getEmployees().subscribe(function (data) {
            _this.setEmployeeSelection(data);
        }, function (err) { return _this.catchError(err); });
    };
    DynastyLoanCreateComponent.prototype.setEmployeeSelection = function (data) {
        // setSuperVisors
        this.form_employee = data;
        var employees = data;
        var fetched_employees = [];
        for (var _i = 0; _i < employees.length; _i++) {
            fetched_employees[_i] = { id: employees[_i].id, name: employees[_i].lastname + ', ' + employees[_i].firstname + ' - Employee Code: ' + employees[_i].employee_code };
        }
        this.form_employee = fetched_employees;
    };
    DynastyLoanCreateComponent.prototype.getEmployee = function (employee_id) {
        var _this = this;
        this._emp_service.getEmployee(employee_id).subscribe(function (data) {
            _this.setEmployeeData(data);
        }, function (err) { return console.error(err); });
    };
    DynastyLoanCreateComponent.prototype.setEmployeeData = function (data) {
        console.log('employee data');
        console.log(data);
        this.employee_response_upload = data;
    };
    DynastyLoanCreateComponent.prototype.getCompanies = function () {
        var _this = this;
        this._emp_service.getCompanies().subscribe(function (data) {
            _this.setCompanies(data);
        }, function (err) { return _this.catchError(err); });
    };
    DynastyLoanCreateComponent.prototype.setCompanies = function (data) {
        var companies = data;
        var fetched_companies = [];
        for (var _i = 0; _i < companies.length; _i++) {
            fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
        }
        this.form_companies = fetched_companies;
    };
    DynastyLoanCreateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
            window.scrollTo(0, 0);
        }
    };
    DynastyLoanCreateComponent = __decorate([
        Component({
            selector: 'dynasty-loan-create',
            templateUrl: './dynasty-loan-create.component.html',
            styleUrls: ['./dynasty-loan-create.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [CommonService, CompanyService, EmployeeService, Router, FormBuilder, Configuration, NgZone])
    ], DynastyLoanCreateComponent);
    return DynastyLoanCreateComponent;
}());
export { DynastyLoanCreateComponent };
//# sourceMappingURL=dynasty-loan-create.component.js.map