var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { DialogService } from "ng2-bootstrap-modal";
import { Configuration } from '../../../app.config';
import { ScheduleAdjustmentsService } from '../../../services/schedule-adjustments.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { ScheduleAdjustmentsModalComponent } from '../../schedule-adjustments/schedule-adjustments-modal/schedule-adjustments-modal.component';
import { ActionCenterModalComponent } from '../../action-center/action-center-modal/action-center-modal.component';
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
import { FormBuilder } from '@angular/forms';
import { CommonService } from '../../../services/common.service';
import * as moment from 'moment';
var MyScheduleAdjustmentsListComponent = /** @class */ (function () {
    function MyScheduleAdjustmentsListComponent(_conf, modalService, _auth_service, _SAservice, _common_service, _fb, daterangepickerOptions) {
        this._conf = _conf;
        this.modalService = modalService;
        this._auth_service = _auth_service;
        this._SAservice = _SAservice;
        this._common_service = _common_service;
        this._fb = _fb;
        this.daterangepickerOptions = daterangepickerOptions;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.sa = [];
        this.user_is_admin = 0;
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.buttonVal = false;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filterForm = _fb.group({
            'status_id': [null],
            'start_date': [null],
            'end_date': [null]
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    MyScheduleAdjustmentsListComponent.prototype.ngOnInit = function () {
        this.dateOption();
        this.getListByIdLimited();
        this.getStatus();
        this.getData();
    };
    MyScheduleAdjustmentsListComponent.prototype.getListByIdLimited = function () {
        var _this = this;
        this._auth_service.getListByIdLimited().
            subscribe(function (data) {
            var user = data;
            _this.employee_id = user.employee_id;
            _this.buttonVal = true;
        }, function (err) { return console.error(err); });
    };
    MyScheduleAdjustmentsListComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    MyScheduleAdjustmentsListComponent.prototype.getData = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._SAservice.getMySA(model).
            subscribe(function (data) {
            _this.sa = data;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    MyScheduleAdjustmentsListComponent.prototype.createSA = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ScheduleAdjustmentsModalComponent, {
            title: 'Create Schedule Adjustments',
            create: true,
            button: 'Add',
            user_is_admin: this.user_is_admin,
            employee_id: this.employee_id,
            user: true
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyScheduleAdjustmentsListComponent.prototype.editSA = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ScheduleAdjustmentsModalComponent, {
            title: 'Edit Schedule Adjustment',
            edit: true,
            button: 'Update',
            sa_id: id,
            user: true,
            user_is_admin: this.user_is_admin,
            employee_id: this.employee_id,
            user_id: this.user_id
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyScheduleAdjustmentsListComponent.prototype.cancelSA = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(RemarksModalComponent, {
            title: 'Cancel Schedule Adjustment',
            id: id,
            url: 'schedule_adjustments',
            button: 'Ok'
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyScheduleAdjustmentsListComponent.prototype.viewDetails = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title: 'Schedule Adjustment',
            id: id,
            url: 'schedule_adjustments',
            date_range: true,
            sa: true,
            user: true
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyScheduleAdjustmentsListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    MyScheduleAdjustmentsListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    MyScheduleAdjustmentsListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    MyScheduleAdjustmentsListComponent.prototype.getStatus = function () {
        var _this = this;
        this._common_service.getStatusType().
            subscribe(function (data) {
            _this.sta = Array.from(data);
            _this.status = _this.sta;
            _this.status_value = [];
            _this.options = {
                multiple: true
            };
            _this.status_current = _this.status_value;
        }, function (err) { return console.error(err); });
    };
    MyScheduleAdjustmentsListComponent.prototype.changedStatus = function (data) {
        this.status_current = data.value;
        if (this.status_current == 0) {
            this.filterForm.value.status_id = null;
        }
        else {
            this.filterForm.value.status_id = this.status_current;
        }
        this.getData();
    };
    MyScheduleAdjustmentsListComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.getData();
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], MyScheduleAdjustmentsListComponent.prototype, "dtElement", void 0);
    MyScheduleAdjustmentsListComponent = __decorate([
        Component({
            selector: 'app-my-schedule-adjustments-list',
            templateUrl: './my-schedule-adjustments-list.component.html',
            styleUrls: ['./my-schedule-adjustments-list.component.css']
        }),
        __metadata("design:paramtypes", [Configuration,
            DialogService,
            AuthUserService,
            ScheduleAdjustmentsService,
            CommonService,
            FormBuilder,
            DaterangepickerConfig])
    ], MyScheduleAdjustmentsListComponent);
    return MyScheduleAdjustmentsListComponent;
}());
export { MyScheduleAdjustmentsListComponent };
//# sourceMappingURL=my-schedule-adjustments-list.component.js.map