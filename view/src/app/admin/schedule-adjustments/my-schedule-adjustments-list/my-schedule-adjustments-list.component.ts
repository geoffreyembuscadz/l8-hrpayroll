import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { OfficialBusinessService } from '../../../services/official-business.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from '../../../app.config';
import { ScheduleAdjustmentsService } from '../../../services/schedule-adjustments.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { AuthUserService } from '../../../services/auth-user.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { ScheduleAdjustmentsModalComponent } from '../../schedule-adjustments/schedule-adjustments-modal/schedule-adjustments-modal.component';
import { ActionCenterModalComponent } from '../../action-center/action-center-modal/action-center-modal.component';
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../../../services/common.service';
import { MomentModule } from 'angular2-moment/moment.module';
import { Select2OptionData } from 'ng2-select2';
import * as moment from 'moment';

@Component({
  selector: 'app-my-schedule-adjustments-list',
  templateUrl: './my-schedule-adjustments-list.component.html',
  styleUrls: ['./my-schedule-adjustments-list.component.css']
})
export class MyScheduleAdjustmentsListComponent implements OnInit,AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	dtOptions: any = {};
	sa_id:any;
	sa=[];
	user_id:any;
	employee_id:any;
	supervisor_id:any;
	user_is_admin = 0;
	public options: Select2Options;

	public mainInput = {
        start: moment().subtract(12, 'month'),
        end: moment().subtract(11, 'month')
    }
    filterForm:any;

	public status : Array<Select2OptionData>;
	public status_value: Array<Select2OptionData>;
	public status_current: any;
	sta:any;

	buttonVal = false;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

  constructor(
  	private _conf: Configuration,
  	private modalService: DialogService,
  	private _auth_service: AuthUserService,
  	private _SAservice: ScheduleAdjustmentsService,
  	private _common_service: CommonService,
	private _fb: FormBuilder,
   	private daterangepickerOptions: DaterangepickerConfig
  	) { 

  	this.filterForm = _fb.group({
    	'status_id': 		[null],
		'start_date': 		[null],
		'end_date': 		[null]
   	});

  	this.body.classList.add("skin-blue");
	this.body.classList.add("sidebar-mini");

  }

  	ngOnInit() {
  		this.dateOption();
		this.getListByIdLimited();
		this.getStatus();
		this.getData();
	}

	getListByIdLimited(){
		this._auth_service.getListByIdLimited().
		subscribe(
			data => {
				let user = data;
				this.employee_id = user.employee_id;
				this.buttonVal = true;
			},
			err => console.error(err)
		);
	}

	dateOption(){
    	this.daterangepickerOptions.settings = {
        	singleDatePicker: false
		};
    }

	getData(){

		let model = this.filterForm.value;
		this._SAservice.getMySA(model).
		subscribe(
			data => {
				this.sa  = data;
				this.rerender();
			},
			err => console.error(err)
		);
	}

	createSA(){
		let disposable = this.modalService.addDialog(ScheduleAdjustmentsModalComponent, {
		    title:'Create Schedule Adjustments',
		    create:true,
		    button:'Add',
		    user_is_admin: this.user_is_admin,
		    employee_id:this.employee_id,
            user:true
			}).subscribe((isConfirmed)=>{
		        this.getData();
				this.dateOption();
		    });
	}

	editSA(id:any){

		let disposable = this.modalService.addDialog(ScheduleAdjustmentsModalComponent, {
            title:'Edit Schedule Adjustment',
            edit:true,
            button:'Update',
            sa_id:id,
            user:true,
            user_is_admin: this.user_is_admin,
            employee_id:this.employee_id,
            user_id:this.user_id
        	}).subscribe((isConfirmed)=>{
                this.getData();
				this.dateOption();
        });

	}

	cancelSA(id:any){

		let disposable = this.modalService.addDialog(RemarksModalComponent, {
            title:'Cancel Schedule Adjustment',
            id:id,
            url:'schedule_adjustments',
            button:'Ok'
        	}).subscribe((isConfirmed)=>{
                this.getData();
				this.dateOption();
            });

	}

	viewDetails(id:any) {

		let disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title:'Schedule Adjustment',
            id:id,
            url:'schedule_adjustments',
            date_range:true,
            sa:true,
            user:true
        	}).subscribe((isConfirmed)=>{
                this.getData();
				this.dateOption();
            });
	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	    
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    getStatus(){
		this._common_service.getStatusType().
		subscribe(
			data => {
				this.sta = Array.from(data);
				this.status = this.sta;
				this.status_value = [];
				this.options = {
					multiple: true
				}
				this.status_current = this.status_value;
			},
			err => console.error(err)
		);
	}
	changedStatus(data:any) {
		this.status_current = data.value;

		if(this.status_current == 0){
			this.filterForm.value.status_id = null;
		}
		else{
			this.filterForm.value.status_id = this.status_current;
		}
			this.getData();
	}

	filterDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;

        let start_date = moment(dateInput.start).format("YYYY-MM-DD");
        let end_date = moment(dateInput.end).format("YYYY-MM-DD");

        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;

        this.getData();

    }

}
