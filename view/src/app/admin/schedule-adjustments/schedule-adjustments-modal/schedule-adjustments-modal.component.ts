import { Component, ViewChild, ElementRef } from '@angular/core';
import { Configuration } from '../../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ScheduleAdjustmentsService } from '../../../services/schedule-adjustments.service';
import { CommonService } from '../../../services/common.service';
import { ScheduleAdjustments } from '../../../model/schedule-adjustment';
import { Select2OptionData } from 'ng2-select2';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import { MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TimepickerModule } from 'ngx-bootstrap';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';

export interface ScheduleAdjustmentsModalModel {
  sa_id:string;
}

@Component({
  selector: 'app-schedule-adjustments-modal',
  templateUrl: './schedule-adjustments-modal.component.html',
  styleUrls: ['./schedule-adjustments-modal.component.css']
})

export class ScheduleAdjustmentsModalComponent extends DialogComponent<null, boolean> implements ScheduleAdjustmentsModalModel {

	// sa_ID
	@ViewChild('sa_ID') sa_ID: ElementRef;
	// Employee ID - For Creating in My Schedule Adjustment & Editing My Schedule Adjustment
	@ViewChild('emp_ID') emp_ID: ElementRef;

  	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public companies: any;
	public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public options: Select2Options;
	public current: any;
	public type : any;
	public type_value : any;
	public start_date:any;
	public end_date:any;
  public time_hours: any;
  public time_minutes: any;
	public start_time:Date = new Date("2017-08-08 08:00:00");
	public end_time:Date = new Date("2017-08-08 17:00:00");
	sa = new ScheduleAdjustments();
	SAForm:any;
	emp:any;
	poststore:any;
	user_id:any;
	public mainInput = {
        start: moment(),
        end: moment().add(1,'days')
    }
  public sa_modal_company_id: any;
	public ismeridian:boolean = true;
	validate = false;
    employee_id:any;
    sa_id:any;
    sa_data:any;
    sa_data_update: any;
    SARFormUpdate: any;
    remarks:any;
    edit:any;
    create:any;
    multi:any;
    single:any;
    dataStorage=[];
    break_start:Date = new Date("2017-08-08 12:00:00");
    break_end:Date = new Date("2017-08-08 13:00:00");
    lunch_mins_break: any;
    tableVal = false;
    empVal = false;
    user = false;
    admin = false;
    dupVal=false;
    supervisor_id:any;
    user_is_admin = 1;
    role_id:any;
    messenger_id:any;
    error_saform_company: any = '';
    innerHTML: any = '';

    public ws_current : any;
	ws_value: Array<Select2OptionData>;
	public work_sched=[];
	value:any;

    socket: SocketIOClient.Socket;
    host:any;

  constructor(
  	private _common_service: CommonService,
   	private _SAservice: ScheduleAdjustmentsService,
   	dialogService: DialogService,
   	private _fb: FormBuilder,
   	private _ar: ActivatedRoute,
   	private daterangepickerOptions: DaterangepickerConfig,
   	private _auth_service: AuthUserService,
   	private _conf: Configuration){
  	super(dialogService);
  	this.host = this._conf.socketUrl;

    this.SAForm = _fb.group({
		'start_time': 		[this.sa.start_time, [Validators.required]],
		'end_time': 		[this.sa.end_time, [Validators.required]],
		'break_start': 		[null, [Validators.required]],
		'lunch_mins_break':	[null, [Validators.required]],
		'remarks': 			[(this.sa.remarks) ? this.sa.remarks : 'Input Reason here', [Validators.required]],
		'status_id':		[null]
    });

    this.sa_data_update = {
    	'id': null,
    	'date': null,
    	'end_time': null,
    	'start_time': null,
    	'break_start': null,
    	'employee_id': null,
    	'company_id': null,
    	'lunch_mins_break': null
    };

    this.SARFormUpdate = _fb.group({
    	'id': [this.sa_id, []],
    	'employee_id': [this.sa_data_update.employee_id, [Validators.required]],
    	'company_id': [this.sa_data_update.company_id, [Validators.required]],
    	'start_time': 		[this.sa_data_update.start_time, [Validators.required]],
		'end_time': 		[this.sa_data_update.end_time, [Validators.required]],
		'break_start': 		[this.sa_data_update.break_start, [Validators.required]],
		'lunch_mins_break':	[this.sa_data_update.lunch_mins_break, [Validators.required]],
		'date':	[this.sa_data_update.date, [Validators.required]]
    });

    this.sa_modal_company_id = null;
    this.daterangepickerOptions.settings = {};
	this.time_hours = this._common_service.getTimeHours();
    this.time_minutes = this._common_service.getTimeMins();
  }

	ngOnInit() {

    	if(this.user == true){
    		this.empVal = true;
    		this.getCompanies();
    		this.SAForm.addControl('emp_id', new FormControl());
    		this.get_employee();
    	}
		if(this.edit == true){
			this.getSA();
			this.empVal = true;
    		this.validate = true;
    		this.getCompanies();
    		this.get_employee();
    	}
    	else{
    		this.getCompanies();
	   		this.get_employee();
    	}
	   		//uncomment this for socket.io
    		// this.connectToServer();
 	}

 	ngAfterViewInit(){
 	}

 	updateSARdata(){
 		this.error_title = '';
		this.error_message = '';
		this.success_title = '';
 		this.success_message = '';
 		this.sa_data_update = this.SARFormUpdate;

 		if(this.sa_data_update.valid){
 			this._SAservice.updateSAR(this.sa_id, this.sa_data_update.value).subscribe(
 				data => {
 					console.log('updated');
 					this.success_title = 'Success!';
 					this.success_message = 'Record has been successfully updated!';
 				}
 			);
 		} else {
 			this.error_title = 'Oops!';
			this.error_message = 'Please Check your input if its valid before submitting.';
 		}
 	}

 	changeSARdataUpdate(select_value: any, index: any){
 		let input_val = select_value.target.value;
 	}

	getSA(){
		console.log(this.sa_id);
		console.log(this.SARFormUpdate);
		// Overwritten Sourcode
		if(this.sa_id){
			this._SAservice.getSArequest(this.sa_id).subscribe(
				data => {
					this.sa_data_update = data;
					this.SARFormUpdate.setValue({
						'id': data.id,
				    	'employee_id': data.employee_id,
				    	'company_id': data.company_id, 
				    	'start_time': 	data.start_time, 
						'end_time': 		data.end_time,
						'break_start': 		data.break_start,
						'lunch_mins_break':	data.lunch_mins_break,
						'date':	data.date
					});
					console.log(this.SARFormUpdate);
				}

			);
		}

		// Jenny's Sourcecode Do Not Use this!
		// this._SAservice.getSA(this.sa_id).subscribe(
	 //      data => {
	 //        this.sa_data=(data);

	 //        this.mainInput.start = this.sa_data[0].start_date;
	 //        this.mainInput.end = this.sa_data[0].end_date;
	 //        this.start_date = this.sa_data[0].start_date;
	 //        this.end_date = this.sa_data[0].end_date;
	 //        this.remarks = this.sa_data[0].remarks;
	 //        this.employee_id= this.sa_data[0].emp_id;

	 //        let start = this.sa_data[0].start_date + " " + this.sa_data[0].start_time;
	 //        let end = this.sa_data[0].end_date + " " + this.sa_data[0].end_time;
	 //        let b_start = this.sa_data[0].start_date + " " + this.sa_data[0].break_start;
	 //        let b_end = this.sa_data[0].end_date + " " + this.sa_data[0].break_end;

	 //        this.start_time = new Date(start);
	 //        this.end_time = new Date(end);
	 //        this.break_start = new Date(b_start);
	 //        this.break_end = new Date(b_end);
  //         this.lunch_mins_break = this.sa_data[0].lunch_mins_break;
  //         /*
	 //        this.daterangepickerOptions.settings.startDate = new Date(this.sa_data[0].start_date);
	 //        this.daterangepickerOptions.settings.endDate = new Date(this.sa_data[0].end_date); */

	 //        let len = this.sa_data.length;
	 //        let i = 1;
	 //        for (let x = 0; x < len; x++) {

		// 		let date = this.sa_data[x].date;
		// 		let start_time = this.sa_data[x].start_time;
		// 		let end_time =  this.sa_data[x].end_time;
		// 		let break_start = this.sa_data[x].break_start;
		// 		let break_end = this.sa_data[x].break_end;
		// 		let request_id = this.sa_data[x].request_id;
		// 		let day = moment(this.sa_data[x].date).format("dddd");

		// 		this.dataStorage.push({i,day,date,start_time,end_time,break_start,break_end,request_id});
		// 		i++;

	 //        }

	 //        this.tableVal = true;
	 //      },
	 //      err => console.error(err)
  //   	);

	}

	private getCompanies(){
		this._common_service.getCompany().subscribe(
			data => {
				this.setCompanies(data);
			},
			err => this.catchError(err)
		);
	}

	private setCompanies(data: any){
		let companies = data;
		let fetched_companies = [];

		for(var _i = 0; _i < companies.length; _i++){
			fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].text };
		}

		this.companies = fetched_companies;
	}

	private getEmployeesByCompany(company_id: any){
    this.sa_modal_company_id = company_id;
		this.getEmployees(company_id);
	}

	private getEmployees(company_id: any){
		this.error_saform_company = '';
		////
		this._common_service.getEmployee().subscribe(
			data => {

				this.employee = this.emp;
				this.employee_value = [];
				if(this.edit == true) {
					this.employee_value = this.employee_id;
					this.validate = true;
		        }
				this.options = {
					multiple: true
				}
				this.current = this.employee_value;
				this.employee = data;

				// if(data.length == 0){
				// 	this.error_saform_company = 'The Selected Company does not have any employee records.';

				// }

				// this.work_sched = data;
				// this.options = {
				// 	dropdownAutoWidth: true
				// }

				// this.ws_value = [];
				// this.ws_current = this.ws_value;

				// let id = 0;
				// let text = 'Suggested Schedule';

				// this.work_sched.unshift({id,text});

				// this.ws_value = this.value;
			},
			err => this.catchError(err)
		);
	}

	get_employee(){
		
		this.employee = this.emp;
		this.employee_value = [];
		if(this.edit == true) {
			this.employee_value = this.employee_id;
			this.validate = true;
        }
		this.options = {
			multiple: true
		}
		this.current = this.employee_value;
		
	}

	changed(data: any) {

		if(this.single == true){
			let v = [];
			v.push(data.value);
			this.current = v;
		}
		else{
			this.current = data.value;
		}

		let len = this.current.length;

		if(len >= 1){
			this.empVal = true;
		}
		else{
			this.empVal = false;
		}

		this.validation();
		this.duplicateEntryValidation();
	}

	validation(){

		if(this.empVal == true && this.tableVal==true && this.dupVal == true){
			this.validate = true;
		}
		else{
			this.validate = false;
		}
	}


	submit(){
		this.validate = false;
		this.error_title = '';
		this.error_message = '';
		this.duplicateEntryValidation();

		let sa = this.SAForm.value;
		let i = sa.details.length -1;
		sa.start_date = sa.details[0].date;
		sa.end_date = sa.details[i].date;

		if(this.edit == true && this.user == false){

			this.SAForm.value.id= this.sa_id;
			sa = this.SAForm.value;

			this._SAservice.updateSA(sa).subscribe(
				data => {
					this.poststore = data;

					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}
		else if(this.edit == true && this.user == true){

			this.SAForm.value.id= this.sa_id;
			sa = this.SAForm.value;

			this._SAservice
			.updateMySA(sa)
			.subscribe(
				data => {
					this.poststore = data;

					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}


		if(this.create == true && this.user == false){
			this._SAservice
			.createSA(sa)
			.subscribe(
				data => {
					this.poststore = data;

					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}
		else if(this.create == true && this.user == true){
			let user_EMPLOYEE_ID = (this.emp_ID.nativeElement.innerHTML != 'N/A') ? this.emp_ID.nativeElement.innerHTML : 0; // This is applicable when user is an employee or account is employee level

			sa.emp_id = user_EMPLOYEE_ID;
			sa.employee_id = [user_EMPLOYEE_ID]; 

			this._SAservice
			.createMySA(sa)
			.subscribe(
				data => {
					this.poststore = data;

					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}
	}

	connectToServer(){
	    let socketUrl = this.host;
	    this.socket = io.connect(socketUrl);
	    this.socket.on("connect", () => this.connect());
	    this.socket.on("disconnect", () => this.disconnect());

	    this.socket.on("error", (error: string) => {
	      console.log(`ERROR: "${error}" (${socketUrl})`);
	    });


	  }

	// Handle connection opening
	private connect() {
	    // Request initial list when connected
	    this.socket.emit("join", 'Client User -> ');
	}
	// Handle connection closing
	private disconnect() {
	    console.log(`Disconnected from "${this.host}"`);
	}

	errorCatch(){
		if(this.poststore.status_code == 200){
			this.success_title = 'Success';
			this.success_message = 'No Changes Happen';
			setTimeout(() => {
	  		   this.close();
	      	}, 1000);
		}
		else if(this.poststore.status_code == 400){

			this.error_title = 'Warning';
			this.error_message = 'Request is already exist';
			let temp;
			setTimeout(() => {
				let disposable = this.dialogService.addDialog(ValidationModalComponent, {
				title:'Warning',
				message:'Schedule Adjustment',
				user_is_admin: this.user_is_admin,
				url:'schedule_adjustments',
				data:this.poststore.data,
				userDup:this.user,
				adminDup:this.admin
				}).subscribe((message)=>{
					if (message == true) {
						this.error_title = '';
						this.error_message = '';

						setTimeout(() => {
				  		   this.close();
				      	}, 1000);
					}
					else if(message != false){
    					temp = message;

    					for (let x = 0; x < temp.length; ++x) {
							let index = this.current.indexOf(temp[x]);
	    					this.current.splice(index, 1);
    					}

	    				this.employee_value = this.current;

    					if(this.current.length == 0){
	    					this.error_title = '';
							this.error_message = '';
							setTimeout(() => {
					  		   this.close();
					      	}, 1000);
    					}
					}
				});
	      	}, 1000);

		}
		else{
			// socket.io emit
			// this.socket.emit('notify', this.poststore);
			this.validate = false;
			this.success_title = "Success";
			this.success_message = "Successfully";
			setTimeout(() => {
	  		   this.close();
	      	}, 2000);
		}
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation. Be sure that the required fields are not empty.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	private selectedDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_date = moment(dateInput.start).format("YYYY-MM-DD");
       	this.end_date = moment(dateInput.end).format("YYYY-MM-DD");

       	this.computeDays();
    }

	//time picker option
	public toggleMode():void {
		this.ismeridian = !this.ismeridian;

	}

	computeDays(){

		this.tableVal = true;
		// this.getWorkingSchedule();
		this.getSuggestedWorkingSchedule();

		if(this.dataStorage != null){
			this.dataStorage = [];
		}

		if(this.start_date == null && this.end_date==null){
			this.start_date = this.mainInput.start;
			this.end_date = this.mainInput.end;
		}

		let start = this.start_date;
		let end = this.end_date;
		let from = moment(this.start_date, 'YYYY-MM-DD'); // format in which you have the date
		let to = moment(this.end_date, 'YYYY-MM-DD');     // format in which you have the date
		let i = 0;
		let start_time = moment(this.start_time).format("HH:mm");
		let end_time =  moment(this.end_time).format("HH:mm");
		let break_start =  moment(this.break_start).format("HH:mm");
		let break_end =  moment(this.break_end).format("HH:mm");
		let lunch_mins_break = (this.lunch_mins_break != null) ? this.lunch_mins_break : 60;
		let request_id = 0;


		/* using diff */
	    let currDate = from.clone().startOf('day');
	    let lastDate = to.clone().startOf('day');

	    let date = moment(currDate.toDate()).format("YYYY-MM-DD");
	    let day = moment(date).format("dddd");

	   	this.dataStorage.push({i,day,date,start_time,end_time,break_start,lunch_mins_break,break_end,request_id});

	    while(currDate.add(1 ,'days').diff(lastDate) < 0) {
	    	let date = moment(currDate.clone().toDate()).format("YYYY-MM-DD");
	    	let day = moment(date).format("dddd");
	        this.dataStorage.push({i,day,date,start_time,end_time,break_start,lunch_mins_break,break_end,request_id});
	    }

	    if(start != end)  {
	    	let date = moment(lastDate.toDate()).format("YYYY-MM-DD");
	    	let day = moment(date).format("dddd");
	    	this.dataStorage.push({i,day,date,start_time,end_time,break_start,lunch_mins_break,break_end,request_id});
	    }

	    let len = this.dataStorage.length;

	    let t = 1;
	    for (let x = 0; x < len; ++x) {
	    	this.dataStorage[x].i = t;
	    	t++;
	    }
	    this.duplicateEntryValidation();
	}

	removeDay(id){
		this.dataStorage.splice(id, 1);

		if(this.dataStorage.length == 0){
			this.tableVal = false;
		}
	}

	duplicateEntryValidation(){
		this.SAForm.value.employee_id = this.current;
		this.SAForm.value.supervisor_id = this.supervisor_id;
		this.SAForm.value.start_date = moment(this.start_date).format("YYYY-MM-DD");
		this.SAForm.value.end_date = moment(this.end_date).format("YYYY-MM-DD");
		this.SAForm.value.created_by = this.user_id;
		this.SAForm.value.details = this.dataStorage;

		if(this.user == true  && this.create == true || this.user == true && this.edit == true){
			let v = [];
			v.push(this.employee_id);
			this.SAForm.value.employee_id = v;
		}

		if(this.edit == true){
			this.SAForm.value.edit = this.edit;
			this.SAForm.value.id= this.sa_id;
		}
		else{
			this.SAForm.value.edit = false;
		}

		let sa = this.SAForm.value;

		if(this.SAForm.value.employee_id != 0 && this.SAForm.value.details.length != 0 || this.edit == true){
			this._SAservice
				.duplicateEntryValidation(sa)
				.subscribe(
					data => {
						this.poststore = data;
						if(this.poststore.message == "duplicate"){
								this.dupVal = false;
								this.errorCatch();
						}
						else{
							this.dupVal = true;
							this.error_title = '';
							this.error_message = '';
						}

						this.validation();
					},
					err => this.catchError(err)
				);
		}
	}

	changeStartTimeHour(index_row: any, select_value: any){
		let start_time = this.dataStorage[index_row].start_time;
		let select_start_time_hour = select_value.target.value;
		let start_time_hour = start_time.split(':');
		let start_time_hour_a = start_time_hour[0];
		let start_time_hour_b = start_time_hour[1];

		let new_start_time = select_start_time_hour + ':' + start_time_hour_b;

		this.dataStorage[index_row].start_time = new_start_time;
	}

	changeStartTimeMin(index_row: any, select_value: any){
		let start_time = this.dataStorage[index_row].start_time;
		let select_start_time_min = select_value.target.value;
		let start_time_min = start_time.split(':');
		let start_time_min_a = start_time_min[0];
		let start_time_min_b = start_time_min[1];

		let new_start_time = start_time_min_a + ':' + select_start_time_min;

		this.dataStorage[index_row].start_time = new_start_time;
	}

	changeEndTimeHour(index_row: any, select_value: any){
		let end_time = this.dataStorage[index_row].end_time;
		let select_end_time_hour = select_value.target.value;
		let end_time_hour = end_time.split(':');
		let end_time_hour_a = end_time_hour[0];
		let end_time_hour_b = end_time_hour[1];

		let new_end_time = select_end_time_hour + ':' + end_time_hour_b;

		this.dataStorage[index_row].end_time = new_end_time;
	}

	changeEndTimeMin(index_row: any, select_value: any){
		let end_time = this.dataStorage[index_row].end_time;
		let select_end_time_min = select_value.target.value;
		let end_time_min = end_time.split(':');
		let end_time_min_a = end_time_min[0];
		let end_time_min_b = end_time_min[1];

		let new_end_time = end_time_min_a + ':' + select_end_time_min;

		this.dataStorage[index_row].end_time = new_end_time;
	}

	changeBreakTimeHour(index_row: any, select_value: any){
		let break_time = this.dataStorage[index_row].end_time;
		let select_break_time_hour = select_value.target.value;
		let break_time_hour = break_time.split(':');
		let break_time_hour_a = break_time_hour[0];
		let break_time_hour_b = break_time_hour[1];

		let new_break_time = select_break_time_hour + ':' + break_time_hour_b;

		this.dataStorage[index_row].break_start = new_break_time;
	}

	changeBreakTimeMin(index_row: any, select_value: any){
		let break_time = this.dataStorage[index_row].end_time;
		let select_break_time_min = select_value.target.value;
		let break_time_min = break_time.split(':');
		let break_time_min_a = break_time_min[0];
		let break_time_min_b = break_time_min[1];

		let new_break_time = break_time_min_a + ':' + select_break_time_min;

		this.dataStorage[index_row].break_start = new_break_time;
	}

	updateData(i,type,event){
		// let time = event.target.value;
		let time = moment("2017-11-29 " + event.target.value).format("HH:mm:ss");

		if (time != '') {
			if (type == 'in') {
				this.dataStorage[i].start_time = time;
			}
			else if(type == 'out'){
				this.dataStorage[i].end_time = time;
			}
			else if(type == 'sb'){
				this.dataStorage[i].break_start = time;
			}
			else if(type == 'eb'){
				this.dataStorage[i].break_end = time;
			} else if(type == 'bm'){
        		this.dataStorage[i].lunch_mins_break = 60;
      		}
		}
	}

	getWorkingSchedule(){
	    let company = this.sa_modal_company_id;

		this._common_service.getWorkingSchedule(company)
		.subscribe(
			data => {
				this.work_sched = data;
				this.options = {
					dropdownAutoWidth: true
				};
				this.ws_value = [];
				this.ws_current = this.ws_value;

				let id = 0;
				let text = 'Suggested Schedule';

				this.work_sched.unshift({id,text});

				this.ws_value = this.value;
				console.log('working schedules suggestion');
				console.log(this.work_sched);
			},
			err => this.catchError(err)
		);
	}

	getSuggestedWorkingSchedule(){
		this._common_service.getSuggestedWorkingSchedule().
		subscribe(
			data => {
				this.work_sched = data;
				this.options = {
					dropdownAutoWidth: true
				};
				this.ws_value = [];
				this.ws_current = this.ws_value;

				let id = 0;
				let text = 'Suggested Schedule';

				this.work_sched.unshift({id,text});

				this.ws_value = this.value;
				console.log('working schedules suggestion');
				console.log(this.work_sched);
			}
		);
	}

	private changedWorkingSchedule(data: any,i:any) {
  		for (let y = 0; y < this.work_sched.length; ++y) {
  			if (this.work_sched[y].id == data.value) {
				this.dataStorage[i].break_start = this.work_sched[y].break_start;
				this.dataStorage[i].break_end = this.work_sched[y].break_end;
				this.dataStorage[i].start_time = this.work_sched[y].time_in;
				this.dataStorage[i].end_time = this.work_sched[y].time_out;
        this.dataStorage[i].lunch_mins_break = this.work_sched[y].lunch_mins_break;
  			}
  		}
    }
}
