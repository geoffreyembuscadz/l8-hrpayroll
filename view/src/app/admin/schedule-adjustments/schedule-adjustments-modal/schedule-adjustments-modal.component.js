var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, ElementRef } from '@angular/core';
import { Configuration } from '../../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ScheduleAdjustmentsService } from '../../../services/schedule-adjustments.service';
import { CommonService } from '../../../services/common.service';
import { ScheduleAdjustments } from '../../../model/schedule-adjustment';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import * as moment from 'moment';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';
var ScheduleAdjustmentsModalComponent = /** @class */ (function (_super) {
    __extends(ScheduleAdjustmentsModalComponent, _super);
    function ScheduleAdjustmentsModalComponent(_common_service, _SAservice, dialogService, _fb, _ar, daterangepickerOptions, _auth_service, _conf) {
        var _this = _super.call(this, dialogService) || this;
        _this._common_service = _common_service;
        _this._SAservice = _SAservice;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this._auth_service = _auth_service;
        _this._conf = _conf;
        _this.start_time = new Date("2017-08-08 08:00:00");
        _this.end_time = new Date("2017-08-08 17:00:00");
        _this.sa = new ScheduleAdjustments();
        _this.mainInput = {
            start: moment(),
            end: moment().add(1, 'days')
        };
        _this.ismeridian = true;
        _this.validate = false;
        _this.dataStorage = [];
        _this.break_start = new Date("2017-08-08 12:00:00");
        _this.break_end = new Date("2017-08-08 13:00:00");
        _this.tableVal = false;
        _this.empVal = false;
        _this.user = false;
        _this.admin = false;
        _this.dupVal = false;
        _this.user_is_admin = 1;
        _this.error_saform_company = '';
        _this.innerHTML = '';
        _this.work_sched = [];
        _this.host = _this._conf.socketUrl;
        _this.SAForm = _fb.group({
            'start_time': [_this.sa.start_time, [Validators.required]],
            'end_time': [_this.sa.end_time, [Validators.required]],
            'break_start': [null, [Validators.required]],
            'lunch_mins_break': [null, [Validators.required]],
            'remarks': [(_this.sa.remarks) ? _this.sa.remarks : 'Input Reason here', [Validators.required]],
            'status_id': [null]
        });
        _this.sa_data_update = {
            'id': null,
            'date': null,
            'end_time': null,
            'start_time': null,
            'break_start': null,
            'employee_id': null,
            'company_id': null,
            'lunch_mins_break': null
        };
        _this.SARFormUpdate = _fb.group({
            'id': [_this.sa_id, []],
            'employee_id': [_this.sa_data_update.employee_id, [Validators.required]],
            'company_id': [_this.sa_data_update.company_id, [Validators.required]],
            'start_time': [_this.sa_data_update.start_time, [Validators.required]],
            'end_time': [_this.sa_data_update.end_time, [Validators.required]],
            'break_start': [_this.sa_data_update.break_start, [Validators.required]],
            'lunch_mins_break': [_this.sa_data_update.lunch_mins_break, [Validators.required]],
            'date': [_this.sa_data_update.date, [Validators.required]]
        });
        _this.sa_modal_company_id = null;
        _this.daterangepickerOptions.settings = {};
        _this.time_hours = _this._common_service.getTimeHours();
        _this.time_minutes = _this._common_service.getTimeMins();
        return _this;
    }
    ScheduleAdjustmentsModalComponent.prototype.ngOnInit = function () {
        if (this.user == true) {
            this.empVal = true;
            this.getCompanies();
            this.SAForm.addControl('emp_id', new FormControl());
            // this.get_employee();
        }
        if (this.edit == true) {
            this.getSA();
            this.empVal = true;
            this.validate = true;
            this.getCompanies();
            // this.get_employee();
        }
        else {
            this.getCompanies();
            // this.get_employee();
        }
        //uncomment this for socket.io
        // this.connectToServer();
    };
    ScheduleAdjustmentsModalComponent.prototype.ngAfterViewInit = function () {
        console.log(this.sa_ID.nativeElement.innerHTML); // POTAENA
    };
    ScheduleAdjustmentsModalComponent.prototype.updateSARdata = function () {
        var _this = this;
        this.error_title = '';
        this.error_message = '';
        this.success_title = '';
        this.success_message = '';
        this.sa_data_update = this.SARFormUpdate;
        if (this.sa_data_update.valid) {
            this._SAservice.updateSAR(this.sa_id, this.sa_data_update.value).subscribe(function (data) {
                console.log('updated');
                _this.success_title = 'Success!';
                _this.success_message = 'Record has been successfully updated!';
            });
        }
        else {
            this.error_title = 'Oops!';
            this.error_message = 'Please Check your input if its valid before submitting.';
        }
    };
    ScheduleAdjustmentsModalComponent.prototype.changeSARdataUpdate = function (select_value, index) {
        var input_val = select_value.target.value;
    };
    ScheduleAdjustmentsModalComponent.prototype.getSA = function () {
        var _this = this;
        console.log(this.sa_id);
        console.log(this.SARFormUpdate);
        // Overwritten Sourcode
        if (this.sa_id) {
            this._SAservice.getSArequest(this.sa_id).subscribe(function (data) {
                _this.sa_data_update = data;
                _this.SARFormUpdate.setValue({
                    'id': data.id,
                    'employee_id': data.employee_id,
                    'company_id': data.company_id,
                    'start_time': data.start_time,
                    'end_time': data.end_time,
                    'break_start': data.break_start,
                    'lunch_mins_break': data.lunch_mins_break,
                    'date': data.date
                });
                console.log(_this.SARFormUpdate);
            });
        }
        // Jenny's Sourcecode Do Not Use this!
        // this._SAservice.getSA(this.sa_id).subscribe(
        //      data => {
        //        this.sa_data=(data);
        //        this.mainInput.start = this.sa_data[0].start_date;
        //        this.mainInput.end = this.sa_data[0].end_date;
        //        this.start_date = this.sa_data[0].start_date;
        //        this.end_date = this.sa_data[0].end_date;
        //        this.remarks = this.sa_data[0].remarks;
        //        this.employee_id= this.sa_data[0].emp_id;
        //        let start = this.sa_data[0].start_date + " " + this.sa_data[0].start_time;
        //        let end = this.sa_data[0].end_date + " " + this.sa_data[0].end_time;
        //        let b_start = this.sa_data[0].start_date + " " + this.sa_data[0].break_start;
        //        let b_end = this.sa_data[0].end_date + " " + this.sa_data[0].break_end;
        //        this.start_time = new Date(start);
        //        this.end_time = new Date(end);
        //        this.break_start = new Date(b_start);
        //        this.break_end = new Date(b_end);
        //         this.lunch_mins_break = this.sa_data[0].lunch_mins_break;
        //         /*
        //        this.daterangepickerOptions.settings.startDate = new Date(this.sa_data[0].start_date);
        //        this.daterangepickerOptions.settings.endDate = new Date(this.sa_data[0].end_date); */
        //        let len = this.sa_data.length;
        //        let i = 1;
        //        for (let x = 0; x < len; x++) {
        // 		let date = this.sa_data[x].date;
        // 		let start_time = this.sa_data[x].start_time;
        // 		let end_time =  this.sa_data[x].end_time;
        // 		let break_start = this.sa_data[x].break_start;
        // 		let break_end = this.sa_data[x].break_end;
        // 		let request_id = this.sa_data[x].request_id;
        // 		let day = moment(this.sa_data[x].date).format("dddd");
        // 		this.dataStorage.push({i,day,date,start_time,end_time,break_start,break_end,request_id});
        // 		i++;
        //        }
        //        this.tableVal = true;
        //      },
        //      err => console.error(err)
        //   	);
    };
    ScheduleAdjustmentsModalComponent.prototype.getCompanies = function () {
        var _this = this;
        this._common_service.getCompany().subscribe(function (data) {
            _this.setCompanies(data);
        }, function (err) { return _this.catchError(err); });
    };
    ScheduleAdjustmentsModalComponent.prototype.setCompanies = function (data) {
        var companies = data;
        var fetched_companies = [];
        for (var _i = 0; _i < companies.length; _i++) {
            fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].text };
        }
        this.companies = fetched_companies;
    };
    ScheduleAdjustmentsModalComponent.prototype.getEmployeesByCompany = function (company_id) {
        this.sa_modal_company_id = company_id;
        this.getEmployees(company_id);
    };
    ScheduleAdjustmentsModalComponent.prototype.getEmployees = function (company_id) {
        var _this = this;
        this.error_saform_company = '';
        ////
        this._common_service.getEmployees("", company_id).subscribe(function (data) {
            _this.employee = _this.emp;
            _this.employee_value = [];
            if (_this.edit == true) {
                _this.employee_value = _this.employee_id;
                _this.validate = true;
            }
            _this.options = {
                multiple: true
            };
            _this.current = _this.employee_value;
            _this.employee = data;
            if (data.length == 0) {
                _this.error_saform_company = 'The Selected Company does not have any employee records.';
            }
            // this.work_sched = data;
            // this.options = {
            // 	dropdownAutoWidth: true
            // }
            // this.ws_value = [];
            // this.ws_current = this.ws_value;
            // let id = 0;
            // let text = 'Suggested Schedule';
            // this.work_sched.unshift({id,text});
            // this.ws_value = this.value;
        }, function (err) { return _this.catchError(err); });
    };
    ScheduleAdjustmentsModalComponent.prototype.get_employee = function () {
        /*
        this.employee = this.emp;
        this.employee_value = [];
        if(this.edit == true) {
            this.employee_value = this.employee_id;
            this.validate = true;
        }
        this.options = {
            multiple: true
        }
        this.current = this.employee_value;
        */
    };
    ScheduleAdjustmentsModalComponent.prototype.changed = function (data) {
        if (this.single == true) {
            var v = [];
            v.push(data.value);
            this.current = v;
        }
        else {
            this.current = data.value;
        }
        var len = this.current.length;
        if (len >= 1) {
            this.empVal = true;
        }
        else {
            this.empVal = false;
        }
        this.validation();
        this.duplicateEntryValidation();
    };
    ScheduleAdjustmentsModalComponent.prototype.validation = function () {
        if (this.empVal == true && this.tableVal == true && this.dupVal == true) {
            this.validate = true;
        }
        else {
            this.validate = false;
        }
    };
    ScheduleAdjustmentsModalComponent.prototype.submit = function () {
        var _this = this;
        this.validate = false;
        this.error_title = '';
        this.error_message = '';
        this.duplicateEntryValidation();
        var sa = this.SAForm.value;
        var i = sa.details.length - 1;
        sa.start_date = sa.details[0].date;
        sa.end_date = sa.details[i].date;
        if (this.edit == true && this.user == false) {
            this.SAForm.value.id = this.sa_id;
            sa = this.SAForm.value;
            this._SAservice.updateSA(sa).subscribe(function (data) {
                _this.poststore = data;
                _this.result = true;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        else if (this.edit == true && this.user == true) {
            this.SAForm.value.id = this.sa_id;
            sa = this.SAForm.value;
            this._SAservice
                .updateMySA(sa)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.result = true;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        if (this.create == true && this.user == false) {
            this._SAservice
                .createSA(sa)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        else if (this.create == true && this.user == true) {
            var user_EMPLOYEE_ID = (this.emp_ID.nativeElement.innerHTML != 'N/A') ? this.emp_ID.nativeElement.innerHTML : 0; // This is applicable when user is an employee or account is employee level
            sa.emp_id = user_EMPLOYEE_ID;
            sa.employee_id = [user_EMPLOYEE_ID];
            this._SAservice
                .createMySA(sa)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
    };
    ScheduleAdjustmentsModalComponent.prototype.connectToServer = function () {
        var _this = this;
        var socketUrl = this.host;
        this.socket = io.connect(socketUrl);
        this.socket.on("connect", function () { return _this.connect(); });
        this.socket.on("disconnect", function () { return _this.disconnect(); });
        this.socket.on("error", function (error) {
            console.log("ERROR: \"" + error + "\" (" + socketUrl + ")");
        });
    };
    // Handle connection opening
    ScheduleAdjustmentsModalComponent.prototype.connect = function () {
        // Request initial list when connected
        this.socket.emit("join", 'Client User -> ');
    };
    // Handle connection closing
    ScheduleAdjustmentsModalComponent.prototype.disconnect = function () {
        console.log("Disconnected from \"" + this.host + "\"");
    };
    ScheduleAdjustmentsModalComponent.prototype.errorCatch = function () {
        var _this = this;
        if (this.poststore.status_code == 200) {
            this.success_title = 'Success';
            this.success_message = 'No Changes Happen';
            setTimeout(function () {
                _this.close();
            }, 1000);
        }
        else if (this.poststore.status_code == 400) {
            this.error_title = 'Warning';
            this.error_message = 'Request is already exist';
            var temp_1;
            setTimeout(function () {
                var disposable = _this.dialogService.addDialog(ValidationModalComponent, {
                    title: 'Warning',
                    message: 'Schedule Adjustment',
                    user_is_admin: _this.user_is_admin,
                    url: 'schedule_adjustments',
                    data: _this.poststore.data,
                    userDup: _this.user,
                    adminDup: _this.admin
                }).subscribe(function (message) {
                    if (message == true) {
                        _this.error_title = '';
                        _this.error_message = '';
                        setTimeout(function () {
                            _this.close();
                        }, 1000);
                    }
                    else if (message != false) {
                        temp_1 = message;
                        for (var x = 0; x < temp_1.length; ++x) {
                            var index = _this.current.indexOf(temp_1[x]);
                            _this.current.splice(index, 1);
                        }
                        _this.employee_value = _this.current;
                        if (_this.current.length == 0) {
                            _this.error_title = '';
                            _this.error_message = '';
                            setTimeout(function () {
                                _this.close();
                            }, 1000);
                        }
                    }
                });
            }, 1000);
        }
        else {
            // socket.io emit
            // this.socket.emit('notify', this.poststore);
            this.validate = false;
            this.success_title = "Success";
            this.success_message = "Successfully";
            setTimeout(function () {
                _this.close();
            }, 2000);
        }
    };
    ScheduleAdjustmentsModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation. Be sure that the required fields are not empty.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    ScheduleAdjustmentsModalComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        //format date
        this.start_date = moment(dateInput.start).format("YYYY-MM-DD");
        this.end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.computeDays();
    };
    //time picker option
    ScheduleAdjustmentsModalComponent.prototype.toggleMode = function () {
        this.ismeridian = !this.ismeridian;
    };
    ScheduleAdjustmentsModalComponent.prototype.computeDays = function () {
        this.tableVal = true;
        // this.getWorkingSchedule();
        this.getSuggestedWorkingSchedule();
        if (this.dataStorage != null) {
            this.dataStorage = [];
        }
        if (this.start_date == null && this.end_date == null) {
            this.start_date = this.mainInput.start;
            this.end_date = this.mainInput.end;
        }
        var start = this.start_date;
        var end = this.end_date;
        var from = moment(this.start_date, 'YYYY-MM-DD'); // format in which you have the date
        var to = moment(this.end_date, 'YYYY-MM-DD'); // format in which you have the date
        var i = 0;
        var start_time = moment(this.start_time).format("HH:mm");
        var end_time = moment(this.end_time).format("HH:mm");
        var break_start = moment(this.break_start).format("HH:mm");
        var break_end = moment(this.break_end).format("HH:mm");
        var lunch_mins_break = (this.lunch_mins_break != null) ? this.lunch_mins_break : 60;
        var request_id = 0;
        /* using diff */
        var currDate = from.clone().startOf('day');
        var lastDate = to.clone().startOf('day');
        var date = moment(currDate.toDate()).format("YYYY-MM-DD");
        var day = moment(date).format("dddd");
        this.dataStorage.push({ i: i, day: day, date: date, start_time: start_time, end_time: end_time, break_start: break_start, lunch_mins_break: lunch_mins_break, break_end: break_end, request_id: request_id });
        while (currDate.add(1, 'days').diff(lastDate) < 0) {
            var date_1 = moment(currDate.clone().toDate()).format("YYYY-MM-DD");
            var day_1 = moment(date_1).format("dddd");
            this.dataStorage.push({ i: i, day: day_1, date: date_1, start_time: start_time, end_time: end_time, break_start: break_start, lunch_mins_break: lunch_mins_break, break_end: break_end, request_id: request_id });
        }
        if (start != end) {
            var date_2 = moment(lastDate.toDate()).format("YYYY-MM-DD");
            var day_2 = moment(date_2).format("dddd");
            this.dataStorage.push({ i: i, day: day_2, date: date_2, start_time: start_time, end_time: end_time, break_start: break_start, lunch_mins_break: lunch_mins_break, break_end: break_end, request_id: request_id });
        }
        var len = this.dataStorage.length;
        var t = 1;
        for (var x = 0; x < len; ++x) {
            this.dataStorage[x].i = t;
            t++;
        }
        this.duplicateEntryValidation();
    };
    ScheduleAdjustmentsModalComponent.prototype.removeDay = function (id) {
        this.dataStorage.splice(id, 1);
        if (this.dataStorage.length == 0) {
            this.tableVal = false;
        }
    };
    ScheduleAdjustmentsModalComponent.prototype.duplicateEntryValidation = function () {
        var _this = this;
        this.SAForm.value.employee_id = this.current;
        this.SAForm.value.supervisor_id = this.supervisor_id;
        this.SAForm.value.start_date = moment(this.start_date).format("YYYY-MM-DD");
        this.SAForm.value.end_date = moment(this.end_date).format("YYYY-MM-DD");
        this.SAForm.value.created_by = this.user_id;
        this.SAForm.value.details = this.dataStorage;
        if (this.user == true && this.create == true || this.user == true && this.edit == true) {
            var v = [];
            v.push(this.employee_id);
            this.SAForm.value.employee_id = v;
        }
        if (this.edit == true) {
            this.SAForm.value.edit = this.edit;
            this.SAForm.value.id = this.sa_id;
        }
        else {
            this.SAForm.value.edit = false;
        }
        var sa = this.SAForm.value;
        if (this.SAForm.value.employee_id != 0 && this.SAForm.value.details.length != 0 || this.edit == true) {
            this._SAservice
                .duplicateEntryValidation(sa)
                .subscribe(function (data) {
                _this.poststore = data;
                if (_this.poststore.message == "duplicate") {
                    _this.dupVal = false;
                    _this.errorCatch();
                }
                else {
                    _this.dupVal = true;
                    _this.error_title = '';
                    _this.error_message = '';
                }
                _this.validation();
            }, function (err) { return _this.catchError(err); });
        }
    };
    ScheduleAdjustmentsModalComponent.prototype.changeStartTimeHour = function (index_row, select_value) {
        var start_time = this.dataStorage[index_row].start_time;
        var select_start_time_hour = select_value.target.value;
        var start_time_hour = start_time.split(':');
        var start_time_hour_a = start_time_hour[0];
        var start_time_hour_b = start_time_hour[1];
        var new_start_time = select_start_time_hour + ':' + start_time_hour_b;
        this.dataStorage[index_row].start_time = new_start_time;
    };
    ScheduleAdjustmentsModalComponent.prototype.changeStartTimeMin = function (index_row, select_value) {
        var start_time = this.dataStorage[index_row].start_time;
        var select_start_time_min = select_value.target.value;
        var start_time_min = start_time.split(':');
        var start_time_min_a = start_time_min[0];
        var start_time_min_b = start_time_min[1];
        var new_start_time = start_time_min_a + ':' + select_start_time_min;
        this.dataStorage[index_row].start_time = new_start_time;
    };
    ScheduleAdjustmentsModalComponent.prototype.changeEndTimeHour = function (index_row, select_value) {
        var end_time = this.dataStorage[index_row].end_time;
        var select_end_time_hour = select_value.target.value;
        var end_time_hour = end_time.split(':');
        var end_time_hour_a = end_time_hour[0];
        var end_time_hour_b = end_time_hour[1];
        var new_end_time = select_end_time_hour + ':' + end_time_hour_b;
        this.dataStorage[index_row].end_time = new_end_time;
    };
    ScheduleAdjustmentsModalComponent.prototype.changeEndTimeMin = function (index_row, select_value) {
        var end_time = this.dataStorage[index_row].end_time;
        var select_end_time_min = select_value.target.value;
        var end_time_min = end_time.split(':');
        var end_time_min_a = end_time_min[0];
        var end_time_min_b = end_time_min[1];
        var new_end_time = end_time_min_a + ':' + select_end_time_min;
        this.dataStorage[index_row].end_time = new_end_time;
    };
    ScheduleAdjustmentsModalComponent.prototype.changeBreakTimeHour = function (index_row, select_value) {
        var break_time = this.dataStorage[index_row].end_time;
        var select_break_time_hour = select_value.target.value;
        var break_time_hour = break_time.split(':');
        var break_time_hour_a = break_time_hour[0];
        var break_time_hour_b = break_time_hour[1];
        var new_break_time = select_break_time_hour + ':' + break_time_hour_b;
        this.dataStorage[index_row].break_start = new_break_time;
    };
    ScheduleAdjustmentsModalComponent.prototype.changeBreakTimeMin = function (index_row, select_value) {
        var break_time = this.dataStorage[index_row].end_time;
        var select_break_time_min = select_value.target.value;
        var break_time_min = break_time.split(':');
        var break_time_min_a = break_time_min[0];
        var break_time_min_b = break_time_min[1];
        var new_break_time = break_time_min_a + ':' + select_break_time_min;
        this.dataStorage[index_row].break_start = new_break_time;
    };
    ScheduleAdjustmentsModalComponent.prototype.updateData = function (i, type, event) {
        // let time = event.target.value;
        var time = moment("2017-11-29 " + event.target.value).format("HH:mm:ss");
        if (time != '') {
            if (type == 'in') {
                this.dataStorage[i].start_time = time;
            }
            else if (type == 'out') {
                this.dataStorage[i].end_time = time;
            }
            else if (type == 'sb') {
                this.dataStorage[i].break_start = time;
            }
            else if (type == 'eb') {
                this.dataStorage[i].break_end = time;
            }
            else if (type == 'bm') {
                this.dataStorage[i].lunch_mins_break = 60;
            }
        }
    };
    ScheduleAdjustmentsModalComponent.prototype.getWorkingSchedule = function () {
        var _this = this;
        var company = this.sa_modal_company_id;
        this._common_service.getWorkingSchedule(company)
            .subscribe(function (data) {
            _this.work_sched = data;
            _this.options = {
                dropdownAutoWidth: true
            };
            _this.ws_value = [];
            _this.ws_current = _this.ws_value;
            var id = 0;
            var text = 'Suggested Schedule';
            _this.work_sched.unshift({ id: id, text: text });
            _this.ws_value = _this.value;
            console.log('working schedules suggestion');
            console.log(_this.work_sched);
        }, function (err) { return _this.catchError(err); });
    };
    ScheduleAdjustmentsModalComponent.prototype.getSuggestedWorkingSchedule = function () {
        var _this = this;
        this._common_service.getSuggestedWorkingSchedule().
            subscribe(function (data) {
            _this.work_sched = data;
            _this.options = {
                dropdownAutoWidth: true
            };
            _this.ws_value = [];
            _this.ws_current = _this.ws_value;
            var id = 0;
            var text = 'Suggested Schedule';
            _this.work_sched.unshift({ id: id, text: text });
            _this.ws_value = _this.value;
            console.log('working schedules suggestion');
            console.log(_this.work_sched);
        });
    };
    ScheduleAdjustmentsModalComponent.prototype.changedWorkingSchedule = function (data, i) {
        for (var y = 0; y < this.work_sched.length; ++y) {
            if (this.work_sched[y].id == data.value) {
                this.dataStorage[i].break_start = this.work_sched[y].break_start;
                this.dataStorage[i].break_end = this.work_sched[y].break_end;
                this.dataStorage[i].start_time = this.work_sched[y].time_in;
                this.dataStorage[i].end_time = this.work_sched[y].time_out;
                this.dataStorage[i].lunch_mins_break = this.work_sched[y].lunch_mins_break;
            }
        }
    };
    __decorate([
        ViewChild('sa_ID'),
        __metadata("design:type", ElementRef)
    ], ScheduleAdjustmentsModalComponent.prototype, "sa_ID", void 0);
    __decorate([
        ViewChild('emp_ID'),
        __metadata("design:type", ElementRef)
    ], ScheduleAdjustmentsModalComponent.prototype, "emp_ID", void 0);
    ScheduleAdjustmentsModalComponent = __decorate([
        Component({
            selector: 'app-schedule-adjustments-modal',
            templateUrl: './schedule-adjustments-modal.component.html',
            styleUrls: ['./schedule-adjustments-modal.component.css']
        }),
        __metadata("design:paramtypes", [CommonService,
            ScheduleAdjustmentsService,
            DialogService,
            FormBuilder,
            ActivatedRoute,
            DaterangepickerConfig,
            AuthUserService,
            Configuration])
    ], ScheduleAdjustmentsModalComponent);
    return ScheduleAdjustmentsModalComponent;
}(DialogComponent));
export { ScheduleAdjustmentsModalComponent };
//# sourceMappingURL=schedule-adjustments-modal.component.js.map