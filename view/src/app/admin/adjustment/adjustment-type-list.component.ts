import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';

import { AdjustmentType } from '../../model/adjustment_type';
import { AdjustmentTypeCreateComponent } from './adjustment-type-create.component';
import { AdjustmentTypeEditComponent } from './adjustment-type-edit.component';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'admin-adjustments-type-list',
  templateUrl: './adjustment-type-list.component.html',
  styleUrls: ['./adjustment-list.component.css']
})

@Injectable()
export class AdjustmentTypeListComponent implements OnInit, AfterViewInit  {

  @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();


  public id: string;
  public adjust_id: any;
  public Adjustment = new AdjustmentType();

  dtOptions: any = {};
  bodyClasses:string = "skin-blue sidebar-mini";
    body = document.getElementsByTagName('body')[0];

  constructor(private _conf: Configuration, private modalService: DialogService, private _adjust_service: AdjustmentTypeService){

  }

  ngOnInit() {
    let authToken = localStorage.getItem('id_token');

    // DataTable Configuration
    this.dtOptions = {
      ajax: {
        url: this._conf.ServerWithApiUrl + 'adjustment_type',
        type: "GET",
        beforeSend: function(xhr){
                            xhr.setRequestHeader("Authorization",
                            "Bearer " + authToken);
                },
      },
      columns: [{
        title: 'ID',
        data: 'id'
      },{
        title: 'Name',
        data: 'name'
      }, {
        title: 'Display Name',
        data: 'display_name'
      }, {
        title: 'Description',
        data: 'description'
      }, {  
        title: 'Taxable',
        data: 'taxable'
      }, {
        title: 'Frequency',
        data: 'frequency'
      }, {
        title: 'Include On Payslip',
        data: 'include_payslip'
      }, {
        title: 'Type',
        data: 'type'
      }],
      rowCallback: (nRow: number, data: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
        let self = this;
        // Unbind first in order to avoid any duplicate handler
        // (see https://github.com/l-lin/angular-datatables/issues/87)
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', () => {
          this.adjust_id = data.id;
            this._adjust_service.setId(this.adjust_id);
          this.openEditModal();
        });
        return nRow;
      }
    };

    //add the the body classes
      this.body.classList.add("skin-blue");
      this.body.classList.add("sidebar-mini");

    
  }

  openEditModal() {
    let disposable = this.modalService.addDialog(AdjustmentTypeEditComponent, {
            title:'Update Adjustment'
          }).subscribe((isConfirmed)=>{
                this.rerender();
                
        });
  }

  openAddModal() {
    let disposable = this.modalService.addDialog(AdjustmentTypeCreateComponent, {
            title:'Add Adjustment'
          }).subscribe((isConfirmed)=>{
                this.rerender();
                
        });
    }

  ngOnDestroy() {
      //remove the the body classes
      this.body.classList.remove("skin-blue");
      this.body.classList.remove("sidebar-mini");
   }

   ngAfterViewInit(): void {
      this.dtTrigger.next();
    }

    rerender(): void {
      this.dtElement.dtInstance.then((dtInstance) => {
        dtInstance.destroy();
        this.dtTrigger.next();
      });
    }
}
