import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';

import { Adjustment } from '../../model/adjustment';
import { AdjustmentService } from '../../services/adjustment.service';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';

import { DaterangepickerConfig } from 'ng2-daterangepicker';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TimepickerModule } from 'ngx-bootstrap';

@Component({
  selector: 'admin-adjustment-edit',
  templateUrl: './adjustment-edit.component.html'
})
export class AdjustmentEditComponent extends DialogComponent<null, boolean> {
  
  title: string;
  message: string;

  public id: any;
  public name: string;
  public display_name: string;
  public description: string;
  public type: string;
  public success_title;
  public success_message;
  private headers: Headers;
  public poststore: any;
  public adjustment = new Adjustment();
  public adj_id: any;
  adjustment_type: any;
  adjust_type_rec: any;
  adjust_rec: any;

  updateAdjustmentForm : FormGroup;

  public date_in = moment();
  public mainInput = {
        start: moment(),
        end: moment()
    }
  date:any;

  constructor(private _adjust_type_serv: AdjustmentTypeService, private daterangepickerOptions: DaterangepickerConfig, private _rt: Router, private _adjust_service: AdjustmentService, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute) {
    super(dialogService);
   
    this.id = this._adjust_service.getId();
    this._adjust_service.getAdjustment(this.id).subscribe(
      data => {
        this.setAdjustment(data);
      },
      err => console.error(err)
    );

    this.updateAdjustmentForm = _fb.group({
      'employee_id': ['', [Validators.required]],
      'adjustment_type': ['', [Validators.required]],
      'type': ['', [Validators.required]],
      'adjustment_amount': ['', [Validators.required]],
      'date_file': ['']
    }); 

    this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            drops: 'up'
    }; 
  }
 
  public validateUpdate() {
    if (this.adjustment_type == 'DEDUCTION') {
      this.updateAdjustmentForm.patchValue({
        'adjustment_amount': this.updateAdjustmentForm.value.adjustment_amount *= -1
      });
    }

    this.updateAdjustmentForm.patchValue({
      'date_file': moment(this.date_in).format("YYYY-MM-DD")
    });

    let role_model = this.updateAdjustmentForm.value;

    this._adjust_service.updateAdjustment(this.id, role_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The adjustment is successfully updated.";
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/adjustment-list']);

      },
      err => this.catchError(err)
    );
  }

  archiveAdjustment(event: Event){
    event.preventDefault();
    let role_id = this.id;

    this._adjust_service.archiveAdjustment(role_id).subscribe(
      data => {
        alert('Record is successfully archived.');
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/adjustment-list']);
        
      },
      err => console.error(err)
    );

  }

  onlyDecimalNumberKey(event) {
      let charCode = (event.which) ? event.which : event.keyCode;
      if (charCode != 46 && charCode > 31
          && (charCode < 48 || charCode > 57))
          return false;
      return true;
  }

  private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        this.date = dateInput.start;
        this.date_in = this.date;
  }

  public catchError(error: any) {

  }

  public setAdjustment(adjustment: any){
    this.id = adjustment.id;
    this.adjustment = adjustment;
  }


  ngOnInit() {
    let start ='';
    let end = '';
    let id = '';
    let type = '';
    let no_of_days = '';
     let adjustment = this._adjust_service.getAdjustments(id, start , end, type, no_of_days)
         .subscribe(
           data => {
             this.adjust_rec = Array.from(data); 
           },
           err => this.catchError(err)
         );
     let adjustment_type = this._adjust_type_serv.getAdjustmentsType()
        .subscribe(
          data => {
            this.adjust_type_rec = Array.from(data);
          },

          err => this.catchError(err)

        );
  }

  getAdjustType(value) {
    this.adj_id = value;

    let adjustment = this._adjust_type_serv.getAdjustmentType(this.adj_id)
        .subscribe(
          data => {
            let adjust = data;
            this.adjustment_type = data.type;
          }
        );
  }
}