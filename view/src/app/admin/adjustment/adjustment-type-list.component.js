var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';
import { AdjustmentType } from '../../model/adjustment_type';
import { AdjustmentTypeCreateComponent } from './adjustment-type-create.component';
import { AdjustmentTypeEditComponent } from './adjustment-type-edit.component';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
var AdjustmentTypeListComponent = /** @class */ (function () {
    function AdjustmentTypeListComponent(_conf, modalService, _adjust_service) {
        this._conf = _conf;
        this.modalService = modalService;
        this._adjust_service = _adjust_service;
        this.dtTrigger = new Subject();
        this.Adjustment = new AdjustmentType();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
    }
    AdjustmentTypeListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var authToken = localStorage.getItem('id_token');
        // DataTable Configuration
        this.dtOptions = {
            ajax: {
                url: this._conf.ServerWithApiUrl + 'adjustment_type',
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + authToken);
                },
            },
            columns: [{
                    title: 'ID',
                    data: 'id'
                }, {
                    title: 'Name',
                    data: 'name'
                }, {
                    title: 'Display Name',
                    data: 'display_name'
                }, {
                    title: 'Description',
                    data: 'description'
                }, {
                    title: 'Taxable',
                    data: 'taxable'
                }, {
                    title: 'Frequency',
                    data: 'frequency'
                }, {
                    title: 'Include On Payslip',
                    data: 'include_payslip'
                }, {
                    title: 'Type',
                    data: 'type'
                }],
            rowCallback: function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
                var self = _this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', function () {
                    _this.adjust_id = data.id;
                    _this._adjust_service.setId(_this.adjust_id);
                    _this.openEditModal();
                });
                return nRow;
            }
        };
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    };
    AdjustmentTypeListComponent.prototype.openEditModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(AdjustmentTypeEditComponent, {
            title: 'Update Adjustment'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    AdjustmentTypeListComponent.prototype.openAddModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(AdjustmentTypeCreateComponent, {
            title: 'Add Adjustment'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    AdjustmentTypeListComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    AdjustmentTypeListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    AdjustmentTypeListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], AdjustmentTypeListComponent.prototype, "dtElement", void 0);
    AdjustmentTypeListComponent = __decorate([
        Component({
            selector: 'admin-adjustments-type-list',
            templateUrl: './adjustment-type-list.component.html',
            styleUrls: ['./adjustment-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration, DialogService, AdjustmentTypeService])
    ], AdjustmentTypeListComponent);
    return AdjustmentTypeListComponent;
}());
export { AdjustmentTypeListComponent };
//# sourceMappingURL=adjustment-type-list.component.js.map