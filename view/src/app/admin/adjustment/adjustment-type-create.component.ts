import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { Observable } from "rxjs/Rx";

import { AdjustmentType } from '../../model/adjustment_type';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';

@Component({
  selector: 'admin-adjustment-type-create',
  templateUrl: './adjustment-type-create.component.html'
})
export class AdjustmentTypeCreateComponent extends DialogComponent<null, boolean> {

  public error_title: string;
  public error_message: string;
  private success_title: string;
  private success_message: string;
  public poststore: any;
  public adjust = new AdjustmentType();
  public adjustmentChk: boolean = false;

  error = false;
  elem: any;
  errorMessage = '';
  createAdjustmentTypeForm : FormGroup;

  constructor(private _rt: Router, dialogService: DialogService, private _fb: FormBuilder, private _adjust_service: AdjustmentTypeService) { 
  	super(dialogService);

  	this.createAdjustmentTypeForm = _fb.group({
	    'name': ['', [Validators.required, this.isAlphaNumeric]],
	    'display_name': [''],
      'description': [''],
      'taxable': [''],
      'billable': [''],
	    'frequency': [''],
      'type': ['', [Validators.required]],
      'include_payslip': ['', [Validators.required]],
    });
  }

  onSubmit() {

  if (this.adjustmentChk == false) {
    this.createAdjustmentTypeForm.patchValue({
      'taxable': 0
    });
    this.createAdjustmentTypeForm.patchValue({
      'billable': 0
    });
    this.createAdjustmentTypeForm.patchValue({
      'frequency': 0
    });
  }

	let  adjustment_model = this.createAdjustmentTypeForm.value;
	this._adjust_service
	.storeAdjustmentType(adjustment_model)
	.subscribe(
		data => {
			this.poststore = Array.from(data);
			this.success_title = "Success";
        	this.success_message = "A new adjustment type record was successfully added.";
      setTimeout(() => {
             this.close();
            }, 2000);
      this._rt.navigate(['/admin/adjustment-type-list']);

		},
		err => this.catchError(err)
	);
  }

  private catchError(error: any){
  	let response_body = error._body;
  	let response_status = error.status;

  	if( response_status == 500 ){
  		this.error_title = 'Error 500';
  		this.error_message = 'Adjustment name already exist.';
  	} else if( response_status == 200 ) {
  		this.error_title = '';
  		this.error_message = '';
  	}
  }


  isAlphaNumeric(control: FormControl): {[s: string]: boolean} {      
    if(control.value === '') {
      return { noAlphaNumeric: true }
    } else if(!control.value.match(/^[a-z_]+$/i))  {
      return { noAlphaNumeric: true };
    }
  }

  getType(value) {
    if (value == 'ADJUSTMENT') {
      this.adjustmentChk = true;
    } else {
      this.adjustmentChk = false;
    }
  }

  ngOnInit() {
  }

}
