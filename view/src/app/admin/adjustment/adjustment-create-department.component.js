var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Adjustment } from '../../model/adjustment';
import { AdjustmentService } from '../../services/adjustment.service';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';
import { EmployeeService } from '../../services/employee.service';
import { AuthUserService } from '../../services/auth-user.service';
import { DepartmentService } from '../../services/department.service';
import { CompanyService } from '../../services/company.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
var AdjustmentCreateDepartmentComponent = /** @class */ (function (_super) {
    __extends(AdjustmentCreateDepartmentComponent, _super);
    function AdjustmentCreateDepartmentComponent(_rt, daterangepickerOptions, _company_serv, _department_serv, _auth_service, _emp_service, dialogService, _fb, _adjust_service, _adjust_type_serv) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this._company_serv = _company_serv;
        _this._department_serv = _department_serv;
        _this._auth_service = _auth_service;
        _this._emp_service = _emp_service;
        _this._fb = _fb;
        _this._adjust_service = _adjust_service;
        _this._adjust_type_serv = _adjust_type_serv;
        _this.adjust = new Adjustment();
        _this.enableDepartment = false;
        _this.employee = [];
        _this.error = false;
        _this.errorMessage = '';
        _this.date_in = moment();
        _this.mainInput = {
            start: moment(),
            end: moment()
        };
        _this.createAdjustmentForm = _fb.group({
            'employee_id': [''],
            'adjustment_type': ['', [Validators.required]],
            'type': ['', [Validators.required]],
            'billable': ['', [Validators.required]],
            'adjustment_amount': ['', [Validators.required]],
            'created_by': [''],
            'date_file': [''],
            'department': ['', [Validators.required]],
            'company': ['', [Validators.required]]
        });
        _this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            drops: 'up'
        };
        return _this;
    }
    AdjustmentCreateDepartmentComponent.prototype.onSubmit = function () {
        var _this = this;
        this.createAdjustmentForm.patchValue({
            'employee_id': this.employee_id
        });
        if (this.adjustment_type == 'DEDUCTION') {
            this.createAdjustmentForm.patchValue({
                'adjustment_amount': this.createAdjustmentForm.value.adjustment_amount *= -1
            });
        }
        this.createAdjustmentForm.patchValue({
            'date_file': moment(this.date_in).format("YYYY-MM-DD")
        });
        this.createAdjustmentForm.value.created_by = this.user_id;
        var adjustment_model = this.createAdjustmentForm.value;
        this._adjust_service
            .storeAdjustment(adjustment_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            _this.success_title = "Success";
            _this.success_message = "A new adjustment record was successfully added.";
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/adjustment-list']);
        }, function (err) { return _this.catchError(err); });
    };
    AdjustmentCreateDepartmentComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        this.date = dateInput.start;
        this.date_in = this.date;
    };
    AdjustmentCreateDepartmentComponent.prototype.onlyDecimalNumberKey = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    };
    AdjustmentCreateDepartmentComponent.prototype.getDepartment = function (value, event) {
        this.employee.length = 0;
        for (var i = 0; i < this.emp_rec.length; i++) {
            if (value == this.emp_rec[i].department) {
                if (this.companies == this.emp_rec[i].company) {
                    this.employee.push(this.emp_rec[i].id);
                }
            }
        }
        this.employee_id = this.employee;
    };
    AdjustmentCreateDepartmentComponent.prototype.getCompany = function (value, event) {
        this.enableDepartment = true;
        this.companies = value;
    };
    AdjustmentCreateDepartmentComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    AdjustmentCreateDepartmentComponent.prototype.ngOnInit = function () {
        var _this = this;
        var adjustment_type = this._adjust_type_serv.getAdjustmentsType()
            .subscribe(function (data) {
            _this.adjust_type_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var department = this._department_serv.getDepartments()
            .subscribe(function (data) {
            _this.department_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var company = this._company_serv.getCompanys()
            .subscribe(function (data) {
            _this.company_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var employee_name = this._emp_service.getEmployees()
            .subscribe(function (data) {
            _this.emp_rec = Array.from(data);
        }, function (err) { return console.error(err); });
        var created_by = this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
        }, function (err) { return console.error(err); });
    };
    AdjustmentCreateDepartmentComponent.prototype.getAdjustType = function (value) {
        var _this = this;
        this.adj_id = value;
        var adjustment = this._adjust_type_serv.getAdjustmentType(this.adj_id)
            .subscribe(function (data) {
            var adjust = data;
            _this.adjustment_type = data.type;
        });
    };
    AdjustmentCreateDepartmentComponent = __decorate([
        Component({
            selector: 'admin-adjustment-create',
            templateUrl: './adjustment-create-department.component.html',
            styleUrls: ['./adjustment-list.component.css']
        }),
        __metadata("design:paramtypes", [Router, DaterangepickerConfig, CompanyService, DepartmentService, AuthUserService, EmployeeService, DialogService, FormBuilder, AdjustmentService, AdjustmentTypeService])
    ], AdjustmentCreateDepartmentComponent);
    return AdjustmentCreateDepartmentComponent;
}(DialogComponent));
export { AdjustmentCreateDepartmentComponent };
//# sourceMappingURL=adjustment-create-department.component.js.map