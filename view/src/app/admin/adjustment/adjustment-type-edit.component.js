var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AdjustmentType } from '../../model/adjustment_type';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';
var AdjustmentTypeEditComponent = /** @class */ (function (_super) {
    __extends(AdjustmentTypeEditComponent, _super);
    function AdjustmentTypeEditComponent(_rt, _adjust_service, dialogService, _fb, _ar) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._adjust_service = _adjust_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.adjustment = new AdjustmentType();
        _this.adjustmentChk = false;
        _this.adjustmentSelect = false;
        _this.id = _this._adjust_service.getId();
        _this._adjust_service.getAdjustmentType(_this.id).subscribe(function (data) {
            if (data.type == 'ADJUSTMENT') {
                _this.adjustmentSelect = true;
                _this.adjustmentChk = true;
            }
            _this.setAdjustmentType(data);
        }, function (err) { return console.error(err); });
        _this.updateAdjustmentTypeForm = _fb.group({
            'name': [_this.adjustment.name, [Validators.required, _this.isAlphaNumeric]],
            'display_name': [_this.adjustment.display_name, [Validators.required]],
            'description': [_this.adjustment.description],
            'taxable': [_this.adjustment.taxable, [Validators.required]],
            'frequency': [_this.adjustment.frequency, [Validators.required]],
            'type': [_this.adjustment.type, [Validators.required]],
            'billable': [_this.adjustment.billable, [Validators.required]],
            'include_payslip': ['', [Validators.required]]
        });
        return _this;
    }
    AdjustmentTypeEditComponent.prototype.validateUpdate = function () {
        var _this = this;
        if (this.adjustmentChk == false) {
            this.updateAdjustmentTypeForm.value.taxable = 0;
            this.updateAdjustmentTypeForm.value.billable = 0;
            this.updateAdjustmentTypeForm.value.frequency = 0;
        }
        var role_model = this.updateAdjustmentTypeForm.value;
        this._adjust_service.updateAdjustmentType(this.id, role_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The adjustment type is successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/adjustment-type-list']);
        }, function (err) { return _this.catchError(err); });
    };
    AdjustmentTypeEditComponent.prototype.archiveAdjustmentType = function (event) {
        var _this = this;
        event.preventDefault();
        var role_id = this.id;
        this._adjust_service.archiveAdjustmentType(role_id).subscribe(function (data) {
            alert('Record is successfully archived.');
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/adjustment-type-list']);
        }, function (err) { return console.error(err); });
        console.log('record archive');
    };
    AdjustmentTypeEditComponent.prototype.catchError = function (error) {
    };
    AdjustmentTypeEditComponent.prototype.setAdjustmentType = function (adjustment) {
        this.id = adjustment.id;
        this.adjustment = adjustment;
    };
    AdjustmentTypeEditComponent.prototype.isAlphaNumeric = function (control) {
        if (control.value === '') {
            return { noAlphaNumeric: true };
        }
        else if (control.value === null || control.value === undefined) {
            return { noAlphaNumeric: false };
        }
        else if (!control.value.match(/^[a-z_]+$/i)) {
            return { noAlphaNumeric: true };
        }
    };
    AdjustmentTypeEditComponent.prototype.getTaxable = function (value) {
        if (value == 'ADJUSTMENT') {
            this.adjustmentChk = true;
            this.adjustmentSelect = true;
        }
        else {
            this.adjustmentChk = false;
            this.adjustmentSelect = false;
        }
    };
    AdjustmentTypeEditComponent.prototype.ngOnInit = function () {
    };
    AdjustmentTypeEditComponent = __decorate([
        Component({
            selector: 'admin-adjustment-type-edit',
            templateUrl: './adjustment-type-edit.component.html'
        }),
        __metadata("design:paramtypes", [Router, AdjustmentTypeService, DialogService, FormBuilder, ActivatedRoute])
    ], AdjustmentTypeEditComponent);
    return AdjustmentTypeEditComponent;
}(DialogComponent));
export { AdjustmentTypeEditComponent };
//# sourceMappingURL=adjustment-type-edit.component.js.map