import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { Observable } from "rxjs/Rx";

import { Adjustment } from '../../model/adjustment';
import { AdjustmentService } from '../../services/adjustment.service';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';
import { EmployeeService } from '../../services/employee.service';
import { AuthUserService } from '../../services/auth-user.service';
import { CompanyService } from '../../services/company.service';

import { DaterangepickerConfig } from 'ng2-daterangepicker';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TimepickerModule } from 'ngx-bootstrap';


@Component({
  selector: 'admin-adjustment-create',
  templateUrl: './adjustment-create-company.component.html',
  styleUrls: ['./adjustment-list.component.css']

})
export class AdjustmentCreateCompanyComponent extends DialogComponent<null, boolean> {

  public error_title: string;
  public error_message: string;
  private success_title: string;
  private success_message: string;
  public poststore: any;
  public companies:any;
  public employee_id: any;
  public adjust = new Adjustment();
  public adj_id: any;
  adjustment_type: any;
  adjust_type_rec: any;
  emp_rec: any;
  user_id: any;
  company_rec: any;

  employee = [];
  error = false;
  elem: any;
  errorMessage = '';
  createAdjustmentForm : FormGroup;


  public date_in = moment();
  public mainInput = {
        start: moment(),
        end: moment()
    }
  date:any;

  constructor(private _rt: Router, private daterangepickerOptions: DaterangepickerConfig, private _company_serv: CompanyService, private _auth_service: AuthUserService, private _emp_service: EmployeeService, dialogService: DialogService, private _fb: FormBuilder, private _adjust_service: AdjustmentService, private _adjust_type_serv: AdjustmentTypeService) { 
  	super(dialogService);

  	this.createAdjustmentForm = _fb.group({
	    'employee_id': [''],
	    'adjustment_type': ['', [Validators.required]],
      'type': ['', [Validators.required]],
      'billable': ['', [Validators.required]],
      'adjustment_amount': ['', [Validators.required]],
      'date_file': [''],
      'created_by': [''],
      'company': ['', [Validators.required]],

    });


    this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            drops: 'up'
    }; 
  }

  

  onSubmit() {

  	this.createAdjustmentForm.patchValue({
   	 'employee_id': this.employee_id
  	});

   
    this.createAdjustmentForm.patchValue({
      'date_file': moment(this.date_in).format("YYYY-MM-DD")
    });
   

    if (this.adjustment_type == 'DEDUCTION') {
      this.createAdjustmentForm.patchValue({
        'adjustment_amount': this.createAdjustmentForm.value.adjustment_amount *= -1
      });
    }


    this.createAdjustmentForm.value.created_by = this.user_id;

  	let  adjustment_model = this.createAdjustmentForm.value;
    console.log(adjustment_model);
  	this._adjust_service
  	.storeAdjustment(adjustment_model)
  	.subscribe(
  		data => {
  			this.poststore = Array.from(data);
  			this.success_title = "Success";
        this.success_message = "A new adjustment record was successfully added.";

        setTimeout(() => {
           this.close();
          }, 2000);
       this._rt.navigate(['/admin/adjustment-list']);
  		},
  		err => this.catchError(err)
  	);
  }

  getCompany(value, event) {
    this.employee.length = 0;
    for (let i = 0; i < this.emp_rec.length; i++) {
      if (value == this.emp_rec[i].company) {
        this.employee.push(this.emp_rec[i].id); 
      } 
    }
    this.employee_id = this.employee;
    
  }


  onlyDecimalNumberKey(event) {
      let charCode = (event.which) ? event.which : event.keyCode;
      if (charCode != 46 && charCode > 31
          && (charCode < 48 || charCode > 57))
          return false;
      return true;
  }

  private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        this.date = dateInput.start;
        this.date_in = this.date;
  }

  private catchError(error: any){
  	let response_body = error._body;
  	let response_status = error.status;

  	if( response_status == 500 ){
  		this.error_title = 'Error 500';
  		this.error_message = 'Something went wrong.';
  	} else if( response_status == 200 ) {
  		this.error_title = '';
  		this.error_message = '';
  	}
  }

  ngOnInit() {

    let adjustment_type = this._adjust_type_serv.getAdjustmentsType()
        .subscribe(
          data => {
            this.adjust_type_rec = Array.from(data);
          },

          err => this.catchError(err)

        );

    let companies = this._company_serv.getCompanys()
        .subscribe(
          data => {
            this.company_rec = Array.from(data);
          },
          err => this.catchError(err)
        );

    let employee_name = this._emp_service.getEmployees()
        .subscribe(
          data => {
            this.emp_rec = Array.from(data);
          },
          err => console.error(err)
        );



     let created_by = this._auth_service.getUser().
        subscribe(
          data => {
            let user = data; 
            this.user_id = user.id;
          },
          err => console.error(err)
        );
  

  }

  getAdjustType(value) {
    this.adj_id = value;

    let adjustment = this._adjust_type_serv.getAdjustmentType(this.adj_id)
        .subscribe(
          data => {
            let adjust = data;
            this.adjustment_type = data.type;
          }
        );
  }

}
