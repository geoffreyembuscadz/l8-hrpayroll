import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';

import { AdjustmentType } from '../../model/adjustment_type';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';

@Component({
  selector: 'admin-adjustment-type-edit',
  templateUrl: './adjustment-type-edit.component.html'
})
export class AdjustmentTypeEditComponent extends DialogComponent<null, boolean> {
  
  title: string;
  message: string;

  public id: any;
  public name: string;
  public display_name: string;
  public description: string;
  public type: string;
  public success_title;
  public success_message;
  private headers: Headers;
  public poststore: any;
  public adjustment = new AdjustmentType();
  public adjustmentChk: boolean = false;
  public adjustmentSelect: boolean = false;

  updateAdjustmentTypeForm : FormGroup;

  constructor(private _rt: Router, private _adjust_service: AdjustmentTypeService, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute) {
    super(dialogService);
   
    this.id = this._adjust_service.getId();
    this._adjust_service.getAdjustmentType(this.id).subscribe(
      data => {
        if (data.type == 'ADJUSTMENT') {
          this.adjustmentSelect = true;
          this.adjustmentChk = true;
        }
        this.setAdjustmentType(data);

      },
      err => console.error(err)
    );

    this.updateAdjustmentTypeForm = _fb.group({
      'name': [this.adjustment.name, [Validators.required, this.isAlphaNumeric]],
      'display_name': [this.adjustment.display_name, [Validators.required]],
      'description': [this.adjustment.description],
      'taxable': [this.adjustment.taxable, [Validators.required]],
      'frequency': [this.adjustment.frequency, [Validators.required]],
      'type': [this.adjustment.type, [Validators.required]],
      'billable': [this.adjustment.billable, [Validators.required]],
      'include_payslip': ['', [Validators.required]]
    });  
  }
 
  public validateUpdate() {

    if (this.adjustmentChk == false) {
      this.updateAdjustmentTypeForm.value.taxable = 0;
      this.updateAdjustmentTypeForm.value.billable = 0;
      this.updateAdjustmentTypeForm.value.frequency = 0;
    }

    let role_model = this.updateAdjustmentTypeForm.value;
    
    this._adjust_service.updateAdjustmentType(this.id, role_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The adjustment type is successfully updated.";
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/adjustment-type-list']);
      },
      err => this.catchError(err)
    );
  }

  archiveAdjustmentType(event: Event){
    event.preventDefault();
    let role_id = this.id;
    this._adjust_service.archiveAdjustmentType(role_id).subscribe(
      data => {
      
        alert('Record is successfully archived.');
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/adjustment-type-list']);
        
      },
      err => console.error(err)
    );
    console.log('record archive');

  }

  public catchError(error: any) {

  }

  public setAdjustmentType(adjustment: any){
    this.id = adjustment.id;
    this.adjustment = adjustment;
  }

  isAlphaNumeric(control: FormControl): {[s: string]: boolean} {      
    if(control.value === '') {
      return { noAlphaNumeric: true }
    } else if (control.value === null || control.value === undefined){
      return { noAlphaNumeric: false }
    } else if(!control.value.match(/^[a-z_]+$/i))  {
      return { noAlphaNumeric: true };
    }
    
  }

  getTaxable(value) {
    if (value == 'ADJUSTMENT') {
      this.adjustmentChk = true;
      this.adjustmentSelect = true;
    } else {
      this.adjustmentChk = false;
      this.adjustmentSelect = false;
    }
  }

  ngOnInit() {
  }

}