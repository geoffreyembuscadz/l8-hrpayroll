import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Adjustment } from '../../model/adjustment';
import { AdjustmentCreateEmployeeComponent } from './adjustment-create-employee.component';
import { AdjustmentCreateCompanyComponent } from './adjustment-create-company.component';
import { AdjustmentCreateDepartmentComponent } from './adjustment-create-department.component';
import { AdjustmentCreatePositionComponent } from './adjustment-create-position.component';
import { AdjustmentEditComponent } from './adjustment-edit.component';
import { AdjustmentService } from '../../services/adjustment.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'admin-adjustments-list',
  templateUrl: './adjustment-list-modal.component.html',
  styleUrls: ['./adjustment-list.component.css']
})

@Injectable()
export class AdjustmentListModalComponent extends DialogComponent<null, boolean> implements OnInit, AfterViewInit {

  @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();

  public id: string;
  public adjust_id: any;
  public Adjustment = new Adjustment();
  public emp_id: any;
  public error_title: string;
  public error_message: string;

  ad_rec: any;
  att_id: any;
  no_of_days: any;
  start:any;
  end:any
  type:any

  dtOptions: any = {};
  bodyClasses:string = "skin-blue sidebar-mini";
  body = document.getElementsByTagName('body')[0];

  constructor(private _rt: Router, dialogService: DialogService, private _conf: Configuration, private modalService: DialogService, private _adjust_service: AdjustmentService){
    super(dialogService);

  }

  ngOnInit() {

    let adjustment  = this._adjust_service.getAdjustments(this.att_id, this.start, this.end, this.type, this.no_of_days)
         .subscribe(data => {
            this.ad_rec = Array.from(data);
            this.rerender();
         },
        err => this.catchError(err)
      );

    //add the the body classes
      this.body.classList.add("skin-blue");
      this.body.classList.add("sidebar-mini");

    
  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'Something went wrong';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }
  }

  openEditModal() {
    let disposable = this.modalService.addDialog(AdjustmentEditComponent, {
            title:'Update Adjustment By Employee'
          }).subscribe((isConfirmed)=>{
                let adjustment  = this._adjust_service.getAdjustments(this.att_id, this.start, this.end, this.type, this.no_of_days)
                   .subscribe(data => {
                      this.ad_rec = Array.from(data);
                      this.rerender();
                   },
                  err => this.catchError(err)
                );
        });
  }

  openAddModalEmployee() {
    let disposable = this.modalService.addDialog(AdjustmentCreateEmployeeComponent, {
            title:'Add Adjustment By Employee'
          }).subscribe((isConfirmed)=>{
                let adjustment  = this._adjust_service.getAdjustments(this.att_id, this.start, this.end, this.type, this.no_of_days)
                   .subscribe(data => {
                      this.ad_rec = Array.from(data);
                      this.rerender();
                   },
                  err => this.catchError(err)
                );
        });
    }
  openAddModalCompany() {
    let disposable = this.modalService.addDialog(AdjustmentCreateCompanyComponent, {
            title:'Add Adjustment By Company'
          }).subscribe((isConfirmed)=>{
              let adjustment  = this._adjust_service.getAdjustments(this.att_id, this.start, this.end, this.type, this.no_of_days)
                 .subscribe(data => {
                    this.ad_rec = Array.from(data);
                    this.rerender();
                 },
                err => this.catchError(err)
              );  
        });
    }

  openAddModalDepartment() {
    let disposable = this.modalService.addDialog(AdjustmentCreateDepartmentComponent, {
            title:'Add Adjustment By Department'
          }).subscribe((isConfirmed)=>{
              let adjustment  = this._adjust_service.getAdjustments(this.att_id, this.start, this.end, this.type, this.no_of_days)
                 .subscribe(data => {
                    this.ad_rec = Array.from(data);
                    this.rerender();
                 },
                err => this.catchError(err)
              );  
        });
  }


  openAddModalPosition() {
    let disposable = this.modalService.addDialog(AdjustmentCreatePositionComponent, {
            title:'Add Adjustment By Position'
          }).subscribe((isConfirmed)=>{
             let adjustment  = this._adjust_service.getAdjustments(this.att_id, this.start, this.end, this.type, this.no_of_days)
                 .subscribe(data => {
                    this.ad_rec = Array.from(data);
                    this.rerender();
                 },
                err => this.catchError(err)
              );   
        });
  }


  checkAdjustmentType(adj_type) {
    if (adj_type == 'DEDUCTION') {
      return true;
    }
  }

  ngOnDestroy() {
      
   }

   ngAfterViewInit(): void {
      this.dtTrigger.next();
    }

    rerender(): void {
      this.dtElement.dtInstance.then((dtInstance) => {
        dtInstance.destroy();
        this.dtTrigger.next();
      });
    }
}
