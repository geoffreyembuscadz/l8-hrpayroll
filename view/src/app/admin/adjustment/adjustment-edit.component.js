var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Adjustment } from '../../model/adjustment';
import { AdjustmentService } from '../../services/adjustment.service';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
var AdjustmentEditComponent = /** @class */ (function (_super) {
    __extends(AdjustmentEditComponent, _super);
    function AdjustmentEditComponent(_adjust_type_serv, daterangepickerOptions, _rt, _adjust_service, dialogService, _fb, _ar) {
        var _this = _super.call(this, dialogService) || this;
        _this._adjust_type_serv = _adjust_type_serv;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this._rt = _rt;
        _this._adjust_service = _adjust_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.adjustment = new Adjustment();
        _this.date_in = moment();
        _this.mainInput = {
            start: moment(),
            end: moment()
        };
        _this.id = _this._adjust_service.getId();
        _this._adjust_service.getAdjustment(_this.id).subscribe(function (data) {
            _this.setAdjustment(data);
        }, function (err) { return console.error(err); });
        _this.updateAdjustmentForm = _fb.group({
            'employee_id': ['', [Validators.required]],
            'adjustment_type': ['', [Validators.required]],
            'type': ['', [Validators.required]],
            'adjustment_amount': ['', [Validators.required]],
            'date_file': ['']
        });
        _this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            drops: 'up'
        };
        return _this;
    }
    AdjustmentEditComponent.prototype.validateUpdate = function () {
        var _this = this;
        if (this.adjustment_type == 'DEDUCTION') {
            this.updateAdjustmentForm.patchValue({
                'adjustment_amount': this.updateAdjustmentForm.value.adjustment_amount *= -1
            });
        }
        this.updateAdjustmentForm.patchValue({
            'date_file': moment(this.date_in).format("YYYY-MM-DD")
        });
        var role_model = this.updateAdjustmentForm.value;
        this._adjust_service.updateAdjustment(this.id, role_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The adjustment is successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/adjustment-list']);
        }, function (err) { return _this.catchError(err); });
    };
    AdjustmentEditComponent.prototype.archiveAdjustment = function (event) {
        var _this = this;
        event.preventDefault();
        var role_id = this.id;
        this._adjust_service.archiveAdjustment(role_id).subscribe(function (data) {
            alert('Record is successfully archived.');
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/adjustment-list']);
        }, function (err) { return console.error(err); });
    };
    AdjustmentEditComponent.prototype.onlyDecimalNumberKey = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    };
    AdjustmentEditComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        this.date = dateInput.start;
        this.date_in = this.date;
    };
    AdjustmentEditComponent.prototype.catchError = function (error) {
    };
    AdjustmentEditComponent.prototype.setAdjustment = function (adjustment) {
        this.id = adjustment.id;
        this.adjustment = adjustment;
    };
    AdjustmentEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        var start = '';
        var end = '';
        var id = '';
        var type = '';
        var no_of_days = '';
        var adjustment = this._adjust_service.getAdjustments(id, start, end, type, no_of_days)
            .subscribe(function (data) {
            _this.adjust_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var adjustment_type = this._adjust_type_serv.getAdjustmentsType()
            .subscribe(function (data) {
            _this.adjust_type_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
    };
    AdjustmentEditComponent.prototype.getAdjustType = function (value) {
        var _this = this;
        this.adj_id = value;
        var adjustment = this._adjust_type_serv.getAdjustmentType(this.adj_id)
            .subscribe(function (data) {
            var adjust = data;
            _this.adjustment_type = data.type;
        });
    };
    AdjustmentEditComponent = __decorate([
        Component({
            selector: 'admin-adjustment-edit',
            templateUrl: './adjustment-edit.component.html'
        }),
        __metadata("design:paramtypes", [AdjustmentTypeService, DaterangepickerConfig, Router, AdjustmentService, DialogService, FormBuilder, ActivatedRoute])
    ], AdjustmentEditComponent);
    return AdjustmentEditComponent;
}(DialogComponent));
export { AdjustmentEditComponent };
//# sourceMappingURL=adjustment-edit.component.js.map