var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Adjustment } from '../../model/adjustment';
import { AdjustmentService } from '../../services/adjustment.service';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';
import { EmployeeService } from '../../services/employee.service';
import { AuthUserService } from '../../services/auth-user.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
var AdjustmentCreateEmployeeComponent = /** @class */ (function (_super) {
    __extends(AdjustmentCreateEmployeeComponent, _super);
    function AdjustmentCreateEmployeeComponent(_rt, daterangepickerOptions, _auth_service, _emp_service, dialogService, _fb, _adjust_service, _adjust_type_serv) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this._auth_service = _auth_service;
        _this._emp_service = _emp_service;
        _this._fb = _fb;
        _this._adjust_service = _adjust_service;
        _this._adjust_type_serv = _adjust_type_serv;
        _this.adjust = new Adjustment();
        _this.error = false;
        _this.errorMessage = '';
        _this.date_in = moment();
        _this.mainInput = {
            start: moment(),
            end: moment()
        };
        _this.createAdjustmentForm = _fb.group({
            'employee_id': [''],
            'adjustment_type': ['', [Validators.required]],
            'type': ['', [Validators.required]],
            'billable': ['', [Validators.required]],
            'adjustment_amount': ['', [Validators.required]],
            'date_file': [''],
            'created_by': [''],
        });
        _this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            drops: 'up'
        };
        return _this;
    }
    AdjustmentCreateEmployeeComponent.prototype.onSubmit = function () {
        var _this = this;
        var emp_len = this.emp_rec.length;
        var emp_code = [];
        for (var i = 0; i < emp_len; i++) {
            if (this.current.indexOf(this.emp_rec[i].id.toString()) > -1) {
                emp_code.push(this.emp_rec[i].id);
            }
            this.emp_code = emp_code;
        }
        this.createAdjustmentForm.patchValue({ 'employee_id': this.emp_code });
        if (this.adjustment_type == 'DEDUCTION') {
            this.createAdjustmentForm.patchValue({
                'adjustment_amount': this.createAdjustmentForm.value.adjustment_amount *= -1
            });
        }
        this.createAdjustmentForm.patchValue({
            'date_file': moment(this.date_in).format("YYYY-MM-DD")
        });
        this.createAdjustmentForm.value.created_by = this.user_id;
        var adjustment_model = this.createAdjustmentForm.value;
        this._adjust_service
            .storeAdjustment(adjustment_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            _this.success_title = "Success";
            _this.success_message = "A new adjustment record was successfully added.";
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/adjustment-list']);
        }, function (err) { return _this.catchError(err); });
    };
    AdjustmentCreateEmployeeComponent.prototype.onlyDecimalNumberKey = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    };
    AdjustmentCreateEmployeeComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        this.date = dateInput.start;
        this.date_in = this.date;
    };
    AdjustmentCreateEmployeeComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    AdjustmentCreateEmployeeComponent.prototype.ngOnInit = function () {
        var _this = this;
        var adjustment_type = this._adjust_type_serv.getAdjustmentsType()
            .subscribe(function (data) {
            _this.adjust_type_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var employee_name = this._emp_service.getEmployees()
            .subscribe(function (data) {
            _this.emp_rec = Array.from(data);
            _this.AdjustmentData = _this.emp_rec;
            _this.employee_code = _this.emp_rec.id;
            _this.value = [];
            _this.options = {
                multiple: true
            };
            _this.current = _this.value;
        }, function (err) { return console.error(err); });
        var created_by = this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
        }, function (err) { return console.error(err); });
    };
    AdjustmentCreateEmployeeComponent.prototype.changed = function (data) {
        this.current = data.value;
    };
    AdjustmentCreateEmployeeComponent.prototype.getAdjustType = function (value) {
        var _this = this;
        this.adj_id = value;
        var adjustment = this._adjust_type_serv.getAdjustmentType(this.adj_id)
            .subscribe(function (data) {
            var adjust = data;
            _this.adjustment_type = data.type;
        });
    };
    AdjustmentCreateEmployeeComponent = __decorate([
        Component({
            selector: 'admin-adjustment-create',
            templateUrl: './adjustment-create-employee.component.html',
            styleUrls: ['./adjustment-list.component.css']
        }),
        __metadata("design:paramtypes", [Router, DaterangepickerConfig, AuthUserService, EmployeeService, DialogService, FormBuilder, AdjustmentService, AdjustmentTypeService])
    ], AdjustmentCreateEmployeeComponent);
    return AdjustmentCreateEmployeeComponent;
}(DialogComponent));
export { AdjustmentCreateEmployeeComponent };
//# sourceMappingURL=adjustment-create-employee.component.js.map