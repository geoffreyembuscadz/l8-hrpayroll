var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';
import { Router } from '@angular/router';
import { Adjustment } from '../../model/adjustment';
import { AdjustmentCreateEmployeeComponent } from './adjustment-create-employee.component';
import { AdjustmentCreateCompanyComponent } from './adjustment-create-company.component';
import { AdjustmentCreateDepartmentComponent } from './adjustment-create-department.component';
import { AdjustmentCreatePositionComponent } from './adjustment-create-position.component';
import { AdjustmentEditComponent } from './adjustment-edit.component';
import { AdjustmentService } from '../../services/adjustment.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
var AdjustmentListModalComponent = /** @class */ (function (_super) {
    __extends(AdjustmentListModalComponent, _super);
    function AdjustmentListModalComponent(_rt, dialogService, _conf, modalService, _adjust_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._conf = _conf;
        _this.modalService = modalService;
        _this._adjust_service = _adjust_service;
        _this.dtTrigger = new Subject();
        _this.Adjustment = new Adjustment();
        _this.dtOptions = {};
        _this.bodyClasses = "skin-blue sidebar-mini";
        _this.body = document.getElementsByTagName('body')[0];
        return _this;
    }
    AdjustmentListModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        var adjustment = this._adjust_service.getAdjustments(this.att_id, this.start, this.end, this.type, this.no_of_days)
            .subscribe(function (data) {
            _this.ad_rec = Array.from(data);
            _this.rerender();
        }, function (err) { return _this.catchError(err); });
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    };
    AdjustmentListModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    AdjustmentListModalComponent.prototype.openEditModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(AdjustmentEditComponent, {
            title: 'Update Adjustment By Employee'
        }).subscribe(function (isConfirmed) {
            var adjustment = _this._adjust_service.getAdjustments(_this.att_id, _this.start, _this.end, _this.type, _this.no_of_days)
                .subscribe(function (data) {
                _this.ad_rec = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    AdjustmentListModalComponent.prototype.openAddModalEmployee = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(AdjustmentCreateEmployeeComponent, {
            title: 'Add Adjustment By Employee'
        }).subscribe(function (isConfirmed) {
            var adjustment = _this._adjust_service.getAdjustments(_this.att_id, _this.start, _this.end, _this.type, _this.no_of_days)
                .subscribe(function (data) {
                _this.ad_rec = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    AdjustmentListModalComponent.prototype.openAddModalCompany = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(AdjustmentCreateCompanyComponent, {
            title: 'Add Adjustment By Company'
        }).subscribe(function (isConfirmed) {
            var adjustment = _this._adjust_service.getAdjustments(_this.att_id, _this.start, _this.end, _this.type, _this.no_of_days)
                .subscribe(function (data) {
                _this.ad_rec = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    AdjustmentListModalComponent.prototype.openAddModalDepartment = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(AdjustmentCreateDepartmentComponent, {
            title: 'Add Adjustment By Department'
        }).subscribe(function (isConfirmed) {
            var adjustment = _this._adjust_service.getAdjustments(_this.att_id, _this.start, _this.end, _this.type, _this.no_of_days)
                .subscribe(function (data) {
                _this.ad_rec = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    AdjustmentListModalComponent.prototype.openAddModalPosition = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(AdjustmentCreatePositionComponent, {
            title: 'Add Adjustment By Position'
        }).subscribe(function (isConfirmed) {
            var adjustment = _this._adjust_service.getAdjustments(_this.att_id, _this.start, _this.end, _this.type, _this.no_of_days)
                .subscribe(function (data) {
                _this.ad_rec = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    AdjustmentListModalComponent.prototype.checkAdjustmentType = function (adj_type) {
        if (adj_type == 'DEDUCTION') {
            return true;
        }
    };
    AdjustmentListModalComponent.prototype.ngOnDestroy = function () {
    };
    AdjustmentListModalComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    AdjustmentListModalComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], AdjustmentListModalComponent.prototype, "dtElement", void 0);
    AdjustmentListModalComponent = __decorate([
        Component({
            selector: 'admin-adjustments-list',
            templateUrl: './adjustment-list-modal.component.html',
            styleUrls: ['./adjustment-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Router, DialogService, Configuration, DialogService, AdjustmentService])
    ], AdjustmentListModalComponent);
    return AdjustmentListModalComponent;
}(DialogComponent));
export { AdjustmentListModalComponent };
//# sourceMappingURL=adjustment-list-modal.component.js.map