var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { AdjustmentType } from '../../model/adjustment_type';
import { AdjustmentTypeService } from '../../services/adjustment-type.service';
var AdjustmentTypeCreateComponent = /** @class */ (function (_super) {
    __extends(AdjustmentTypeCreateComponent, _super);
    function AdjustmentTypeCreateComponent(_rt, dialogService, _fb, _adjust_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._fb = _fb;
        _this._adjust_service = _adjust_service;
        _this.adjust = new AdjustmentType();
        _this.adjustmentChk = false;
        _this.error = false;
        _this.errorMessage = '';
        _this.createAdjustmentTypeForm = _fb.group({
            'name': ['', [Validators.required, _this.isAlphaNumeric]],
            'display_name': [''],
            'description': [''],
            'taxable': [''],
            'billable': [''],
            'frequency': [''],
            'type': ['', [Validators.required]],
            'include_payslip': ['', [Validators.required]],
        });
        return _this;
    }
    AdjustmentTypeCreateComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.adjustmentChk == false) {
            this.createAdjustmentTypeForm.patchValue({
                'taxable': 0
            });
            this.createAdjustmentTypeForm.patchValue({
                'billable': 0
            });
            this.createAdjustmentTypeForm.patchValue({
                'frequency': 0
            });
        }
        var adjustment_model = this.createAdjustmentTypeForm.value;
        this._adjust_service
            .storeAdjustmentType(adjustment_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            _this.success_title = "Success";
            _this.success_message = "A new adjustment type record was successfully added.";
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/adjustment-type-list']);
        }, function (err) { return _this.catchError(err); });
    };
    AdjustmentTypeCreateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Adjustment name already exist.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    AdjustmentTypeCreateComponent.prototype.isAlphaNumeric = function (control) {
        if (control.value === '') {
            return { noAlphaNumeric: true };
        }
        else if (!control.value.match(/^[a-z_]+$/i)) {
            return { noAlphaNumeric: true };
        }
    };
    AdjustmentTypeCreateComponent.prototype.getType = function (value) {
        if (value == 'ADJUSTMENT') {
            this.adjustmentChk = true;
        }
        else {
            this.adjustmentChk = false;
        }
    };
    AdjustmentTypeCreateComponent.prototype.ngOnInit = function () {
    };
    AdjustmentTypeCreateComponent = __decorate([
        Component({
            selector: 'admin-adjustment-type-create',
            templateUrl: './adjustment-type-create.component.html'
        }),
        __metadata("design:paramtypes", [Router, DialogService, FormBuilder, AdjustmentTypeService])
    ], AdjustmentTypeCreateComponent);
    return AdjustmentTypeCreateComponent;
}(DialogComponent));
export { AdjustmentTypeCreateComponent };
//# sourceMappingURL=adjustment-type-create.component.js.map