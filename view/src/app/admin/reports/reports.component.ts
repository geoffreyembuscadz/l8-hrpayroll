import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { AttendanceReportService } from '../../services/attendance-report.service';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { DataTableDirective } from 'angular-datatables';
import 'rxjs/add/operator/map';
import { CommonService } from '../../services/common.service';
import { Subject } from 'rxjs/Rx';
import {MomentModule} from 'angular2-moment/moment.module';
import { Select2OptionData } from 'ng2-select2';
import * as moment from 'moment';
import { AuthUserService } from '../../services/auth-user.service';

@Component({
	selector: 'app-reports',
	templateUrl: './reports.component.html',
	styleUrls: ['./reports.component.css']
})

export class ReportsComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	public pieChartLabels:string[] = ['Present', 'Absent',  'Paid Leave', 'Unpaid Leave'];
	public pieChartData: any[] = [];
	public pieChartType:string = 'pie';

	private headers: Headers;
	dtOptions: any = {};	
	late_early: any;
	temp: any;
	perm_rec: any;
	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];
	numberOfDays = moment().daysInMonth();
	days = [];
	month: any;
	year:any;
	employee:any;
	date = [];
	statusForm: any;
	dates = new Date();
	y = this.dates.getFullYear();
	m = this.dates.getMonth();
	firstDay = new Date(this.y, this.m, 1);
	lastDay = new Date(this.y, this.m + 1, 0);
	start_date = moment(this.firstDay).format("ddd YYYY-MM-DD");
	start = moment(this.firstDay).format("MMM Do YYYY");
	now = moment().format("ddd YYYY-MM-DD");
	today = moment().format("MMM Do YYYY");
	mos = moment().format("MMMM YYYY");
	end_date = moment().format("ddd YYYY-MM-DD");
	duration:number;
	public myPieChart: any;
	isRed: any;
	isGreen: any;
	isBlue: any;
	isYellow: any;
	status: any;
	todaysDate: any;
	emp: any;
	stat: any;
	employee_id: any;
	public employee_value: Array<Select2OptionData>;
	employees: any;
	entry = moment().format('hh:mm A');
	exit = moment().format('hh:mm P');
	time_in = moment().format('hh:mm:ss');
	time_out = moment().format('hh:mm:ss');
	colorObject: any[] = [{backgroundColor:["Green", "Red", "Violet", "Pink"]}];
	diffDays: any;
	days_summmary: any;
	hours_summmary: any;
	public current: any;
	seconds: any;
	sec: any;
	ent: any;
	out: any;
	tmp: any;
	hrs: any;
	emps: any;
	empDates: Object[];		
	user_id:any;
	supervisor_id:any;
	activeEmployee: any;
	byDate = false;
	byCompany = false;
	byEmployee = false;
	byDepartment = false;
	bySort = false;
	company: any;
	value:any;
	dept: any;
	department: any;
	lateErly_fltr: any;
	public employee_current : any;
	public company_current : any;
	public options: Select2Options;
	company_value: Array<Select2OptionData>;
	department_value: Array<Select2OptionData>;
	emp_id: any;
	user_report: any;
	arrayDate=[];
	emp_by_comp: any;
	comp = false;
	tableVal = false;
	employee_name:any;
	hours = false;
	stats = false;
	late_early_per_emp: any;
	isLoading = false;
	selectedEmployee: any = null;
	per_emp_days_summary: any;
	per_emp_hours_summary: any;
	jsonParse: any;
	json_per_emp_hours_summary: any;
	working_sched: any;
	p: number = 1;
	selectedEmp: Object = {};
	
	constructor(
		dialogService: DialogService,
		private _a_r_service: AttendanceReportService,
		private _cmon_service: CommonService,
		private _ar: ActivatedRoute,
		private _rt: Router,
		private _fb: FormBuilder,
		private _common_service: CommonService,
		private _auth_service: AuthUserService
		) 
	{ 
		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");
		// Header Variables
		this.headers = new Headers();
		let authToken = localStorage.getItem('id_token');
		this.headers.append('Authorization', `Bearer ${authToken}`);
		this.headers.append('Content-Type', 'application/json');
		this.headers.append('Accept', 'application/json');
			
		this.statusForm = _fb.group({
			'start_date':	[null],
			'end_date':		[null],	
			'comp_id':		[null],
			'dept':			[null],
			'emp_id':		[null],
			'sort':			[null],
		});
	}

	public mainInput = {
		start: moment().subtract(12, 'month'),
		end: moment().subtract(11, 'month')
	}
	ngOnInit() {		
		this.getDaysArray();
		this.get_UserId();
		this.getAttendanceReport();
		this.getDaysOfMonth();	
		this.getCompany();
		this.getEmp();
		this.getDept();
		// this.workingSched();

	}

	get_UserId(){
		this._auth_service.getUser().
		subscribe(
			data => {
				let user = data;
				this.user_id = user.id;
				this.getEmployeeId();
			},
			err => console.error(err)
			);
	}

	getEmployeeId(){
		this._auth_service.getUserById(this.user_id).
		subscribe(
			data => {
				let user = data;
				this.supervisor_id = user.employee_id;
				this.getEmployee();
				this.getEmp();
			},
			err => console.error(err)
			);
	}

	ngAfterViewInit(){
		this.dtTrigger.next();
	}

	rerender(): void {
		this.dtElement.dtInstance.then((dtInstance) => {
			dtInstance.destroy();
			this.dtTrigger.next();
		});
	}

	public chartClicked(e:any):void {
		console.log(e);
	}

	public chartHovered(e:any):void {
		console.log(e);
	}

	getAttendanceReport(){
		this._a_r_service.getAttendanceReport().
		subscribe(
			data => {
				this.pieChartData = Array.from(data);
			},
			err => console.error(err)
			);
	}

	getEmployee(){
		let id = this.supervisor_id;
		this._common_service.getEmployees(id).
		subscribe(
			data => {
				this.employee = Array.from(data);
				let len = this.employee.length;
				let emp = [];
				for (var x = 0; x < len; ++x) {
					emp.push(this.employee[x].id);
				}
			},
			err => console.error(err)
			);
	}

	getDaysOfMonth(){
		let start = this.start_date;
		let end = this.end_date;
		let from = moment(this.start_date, 'YYYY-MM-DD'); 
		let to = moment(this.end_date, 'YYYY-MM-DD');   

		/* using diff */
		this.duration = to.diff(from, 'days');

		let currDate = from.clone().startOf('day');
		let lastDate = to.clone().startOf('day');

		this.date.push(currDate.toDate());

		while(currDate.add(1 ,'days').diff(lastDate) < 0) {
			this.date.push(currDate.clone().toDate());
		}

		if(start != end)  {
			this.date.push(lastDate.toDate());
		}

		this.transformDate();
	}

	transformDate(){
		var n = {}
 		for(var i = 0; i < this.date.length; i++) 
 		{
 			if (!n[this.date[i]]) 
 			{
 				n[this.date[i]] = true; 
 				this.arrayDate.push(moment(this.date[i]).format("YYYY-MM-DD")); 
 			}
 		} 
	}

	getNumberOfDays(){
		for (var i = 1; i < this.numberOfDays; ++i) {
			this.days.push(i);
		}
	}

	getDaysArray(){
		let date = new Date(this.year, this.m, 1);
		while (date.getMonth() === this.m) {
			this.days.push(new Date(date));
			date.setDate(date.getDate() + 1);
		}
	}
	perEmpLateEarly(){
		let model = this.statusForm.value;
		this.statusForm.value.emp_id = this.selectedEmployee.id;
		this.statusForm.value.start_date = moment(this.firstDay).format("YYYY-MM-DD");
		this.statusForm.value.end_date = moment().format("YYYY-MM-DD");
		this._a_r_service.perEmpLateEarly(model).
		subscribe(
			data => {
				this.late_early_per_emp = Array.from(data);
				for (var i = 0, l= this.late_early_per_emp.length; i < l; i++) {
					this.entry = this.late_early_per_emp[i].start_shift; 
					this.exit = this.late_early_per_emp[i].end_shift;
					this.time_out = this.late_early_per_emp[i].emp_out;
					this.time_in =this.late_early_per_emp[i].emp_in; 

					let diffInMs: number = Date.parse('2017-10-02 ' + this.entry) - Date.parse('2017-10-02 ' + this.time_in);
					let diffInHours: number = diffInMs / 1000 / 60 / 60; 	
					this.late_early_per_emp[i]['lateClass'] = this.checkLateEarly(diffInHours); 

					let diffInTime: number = Date.parse('2017-10-02 ' + this.exit) - Date.parse('2017-10-02 ' + this.time_out);
					let diffInHrs: number = diffInTime / 1000 / 60 / 60; 	
					this.late_early_per_emp[i]['otClass'] = this.checkEmpOut(diffInHrs);  										
				}
			},
			err => console.error(err)
			);
	}

	lateErlyFltr(){
		
		let model = this.statusForm.value;
		this._a_r_service.lateErlyFltr(model).
		subscribe(
			data => {
				this.late_early = Array.from(data);
				console.log(this.late_early);
				for (let i = 0, j = this.late_early.length; i < j; i++) {
					this.entry = this.late_early[i].start_shift;    
					this.exit = this.late_early[i].end_shift;
					this.time_out = this.late_early[i].emp_out;
					this.time_in =this.late_early[i].emp_in;
					let diffInMs: number = Date.parse('2017-10-02 ' + this.entry) - Date.parse('2017-10-02 ' + this.time_in);
					let diffInHours: number = diffInMs / 1000 / 60 / 60; 	
					this.late_early[i]['lateClass'] = this.checkLateEarly(diffInHours); 

					let diffInTime: number = Date.parse('2017-10-02 ' + this.exit) - Date.parse('2017-10-02 ' + this.time_out);
					let diffInHrs: number = diffInTime / 1000 / 60 / 60; 	
					this.late_early[i]['otClass'] = this.checkEmpOut(diffInHrs); 
				}          
				this.rerender();
			},
			err => console.error(err)
			);
	}
	checkLateEarly(diffInHours){
		let color = '';

		if(diffInHours < 0){
			color = 'red';
		} else if (diffInHours > 0) {
			color = 'green'; 
		}

		return color;			
	}

	checkEmpOut(diffInHrs){
		let kulay = '';

		if(diffInHrs < 0){
			kulay = 'violet';
		} else if (diffInHrs > 0){
			kulay = 'blue'; 
		}
		return kulay;	
	}
	
	daySummaryDefaultDate(){
		this.statusForm.value.start_date = moment(this.firstDay).format('YYYY-MM-DD');
		this.statusForm.value.end_date = moment().format('YYYY-MM-DD');
		this.getDaySummaryReport();

	}
	getDaySummaryReport(){
		
		let model = this.statusForm.value;
		this.isLoading = true;
		this._a_r_service.getDaySummaryReport(model).
		subscribe(
			data => {
				this.days_summmary = data;
				this.isLoading = false;
			},
			err => console.error(err)
			);
	}

	hourSummaryDefaultDate(){
		this.statusForm.value.start_date = moment(this.firstDay).format('YYYY-MM-DD');
		this.statusForm.value.end_date = moment().format('YYYY-MM-DD');
		this.getHoursSummaryReport();
	}

	getHoursSummaryReport(){
		let model = this.statusForm.value;	
		this.isLoading = true;
		this._a_r_service.getHoursSummaryReport(model).
		subscribe(
			data => {
				this.hours_summmary = data;
				this.isLoading = false;
			},
			err => console.error(err)
			);
	}
	
	hoursReport(id: any){
		let model = this.statusForm.value;		
		this.statusForm.value.emp_id = id;
		this.activeEmployee = id;		
		this._a_r_service.hoursReport(model).
		subscribe(
			data => {	
				this.empDates = Array.from(data);
				for (let i = 0, l = this.date.length; i < l; i++) {
					for (let j = 0; j < this.empDates.length; j++) {
						let date = moment(this.date[i]).format('YYYY-MM-DD').toString();
						if (date == this.empDates[j]["date"]) {
							this.date[i]["status"] = this.empDates[j]["status"];
						} 
					}
				}
			},
			err => console.error(err)
			);
	}

	statusReport(id: any){
		let model = this.statusForm.value;
		this.statusForm.value.emp_id = id;
		this.activeEmployee = id;
		this.isLoading = true;
		this._a_r_service.statusReport(model).
		subscribe(
			data => {
				this.empDates = Array.from(data);
				for (var a = 0, b = this.date.length; a < b; a++) {
					for (var i = 0; i < this.empDates.length; i++) {
						if (moment(this.date[a]).format('YYYY-MM-DD').toString() == this.empDates[i]["date"]){

							this.date[a]["date"] = moment(this.date[a]).format('YYYY-MM-DD').toString();
							this.date[a]["status"] = this.empDates[i]["status"];
							this.date[a]["hours_count"] = this.empDates[i]["hours_count"];

								if ( this.empDates[i]["status"] == 'P'){
									this.date[a].color = 'green-user-report';
								} else if (this.empDates[i]["status"] == 'PL') {
									this.date[a].color = 'blue-user-report'
								} else if (this.empDates[i]["status"]  == 'H') {
									this.date[a].color = 'holiday-user-report'
								}
							break;

						} else {  
							this.date[a]["date"] = moment(this.date[a]).format('YYYY-MM-DD').toString();
							this.date[a]["status"] = 'A';
							this.date[a]["hours_count"] = '0';
							this.date[a].color = 'red-user-report';
						}
					}
				}
				
				this.isLoading = false;
			},
			err => console.error(err)
			);
	}
	
	userReport(){
		let model = this.statusForm.value;	
		this.statusForm.value.emp_id = this.selectedEmployee.id;
		this.statusForm.value.comp_id = this.company_current;
		let x = moment(this.start, 'YYYY-MM-DD');
		this.statusForm.value.start_date = moment(this.firstDay).format('YYYY-MM-DD');	
		this.isLoading = true;	
		this._a_r_service.userReport(model).
		subscribe(
			data => {
				this.user_report = Array.from(data);

				for (var a = 0, b = this.date.length; a < b; a++) {
					for (var i = 0; i < this.user_report.length; i++) {
							
						if (moment(this.date[a]).format('YYYY-MM-DD').toString() == this.user_report[i]["date"]) {

							this.date[a]["date"] = moment(this.date[a]).format('YYYY-MM-DD').toString();
							this.date[a]["status"] = this.user_report[i]["status"];
							this.date[a]["time_in"] = this.user_report[i]["time_in"];
							this.date[a]["time_out"] = this.user_report[i]["time_out"];
							this.date[a]["total_hours"] = this.user_report[i]["total_hours"];
							this.date[a]["pay_hrs"] = this.user_report[i]["pay_hrs"];

							if ( this.user_report[i].status == 'Present'){
								this.date[a].color = 'green-user-report';
							} else if (this.user_report[i].status == 'Paid Leave') {
								this.date[a].color = 'blue-user-report'
							} else if (this.user_report[i].status == 'Holiday') {
								this.date[a].color = 'holiday-user-report'
							}
							break;

						} else {  
							this.date[a]["date"] = moment(this.date[a]).format('YYYY-MM-DD').toString();
							this.date[a]["time_in"] = '00:00';
							this.date[a]["time_out"] = '00:00';
							this.date[a]["total_hours"] = '00:00';
							this.date[a]["pay_hrs"] = '0';
							this.date[a]["status"] = 'Absent';
							this.date[a].color = 'red-user-report';

						}
						
					} 				

				}					
				this.isLoading = false;
			},
			err => console.error(err)
			);
	}


	choiceFilter(id){

		if(id == 1){
			this.byDate = true;
			this.byCompany = true;
			this.byEmployee = true;
			this.byDepartment = true;
		} else if ( id == 2) {
			this.byDate = true;
		} else if ( id == 3) {
			this.byCompany = true;
		} else if ( id == 4) {
			this.byDepartment = true;
		} else if ( id == 5) {
			this.byEmployee = true;
			this.lateErlyFltr();
		} else {
			this.byDate = false;
			this.byCompany = false;
			this.byEmployee = false;
			this.bySort = false;

			this.statusForm.value.start_date = null;
			this.statusForm.value.end_date = null;
			this.statusForm.value.comp = null;
			this.statusForm.value.dept = null;
			this.statusForm.value.emp_filter = null;
			this.statusForm.value.sort = null;
		}
	}

	filterDate(value:any, dateInput:any) {
		dateInput.start = value.start;
		dateInput.end = value.end;

		let start_date = moment(dateInput.start).format("YYYY-MM-DD");
		let end_date = moment(dateInput.end).format("YYYY-MM-DD");
		
		this.statusForm.value.start_date = start_date;
		this.statusForm.value.end_date = end_date;


		this.start = moment( start_date).format("MMM Do YYYY");
		this.today = moment( end_date).format("MMM Do YYYY");

		this.getDaySummaryReport();
		this.getHoursSummaryReport();
	}

	getCompany(){
		this._common_service.getCompany()
		.subscribe(
			data => {
				this.company = Array.from(data);
				this.company_value = [];
				let id = 0;
		        let text = 'Select Company';

		        this.company.unshift({id,text});
				this.company_current = this.company_value;				
				this.company_value = this.value;
			},
			err => console.error(err)
			);
	}

	changedCompany(data: any) {
		if(data.value != 0){
			this.company_current = data.value;		
			this.getEmpByComp();
		} else {
			this.company_current = null;
		}
		
	}

	getEmpByComp(){
		this.comp = true;		
		let company_id = this.company_current;
		this.isLoading = true;
		this._a_r_service.getEmpByComp(company_id).
		subscribe(
			data => {
				this.emp_by_comp = Array.from(data);
				this.employees = this.emp_by_comp;
				let id = 0;
		        let text = 'Select Employee';

		        this.emp_by_comp.unshift({id,text});
				this.current = this.employee_value;
				this.isLoading = false;
			},
			err => console.error(err)
			);
	}
	
	changeRprt(data: any){
		if(data.value != 0){
		this.selectedEmp = data;
			this.current = data.value;
			this.tableVal = true;
			for (let i = 0; i < this.emp_by_comp.length; ++i) {
				if(data.value == this.emp_by_comp[i].id){
					this.employee_name = this.emp_by_comp[i].text;
				}
			}
			this.userReport();
		} else {
			this.current = null;
		}
	}

	getEmp(){
		let id = this.employee_id;
		this._common_service.getEmployees(id).
		subscribe(
			data => {
				this.emp = Array.from(data);
				this.employees = this.emp;	
				let id = 0;
		        let text = 'Select Employee';

		        this.emp.unshift({id,text});
				this.current = this.employee_value;
			},
			err => console.error(err)
			);
	}

	changedEmployee(data: any) {
		this.current = data.value;
		if(this.current == 0){
			this.statusForm.value.emp_id = null;
		} else {
			this.statusForm.value.emp_id = this.current;
		}
		this.lateErlyFltr();
	}

	getDept(){
		let id = this.employee_id;
		this._common_service.getDepartment().
		subscribe(
			data => {
				this.dept = Array.from(data);
				this.department = this.dept;
				this.department_value = [];	
				this.current = this.department_value;
			},
			err => console.error(err)
			);
	}

	changedDepartment(data: any) {
		this.current = data.value;
	}

	ifNaN(a: any, b: any){
		a = parseInt(a);
		b = parseInt(b);
		let tmp = (isNaN(a + b) || a + b == '' ? '0' : a + b);
		return tmp;
	}

	report(id){

		if(id == 1){
			this.stats = true;
			this.hours = false;
		} else if ( id == 2) {
			this.hours = true;
			this.stats = false;
		} else {
			this.stats = false;
			this.hours = false;
		}
	}
		
	setActiveEmployee(employee: any) {
		this.selectedEmployee = employee;
		let myJSON = JSON.stringify(this.selectedEmployee);
		this.perEmpLateEarly();
		this.workingSched(employee.id);
	}

	perEmpDaysSummary(){
		let model = this.statusForm.value;
		this.statusForm.value.emp_id = this.selectedEmployee.id;
		this.statusForm.value.start_date = moment(this.firstDay).format("YYYY-MM-DD");
		this.statusForm.value.end_date = moment().format("YYYY-MM-DD");			
		this._a_r_service.perEmpDaysSummary(model).
		subscribe(
			data => {
				this.per_emp_days_summary = data;

				this.jsonParse = JSON.parse(JSON.stringify(this.per_emp_days_summary));
			},
			err => console.error(err)
			);
	}

	perEmpHoursSummary(){
		let model = this.statusForm.value;
		this.statusForm.value.emp_id = this.selectedEmployee.id;
		this.statusForm.value.start_date = moment(this.firstDay).format("YYYY-MM-DD");
		this.statusForm.value.end_date = moment().format("YYYY-MM-DD");			
		this._a_r_service.perEmpHoursSummary(model).
		subscribe(
			data => {
				this.per_emp_hours_summary = data;

				this.json_per_emp_hours_summary = JSON.parse(JSON.stringify(this.per_emp_hours_summary));
				console.log(this.json_per_emp_hours_summary);
			},
			err => console.error(err)
			);
	}

	getLateEarlyReport(){
		let id = this.employee_id;
		this._a_r_service.getLateEarlyReport().
		subscribe(
			data => {
				this.late_early = Array.from(data);
				for (let i = 0, j = this.late_early.length; i < j; i++) {
					this.entry = this.late_early[i].start_shift;    
					this.exit = this.late_early[i].end_shift;
					this.time_out = this.late_early[i].emp_out;
					this.time_in =this.late_early[i].emp_in;
					let diffInMs: number = Date.parse('2017-10-02 ' + this.entry) - Date.parse('2017-10-02 ' + this.time_in);
					let diffInHours: number = diffInMs / 1000 / 60 / 60; 	
					this.late_early[i]['lateClass'] = this.checkLateEarly(diffInHours); 

					let diffInTime: number = Date.parse('2017-10-02 ' + this.exit) - Date.parse('2017-10-02 ' + this.time_out);
					let diffInHrs: number = diffInTime / 1000 / 60 / 60; 	
					this.late_early[i]['otClass'] = this.checkEmpOut(diffInHrs); 
				}       
				this.rerender();
			},
			err => console.error(err)
			);
	}

	resetSelectedEmployee(emp) {
		emp = null;
	}

	workingSched(id){
		let model = this.statusForm.value;
		this.statusForm.value.emp_id = id;
		this._a_r_service.workingSched(model).
		subscribe(
			data => {
				this.working_sched = Array.from(data);
			},
			err => console.error(err)
			);
	}


}