var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { AttendanceReportService } from '../../services/attendance-report.service';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DialogService } from "ng2-bootstrap-modal";
import { DataTableDirective } from 'angular-datatables';
import 'rxjs/add/operator/map';
import { CommonService } from '../../services/common.service';
import { Subject } from 'rxjs/Rx';
import * as moment from 'moment';
import { AuthUserService } from '../../services/auth-user.service';
var ReportsComponent = /** @class */ (function () {
    function ReportsComponent(dialogService, _a_r_service, _cmon_service, _ar, _rt, _fb, _common_service, _auth_service) {
        this._a_r_service = _a_r_service;
        this._cmon_service = _cmon_service;
        this._ar = _ar;
        this._rt = _rt;
        this._fb = _fb;
        this._common_service = _common_service;
        this._auth_service = _auth_service;
        this.dtTrigger = new Subject();
        this.pieChartLabels = ['Present', 'Absent', 'Paid Leave', 'Unpaid Leave'];
        this.pieChartData = [];
        this.pieChartType = 'pie';
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.numberOfDays = moment().daysInMonth();
        this.days = [];
        this.date = [];
        this.dates = new Date();
        this.y = this.dates.getFullYear();
        this.m = this.dates.getMonth();
        this.firstDay = new Date(this.y, this.m, 1);
        this.lastDay = new Date(this.y, this.m + 1, 0);
        this.start_date = moment(this.firstDay).format("ddd YYYY-MM-DD");
        this.start = moment(this.firstDay).format("MMM Do YYYY");
        this.now = moment().format("ddd YYYY-MM-DD");
        this.today = moment().format("MMM Do YYYY");
        this.mos = moment().format("MMMM YYYY");
        this.end_date = moment().format("ddd YYYY-MM-DD");
        this.entry = moment().format('hh:mm A');
        this.exit = moment().format('hh:mm P');
        this.time_in = moment().format('hh:mm:ss');
        this.time_out = moment().format('hh:mm:ss');
        this.colorObject = [{ backgroundColor: ["Green", "Red", "Violet", "Pink"] }];
        this.byDate = false;
        this.byCompany = false;
        this.byEmployee = false;
        this.byDepartment = false;
        this.bySort = false;
        this.arrayDate = [];
        this.comp = false;
        this.tableVal = false;
        this.hours = false;
        this.stats = false;
        this.isLoading = false;
        this.selectedEmployee = null;
        this.p = 1;
        this.selectedEmp = {};
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.statusForm = _fb.group({
            'start_date': [null],
            'end_date': [null],
            'comp_id': [null],
            'dept': [null],
            'emp_id': [null],
            'sort': [null],
        });
    }
    ReportsComponent.prototype.ngOnInit = function () {
        this.getDaysArray();
        this.get_UserId();
        this.getAttendanceReport();
        this.getDaysOfMonth();
        this.getCompany();
        this.getEmp();
        this.getDept();
        // this.workingSched();
    };
    ReportsComponent.prototype.get_UserId = function () {
        var _this = this;
        this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
            _this.getEmployeeId();
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.getEmployeeId = function () {
        var _this = this;
        this._auth_service.getUserById(this.user_id).
            subscribe(function (data) {
            var user = data;
            _this.supervisor_id = user.employee_id;
            _this.getEmployee();
            _this.getEmp();
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    ReportsComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    ReportsComponent.prototype.chartClicked = function (e) {
        console.log(e);
    };
    ReportsComponent.prototype.chartHovered = function (e) {
        console.log(e);
    };
    ReportsComponent.prototype.getAttendanceReport = function () {
        var _this = this;
        this._a_r_service.getAttendanceReport().
            subscribe(function (data) {
            _this.pieChartData = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.getEmployee = function () {
        var _this = this;
        var id = this.supervisor_id;
        this._common_service.getEmployees(id).
            subscribe(function (data) {
            _this.employee = Array.from(data);
            var len = _this.employee.length;
            var emp = [];
            for (var x = 0; x < len; ++x) {
                emp.push(_this.employee[x].id);
            }
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.getDaysOfMonth = function () {
        var start = this.start_date;
        var end = this.end_date;
        var from = moment(this.start_date, 'YYYY-MM-DD');
        var to = moment(this.end_date, 'YYYY-MM-DD');
        /* using diff */
        this.duration = to.diff(from, 'days');
        var currDate = from.clone().startOf('day');
        var lastDate = to.clone().startOf('day');
        this.date.push(currDate.toDate());
        while (currDate.add(1, 'days').diff(lastDate) < 0) {
            this.date.push(currDate.clone().toDate());
        }
        if (start != end) {
            this.date.push(lastDate.toDate());
        }
        this.transformDate();
    };
    ReportsComponent.prototype.transformDate = function () {
        var n = {};
        for (var i = 0; i < this.date.length; i++) {
            if (!n[this.date[i]]) {
                n[this.date[i]] = true;
                this.arrayDate.push(moment(this.date[i]).format("YYYY-MM-DD"));
            }
        }
    };
    ReportsComponent.prototype.getNumberOfDays = function () {
        for (var i = 1; i < this.numberOfDays; ++i) {
            this.days.push(i);
        }
    };
    ReportsComponent.prototype.getDaysArray = function () {
        var date = new Date(this.year, this.m, 1);
        while (date.getMonth() === this.m) {
            this.days.push(new Date(date));
            date.setDate(date.getDate() + 1);
        }
    };
    ReportsComponent.prototype.perEmpLateEarly = function () {
        var _this = this;
        var model = this.statusForm.value;
        this.statusForm.value.emp_id = this.selectedEmployee.id;
        this.statusForm.value.start_date = moment(this.firstDay).format("YYYY-MM-DD");
        this.statusForm.value.end_date = moment().format("YYYY-MM-DD");
        this._a_r_service.perEmpLateEarly(model).
            subscribe(function (data) {
            _this.late_early_per_emp = Array.from(data);
            for (var i = 0, l = _this.late_early_per_emp.length; i < l; i++) {
                _this.entry = _this.late_early_per_emp[i].start_shift;
                _this.exit = _this.late_early_per_emp[i].end_shift;
                _this.time_out = _this.late_early_per_emp[i].emp_out;
                _this.time_in = _this.late_early_per_emp[i].emp_in;
                var diffInMs = Date.parse('2017-10-02 ' + _this.entry) - Date.parse('2017-10-02 ' + _this.time_in);
                var diffInHours = diffInMs / 1000 / 60 / 60;
                _this.late_early_per_emp[i]['lateClass'] = _this.checkLateEarly(diffInHours);
                var diffInTime = Date.parse('2017-10-02 ' + _this.exit) - Date.parse('2017-10-02 ' + _this.time_out);
                var diffInHrs = diffInTime / 1000 / 60 / 60;
                _this.late_early_per_emp[i]['otClass'] = _this.checkEmpOut(diffInHrs);
            }
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.lateErlyFltr = function () {
        var _this = this;
        var model = this.statusForm.value;
        this._a_r_service.lateErlyFltr(model).
            subscribe(function (data) {
            _this.late_early = Array.from(data);
            console.log(_this.late_early);
            for (var i = 0, j = _this.late_early.length; i < j; i++) {
                _this.entry = _this.late_early[i].start_shift;
                _this.exit = _this.late_early[i].end_shift;
                _this.time_out = _this.late_early[i].emp_out;
                _this.time_in = _this.late_early[i].emp_in;
                var diffInMs = Date.parse('2017-10-02 ' + _this.entry) - Date.parse('2017-10-02 ' + _this.time_in);
                var diffInHours = diffInMs / 1000 / 60 / 60;
                _this.late_early[i]['lateClass'] = _this.checkLateEarly(diffInHours);
                var diffInTime = Date.parse('2017-10-02 ' + _this.exit) - Date.parse('2017-10-02 ' + _this.time_out);
                var diffInHrs = diffInTime / 1000 / 60 / 60;
                _this.late_early[i]['otClass'] = _this.checkEmpOut(diffInHrs);
            }
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.checkLateEarly = function (diffInHours) {
        var color = '';
        if (diffInHours < 0) {
            color = 'red';
        }
        else if (diffInHours > 0) {
            color = 'green';
        }
        return color;
    };
    ReportsComponent.prototype.checkEmpOut = function (diffInHrs) {
        var kulay = '';
        if (diffInHrs < 0) {
            kulay = 'violet';
        }
        else if (diffInHrs > 0) {
            kulay = 'blue';
        }
        return kulay;
    };
    ReportsComponent.prototype.daySummaryDefaultDate = function () {
        this.statusForm.value.start_date = moment(this.firstDay).format('YYYY-MM-DD');
        this.statusForm.value.end_date = moment().format('YYYY-MM-DD');
        this.getDaySummaryReport();
    };
    ReportsComponent.prototype.getDaySummaryReport = function () {
        var _this = this;
        var model = this.statusForm.value;
        this.isLoading = true;
        this._a_r_service.getDaySummaryReport(model).
            subscribe(function (data) {
            _this.days_summmary = data;
            _this.isLoading = false;
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.hourSummaryDefaultDate = function () {
        this.statusForm.value.start_date = moment(this.firstDay).format('YYYY-MM-DD');
        this.statusForm.value.end_date = moment().format('YYYY-MM-DD');
        this.getHoursSummaryReport();
    };
    ReportsComponent.prototype.getHoursSummaryReport = function () {
        var _this = this;
        var model = this.statusForm.value;
        this.isLoading = true;
        this._a_r_service.getHoursSummaryReport(model).
            subscribe(function (data) {
            _this.hours_summmary = data;
            _this.isLoading = false;
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.hoursReport = function (id) {
        var _this = this;
        var model = this.statusForm.value;
        this.statusForm.value.emp_id = id;
        this.activeEmployee = id;
        this._a_r_service.hoursReport(model).
            subscribe(function (data) {
            _this.empDates = Array.from(data);
            for (var i = 0, l = _this.date.length; i < l; i++) {
                for (var j = 0; j < _this.empDates.length; j++) {
                    var date = moment(_this.date[i]).format('YYYY-MM-DD').toString();
                    if (date == _this.empDates[j]["date"]) {
                        _this.date[i]["status"] = _this.empDates[j]["status"];
                    }
                }
            }
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.statusReport = function (id) {
        var _this = this;
        var model = this.statusForm.value;
        this.statusForm.value.emp_id = id;
        this.activeEmployee = id;
        this.isLoading = true;
        this._a_r_service.statusReport(model).
            subscribe(function (data) {
            _this.empDates = Array.from(data);
            for (var a = 0, b = _this.date.length; a < b; a++) {
                for (var i = 0; i < _this.empDates.length; i++) {
                    if (moment(_this.date[a]).format('YYYY-MM-DD').toString() == _this.empDates[i]["date"]) {
                        _this.date[a]["date"] = moment(_this.date[a]).format('YYYY-MM-DD').toString();
                        _this.date[a]["status"] = _this.empDates[i]["status"];
                        _this.date[a]["hours_count"] = _this.empDates[i]["hours_count"];
                        if (_this.empDates[i]["status"] == 'P') {
                            _this.date[a].color = 'green-user-report';
                        }
                        else if (_this.empDates[i]["status"] == 'PL') {
                            _this.date[a].color = 'blue-user-report';
                        }
                        else if (_this.empDates[i]["status"] == 'H') {
                            _this.date[a].color = 'holiday-user-report';
                        }
                        break;
                    }
                    else {
                        _this.date[a]["date"] = moment(_this.date[a]).format('YYYY-MM-DD').toString();
                        _this.date[a]["status"] = 'A';
                        _this.date[a]["hours_count"] = '0';
                        _this.date[a].color = 'red-user-report';
                    }
                }
            }
            _this.isLoading = false;
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.userReport = function () {
        var _this = this;
        var model = this.statusForm.value;
        this.statusForm.value.emp_id = this.selectedEmployee.id;
        this.statusForm.value.comp_id = this.company_current;
        var x = moment(this.start, 'YYYY-MM-DD');
        this.statusForm.value.start_date = moment(this.firstDay).format('YYYY-MM-DD');
        this.isLoading = true;
        this._a_r_service.userReport(model).
            subscribe(function (data) {
            _this.user_report = Array.from(data);
            for (var a = 0, b = _this.date.length; a < b; a++) {
                for (var i = 0; i < _this.user_report.length; i++) {
                    if (moment(_this.date[a]).format('YYYY-MM-DD').toString() == _this.user_report[i]["date"]) {
                        _this.date[a]["date"] = moment(_this.date[a]).format('YYYY-MM-DD').toString();
                        _this.date[a]["status"] = _this.user_report[i]["status"];
                        _this.date[a]["time_in"] = _this.user_report[i]["time_in"];
                        _this.date[a]["time_out"] = _this.user_report[i]["time_out"];
                        _this.date[a]["total_hours"] = _this.user_report[i]["total_hours"];
                        _this.date[a]["pay_hrs"] = _this.user_report[i]["pay_hrs"];
                        if (_this.user_report[i].status == 'Present') {
                            _this.date[a].color = 'green-user-report';
                        }
                        else if (_this.user_report[i].status == 'Paid Leave') {
                            _this.date[a].color = 'blue-user-report';
                        }
                        else if (_this.user_report[i].status == 'Holiday') {
                            _this.date[a].color = 'holiday-user-report';
                        }
                        break;
                    }
                    else {
                        _this.date[a]["date"] = moment(_this.date[a]).format('YYYY-MM-DD').toString();
                        _this.date[a]["time_in"] = '00:00';
                        _this.date[a]["time_out"] = '00:00';
                        _this.date[a]["total_hours"] = '00:00';
                        _this.date[a]["pay_hrs"] = '0';
                        _this.date[a]["status"] = 'Absent';
                        _this.date[a].color = 'red-user-report';
                    }
                }
            }
            _this.isLoading = false;
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.choiceFilter = function (id) {
        if (id == 1) {
            this.byDate = true;
            this.byCompany = true;
            this.byEmployee = true;
            this.byDepartment = true;
        }
        else if (id == 2) {
            this.byDate = true;
        }
        else if (id == 3) {
            this.byCompany = true;
        }
        else if (id == 4) {
            this.byDepartment = true;
        }
        else if (id == 5) {
            this.byEmployee = true;
            this.lateErlyFltr();
        }
        else {
            this.byDate = false;
            this.byCompany = false;
            this.byEmployee = false;
            this.bySort = false;
            this.statusForm.value.start_date = null;
            this.statusForm.value.end_date = null;
            this.statusForm.value.comp = null;
            this.statusForm.value.dept = null;
            this.statusForm.value.emp_filter = null;
            this.statusForm.value.sort = null;
        }
    };
    ReportsComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.statusForm.value.start_date = start_date;
        this.statusForm.value.end_date = end_date;
        this.start = moment(start_date).format("MMM Do YYYY");
        this.today = moment(end_date).format("MMM Do YYYY");
        this.getDaySummaryReport();
        this.getHoursSummaryReport();
    };
    ReportsComponent.prototype.getCompany = function () {
        var _this = this;
        this._common_service.getCompany()
            .subscribe(function (data) {
            _this.company = Array.from(data);
            _this.company_value = [];
            var id = 0;
            var text = 'Select Company';
            _this.company.unshift({ id: id, text: text });
            _this.company_current = _this.company_value;
            _this.company_value = _this.value;
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.changedCompany = function (data) {
        if (data.value != 0) {
            this.company_current = data.value;
            this.getEmpByComp();
        }
        else {
            this.company_current = null;
        }
    };
    ReportsComponent.prototype.getEmpByComp = function () {
        var _this = this;
        this.comp = true;
        var company_id = this.company_current;
        this.isLoading = true;
        this._a_r_service.getEmpByComp(company_id).
            subscribe(function (data) {
            _this.emp_by_comp = Array.from(data);
            _this.employees = _this.emp_by_comp;
            var id = 0;
            var text = 'Select Employee';
            _this.emp_by_comp.unshift({ id: id, text: text });
            _this.current = _this.employee_value;
            _this.isLoading = false;
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.changeRprt = function (data) {
        if (data.value != 0) {
            this.selectedEmp = data;
            this.current = data.value;
            this.tableVal = true;
            for (var i = 0; i < this.emp_by_comp.length; ++i) {
                if (data.value == this.emp_by_comp[i].id) {
                    this.employee_name = this.emp_by_comp[i].text;
                }
            }
            this.userReport();
        }
        else {
            this.current = null;
        }
    };
    ReportsComponent.prototype.getEmp = function () {
        var _this = this;
        var id = this.employee_id;
        this._common_service.getEmployees(id).
            subscribe(function (data) {
            _this.emp = Array.from(data);
            _this.employees = _this.emp;
            var id = 0;
            var text = 'Select Employee';
            _this.emp.unshift({ id: id, text: text });
            _this.current = _this.employee_value;
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.changedEmployee = function (data) {
        this.current = data.value;
        if (this.current == 0) {
            this.statusForm.value.emp_id = null;
        }
        else {
            this.statusForm.value.emp_id = this.current;
        }
        this.lateErlyFltr();
    };
    ReportsComponent.prototype.getDept = function () {
        var _this = this;
        var id = this.employee_id;
        this._common_service.getDepartment().
            subscribe(function (data) {
            _this.dept = Array.from(data);
            _this.department = _this.dept;
            _this.department_value = [];
            _this.current = _this.department_value;
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.changedDepartment = function (data) {
        this.current = data.value;
    };
    ReportsComponent.prototype.ifNaN = function (a, b) {
        a = parseInt(a);
        b = parseInt(b);
        var tmp = (isNaN(a + b) || a + b == '' ? '0' : a + b);
        return tmp;
    };
    ReportsComponent.prototype.report = function (id) {
        if (id == 1) {
            this.stats = true;
            this.hours = false;
        }
        else if (id == 2) {
            this.hours = true;
            this.stats = false;
        }
        else {
            this.stats = false;
            this.hours = false;
        }
    };
    ReportsComponent.prototype.setActiveEmployee = function (employee) {
        this.selectedEmployee = employee;
        var myJSON = JSON.stringify(this.selectedEmployee);
        this.perEmpLateEarly();
        this.workingSched(employee.id);
    };
    ReportsComponent.prototype.perEmpDaysSummary = function () {
        var _this = this;
        var model = this.statusForm.value;
        this.statusForm.value.emp_id = this.selectedEmployee.id;
        this.statusForm.value.start_date = moment(this.firstDay).format("YYYY-MM-DD");
        this.statusForm.value.end_date = moment().format("YYYY-MM-DD");
        this._a_r_service.perEmpDaysSummary(model).
            subscribe(function (data) {
            _this.per_emp_days_summary = data;
            _this.jsonParse = JSON.parse(JSON.stringify(_this.per_emp_days_summary));
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.perEmpHoursSummary = function () {
        var _this = this;
        var model = this.statusForm.value;
        this.statusForm.value.emp_id = this.selectedEmployee.id;
        this.statusForm.value.start_date = moment(this.firstDay).format("YYYY-MM-DD");
        this.statusForm.value.end_date = moment().format("YYYY-MM-DD");
        this._a_r_service.perEmpHoursSummary(model).
            subscribe(function (data) {
            _this.per_emp_hours_summary = data;
            _this.json_per_emp_hours_summary = JSON.parse(JSON.stringify(_this.per_emp_hours_summary));
            console.log(_this.json_per_emp_hours_summary);
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.getLateEarlyReport = function () {
        var _this = this;
        var id = this.employee_id;
        this._a_r_service.getLateEarlyReport().
            subscribe(function (data) {
            _this.late_early = Array.from(data);
            for (var i = 0, j = _this.late_early.length; i < j; i++) {
                _this.entry = _this.late_early[i].start_shift;
                _this.exit = _this.late_early[i].end_shift;
                _this.time_out = _this.late_early[i].emp_out;
                _this.time_in = _this.late_early[i].emp_in;
                var diffInMs = Date.parse('2017-10-02 ' + _this.entry) - Date.parse('2017-10-02 ' + _this.time_in);
                var diffInHours = diffInMs / 1000 / 60 / 60;
                _this.late_early[i]['lateClass'] = _this.checkLateEarly(diffInHours);
                var diffInTime = Date.parse('2017-10-02 ' + _this.exit) - Date.parse('2017-10-02 ' + _this.time_out);
                var diffInHrs = diffInTime / 1000 / 60 / 60;
                _this.late_early[i]['otClass'] = _this.checkEmpOut(diffInHrs);
            }
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    ReportsComponent.prototype.resetSelectedEmployee = function (emp) {
        emp = null;
    };
    ReportsComponent.prototype.workingSched = function (id) {
        var _this = this;
        var model = this.statusForm.value;
        this.statusForm.value.emp_id = id;
        this._a_r_service.workingSched(model).
            subscribe(function (data) {
            _this.working_sched = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], ReportsComponent.prototype, "dtElement", void 0);
    ReportsComponent = __decorate([
        Component({
            selector: 'app-reports',
            templateUrl: './reports.component.html',
            styleUrls: ['./reports.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            AttendanceReportService,
            CommonService,
            ActivatedRoute,
            Router,
            FormBuilder,
            CommonService,
            AuthUserService])
    ], ReportsComponent);
    return ReportsComponent;
}());
export { ReportsComponent };
//# sourceMappingURL=reports.component.js.map