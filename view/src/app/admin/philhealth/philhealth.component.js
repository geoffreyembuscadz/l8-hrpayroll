var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable } from '@angular/core';
import { PhilhealthService } from '../../services/philhealth.service';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Philhealth } from '../../model/philhealth';
import { DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import 'rxjs/add/operator/map';
var PhilhealthComponent = /** @class */ (function () {
    function PhilhealthComponent(dialogService, _perms_service, modalService, _ar, _philhealth_service, _rt, _fb, _conf) {
        this._perms_service = _perms_service;
        this.modalService = modalService;
        this._ar = _ar;
        this._philhealth_service = _philhealth_service;
        this._rt = _rt;
        this._fb = _fb;
        this._conf = _conf;
        this.philhealth = new Philhealth();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.api_philhealth = this._conf.ServerWithApiUrl + 'philhealth/';
    }
    PhilhealthComponent.prototype.ngAfterViewInit = function () {
    };
    PhilhealthComponent.prototype.showMe = function (philhealths) {
        this.philhealth_rec = philhealths;
    };
    PhilhealthComponent.prototype.ngOnInit = function () {
        var _this = this;
        var philhealths = this._perms_service.getPermissions().
            subscribe(function (data) {
            _this.perm_rec = Array.from(data); // fetched record
        }, function (err) { return console.error(err); });
        var authToken = localStorage.getItem('id_token');
        // DataTable Configuration
        this.dtOptions = {
            ajax: {
                url: this.api_philhealth,
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + authToken);
                },
            },
            columns: [{
                    title: 'Bracket ID',
                    data: 'bracket_id'
                }, {
                    title: 'Salary Base',
                    data: 'salary_base'
                }, {
                    title: 'Salary Ceiling',
                    data: 'salary_ceiling'
                }, {
                    title: 'Employee Share',
                    data: 'employee_share'
                }, {
                    title: 'Employer Share',
                    data: 'employer_share'
                }, {
                    title: 'Total Monthly Premium',
                    data: 'total_monthly_premium'
                }],
            rowCallback: function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
                var self = _this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', function () {
                    // this._rt.navigate(['admin/role-edit/', data.id]);
                    // console.log(oData.id);
                    // this.philhealth_id = data.id;
                    // console.log(this.philhealth_id);
                });
                return nRow;
            }
        };
    };
    PhilhealthComponent = __decorate([
        Component({
            selector: 'app-philhealth',
            templateUrl: './philhealth.component.html',
            styleUrls: ['./philhealth.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService, PermissionService, DialogService, ActivatedRoute, PhilhealthService, Router, FormBuilder, Configuration])
    ], PhilhealthComponent);
    return PhilhealthComponent;
}());
export { PhilhealthComponent };
//# sourceMappingURL=philhealth.component.js.map