import { Component, OnInit, Injectable } from '@angular/core';
import { PhilhealthService } from '../../services/philhealth.service';

import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Philhealth } from '../../model/philhealth';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';



@Component({
  selector: 'app-philhealth',
  templateUrl: './philhealth.component.html',
  styleUrls: ['./philhealth.component.css']
})

@Injectable()
export class PhilhealthComponent implements OnInit {


  	public id: string;
	public philhealth_id: any;
	public error_title: string;
	public error_message: string;
	public success_title;
	public success_message;
	public poststore: any;
	public philhealth = new Philhealth();
	philhealthForm: FormGroup;

	// TEMP variables
	public confirm_archiving: any;
	public form


	private headers: Headers;
	dtOptions: any = {};
	philhealth_rec: any;
	// get permissions list
    perm_rec: any;
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	api_philhealth: String;

  	constructor(dialogService: DialogService, private _perms_service: PermissionService, private modalService: DialogService, private _ar: ActivatedRoute, private _philhealth_service: PhilhealthService, private _rt: Router, private _fb: FormBuilder, private _conf: Configuration){
		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	      // Header Variables
		this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');

        this.api_philhealth = this._conf.ServerWithApiUrl + 'philhealth/';


	}

	ngAfterViewInit(){

	}

	showMe(philhealths: any){
		this.philhealth_rec = philhealths;
	}

	ngOnInit() {


		let philhealths = this._perms_service.getPermissions().
		subscribe(
			data => {
				this.perm_rec = Array.from(data); // fetched record
			},
			err => console.error(err)
		);

		let authToken = localStorage.getItem('id_token');


		// DataTable Configuration
		this.dtOptions = {
			ajax:
			{
				url: this.api_philhealth,
				type: "GET",
				beforeSend: function(xhr){
                            xhr.setRequestHeader("Authorization",
                            "Bearer " + authToken);
                },
			},
			columns: [{
				
				title: 'Bracket ID',
				data: 'bracket_id'
				  },{
				title: 'Salary Base',
				data: 'salary_base'
				 },{
				title: 'Salary Ceiling',
				data: 'salary_ceiling'
				 },{
				title: 'Employee Share',
				data: 'employee_share'
				 },{
				title: 'Employer Share',
				data: 'employer_share'
				 },{
				title: 'Total Monthly Premium',
				data: 'total_monthly_premium'
				 }],
			rowCallback: (nRow: number, data: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
				let self = this;
				// Unbind first in order to avoid any duplicate handler
				// (see https://github.com/l-lin/angular-datatables/issues/87)
				$('td', nRow).unbind('click');
				$('td', nRow).bind('click', () => {
					// this._rt.navigate(['admin/role-edit/', data.id]);
				    // console.log(oData.id);
				    // this.philhealth_id = data.id;
				    // console.log(this.philhealth_id);
				   
					

				});
				return nRow;
			}
		};
		
	}

}
