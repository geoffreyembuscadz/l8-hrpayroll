var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Company } from '../../model/company';
import { CompanyBranch } from '../../model/company_branch';
import { CompanyService } from '../../services/company.service';
var URL = '';
var CompanyBranchEditComponent = /** @class */ (function () {
    function CompanyBranchEditComponent(_ar, _conf, _company_service, _rt, _fb) {
        var _this = this;
        this._ar = _ar;
        this._conf = _conf;
        this._company_service = _company_service;
        this._rt = _rt;
        this._fb = _fb;
        this.company = new Company();
        this.company_branch = new CompanyBranch();
        this.message_title = '';
        this.message_success = '';
        this.message_error_title = '';
        this.message_error = '';
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', "Bearer " + authToken);
        // Company API modes
        this.api_company = this._conf.ServerWithApiUrl + 'company/';
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.company_id = this._ar.snapshot.params['id'];
        this._company_service.getCompany(this.company_id).subscribe(function (data) {
            _this.setCompany(data);
        }, function (err) { return console.error(err); });
        this.updateCompanyBranchesForm = _fb.group({});
    }
    CompanyBranchEditComponent.prototype.ngOnInit = function () {
    };
    CompanyBranchEditComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    CompanyBranchEditComponent.prototype.setCompany = function (company) {
        this.company = company;
        this.company_id = this.company.id;
        this.form_company_branches = this.company.branches;
        var company_branch_index_count = 0;
        for (var _i = 0, _a = this.form_company_branches; _i < _a.length; _i++) {
            var company_branch = _a[_i];
            this.company_branch.id[company_branch_index_count] = company_branch.id;
            this.company_branch.branch_name[company_branch_index_count] = company_branch.branch_name;
            this.company_branch.branch_address[company_branch_index_count] = company_branch.branch_address;
            this.company_branch.contact_number[company_branch_index_count] = company_branch.contact_number;
            this.company_branch.contact_person[company_branch_index_count] = company_branch.contact_person;
            company_branch_index_count++;
        }
    };
    CompanyBranchEditComponent.prototype.addCompanyBranchRow = function () {
        var company_branch_new_row = {
            'branch_name': '',
            'id': '',
            'branch_address': '',
            'contact_number': '',
            'contact_person': ''
        };
        var company_branches = this.company.branches;
        company_branches.push(company_branch_new_row);
        this.form_company_branches = company_branches;
        this.message_title = '';
        this.message_success = '';
        this.message_error_title = '';
        this.message_error = '';
    };
    CompanyBranchEditComponent.prototype.removeCompanyBranchRow = function (row_data_index) {
        var array_company_branches = this.form_company_branches;
        array_company_branches.splice(row_data_index, 1);
        this.form_company_branches = array_company_branches;
        document.getElementById("row-company-branch-" + row_data_index).outerHTML = '';
    };
    CompanyBranchEditComponent.prototype.fetchUpdateReponse = function (data) {
        var _this = this;
        this.message_title = 'Success!';
        this.message_success = 'You record has successfully updated.';
        setTimeout(function () {
            _this._rt.navigateByUrl('admin/company-branch');
        }, 4000);
    };
    CompanyBranchEditComponent.prototype.UpdateCompanyBranches = function (CompanyBranches, isValid) {
        var _this = this;
        this.clearMsg();
        var company_branches = [];
        company_branches = CompanyBranches;
        if (isValid === true) {
            this.success_update = true;
            this.message_success = 'You have successfully updated the company branches for this company.';
            this._company_service.updateCompanyBranchesByCompany(this.company_id, CompanyBranches).subscribe(function (data) {
                _this.fetchUpdateReponse(data);
            }, function (err) {
                _this.message_title = 'Invalid Entry';
                _this.message_error = 'Please check the following fields if the entries have filled.';
            });
        }
        else {
            this.success_update = false;
            this.message_error_title = 'Whooops!';
            this.message_error = 'Please fill in the fields! Make sure you don\'t leave a blank field.';
        }
    };
    CompanyBranchEditComponent.prototype.clearMsg = function () {
        this.message_title = '';
        this.message_error = '';
        this.message_success = '';
    };
    CompanyBranchEditComponent = __decorate([
        Component({
            selector: 'admin-company-branch-edit',
            templateUrl: './company-branch-edit.component.html',
            styleUrls: ['./company-branch-edit.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute, Configuration, CompanyService, Router, FormBuilder])
    ], CompanyBranchEditComponent);
    return CompanyBranchEditComponent;
}());
export { CompanyBranchEditComponent };
//# sourceMappingURL=company-branch-edit.component.js.map