import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';


import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';

import { Configuration } from '../../app.config';
import { Company } from '../../model/company';
import { CompanyBranch } from '../../model/company_branch';
import { Employee } from '../../model/employee';

import { CompanyService } from '../../services/company.service';

const URL = '';

@Component({
  selector: 'admin-company-branch-edit',
  templateUrl: './company-branch-edit.component.html',
  styleUrls: ['./company-branch-edit.component.css']
})

export class CompanyBranchEditComponent implements OnInit {
    public id: string;
    public company_id: any;
    public poststore: any;
    public company = new Company();
    private company_branch = new CompanyBranch();
    private updateCompanyBranchesForm: FormGroup;
    public form_select_date_options: any;
    private form_company_branches: any;
    private cutoff_date_pieces_first: any;
    private cutoff_date_pieces_second: any;
    private success_update: any;
    public message_title = '';
    public message_success = '';
    public message_error_title = '';
    public message_error = '';

    public response: any;

    // TEMP variables
    public confirm_archiving: any;

    private headers: Headers;
    dtOptions: any = {};
    company_rec: any;
    api_company: String;

    // get permissions list
    perm_rec: any;
    bodyClasses:string = "skin-blue sidebar-mini";
    body = document.getElementsByTagName('body')[0];

    constructor( private _ar: ActivatedRoute, private _conf: Configuration, private _company_service: CompanyService, private _rt: Router, private _fb: FormBuilder){
        // Header Variables
        this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        
        // Company API modes
        this.api_company = this._conf.ServerWithApiUrl + 'company/';
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.company_id = this._ar.snapshot.params['id'];

        this._company_service.getCompany(this.company_id).subscribe(
            data => {
                this.setCompany(data);
            },
            err => console.error(err)
        );

        this.updateCompanyBranchesForm = _fb.group({
        });        
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    }

    private setCompany(company: any){
        this.company = company;
        this.company_id = this.company.id;
        this.form_company_branches = this.company.branches;

        let company_branch_index_count = 0;

        for(let company_branch of this.form_company_branches){
            this.company_branch.id[company_branch_index_count] = company_branch.id;
            this.company_branch.branch_name[company_branch_index_count] = company_branch.branch_name;
            this.company_branch.branch_address[company_branch_index_count] = company_branch.branch_address;
            this.company_branch.contact_number[company_branch_index_count] = company_branch.contact_number;
            this.company_branch.contact_person[company_branch_index_count] = company_branch.contact_person;
            company_branch_index_count++;
        }
    }

    private addCompanyBranchRow(){
        let company_branch_new_row = {
            'branch_name': '',
            'id': '',
            'branch_address': '',
            'contact_number': '',
            'contact_person': ''
        };
        let company_branches = this.company.branches;
        
        company_branches.push(company_branch_new_row);

        this.form_company_branches = company_branches;

        this.message_title = '';
        this.message_success = '';
        this.message_error_title = '';
        this.message_error = '';
    }

    public removeCompanyBranchRow(row_data_index: any){
        let array_company_branches = this.form_company_branches;

        array_company_branches.splice(row_data_index, 1);

        this.form_company_branches = array_company_branches;

        document.getElementById("row-company-branch-" + row_data_index).outerHTML='';
    }

    public fetchUpdateReponse(data: any){
        this.message_title = 'Success!';
        this.message_success = 'You record has successfully updated.';

        setTimeout(() => {
            this._rt.navigateByUrl('admin/company-branch');
        }, 4000);
    }

    public UpdateCompanyBranches(CompanyBranches: any, isValid: boolean){
        this.clearMsg();
        let company_branches = [];
        company_branches = CompanyBranches;

        if(isValid === true){
            this.success_update = true;
            this.message_success = 'You have successfully updated the company branches for this company.';

            this._company_service.updateCompanyBranchesByCompany(this.company_id, CompanyBranches).subscribe(
                data => {
                    this.fetchUpdateReponse(data);
                },
                err => {
                    this.message_title = 'Invalid Entry';
                    this.message_error = 'Please check the following fields if the entries have filled.';
                }
            );
        } else {
            this.success_update = false;
            this.message_error_title = 'Whooops!';
            this.message_error = 'Please fill in the fields! Make sure you don\'t leave a blank field.';
        }
    }

    public clearMsg(){
        this.message_title = '';
        this.message_error = '';
        this.message_success = '';        
    }
}
