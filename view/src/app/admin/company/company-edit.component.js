var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Company } from '../../model/company';
import { Headers } from '@angular/http';
import { CompanyService } from '../../services/company.service';
import { PermissionService } from '../../services/permission.service';
var CompanyEditComponent = /** @class */ (function (_super) {
    __extends(CompanyEditComponent, _super);
    function CompanyEditComponent(_perms_service, dialogService, _fb, _ar, _company_service, _rt) {
        var _this = _super.call(this, dialogService) || this;
        _this._perms_service = _perms_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._company_service = _company_service;
        _this._rt = _rt;
        _this.company = new Company();
        // Header Variables
        _this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        _this.headers.append('Authorization', "Bearer " + authToken);
        _this.headers.append('Content-Type', 'application/json');
        _this.headers.append('Accept', 'application/json');
        _this.id = _this._company_service.getId();
        _this._company_service.getCompany(_this.id).subscribe(function (data) {
            _this.setCompany(data);
        }, function (err) { return console.error(err); });
        _this.updateCompanyForm = _fb.group({
            'name': [_this.company.name, [Validators.required]],
            'address': [_this.company.address, [Validators.required]],
            'email': [_this.company.email, [
                    Validators.required,
                    Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                ]],
            'phone_number': [_this.company.phone_number, [Validators.required]],
            'contact_person': [_this.company.contact_person, [Validators.required]],
        });
        return _this;
    }
    CompanyEditComponent.prototype.validateUpdate = function () {
        var _this = this;
        var company_model = this.updateCompanyForm.value;
        this._company_service.updateCompany(this.id, company_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The Company is successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 1000);
        }, function (err) { return _this.catchError(err); });
    };
    CompanyEditComponent.prototype.catchError = function (error) {
    };
    CompanyEditComponent.prototype.setCompany = function (company) {
        this.id = company.id;
        this.company = company;
    };
    CompanyEditComponent.prototype.archiveCompanyRecord = function () {
    };
    CompanyEditComponent.prototype.confirmArchived = function () {
        this.confirm_archiving = 1;
    };
    CompanyEditComponent.prototype.dismissConfirmArchive = function () {
        this.confirm_archiving = null;
    };
    CompanyEditComponent.prototype.ngAfterViewInit = function () {
    };
    CompanyEditComponent.prototype.archiveCompany = function (event) {
        var _this = this;
        event.preventDefault();
        var company_id = this.id;
        this._company_service.archiveCompany(company_id).subscribe(function (data) {
            _this.success_title = "Success!";
            _this.success_message = "The Company is successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 1000);
        }, function (err) { return console.error(err); });
        console.log('record archive');
    };
    CompanyEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        // display permission
        var companies = this._perms_service.getPermissions().
            subscribe(function (data) {
            _this.perm_rec = Array.from(data); // fetched record
        }, function (err) { return console.error(err); });
    };
    CompanyEditComponent = __decorate([
        Component({
            selector: 'confirm',
            templateUrl: './company-edit.component.html'
        }),
        __metadata("design:paramtypes", [PermissionService, DialogService, FormBuilder, ActivatedRoute, CompanyService, Router])
    ], CompanyEditComponent);
    return CompanyEditComponent;
}(DialogComponent));
export { CompanyEditComponent };
//# sourceMappingURL=company-edit.component.js.map