import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { CompanyService } from '../../../services/company.service';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-company-policy-type-modal',
  templateUrl: './company-policy-type-modal.component.html',
  styleUrls: ['./company-policy-type-modal.component.css']
})
export class CompanyPolicyTypeModalComponent extends DialogComponent<null, boolean> {

    public success_title;
    public success_message;
    public error_title: string
    public error_message: string;

    Form : FormGroup;

    id:any;
    name:any;
    company_id:any;
    postore:any;

    edit = false;
    create = false;

    company: any;
	company_current : any;
	company_value: any;

	constructor(
	  dialogService: DialogService, 
	  private _fb: FormBuilder, 
	  private company_service: CompanyService,  
	  private _common_service: CommonService,
	  private modalService: DialogService) {
		super(dialogService);

	  this.Form = _fb.group({
	     'name':        [null, [Validators.required]],
	     'company_id':	[null, [Validators.required]]
	  });  
	}

  	ngOnInit() {
  		if (this.edit) {
			this.getdata();
  		}

  		this.getCompany();
	}


	getdata(){
		let id = this.id;
    	this.company_service.getCompanyPolicyType(id).
	      	subscribe(
	        data => {
	          this.assign(data);
	        },
	        err => console.error(err)
	     );
	}

	assign(d){
		this.name = d.name;
		this.company_id = d.company_id;
	}

	getCompany(){
	      this._common_service.getCompany()
	      .subscribe(
	        data => {
	        this.company = Array.from(data);
	        
	        this.company_value = [];

	        if (this.edit) {
	        	this.company_value = this.company_id;
	        }

	        if (this.create) {
		        let id = 0;
	        	let text = 'Select Company';
	        	this.company.unshift({id,text});
	        }

	        this.company_current = this.company_value;
	        
	        },
	        err => console.error(err)
	    );
	}
	changedCompany(datas: any,i:any,x:any) {

		if(datas.value != 0){
			this.company_current = datas.value;
		}
	}

	onSubmit(){

		if (this.edit) {

			this.Form.value.company_id = this.company_current;
			let model = this.Form.value;
			let id = this.id;

	    	this.company_service.updateCompanyPolicyType(id,model).
		      	subscribe(
		        data => {
		          this.postore = data;
		          this.success_title = "Success!";
	            	this.success_message = "Successfully updated";

	            	setTimeout(() => {
	            	this.close();
	            	}, 1000);

		        },
		        err => console.error(err)
		     );
		}

		if (this.create) {

			this.Form.value.company_id = this.company_current;
			let model = this.Form.value;

	    	this.company_service.storeCompanyPolicyType(model).
		      	subscribe(
		        data => {
		          this.postore = data;
		          this.success_title = "Success!";
	            	this.success_message = "Successfully created";

	            	setTimeout(() => {
	            	this.close();
	            	}, 1000);

		        },
		        err => console.error(err)
		     );
		}
	}

}
