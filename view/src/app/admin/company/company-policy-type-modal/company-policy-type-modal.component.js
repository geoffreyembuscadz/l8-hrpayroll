var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { CompanyService } from '../../../services/company.service';
import { CommonService } from '../../../services/common.service';
var CompanyPolicyTypeModalComponent = /** @class */ (function (_super) {
    __extends(CompanyPolicyTypeModalComponent, _super);
    function CompanyPolicyTypeModalComponent(dialogService, _fb, company_service, _common_service, modalService) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this.company_service = company_service;
        _this._common_service = _common_service;
        _this.modalService = modalService;
        _this.edit = false;
        _this.create = false;
        _this.Form = _fb.group({
            'name': [null, [Validators.required]],
            'company_id': [null, [Validators.required]]
        });
        return _this;
    }
    CompanyPolicyTypeModalComponent.prototype.ngOnInit = function () {
        if (this.edit) {
            this.getdata();
        }
        this.getCompany();
    };
    CompanyPolicyTypeModalComponent.prototype.getdata = function () {
        var _this = this;
        var id = this.id;
        this.company_service.getCompanyPolicyType(id).
            subscribe(function (data) {
            _this.assign(data);
        }, function (err) { return console.error(err); });
    };
    CompanyPolicyTypeModalComponent.prototype.assign = function (d) {
        this.name = d.name;
        this.company_id = d.company_id;
    };
    CompanyPolicyTypeModalComponent.prototype.getCompany = function () {
        var _this = this;
        this._common_service.getCompany()
            .subscribe(function (data) {
            _this.company = Array.from(data);
            _this.company_value = [];
            if (_this.edit) {
                _this.company_value = _this.company_id;
            }
            if (_this.create) {
                var id = 0;
                var text = 'Select Company';
                _this.company.unshift({ id: id, text: text });
            }
            _this.company_current = _this.company_value;
        }, function (err) { return console.error(err); });
    };
    CompanyPolicyTypeModalComponent.prototype.changedCompany = function (datas, i, x) {
        if (datas.value != 0) {
            this.company_current = datas.value;
        }
    };
    CompanyPolicyTypeModalComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.edit) {
            this.Form.value.company_id = this.company_current;
            var model = this.Form.value;
            var id = this.id;
            this.company_service.updateCompanyPolicyType(id, model).
                subscribe(function (data) {
                _this.postore = data;
                _this.success_title = "Success!";
                _this.success_message = "Successfully updated";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return console.error(err); });
        }
        if (this.create) {
            this.Form.value.company_id = this.company_current;
            var model = this.Form.value;
            this.company_service.storeCompanyPolicyType(model).
                subscribe(function (data) {
                _this.postore = data;
                _this.success_title = "Success!";
                _this.success_message = "Successfully created";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return console.error(err); });
        }
    };
    CompanyPolicyTypeModalComponent = __decorate([
        Component({
            selector: 'app-company-policy-type-modal',
            templateUrl: './company-policy-type-modal.component.html',
            styleUrls: ['./company-policy-type-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            CompanyService,
            CommonService,
            DialogService])
    ], CompanyPolicyTypeModalComponent);
    return CompanyPolicyTypeModalComponent;
}(DialogComponent));
export { CompanyPolicyTypeModalComponent };
//# sourceMappingURL=company-policy-type-modal.component.js.map