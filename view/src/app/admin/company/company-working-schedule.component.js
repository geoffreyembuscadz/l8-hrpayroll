var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable } from '@angular/core';
import { CompanyService } from '../../services/company.service';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Company } from '../../model/company';
import { DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import 'rxjs/add/operator/map';
var CompanyWorkingScheduleComponent = /** @class */ (function () {
    function CompanyWorkingScheduleComponent(dialogService, _perms_service, modalService, _ar, _conf, _company_service, _rt, _fb) {
        this._perms_service = _perms_service;
        this.modalService = modalService;
        this._ar = _ar;
        this._conf = _conf;
        this._company_service = _company_service;
        this._rt = _rt;
        this._fb = _fb;
        this.company = new Company();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.api_company = this._conf.ServerWithApiUrl + 'company/';
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }
    CompanyWorkingScheduleComponent.prototype.ngAfterViewInit = function () {
    };
    CompanyWorkingScheduleComponent.prototype.ngOnInit = function () {
        var _this = this;
        var companys = this._perms_service.getPermissions().
            subscribe(function (data) {
            _this.perm_rec = Array.from(data); // fetched record
        }, function (err) { return console.error(err); });
        var authToken = localStorage.getItem('id_token');
        // DataTable Configuration
        this.dtOptions = {
            ajax: {
                url: this.api_company,
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + authToken);
                },
            },
            columns: [{
                    title: 'Company',
                    data: 'name'
                }],
            rowCallback: function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
                var self = _this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', function () {
                    _this._rt.navigate(['/admin/company-working-schedule-edit/' + data.id]);
                    // console.log(data.id);
                });
                return nRow;
            }
        };
    };
    CompanyWorkingScheduleComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    CompanyWorkingScheduleComponent = __decorate([
        Component({
            selector: 'app-company-working-schedule',
            templateUrl: './company-working-schedule.component.html',
            styleUrls: ['./company-working-schedule.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService, PermissionService, DialogService, ActivatedRoute, Configuration, CompanyService, Router, FormBuilder])
    ], CompanyWorkingScheduleComponent);
    return CompanyWorkingScheduleComponent;
}());
export { CompanyWorkingScheduleComponent };
//# sourceMappingURL=company-working-schedule.component.js.map