import { Component, OnInit, Injectable } from '@angular/core';
import { CompanyService } from '../../services/company.service';

import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Company } from '../../model/company';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { CompanyEditComponent } from './company-edit.component';
import { CompanyAddComponent } from './company-add.component';
import { PermissionService } from '../../services/permission.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-company-working-schedule',
  templateUrl: './company-working-schedule.component.html',
  styleUrls: ['./company-working-schedule.component.css']
})

@Injectable()
export class CompanyWorkingScheduleComponent implements OnInit {
    public id: string;
    public company_id: any;
    public error_title: string;
    public error_message: string;
    public success_title;
    public success_message;
    public poststore: any;
    public company = new Company();
    companyForm: FormGroup;

    // TEMP variables
    public confirm_archiving: any;
    public form

    private headers: Headers;
    dtOptions: any = {};
    company_rec: any;
    api_company: String;

    // get permissions list
    perm_rec: any;
    bodyClasses:string = "skin-blue sidebar-mini";
    body = document.getElementsByTagName('body')[0];

    constructor(dialogService: DialogService, private _perms_service: PermissionService, private modalService: DialogService, private _ar: ActivatedRoute, private _conf: Configuration, private _company_service: CompanyService, private _rt: Router, private _fb: FormBuilder){
          this.api_company = this._conf.ServerWithApiUrl + 'company/';
          this.body.classList.add("skin-blue");
          this.body.classList.add("sidebar-mini");

          // Header Variables
        this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }

    ngAfterViewInit(){
    }

    ngOnInit() {
        let companys = this._perms_service.getPermissions().
        subscribe(
            data => {
                this.perm_rec = Array.from(data); // fetched record
            },
            err => console.error(err)
        );

        let authToken = localStorage.getItem('id_token');

        // DataTable Configuration
        this.dtOptions = {
            ajax: {
                url: this.api_company,
                type: "GET",
                beforeSend: function(xhr){
                    xhr.setRequestHeader("Authorization", "Bearer " + authToken);
                },
            },
            columns: [{
                title: 'Company',
                data: 'name'
            }],
            rowCallback: (nRow: number, data: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
                let self = this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', () => {
                    this._rt.navigate(['/admin/company-working-schedule-edit/'+ data.id]);
                    // console.log(data.id);
                });
                return nRow;
            }
        };
    }

    ngOnDestroy() {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    }    
}
