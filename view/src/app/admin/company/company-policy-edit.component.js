var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable } from '@angular/core';
import { CompanyService } from '../../services/company.service';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Company } from '../../model/company';
import { CompanyPolicy } from '../../model/company_policy';
import { CompanyPolicyEquation } from '../../model/company_policy_equation';
import { CompanyGovernmentInput } from '../../model/company_government_input';
import { CompanyCutOffDate } from '../../model/company_cutoff_date';
import { DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import 'rxjs/add/operator/map';
var CompanyPolicyEditComponent = /** @class */ (function () {
    function CompanyPolicyEditComponent(dialogService, _perms_service, modalService, _ar, _conf, _company_service, _rt, _fb) {
        var _this = this;
        this._perms_service = _perms_service;
        this.modalService = modalService;
        this._ar = _ar;
        this._conf = _conf;
        this._company_service = _company_service;
        this._rt = _rt;
        this._fb = _fb;
        this.company = new Company();
        this.company_policy = new CompanyPolicy();
        this.company_policy_equation = new CompanyPolicyEquation();
        this.company_cutoff_date = new CompanyCutOffDate();
        this.company_government_inputs = new CompanyGovernmentInput();
        this.message_title = '';
        this.message_success = '';
        this.message_error = '';
        this.form_de_yn = [{ value: 0, label: 'No' }, { value: 1, label: 'Yes' }];
        this.form_ut_yn = [{ value: 0, label: 'No' }, { value: 1, label: 'Yes' }];
        this.form_bb_yn = [{ value: 0, label: 'No (without Admin)' }, { value: 1, label: 'Yes (with Admin)' }];
        this.form_ns_yn = [{ value: 0, label: 'No' }, { value: 3, label: 'No (Display on Payroll)' }, { value: 1, label: 'Yes (multiple by 1)' }, { value: 2, label: 'Yes (multiple by government rate)' }];
        this.form_ot_yn = [{ value: 0, label: 'No' }, { value: 3, label: 'No (Display on Payroll)' }, { value: 1, label: 'Yes (multiple by 1)' }, { value: 2, label: 'Yes (multiple by government rate)' }];
        this.form_hd_yn = [{ value: 0, label: 'No' }, { value: 3, label: 'No (Display on Payroll)' }, { value: 1, label: 'Yes (multiple by 1)' }, { value: 2, label: 'Yes (multiple by government rate)' }];
        this.form_government_field_terms = [
            { code: null, label: null },
            { code: 'MONTHLY', label: 'Monthly' },
            { code: 'DAILY', label: 'Daily' }
        ];
        this.form_payroll_periods = [
            'daily', 'weekly', 'semi-monthly', 'monthly'
        ];
        this.form_company_policies = [];
        this.form_company_policy_equations = [];
        this.form_company_government_fields = [];
        this.form_government_fields = [
            { code: 'SSS', label: 'SSS No.', monthly: 0, daily: 0, days_min: 0 },
            { code: 'PAGIBIG', label: 'Pag-ibig', monthly: 0, daily: 0, days_min: 0 },
            { code: 'PHILHEALTH', label: 'PhilHealth No.', monthly: 0, daily: 0, days_min: 0 },
            { code: 'TIN', label: 'TIN No.', monthly: 0, daily: 0, days_min: 0 }
        ];
        this.form_company_policy_types = [];
        this.form_company_policy_formats = [
            { name: 'none', label: '-none-' }, { name: 'date', label: 'Date' }, { name: 'number', label: 'Number' }, { name: 'text', label: 'String' }, { name: 'time', label: 'Time(Mins.)' }
        ];
        this.form_equivalent_units = [
            { name: '', label: '' },
            { name: 'HOUR', label: 'Hour' },
            { name: 'MINUTE', label: 'Minute' },
            { name: 'DAY', label: 'Day' },
            { name: 'CASH_PESOS', label: 'Pesos' }
        ];
        this.form_company_overtime = [{ value: 0, label: 'No' }, { value: 1, label: 'Yes, normal rate (multiple by 1)' }];
        this.form_company_nightshift = [{ value: 0, label: 'No' }, { value: 1, label: 'Yes, normal rate (multiple by 1)' }];
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.api_company = this._conf.ServerWithApiUrl + 'company/';
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.company_id = this._ar.snapshot.params['company_id'];
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.form_select_date_options = [{ index: '', label: '' }];
        var date = 1;
        for (; date < 33; date++) {
            this.form_select_date_options[date] = { index: date, label: date };
            if (date == 32) {
                this.form_select_date_options[date] = { index: 'last_day_of_month', label: 'Last day of the Month' };
            }
        }
        this._company_service.getCompanyPolicyTypes().subscribe(function (data) {
            _this.setCompanyPolicyTypes(data);
        }, function (err) { return console.error(err); });
        this._company_service.getCompany(this.company_id).subscribe(function (data) {
            _this.setCompany(data);
        }, function (err) { return console.error(err); });
    }
    CompanyPolicyEditComponent.prototype.ngOnInit = function () {
    };
    CompanyPolicyEditComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    CompanyPolicyEditComponent.prototype.setCompanyPolicyTypes = function (company_policy_types) {
        this.form_company_policy_types = company_policy_types;
    };
    CompanyPolicyEditComponent.prototype.setCompany = function (company) {
        this.company = company;
        this.company_id = this.company.id;
        this.form_company_policies = company.policies;
        this.company_policy.company_id = this.company_id;
        this.company_policy_equation.company_id = this.company_id;
        this.form_company_policy_equations = company.policy_equations;
        this.form_company_government_fields = company.emp_govrn_fields;
        this.company_policy.cola_amount = company.cola_amount;
        this.company_policy.billing_asf = company.billing_asf;
        this.company_policy.billing_rate = company.billing_rate;
        this.company_policy.billing_vat = company.billing_vat;
        this.company_policy.billing_13th = company.billing_13th;
        this.company_policy.billing_breakdown = company.billing_breakdown;
        this.company_policy.attendance_default = company.attendance_default;
        for (var _i = 0; _i < company.policies.length; _i++) {
            var policy_row = company.policies[_i];
            this.company_policy.policies.id[_i] = policy_row.id;
            this.company_policy.policies.company_policy[_i] = policy_row.name;
            this.company_policy.policies.company_policy_type_id[_i] = policy_row.company_policy_type_id;
            this.company_policy.policies.format_value[_i] = policy_row.format_value;
            this.company_policy.policies.default_value[_i] = policy_row.default_value;
            this.company_policy.policies.start_value[_i] = policy_row.start_value;
            this.company_policy.policies.end_value[_i] = policy_row.end_value;
        }
        for (var _i = 0; _i < company.policy_equations.length; _i++) {
            var policy_equation_row = company.policy_equations[_i];
            this.company_policy_equation.policy_equations.company_id[_i] = this.company_id;
            this.company_policy_equation.policy_equations.id[_i] = policy_equation_row.id;
            this.company_policy_equation.policy_equations.name[_i] = policy_equation_row.name;
            this.company_policy_equation.policy_equations.description[_i] = policy_equation_row.description;
            this.company_policy_equation.policy_equations.value[_i] = policy_equation_row.value;
            this.company_policy_equation.policy_equations.value_unit[_i] = policy_equation_row.value_unit;
            this.company_policy_equation.policy_equations.equivalent_value[_i] = policy_equation_row.equivalent_value;
            this.company_policy_equation.policy_equations.equivalent_unit[_i] = policy_equation_row.equivalent_unit;
        }
        this.company_cutoff_date.days_before_notif = company.cutoff_dates.days_before_notif;
        this.company_cutoff_date.first_cutoff_first_date = null;
        this.company_cutoff_date.first_cutoff_second_date = null;
        this.company_cutoff_date.second_cutoff_first_date = null;
        this.company_cutoff_date.second_cutoff_second_date = null;
        if (company.cutoff_dates) {
            this.company_cutoff_date.first_cutoff_first_date = JSON.parse('[' + company.cutoff_dates.first_cutoff_date + ']')[0][0];
            this.company_cutoff_date.first_cutoff_second_date = JSON.parse('[' + company.cutoff_dates.first_cutoff_date + ']')[0][1];
            this.company_cutoff_date.second_cutoff_first_date = JSON.parse('[' + company.cutoff_dates.second_cutoff_date + ']')[0][0];
            this.company_cutoff_date.second_cutoff_second_date = JSON.parse('[' + company.cutoff_dates.second_cutoff_date + ']')[0][1];
        }
        for (var _indexformgovernment = 0; _indexformgovernment < this.form_government_fields.length; _indexformgovernment++) {
            this.company_government_inputs.code[_indexformgovernment] = this.form_government_fields[_indexformgovernment].code;
            this.company_government_inputs.active[_indexformgovernment] = company.emp_govrn_fields[_indexformgovernment].active;
            this.company_government_inputs.id[_indexformgovernment] = company.emp_govrn_fields[_indexformgovernment].id;
            this.company_government_inputs.company_id[_indexformgovernment] = company.emp_govrn_fields[_indexformgovernment].company_id;
            this.company_government_inputs.first_cutoff[_indexformgovernment] = company.emp_govrn_fields[_indexformgovernment].first_cutoff;
            this.company_government_inputs.second_cutoff[_indexformgovernment] = company.emp_govrn_fields[_indexformgovernment].second_cutoff;
            this.company_government_inputs.min_days[_indexformgovernment] = company.emp_govrn_fields[_indexformgovernment].daily_min_days;
            this.company_government_inputs.tax_term[_indexformgovernment] = company.emp_govrn_fields[_indexformgovernment].tax_term;
            this.company_government_inputs.is_default[_indexformgovernment] = company.emp_govrn_fields[_indexformgovernment].is_default;
            this.company_government_inputs.default_value[_indexformgovernment] = company.emp_govrn_fields[_indexformgovernment].default_value;
        }
        console.log(this.company);
    };
    CompanyPolicyEditComponent.prototype.fetchUpdateReponse = function (data) {
        var _this = this;
        this.message_title = 'Success!';
        this.message_success = 'You record has successfully updated.';
        setTimeout(function () {
            _this._rt.navigateByUrl('admin/company-policy');
        }, 4000);
    };
    CompanyPolicyEditComponent.prototype.UpdateCompanyPolicies = function (CompanyPolicies, isValid) {
        var _this = this;
        this.message_title = '';
        this.message_success = '';
        this.message_error = '';
        this._company_service.updateCompanyPolicy(CompanyPolicies).subscribe(function (data) {
            _this.fetchUpdateReponse(data);
        }, function (err) {
            _this.message_title = 'Invalid Entry';
            _this.message_error = 'Please check the following fields if the entries have filled.';
        });
    };
    CompanyPolicyEditComponent.prototype.addPolicyRow = function () {
        var new_policy_row = {
            id: '',
            company_policy: '',
            company_policy_type: '',
            company_policy_type_id: '',
            default_value: '',
            end_value: '',
            format_value: '',
            start_value: ''
        };
        this.form_company_policies.push(new_policy_row);
    };
    CompanyPolicyEditComponent.prototype.removePolicyRow = function (index) {
        var policy_row = this.form_company_policies[index];
        if (policy_row.id) {
            this._company_service.deleteCompanyPolicy(policy_row.id).subscribe(function (data) {
                console.log(data);
            }, function (err) { return console.log(err); });
        }
        this.form_company_policies.splice(index, 1);
    };
    CompanyPolicyEditComponent.prototype.addEquivalentUnitRow = function () {
        var new_policy_equivalent_row = {
            id: '',
            name: '',
            description: '',
            value: '',
            value_unit: '',
            equivalent_unit: '',
            equivalent_value: ''
        };
        this.form_company_policy_equations.push(new_policy_equivalent_row);
    };
    CompanyPolicyEditComponent.prototype.removePolicyEquivalentRow = function (index) {
        var policy_equivalent_row = this.form_company_policy_equations[index];
        if (policy_equivalent_row.id) {
            this._company_service.deleteCompanyPolicyEquivalent(policy_equivalent_row.id).subscribe(function (data) {
                console.log(data);
            }, function (err) { return console.log(err); });
        }
        this.form_company_policy_equations.splice(index, 1);
    };
    CompanyPolicyEditComponent.prototype.clearMsg = function () {
        this.message_title = '';
        this.message_error = '';
        this.message_success = '';
    };
    CompanyPolicyEditComponent.prototype.archiveCompanyPolicy = function (company_policy_id) {
        console.log(company_policy_id);
    };
    CompanyPolicyEditComponent.prototype.validateFieldRequirements = function (procedure_value, index_of_government_field) {
        if (procedure_value == 'MONTHLY') {
            this.form_government_fields[index_of_government_field].monthly = 1;
            this.form_government_fields[index_of_government_field].daily = 0;
        }
        else {
            this.form_government_fields[index_of_government_field].monthly = 0;
            this.form_government_fields[index_of_government_field].daily = 1;
        }
    };
    CompanyPolicyEditComponent.prototype.changeOptionGovernmentField = function (index_government_field, index_government_option) {
    };
    CompanyPolicyEditComponent = __decorate([
        Component({
            selector: 'app-company-policy-edit',
            templateUrl: './company-policy-edit.component.html',
            styleUrls: ['./company-policy-edit.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService, PermissionService, DialogService, ActivatedRoute, Configuration, CompanyService, Router, FormBuilder])
    ], CompanyPolicyEditComponent);
    return CompanyPolicyEditComponent;
}());
export { CompanyPolicyEditComponent };
//# sourceMappingURL=company-policy-edit.component.js.map