import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Company } from '../../model/company';
import { Response, Headers } from '@angular/http';
import { CompanyService } from '../../services/company.service';
import { PermissionService } from '../../services/permission.service';
import { Configuration } from '../../app.config';


@Component({
  selector: 'confirm',
  templateUrl: './company-edit.component.html'
})
export class CompanyEditComponent extends DialogComponent<null, boolean> {
  perm_rec: any;

  title: string;
  message: string;
  public id: any;
  public name: string;
  public display_name: string;
  public description: string;
  public success_title;
  public success_message;
  private headers: Headers;
  public poststore: any;
  public company = new Company();

  updateCompanyForm : FormGroup;
  public confirm_archiving: any;

  constructor(private _perms_service: PermissionService, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute, private _company_service: CompanyService,  private _rt: Router) {
    super(dialogService);
      // Header Variables
    this.headers = new Headers();
    let authToken = localStorage.getItem('id_token');
    this.headers.append('Authorization', `Bearer ${authToken}`);
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');

    this.id = this._company_service.getId();
    
    this._company_service.getCompany(this.id).subscribe(
      data => {
        this.setCompany(data);
      },
      err => console.error(err)
    );

    this.updateCompanyForm = _fb.group({
      'name': [this.company.name, [Validators.required]],
      'address': [this.company.address, [Validators.required]],
      'email': [this.company.email, [
            Validators.required,
            Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]],
      'phone_number': [this.company.phone_number, [Validators.required]],
      'contact_person': [this.company.contact_person, [Validators.required]],
      
      
      
    });  
  }
 
  public validateUpdate() {
    let company_model = this.updateCompanyForm.value;

    this._company_service.updateCompany(this.id, company_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The Company is successfully updated.";
        setTimeout(() => {
       this.close();
      }, 1000);
      },
      err => this.catchError(err)
    );
  }

  public catchError(error: any) {

  }

  public setCompany(company: any){
    this.id = company.id;
    this.company = company;
    
  }
  public archiveCompanyRecord(){
    
  }
public confirmArchived(){
    this.confirm_archiving = 1;
  }

  public dismissConfirmArchive(){
    this.confirm_archiving = null;
  }

  

  ngAfterViewInit(){
  }

  archiveCompany(event: Event){
    event.preventDefault();
    let company_id = this.id;
    this._company_service.archiveCompany(company_id).subscribe(
      data => {
       this.success_title = "Success!";
        this.success_message = "The Company is successfully updated.";
        setTimeout(() => {
           this.close();
        }, 1000);
      },
      err => console.error(err)
    );
    console.log('record archive');

  }

  ngOnInit() {
    // display permission
    let companies = this._perms_service.getPermissions().
      subscribe(
        data => {
          this.perm_rec = Array.from(data); // fetched record
        },
        err => console.error(err)
      );
    }

}