import { Component, OnInit, Injectable } from '@angular/core';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Configuration } from '../../app.config';
import { Company } from '../../model/company';
import { CompanyPolicy } from '../../model/company_policy';
import { CompanyWorkingSchedule } from '../../model/company_working_schedule';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { CompanyEditComponent } from './company-edit.component';
import { CompanyAddComponent } from './company-add.component';
import { PermissionService } from '../../services/permission.service';
import { CompanyService } from '../../services/company.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component({
    selector: 'app-company-working-schedule-edit',
    templateUrl: './company-working-schedule-edit.component.html',
    styleUrls: ['./company-working-schedule-edit.component.css']
})

@Injectable()
export class CompanyWorkingScheduleEditComponent implements OnInit {
    public id: string;
    public company_id: any;
    public poststore: any;
    public putData: any;
    public company = new Company();

    private companyForm: FormGroup;
    private company_working_schedules = new CompanyWorkingSchedule();
    private working_schedules = [];
    private form_days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
    public message_title = '';
    public message_error = '';
    public message_success = '';

    public response: any;

    // TEMP variables
    public confirm_archiving: any;

    private headers: Headers;
    dtOptions: any = {};
    company_rec: any;
    api_company: String;

    // get permissions list
    perm_rec: any;
    bodyClasses:string = "skin-blue sidebar-mini";
    body = document.getElementsByTagName('body')[0];

    constructor(dialogService: DialogService, private _perms_service: PermissionService, private modalService: DialogService, private _ar: ActivatedRoute, private _conf: Configuration, private _company_service: CompanyService, private _rt: Router, private _fb: FormBuilder){

        this.api_company = this._conf.ServerWithApiUrl + 'company/';
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.company_id = this._ar.snapshot.params['id']; // company id

        // Header Variables
        this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');

        this._company_service.getCompany(this.company_id).subscribe(
            data => {
                this.setCompany(data);
            },
            err => console.error(err)
        );

        this._company_service.getWorkingSchedule('company_working_schedule?company_id=' + this.company_id).subscribe(
            data => {
                this.setCompanySchedules(data);
            },
            err => console.error(err)
        );
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    }

    private addCompanyScheduleRow(){
        let new_policy_row = {
            id: 0,
            name: '',
            flexible_time: 0,
            grace_period_mins: '',
            time_in: '',
            time_out: '',
            lunch_break_time: '',
            lunch_mins_break: '',
            days: '[]',
        };

        this.working_schedules.push(new_policy_row);
    }

    private setCompanySchedules(data: any){
        this.company_working_schedules.company_id = this.company_id;
        this.working_schedules = data;

        for(var _i = 0; _i < data.length; _i++){
            let schedule = data[_i];

            this.company_working_schedules.schedules.id[_i] = data[_i].id;
            this.company_working_schedules.schedules.name[_i] = data[_i].name;
            this.company_working_schedules.schedules.flexible_time[_i] = data[_i].flexible_time;
            this.company_working_schedules.schedules.grace_period_mins[_i] = data[_i].grace_period_mins;
            this.company_working_schedules.schedules.time_in[_i] = data[_i].time_in;

            this.company_working_schedules.schedules.lunch_break_time[_i] = data[_i].lunch_break_time;
            this.company_working_schedules.schedules.lunch_mins_break[_i] = data[_i].lunch_mins_break;

            this.company_working_schedules.schedules.time_out[_i] = data[_i].time_out;
            this.company_working_schedules.schedules.days[_i] = JSON.parse(data[_i].days);
        }
        console.log(this.company_working_schedules);

        // this.company_policy.company_id = this.company_id;
        // for(var _i = 0; _i < company.policies.length; _i++){
        //     let policy_row = company.policies[_i];
        //     this.company_policy.policies.id[_i] = policy_row.id;
        //     this.company_policy.policies.company_policy[_i] = policy_row.company_policy;
        //     this.company_policy.policies.company_policy_type_id[_i] = policy_row.company_policy_type_id;
        //     this.company_policy.policies.format_value[_i] = policy_row.format_value;
        //     this.company_policy.policies.default_value[_i] = policy_row.default_value;
        //     this.company_policy.policies.start_value[_i] = policy_row.start_value;
        //     this.company_policy.policies.end_value[_i] = policy_row.end_value;
        // }
    }

    private checkIfDayIsChecked(company_schedule_days: any, day: string){
        let working_days = JSON.parse(company_schedule_days);

        for(let working_day of working_days){
            if(working_day == day){
                return true;
            }
        }
    }

    private RemoveScheduleRow(row_schedule_index: any, schedule_id: number = 0){

        let array_working_schedule = this.working_schedules;

        array_working_schedule.splice(row_schedule_index, 1);

        this.working_schedules = array_working_schedule;

        document.getElementById("row-company-schedule-" + row_schedule_index).outerHTML='';
        // console.log(this.working_schedules);

        this._company_service.removeCompanySchedule("company_working_schedule", schedule_id)
            .subscribe(
                data => { console.log(data); }
            );
    }

    private updateCompanyWorkingSchedules(data: any, is_valid: any){
        this.clearMsg();
        let valid = 1;

        if(valid){
            this._company_service.updateCompanySchedules("company_update_schedule/" + this.company_id, this.company_id, data)
            .subscribe(
                data => {
                    this.putData = Array.from(data); // fetched the records

                    this.message_title = 'Oh Yes!';
                    this.message_success = 'You have successfully editing the following schedules for the company.';

                    setTimeout(() => {
                        this._rt.navigate(['admin/company-working-schedule']);
                    }, 2000);                    
                },
                err => {
                    this.message_title = 'Invalid Entry';
                    this.message_error = 'Please Check first the fields and make sure are valid.';
                    console.error(err);
                }
            );
        } else {
            this.message_title = 'Invalid Entry';
            this.message_error = 'Please Check first the fields and make sure are valid.';
        }
    }

    private setCompany(company: any) {
        this.company = company;
        this.company_id = this.company.id;
    }

    public clearMsg() {
        this.message_title = '';
        this.message_error = '';
        this.message_success = '';        
    }
}
