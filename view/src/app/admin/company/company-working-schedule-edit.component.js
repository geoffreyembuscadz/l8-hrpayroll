var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Configuration } from '../../app.config';
import { Company } from '../../model/company';
import { CompanyWorkingSchedule } from '../../model/company_working_schedule';
import { DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import { CompanyService } from '../../services/company.service';
import 'rxjs/add/operator/map';
var CompanyWorkingScheduleEditComponent = /** @class */ (function () {
    function CompanyWorkingScheduleEditComponent(dialogService, _perms_service, modalService, _ar, _conf, _company_service, _rt, _fb) {
        var _this = this;
        this._perms_service = _perms_service;
        this.modalService = modalService;
        this._ar = _ar;
        this._conf = _conf;
        this._company_service = _company_service;
        this._rt = _rt;
        this._fb = _fb;
        this.company = new Company();
        this.company_working_schedules = new CompanyWorkingSchedule();
        this.working_schedules = [];
        this.form_days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
        this.message_title = '';
        this.message_error = '';
        this.message_success = '';
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.api_company = this._conf.ServerWithApiUrl + 'company/';
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.company_id = this._ar.snapshot.params['id']; // company id
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this._company_service.getCompany(this.company_id).subscribe(function (data) {
            _this.setCompany(data);
        }, function (err) { return console.error(err); });
        this._company_service.getWorkingSchedule('company_working_schedule?company_id=' + this.company_id).subscribe(function (data) {
            _this.setCompanySchedules(data);
        }, function (err) { return console.error(err); });
    }
    CompanyWorkingScheduleEditComponent.prototype.ngOnInit = function () {
    };
    CompanyWorkingScheduleEditComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    CompanyWorkingScheduleEditComponent.prototype.addCompanyScheduleRow = function () {
        var new_policy_row = {
            id: 0,
            name: '',
            flexible_time: 0,
            grace_period_mins: '',
            time_in: '',
            time_out: '',
            lunch_break_time: '',
            lunch_mins_break: '',
            days: '[]',
        };
        this.working_schedules.push(new_policy_row);
    };
    CompanyWorkingScheduleEditComponent.prototype.setCompanySchedules = function (data) {
        this.company_working_schedules.company_id = this.company_id;
        this.working_schedules = data;
        for (var _i = 0; _i < data.length; _i++) {
            var schedule = data[_i];
            this.company_working_schedules.schedules.id[_i] = data[_i].id;
            this.company_working_schedules.schedules.name[_i] = data[_i].name;
            this.company_working_schedules.schedules.flexible_time[_i] = data[_i].flexible_time;
            this.company_working_schedules.schedules.grace_period_mins[_i] = data[_i].grace_period_mins;
            this.company_working_schedules.schedules.time_in[_i] = data[_i].time_in;
            this.company_working_schedules.schedules.lunch_break_time[_i] = data[_i].lunch_break_time;
            this.company_working_schedules.schedules.lunch_mins_break[_i] = data[_i].lunch_mins_break;
            this.company_working_schedules.schedules.time_out[_i] = data[_i].time_out;
            this.company_working_schedules.schedules.days[_i] = JSON.parse(data[_i].days);
        }
        console.log(this.company_working_schedules);
        // this.company_policy.company_id = this.company_id;
        // for(var _i = 0; _i < company.policies.length; _i++){
        //     let policy_row = company.policies[_i];
        //     this.company_policy.policies.id[_i] = policy_row.id;
        //     this.company_policy.policies.company_policy[_i] = policy_row.company_policy;
        //     this.company_policy.policies.company_policy_type_id[_i] = policy_row.company_policy_type_id;
        //     this.company_policy.policies.format_value[_i] = policy_row.format_value;
        //     this.company_policy.policies.default_value[_i] = policy_row.default_value;
        //     this.company_policy.policies.start_value[_i] = policy_row.start_value;
        //     this.company_policy.policies.end_value[_i] = policy_row.end_value;
        // }
    };
    CompanyWorkingScheduleEditComponent.prototype.checkIfDayIsChecked = function (company_schedule_days, day) {
        var working_days = JSON.parse(company_schedule_days);
        for (var _a = 0, working_days_1 = working_days; _a < working_days_1.length; _a++) {
            var working_day = working_days_1[_a];
            if (working_day == day) {
                return true;
            }
        }
    };
    CompanyWorkingScheduleEditComponent.prototype.RemoveScheduleRow = function (row_schedule_index, schedule_id) {
        if (schedule_id === void 0) { schedule_id = 0; }
        var array_working_schedule = this.working_schedules;
        array_working_schedule.splice(row_schedule_index, 1);
        this.working_schedules = array_working_schedule;
        document.getElementById("row-company-schedule-" + row_schedule_index).outerHTML = '';
        // console.log(this.working_schedules);
        this._company_service.removeCompanySchedule("company_working_schedule", schedule_id)
            .subscribe(function (data) { console.log(data); });
    };
    CompanyWorkingScheduleEditComponent.prototype.updateCompanyWorkingSchedules = function (data, is_valid) {
        var _this = this;
        this.clearMsg();
        var valid = 1;
        if (valid) {
            this._company_service.updateCompanySchedules("company_update_schedule/" + this.company_id, this.company_id, data)
                .subscribe(function (data) {
                _this.putData = Array.from(data); // fetched the records
                _this.message_title = 'Oh Yes!';
                _this.message_success = 'You have successfully editing the following schedules for the company.';
                setTimeout(function () {
                    _this._rt.navigate(['admin/company-working-schedule']);
                }, 2000);
            }, function (err) {
                _this.message_title = 'Invalid Entry';
                _this.message_error = 'Please Check first the fields and make sure are valid.';
                console.error(err);
            });
        }
        else {
            this.message_title = 'Invalid Entry';
            this.message_error = 'Please Check first the fields and make sure are valid.';
        }
    };
    CompanyWorkingScheduleEditComponent.prototype.setCompany = function (company) {
        this.company = company;
        this.company_id = this.company.id;
    };
    CompanyWorkingScheduleEditComponent.prototype.clearMsg = function () {
        this.message_title = '';
        this.message_error = '';
        this.message_success = '';
    };
    CompanyWorkingScheduleEditComponent = __decorate([
        Component({
            selector: 'app-company-working-schedule-edit',
            templateUrl: './company-working-schedule-edit.component.html',
            styleUrls: ['./company-working-schedule-edit.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService, PermissionService, DialogService, ActivatedRoute, Configuration, CompanyService, Router, FormBuilder])
    ], CompanyWorkingScheduleEditComponent);
    return CompanyWorkingScheduleEditComponent;
}());
export { CompanyWorkingScheduleEditComponent };
//# sourceMappingURL=company-working-schedule-edit.component.js.map