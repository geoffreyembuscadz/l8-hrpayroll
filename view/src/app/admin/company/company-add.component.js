var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Company } from '../../model/company';
import { CompanyService } from '../../services/company.service';
import { Headers } from '@angular/http';
import { PermissionService } from '../../services/permission.service';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
var CompanyAddComponent = /** @class */ (function (_super) {
    __extends(CompanyAddComponent, _super);
    function CompanyAddComponent(_perms_service, dialogService, _fb, _ar, _company_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._perms_service = _perms_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._company_service = _company_service;
        _this.company = new Company();
        // Header Variables
        _this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        _this.headers.append('Authorization', "Bearer " + authToken);
        _this.headers.append('Content-Type', 'application/json');
        _this.headers.append('Accept', 'application/json');
        _this.addCompanyForm = _fb.group({
            'name': [_this.company.name, [Validators.required]],
            'address': [_this.company.address, [Validators.required]],
            'email': [_this.company.email, [
                    Validators.required,
                    Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                ]],
            'phone_number': [_this.company.phone_number, [Validators.required]],
            'contact_person': [_this.company.contact_person, [Validators.required]],
        });
        return _this;
    }
    CompanyAddComponent.prototype.catchError = function (error) {
    };
    CompanyAddComponent.prototype.onSubmit = function () {
        var _this = this;
        var company_model = this.addCompanyForm.value;
        this._company_service.storeCompany(company_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            _this.success_title = "Success!";
            _this.success_message = "New company is added!";
            setTimeout(function () {
                _this.close();
            }, 1000);
        }, function (err) { return _this.catchError(err); });
    };
    CompanyAddComponent = __decorate([
        Component({
            selector: 'app-company-add',
            templateUrl: './company-add.component.html',
            styleUrls: ['./company-add.component.css']
        }),
        __metadata("design:paramtypes", [PermissionService, DialogService, FormBuilder, ActivatedRoute, CompanyService])
    ], CompanyAddComponent);
    return CompanyAddComponent;
}(DialogComponent));
export { CompanyAddComponent };
//# sourceMappingURL=company-add.component.js.map