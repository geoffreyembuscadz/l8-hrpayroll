import 'rxjs/add/operator/catch';
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Company } from '../../model/company';
import { CompanyService } from '../../services/company.service';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { PermissionService } from '../../services/permission.service';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";

@Component({
  selector: 'app-company-add',
  templateUrl: './company-add.component.html',
  styleUrls: ['./company-add.component.css']
})
export class CompanyAddComponent extends DialogComponent<null, boolean> {
	

	private poststore: any;
	private post_data: any;
	private error_title: string;
	private error_message: string;
	private success_title: string;
	private success_message: string;
	private headers: Headers;
	public company = new Company();

	addCompanyForm : FormGroup;
	
  constructor(private _perms_service: PermissionService, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute, private _company_service: CompanyService) {
    super(dialogService);
      // Header Variables
    this.headers = new Headers();
    let authToken = localStorage.getItem('id_token');
    this.headers.append('Authorization', `Bearer ${authToken}`);
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');

    

    this.addCompanyForm = _fb.group({
      'name': [this.company.name, [Validators.required]],
      'address': [this.company.address, [Validators.required]],
      'email': [this.company.email, [
            Validators.required,
            Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]],
      'phone_number': [this.company.phone_number, [Validators.required]],
      'contact_person': [this.company.contact_person, [Validators.required]],
      
      
    });  
  }

  public catchError(error: any) {

  }
 public onSubmit() {
		let company_model = this.addCompanyForm.value;

    this._company_service.storeCompany(company_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data);
        this.success_title = "Success!";
        this.success_message = "New company is added!";
        setTimeout(() => {
         this.close();
        }, 1000);
      },
      err => this.catchError(err)
    );

		
	}

  
}