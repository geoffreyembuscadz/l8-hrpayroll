var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core'; //1
import { CompanyService } from '../../services/company.service';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Company } from '../../model/company';
import { DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { CompanyModalComponent } from '../company/company-modal/company-modal.component';
var CompanyComponent = /** @class */ (function () {
    function CompanyComponent(dialogService, _perms_service, modalService, _ar, _company_service, _rt, _fb, _conf) {
        this._perms_service = _perms_service;
        this.modalService = modalService;
        this._ar = _ar;
        this._company_service = _company_service;
        this._rt = _rt;
        this._fb = _fb;
        this._conf = _conf;
        this.dtTrigger = new Subject();
        this.company = new Company();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.api_company = this._conf.ServerWithApiUrl + 'company/';
    }
    CompanyComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    CompanyComponent.prototype.showMe = function (companys) {
        this.company_rec = companys;
    };
    CompanyComponent.prototype.ngOnInit = function () {
        this.companies();
    };
    CompanyComponent.prototype.addModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(CompanyModalComponent, {
            title: 'Add Company',
            button: 'Add',
            url: 'company',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    CompanyComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    CompanyComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    CompanyComponent.prototype.companies = function () {
        var _this = this;
        this._company_service.companies().
            subscribe(function (data) {
            _this.com = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    CompanyComponent.prototype.editCompany = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(CompanyModalComponent, {
            title: 'Update Company',
            button: 'Update',
            edit: true,
            url: 'company',
            id: id
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    CompanyComponent.prototype.archiveCompany = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'category'
        }, { backdropColor: 'rgba(238,238,238)' }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], CompanyComponent.prototype, "dtElement", void 0);
    CompanyComponent = __decorate([
        Component({
            selector: 'app-company',
            templateUrl: './company.component.html',
            styleUrls: ['./company.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            PermissionService,
            DialogService,
            ActivatedRoute,
            CompanyService,
            Router,
            FormBuilder,
            Configuration])
    ], CompanyComponent);
    return CompanyComponent;
}());
export { CompanyComponent };
//# sourceMappingURL=company.component.js.map