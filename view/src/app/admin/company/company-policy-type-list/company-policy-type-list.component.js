var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { CompanyService } from '../../../services/company.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { CompanyPolicyTypeModalComponent } from '../../company/company-policy-type-modal/company-policy-type-modal.component';
var CompanyPolicyTypeListComponent = /** @class */ (function () {
    function CompanyPolicyTypeListComponent(dialogService, modalService, company_service) {
        this.modalService = modalService;
        this.company_service = company_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    CompanyPolicyTypeListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    CompanyPolicyTypeListComponent.prototype.ngOnInit = function () {
        this.getData();
    };
    CompanyPolicyTypeListComponent.prototype.getData = function () {
        var _this = this;
        this.company_service.getCompanyPolicyTypes().
            subscribe(function (data) {
            _this.data = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    CompanyPolicyTypeListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    CompanyPolicyTypeListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    CompanyPolicyTypeListComponent.prototype.archive = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'company_policy_type'
        }).subscribe(function (isConfirmed) {
            _this.getData();
        });
    };
    CompanyPolicyTypeListComponent.prototype.edit = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(CompanyPolicyTypeModalComponent, {
            title: 'Update Company Policy Type',
            button: 'Update',
            edit: true,
            id: id
        }).subscribe(function (isConfirmed) {
            _this.getData();
        });
    };
    CompanyPolicyTypeListComponent.prototype.add = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(CompanyPolicyTypeModalComponent, {
            title: 'Create Company Policy Type',
            button: 'Create',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.getData();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], CompanyPolicyTypeListComponent.prototype, "dtElement", void 0);
    CompanyPolicyTypeListComponent = __decorate([
        Component({
            selector: 'app-company-policy-type-list',
            templateUrl: './company-policy-type-list.component.html',
            styleUrls: ['./company-policy-type-list.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            DialogService,
            CompanyService])
    ], CompanyPolicyTypeListComponent);
    return CompanyPolicyTypeListComponent;
}());
export { CompanyPolicyTypeListComponent };
//# sourceMappingURL=company-policy-type-list.component.js.map