import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { CompanyService } from '../../../services/company.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { CompanyPolicyTypeModalComponent } from '../../company/company-policy-type-modal/company-policy-type-modal.component';


@Component({
  selector: 'app-company-policy-type-list',
  templateUrl: './company-policy-type-list.component.html',
  styleUrls: ['./company-policy-type-list.component.css']
})
export class CompanyPolicyTypeListComponent implements OnInit, AfterViewInit {
	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};

	data:any;

	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	constructor(
  		dialogService: DialogService, 
  		private modalService: DialogService,
  		private company_service: CompanyService
  		){
		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");


	}

	ngAfterViewInit(){
		this.dtTrigger.next();
	}

	ngOnInit() {
		this.getData();
	}
	getData(){

    	this.company_service.getCompanyPolicyTypes().
	      	subscribe(
	        data => {
	          this.data= Array.from(data);
	          this.rerender();
	        },
	        err => console.error(err)
	     );
	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    archive(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'company_policy_type'
        	}).subscribe((isConfirmed)=>{
		       	this.getData();
        });
	}

	edit(id) {
		let disposable = this.modalService.addDialog(CompanyPolicyTypeModalComponent, {
            title:'Update Company Policy Type',
            button:'Update',
		    edit:true,
		    id:id
        	}).subscribe((isConfirmed)=>{
                this.getData();
        });
	}

	add() {
		let disposable = this.modalService.addDialog(CompanyPolicyTypeModalComponent, {
            title:'Create Company Policy Type',
            button:'Create',
		    create:true
        	}).subscribe((isConfirmed)=>{
                this.getData();
        });
	}

}
