import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';//1
import { CompanyService } from '../../services/company.service';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Company } from '../../model/company';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import { Observable } from 'rxjs/Observable';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { CompanyModalComponent } from '../company/company-modal/company-modal.component';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})

@Injectable()
export class CompanyComponent implements OnInit,  AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	public id: string;
	public company_id: any;
	public error_title: string;
	public error_message: string;
	public success_title;
	public success_message;
	public poststore: any;
	public company = new Company();
	companyForm: FormGroup;
	com: any;
	public confirm_archiving: any;
	public form
	private headers: Headers;
	dtOptions: any = {};
	company_rec: any;
	// get permissions list
    perm_rec: any;
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	api_company: String;

  	constructor(
  				dialogService: DialogService, 
  				private _perms_service: PermissionService, 
  				private modalService: DialogService, 
  				private _ar: ActivatedRoute, 
  				private _company_service: CompanyService, 
  				private _rt: Router, 
  				private _fb: FormBuilder, 
  				private _conf: Configuration
  				){
		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	      // Header Variables
		this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.api_company = this._conf.ServerWithApiUrl + 'company/';
	}

	ngAfterViewInit(){
		this.dtTrigger.next();
	}

	showMe(companys: any){
		this.company_rec = companys;
	}

	ngOnInit() {
		this.companies();
		
	}
	addModal() {
		let disposable = this.modalService.addDialog(CompanyModalComponent, {
	        title:'Add Company',
	        button:'Add',
		    url:'company',
		    create:true
	    	}).subscribe((isConfirmed)=>{
	            this.rerender();
	            this.ngOnInit();
	        });
		}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	 }

	rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    companies(){
    	this._company_service.companies().
    	subscribe(
    		data => {
    			this.com = Array.from(data);
    			this.rerender();
    		},
    		err => console.error(err)
    		);
    }
    editCompany(id:any) {
		let disposable = this.modalService.addDialog(CompanyModalComponent, {
            title:'Update Company',
            button:'Update',
		    edit:true,
		    url:'company',
		    id:id
        	}).subscribe((isConfirmed)=>{
                this.rerender();
                this.ngOnInit();
        });
	}
	archiveCompany(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'category'
        	}, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
		       	this.rerender();
                this.ngOnInit();
        });
	}

}
