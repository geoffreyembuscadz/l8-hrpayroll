var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Configuration } from '../../app.config';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AttendanceService } from '../../services/attendance.service';
import { AppraisalService } from '../../services/appraisal.service';
var AlertModalComponent = /** @class */ (function (_super) {
    __extends(AlertModalComponent, _super);
    function AlertModalComponent(dialogService, _fb, _conf, _attendservice, _appraisal_service, _rt) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._conf = _conf;
        _this._attendservice = _attendservice;
        _this._appraisal_service = _appraisal_service;
        _this._rt = _rt;
        _this.attValEmp = false;
        return _this;
    }
    AlertModalComponent.prototype.ngOnInit = function () {
    };
    AlertModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    AlertModalComponent.prototype.confirm = function () {
        var _this = this;
        if (this.attValEmp == true) {
            var data_model = this.model;
            this._attendservice.ignoreEmpInDayAttendance(data_model).subscribe(function (data) {
                _this.poststore = data;
                _this.success_title = "Success";
                _this.success_message = "Attendance was successfully added.";
                setTimeout(function () {
                    _this._rt.navigateByUrl('admin/attendance-list');
                    _this.close();
                }, 3000);
            }, function (err) { return _this.catchError(err); });
        }
        if (this.approved == true) {
            var model = {
                updated_by: this.updated_by,
                status: this.status
            };
            this._appraisal_service.updateAppraisal(this.id, model).subscribe(function (data) {
                _this.poststore = data;
                _this.success_title = "Success";
                _this.success_message = "Attendance was successfully added.";
                setTimeout(function () {
                    _this.close();
                }, 3000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    AlertModalComponent = __decorate([
        Component({
            selector: 'app-alert-modal',
            templateUrl: './alert-modal.component.html',
            styleUrls: ['./alert-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            Configuration,
            AttendanceService,
            AppraisalService,
            Router])
    ], AlertModalComponent);
    return AlertModalComponent;
}(DialogComponent));
export { AlertModalComponent };
//# sourceMappingURL=alert-modal.component.js.map