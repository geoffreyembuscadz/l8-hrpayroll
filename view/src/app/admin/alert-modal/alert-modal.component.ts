import { Component, OnInit } from '@angular/core';
import { Configuration } from '../../app.config';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AttendanceService } from '../../services/attendance.service';
import { AppraisalService } from '../../services/appraisal.service';

@Component({
  selector: 'app-alert-modal',
  templateUrl: './alert-modal.component.html',
  styleUrls: ['./alert-modal.component.css']
})
export class AlertModalComponent extends DialogComponent<null, boolean> {
  
	title: string;
	public id: any;
	public status: any;
	public name: string;
	public success_title;
	public success_message;
	public error_title;
	public error_message;
	public poststore: any;
	attValEmp = false;
	data:any;
	model:any;
	approved: any;
	updated_by : any;

 constructor( 
   dialogService: DialogService, 
   private _fb: FormBuilder, 
   private _conf: Configuration,
   private _attendservice: AttendanceService, 
   private _appraisal_service: AppraisalService, 
   private _rt: Router,
   ){
    super(dialogService);
  }
 
	ngOnInit() {

	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	confirm(){
		
		if (this.attValEmp == true) {
			let data_model = this.model;
    		this._attendservice.ignoreEmpInDayAttendance(data_model).subscribe(
				data => {
					this.poststore = data; 
					
					this.success_title = "Success";
					this.success_message = "Attendance was successfully added.";
					setTimeout(() => {
			   		this._rt.navigateByUrl('admin/attendance-list');
			   		this.close();
	    			}, 3000);
				},
				err => this.catchError(err)
			);
		}

		if(this.approved == true){
			let model = {
				updated_by:this.updated_by,
				status: this.status
			};
    		this._appraisal_service.updateAppraisal(this.id,model).subscribe(
				data => {
					this.poststore = data; 
					
					this.success_title = "Success";
					this.success_message = "Attendance was successfully added.";
					setTimeout(() => {
			   		this.close();
	    			}, 3000);
				},
				err => this.catchError(err)
			);
		}
	}



}
