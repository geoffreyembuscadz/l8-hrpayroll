import { Component } from '@angular/core';
import { CompanyService } from '../../services/company.service';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { AttendanceService } from '../../services/attendance.service';
import { CommonService } from '../../services/common.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import {MomentModule} from 'angular2-moment/moment.module';
import { Select2OptionData } from 'ng2-select2';
import * as moment from 'moment';
import { AuthUserService } from '../../services/auth-user.service';

@Component({
  selector: 'app-attendance-report',
  templateUrl: './attendance-report.component.html',
  styleUrls: ['./attendance-report.component.css']
})
export class AttendanceReportComponent {
  public error_title: string;
  public error_message: string;
  public success_title;
  public success_message;
  public employee : any;
  public employee_current : any;
  public department_current : any;
  public company_current : any;
  start_date: any;
  end_date: any;
  company: any;
  department: any;
  employee_value: Array<Select2OptionData>;
  department_value: Array<Select2OptionData>;
  company_value: Array<Select2OptionData>;
  public barChartOptions:any;
  public barChartLabels: any = ['2006'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any = [
  {data: [0], label: 'Present'},
  {data: [0], label: 'Absent'}
  ];

  user_id:any;
  supervisor_id:any;

  constructor(
    private daterangepickerOptions: DaterangepickerConfig,
    private _common_service: CommonService,
    private _company_service: CompanyService,
    private _attend_service: AttendanceService,
    private _fb: FormBuilder,
    private _auth_service: AuthUserService
    ){
    this.daterangepickerOptions.settings = {
      locale: { format: 'MM/DD/YYYY' },
      alwaysShowCalendars: false
    }; 
  }
  public mainInput = {
    start: moment().subtract(12, 'month'),
    end: moment().subtract(6, 'month')
  }

  public chartClicked(e:any):void {

  }
  public chartHovered(e:any):void {

  }
  public initializeBarGraph(){
    this.barChartOptions = {
      scaleShowVerticalLines: false,
      responsive: true,
      backgroundColor: [
      'rgba(255, 99, 132, 0.2)',
      'rgba(54, 162, 235, 0.2)',
      'rgba(255, 206, 86, 0.2)',
      'rgba(75, 192, 192, 0.2)',
      'rgba(153, 102, 255, 0.2)',
      'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
      'rgba(255,99,132,1)',
      'rgba(54, 162, 235, 1)',
      'rgba(255, 206, 86, 1)',
      'rgba(75, 192, 192, 1)',
      'rgba(153, 102, 255, 1)',
      'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    };
    this.barChartType = 'bar';
    this.barChartLegend = true;
  }
  public catchError(error: any) {
    let response_body = error._body;
    let response_status = error.status;
    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'The given data failed to pass validation.'; } 
      else if( response_status == 200 ) {
        this.error_title = '';
        this.error_message = '';
      }
    }
    ngOnInit(){
      this.getCompanys();
      this.getDepartments();
      this.get_UserId();
      // this.getSelectedDate(); 
      this.onSearch();
      this.initializeBarGraph();
    }

    get_UserId(){
    this._auth_service.getUser().
    subscribe(
      data => {
        let user = data;
        this.user_id = user.id;
        this.getEmployeeId();
      },
      err => console.error(err)
    );
  }

    getEmployeeId(){
      this._auth_service.getUserById(this.user_id).
      subscribe(
        data => {
          let user = data;
          this.supervisor_id = user.employee_id;
          this.getEmployees();
        },
        err => console.error(err)
      );
    }

    getCompanys(){
      this._common_service.getCompany()
      .subscribe(
        data => {
          this.company = Array.from(data);
          this.company_value = [];
          this.company_current = this.company_value;
        },
        err => this.catchError(err)
        );
    }

    getDepartments(){
      this._common_service.getDepartment()
      .subscribe(
        data => {
          this.department = Array.from(data);
          this.department_value = [];
          this.department_current = this.department_value;
        },
        err => this.catchError(err)
        );
    }

    getEmployees(){
      
      let id = this.supervisor_id;

      this._common_service.getEmployees(id)
      .subscribe(
        data => {
          this.employee = Array.from(data);
          this.employee_value = [];
          this.employee_current = this.employee_value;
        },
        err => this.catchError(err)
        );
    }

    changedEmployee(data: any) {
      this.employee_current = data.value;
    }  
    changedDepartment(data: any) {
      this.department_current = data.value;
    } 
    changedCompany(data: any) {
      this.company_current = data.value;
    }  
    getSelectedDate(){
      this._attend_service.selectedDate()
      .subscribe(
        data => {
          this.barChartLabels = Array.from(data);
          console.log('barChartLabels:',this.barChartLabels);
        },
        err => this.catchError(err)
        );
    }
    onSearch(){
      let emp = this.employee_current;
      let company = this.company_current;
      let dept = this.department_current;
      let start = this.start_date;
      let end = this.end_date;
      this._attend_service
      .attendanceReport(emp,company,dept, start, end)
      .subscribe(
        data => {
          this.barChartData = Array.from(data);
        },
        err => this.catchError(err)
        );
    }
    private selectedDate(value: any, dateInput: any) {
      dateInput.start = value.start;
      dateInput.end = value.end;
      this.start_date = dateInput.start;
      this.end_date = dateInput.end;
    }
  }