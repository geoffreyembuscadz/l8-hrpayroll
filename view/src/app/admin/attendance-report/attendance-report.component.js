var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { CompanyService } from '../../services/company.service';
import { FormBuilder } from '@angular/forms';
import { AttendanceService } from '../../services/attendance.service';
import { CommonService } from '../../services/common.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
import { AuthUserService } from '../../services/auth-user.service';
var AttendanceReportComponent = /** @class */ (function () {
    function AttendanceReportComponent(daterangepickerOptions, _common_service, _company_service, _attend_service, _fb, _auth_service) {
        this.daterangepickerOptions = daterangepickerOptions;
        this._common_service = _common_service;
        this._company_service = _company_service;
        this._attend_service = _attend_service;
        this._fb = _fb;
        this._auth_service = _auth_service;
        this.barChartLabels = ['2006'];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barChartData = [
            { data: [0], label: 'Present' },
            { data: [0], label: 'Absent' }
        ];
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(6, 'month')
        };
        this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false
        };
    }
    AttendanceReportComponent.prototype.chartClicked = function (e) {
    };
    AttendanceReportComponent.prototype.chartHovered = function (e) {
    };
    AttendanceReportComponent.prototype.initializeBarGraph = function () {
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        };
        this.barChartType = 'bar';
        this.barChartLegend = true;
    };
    AttendanceReportComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    AttendanceReportComponent.prototype.ngOnInit = function () {
        this.getCompanys();
        this.getDepartments();
        this.get_UserId();
        // this.getSelectedDate(); 
        this.onSearch();
        this.initializeBarGraph();
    };
    AttendanceReportComponent.prototype.get_UserId = function () {
        var _this = this;
        this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
            _this.getEmployeeId();
        }, function (err) { return console.error(err); });
    };
    AttendanceReportComponent.prototype.getEmployeeId = function () {
        var _this = this;
        this._auth_service.getUserById(this.user_id).
            subscribe(function (data) {
            var user = data;
            _this.supervisor_id = user.employee_id;
            _this.getEmployees();
        }, function (err) { return console.error(err); });
    };
    AttendanceReportComponent.prototype.getCompanys = function () {
        var _this = this;
        this._common_service.getCompany()
            .subscribe(function (data) {
            _this.company = Array.from(data);
            _this.company_value = [];
            _this.company_current = _this.company_value;
        }, function (err) { return _this.catchError(err); });
    };
    AttendanceReportComponent.prototype.getDepartments = function () {
        var _this = this;
        this._common_service.getDepartment()
            .subscribe(function (data) {
            _this.department = Array.from(data);
            _this.department_value = [];
            _this.department_current = _this.department_value;
        }, function (err) { return _this.catchError(err); });
    };
    AttendanceReportComponent.prototype.getEmployees = function () {
        var _this = this;
        var id = this.supervisor_id;
        this._common_service.getEmployees(id)
            .subscribe(function (data) {
            _this.employee = Array.from(data);
            _this.employee_value = [];
            _this.employee_current = _this.employee_value;
        }, function (err) { return _this.catchError(err); });
    };
    AttendanceReportComponent.prototype.changedEmployee = function (data) {
        this.employee_current = data.value;
    };
    AttendanceReportComponent.prototype.changedDepartment = function (data) {
        this.department_current = data.value;
    };
    AttendanceReportComponent.prototype.changedCompany = function (data) {
        this.company_current = data.value;
    };
    AttendanceReportComponent.prototype.getSelectedDate = function () {
        var _this = this;
        this._attend_service.selectedDate()
            .subscribe(function (data) {
            _this.barChartLabels = Array.from(data);
            console.log('barChartLabels:', _this.barChartLabels);
        }, function (err) { return _this.catchError(err); });
    };
    AttendanceReportComponent.prototype.onSearch = function () {
        var _this = this;
        var emp = this.employee_current;
        var company = this.company_current;
        var dept = this.department_current;
        var start = this.start_date;
        var end = this.end_date;
        this._attend_service
            .attendanceReport(emp, company, dept, start, end)
            .subscribe(function (data) {
            _this.barChartData = Array.from(data);
        }, function (err) { return _this.catchError(err); });
    };
    AttendanceReportComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        this.start_date = dateInput.start;
        this.end_date = dateInput.end;
    };
    AttendanceReportComponent = __decorate([
        Component({
            selector: 'app-attendance-report',
            templateUrl: './attendance-report.component.html',
            styleUrls: ['./attendance-report.component.css']
        }),
        __metadata("design:paramtypes", [DaterangepickerConfig,
            CommonService,
            CompanyService,
            AttendanceService,
            FormBuilder,
            AuthUserService])
    ], AttendanceReportComponent);
    return AttendanceReportComponent;
}());
export { AttendanceReportComponent };
//# sourceMappingURL=attendance-report.component.js.map