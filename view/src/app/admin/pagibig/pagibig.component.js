var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable } from '@angular/core';
import { PagibigService } from '../../services/pagibig.service';
var PagibigComponent = /** @class */ (function () {
    function PagibigComponent(_pagibig_service) {
        this._pagibig_service = _pagibig_service;
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
    }
    PagibigComponent.prototype.ngAfterViewInit = function () {
    };
    PagibigComponent.prototype.showMe = function (PagibigList) {
        this.pagibig_rec = PagibigList;
    };
    PagibigComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Server Communication
        var pagibig = this._pagibig_service.getPagibig().
            subscribe(function (data) {
            _this.pagibig_rec = Array.from(data); // fetched record
        }, function (err) { return console.error(err); });
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    };
    PagibigComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    PagibigComponent = __decorate([
        Component({
            selector: 'app-pagibig',
            templateUrl: './pagibig.component.html',
            styleUrls: ['./pagibig.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [PagibigService])
    ], PagibigComponent);
    return PagibigComponent;
}());
export { PagibigComponent };
//# sourceMappingURL=pagibig.component.js.map