import { Component, OnInit, Injectable } from '@angular/core';
import { PagibigService } from '../../services/pagibig.service';


@Component({
  selector: 'app-pagibig',
  templateUrl: './pagibig.component.html',
  styleUrls: ['./pagibig.component.css']
})

@Injectable()
export class PagibigComponent implements OnInit {

dtOptions: any = {};
	pagibig_rec: any;
	bodyClasses:string = "skin-blue sidebar-mini";
 	body = document.getElementsByTagName('body')[0];

	constructor(private _pagibig_service: PagibigService){

	}

	ngAfterViewInit(){

	}

	showMe(PagibigList: any){
		this.pagibig_rec = PagibigList;
	}

	ngOnInit() {
		// Server Communication
		let pagibig = this._pagibig_service.getPagibig().
			subscribe(
				data => {
					this.pagibig_rec = Array.from(data); // fetched record
				},
				err => console.error(err)
			);

		//add the the body classes
	    this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");
	}

	ngOnDestroy() {
	    //remove the the body classes
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	  }

}

