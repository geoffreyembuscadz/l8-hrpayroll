var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { QuestionService } from '../../services/question.service';
import { AppraisalService } from '../../services/appraisal.service';
import { FormBuilder } from '@angular/forms';
import { AuthUserService } from '../../services/auth-user.service';
import { DevelopmentService } from '../../services/development.service';
import { ObjectiveModalComponent } from '../appraisal/objective-modal/objective-modal.component';
import { DevelopmentModalComponent } from '../appraisal/development-modal/development-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
var AppraisalComponent = /** @class */ (function () {
    function AppraisalComponent(modalService, _conf, _q_service, _dev_service, _fb, _auth_service, _appraisal_service) {
        this.modalService = modalService;
        this._conf = _conf;
        this._q_service = _q_service;
        this._dev_service = _dev_service;
        this._fb = _fb;
        this._auth_service = _auth_service;
        this._appraisal_service = _appraisal_service;
        this.dtTrigger = new Subject();
        this.hidden = "hidden";
        this.selectedEmployee = null;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api = this._conf.ServerWithApiUrl + 'evaluation/';
        this.Form =
            _fb.group({
                'supervisor_id': ['null'],
                'employee_id': ['null']
            });
    }
    AppraisalComponent.prototype.ngOnInit = function () {
        this.get_UserId();
    };
    AppraisalComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    AppraisalComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    AppraisalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    AppraisalComponent.prototype.get_UserId = function () {
        var _this = this;
        this._auth_service.getUser().
            subscribe(function (data) {
            var user = data;
            _this.user_id = user.id;
            _this.supervisor_id = user.id;
            _this.getEmployee();
        }, function (err) { return console.error(err); });
    };
    AppraisalComponent.prototype.getEmployee = function () {
        var _this = this;
        var model = this.Form.value;
        this.Form.value.supervisor_id = this.supervisor_id;
        this._q_service.getEmployee(model).
            subscribe(function (data) {
            _this.employee = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    AppraisalComponent.prototype.setActiveEmployee = function (employee) {
        this.selectedEmployee = employee;
        var myJSON = JSON.stringify(this.selectedEmployee);
        this.getObjectives();
        this.showDevelopment();
    };
    AppraisalComponent.prototype.addObj = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ObjectiveModalComponent, {
            title: 'Objectives',
            button: 'Add',
            employee_id: this.selectedEmployee.id,
            create: true
        }).subscribe(function (isConfirmed) {
            _this.getObjectives();
        });
    };
    AppraisalComponent.prototype.addDevelopment = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(DevelopmentModalComponent, {
            title: 'Personal Development Plan',
            button: 'Add',
            url: 'appraisal',
            create: true,
            employee_id: this.selectedEmployee.id
        }).subscribe(function (isConfirmed) {
            _this.ngOnInit();
        });
    };
    AppraisalComponent.prototype.getObjectives = function () {
        var _this = this;
        this.Form.value.employee_id = this.selectedEmployee.id;
        var model = this.Form.value;
        this._appraisal_service.getObjectives(model).subscribe(function (data) {
            _this.obj = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    AppraisalComponent.prototype.showDevelopment = function () {
        var _this = this;
        this.Form.value.employee_id = this.selectedEmployee.id;
        var model = this.Form.value;
        this._dev_service.showDevelopment(model).subscribe(function (data) {
            _this.dev = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    AppraisalComponent.prototype.editObj = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ObjectiveModalComponent, {
            title: 'Update Objective',
            button: 'Update',
            edit: true,
            url: 'appraisal',
            id: id
        }).subscribe(function (isConfirmed) {
            _this.ngOnInit();
        });
    };
    AppraisalComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    AppraisalComponent.prototype.archiveObj = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'appraisal'
        }, { backdropColor: 'rgba(238,238,238)' }).subscribe(function (isConfirmed) {
            _this.ngOnInit();
        });
    };
    AppraisalComponent.prototype.archiveDev = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'development'
        }, { backdropColor: 'rgba(238,238,238)' }).subscribe(function (isConfirmed) {
            _this.showDevelopment();
        });
    };
    AppraisalComponent.prototype.editDev = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(DevelopmentModalComponent, {
            title: 'Update Objective',
            button: 'Update',
            edit: true,
            url: 'development',
            id: id
        }).subscribe(function (isConfirmed) {
            _this.showDevelopment();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], AppraisalComponent.prototype, "dtElement", void 0);
    AppraisalComponent = __decorate([
        Component({
            selector: 'app-appraisal',
            templateUrl: './appraisal.component.html',
            styleUrls: ['./appraisal.component.css'],
        }),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            QuestionService,
            DevelopmentService,
            FormBuilder,
            AuthUserService,
            AppraisalService])
    ], AppraisalComponent);
    return AppraisalComponent;
}());
export { AppraisalComponent };
//# sourceMappingURL=appraisal.component.js.map