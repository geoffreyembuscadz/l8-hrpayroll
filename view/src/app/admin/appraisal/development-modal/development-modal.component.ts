import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response} from '@angular/http';
import { QuestionService } from '../../../services/question.service';
import { DevelopmentService } from '../../../services/development.service';
import { Configuration } from '../../../app.config';
import { ConfirmModalComponent } from './../../confirm-modal/confirm-modal.component';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';

@Component({
	selector: 'app-development-modal',
	templateUrl: './development-modal.component.html',
	styleUrls: ['./development-modal.component.css']
})
export class DevelopmentModalComponent extends DialogComponent<null, boolean> {
	Form : FormGroup;
	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public poststore: any;
	public id: any;
	edit:any;
	create:any;
	url:any;
	first_month_format: any;
	thrd_month_format: any;
	six_months_format: any;
	one_year_format: any;
	dates: any;
	month: any;
	development: any;
	achieve_role: any;
	development_role: any;
	first_month_check: any;
	thrd_month_check: any;
	six_months_check: any;
	one_year_check: any;
	employee_id: any;

	public mainInput = {
		start: moment().subtract(12, 'month'),                              
		end: moment().subtract(11, 'month')
	}

	constructor(
		dialogService: DialogService, 
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute, 
		private _dev_service: DevelopmentService,  
		private _rt: Router,
		private modalService: DialogService, 
		private daterangepickerOptions: DaterangepickerConfig
		) {
		super(dialogService);

		this.Form = _fb.group({
			'employee_id': 			['null'],
			'development': 			['null'],
			'achieve_role': 		['null'],
			'development_role': 	['null'],
			'due_by':				['null'],
			'months':				['null']

		});  

		this.daterangepickerOptions.settings = {
			locale: { format: 'MM/DD/YYYY' },
			alwaysShowCalendars: false,
			singleDatePicker: true,
			showDropdowns: true,
			opens: "center"
		};  

		// get the future date
		let first_month = moment().add(1, 'months');
		this.first_month_format = moment(first_month).format("YYYY-MM-DD");

		let thrd_month = moment().add(3, 'months');
		this.thrd_month_format = moment(thrd_month).format("YYYY-MM-DD");

		let six_months = moment().add(6, 'months');
		this.six_months_format = moment(six_months).format("YYYY-MM-DD");

		let one_year = moment().add(1, 'years');
		this.one_year_format = moment(one_year).format("YYYY-MM-DD");

	}

	ngOnInit() {
		this.getData();
	}
	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}
	archive(){
		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
			title:'Archive Data',
			message:'Are you sure you want to archive this data?',
			action:'Delete',
			id:this.id,
			url:this.url
		}).subscribe((isConfirmed)=>{
			setTimeout(() => {
				this.close();

			}, 1000);

		});
	}

	private selectedDate(value:any, dateInput:any) {
        dateInput.start = value.start;
       	this.dates = moment(dateInput.start).format("YYYY-MM-DD");
    }

    due(x){

    	if (x == 1) {
    		this.mainInput.start = this.first_month_format;
    		this.dates = this.mainInput.start;
    		this.month = 1;
    	} else if (x == 3) {
    		this.mainInput.start = this.thrd_month_format;
    		this.dates = this.mainInput.start;
    		this.month = 3;
    	} else if (x == 6) {
    		this.mainInput.start = this.six_months_format;
    		this.dates = this.mainInput.start;
    		this.month = 6;
    	} else if (x == 12) {
    		this.mainInput.start = this.one_year_format;
    		this.dates = this.mainInput.start;
    		this.month = 12;
    	}
    }


	onSubmit() {
		this.Form.value.due =  this.dates;
		this.Form.value.employee_id = this.employee_id;
		let model = this.Form.value;


		if(this.edit == true){
			let id = this.id;
			this._dev_service.updateDevelopment(id, model)
			.subscribe(
				data => {
					this.poststore = Array.from(data); 
					this.success_title = "Success!";
					this.success_message = "Successfully updated";
					setTimeout(() => {
						this.close();
					}, 1000);
				},
				err => this.catchError(err)
				);
		} else if ( this.create = true ) {
			this._dev_service.storeDevelopment(model)
			.subscribe(
				data => {
					this.poststore = Array.from(data); 
					this.success_title = "Success!";
					this.success_message = "Successfully created";
					setTimeout(() => {
						this.close();
					}, 1000);
				},
				err => this.catchError(err)
				);		
		}
	}

	getData(){
    	let id = this.id;
    	this._dev_service.getDevelopment(id).subscribe(
    		data => {
    			this.setType(data);
    		},
    		err => console.error(err)
    		);
    }

    public setType(type: any){
    	this.id = type.id;
    	this.development = type.development;
    	this.achieve_role = type.achieve_role;
    	this.development_role = type.development_role;
    	this.mainInput.start = type.due;
    	this.month = type.months;


    	if ( this.month == 1 ) {
    		this.first_month_check = true;
    	} else if(this.month == 3){
    		this.thrd_month_check = true;
    	} else if(this.month == 6){
    		this.six_months_check = true;
    	} else if(this.month == 12){
    		this.one_year_check =true;
    	}
    }

}