import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core'
;import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { QuestionService } from '../../services/question.service';
import { AppraisalService } from '../../services/appraisal.service';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng2-select2';
import { AuthUserService } from '../../services/auth-user.service';
import { DevelopmentService } from '../../services/development.service';
import { ObjectiveModalComponent } from '../appraisal/objective-modal/objective-modal.component';
import { DevelopmentModalComponent } from '../appraisal/development-modal/development-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-appraisal',
  templateUrl: './appraisal.component.html',
  styleUrls: ['./appraisal.component.css'],

 
})
export class AppraisalComponent implements OnInit, AfterViewInit {


	public Form: FormGroup;
	
	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public emp_contract_statuses: any;
	public poststore: any;
    private hidden:string="hidden";

	api: String;
	user_id:any;
	supervisor_id: any;
	employee: any;
	obj: any;
	dev: any;
	selectedEmployee: any = null;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _q_service: QuestionService,
		private _dev_service: DevelopmentService,
		private _fb: FormBuilder,
		private _auth_service: AuthUserService,
		private _appraisal_service: AppraisalService,

		){

		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");

		this.api = this._conf.ServerWithApiUrl + 'evaluation/';

		this.Form = 
			_fb.group({
				'supervisor_id': 	['null'],
				'employee_id':		['null']
		});

	}

 	ngOnInit() { 	
 		this.get_UserId();			
 	}
	
	ngOnDestroy() {
		this.body.classList.remove("skin-blue");
		this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
		this.dtTrigger.next();
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	get_UserId(){
		this._auth_service.getUser().
		subscribe(
			data => {
				let user = data;
				this.user_id = user.id;
				this.supervisor_id = user.id;
        		this.getEmployee();     
			},
			err => console.error(err)
		);

    }

	getEmployee(){
		let  model = this.Form.value;
		this.Form.value.supervisor_id = this.supervisor_id;
		this._q_service.getEmployee(model).
		subscribe(
			data => {
				this.employee = Array.from(data);	
			},
			err => console.error(err)
			);
	}

	setActiveEmployee(employee: any) {
		this.selectedEmployee = employee;
		let myJSON = JSON.stringify(this.selectedEmployee);
		this.getObjectives();
		this.showDevelopment();
	}

	
	addObj() {
		let disposable = this.modalService.addDialog(ObjectiveModalComponent, {
            title:'Objectives',
            button:'Add',
            employee_id: this.selectedEmployee.id,
		    create:true
        	}).subscribe((isConfirmed)=>{
        		this.getObjectives();

            });
	}

	addDevelopment() {
		let disposable = this.modalService.addDialog(DevelopmentModalComponent, {
            title:'Personal Development Plan',
            button:'Add',
		    url:'appraisal',
		    create:true,
		    employee_id: this.selectedEmployee.id
        	}).subscribe((isConfirmed)=>{
                this.ngOnInit();
            });
	}

	getObjectives(){
		this.Form.value.employee_id = this.selectedEmployee.id;
		let model = this.Form.value;
        this._appraisal_service.getObjectives(model).subscribe(
          data => {
            this.obj = Array.from(data);
          },
          err => console.error(err)
          );
    }

    showDevelopment(){
    	this.Form.value.employee_id = this.selectedEmployee.id;
		let model = this.Form.value;
        this._dev_service.showDevelopment(model).subscribe(
          data => {
            this.dev = Array.from(data);
          },
          err => console.error(err)
          );
    }

    editObj(id:any) {
		let disposable = this.modalService.addDialog(ObjectiveModalComponent, {
            title:'Update Objective',
            button:'Update',
		    edit:true,
		    url:'appraisal',
		    id:id
        	}).subscribe((isConfirmed)=>{
                this.ngOnInit();
        });
	}

	rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    archiveObj(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'appraisal'
        	}, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
                this.ngOnInit();
        });
	}

	archiveDev(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'development'
        	}, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
                this.showDevelopment();
        });
	}

	editDev(id:any) {
		let disposable = this.modalService.addDialog(DevelopmentModalComponent, {
            title:'Update Objective',
            button:'Update',
		    edit:true,
		    url:'development',
		    id:id
        	}).subscribe((isConfirmed)=>{
                this.showDevelopment();
        });
	}

	
	

}

