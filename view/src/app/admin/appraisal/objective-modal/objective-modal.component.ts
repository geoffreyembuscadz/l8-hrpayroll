import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response} from '@angular/http';
import { QuestionService } from '../../../services/question.service';
import { AppraisalService } from '../../../services/appraisal.service';
import { CommonService } from '../../../services/common.service';
import { Configuration } from '../../../app.config';
import { ConfirmModalComponent } from './../../confirm-modal/confirm-modal.component';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { DaterangepickerConfig } from 'ng2-daterangepicker';

@Component({
	selector: 'app-objective-modal',
	templateUrl: './objective-modal.component.html',
	styleUrls: ['./objective-modal.component.css']
})
export class ObjectiveModalComponent extends DialogComponent<null, boolean> {
	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public poststore: any;
	public id: any;

	date = moment().format("YYYY-MM-DD");
	first_month = moment().add(1, 'months');
	today = new Date();
	edit:any;
	create:any;
	url:any;
	Form : FormGroup;
	save = false;
	dates: any;
	first_month_format: any;
	thrd_month_format: any;
	six_months_format: any;
	one_year_format: any;
	implement: any;
	objective: any;
	achieve_obj: any;
	one_month: any;
	three_months: any;
	six_months: any;
	twelve_months: any;
	employee_id: any;
	first_month_check = false;
	thrd_month_check = false;
	six_months_check = false;
	one_year_check = false;
	month:any;
	status_id: any;

	public mainInput = {
		start: moment().subtract(12, 'month'),                              
		end: moment().subtract(11, 'month')
	}

	constructor(
		dialogService: DialogService, 
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute, 
		private _q_service: QuestionService,  
		private _appraisal_service: AppraisalService,  
		private _common_service: CommonService,  
		private _rt: Router,
		private modalService: DialogService,
		private daterangepickerOptions: DaterangepickerConfig ) {
		super(dialogService);

		this.Form = _fb.group({
			'objective':		[null],
			'achieve_obj':		[null],
			'implement':		[null],
			'due':				[null],
			'months':			[null],
			'employee_id':		[null],
			'status':		[null],
		}); 

		this.daterangepickerOptions.settings = {
			locale: { format: 'MM/DD/YYYY' },
			alwaysShowCalendars: false,
			singleDatePicker: true,
			showDropdowns: true,
			opens: "center"
		};  

		// get the future date
		let first_month = moment().add(1, 'months');
		this.first_month_format = moment(first_month).format("YYYY-MM-DD");

		let thrd_month = moment().add(3, 'months');
		this.thrd_month_format = moment(thrd_month).format("YYYY-MM-DD");

		let six_months = moment().add(6, 'months');
		this.six_months_format = moment(six_months).format("YYYY-MM-DD");

		let one_year = moment().add(1, 'years');
		this.one_year_format = moment(one_year).format("YYYY-MM-DD");


		// get the number of months/year

		let one = moment(this.first_month_format).diff(new Date(this.date), 'months', true);
		let three = moment(this.first_month_format).diff(new Date(this.date), 'months', true);
		let six = moment(this.first_month_format).diff(new Date(this.date), 'months', true);
		let twelve = moment(this.first_month_format).diff(new Date(this.date), 'months', true);

		this.one_month = Math.ceil(one);
		this.three_months = Math.ceil(three);
		this.six_months = Math.ceil(six);
		this.twelve_months = Math.ceil(twelve);

	}

	ngOnInit() {
		if(this.edit == true){
			this.getData();
		}
	}
	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;
		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}
	archive(){
		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
			title:'Archive Data',
			message:'Are you sure you want to archive this data?',
			action:'Delete',
			id:this.id,
			url:this.url
		}).subscribe((isConfirmed)=>{
			setTimeout(() => {
				this.close();

			}, 1000);

		});
	}


	onSubmit() {

		this.Form.value.implement =  this.date;
		this.Form.value.due =  this.dates;
		this.Form.value.months =  this.month;
		this.Form.value.employee_id =  this.employee_id;
		this.Form.value.status =  1;		
		let model = this.Form.value;
		console.log(model);
		if(this.edit == true){
			let id = this.id;
			this._appraisal_service.updateAppraisal(id, model)
			.subscribe(
				data => {
					this.poststore = Array.from(data); 
					this.success_title = "Success!";
					this.success_message = "Successfully updated";
					setTimeout(() => {
						this.close();
					}, 1000);
				},
				err => this.catchError(err)
				);
		} else if ( this.create = true ) {
			this._appraisal_service.storeAppraisal(model)
			.subscribe(
				data => {
					this.poststore = Array.from(data); 
					this.success_title = "Success!";
					this.success_message = "Successfully created";
					setTimeout(() => {
						this.close();
					}, 1000);
				},
				err => this.catchError(err)
				);		
		}
	}

    private selectedDate(value:any, dateInput:any) {
        dateInput.start = value.start;
       	this.dates = moment(dateInput.start).format("YYYY-MM-DD");
    }

    due(x){

    	if (x == 1) {
    		this.mainInput.start = this.first_month_format;
    		this.dates = this.mainInput.start;
    		this.month = 1;
    	} else if (x == 3) {
    		this.mainInput.start = this.thrd_month_format;
    		this.dates = this.mainInput.start;
    		this.month = 3;
    	} else if (x == 6) {
    		this.mainInput.start = this.six_months_format;
    		this.dates = this.mainInput.start;
    		this.month = 6;
    	} else if (x == 12) {
    		this.mainInput.start = this.one_year_format;
    		this.dates = this.mainInput.start;
    		this.month = 12;
    	}
    }

    getData(){
    	let id = this.id;
    	this._appraisal_service.getAppraisal(id).subscribe(
    		data => {
    			this.setType(data);
    		},
    		err => console.error(err)
    		);
    }

    public setType(type: any){
    	this.id = type.id;
    	this.implement = type.implement;
    	this.objective = type.objective;
    	this.achieve_obj = type.achieve_obj;
    	this.mainInput.start = type.due;
    	this.month = type.months;


    	if ( this.month == 1 ) {
    		this.first_month_check = true;
    	} else if(this.month == 3){
    		this.thrd_month_check = true;
    	} else if(this.month == 6){
    		this.six_months_check = true;
    	} else if(this.month == 12){
    		this.one_year_check =true;
    	}
    }

}