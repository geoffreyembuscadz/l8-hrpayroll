var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { QuestionService } from '../../../services/question.service';
import { AppraisalService } from '../../../services/appraisal.service';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from './../../confirm-modal/confirm-modal.component';
import * as moment from 'moment';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
var ObjectiveModalComponent = /** @class */ (function (_super) {
    __extends(ObjectiveModalComponent, _super);
    function ObjectiveModalComponent(dialogService, _fb, _ar, _q_service, _appraisal_service, _common_service, _rt, modalService, daterangepickerOptions) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._q_service = _q_service;
        _this._appraisal_service = _appraisal_service;
        _this._common_service = _common_service;
        _this._rt = _rt;
        _this.modalService = modalService;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this.date = moment().format("YYYY-MM-DD");
        _this.first_month = moment().add(1, 'months');
        _this.today = new Date();
        _this.save = false;
        _this.first_month_check = false;
        _this.thrd_month_check = false;
        _this.six_months_check = false;
        _this.one_year_check = false;
        _this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        _this.Form = _fb.group({
            'objective': [null],
            'achieve_obj': [null],
            'implement': [null],
            'due': [null],
            'months': [null],
            'employee_id': [null],
            'status': [null],
        });
        _this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
            showDropdowns: true,
            opens: "center"
        };
        // get the future date
        var first_month = moment().add(1, 'months');
        _this.first_month_format = moment(first_month).format("YYYY-MM-DD");
        var thrd_month = moment().add(3, 'months');
        _this.thrd_month_format = moment(thrd_month).format("YYYY-MM-DD");
        var six_months = moment().add(6, 'months');
        _this.six_months_format = moment(six_months).format("YYYY-MM-DD");
        var one_year = moment().add(1, 'years');
        _this.one_year_format = moment(one_year).format("YYYY-MM-DD");
        // get the number of months/year
        var one = moment(_this.first_month_format).diff(new Date(_this.date), 'months', true);
        var three = moment(_this.first_month_format).diff(new Date(_this.date), 'months', true);
        var six = moment(_this.first_month_format).diff(new Date(_this.date), 'months', true);
        var twelve = moment(_this.first_month_format).diff(new Date(_this.date), 'months', true);
        _this.one_month = Math.ceil(one);
        _this.three_months = Math.ceil(three);
        _this.six_months = Math.ceil(six);
        _this.twelve_months = Math.ceil(twelve);
        return _this;
    }
    ObjectiveModalComponent.prototype.ngOnInit = function () {
        if (this.edit == true) {
            this.getData();
        }
    };
    ObjectiveModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    ObjectiveModalComponent.prototype.archive = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: this.id,
            url: this.url
        }).subscribe(function (isConfirmed) {
            setTimeout(function () {
                _this.close();
            }, 1000);
        });
    };
    ObjectiveModalComponent.prototype.onSubmit = function () {
        var _this = this;
        this.Form.value.implement = this.date;
        this.Form.value.due = this.dates;
        this.Form.value.months = this.month;
        this.Form.value.employee_id = this.employee_id;
        this.Form.value.status = 1;
        var model = this.Form.value;
        console.log(model);
        if (this.edit == true) {
            var id = this.id;
            this._appraisal_service.updateAppraisal(id, model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success!";
                _this.success_message = "Successfully updated";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
        else if (this.create = true) {
            this._appraisal_service.storeAppraisal(model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success!";
                _this.success_message = "Successfully created";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    ObjectiveModalComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        this.dates = moment(dateInput.start).format("YYYY-MM-DD");
    };
    ObjectiveModalComponent.prototype.due = function (x) {
        if (x == 1) {
            this.mainInput.start = this.first_month_format;
            this.dates = this.mainInput.start;
            this.month = 1;
        }
        else if (x == 3) {
            this.mainInput.start = this.thrd_month_format;
            this.dates = this.mainInput.start;
            this.month = 3;
        }
        else if (x == 6) {
            this.mainInput.start = this.six_months_format;
            this.dates = this.mainInput.start;
            this.month = 6;
        }
        else if (x == 12) {
            this.mainInput.start = this.one_year_format;
            this.dates = this.mainInput.start;
            this.month = 12;
        }
    };
    ObjectiveModalComponent.prototype.getData = function () {
        var _this = this;
        var id = this.id;
        this._appraisal_service.getAppraisal(id).subscribe(function (data) {
            _this.setType(data);
        }, function (err) { return console.error(err); });
    };
    ObjectiveModalComponent.prototype.setType = function (type) {
        this.id = type.id;
        this.implement = type.implement;
        this.objective = type.objective;
        this.achieve_obj = type.achieve_obj;
        this.mainInput.start = type.due;
        this.month = type.months;
        if (this.month == 1) {
            this.first_month_check = true;
        }
        else if (this.month == 3) {
            this.thrd_month_check = true;
        }
        else if (this.month == 6) {
            this.six_months_check = true;
        }
        else if (this.month == 12) {
            this.one_year_check = true;
        }
    };
    ObjectiveModalComponent = __decorate([
        Component({
            selector: 'app-objective-modal',
            templateUrl: './objective-modal.component.html',
            styleUrls: ['./objective-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            QuestionService,
            AppraisalService,
            CommonService,
            Router,
            DialogService,
            DaterangepickerConfig])
    ], ObjectiveModalComponent);
    return ObjectiveModalComponent;
}(DialogComponent));
export { ObjectiveModalComponent };
//# sourceMappingURL=objective-modal.component.js.map