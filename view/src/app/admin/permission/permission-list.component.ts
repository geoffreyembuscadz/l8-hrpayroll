import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';

import { Permission } from '../../model/permission';
import { PermissionCreateComponent } from './permission-create.component';
import { PermissionEditComponent } from './permission-edit.component';
import { PermissionService } from '../../services/permission.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'admin-permissions-list',
  templateUrl: './permission-list.component.html',
  styleUrls: ['./permission-list.component.css']
})

@Injectable()
export class PermissionListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	public id: string;
	public perms_id: any;
	public permission = new Permission();

	public error_title: string;
    public error_message: string;
    ad_perms: any;

	dtOptions: any = {};
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

	constructor(private _conf: Configuration, private modalService: DialogService, private _perms_service: PermissionService){

	}

	ngOnInit() {

		let permission = this._perms_service.getPermissions()
			.subscribe(data => {
				this.ad_perms = Array.from(data);
            	this.rerender();
			},
			err => this.catchError(err)
		);
		//add the the body classes
	    this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

		
	}

	clickRow(id) {
	    this._perms_service.setId(id);
	    this.openEditModal();
	}


	private catchError(error: any){
	    let response_body = error._body;
	    let response_status = error.status;

	    if( response_status == 500 ){
	      this.error_title = 'Error 500';
	      this.error_message = 'Something went wrong';
	    } else if( response_status == 200 ) {
	      this.error_title = '';
	      this.error_message = '';
	    }
	}

	openEditModal() {
		let disposable = this.modalService.addDialog(PermissionEditComponent, {
            title:'Update Permission'
        	}).subscribe((isConfirmed)=>{
        		let permission = this._perms_service.getPermissions()
					.subscribe(data => {
						this.ad_perms = Array.from(data);
		            	this.rerender();
					},
					err => this.catchError(err)
				);
        });
	}

	openAddModal() {
		let disposable = this.modalService.addDialog(PermissionCreateComponent, {
            title:'Add Permission'
        	}).subscribe((isConfirmed)=>{
        		let permission = this._perms_service.getPermissions()
					.subscribe(data => {
						this.ad_perms = Array.from(data);
		            	this.rerender();
					},
					err => this.catchError(err)
				);
        });
    }

	ngOnDestroy() {
	    //remove the the body classes
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	 }

	 ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      // Destroy the table first
	      dtInstance.destroy();
	      // Call the dtTrigger to rerender again
	      this.dtTrigger.next();
	    });
    }
}
