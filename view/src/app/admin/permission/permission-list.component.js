var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';
import { Permission } from '../../model/permission';
import { PermissionCreateComponent } from './permission-create.component';
import { PermissionEditComponent } from './permission-edit.component';
import { PermissionService } from '../../services/permission.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
var PermissionListComponent = /** @class */ (function () {
    function PermissionListComponent(_conf, modalService, _perms_service) {
        this._conf = _conf;
        this.modalService = modalService;
        this._perms_service = _perms_service;
        this.dtTrigger = new Subject();
        this.permission = new Permission();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
    }
    PermissionListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var permission = this._perms_service.getPermissions()
            .subscribe(function (data) {
            _this.ad_perms = Array.from(data);
            _this.rerender();
        }, function (err) { return _this.catchError(err); });
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    };
    PermissionListComponent.prototype.clickRow = function (id) {
        this._perms_service.setId(id);
        this.openEditModal();
    };
    PermissionListComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    PermissionListComponent.prototype.openEditModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(PermissionEditComponent, {
            title: 'Update Permission'
        }).subscribe(function (isConfirmed) {
            var permission = _this._perms_service.getPermissions()
                .subscribe(function (data) {
                _this.ad_perms = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    PermissionListComponent.prototype.openAddModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(PermissionCreateComponent, {
            title: 'Add Permission'
        }).subscribe(function (isConfirmed) {
            var permission = _this._perms_service.getPermissions()
                .subscribe(function (data) {
                _this.ad_perms = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    PermissionListComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    PermissionListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    PermissionListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], PermissionListComponent.prototype, "dtElement", void 0);
    PermissionListComponent = __decorate([
        Component({
            selector: 'admin-permissions-list',
            templateUrl: './permission-list.component.html',
            styleUrls: ['./permission-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration, DialogService, PermissionService])
    ], PermissionListComponent);
    return PermissionListComponent;
}());
export { PermissionListComponent };
//# sourceMappingURL=permission-list.component.js.map