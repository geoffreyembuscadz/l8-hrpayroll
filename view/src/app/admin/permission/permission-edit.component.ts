import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';

import { Permission } from '../../model/permission';
import { PermissionService } from '../../services/permission.service';
import { MenuService } from '../../services/menu.service';



@Component({
  selector: 'admin-permission-edit',
  templateUrl: './permission-edit.component.html'
})
export class PermissionEditComponent extends DialogComponent<null, boolean> {
  
  title: string;
  message: string;

  public id: any;
  public name: string;
  public display_name: string;
  public description: string;
  private error_title: string;
  private error_message: string;
  public parent_id: string;

  public success_title;
  public success_message;
  private headers: Headers;
  public poststore: any;
  public permission = new Permission();

  menu_rec: any;

  updatePermissionForm : FormGroup;

  constructor(private _menu_service: MenuService, private _rt: Router, private _perms_service: PermissionService, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute) {
    super(dialogService);
   
    this.id = this._perms_service.getId();
    this._perms_service.getPermission(this.id).subscribe(
      data => {
        this.setRole(data);
      },
      err => console.error(err)
    );

    this.updatePermissionForm = _fb.group({

      'name': [this.permission.name, [Validators.required]],
      'display_name': [this.permission.display_name, [Validators.required]],
      'description': [this.permission.description, [Validators.required]],
      'parent_id': [this.permission.parent_id, [Validators.required]]
    });  
  }
 
  public validateUpdate() {
    let role_model = this.updatePermissionForm.value;

    this._perms_service.updatePermission(this.id, role_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The permission is successfully updated.";
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/permission-list']);
        this.clearErrorMsg();
      },
      err => this.catchError(err)
    );
  }

  archivePermission(event: Event){
    event.preventDefault();
    let role_id = this.id;
    this._perms_service.archivePermission(role_id).subscribe(
      data => {
      
        alert('Record is successfully archived.');
        this.close();
        this._rt.navigateByUrl('admin/permission-list');
        
      },
      err => console.error(err)
    );

  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'Permission name already exist.';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }
  }

  public setRole(permission: any){
    this.id = permission.id;
    this.permission = permission;
  }

  private clearErrorMsg(){
    this.error_title = '';
    this.error_message = '';
  }


  ngOnInit() {
    let menu = this._menu_service.getMenus()
         .subscribe(
            data => {
               this.menu_rec = Array.from(data);
            },
            err => this.catchError(err)
         );
  }

}