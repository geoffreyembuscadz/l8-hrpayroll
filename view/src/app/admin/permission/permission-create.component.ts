import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { Observable } from "rxjs/Rx";

import { Permission } from '../../model/permission';
import { PermissionService } from '../../services/permission.service';
import { MenuService } from '../../services/menu.service';

@Component({
  selector: 'admin-permission-create',
  templateUrl: './permission-create.component.html',
  styleUrls: ['./permission-create.component.css']
})
export class PermissionCreateComponent extends DialogComponent<null, boolean> {

  menu_rec: any;
  public error_title: string;
  public error_message: string;
  private success_title: string;
  private success_message: string;
  public poststore: any;
  public perms = new Permission();

  error = false;
  elem: any;
  errorMessage = '';
  createPermissionForm : FormGroup;

  constructor(private _rt: Router,dialogService: DialogService, private _fb: FormBuilder, private _perms_service: PermissionService,  private _menu_service: MenuService) { 
  	super(dialogService);

  	this.createPermissionForm = _fb.group({
	    'name': ['', [Validators.required]],
	    'display_name': ['', [Validators.required]],
	    'description': [''],
	    'parent_id' : ['', [Validators.required]]
    });
  }

  onSubmit() {

	let  role_model = this.createPermissionForm.value;
  console.log(role_model);
	this._perms_service
	.storePermission(role_model)
	.subscribe(
		data => {
			this.poststore = Array.from(data);
			this.success_title = "Success";
      this.success_message = "A new user record was successfully added.";
      
		  setTimeout(() => {
             this.close();
            }, 2000);
      this._rt.navigate(['/admin/permission-list']);

    },
		err => this.catchError(err)
	);
  }

  private catchError(error: any){
  	let response_body = error._body;
  	let response_status = error.status;

  	if( response_status == 500 ){
  		this.error_title = 'Error 500';
  		this.error_message = 'Permission name/display name already exist.';
  	} else if( response_status == 200 ) {
  		this.error_title = '';
  		this.error_message = '';
  	}
  }

  ngOnInit() {
     let menu = this._menu_service.getMenus()
         .subscribe(
            data => {
               this.menu_rec = Array.from(data);
            },
            err => this.catchError(err)
         );


  }

}
