var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Permission } from '../../model/permission';
import { PermissionService } from '../../services/permission.service';
import { MenuService } from '../../services/menu.service';
var PermissionEditComponent = /** @class */ (function (_super) {
    __extends(PermissionEditComponent, _super);
    function PermissionEditComponent(_menu_service, _rt, _perms_service, dialogService, _fb, _ar) {
        var _this = _super.call(this, dialogService) || this;
        _this._menu_service = _menu_service;
        _this._rt = _rt;
        _this._perms_service = _perms_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.permission = new Permission();
        _this.id = _this._perms_service.getId();
        _this._perms_service.getPermission(_this.id).subscribe(function (data) {
            _this.setRole(data);
        }, function (err) { return console.error(err); });
        _this.updatePermissionForm = _fb.group({
            'name': [_this.permission.name, [Validators.required]],
            'display_name': [_this.permission.display_name, [Validators.required]],
            'description': [_this.permission.description, [Validators.required]],
            'parent_id': [_this.permission.parent_id, [Validators.required]]
        });
        return _this;
    }
    PermissionEditComponent.prototype.validateUpdate = function () {
        var _this = this;
        var role_model = this.updatePermissionForm.value;
        this._perms_service.updatePermission(this.id, role_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The permission is successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/permission-list']);
            _this.clearErrorMsg();
        }, function (err) { return _this.catchError(err); });
    };
    PermissionEditComponent.prototype.archivePermission = function (event) {
        var _this = this;
        event.preventDefault();
        var role_id = this.id;
        this._perms_service.archivePermission(role_id).subscribe(function (data) {
            alert('Record is successfully archived.');
            _this.close();
            _this._rt.navigateByUrl('admin/permission-list');
        }, function (err) { return console.error(err); });
    };
    PermissionEditComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Permission name already exist.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    PermissionEditComponent.prototype.setRole = function (permission) {
        this.id = permission.id;
        this.permission = permission;
    };
    PermissionEditComponent.prototype.clearErrorMsg = function () {
        this.error_title = '';
        this.error_message = '';
    };
    PermissionEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        var menu = this._menu_service.getMenus()
            .subscribe(function (data) {
            _this.menu_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
    };
    PermissionEditComponent = __decorate([
        Component({
            selector: 'admin-permission-edit',
            templateUrl: './permission-edit.component.html'
        }),
        __metadata("design:paramtypes", [MenuService, Router, PermissionService, DialogService, FormBuilder, ActivatedRoute])
    ], PermissionEditComponent);
    return PermissionEditComponent;
}(DialogComponent));
export { PermissionEditComponent };
//# sourceMappingURL=permission-edit.component.js.map