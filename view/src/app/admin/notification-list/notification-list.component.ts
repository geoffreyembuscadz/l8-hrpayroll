import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { Configuration } from '../../app.config';
import { Observable, Subscription } from "rxjs";
import * as io from "socket.io-client";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import { NotificationService } from '../../services/notification.service';
import { AuthUserService } from '../../services/auth-user.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { DaterangepickerConfig } from 'ng2-daterangepicker';


@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.css']
})
export class NotificationListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	dtOptions: any = {};

  	user_id:any;
  	notif:any;

  	filterForm:any;
  	markAllVal = false;

  	public mainInput = {
        start: moment().subtract(12, 'month'),
        end: moment().subtract(11, 'month')
    }

    socket: SocketIOClient.Socket;
    host:any;

  	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

  constructor(
		private _auth_service: AuthUserService,
		private _notif_service: NotificationService, 
		private _rt: Router,
		private _fb: FormBuilder,
	   	private _conf: Configuration,
	   	private daterangepickerOptions: DaterangepickerConfig
	){

	  	this.host = this._conf.socketUrl;

		this.filterForm = _fb.group({
			'user_id': 			[null],
			'start_date': 		[null],
			'end_date': 		[null]
		});

		this.dtOptions = {
	      searching: false,
	      lengthChange: false,
	      pageLength: 10,
	      // paging: false,
	      ordering: false,
	      info: false,
	      lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
	      // dom: '<"top"flpi>rt<"bottom">',
	      // pagingType: "numbers",
	      processing: true,
	      language: {
		        paginate: {
		            previous: '‹‹',
		            next:     '››'
		        },
		        aria: {
		            paginate: {
		                previous: 'Previous',
		                next:     'Next'
		            }
		        }
		    }
	    };

	    this.daterangepickerOptions.settings = {
            singleDatePicker: false,
		}; 

		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	}

  	ngOnInit() {
		this.getData();
		this.connectToServer();
	}

	getData(){
		let model = this.filterForm.value;
		this._notif_service.getAllNotification(model).
		subscribe(
			data => {
				this.notif = data;
				this.markAllVal = true;
				this.rerender();
			},
			err => console.error(err)
		);
	}

	viewNotif(details){

		let route = details.route;
		this._rt.navigateByUrl('admin/'+route);

		if(details.seen != 1){

			this._notif_service.updateNotif(details)
			.subscribe(
				data => {
					let notif = data;
				}
			);
		}
	}

	filterDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;

        let start_date = moment(dateInput.start).format("YYYY-MM-DD");
        let end_date = moment(dateInput.end).format("YYYY-MM-DD");

        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;

        this.getData();

    }

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    markAll(){
    this._notif_service.markAllAsRead()
      .subscribe(
        data => {
          let notif = data;
          this.getData();
        }
      );
  	}

  	connectToServer(){
	    let socketUrl = this.host;
	    this.socket = io.connect(socketUrl);
	    this.socket.on("connect", () => this.connect());
	    this.socket.on("disconnect", () => this.disconnect());
	    this.socket.on("notification_list", (data:any) => this.getData());
	    
	    this.socket.on("error", (error: string) => {
	      console.log(`ERROR: "${error}" (${socketUrl})`);
	    });
	  }

	// Handle connection opening
	private connect() {
	    // Request initial list when connected
	    this.socket.emit("join", 'Client User -> ');
	}
	// Handle connection closing
	private disconnect() {
	    console.log(`Disconnected from "${this.host}"`);
	}

}
