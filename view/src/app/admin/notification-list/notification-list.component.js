var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Configuration } from '../../app.config';
import * as io from "socket.io-client";
import { FormBuilder } from '@angular/forms';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import { NotificationService } from '../../services/notification.service';
import { AuthUserService } from '../../services/auth-user.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import * as moment from 'moment';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
var NotificationListComponent = /** @class */ (function () {
    function NotificationListComponent(_auth_service, _notif_service, _rt, _fb, _conf, daterangepickerOptions) {
        this._auth_service = _auth_service;
        this._notif_service = _notif_service;
        this._rt = _rt;
        this._fb = _fb;
        this._conf = _conf;
        this.daterangepickerOptions = daterangepickerOptions;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.markAllVal = false;
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.host = this._conf.socketUrl;
        this.filterForm = _fb.group({
            'user_id': [null],
            'start_date': [null],
            'end_date': [null]
        });
        this.dtOptions = {
            searching: false,
            lengthChange: false,
            pageLength: 10,
            // paging: false,
            ordering: false,
            info: false,
            lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            // dom: '<"top"flpi>rt<"bottom">',
            // pagingType: "numbers",
            processing: true,
            language: {
                paginate: {
                    previous: '‹‹',
                    next: '››'
                },
                aria: {
                    paginate: {
                        previous: 'Previous',
                        next: 'Next'
                    }
                }
            }
        };
        this.daterangepickerOptions.settings = {
            singleDatePicker: false,
        };
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    NotificationListComponent.prototype.ngOnInit = function () {
        this.getData();
        this.connectToServer();
    };
    NotificationListComponent.prototype.getData = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._notif_service.getAllNotification(model).
            subscribe(function (data) {
            _this.notif = data;
            _this.markAllVal = true;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    NotificationListComponent.prototype.viewNotif = function (details) {
        var route = details.route;
        this._rt.navigateByUrl('admin/' + route);
        if (details.seen != 1) {
            this._notif_service.updateNotif(details)
                .subscribe(function (data) {
                var notif = data;
            });
        }
    };
    NotificationListComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.getData();
    };
    NotificationListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    NotificationListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    NotificationListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    NotificationListComponent.prototype.markAll = function () {
        var _this = this;
        this._notif_service.markAllAsRead()
            .subscribe(function (data) {
            var notif = data;
            _this.getData();
        });
    };
    NotificationListComponent.prototype.connectToServer = function () {
        var _this = this;
        var socketUrl = this.host;
        this.socket = io.connect(socketUrl);
        this.socket.on("connect", function () { return _this.connect(); });
        this.socket.on("disconnect", function () { return _this.disconnect(); });
        this.socket.on("notification_list", function (data) { return _this.getData(); });
        this.socket.on("error", function (error) {
            console.log("ERROR: \"" + error + "\" (" + socketUrl + ")");
        });
    };
    // Handle connection opening
    NotificationListComponent.prototype.connect = function () {
        // Request initial list when connected
        this.socket.emit("join", 'Client User -> ');
    };
    // Handle connection closing
    NotificationListComponent.prototype.disconnect = function () {
        console.log("Disconnected from \"" + this.host + "\"");
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], NotificationListComponent.prototype, "dtElement", void 0);
    NotificationListComponent = __decorate([
        Component({
            selector: 'app-notification-list',
            templateUrl: './notification-list.component.html',
            styleUrls: ['./notification-list.component.css']
        }),
        __metadata("design:paramtypes", [AuthUserService,
            NotificationService,
            Router,
            FormBuilder,
            Configuration,
            DaterangepickerConfig])
    ], NotificationListComponent);
    return NotificationListComponent;
}());
export { NotificationListComponent };
//# sourceMappingURL=notification-list.component.js.map