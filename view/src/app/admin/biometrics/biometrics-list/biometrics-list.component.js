var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { BiometricsService } from '../../../services/biometrics.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { BiometricsModalComponent } from '../../biometrics/biometrics-modal/biometrics-modal.component';
var BiometricsListComponent = /** @class */ (function () {
    function BiometricsListComponent(dialogService, modalService, bio_service) {
        this.modalService = modalService;
        this.bio_service = bio_service;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    BiometricsListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    BiometricsListComponent.prototype.ngOnInit = function () {
        this.data();
    };
    BiometricsListComponent.prototype.data = function () {
        var _this = this;
        this.bio_service.getBiometrics().
            subscribe(function (data) {
            _this.bio = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    BiometricsListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    BiometricsListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    BiometricsListComponent.prototype.archive = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'biometrics'
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    BiometricsListComponent.prototype.edit = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(BiometricsModalComponent, {
            title: 'Update Biometrics',
            button: 'Update',
            edit: true,
            id: id
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    BiometricsListComponent.prototype.add = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(BiometricsModalComponent, {
            title: 'Create Biometrics',
            button: 'Create',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.data();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], BiometricsListComponent.prototype, "dtElement", void 0);
    BiometricsListComponent = __decorate([
        Component({
            selector: 'app-biometrics-list',
            templateUrl: './biometrics-list.component.html',
            styleUrls: ['./biometrics-list.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            DialogService,
            BiometricsService])
    ], BiometricsListComponent);
    return BiometricsListComponent;
}());
export { BiometricsListComponent };
//# sourceMappingURL=biometrics-list.component.js.map