import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { BiometricsService } from '../../../services/biometrics.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { BiometricsModalComponent } from '../../biometrics/biometrics-modal/biometrics-modal.component';



@Component({
  selector: 'app-biometrics-list',
  templateUrl: './biometrics-list.component.html',
  styleUrls: ['./biometrics-list.component.css']
})
export class BiometricsListComponent implements OnInit, AfterViewInit {
	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};

	bio:any;

	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	constructor(
  		dialogService: DialogService, 
  		private modalService: DialogService,
  		private bio_service: BiometricsService
  		){
		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");


	}

	ngAfterViewInit(){
		this.dtTrigger.next();
	}

	ngOnInit() {
		this.data();
	}
	data(){

    	this.bio_service.getBiometrics().
	      	subscribe(
	        data => {
	          this.bio= Array.from(data);
	          this.rerender();
	        },
	        err => console.error(err)
	     );
	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    archive(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'biometrics'
        	}).subscribe((isConfirmed)=>{
		       	this.data();
        });
	}

	edit(id) {
		let disposable = this.modalService.addDialog(BiometricsModalComponent, {
            title:'Update Biometrics',
            button:'Update',
		    edit:true,
		    id:id
        	}).subscribe((isConfirmed)=>{
                this.data();
        });
	}

	add() {
		let disposable = this.modalService.addDialog(BiometricsModalComponent, {
            title:'Create Biometrics',
            button:'Create',
		    create:true
        	}).subscribe((isConfirmed)=>{
                this.data();
        });
	}

}
