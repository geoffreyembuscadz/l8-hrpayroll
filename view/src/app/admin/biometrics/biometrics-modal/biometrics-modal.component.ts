import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { BiometricsService } from '../../../services/biometrics.service';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-biometrics-modal',
  templateUrl: './biometrics-modal.component.html',
  styleUrls: ['./biometrics-modal.component.css']
})
export class BiometricsModalComponent extends DialogComponent<null, boolean> {

    public success_title;
    public success_message;
    public error_title: string
    public error_message: string;

    Form : FormGroup;

    id:any;
    name:any;
    ip_address:any;
    branch_id:any;
    postore:any;

    edit = false;
    create = false;

    branch=[];
	branch_current : any;
	branch_value: any;

	public company: any;
	public company_current : any;
	company_value:any;
	company_id:any;

	constructor(
	  dialogService: DialogService, 
	  private _fb: FormBuilder, 
	  private bio_service: BiometricsService,  
	  private _common_service: CommonService,
	  private modalService: DialogService) {
		super(dialogService);

	  this.Form = _fb.group({
	     'name':        [null, [Validators.required]],
	     'ip_address':	[null, [Validators.required]],
	     'branch_id':	[null]
	  });  
	}

  	ngOnInit() {
  		if (this.edit) {
			this.getdata();
  		}
  		this.getCompany();
	}


	getdata(){
		let id = this.id;
    	this.bio_service.getBiometricsById(id).
	      	subscribe(
	        data => {
	          this.assign(data);
	        },
	        err => console.error(err)
	     );
	}

	assign(d){
		this.name = d.name;
		this.ip_address = d.ip_address;
		this.branch_id = d.branch_id;
		this.company_id = d.company_id;
	}

	getBranch(){
	      let id = this.company_current;
	      this._common_service.getBranchByCompany(id)
	      .subscribe(
	        data => {
	        this.branch = Array.from(data);
	        
	        this.branch_value = [];

	        if (this.edit) {
	        	this.branch_value = this.branch_id;
	        }

	        if (this.create) {
		        let id = 0;
	        	let text = 'Select Branch';
	        	this.branch.unshift({id,text});
	        }

	        this.branch_current = this.branch_value;
	        
	        },
	        err => console.error(err)
	    );
	}
	changedBranch(datas: any,i:any,x:any) {

		if(datas.value != 0){
			this.branch_current = datas.value;
		}
	}

	onSubmit(){

		if (this.edit) {

			this.Form.value.branch_id = this.branch_current;
			let model = this.Form.value;
			let id = this.id;

	    	this.bio_service.updateBiometrics(id,model).
		      	subscribe(
		        data => {
		          this.postore = data;
		          this.success_title = "Success!";
	            	this.success_message = "Successfully updated";

	            	setTimeout(() => {
	            	this.close();
	            	}, 1000);

		        },
		        err => console.error(err)
		     );
		}

		if (this.create) {

			this.Form.value.branch_id = this.branch_current;
			let model = this.Form.value;

	    	this.bio_service.createBiometrics(model).
		      	subscribe(
		        data => {
		          this.postore = data;
		          this.success_title = "Success!";
	            	this.success_message = "Successfully created";

	            	setTimeout(() => {
	            	this.close();
	            	}, 1000);

		        },
		        err => console.error(err)
		     );
		}
	}

	getCompany(){
	      this._common_service.getCompany()
	      .subscribe(
	        data => {
	        this.company = Array.from(data);
	        this.company_value = [];

	        if (this.edit) {
	        	this.company_value = this.company_id;
	        	this.company_current = this.company_id;
	        	this.getBranch();
	        }

	        if (this.create) {
		        let id = 0;
	        	let text = 'Select Company';
	        	this.company.unshift({id,text});
	        }

	        this.company_current = this.company_value;
	        
	        },
	        err => console.error(err)
	    );
	}
  	changedCompany(data: any) {

      this.company_current = data.value;
      let len = this.company_current.length;

      if(len >= 1 && this.company_current != 0){
      	this.getBranch();
      }
    }

}
