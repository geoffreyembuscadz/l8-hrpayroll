var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { BiometricsService } from '../../../services/biometrics.service';
import { CommonService } from '../../../services/common.service';
var BiometricsModalComponent = /** @class */ (function (_super) {
    __extends(BiometricsModalComponent, _super);
    function BiometricsModalComponent(dialogService, _fb, bio_service, _common_service, modalService) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this.bio_service = bio_service;
        _this._common_service = _common_service;
        _this.modalService = modalService;
        _this.edit = false;
        _this.create = false;
        _this.branch = [];
        _this.Form = _fb.group({
            'name': [null, [Validators.required]],
            'ip_address': [null, [Validators.required]],
            'branch_id': [null]
        });
        return _this;
    }
    BiometricsModalComponent.prototype.ngOnInit = function () {
        if (this.edit) {
            this.getdata();
        }
        this.getCompany();
    };
    BiometricsModalComponent.prototype.getdata = function () {
        var _this = this;
        var id = this.id;
        this.bio_service.getBiometricsById(id).
            subscribe(function (data) {
            _this.assign(data);
        }, function (err) { return console.error(err); });
    };
    BiometricsModalComponent.prototype.assign = function (d) {
        this.name = d.name;
        this.ip_address = d.ip_address;
        this.branch_id = d.branch_id;
        this.company_id = d.company_id;
    };
    BiometricsModalComponent.prototype.getBranch = function () {
        var _this = this;
        var id = this.company_current;
        this._common_service.getBranchByCompany(id)
            .subscribe(function (data) {
            _this.branch = Array.from(data);
            _this.branch_value = [];
            if (_this.edit) {
                _this.branch_value = _this.branch_id;
            }
            if (_this.create) {
                var id_1 = 0;
                var text = 'Select Branch';
                _this.branch.unshift({ id: id_1, text: text });
            }
            _this.branch_current = _this.branch_value;
        }, function (err) { return console.error(err); });
    };
    BiometricsModalComponent.prototype.changedBranch = function (datas, i, x) {
        if (datas.value != 0) {
            this.branch_current = datas.value;
        }
    };
    BiometricsModalComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.edit) {
            this.Form.value.branch_id = this.branch_current;
            var model = this.Form.value;
            var id = this.id;
            this.bio_service.updateBiometrics(id, model).
                subscribe(function (data) {
                _this.postore = data;
                _this.success_title = "Success!";
                _this.success_message = "Successfully updated";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return console.error(err); });
        }
        if (this.create) {
            this.Form.value.branch_id = this.branch_current;
            var model = this.Form.value;
            this.bio_service.createBiometrics(model).
                subscribe(function (data) {
                _this.postore = data;
                _this.success_title = "Success!";
                _this.success_message = "Successfully created";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return console.error(err); });
        }
    };
    BiometricsModalComponent.prototype.getCompany = function () {
        var _this = this;
        this._common_service.getCompany()
            .subscribe(function (data) {
            _this.company = Array.from(data);
            _this.company_value = [];
            if (_this.edit) {
                _this.company_value = _this.company_id;
                _this.company_current = _this.company_id;
                _this.getBranch();
            }
            if (_this.create) {
                var id = 0;
                var text = 'Select Company';
                _this.company.unshift({ id: id, text: text });
            }
            _this.company_current = _this.company_value;
        }, function (err) { return console.error(err); });
    };
    BiometricsModalComponent.prototype.changedCompany = function (data) {
        this.company_current = data.value;
        var len = this.company_current.length;
        if (len >= 1 && this.company_current != 0) {
            this.getBranch();
        }
    };
    BiometricsModalComponent = __decorate([
        Component({
            selector: 'app-biometrics-modal',
            templateUrl: './biometrics-modal.component.html',
            styleUrls: ['./biometrics-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            BiometricsService,
            CommonService,
            DialogService])
    ], BiometricsModalComponent);
    return BiometricsModalComponent;
}(DialogComponent));
export { BiometricsModalComponent };
//# sourceMappingURL=biometrics-modal.component.js.map