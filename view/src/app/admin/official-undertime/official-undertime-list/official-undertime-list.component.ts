import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { UndertimeModalComponent } from '../../official-undertime/undertime-modal/undertime-modal.component';
import { OfficialUndertimeService } from '../../../services/official-undertime.service';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
import { ActionCenterModalComponent } from '../../action-center/action-center-modal/action-center-modal.component';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Select2OptionData } from 'ng2-select2';
import { AuthUserService } from '../../../services/auth-user.service';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';

@Component({
  selector: 'app-official-undertime-list',
  templateUrl: './official-undertime-list.component.html',
  styleUrls: ['./official-undertime-list.component.css']
})
export class OfficialUndertimeListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

 	dtOptions: any = {};
	ou=[];

	byStatus=false;
	byDate=false;
	byCompany=false;
	byEmployee=false;

	public company: any;
	public company_current : any;
	company_value: Array<Select2OptionData>;
	value:any;

	public mainInput = {
        start: moment().subtract(12, 'month'),
        end: moment().subtract(11, 'month')
    }
    filterForm:any;

    public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public employee_current: any;
	public options: Select2Options;
	emp:any;

	public status : Array<Select2OptionData>;
	public status_value: Array<Select2OptionData>;
	public status_current: any;
	sta:any;

	user_id:any;
	supervisor_id:any;
	role_id:any;
	employee_id:any;

	btnCreate = false;
	btnEdit = false;
	btnApproved = false;
	btnAction = false;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _common_service: CommonService,
   		private _OUservice: OfficialUndertimeService,
   		private daterangepickerOptions: DaterangepickerConfig,
   		private _fb: FormBuilder,
   		private _auth_service: AuthUserService
		){

		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	    this.filterForm = _fb.group({
	    	'status_id': 		[null],
			'start_date': 		[null],
			'end_date': 		[null],
			'company_id': 		[null],
			'employee_id': 		[null]
    	});
	}


	ngOnInit() {
		this.dateOption();
		this.getEmployee();
		this.getPermissionId();
	}

	getPermissionId(){
		this._auth_service.getPermissionId().
		subscribe(
			data => {
				this.permissionValidation(data);
			},
			err => console.error(err)
		);

	}

	permissionValidation(user){
		let len = user.length;
		for (let i = 0; i < len; ++i) {
			if(user[i] == "167"){
				this.btnCreate = true;
			}
			else if(user[i] == "168"){
				this.btnEdit = true;
			}
			else if(user[i] == "169"){
				this.btnApproved = true;
			}
		}

		if(this.btnEdit == false && this.btnApproved == false){
			this.btnAction = true;
		}

		this.data();
	}

	dateOption(){
    	this.daterangepickerOptions.settings = {
        	singleDatePicker: false
		};
    }
	
	data(){

		let model = this.filterForm.value;
    	
    	this._OUservice.getOUList(model).
	      	subscribe(
	        data => {
	          this.ou = Array.from(data);
	          this.rerender();
	        },
	        err => console.error(err)
	    );

	}

	getEmployee(){

		this._common_service.getEmployee().
		subscribe(
			data => {
				this.emp = Array.from(data); // fetched record
				this.employee = this.emp;
				this.employee_value = [];
				this.options = {
					multiple: true
				}
				this.employee_current = this.employee_value;
			},
			err => console.error(err)
		);
	}
	changedEmployee(data: any) {
		this.employee_current = data.value;

		if(this.employee_current == 0){
			this.filterForm.value.employee_id = null;
		}
		else{
			this.filterForm.value.employee_id = this.employee_current;
		}
			this.data();
	}

	getStatus(){
		this._common_service.getStatusType().
		subscribe(
			data => {
				this.sta = Array.from(data); // fetched record
				this.status = this.sta;
				this.status_value = [];
				this.options = {
					multiple: true
				}
				this.status_current = this.status_value;
			},
			err => console.error(err)
		);
	}
	changedStatus(data: any) {
		this.status_current = data.value;

		if(this.status_current == 0){
			this.filterForm.value.status_id = null;
		}
		else{
			this.filterForm.value.status_id = this.status_current;
		}
			this.data();
	}

     getCompany(){
	      this._common_service.getCompany()
	      .subscribe(
	        data => {
	        this.company = Array.from(data);
	        this.company_value = [];
	        this.options = {
				multiple: true
			}
	        this.company_current = this.company_value;

	        let id = 0;
	        let text = 'Select Company';

	        this.company.unshift({id,text});

	        this.company_value = this.value;
	        
	        },
	        err => console.error(err)
	    );
	}
	changedCompany(data: any) {

		this.company_current = data.value;

		if(this.company_current == 0){
			this.filterForm.value.company_id = null;
		}
		else{
			this.filterForm.value.company_id = this.company_current;
		}
			this.data();

	} 

	choiceFilter(id){

		if(id==1){
			this.getStatus();
			this.getCompany();
			this.getEmployee();
			this.byStatus=true;
			this.byDate=true;
			this.byCompany=true;
			this.byEmployee=true;
		}
		else if(id==2){
			if (this.byStatus==false) { 
				this.getStatus();
				this.byStatus=true;
			} else {
				this.byStatus=false;
				this.filterForm.value.status_id = null;
			}
		}
		else if(id==3){
			if (this.byDate==false) { 
				this.byDate=true;
			} else {
				this.byDate=false;
				this.filterForm.value.start_date = null;
        		this.filterForm.value.end_date = null;
			}
		}
		else if(id==4){
			if (this.byCompany==false) { 
				this.getCompany();
				this.byCompany=true;
			} else {
				this.byCompany=false;
				this.filterForm.value.company_id = null;
			}
		}
		else if(id==5){
			if (this.byEmployee==false) { 
				this.getEmployee();
				this.byEmployee=true;
			} else {
				this.byEmployee=false;
				this.filterForm.value.employee_id = null;
			}
		}
		else{
			this.byStatus = false;
			this.byDate = false;
			this.byCompany=false;
			this.byEmployee=false;

			this.filterForm.value.status_id = null;
        	this.filterForm.value.company_id = null;
        	this.filterForm.value.start_date = null;
        	this.filterForm.value.end_date = null;
        	this.filterForm.value.employee_id = null;
        	this.data();
		}

	}

	filterDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;

        let start_date = moment(dateInput.start).format("YYYY-MM-DD");
        let end_date = moment(dateInput.end).format("YYYY-MM-DD");

        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;

        this.data();

    }

	approvedOU(id:any) {

		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Approved Undertime',
            message:'Are you sure you want to Approved this Undertime?',
            action:'Approved',
            id:id,
            url:'official_undertime',
            request:true
        	}).subscribe((isConfirmed)=>{
                this.dateOption();
                this.data();

            });
	}

	rejectedOU(id:any) {

		let disposable = this.modalService.addDialog(RemarksModalComponent, {
            title:'Reject Undertime',
            id:id,
            url:'official_undertime',
            button:'Reject'
        	}).subscribe((isConfirmed)=>{
                this.dateOption();
                this.data();

            });
	}

	createOU() {
		let disposable = this.modalService.addDialog(UndertimeModalComponent, {
            title:'Create Undertime',
            button:'Add',
            create:true,
            multi:true,
            admin:true,
            emp:this.emp
        	}).subscribe((isConfirmed)=>{
                this.dateOption();
                this.data();
            });
	}

	viewDetails(id:any,status:any) {
		let buttons = true;
		if (status == 'REJECTED' || status == 'APPROVED' || status == 'CANCELLED') {
			buttons = false;
		}
		
		let disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title:'Undertime',
            id:id,
            url:'official_undertime',
            start_end_time:true,
            buttons:buttons,
            created_by:this.user_id,
			messenger_id:this.employee_id
        	}).subscribe((isConfirmed)=>{
                this.dateOption();
                this.data();

            });
	}

	editOU(id,emp_id){

		let v = [];
		v.push(emp_id);
		let employee = v;
		

		let disposable = this.modalService.addDialog(UndertimeModalComponent, {
            title:'Edit Undertime',
            button:'Update',
            edit:true,
            admin:true,
            ou_id:id,
            single:true,
            emp:this.emp,
            employee_id:employee
        	}).subscribe((isConfirmed)=>{
                this.dateOption();
                this.data();
        });

	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

}
