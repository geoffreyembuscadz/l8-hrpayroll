var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import 'rxjs/add/operator/map';
import { OfficialUndertimeService } from '../../../services/official-undertime.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { DataTableDirective } from 'angular-datatables';
import { Configuration } from '../../../app.config';
import { Subject } from 'rxjs/Rx';
import { UndertimeModalComponent } from '../../official-undertime/undertime-modal/undertime-modal.component';
import { RemarksModalComponent } from '../../remarks-modal/remarks-modal.component';
import { ActionCenterModalComponent } from '../../action-center/action-center-modal/action-center-modal.component';
import { FormBuilder } from '@angular/forms';
import { CommonService } from '../../../services/common.service';
import * as moment from 'moment';
var MyUndertimeListComponent = /** @class */ (function () {
    function MyUndertimeListComponent(modalService, _conf, _auth_service, _OUservice, _common_service, _fb, daterangepickerOptions) {
        this.modalService = modalService;
        this._conf = _conf;
        this._auth_service = _auth_service;
        this._OUservice = _OUservice;
        this._common_service = _common_service;
        this._fb = _fb;
        this.daterangepickerOptions = daterangepickerOptions;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.ou = [];
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.buttonVal = false;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filterForm = _fb.group({
            'status_id': [null],
            'start_date': [null],
            'end_date': [null]
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    MyUndertimeListComponent.prototype.ngOnInit = function () {
        this.dateOption();
        this.getListByIdLimited();
        this.getStatus();
        this.getData();
    };
    MyUndertimeListComponent.prototype.getListByIdLimited = function () {
        var _this = this;
        this._auth_service.getListByIdLimited().
            subscribe(function (data) {
            var user = data;
            _this.employee_id = user.employee_id;
            _this.buttonVal = true;
        }, function (err) { return console.error(err); });
    };
    MyUndertimeListComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    MyUndertimeListComponent.prototype.getData = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._OUservice.getMyOU(model).
            subscribe(function (data) {
            _this.ou = data;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    MyUndertimeListComponent.prototype.createOU = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(UndertimeModalComponent, {
            title: 'Create Undertime',
            button: 'Add',
            create: true,
            employee_id: this.employee_id,
            single: false,
            user: true
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyUndertimeListComponent.prototype.editOU = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(UndertimeModalComponent, {
            title: 'Edit Undertime',
            button: 'Update',
            edit: true,
            ou_id: id,
            user: true,
            employee_id: this.employee_id
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyUndertimeListComponent.prototype.cancelOU = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(RemarksModalComponent, {
            title: 'Cancel Undertime',
            id: id,
            url: 'official_undertime',
            button: 'Ok'
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyUndertimeListComponent.prototype.viewDetails = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title: 'Undertime',
            id: id,
            url: 'official_undertime',
            start_end_time: true,
            user: true
        }).subscribe(function (isConfirmed) {
            _this.getData();
            _this.dateOption();
        });
    };
    MyUndertimeListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    MyUndertimeListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    MyUndertimeListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    MyUndertimeListComponent.prototype.getStatus = function () {
        var _this = this;
        this._common_service.getStatusType().
            subscribe(function (data) {
            _this.sta = Array.from(data);
            _this.status = _this.sta;
            _this.status_value = [];
            _this.options = {
                multiple: true
            };
            _this.status_current = _this.status_value;
        }, function (err) { return console.error(err); });
    };
    MyUndertimeListComponent.prototype.changedStatus = function (data) {
        this.status_current = data.value;
        if (this.status_current == 0) {
            this.filterForm.value.status_id = null;
        }
        else {
            this.filterForm.value.status_id = this.status_current;
        }
        this.getData();
    };
    MyUndertimeListComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.getData();
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], MyUndertimeListComponent.prototype, "dtElement", void 0);
    MyUndertimeListComponent = __decorate([
        Component({
            selector: 'app-my-undertime-list',
            templateUrl: './my-undertime-list.component.html',
            styleUrls: ['./my-undertime-list.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            Configuration,
            AuthUserService,
            OfficialUndertimeService,
            CommonService,
            FormBuilder,
            DaterangepickerConfig])
    ], MyUndertimeListComponent);
    return MyUndertimeListComponent;
}());
export { MyUndertimeListComponent };
//# sourceMappingURL=my-undertime-list.component.js.map