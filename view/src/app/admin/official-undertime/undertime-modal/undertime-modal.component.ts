import { Component } from '@angular/core';
import { Configuration } from '../../../app.config';
import * as io from "socket.io-client";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { OfficialUndertimeService } from '../../../services/official-undertime.service';
import { CommonService } from '../../../services/common.service';
import { OfficialUndertime } from '../../../model/official-undertime';
import { Select2OptionData } from 'ng2-select2';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { AuthUserService } from '../../../services/auth-user.service';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TimepickerModule } from 'ngx-bootstrap';
import { ValidationModalComponent } from '../../validation-modal/validation-modal.component';

@Component({
  selector: 'app-undertime-modal',
  templateUrl: './undertime-modal.component.html',
  styleUrls: ['./undertime-modal.component.css']
})
export class UndertimeModalComponent extends DialogComponent<null, boolean> {

 	public success_title;
	public success_message; 
	public error_title: string
	public error_message: string;
	public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public options: Select2Options;
	public current: any;
	public type : any;
	public type_value : any;
	ou = new OfficialUndertime();
	OUForm:any;
	emp:any;
	poststore:any;
	user_id:any;
	public ismeridian:boolean = true;
	public mainInput = {
        start: moment(),
        end: moment().subtract(11, 'month')
    }
    public start_time:Date = new Date("2017-05-15 16:00:00");
	public end_time:Date = new Date("2017-05-15 17:00:00");
    validate = true;
    employee_id:any;
    ou_id:any;
    ou_data:any;
    remarks:any;
    edit:any;
    create:any;
    multi:any;
    single:any;
    date:any;
    length_hours:any;
    user=false;
    admin=false;
    empStats=false;
    dupVal=false;
    supervisor_id:any;
    role_id:any;

    messenger_id:any;

    socket: SocketIOClient.Socket;
    host:any;

  constructor(
  	private _common_service: CommonService,
   	private _OUservice: OfficialUndertimeService, 
   	dialogService: DialogService, 
   	private _fb: FormBuilder, 
   	private _ar: ActivatedRoute,
   	private daterangepickerOptions: DaterangepickerConfig,
   	private _auth_service: AuthUserService,
   	private _conf: Configuration
  	){ 
  	
  	super(dialogService);

  	this.host = this._conf.socketUrl;

    this.OUForm = _fb.group({
    	'start_time': 		[null, [Validators.required]],
		'end_time': 		[null, [Validators.required]],
		'remarks': 			[this.ou.remarks, [Validators.required]],
		'status_id':		[null],
		'date': 			[null]
    }); 

    this.daterangepickerOptions.settings = {
        locale: { format: 'MM/DD/YYYY' },
        alwaysShowCalendars: false,
        singleDatePicker:true,
		showDropdowns: true,
		opens: "center"
	};
  }

	ngOnInit() {
		
		if(this.user == true){
    		this.empStats = true;
    		this.get_employee();
    	}
		if(this.edit == true){
			this.getOU();
    		this.validate = true;
    		this.get_employee();
    	}
    	else{
	   		this.get_employee();
    	}
	   		//uncomment this for socket.io
    		// this.connectToServer();
		this.computeHours();
 	}

	getOU(){

		this._OUservice.getOU(this.ou_id).subscribe(
	      data => {
	        this.ou_data=(data);

	        this.mainInput.start = this.ou_data.date;
	        let start = this.ou_data.date + " " + this.ou_data.start_time;
	        let end = this.ou_data.date + " " + this.ou_data.end_time;
	        this.start_time = new Date(start);
	        this.end_time = new Date(end);
	        this.remarks = this.ou_data.reason;
	        this.current=this.employee_id;
	        this.length_hours = this.ou_data.total_hours;
	      },
	      err => console.error(err)
    	);
    	
	}

	get_employee(){
		
		this.employee = this.emp;
		this.employee_value = [];
		if(this.edit == true) {
			this.employee_value = this.employee_id;
			this.validate = true;
        } 

		this.options = {
			multiple: true
		}
		this.current = this.employee_value;
	}
	changed(data: any) {

		if(this.single == true){
			let v = [];
			v.push(data.value);
			this.current = v;
		}
		else{
			this.current = data.value;
		}
		let len = this.current.length;

		if(len >= 1){
			this.empStats = true;
		}
		else{
			this.empStats = false;
		}

	    this.validation();
	    this.duplicateEntryValidation();
	}

	validation(){
		if(this.empStats == true && this.dupVal == true|| this.edit == true){
      		this.validate=true;
     	}
     	else{
     		this.validate=false;
     	}
	}

	submit(){

		this.computeHours();
		this.duplicateEntryValidation();

		let ou = this.OUForm.value;

		if(this.edit == true && this.user == false){
			let id = this.ou_id;
			this.OUForm.value.id= this.ou_id;
			ou = this.OUForm.value;

			this._OUservice
			.updateOU(ou)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
			
		}
		else if(this.edit == true && this.user == true){
			let id = this.ou_id;
			this.OUForm.value.id= this.ou_id;
			ou = this.OUForm.value;

			this._OUservice
			.updateMyOU(ou)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
			
		}

		if(this.create == true && this.user == false){
			this._OUservice
			.createOU(ou)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}
		else if(this.create == true && this.user == true){
			this._OUservice
			.createMyOU(ou)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}

	}

	connectToServer(){
	    let socketUrl = this.host;
	    this.socket = io.connect(socketUrl);
	    this.socket.on("connect", () => this.connect());
	    this.socket.on("disconnect", () => this.disconnect());
	    
	    this.socket.on("error", (error: string) => {
	      console.log(`ERROR: "${error}" (${socketUrl})`);
	    });

	   
	  }

	// Handle connection opening
	private connect() {
	    // Request initial list when connected
	    this.socket.emit("join", 'Client User -> ');
	}
	// Handle connection closing
	private disconnect() {
	    console.log(`Disconnected from "${this.host}"`);
	}

	errorCatch(){
		if(this.poststore.status_code == 200){
			this.success_title = 'Success';
			this.success_message = 'No Changes Happen';
			setTimeout(() => {
	  		   this.close();
	      	}, 1000);
		}
		else if(this.poststore.status_code == 400){

			this.error_title = 'Warning';
			this.error_message = 'Request is already exist';
			let temp;
			setTimeout(() => {
				let disposable = this.dialogService.addDialog(ValidationModalComponent, {
				title:'Warning',
				message:'Undertime',
				url:'official_undertime',
				duplicate:true,
				data:this.poststore.data,
				userDup:this.user,
				adminDup:this.admin,
            	emp:this.emp
				}).subscribe((message)=>{
					if (message == true) { 
						this.error_title = '';
						this.error_message = '';

						setTimeout(() => {
				  		   this.close();
				      	}, 1000);
					}
					else if(message != false){
    					temp = message;

    					for (let x = 0; x < temp.length; ++x) {
							let index = this.current.indexOf(temp[x]);
	    					this.current.splice(index, 1);
    					}

	    				this.employee_value = this.current;

    					if(this.current.length == 0){
	    					this.error_title = '';
							this.error_message = '';
							setTimeout(() => {
					  		   this.close();
					      	}, 1000);
    					}
					}
				});
	      	}, 1000);

		}
		else{
			// socket.io emit
			// this.socket.emit('notify', this.poststore);
			this.validate = false;
			this.success_title = "Success";
			this.success_message = "Successfully";
			setTimeout(() => {
	  		   this.close();
	      	}, 2000);
		}
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	private selectedDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        this.date = moment(dateInput.start).format("YYYY-MM-DD");

        this.duplicateEntryValidation();
    }


	public toggleMode():void {
	this.ismeridian = !this.ismeridian;

	}

	computeHours(){
		let startTime=moment(this.start_time, "HH:mm a");
		let endTime=moment(this.end_time, "HH:mm a");
		this.length_hours = endTime.diff(startTime, 'hours');
	}

	duplicateEntryValidation(){
		this.OUForm.value.employee_id = this.current;
		this.OUForm.value.supervisor_id = this.supervisor_id;
		this.OUForm.value.created_by = this.user_id;
		this.OUForm.value.start_time = moment(this.start_time).format("HH:mm");
		this.OUForm.value.end_time = moment(this.end_time).format("HH:mm");
		this.OUForm.value.date = this.date;
		this.OUForm.value.total_hours = this.length_hours;

		if (this.date == null) {
			this.OUForm.value.date = moment(this.mainInput.start).format("YYYY-MM-DD");
		}

		if(this.user == true  && this.create == true || this.user == true && this.edit == true){
			let v = [];
			v.push(this.employee_id);
			this.OUForm.value.employee_id = v;
		}

		if(this.edit == true){
			this.OUForm.value.edit = this.edit;
			this.OUForm.value.id= this.ou_id;
		}
		else{
			this.OUForm.value.edit = false;
		}

		let ou = this.OUForm.value;
		
		if(this.OUForm.value.employee_id != 0 && this.OUForm.value.date != null || this.edit == true){
			this._OUservice
			.duplicateEntryValidation(ou)
			.subscribe(
				data => {
					this.poststore = data;
					if(this.poststore.message == "duplicate"){
						this.dupVal = false;
						this.errorCatch();
					}
					else{
						this.dupVal = true;
						this.error_title = '';
						this.error_message = '';
					}

					this.validation();
				},
				err => this.catchError(err)
			);
		}
	}

}
