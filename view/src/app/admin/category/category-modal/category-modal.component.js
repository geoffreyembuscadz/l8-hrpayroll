var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Headers } from '@angular/http';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { CategoryService } from '../../../services/category.service';
import { CommonService } from '../../../services/common.service';
var CategoryModalComponent = /** @class */ (function (_super) {
    __extends(CategoryModalComponent, _super);
    function CategoryModalComponent(dialogService, _fb, _ar, _category_service, _rt, modalService, _common_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this._ar = _ar;
        _this._category_service = _category_service;
        _this._rt = _rt;
        _this.modalService = modalService;
        _this._common_service = _common_service;
        _this.validate = true;
        _this.start_date = new Date();
        _this.subCategory = false;
        _this.is_cat_value = ['1', '0'];
        _this.is_cat = [
            { id: 1, text: 'Main Category' },
            { id: 0, text: 'Sub-Category' }
        ];
        _this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        _this.headers.append('Authorization', "Bearer " + authToken);
        _this.headers.append('Content-Type', 'application/json');
        _this.headers.append('Accept', 'application/json');
        _this.Form = _fb.group({
            'name': [null],
            'description': [null],
            'is_cat': [null],
        });
        return _this;
    }
    CategoryModalComponent.prototype.ngOnInit = function () {
        if (this.edit == true) {
            this.get_data();
            this.getIsCat();
        }
        else {
            this.getIsCat();
        }
    };
    CategoryModalComponent.prototype.get_data = function () {
        var _this = this;
        var id = this.id;
        this._category_service.getCategory(id).subscribe(function (data) {
            _this.setType(data);
        }, function (err) { return console.error(err); });
    };
    CategoryModalComponent.prototype.setType = function (type) {
        this.id = type.id;
        this.name = type.name;
        this.description = type.description;
        this.is_cat_value = type.is_cat;
    };
    CategoryModalComponent.prototype.onSubmit = function () {
        var _this = this;
        this.Form.value.is_cat = this.is_cat_current;
        var model = this.Form.value;
        if (this.edit == true) {
            var id = this.id;
            this._category_service.updateCategory(id, model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data); // fetched the records
                _this.validate = false;
                _this.success_title = "Success!";
                _this.success_message = "Successfully updated";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
        if (this.create == true) {
            this._category_service.storeCategory(model)
                .subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success!";
                _this.success_message = "Successfully created";
                setTimeout(function () {
                    _this.close();
                }, 1000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    CategoryModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    CategoryModalComponent.prototype.archive = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: this.id,
            url: 'category'
        }, { backdropColor: 'rgba(238,238,238)',
            closeByClickingOutside: true }).subscribe(function (isConfirmed) {
            _this.close();
        });
    };
    CategoryModalComponent.prototype.getIsCat = function () {
        this.is_cat_current = this.is_cat_value;
    };
    CategoryModalComponent.prototype.changedIsCat = function (data) {
        this.is_cat_current = data.value;
    };
    CategoryModalComponent = __decorate([
        Component({
            selector: 'app-category-modal',
            templateUrl: './category-modal.component.html',
            styleUrls: ['./category-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            ActivatedRoute,
            CategoryService,
            Router,
            DialogService,
            CommonService])
    ], CategoryModalComponent);
    return CategoryModalComponent;
}(DialogComponent));
export { CategoryModalComponent };
//# sourceMappingURL=category-modal.component.js.map