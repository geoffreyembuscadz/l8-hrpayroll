import 'rxjs/add/operator/catch'
import { Component, OnInit, NgZone, Inject, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
import { CategoryService } from '../../../services/category.service';
import { Select2OptionData } from 'ng2-select2';
import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-category-modal',
  templateUrl: './category-modal.component.html',
  styleUrls: ['./category-modal.component.css']
})
export class CategoryModalComponent extends DialogComponent<null, boolean> {

	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	private headers: Headers;
	public poststore: any;
	public id: any;
	public type: any;
	public name: any;
	public description: any;
	public parent:any;
	edit:any;
	create:any;
  	Form : FormGroup;
  	validate = true;
	public type_value : any;
	type_current:any;
	type_id:any;
	value:any;
	public start_date:Date = new Date();
	subCategory = false;	
	question: any;
	question_value: Array<Select2OptionData>;
	public question_current : any;
	public options: Select2Options;
	user_id:any;
	created_by:any;
	get_segment: any;
	segment_id: any;
	segment_name: any;
	segment_value: Array<Select2OptionData>;
	public segment_current: any;

	public is_cat_current: any;
	is_cat_value = ['1', '0'];
	is_cat=[
			{id:1,text:'Main Category'},
			{id:0,text:'Sub-Category'}
			
		];

constructor( 
	dialogService: DialogService, 
	private _fb: FormBuilder, 
	private _ar: ActivatedRoute,  
	private _category_service: CategoryService,  
	private _rt: Router,
	private modalService: DialogService,
	private _common_service: CommonService,
	) {

	super(dialogService);
	this.headers = new Headers();
	let authToken = localStorage.getItem('id_token');
	this.headers.append('Authorization', `Bearer ${authToken}`);
	this.headers.append('Content-Type', 'application/json');
	this.headers.append('Accept', 'application/json');

	this.Form = _fb.group({
	  'name':			[null],
	  'description': 	[null],
	  'is_cat': 		[null],
	});  
	
	}

	ngOnInit() {
		
		if(this.edit == true){
			this.get_data();
			this.getIsCat();
		}else{
			this.getIsCat();
		}

	}

	get_data(){
		let id = this.id;
		this._category_service.getCategory(id).subscribe(
		  data => {
		    this.setType(data);
		  },
		  err => console.error(err)
		);
	}

	public setType(type: any){
		this.id = type.id;
		this.name = type.name;
		this.description = type.description;
		this.is_cat_value = type.is_cat;
	}

  	onSubmit() {
  		this.Form.value.is_cat = this.is_cat_current;
		let model = this.Form.value;
	  	if(this.edit == true){
		    let id = this.id;		   
		    this._category_service.updateCategory(id, model)
		    .subscribe(
		      data => {
		        this.poststore = Array.from(data); // fetched the records
		        this.validate = false;
		        this.success_title = "Success!";
		       	this.success_message = "Successfully updated";
		        setTimeout(() => {
		       this.close();
		      }, 1000);
		      },
		      err => this.catchError(err)
		    );
	  	}

	  	if(this.create == true){

	  		this._category_service.storeCategory(model)
		    .subscribe(
		      data => {
		        this.poststore = Array.from(data); 
		        this.success_title = "Success!";
		        this.success_message = "Successfully created";
		        setTimeout(() => {
		       this.close();
		      }, 1000);
		      },
		      err => this.catchError(err)
		    );
	  	}
  	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}
  	archive(){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:this.id,
            url:'category'
        	},
        	{ backdropColor: 'rgba(238,238,238)' ,
        	  closeByClickingOutside:true}
        	).subscribe((isConfirmed)=>{
		       	this.close();               
        });
	}

	getIsCat(){

		this.is_cat_current = this.is_cat_value;
		
	}
	changedIsCat(data: any) {
		this.is_cat_current = data.value;		
	}
}
