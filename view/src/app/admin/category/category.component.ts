import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { CategoryService } from '../../services/category.service';
import { CommonService } from '../../services/common.service';
import { CategoryModalComponent } from '../category/category-modal/category-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { AuthUserService } from '../../services/auth-user.service';

@Component({
	selector: 'app-category',
	templateUrl: './category.component.html',
	styleUrls: ['./category.component.css']
})

@Injectable()
export class CategoryComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	dtElement: DataTableDirective;
	dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};
	public OT_id: any;
	api: String;
	status:any;
	cat:any;
	
	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	constructor( 
		private modalService: DialogService,
		private _conf: Configuration,
		private _common_service: CommonService,
		private _category_service: CategoryService,
		
		){

		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");

		this.api = this._conf.ServerWithApiUrl + 'overtime/';
	}

	ngOnInit() {
		this.data();
	}

	data(){
		this._category_service.category_name().
		subscribe(
			data => {
				this.cat = data; 
				this.rerender();
			},
			err => console.error(err)
			);
	}

	addModal(){
		let disposable = this.modalService.addDialog(CategoryModalComponent, {
			title:'Create Category',
			button:'Add',
			url:'category',
			create:true
		}).subscribe((isConfirmed)=>{
			this.rerender();
			this.ngOnInit();
		});
	}

	editModal(id:any){
		let disposable = this.modalService.addDialog(CategoryModalComponent, {
			title:'Edit Category',
			button:'Add',
			edit:true,
			id:id
		}).subscribe((isConfirmed)=>{
			this.rerender();
			this.ngOnInit();
		});
	}


	ngOnDestroy() {
		this.body.classList.remove("skin-blue");
		this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
		this.dtTrigger.next();
	}

	rerender(): void {
		this.dtElement.dtInstance.then((dtInstance) => {
			dtInstance.destroy();
			this.dtTrigger.next();
		});
	}

	archiveCategory(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'category'
        	}, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
		       	this.rerender();
                this.ngOnInit();
        });
	}
}
