var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { NgUploaderOptions } from 'ngx-uploader';
import { Configuration } from '../../app.config';
import { Employee } from '../../model/employee';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { CommonService } from '../../services/common.service';
var URL = '';
var EmployeeEditComponent = /** @class */ (function () {
    /*
    form_positions: any;
    form_position_count: number;
    */
    function EmployeeEditComponent(_conf, _fb, _ar, _company_service, _empservice, _rt, _zone, _common_service) {
        var _this = this;
        this._conf = _conf;
        this._fb = _fb;
        this._ar = _ar;
        this._company_service = _company_service;
        this._empservice = _empservice;
        this._rt = _rt;
        this._zone = _zone;
        this._common_service = _common_service;
        this.user_thumbnail_employee = 'assets/img/user-placeholder.png';
        this.route_upload_attachment = 'upload?type=employee_attachment';
        this.route_upload_img = 'upload?type=employee_img';
        this.msg_terminate_title = null;
        this.msg_terminate_msg = null;
        this.employee = new Employee();
        this.form_job_titles = [];
        this.form_departments = [];
        this.form_employee_dependent = [{ index: 0 }];
        this.form_confirm_archive = false;
        this.form_confirm_terminate = false;
        this.form_employee_dependency_row = 0;
        this.form_validation_errors = [];
        this.form_default_user_placeholder = '';
        this.form_user_image = '';
        this.form_government_fields = [
            { code: 'SSS', label: 'SSS No.' },
            { code: 'PAGIBIG', label: 'Pag-ibig' },
            { code: 'PHILHEALTH', label: 'PhilHealth No.' },
            { code: 'TIN', label: 'TIN No.' }
        ];
        this.errorUploadImageMessage = '';
        this.errorUploadAttachmentMessage = '';
        this.fetched_company_schedules = [];
        this.selected_company_schedule = { days: [] };
        this.sizeLimit = 1000000;
        this.selectTwoWorkingScheduleOptions = {
            multiple: true
        };
        this.onboarding = [];
        this.checkAllValOnboarding = false;
        this.getSuperVisors();
        this.getCompanies();
        this.getDepartments();
        this.getJobTitles();
        this.id = this._ar.snapshot.params['employee_id'];
        this._empservice.getEmployee(this.id).subscribe(function (data) {
            _this.setEmployee(data);
        }, function (err) { return console.error(err); });
        this.form_genders = this._empservice.getGenders();
        this.form_suffixes = this._empservice.getSuffixes();
        this.form_skills = this._empservice.getEmployeeSkills();
        this.form_blood_types = this._empservice.getBloodTypes();
        this.form_income_type = this._empservice.getIncomeType();
        this.form_employee_bank_name = this._empservice.getBanks();
        this.form_marital_statuses = this._empservice.getEmployeeMaritalStatuses();
        this.form_employment_status = this._empservice.getEmploymentStatus();
        this.updateEmergencyContacts = _fb.group({
            'name[0]': '',
            'relationship[0]': '',
            'contact_number[0]': ''
        });
        this.updateEmployeeDependencies = _fb.group({
            'employee_dependent_name[0]': ['', [Validators.required]],
            'employee_dependent_age[0]': ['', [Validators.required]],
            'employee_dependent_relationship[0]': ['', [Validators.required]],
            'employee_dependent_pwd[0]': ['', [Validators.required]]
        });
        this.updateEmployeeForm = _fb.group({
            'firstname': [this.employee.firstname, [Validators.required]],
            'middlename': [this.employee.middlename, [Validators.required]],
            'lastname': [this.employee.lastname, [Validators.required]],
            'suffix': [this.employee.suffix, [Validators.required]],
            'gender': [this.employee.gender, [Validators.required]],
            'birthdate': [this.employee.birthdate, [Validators.required]],
            'blood_type': [this.employee.blood_type, [Validators.required]],
            'citizenship': [this.employee.citizenship, [Validators.required]],
            'employee_code': [this.employee.employee_code, [Validators.required]],
            'department': [this.employee.department, [Validators.required]],
            'position': [this.employee.position, [Validators.required]],
            'tel_no': [this.employee.tel_no, [Validators.required]],
            'cel_no': [this.employee.cel_no, [Validators.required]],
            'email': [this.employee.email, [Validators.required]],
            'primary_company': [this.employee.primary_company, [Validators.required]],
            'company_branch': [this.employee.company_branch, [Validators.required]],
            'skills': [this.employee.skills, [Validators.required]],
            'marital_status': [this.employee.marital_status, [Validators.required]],
            'starting_date': [this.employee.starting_date, [Validators.required]],
            'ending_date': [this.employee.ending_date, [Validators.required]],
            'salary_receiving_type': [this.employee.salary_receiving_type, [Validators.required]],
            'income_rate': [this.employee.income_rate, [Validators.required]],
            'allowance_amount': [this.employee.allowance_amount, [Validators.required]],
            'current_address': [this.employee.current_address, [Validators.required]],
            'province_address': [this.employee.province_address, [Validators.required]],
            'nationality': ['Filipino', [Validators.required]],
            'employment_status': [this.employee.employment_status, [Validators.required]],
            'driving_license_no': [this.employee.driving_license_no, [Validators.required]],
            'sss': [this.employee.sss, [Validators.required]],
            'pagibig_no': [this.employee.pagibig_no, [Validators.required]],
            'philhealth_no': [this.employee.philhealth_no, [Validators.required]],
            'tin_no': [this.employee.tin_no, [Validators.required]],
            'supervisor_id': this.employee.supervisor_id,
            'employee_image': [this.employee.employee_image, [Validators.required]],
            'sick_leave_credit': ['', [Validators.required]],
            'e_cola': [this.employee.e_cola, [Validators.required]],
            'bank_name': [this.employee.bank_name, [Validators.required]],
            'bank_code': [this.employee.bank_code, [Validators.required]],
            'bank_account_number': [this.employee.bank_account_number, [Validators.required]],
            'vacation_leave_credit': [this.employee.vacation_leave_credit, [Validators.required]],
            'emergency_leave_credit': [this.employee.emergency_leave_credit, [Validators.required]],
            'maternity_leave_credit': [this.employee.maternity_leave_credit, [Validators.required]],
            'others_leave_credit': [this.employee.others_leave_credit, [Validators.required]],
            'working_schedule': [this.employee.working_schedule, [Validators.required]],
            'ip_address': [this.employee.ip_address, [Validators.required]],
            'mac_address': [this.employee.mac_address, [Validators.required]],
            'ddns': [this.employee.ddns, [Validators.required]],
            'onboarding': [null]
        });
        // uploader ngzone
        this.options = new NgUploaderOptions({
            url: this._conf.ServerWithApiUrl + this.route_upload_attachment,
            autoUpload: true,
            calculateSpeed: true
        });
        this.optionsImg = new NgUploaderOptions({
            url: this._conf.ServerWithApiUrl + this.route_upload_img,
            autoUpload: true,
            calculateSpeed: true,
            filterExtensions: true,
            allowedExtensions: ['jpg', 'png']
        });
        if (this.employee.date_terminated != null) {
            this.msg_terminate_title = 'Inactive Record';
            this.msg_terminate_msg = 'This employee has been terminated.';
        }
    }
    EmployeeEditComponent.prototype.onchangeSkills = function (select_options) {
        this.selectedEmployeeSkillsOptions = Array.apply(null, select_options)
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this.employee.skills = this.selectedEmployeeSkillsOptions;
    };
    EmployeeEditComponent.prototype.onchangeSupervisor = function (select_options) {
        this.selectedEmployeeSupervisorsOptions = Array.apply(null, select_options)
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this.employee.supervisor_id = this.selectedEmployeeSupervisorsOptions;
    };
    EmployeeEditComponent.prototype.changeSelectEmployeeWorkingSchedule = function (select_options) {
        this.selectedEmployeeSchedOptions = Array.apply(null, select_options)
            .filter(function (option) { return option.selected; })
            .map(function (option) { return option.value; });
        this.employee.working_schedule = this.selectedEmployeeSchedOptions;
    };
    EmployeeEditComponent.prototype.validateUpdate = function () {
        var _this = this;
        /* for multiple select to get the values.
        No Support for catching values using ngModel & formControl for select multiple on Angular 2. */
        this.updateEmployeeForm.get('working_schedule').setValue(this.employee.working_schedule);
        this.onboardingCheck();
        var employee_model = this.updateEmployeeForm.value;
        var employee_model_keys = Object.keys(employee_model);
        var error_fields = {};
        var required_fields = ['firstname', 'middlename', 'lastname', 'income_rate', 'working_schedule', 'employment_status'];
        for (var index = 0; index < employee_model_keys.length; index++) {
            if (required_fields.indexOf(employee_model_keys[index]) > -1 && employee_model[employee_model_keys[index]] == '') {
                error_fields[employee_model_keys[index]] = "Income Rate field is required.";
            }
        }
        if (Object.keys(error_fields).length == 0) {
            this._empservice.updateEmployee(this.id, employee_model).subscribe(function (data) {
                _this.poststore = Array.from(data);
                _this.success_title = "Success!";
                _this.success_message = "An employee record was successfully updated.";
                _this.error_title = null;
                _this.error_message = null;
            }, function (err) { return function (err) {
                this.error_title = 'Error';
                this.error_message = err;
            }; });
        }
        else {
            var error_message = Object.keys(error_fields).join();
            error_message = error_message.replace(/_/g, ' ');
            error_message = error_message.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
            this.error_title = 'Oops!';
            this.error_message = 'Seems like there\'s a field you missed. Please check the following fields: ' + error_message;
        }
    };
    EmployeeEditComponent.prototype.catchError = function (error) {
    };
    EmployeeEditComponent.prototype.setEmployee = function (emp) {
        var _this = this;
        this.id = emp.id;
        this.employee = emp;
        this.employee.skills = (emp.skills != null) ? emp.skills.split(',') : [];
        this.employee.supervisor_id = JSON.parse(emp.supervisor_id);
        var working_schedules = [];
        var fetch_employee_working_schedules = emp.working_schedule;
        this.employee.working_schedule = fetch_employee_working_schedules;
        if (this.employee.employee_image != null || this.employee.employee_image != '') {
            this.form_user_image = this.employee.employee_image;
            this.form_default_user_placeholder = this.employee.employee_image;
        }
        else if (this.employee.employee_image == '') {
            this.form_user_image = this._conf.frontEndUrl + this.user_thumbnail_employee;
            this.form_default_user_placeholder = this._conf.frontEndUrl + this.user_thumbnail_employee;
        }
        var employee_emergency_contacts = this.employee.emergency_contacts;
        for (var _index_employee_emergency_contacts = 0; _index_employee_emergency_contacts < employee_emergency_contacts.length; _index_employee_emergency_contacts++) {
            this.updateEmergencyContacts.addControl('name[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].person_name);
            this.updateEmergencyContacts.addControl('relationship[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].relationship);
            this.updateEmergencyContacts.addControl('contact_number[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].contact_number);
        }
        if (this.employee.primary_company != null) {
            this._company_service.getCompany(this.employee.primary_company).subscribe(function (data) {
                _this.setCompanyBranches(data.branches);
                _this.setCompanyPolicies(data.policies);
                _this.setCompanyGovernmentFields(data.emp_govrn_fields);
                _this.setCompanySchedulesSelection(data.schedules);
            }, function (err) { return console.error(err); });
        }
        if (this.form_user_image == '0') {
            this.form_user_image = this._conf.frontEndUrl + this.user_thumbnail_employee;
        }
        this.getOnBoarding(emp.onboarding);
        this.employee.ip_address = emp.user_time_setting.ip_address;
        this.employee.mac_address = emp.user_time_setting.mac_address;
        this.employee.ddns = emp.user_time_setting.ddns;
    };
    EmployeeEditComponent.prototype.primaryCompanyChanged = function () {
        var _this = this;
        var company_id = this.updateEmployeeForm.get('primary_company').value;
        this._company_service.getCompany(company_id).subscribe(function (data) {
            _this.setCompanyBranches(data.branches);
            _this.setCompanyPolicies(data.policies);
            _this.setCompanySchedulesSelection(data.schedules);
        });
    };
    EmployeeEditComponent.prototype.setCompanyBranches = function (data) {
        var company_branches = [];
        var index = 0;
        for (var _a = 0, data_1 = data; _a < data_1.length; _a++) {
            var company_branch = data_1[_a];
            company_branches[index] = company_branch;
            index++;
        }
        this.form_company_branches = company_branches;
    };
    EmployeeEditComponent.prototype.changeBranchAndSched = function (company_id) {
        this.primaryCompanyChanged();
        this.changeCompanyBranch(company_id);
    };
    EmployeeEditComponent.prototype.changeCompanyBranch = function (company_id) {
        var _this = this;
        this._company_service.getCompany(company_id).subscribe(function (data) {
            _this.setCompanyBranches(data.branches);
        }, function (err) { return console.error(err); });
    };
    EmployeeEditComponent.prototype.setCompanySchedulesSelection = function (data) {
        var index = 0;
        var schedule_selection = [];
        var employee_assigned_schedule = this.employee.working_schedule;
        this.form_working_schedules = [];
        //someArray.indexOf("Some entry in array") > -1
        for (var _a = 0, data_2 = data; _a < data_2.length; _a++) {
            var sched = data_2[_a];
            var days_in_sched = JSON.parse(sched.days);
            var selected_schedule = null;
            var schedule_approved = null;
            if (employee_assigned_schedule.indexOf(sched.id) > -1) {
                selected_schedule = true;
            }
            if (employee_assigned_schedule.indexOf(sched.approved) > -1) {
                schedule_approved = true;
            }
            schedule_selection[index] = { id: sched.id, name: sched.name, days: days_in_sched, time_in: sched.time_in, time_out: sched.time_out, selected_sched: selected_schedule, approved: schedule_approved };
            // schedule_selection[index] = { id: sched.id, text: sched.name + ' - Days:' + days_in_sched + ' - Time-in:' + sched.time_in + ' - Time-out' + sched.time_out };
            this.fetched_company_schedules[sched.id] = sched;
            index++;
        }
        this.form_working_schedules = schedule_selection;
    };
    EmployeeEditComponent.prototype.setCompanyPolicies = function (data) {
    };
    EmployeeEditComponent.prototype.setCompanyGovernmentFields = function (data) {
    };
    EmployeeEditComponent.prototype.checkIfScheduleIsIncluded = function (schedule_id) {
        var employee_working_schdules = this.employee.working_schedule;
        for (var _a = 0, employee_working_schdules_1 = employee_working_schdules; _a < employee_working_schdules_1.length; _a++) {
            var employee_working_sched_id = employee_working_schdules_1[_a];
            if (employee_working_sched_id == schedule_id) {
                return true;
            }
        }
        return false;
    };
    EmployeeEditComponent.prototype.renderScheduleData = function (schedule_id) {
        this.selected_company_schedule = this.fetched_company_schedules[schedule_id];
        this.selected_company_schedule.days = JSON.parse(this.fetched_company_schedules[schedule_id].days);
    };
    EmployeeEditComponent.prototype.clearErrorMsg = function () {
        this.error_title = '';
        this.error_message = '';
        this.success_title = '';
        this.success_message = '';
    };
    EmployeeEditComponent.prototype.archiveEmployeeRecord = function () {
        this.form_confirm_archive = true;
    };
    EmployeeEditComponent.prototype.cancelTerminationOfEmployee = function (event) {
        this.form_confirm_terminate = false;
    };
    EmployeeEditComponent.prototype.ngAfterViewInit = function () {
    };
    EmployeeEditComponent.prototype.getAttachments = function () {
        var _this = this;
        var employee_id = this.id;
        this._empservice.getAttachments(employee_id).subscribe(function (data) {
            _this.employee.attachments = data;
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeEditComponent.prototype.downloadAttachment = function (file_url) {
        window.open(file_url);
    };
    EmployeeEditComponent.prototype.deleteAttachment = function (attachment_id) {
        var _this = this;
        var employee_model = this.updateEmployeeForm.value;
        this._empservice.deleteEmployeAttachment(attachment_id).subscribe(function (data) {
            _this.getAttachments();
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeEditComponent.prototype.ngOnInit = function () { };
    EmployeeEditComponent.prototype.handleImgUpload = function (data) {
        var _this = this;
        this._zone.run(function () {
            _this.response_image = data;
            if (data && data.response) {
                _this.response_image = JSON.parse(data.response);
                _this.form_user_image = _this.response_image.file_url;
                _this.updateEmployeeForm.get('employee_image').setValue(_this.form_user_image);
            }
        });
    };
    EmployeeEditComponent.prototype.handleUpload = function (data) {
        var _this = this;
        this._zone.run(function () {
            _this.response = data;
            if (data && data.response) {
                _this.response = JSON.parse(data.response);
                var employee_id = _this.id;
                var employee_attachment = _this.response.file_url;
                var employee_attachment_dir = _this.response.file_dir;
                _this._empservice.attachEmployeeFiles(employee_id, employee_attachment, employee_attachment_dir).subscribe(function (data) {
                    _this.getAttachments();
                }, function (err) { return _this.catchError(err); });
            }
        });
    };
    EmployeeEditComponent.prototype.beforeUploadImg = function (uploadingFile) {
        if (uploadingFile.size > this.sizeLimit) {
            uploadingFile.setAbort();
            this.errorUploadImageMessage = 'File is too large!';
        }
    };
    EmployeeEditComponent.prototype.beforeUploadAttachments = function (uploadingFile) {
        if (uploadingFile.size > this.sizeLimit) {
            uploadingFile.setAbort();
            this.errorUploadAttachmentMessage = 'File is too large!';
        }
    };
    EmployeeEditComponent.prototype.confirmArchiveEmployee = function (event) {
        event.preventDefault();
        this.form_confirm_archive = true;
    };
    EmployeeEditComponent.prototype.confirmTermination = function (event) {
        event.preventDefault();
        this.form_confirm_terminate = true;
    };
    EmployeeEditComponent.prototype.archiveEmployee = function (event) {
        var _this = this;
        event.preventDefault();
        var employee_id = this.id;
        this._empservice.archiveEmployee(employee_id).subscribe(function (data) {
            //this.setEmployee(data);
            alert('Record is successfully archived.');
            _this._rt.navigateByUrl('admin/employee-list');
        }, function (err) { return console.error(err); });
    };
    EmployeeEditComponent.prototype.cancelArchiveOfRecord = function (event) {
        event.preventDefault();
        this.form_confirm_archive = false;
    };
    EmployeeEditComponent.prototype.terminateEmployee = function (employee_id, event) {
        var _this = this;
        event.preventDefault();
        this._empservice.updateEmployee(employee_id, { id: this.id, date_terminated: 1 }).subscribe(function (data) {
            //this.setEmployee(data);
            alert('Employee Record is successfully marked as terminated.');
            _this._rt.navigateByUrl('admin/employee-list');
        }, function (err) { return console.error(err); });
    };
    EmployeeEditComponent.prototype.onChangeDepartment = function (department_id) {
        if (department_id != null) {
            this.getJobTitles();
        }
    };
    EmployeeEditComponent.prototype.getDepartments = function () {
        var _this = this;
        this._empservice.getDepartments().subscribe(function (data) {
            _this.setDepartments(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeEditComponent.prototype.setDepartments = function (data) {
        var departments = [];
        data.forEach(function (values, index) {
            departments[index] = { id: values.id, name: values.name };
        });
        this.form_departments = departments;
    };
    EmployeeEditComponent.prototype.getJobTitles = function () {
        var _this = this;
        this._empservice.getJobTitles().subscribe(function (data) {
            _this.fetchJobTitles(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeEditComponent.prototype.fetchJobTitles = function (data) {
        var job_titles = [];
        data.forEach(function (values, index) {
            job_titles[index] = { id: values.id, name: values.name };
        });
        this.form_job_titles = job_titles;
    };
    EmployeeEditComponent.prototype.getSuperVisors = function () {
        var _this = this;
        this._empservice.getEmployees().subscribe(function (data) {
            _this.setSuperVisors(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeEditComponent.prototype.addDependencyRow = function () {
        this.form_employee_dependency_row++;
        var index_count = this.form_employee_dependency_row;
        var counter = this.form_employee_dependent.length; //(this.form_emergency_contact.length > 1) ? (this.form_emergency_contact.length - 1) : this.form_emergency_contact.length;
        this.updateEmployeeDependencies.addControl('employee_dependent_name[' + index_count + ']', new FormControl());
        this.updateEmployeeDependencies.addControl('employee_dependent_age[' + index_count + ']', new FormControl());
        this.updateEmployeeDependencies.addControl('employee_dependent_relationship[' + index_count + ']', new FormControl());
        this.updateEmployeeDependencies.addControl('employee_dependent_pwd[' + index_count + ']', new FormControl());
        this.form_employee_dependent.push({ index: index_count });
    };
    EmployeeEditComponent.prototype.setSuperVisors = function (data) {
        var supervisors = data;
        var fetched_supervisors = [];
        for (var _i = 0; _i < supervisors.length; _i++) {
            fetched_supervisors[_i] = { id: supervisors[_i].id, name: supervisors[_i].lastname + ', ' + supervisors[_i].firstname + ' - Employee Code: ' + supervisors[_i].employee_code };
        }
        this.form_supervisors = fetched_supervisors;
    };
    EmployeeEditComponent.prototype.getCompanies = function () {
        var _this = this;
        this._empservice.getCompanies().subscribe(function (data) {
            _this.setCompanies(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeEditComponent.prototype.getCompanyBranches = function () {
        // this._company_service.getCompanyBranchesByCompany(this.employee.primary_company).subscribe(
        // 	data => {
        // 	}
        // );
    };
    EmployeeEditComponent.prototype.setCompanies = function (data) {
        var companies = data;
        var fetched_companies = [];
        for (var _i = 0; _i < companies.length; _i++) {
            fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
        }
        this.form_companies = fetched_companies;
    };
    EmployeeEditComponent.prototype.checkIfEmployeeHasThisSkill = function (skill) {
        if (this.employee.skills != null) {
            for (var _a = 0, _b = this.employee.skills; _a < _b.length; _a++) {
                var employee_skill = _b[_a];
                if (skill == employee_skill) {
                    return true;
                }
            }
        }
    };
    EmployeeEditComponent.prototype.checkifEmployeeSupervisorSelected = function (supervisor) {
        if (this.employee.supervisor_id != null) {
            for (var _a = 0, _b = this.employee.supervisor_id; _a < _b.length; _a++) {
                var employee_supervisor = _b[_a];
                if (supervisor == employee_supervisor.id) {
                    return true;
                }
            }
        }
    };
    EmployeeEditComponent.prototype.getOnBoarding = function (dat) {
        var _this = this;
        this._common_service.getOnboarding().
            subscribe(function (data) {
            _this.onboarding = data;
            if (_this.onboarding != null && dat != null) {
                for (var i = 0; i < _this.onboarding.length; ++i) {
                    for (var x = 0; x < dat.length; ++x) {
                        if (_this.onboarding[i].id == dat[x].id) {
                            _this.onboarding[i].val = true;
                        }
                    }
                }
            }
        }, function (err) { return console.error(err); });
    };
    EmployeeEditComponent.prototype.onboardingCheckbox = function (index) {
        if (this.onboarding[index].val == false) {
            this.onboarding[index].val = true;
        }
        else {
            this.onboarding[index].val = false;
            this.checkAllValOnboarding = false;
        }
    };
    EmployeeEditComponent.prototype.checkAllOnboarding = function () {
        if (this.checkAllValOnboarding == false) {
            var leng = this.onboarding.length;
            for (var i = 0; i < leng; i++) {
                this.onboarding[i].val = true;
            }
            this.checkAllValOnboarding = true;
        }
        else {
            var len = this.onboarding.length;
            for (var i = 0; i < len; i++) {
                this.onboarding[i].val = false;
            }
            this.checkAllValOnboarding = false;
        }
    };
    EmployeeEditComponent.prototype.onboardingCheck = function () {
        var len = this.onboarding.length;
        var temp = [];
        for (var i = 0; i < len; ++i) {
            if (this.onboarding[i].val == true) {
                temp.push(this.onboarding[i].id);
            }
        }
        this.updateEmployeeForm.value.onboarding = temp;
    };
    EmployeeEditComponent = __decorate([
        Component({
            selector: 'admin-employee-edit',
            templateUrl: './employee-edit.component.html',
            styleUrls: ['./employee-edit.component.css']
        }),
        __metadata("design:paramtypes", [Configuration,
            FormBuilder,
            ActivatedRoute,
            CompanyService,
            EmployeeService,
            Router,
            NgZone,
            CommonService])
    ], EmployeeEditComponent);
    return EmployeeEditComponent;
}());
export { EmployeeEditComponent };
//# sourceMappingURL=employee-edit.component.js.map