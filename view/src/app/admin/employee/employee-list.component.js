var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Rx';
import { Configuration } from '../../app.config';
import { EmployeeService } from '../../services/employee.service';
import { CommonService } from '../../services/common.service';
var EmployeeListComponent = /** @class */ (function () {
    function EmployeeListComponent(_commonserv, _emp_service, _route, _fb, _conf) {
        this._commonserv = _commonserv;
        this._emp_service = _emp_service;
        this._route = _route;
        this._fb = _fb;
        this._conf = _conf;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filter_status_field = [{ 'value': 0, text: 'Inactive' }, { 'value': 1, text: 'Active' }];
        this.getCompanySelectionFilter();
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api_employee = this._conf.ServerWithApiUrl + 'employee/';
        this.filterEmployeeList = _fb.group({
            'name': ['', [Validators.required]],
            'company': ['', [Validators.required]],
            'status': ['', [Validators.required]]
        });
    }
    EmployeeListComponent.prototype.ngAfterViewInit = function () {
    };
    EmployeeListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var auth_token = localStorage.getItem('id_token');
        // DataTable Configuration
        this.dtOptions = {
            bFilter: true,
            searching: true,
            ajax: {
                url: this.api_employee,
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + auth_token);
                },
            },
            columns: [
                {
                    title: 'ID',
                    data: 'id'
                },
                {
                    title: 'Employee Num.',
                    data: 'employee_code'
                }, {
                    title: 'Name',
                    data: 'text'
                }, {
                    title: 'Company',
                    data: 'company'
                }, {
                    title: 'Status',
                    data: 'status',
                    render: function (data, type, row, meta) {
                        return data == 1 ? 'Active' : 'Inactive';
                    }
                }, {
                    title: 'Action',
                    data: 'id',
                    render: function (data, type, row, meta) {
                        return '<a class="btn btn-info btn-edit-employee" href="admin/employee-edit/' + data + '" ><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    }
                }
            ],
            rowCallback: function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
                var self = _this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                // $('td', nRow).unbind('click');
                // $('td', nRow).bind('click', () => {
                // 	this._route.navigate(['admin/employee-edit/', data.id]);
                // });
                $('.btn-edit-employee', nRow).unbind('click');
                $('.btn-edit-employee', nRow).bind('click', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    _this._route.navigate(['admin/employee-edit', data.id]);
                });
                return nRow;
            }
        };
    };
    EmployeeListComponent.prototype.validateFilter = function (employeeForm, eventEmployeeFilter) {
        eventEmployeeFilter.preventDefault();
        // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        // dtInstance.destroy();
        // Call the dtTrigger to rerender again
        // this.dtTrigger.next();
        // });
        // console.log(this.dtOptions.ajax.load('core'));
        // console.log(this.dtOptions.ajax.url('yahoo.com'));//.url('asdasd').reload()
    };
    EmployeeListComponent.prototype.getCompanySelectionFilter = function () {
        var _this = this;
        this._commonserv.getCompany().subscribe(function (data) {
            _this.setCompanySelectionFilter(data);
        }, function (err) { return console.error(err); });
    };
    EmployeeListComponent.prototype.setCompanySelectionFilter = function (data) {
        this.filter_company_field = data;
    };
    EmployeeListComponent = __decorate([
        Component({
            selector: 'admin-employee-list',
            templateUrl: './employee-list.component.html',
            styleUrls: ['./employee-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [CommonService, EmployeeService, Router, FormBuilder, Configuration])
    ], EmployeeListComponent);
    return EmployeeListComponent;
}());
export { EmployeeListComponent };
//# sourceMappingURL=employee-list.component.js.map