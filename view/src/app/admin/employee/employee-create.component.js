var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { NgUploaderOptions } from 'ngx-uploader';
import { AuthUserService } from '../../services/auth-user.service';
import { Configuration } from '../../app.config';
import { Employee } from '../../model/employee';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { CommonService } from '../../services/common.service';
var URL = '';
var EmployeeCreateComponent = /** @class */ (function () {
    function EmployeeCreateComponent(_conf, _fb, _ar, _rt, _companyserv, _empservice, _zone, _auth_service, _common_service) {
        this._conf = _conf;
        this._fb = _fb;
        this._ar = _ar;
        this._rt = _rt;
        this._companyserv = _companyserv;
        this._empservice = _empservice;
        this._zone = _zone;
        this._auth_service = _auth_service;
        this._common_service = _common_service;
        this.route_upload_attachment = 'upload?type=employee_attachment';
        this.route_upload_img = 'upload?type=employee_img';
        this.employee = new Employee();
        this.form_emergency_contact = [{ index: 0 }];
        this.form_employee_dependent = [{ index: 0 }];
        this.form_emergency_contact_row = 0;
        this.form_employee_dependency_row = 0;
        this.form_default_user_placeholder = '';
        this.form_user_image = '';
        this.fetched_company_schedules = [];
        this.errorUploadImageMessage = '';
        this.errorUploadAttachmentMessage = '';
        this.error_basic_fields = [];
        this.sizeLimit = 1000000;
        this.onProcessSubmit = 0;
        this.onboarding = [];
        this.checkAllValOnboarding = false;
        this.getCompanies();
        this.getDepartments();
        this.getSuperVisors();
        this.form_genders = this._empservice.getGenders();
        this.form_suffixes = this._empservice.getSuffixes();
        this.form_blood_types = this._empservice.getBloodTypes();
        this.form_skills = this._empservice.getEmployeeSkills();
        this.form_employee_bank_name = this._empservice.getBanks();
        this.form_marital_statuses = this._empservice.getEmployeeMaritalStatuses();
        this.form_employment_status = this._empservice.getEmploymentStatus();
        this.form_income_type = this._empservice.getIncomeType();
        this.form_dependency_relationships = this._empservice.getEmployeeDependencyRelationships();
        this.form_default_user_placeholder = this._conf.frontEndUrl + 'assets/img/user-placeholder.png';
        this.emergencyContacts = _fb.group({
            'emergency_person_name[0]': ['', [Validators.required]],
            'emergency_relationship[0]': ['', [Validators.required]],
            'emergency_contact_number[0]': ['', [Validators.required]]
        });
        this.employeeDependencies = _fb.group({
            'employee_dependent_name[0]': ['', [Validators.required]],
            'employee_dependent_age[0]': ['', [Validators.required]],
            'employee_dependent_relationship[0]': ['', [Validators.required]],
            'employee_dependent_pwd[0]': ['', [Validators.required]]
        });
        this.createEmployeeForm = _fb.group({
            'firstname': ['', [Validators.required]],
            'middlename': ['-N/A-', [Validators.required]],
            'lastname': ['', [Validators.required]],
            'suffix': ['', [Validators.required]],
            'primary_company': ['', [Validators.required]],
            'company_branch': ['', [Validators.required]],
            'gender': ['', [Validators.required]],
            'birthdate': ['', [Validators.required]],
            'blood_type': ['', [Validators.required]],
            'citizenship': ['', [Validators.required]],
            'employee_code': ['', [Validators.required]],
            'department': ['', [Validators.required]],
            'position': ['', [Validators.required]],
            'tel_no': ['', [Validators.required]],
            'cel_no': ['', [Validators.required]],
            'email': ['', [Validators.required]],
            'current_address': ['', [Validators.required]],
            'province_address': ['', [Validators.required]],
            'skills': ['', [Validators.required]],
            'marital_status': ['', [Validators.required]],
            'salary_receiving_type': ['', [Validators.required]],
            'income_rate': ['', [Validators.required]],
            'allowance_amount': ['', [Validators.required]],
            'nationality': ['Filipino', [Validators.required]],
            'employment_status': ['', [Validators.required]],
            'driving_license_no': ['', [Validators.required]],
            'sss': ['', [Validators.required]],
            'philhealth_no': ['', [Validators.required]],
            'tin_no': ['', [Validators.required]],
            'supervisor_id': '',
            'pagibig_no': ['', [Validators.required]],
            'emergency_contacts': this.emergencyContacts,
            'employee_dependents': this.employeeDependencies,
            'employee_image': [this._conf.frontEndUrl + 'assets/img/user-placeholder.png', [Validators.required]],
            'starting_date': ['', [Validators.required]],
            'ending_date': ['', [Validators.required]],
            'user_credentials': ['', [Validators.required]],
            'sick_leave_credit': ['', [Validators.required]],
            'vacation_leave_credit': ['', [Validators.required]],
            'emergency_leave_credit': ['', [Validators.required]],
            'maternity_leave_credit': ['', [Validators.required]],
            'others_leave_credit': ['', [Validators.required]],
            'e_cola': ['', [Validators.required]],
            'working_schedule': ['', [Validators.required]],
            'bank_name': ['', [Validators.required]],
            'bank_code': ['', [Validators.required]],
            'bank_account_number': ['', [Validators.required]],
            'ip_address': [null],
            'mac_address': [null],
            'ddns': [null],
            'onboarding': [null]
        });
        // uploader ngzone
        this.options = new NgUploaderOptions({
            url: this._conf.ServerWithApiUrl + this.route_upload_attachment,
            autoUpload: true,
            calculateSpeed: true
        });
        this.optionsImg = new NgUploaderOptions({
            url: this._conf.ServerWithApiUrl + this.route_upload_img,
            autoUpload: true,
            calculateSpeed: true,
            filterExtensions: true,
            allowedExtensions: ['jpg', 'png']
        });
    }
    EmployeeCreateComponent.prototype.ngOnInit = function () {
        this.getOnBoarding();
    };
    EmployeeCreateComponent.prototype.handleImgUpload = function (data) {
        var _this = this;
        this._zone.run(function () {
            _this.response_image = data;
            if (data && data.response) {
                _this.response_image = JSON.parse(data.response);
                _this.form_user_image = _this.response_image.file_url;
                _this.createEmployeeForm.get('employee_image').setValue(_this.form_user_image);
            }
        });
    };
    EmployeeCreateComponent.prototype.handleUpload = function (data) {
        var _this = this;
        this._zone.run(function () {
            _this.response = data;
            if (data && data.response) {
                _this.response = JSON.parse(data.response);
                var employee_id = _this.post_data.id;
                var employee_attachment = _this.response.file_url;
                var employee_attachment_dir = _this.response.file_dir;
                _this._empservice.attachEmployeeFiles(employee_id, employee_attachment, employee_attachment_dir).subscribe(function (data) { }, function (err) { return _this.catchError(err); });
            }
        });
        this.success_title = "Success!";
        this.success_message = "A new employee record was successfully added.";
        setTimeout(function () {
            _this._rt.navigateByUrl('admin/employee-list');
        }, 60000);
    };
    EmployeeCreateComponent.prototype.beforeUploadImg = function (uploadingFile) {
        if (uploadingFile.size > this.sizeLimit) {
            uploadingFile.setAbort();
            this.errorUploadImageMessage = 'File is too large!';
        }
    };
    EmployeeCreateComponent.prototype.primaryCompanyChanged = function () {
        var _this = this;
        var company_id = this.createEmployeeForm.get('primary_company').value;
        this._companyserv.getCompany(company_id).subscribe(function (data) {
            _this.setCompanyBranches(data.branches);
            _this.setCompanyPolicies(data.policies);
            _this.setCompanySchedulesSelection(data.schedules);
        });
    };
    EmployeeCreateComponent.prototype.setCompanySchedulesSelection = function (data) {
        var schedule_selection = [];
        var index = 0;
        for (var _a = 0, data_1 = data; _a < data_1.length; _a++) {
            var sched = data_1[_a];
            var days = JSON.parse(sched.days);
            days = days.join();
            schedule_selection[index] = { id: sched.id, name: sched.name, time_in: sched.time_in, time_out: sched.time_out, days: days };
            this.fetched_company_schedules[sched.id] = sched;
            index++;
        }
        this.form_working_schedules = schedule_selection;
    };
    EmployeeCreateComponent.prototype.setCompanyBranches = function (data) {
        var company_branches = [];
        var index = 0;
        for (var _a = 0, data_2 = data; _a < data_2.length; _a++) {
            var company_branch = data_2[_a];
            company_branches[index] = company_branch;
            index++;
        }
        if (company_branches.length > 0) {
            this.form_company_branches = company_branches;
            this.error_title = '';
            this.error_message = '';
        }
        else {
            alert('Please add a company branch for this company.');
            this.form_company_branches = [];
            this.error_title = 'Warning!';
            this.error_message = 'Please add a company branch for this company.';
        }
    };
    EmployeeCreateComponent.prototype.setCompanyPolicies = function (data) {
        var company_policies_selection = [];
        var index = 0;
        for (var _a = 0, data_3 = data; _a < data_3.length; _a++) {
            var policy = data_3[_a];
            company_policies_selection[index] = { id: policy.id, name: policy.name, policy_type: policy.company_policy_type };
            index++;
        }
        this.form_company_policies = company_policies_selection;
    };
    EmployeeCreateComponent.prototype.beforeUploadAttachments = function (uploadingFile) {
        if (uploadingFile.size > this.sizeLimit) {
            uploadingFile.setAbort();
            this.errorUploadAttachmentMessage = 'File is too large!';
        }
    };
    EmployeeCreateComponent.prototype.exampleValidator = function (control) {
        if (control.value === '') {
            return { example: true };
        }
        return null;
    };
    EmployeeCreateComponent.prototype.postStore = function (data) {
        var _this = this;
        this.post_data = data;
        this.error_title = null;
        this.error_message = null;
        this.success_title = 'Success!';
        this.success_message = 'You have Successfully added a new record of employee.';
        setTimeout(function () {
            _this.onProcessSubmit = 1;
            _this._rt.navigateByUrl('admin/employee-list');
        }, 60000);
    };
    EmployeeCreateComponent.prototype.addDependencyRow = function () {
        this.form_employee_dependency_row++;
        var index_count = this.form_employee_dependency_row;
        var counter = this.form_employee_dependent.length; //(this.form_emergency_contact.length > 1) ? (this.form_emergency_contact.length - 1) : this.form_emergency_contact.length;
        this.employeeDependencies.addControl('employee_dependent_name[' + index_count + ']', new FormControl());
        this.employeeDependencies.addControl('employee_dependent_age[' + index_count + ']', new FormControl());
        this.employeeDependencies.addControl('employee_dependent_relationship[' + index_count + ']', new FormControl());
        this.employeeDependencies.addControl('employee_dependent_pwd[' + index_count + ']', new FormControl());
        this.form_employee_dependent.push({ index: index_count });
    };
    EmployeeCreateComponent.prototype.removeDependencyRow = function (row_count) {
        var data_row_dependent_contacts = this.form_employee_dependent;
        var data_remove_row = data_row_dependent_contacts[row_count];
        var data_remove_row_index = data_remove_row.index;
        data_row_dependent_contacts.splice(row_count, 1);
        this.emergencyContacts.removeControl('employee_dependent_name[' + data_remove_row_index + ']');
        this.emergencyContacts.removeControl('employee_dependent_age[' + data_remove_row_index + ']');
        this.emergencyContacts.removeControl('employee_dependent_relationship[' + data_remove_row_index + ']');
        this.emergencyContacts.removeControl('employee_dependent_pwd[' + data_remove_row_index + ']');
        this.form_employee_dependent = data_row_dependent_contacts;
    };
    EmployeeCreateComponent.prototype.addEmergencyContactRow = function () {
        this.form_emergency_contact_row++;
        var index_count = this.form_emergency_contact_row;
        var counter = this.form_emergency_contact.length; //(this.form_emergency_contact.length > 1) ? (this.form_emergency_contact.length - 1) : this.form_emergency_contact.length;
        this.emergencyContacts.addControl('emergency_person_name[' + index_count + ']', new FormControl());
        this.emergencyContacts.addControl('emergency_relationship[' + index_count + ']', new FormControl());
        this.emergencyContacts.addControl('emergency_contact_number[' + index_count + ']', new FormControl());
        this.form_emergency_contact.push({ index: index_count });
    };
    EmployeeCreateComponent.prototype.removeEmergencyContactRow = function (row_count) {
        var data_row_emergency_contacts = this.form_emergency_contact;
        var data_remove_row = data_row_emergency_contacts[row_count];
        var data_remove_row_index = data_remove_row.index;
        data_row_emergency_contacts.splice(row_count, 1);
        this.emergencyContacts.removeControl('emergency_person_name[' + data_remove_row_index + ']');
        this.emergencyContacts.removeControl('emergency_relationship[' + data_remove_row_index + ']');
        this.emergencyContacts.removeControl('emergency_contact_number[' + data_remove_row_index + ']');
        this.form_emergency_contact = data_row_emergency_contacts;
    };
    EmployeeCreateComponent.prototype.validateCreation = function () {
        var _this = this;
        this.onProcessSubmit = 1;
        this.onboardingCheck();
        var employee_model = this.createEmployeeForm.value;
        this._empservice.storeEmployee(employee_model).subscribe(function (data) {
            _this.postStore(data);
            _this.onProcessSubmit = 1;
            _this.show_attachment_btn = true;
        }, function (err) {
            _this.onProcessSubmit = 0;
            _this.catchError(err);
            console.log(err);
        });
    };
    EmployeeCreateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
            window.scrollTo(0, 0);
        }
    };
    EmployeeCreateComponent.prototype.clearErrorMsg = function () {
        this.error_title = '';
        this.error_message = '';
    };
    EmployeeCreateComponent.prototype.ngAfterViewInit = function () {
    };
    EmployeeCreateComponent.prototype.onChangeDepartment = function (department_id) {
        if (department_id != null) {
            this.getJobTitles();
        }
    };
    EmployeeCreateComponent.prototype.getDepartments = function () {
        var _this = this;
        this._empservice.getDepartments().subscribe(function (data) {
            _this.setDepartments(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeCreateComponent.prototype.setDepartments = function (data) {
        var departments = [];
        data.forEach(function (values, index) {
            departments[index] = { id: values.id, name: values.name };
        });
        this.form_departments = departments;
    };
    EmployeeCreateComponent.prototype.getJobTitles = function () {
        var _this = this;
        this._empservice.getJobTitles().subscribe(function (data) {
            _this.fetchJobTitles(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeCreateComponent.prototype.fetchJobTitles = function (data) {
        var job_titles = [];
        data.forEach(function (values, index) {
            job_titles[index] = { id: values.id, name: values.name };
        });
        this.form_job_titles = job_titles;
    };
    EmployeeCreateComponent.prototype.getSuperVisors = function () {
        var _this = this;
        this._empservice.getEmployees().subscribe(function (data) {
            _this.setSuperVisors(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeCreateComponent.prototype.setSuperVisors = function (data) {
        var supervisors = data;
        var fetched_supervisors = [];
        for (var _i = 0; _i < supervisors.length; _i++) {
            fetched_supervisors[_i] = { id: supervisors[_i].id, name: '' + supervisors[_i].lastname + ', ' + supervisors[_i].firstname + ' - Employee Code: ' + supervisors[_i].employee_code };
        }
        this.form_supervisors = fetched_supervisors;
    };
    EmployeeCreateComponent.prototype.getCompanies = function () {
        var _this = this;
        this._empservice.getCompanies().subscribe(function (data) {
            _this.setCompanies(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeCreateComponent.prototype.setCompanies = function (data) {
        var companies = data;
        var fetched_companies = [];
        for (var _i = 0; _i < companies.length; _i++) {
            fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
        }
        this.form_companies = fetched_companies;
    };
    EmployeeCreateComponent.prototype.getOnBoarding = function () {
        var _this = this;
        this._common_service.getOnboarding().
            subscribe(function (data) {
            _this.onboarding = Array.from(data);
        }, function (err) { return console.error(err); });
    };
    EmployeeCreateComponent.prototype.onboardingCheckbox = function (index) {
        if (this.onboarding[index].val == false) {
            this.onboarding[index].val = true;
        }
        else {
            this.onboarding[index].val = false;
            this.checkAllValOnboarding = false;
        }
    };
    EmployeeCreateComponent.prototype.checkAllOnboarding = function () {
        if (this.checkAllValOnboarding == false) {
            var leng = this.onboarding.length;
            for (var i = 0; i < leng; i++) {
                this.onboarding[i].val = true;
            }
            this.checkAllValOnboarding = true;
        }
        else {
            var len = this.onboarding.length;
            for (var i = 0; i < len; i++) {
                this.onboarding[i].val = false;
            }
            this.checkAllValOnboarding = false;
        }
    };
    EmployeeCreateComponent.prototype.onboardingCheck = function () {
        var len = this.onboarding.length;
        var temp = [];
        for (var i = 0; i < len; ++i) {
            if (this.onboarding[i].val == true) {
                temp.push(this.onboarding[i].id);
            }
        }
        this.createEmployeeForm.value.onboarding = temp;
    };
    EmployeeCreateComponent = __decorate([
        Component({
            selector: 'admin-employee-create',
            templateUrl: './employee-create.component.html',
            styleUrls: ['./employee-create.component.css']
        }),
        __metadata("design:paramtypes", [Configuration,
            FormBuilder,
            ActivatedRoute,
            Router,
            CompanyService,
            EmployeeService,
            NgZone,
            AuthUserService,
            CommonService])
    ], EmployeeCreateComponent);
    return EmployeeCreateComponent;
}());
export { EmployeeCreateComponent };
//# sourceMappingURL=employee-create.component.js.map