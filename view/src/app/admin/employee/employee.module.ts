import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRouting } from './employee.routing';
import { EmployeeEditComponent } from './employee-edit.component';
import { EmployeeListComponent } from './employee-list.component';
import { EmployeeCreateComponent } from './employee-create.component';
import { EmployeeMassUploadComponent } from './employee-mass-upload.component';
import { EmployeeRemarksListComponent } from './employee-remarks-list.component';
import { EmployeeRemarksViewComponent } from './employee-remarks-view.component';
import { EmployeeRemarksCreateComponent } from './employee-remarks-create.component';

@NgModule({
    imports: [
        CommonModule,
        EmployeeRouting
    ],
    declarations: [
        EmployeeEditComponent,
        EmployeeListComponent,        
        EmployeeCreateComponent,
        EmployeeMassUploadComponent,
        EmployeeRemarksListComponent,
        EmployeeRemarksViewComponent,
        EmployeeRemarksCreateComponent
    ]
})
export class EmployeeModule {
}