import { Component, OnInit, Injectable } from '@angular/core';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { Configuration } from '../../app.config';
import { EmployeeService } from '../../services/employee.service';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'admin-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})

@Injectable()
export class EmployeeListComponent implements OnInit {
	dtElement: DataTableDirective;
	dtTrigger: any = new Subject();
	dtOptions: any = {};
	api_employee: String;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	private filter_company_field: any;
	private filter_status_field = [{ 'value':0, text:'Inactive' }, { 'value':1, text:'Active' }];

	filterEmployeeList : FormGroup;

	constructor(private _commonserv: CommonService, private _emp_service: EmployeeService, private _route: Router, private _fb: FormBuilder, private _conf: Configuration){
		this.getCompanySelectionFilter();

		//add the the body classes
		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");
		
		this.api_employee = this._conf.ServerWithApiUrl + 'employee/';

		this.filterEmployeeList = _fb.group({
			'name': ['', [Validators.required]],
			'company': ['', [Validators.required]],
			'status': ['', [Validators.required]]
		});
	}

	ngAfterViewInit(){
	}

	ngOnInit() {
		let auth_token = localStorage.getItem('id_token');
		// DataTable Configuration
		this.dtOptions = {
			bFilter: true,
			searching: true,
			ajax: {
				url: this.api_employee,
				type: "GET",
				beforeSend: function(xhr){
                            xhr.setRequestHeader("Authorization",
                            "Bearer " + auth_token);
            	},
	        },
			columns: [
			{
				title: 'ID',
				data: 'id'
			},
			{
				title: 'Employee Num.',
				data: 'employee_code'
			}, {
				title: 'Name',
				data: 'text'
			}, {
				title: 'Company',
				data: 'company'
			}, {
				title: 'Status',
				data: 'status',
				render: function(data, type, row, meta){
					return data == 1 ? 'Active' : 'Inactive';
				}
			}, {
				title: 'Action',
				data: 'id',
				render: function(data, type, row, meta){
					return '<a class="btn btn-edit-employee" href="admin/employee-edit/' + data + '" ><i class="fa fa-eye"></i></a>';
				}
			}],
			rowCallback: (nRow: number, data: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
				let self = this;
				// Unbind first in order to avoid any duplicate handler
				// (see https://github.com/l-lin/angular-datatables/issues/87)

				// $('td', nRow).unbind('click');
				// $('td', nRow).bind('click', () => {
				// 	this._route.navigate(['admin/employee-edit/', data.id]);
				// });

				$('.btn-edit-employee', nRow).unbind('click');
				$('.btn-edit-employee', nRow).bind('click', (e) => {
					e.preventDefault();
					e.stopPropagation();
					this._route.navigate(['admin/employee-edit', data.id]);
				});
				return nRow;
			}
		};
	}

	validateFilter(employeeForm: any, eventEmployeeFilter: any){
		eventEmployeeFilter.preventDefault();
		// this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
			// Destroy the table first
			// dtInstance.destroy();
			// Call the dtTrigger to rerender again
			// this.dtTrigger.next();
		// });
		// console.log(this.dtOptions.ajax.load('core'));
		// console.log(this.dtOptions.ajax.url('yahoo.com'));//.url('asdasd').reload()
	}

	getCompanySelectionFilter(){
		this._commonserv.getCompany().subscribe(
			data => {
				this.setCompanySelectionFilter(data);
			},
			err => console.error(err)
		);
	}

	setCompanySelectionFilter(data: any){
		this.filter_company_field = data;
	}
}
