import { Component, OnInit, Injectable } from '@angular/core';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Configuration } from '../../app.config';
import { EmployeeService } from '../../services/employee.service';
import { CommonService } from '../../services/common.service';

@Component({
	selector: 'admin-employee-audit-list',
	templateUrl: './employee-audit-list.component.html',
	styleUrls: ['./employee-audit-list.component.css']
})

@Injectable()
export class EmployeeAuditListComponent implements OnInit {
	dtOptions: any = {};
	api_employee: String;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	private filter_company_field: any;
	private filter_status_field = [{ 'value':0, text:'Inactive' }, { 'value':1, text:'Active' }];

	filterEmployeeList : FormGroup;

	constructor(private _commonserv: CommonService, private _emp_service: EmployeeService, private _route: Router, private _fb: FormBuilder, private _conf: Configuration){
		this.getCompanySelectionFilter();

		// add the the body classes
		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");
		
		let employee = 'employee';
		this.api_employee = this._conf.ServerWithApiUrl + 'employee/';

		this.filterEmployeeList = _fb.group({
			'name': ['', [Validators.required]],
			'company': ['', [Validators.required]],
			'status': ['', [Validators.required]]
		});
	}

	ngAfterViewInit(){
	}

	ngOnInit() {
		let authToken = localStorage.getItem('id_token');
		// DataTable Configuration
		this.dtOptions = {
			searching: true,
			ajax: {
				url: this.api_employee,
				type: "GET",
				beforeSend: function(xhr){
                            xhr.setRequestHeader("Authorization",
                            "Bearer " + authToken);
            	},
	        },
			columns: [
			{
				title: 'Employee Num.',
				/*data: 'employee_code',*/
				render: function(data, type, row, meta){
					return data == null ? '-N/A-' : data;
				}
			}, {
				title: 'Name',
				data: 'text'
			}, {
				title: 'Current Employer',
				data: 'company'
			}, {
				title: 'Job Title',
				data: 'position'
			}, {
				title: 'Status',
				render: function(data, type, row, meta){
					return data == 1 ? 'Active' : 'Inactive';
				}
			}],
			rowCallback: (nRow: number, data: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
				let self = this;
				/* Unbind first in order to avoid any duplicate handler*/
				/* (see https://github.com/l-lin/angular-datatables/issues/87)*/
				$('td', nRow).unbind('click');
				$('td', nRow).bind('click', () => {
					this._route.navigate(['admin/employee-audit-logs/', data.id]);
				});
				return nRow;
			}
		};
	}

	editemployee(id: any){
		console.log();
	}

	validateFilter(){
		console.log(123123);
		this.dtOptions.ajax.url('asdasd').reload();
	}

	getCompanySelectionFilter(){
		this._commonserv.getCompany().subscribe(
			data => {
				this.setCompanySelectionFilter(data);
			},
			err => console.error(err)
		);
	}

	setCompanySelectionFilter(data: any){
		this.filter_company_field = data;
	}
}
