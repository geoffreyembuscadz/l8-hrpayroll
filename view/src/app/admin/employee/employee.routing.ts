import { Routes, RouterModule } from '@angular/router';

import { EmployeeComponent } from './employee.component';
import { EmployeeListComponent } from './employee-list.component';
import { EmployeeRemarksListComponent } from './employee-remarks-list.component';
import { EmployeeRemarksViewComponent } from './employee-remarks-view.component';
import { EmployeeRemarksCreateComponent } from './employee-remarks-create.component';
import { EmployeeCreateComponent } from './employee-create.component';
import { EmployeeEditComponent } from './employee-edit.component';

const EMPLOYEE_ROUTES: Routes = [
	{ path: 'employee', component: EmployeeComponent, children: [
		{ path: 'employee-list', component: EmployeeListComponent },
	    { path: 'employee-create', component: EmployeeCreateComponent },
	    { path: 'employee-edit/:employee_id', component: EmployeeEditComponent },
	    { path: 'employee-remarks-list', component: EmployeeRemarksListComponent },
	    { path: 'employee-remarks-view/:employee_id', component: EmployeeRemarksViewComponent },
	    { path: 'employee-remarks-create', component: EmployeeRemarksCreateComponent} ]
	}
];

export const EmployeeRouting = RouterModule.forChild(EMPLOYEE_ROUTES);
