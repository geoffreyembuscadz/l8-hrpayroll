var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Employee } from '../../model/employee';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
var URL = '';
var EmployeeRemarksViewComponent = /** @class */ (function () {
    function EmployeeRemarksViewComponent(_conf, _fb, _ar, _companyserv, _rt, _empservice, _zone) {
        var _this = this;
        this._conf = _conf;
        this._fb = _fb;
        this._ar = _ar;
        this._companyserv = _companyserv;
        this._rt = _rt;
        this._empservice = _empservice;
        this._zone = _zone;
        this.route_upload_attachment = 'upload?type=employee_attachment';
        this.route_upload_img = 'upload?type=employee_img';
        this.employee = new Employee();
        this.form_companies = [];
        this.employee_id = this._ar.snapshot.params['employee_id'];
        this._empservice.getEmployee(this.employee_id).subscribe(function (data) {
            _this.setEmployee(data);
        }, function (err) { return console.error(err); });
    }
    EmployeeRemarksViewComponent.prototype.ngOnInit = function () {
        // console.log(this.employee_id);
    };
    EmployeeRemarksViewComponent.prototype.setEmployee = function (data) {
        this.employee = data;
        this.employee_remarks = data.remarks;
    };
    EmployeeRemarksViewComponent = __decorate([
        Component({
            selector: 'admin-employee-remarks-view',
            templateUrl: './employee-remarks-view.component.html',
            styleUrls: ['./employee-remarks-view.component.css']
        }),
        __metadata("design:paramtypes", [Configuration, FormBuilder, ActivatedRoute, CompanyService, Router, EmployeeService, NgZone])
    ], EmployeeRemarksViewComponent);
    return EmployeeRemarksViewComponent;
}());
export { EmployeeRemarksViewComponent };
//# sourceMappingURL=employee-remarks-view.component.js.map