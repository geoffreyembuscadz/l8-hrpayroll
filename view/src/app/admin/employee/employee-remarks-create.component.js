var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/catch';
import { Component, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Employee } from '../../model/employee';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
var URL = '';
var EmployeeRemarksCreateComponent = /** @class */ (function () {
    function EmployeeRemarksCreateComponent(_conf, _fb, _ar, _companyserv, _rt, _empservice, _zone) {
        this._conf = _conf;
        this._fb = _fb;
        this._ar = _ar;
        this._companyserv = _companyserv;
        this._rt = _rt;
        this._empservice = _empservice;
        this._zone = _zone;
        this.route_upload_attachment = 'upload?type=employee_attachment';
        this.route_upload_img = 'upload?type=employee_img';
        this.employee = new Employee();
        this.form_companies = [];
        this.getCompanies();
        this.createEmployeeRemarksForm = _fb.group({
            'company_id': ['', [Validators.required]],
            'employee_id': ['', [Validators.required]],
            'remarks': ['', [Validators.required]]
        });
    }
    EmployeeRemarksCreateComponent.prototype.ngOnInit = function () {
    };
    EmployeeRemarksCreateComponent.prototype.getCompanies = function () {
        var _this = this;
        this._empservice.getCompanies().subscribe(function (data) {
            _this.setCompanies(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeRemarksCreateComponent.prototype.setCompanies = function (data) {
        var companies = data;
        var fetched_companies = [];
        for (var _i = 0; _i < companies.length; _i++) {
            this.form_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
        }
    };
    EmployeeRemarksCreateComponent.prototype.validateCreation = function () {
        var _this = this;
        var employee_model = this.createEmployeeRemarksForm.value;
        // console.log(employee_model);
        this._empservice.addEmployeeRemark(employee_model).subscribe(function (data) {
            _this.postStore(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeRemarksCreateComponent.prototype.postStore = function (data) {
        var _this = this;
        this.post_data = data;
        this.error_title = null;
        this.error_message = null;
        this.success_title = 'New Remark Added!';
        this.success_message = 'You have successfully added a new remark.';
        setTimeout(function () {
            _this._rt.navigateByUrl('admin/employee-remarks-list');
        }, 4000);
    };
    EmployeeRemarksCreateComponent.prototype.companySelectionChange = function () {
        var _this = this;
        var company_id = this.createEmployeeRemarksForm.get('company_id').value;
        this._empservice.getEmployees('company_id=' + company_id).subscribe(function (data) {
            _this.setEmployeesSelection(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeRemarksCreateComponent.prototype.setEmployeesSelection = function (data) {
        this.form_employees = data;
        console.log(this.form_employees);
    };
    EmployeeRemarksCreateComponent.prototype.getEmployeeByCompany = function (company_id) {
    };
    EmployeeRemarksCreateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
            window.scrollTo(0, 0);
        }
    };
    EmployeeRemarksCreateComponent.prototype.clearErrorMsg = function () {
        this.error_title = '';
        this.error_message = '';
    };
    EmployeeRemarksCreateComponent = __decorate([
        Component({
            selector: 'admin-employee-remarks-create',
            templateUrl: './employee-remarks-create.component.html',
            styleUrls: ['./employee-remarks-create.component.css']
        }),
        __metadata("design:paramtypes", [Configuration, FormBuilder, ActivatedRoute, CompanyService, Router, EmployeeService, NgZone])
    ], EmployeeRemarksCreateComponent);
    return EmployeeRemarksCreateComponent;
}());
export { EmployeeRemarksCreateComponent };
//# sourceMappingURL=employee-remarks-create.component.js.map