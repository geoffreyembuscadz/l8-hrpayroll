import { Router } from '@angular/router';
import { Component, OnInit, Injectable, NgZone } from '@angular/core';
import { EmployeeMassUploadForm } from '../../model/employee_mass_upload_form';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';

import { NgUploaderOptions, UploadedFile, UploadRejected } from 'ngx-uploader';
import { Subject } from 'rxjs/Rx';
import { Configuration } from '../../app.config';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { CommonService } from '../../services/common.service';

@Component({
	selector: 'admin-employee-mass-upload',
	templateUrl: './employee-mass-upload.component.html',
	styleUrls: ['./employee-mass-upload.component.css']
})

@Injectable()
export class EmployeeMassUploadComponent implements OnInit {
	api_employee: String;
	private post_data: any;
	private success;
	private route_upload_attachment = 'upload?type=employee_mass_upload';
	private formEmployeeMassUpload: FormGroup;
	private employee_mass_upload = new EmployeeMassUploadForm();
	private success_title: any;
	private success_message: any;
	private error_title: any;
	private error_message: any;
	private has_error = 0;
	
	public form_genders: any;
	public form_suffixes = [];
	public form_companies = [];
	public form_job_titles = [];
	public form_departments = [];
	public form_marital_status = [];
	public form_company_branches = [];
	public form_employment_status = [];
	public form_show_loading_status = 0;
	public form_show_download_import = 0;
	public form_company_selected_import: any;
	public form_company_branches_export = [];
	public form_company_branch_selected_import: any;
	public form_employee_export_csv_fields: any;

	public options: NgUploaderOptions;
	public optionsCsv: NgUploaderOptions;
	public response: any;
	public response_csv: any;
	public sizeLimit: number = 1000000;
	public hasBaseDropZoneOver: boolean;
	public employee_response_upload = [];

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	filterEmployeeList : FormGroup;
	exportEmployeeForm: FormGroup;

	constructor(private _commonserv: CommonService, private _company_service: CompanyService, private _emp_service: EmployeeService, private _route: Router, private _fb: FormBuilder, private _conf: Configuration, private _zone: NgZone){
		//add the the body classes
		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");
		
		let employee = 'employee';
		this.error_message = null;
		this.api_employee = this._conf.ServerWithApiUrl + 'employee/';
		this.form_employee_export_csv_fields = [
			{ name: "employee_code", selected: true },
			{ name: "firstname", selected: true },
			{ name: "middlename", selected: true },
			{ name: "lastname", selected: true },
			{ name: "company", selected: true },
			{ name: "branch", selected: true },
			{ name: "gender", selected: true },
			{ name: "birthdate", selected: true },
			{ name: "e_cola", selected: true },
			{ name: "salary_receiving_type", selected: true },
			{ name: "rate", selected: true },
			{ name: "allowance_amount", selected: true },
			
			{ name: "sss", selected: true },
			{ name: "philhealth_no", selected: true },
			{ name: "pagibig_no", selected: true },
			{ name: "tin_no", selected: true },
			{ name: "nationality", selected: true },
			{ name: "cel_no", selected: true },
			{ name: "starting_date", selected: true },
			{ name: "current_address", selected: true },
			{ name: "status", selected: true },

			{ name: "position_type", selected: null },
			{ name: "suffix", selected: null },
			{ name: "department", selected: null },
			{ name: "position", selected: null },
			{ name: "skills", selected: null },
			{ name: "blood_type", selected: null },
			{ name: "citizenship", selected: null },
			{ name: "driving_license_no", selected: null },
			{ name: "hdmf_no", selected: null },
			{ name: "supervisor_id", selected: null },
			{ name: "marital_status", selected: null },
			{ name: "tel_no", selected: null },
			{ name: "email", selected: null },
			{ name: "employment_status", selected: null },
			{ name: "date_hired", selected: null },
			{ name: "regularization_date", selected: null },
			{ name: "end_date", selected: null },
			{ name: "bank_name", selected: null },
			{ name: "bank_code", selected: null },
			{ name: "bank_account_number", selected: null },
			{ name: "permanent_address", selected: null },
			{ name: "province_address", selected: null },
			{ name: "employee_image", selected: null },
			{ name: "date_terminated", selected: null },
			{ name: "sick_leave_credit", selected: null },
			{ name: "use_sick_leave_credit", selected: null },
			{ name: "vacation_leave_credit", selected: null },
			{ name: "use_vacation_leave_credit", selected: null },
			{ name: "emergency_leave_credit", selected: null },
			{ name: "use_emergency_leave_credit", selected: null },
			{ name: "maternity_leave_credit", selected: null },
			{ name: "use_maternity_leave_credit", selected: null },
			{ name: "others_leave_credit", selected: null },
			{ name: "use_others_leave_credit", selected: null }];

		this.employee_mass_upload.mass_upload_fields = [
			"employee_code",
			"firstname", 
			"middlename", 
			"lastname", 
			"company", 
			"branch", 
			"gender", 
			"birthdate", 
			"nationality", 
			"sss", 
			"philhealth_no", 
			"pagibig_no", 
			"tin_no", 
			"e_cola", 
			"salary_receiving_type", 
			"rate", 
			"allowance_amount", 
			"status", 
			"current_address", 
		];

		this.getCompanies();
		this.getDepartments();
		this.getJobTitles();
		this.form_genders = this._emp_service.getGenders();
		this.form_suffixes = this._emp_service.getSuffixes();
		this.form_marital_status = this._emp_service.getEmployeeMaritalStatuses();
		this.form_employment_status = this._emp_service.getEmploymentStatus();

		// uploader ngzone
	    this.optionsCsv = new NgUploaderOptions({
	    	url: this._conf.ServerWithApiUrl + this.route_upload_attachment,
	    	autoUpload: true,
	    	calculateSpeed: true,
	    	filterExtensions: true,
	    	allowedExtensions: ['csv']
	    });

	    this.exportEmployeeForm = _fb.group({
	    	'primary_company': ['', [Validators.required]],
	    	'company_branch': ['', [Validators.required]]
	    });
	}

	ngOnInit() {
	}

	ngAfterViewInit() {
	}

	private getDepartments(){
		this._emp_service.getDepartments().subscribe(
			data => {
				this.setDepartments(data);
			},
			err => this.catchError(err)
		);
	}

	private setDepartments(data: any){
		let departments = [];
		data.forEach((values, index) => {
			departments[index] = { id: values.id, name: values.name };
		});

		this.form_departments = departments;
		console.log(this.form_departments);
	}

	private getCompanies(){
		this._emp_service.getCompanies().subscribe(
			data => {
				this.setCompanies(data);
			},
			err => this.catchError(err)
		);
	}

	private setCompanies(data: any){
		let companies = data;
		let fetched_companies = [];

		for(var _i = 0; _i < companies.length; _i++){
			fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
		}

		this.form_companies = fetched_companies;
	}

	private primaryCompanyChangedExport(selected_company: any){
		let company_id = selected_company.value;
		this.form_company_selected_import = company_id;

        this._company_service.getCompany(company_id).subscribe(
        	data => {
        		this.setCompanyBranchesExport(data.branches);
        		
        	}
        );
	}

	private displayDownloadFormButton(){
		this.form_show_download_import = 1;
	}

	private primaryCompanyBranchChanged(company_branch: any){
		let select_company_branch = company_branch;
		this.form_company_branch_selected_import = select_company_branch;
		console.log(this.form_company_branch_selected_import);
		
	}

	private setCompanyBranchesExport(company_branches: any){
		let data = [];
		let index = 0;

		this.form_company_branches = [];

		for(let company_branch of company_branches){
			data[index] = company_branch;
			index++;
		}

		this.form_company_branches_export = data;
	}

	downloadUploadForm(data: any, valid: any) {
		let form_valid = valid;
		let company = this.employee_mass_upload.company;
		let company_branch = this.employee_mass_upload.company_branch;
		let selected_fields = this.employee_mass_upload.mass_upload_fields;

		console.log([company, company_branch, selected_fields]);

		if(form_valid){
			this._emp_service.exportEmployeeMassUploadform(company, company_branch, selected_fields);
		} else {
			alert('Please Select a Company, Branch & Fields for the upload form.');
		}
		
	}

	private primaryCompanyChanged(){
		let company_id = this.exportEmployeeForm.get('primary_company').value;

        this._company_service.getCompany(company_id).subscribe(
        	data => {
        		this.setCompanyBranches(data.branches);
        		
        	}
        );
	}

	private exportEmployeeListReport(){
		let form_data = this.exportEmployeeForm.value;

		this._emp_service.exportEmployeeList(form_data);
	}

	private setCompanyBranches(company_branches: any){
		let data = [];
		let index = 0;

		this.form_company_branches = [];

		for(let company_branch of company_branches){
			data[index] = company_branch;
			index++;
		}

		this.form_company_branches = data;
	}

	private getJobTitles(){
		this._emp_service.getJobTitles().subscribe(
			data => {
				this.fetchJobTitles(data);
			},
			err => this.catchError(err)
		);
	}

	private fetchJobTitles(data: any){
		let job_titles = [];
		data.forEach((values, index) => {
			job_titles[index] = { id: values.id, name: values.name };
		});

		this.form_job_titles = job_titles;
	}

	consoleFunc(){
		console.log(123);
	}

	resetSelectionFields(){
		// Reset Selection fields
		this.form_employee_export_csv_fields = [
			{ name: "employee_code", selected: true },
			{ name: "firstname", selected: true },
			{ name: "middlename", selected: true },
			{ name: "lastname", selected: true },
			{ name: "company", selected: true },
			{ name: "branch", selected: true },
			{ name: "gender", selected: true },
			{ name: "birthdate", selected: true },
			{ name: "e_cola", selected: true },
			{ name: "salary_receiving_type", selected: true },
			{ name: "rate", selected: true },
			{ name: "allowance_amount", selected: true },
			
			{ name: "sss", selected: true },
			{ name: "philhealth_no", selected: true },
			{ name: "pagibig_no", selected: true },
			{ name: "tin_no", selected: true },
			{ name: "nationality", selected: true },
			{ name: "cel_no", selected: true },
			{ name: "starting_date", selected: true },
			{ name: "current_address", selected: true },
			{ name: "status", selected: true },

			{ name: "position_type", selected: null },
			{ name: "suffix", selected: null },
			{ name: "department", selected: null },
			{ name: "position", selected: null },
			{ name: "skills", selected: null },
			{ name: "blood_type", selected: null },
			{ name: "citizenship", selected: null },
			{ name: "driving_license_no", selected: null },
			{ name: "hdmf_no", selected: null },
			{ name: "supervisor_id", selected: null },
			{ name: "marital_status", selected: null },
			{ name: "tel_no", selected: null },
			{ name: "email", selected: null },
			{ name: "employment_status", selected: null },
			{ name: "date_hired", selected: null },
			{ name: "regularization_date", selected: null },
			{ name: "end_date", selected: null },
			{ name: "bank_name", selected: null },
			{ name: "bank_code", selected: null },
			{ name: "bank_account_number", selected: null },
			{ name: "permanent_address", selected: null },
			{ name: "province_address", selected: null },
			{ name: "employee_image", selected: null },
			{ name: "date_terminated", selected: null },
			{ name: "sick_leave_credit", selected: null },
			{ name: "use_sick_leave_credit", selected: null },
			{ name: "vacation_leave_credit", selected: null },
			{ name: "use_vacation_leave_credit", selected: null },
			{ name: "emergency_leave_credit", selected: null },
			{ name: "use_emergency_leave_credit", selected: null },
			{ name: "maternity_leave_credit", selected: null },
			{ name: "use_maternity_leave_credit", selected: null },
			{ name: "others_leave_credit", selected: null },
			{ name: "use_others_leave_credit", selected: null }];
	}

	beforeUploadCsv(uploadingFile: UploadedFile){
		this.form_show_loading_status = 1;
		if (uploadingFile.size > this.sizeLimit){
			uploadingFile.setAbort();console.error('File is too large!');
  		}
	}

	handleCsvUpload(data: any){
		this._zone.run(() => {
			this.response_csv = data;
			if(data && data.response){
				this.response_csv = JSON.parse(data.response);
				
				console.log(this.response_csv); // dito 
				this.renderEmployeeUpload(this.response_csv);
			}
		});
	}

	handleUpload(data: any) {
	    this._zone.run(() => {
	    	this.response = data;
	    	if (data && data.response) {
	    		this.response = JSON.parse(data.response);

	    		let employee_id = this.post_data.id;
				let employee_attachment = this.response.file_url;
				let employee_attachment_dir = this.response.file_dir;

				this._emp_service.attachEmployeeFiles(employee_id, employee_attachment, employee_attachment_dir).subscribe(
					data => {
						console.log(1);
					},
					err => this.catchError(err)
				);
	    	}
	    });
	}

	renderEmployeeUpload(data: any){
		let form_data = { 'filename': data.filename, 'file_dir': data.file_dir };

		this._emp_service.uploadMassEmployeeCsv(form_data).subscribe(
			data => {
				this.form_show_loading_status = 0;

				this.renderErrorMessages(data.error);
				this.renderEmployeeUploaded(Array.from(data));

				setTimeout(() => {
					this._route.navigateByUrl('admin/employee-list');

				}, 3000);
			},
			err => {
				this.error_title = "Oops!";
	    		this.error_message = "It seems there's a problem in the employee upload. Be sure that the filetype is a CSV file or report this biuug.";
	    		this.form_show_loading_status = 0;
			}
		);
	}

	private renderErrorMessages(data: any){
		this.error_message = false;
		this.has_error = 0;

		if(data.error_msg != null){
			this.has_error = 1;
			this.error_message = data.error_msg;
			console.log(this.error_message);
		} else {
			this.has_error = 0;
			this.error_message = null;
			this.success_title = "Success!";
			this.success_message = "A new employee record was successfully added & updated the existing records.";


		}
	}

	private renderEmployeeUploaded(data: any){
		this.employee_response_upload = data;

		console.log(this.employee_response_upload);
	}

	validateFilter(employeeForm: any, eventEmployeeFilter: any) {
	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
			window.scrollTo(0, 0);
		}
	}
}
