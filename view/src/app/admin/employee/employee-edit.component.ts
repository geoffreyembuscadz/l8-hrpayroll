import 'rxjs/add/operator/catch';
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Select2Module } from 'ng2-select2';

import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { NgUploaderOptions, UploadedFile, UploadRejected } from 'ngx-uploader';


import { Configuration } from '../../app.config';
import { Employee } from '../../model/employee';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { CommonService } from '../../services/common.service';

const URL = '';

@Component({
  selector: 'admin-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})

export class EmployeeEditComponent implements OnInit {
	public id: string;
	public error_title: string;
	public error_message: string;
	public success_title;

	private user_thumbnail_employee = 'assets/img/user-placeholder.png';

	public success_message;
	private route_upload_attachment = 'upload?type=employee_attachment';
	private route_upload_img = 'upload?type=employee_img';
	public msg_terminate_title = null;
	public msg_terminate_msg = null;

	public poststore: any;
	public employee = new Employee();

	public form_genders: any;
	public form_suffixes: any;
	public form_job_titles = [];
	public form_blood_types: any;
	public form_departments = [];
	public form_skills: any;
	public form_companies: any;
	public form_company_branches: any;
	public form_marital_statuses: any;
	public form_supervisors: any;
	public form_employee_dependent = [{ index:0 }];
	public form_employment_status: any;
	public form_working_schedules: any;
	private form_confirm_archive = false;
	private form_confirm_terminate = false;
	public form_employee_dependency_row = 0;
	private form_validation_errors = [];
	public selectedEmployeeSchedOptions: any;
	public selectedEmployeeSkillsOptions: any;
	public selectedEmployeeSupervisorsOptions: any;
	public form_employee_bank_name: any;

	public form_default_user_placeholder = '';
	public form_income_type: any;
	public form_user_image = '';
	
	public form_government_fields = [
        { code: 'SSS', label: 'SSS No.' },
        { code: 'PAGIBIG', label: 'Pag-ibig' },
        { code: 'PHILHEALTH', label: 'PhilHealth No.' },
        { code: 'TIN', label: 'TIN No.' }
    ];
    public errorUploadImageMessage = '';
    public errorUploadAttachmentMessage = '';
    public fetched_company_schedules = [];
	public selected_company_schedule = { days: [] };

	public options: NgUploaderOptions;
	public optionsImg: NgUploaderOptions;
	public response: any;
	public response_image: any;
	public sizeLimit: number = 1000000;
	public hasBaseDropZoneOver: boolean;

	updateEmployeeForm : FormGroup;
	updateEmergencyContacts: FormGroup;
	updateEmployeeDependencies: FormGroup;

	public selectTwoWorkingScheduleOptions = {
		multiple: true
	};

	public onboarding=[];
	checkAllValOnboarding = false;

	/*
	form_positions: any;
	form_position_count: number;
	*/

	constructor(
		private _conf: Configuration, 
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute, 
		private _company_service: CompanyService, 
		private _empservice: EmployeeService, 
		private _rt: Router, 
		private _zone: NgZone,
   		private _common_service: CommonService
		){
		this.getSuperVisors();
		this.getCompanies();
		this.getDepartments();
		this.getJobTitles();

		this.id = this._ar.snapshot.params['employee_id'];
		this._empservice.getEmployee(this.id).subscribe(
			data => {
				this.setEmployee(data);
			},
			err => console.error(err)
		);
		
		this.form_genders = this._empservice.getGenders();
		this.form_suffixes = this._empservice.getSuffixes();
		this.form_skills = this._empservice.getEmployeeSkills();
		this.form_blood_types = this._empservice.getBloodTypes();
		this.form_income_type = this._empservice.getIncomeType();
		this.form_employee_bank_name = this._empservice.getBanks();
		this.form_marital_statuses = this._empservice.getEmployeeMaritalStatuses();
		this.form_employment_status = this._empservice.getEmploymentStatus();

		this.updateEmergencyContacts = _fb.group({
			'name[0]': '',
			'relationship[0]': '',
			'contact_number[0]': ''
		});

		this.updateEmployeeDependencies = _fb.group({
			'employee_dependent_name[0]': ['', [Validators.required]],
			'employee_dependent_age[0]': ['', [Validators.required]],
			'employee_dependent_relationship[0]': ['', [Validators.required]],
			'employee_dependent_pwd[0]': ['', [Validators.required]]
		});

		this.updateEmployeeForm = _fb.group({
			'firstname': [this.employee.firstname, [Validators.required]],
			'middlename': [this.employee.middlename, [Validators.required]],
			'lastname': [this.employee.lastname, [Validators.required]],
			'suffix': [this.employee.suffix, [Validators.required]],
			'gender': [this.employee.gender, [Validators.required]],
			'birthdate': [this.employee.birthdate, [Validators.required]],
			'blood_type': [this.employee.blood_type, [Validators.required]],
			'citizenship': [this.employee.citizenship, [Validators.required]],
			'employee_code': [this.employee.employee_code, [Validators.required]],
			'department': [this.employee.department, [Validators.required]],
			'position': [this.employee.position, [Validators.required]],
			'tel_no': [this.employee.tel_no, [Validators.required]],
			'cel_no': [this.employee.cel_no, [Validators.required]],
			'email': [this.employee.email, [Validators.required]],
			'primary_company': [this.employee.primary_company, [Validators.required]],
			'company_branch': [this.employee.company_branch, [Validators.required]],
			'skills': [this.employee.skills, [Validators.required]],
			'marital_status': [this.employee.marital_status, [Validators.required]],
			'starting_date': [this.employee.starting_date, [Validators.required]],
			'ending_date': [this.employee.ending_date, [Validators.required]],
			'salary_receiving_type': [this.employee.salary_receiving_type, [Validators.required]],
			'income_rate': [this.employee.income_rate, [Validators.required]],
			'allowance_amount': [this.employee.allowance_amount, [Validators.required]],
			'current_address': [this.employee.current_address, [Validators.required]],
			'province_address': [this.employee.province_address, [Validators.required]],
			'nationality': ['Filipino', [Validators.required]],
	    	'employment_status': [this.employee.employment_status, [Validators.required]],
	    	'driving_license_no': [this.employee.driving_license_no, [Validators.required]],
	    	'sss': [this.employee.sss, [Validators.required]],
	    	'pagibig_no': [this.employee.pagibig_no, [Validators.required]],
	    	'philhealth_no': [this.employee.philhealth_no, [Validators.required]],
	    	'tin_no': [this.employee.tin_no, [Validators.required]],
	    	'supervisor_id': this.employee.supervisor_id,
	    	'employee_image': [this.employee.employee_image, [Validators.required]],
	    	'sick_leave_credit': ['', [Validators.required]],
	    	'e_cola': [this.employee.e_cola, [Validators.required]],
	    	'bank_name': [this.employee.bank_name, [Validators.required]],
	    	'bank_code': [this.employee.bank_code, [Validators.required]],
	    	'bank_account_number': [this.employee.bank_account_number, [Validators.required]],
	    	'vacation_leave_credit': [this.employee.vacation_leave_credit, [Validators.required]],
	    	'emergency_leave_credit': [this.employee.emergency_leave_credit, [Validators.required]],
	    	'maternity_leave_credit': [this.employee.maternity_leave_credit, [Validators.required]],
	    	'others_leave_credit': [this.employee.others_leave_credit, [Validators.required]],
	    	'working_schedule': [this.employee.working_schedule, [Validators.required]],
	    	'ip_address': [this.employee.ip_address, [Validators.required]],
	    	'mac_address': [this.employee.mac_address, [Validators.required]],
	    	'ddns': [this.employee.ddns, [Validators.required]],
	    	'onboarding': [null]
		});

		// uploader ngzone
	    this.options = new NgUploaderOptions({
	    	url: this._conf.ServerWithApiUrl + this.route_upload_attachment,
	    	autoUpload: true,
	    	calculateSpeed: true
	    });

	    this.optionsImg = new NgUploaderOptions({
	    	url: this._conf.ServerWithApiUrl + this.route_upload_img,
	    	autoUpload: true,
	    	calculateSpeed: true,
	    	filterExtensions: true,
	    	allowedExtensions: ['jpg', 'png']
	    });

	    if(this.employee.date_terminated != null){
	    	this.msg_terminate_title = 'Inactive Record';
			this.msg_terminate_msg = 'This employee has been terminated.';
	    }
	}

	public onchangeSkills(select_options: any){
		this.selectedEmployeeSkillsOptions = Array.apply(null,select_options)
			.filter(option => option.selected)
			.map(option => option.value);

		this.employee.skills = this.selectedEmployeeSkillsOptions;
	}

	public onchangeSupervisor(select_options: any){
		this.selectedEmployeeSupervisorsOptions = Array.apply(null,select_options)
			.filter(option => option.selected)
			.map(option => option.value);

		this.employee.supervisor_id = this.selectedEmployeeSupervisorsOptions;
	}

	public changeSelectEmployeeWorkingSchedule(select_options: any){
		this.selectedEmployeeSchedOptions = Array.apply(null,select_options)
			.filter(option => option.selected)
			.map(option => option.value);

		this.employee.working_schedule = this.selectedEmployeeSchedOptions;
	}

	public validateUpdate(): void {
		/* for multiple select to get the values.
		No Support for catching values using ngModel & formControl for select multiple on Angular 2. */
		this.updateEmployeeForm.get('working_schedule').setValue(this.employee.working_schedule);

		this.onboardingCheck();

		let employee_model = this.updateEmployeeForm.value;
		let employee_model_keys = Object.keys(employee_model);
		let error_fields = {};
		let required_fields = [ 'firstname', 'middlename', 'lastname', 'income_rate', 'working_schedule', 'employment_status' ];

		for(let index = 0; index < employee_model_keys.length; index++){
			if( required_fields.indexOf(employee_model_keys[index]) > -1 && employee_model[employee_model_keys[index]] == ''){
				error_fields[employee_model_keys[index]] = "Income Rate field is required.";
			}
		}
	
		if(Object.keys(error_fields).length == 0){
			this._empservice.updateEmployee(this.id, employee_model).subscribe(
				data => {
					this.poststore = Array.from(data);
					this.success_title = "Success!";
					this.success_message = "An employee record was successfully updated.";

					this.error_title = null;
					this.error_message = null;

				},
				err => function(err){
					this.error_title = 'Error';
					this.error_message = err;
				}
			);
		} else {
			let error_message = Object.keys(error_fields).join();
			error_message = error_message.replace(/_/g,' ');
			error_message = error_message.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});

			this.error_title = 'Oops!';
			this.error_message = 'Seems like there\'s a field you missed. Please check the following fields: ' + error_message;
		}
		
	}

	private catchError(error: any){
	}

	public setEmployee(emp: any){

		this.id = emp.id;
		this.employee = emp;
		this.employee.skills = ( emp.skills != null ) ? emp.skills.split(',') : [];
		this.employee.supervisor_id = JSON.parse(emp.supervisor_id);

		let working_schedules = [];
		let fetch_employee_working_schedules = emp.working_schedule;
		
		this.employee.working_schedule = fetch_employee_working_schedules;

		if(this.employee.employee_image != null || this.employee.employee_image != ''){
			this.form_user_image = this.employee.employee_image;
			this.form_default_user_placeholder = this.employee.employee_image;
		} else if(this.employee.employee_image == ''){
			this.form_user_image = this._conf.frontEndUrl + this.user_thumbnail_employee;
			this.form_default_user_placeholder = this._conf.frontEndUrl + this.user_thumbnail_employee;
		}

		let employee_emergency_contacts = this.employee.emergency_contacts;

		for(let _index_employee_emergency_contacts = 0; _index_employee_emergency_contacts < employee_emergency_contacts.length; _index_employee_emergency_contacts++){
			this.updateEmergencyContacts.addControl('name[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].person_name);
			this.updateEmergencyContacts.addControl('relationship[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].relationship);
			this.updateEmergencyContacts.addControl('contact_number[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].contact_number);
		}

		if(this.employee.primary_company != null){
			this._company_service.getCompany(this.employee.primary_company).subscribe(
	            data => {
	            	this.setCompanyBranches(data.branches);
	                this.setCompanyPolicies(data.policies);
	                this.setCompanyGovernmentFields(data.emp_govrn_fields);
	                this.setCompanySchedulesSelection(data.schedules);
	            },
	            err => console.error(err)
	        );
		}

		if(this.form_user_image == '0'){
			this.form_user_image = this._conf.frontEndUrl + this.user_thumbnail_employee;
		}
		
		this.getOnBoarding(emp.onboarding);


		this.employee.ip_address = emp.user_time_setting.ip_address;
		this.employee.mac_address = emp.user_time_setting.mac_address;
		this.employee.ddns = emp.user_time_setting.ddns;



	}

	primaryCompanyChanged(){
		let company_id = this.updateEmployeeForm.get('primary_company').value;

        this._company_service.getCompany(company_id).subscribe(
        	data => {
        		this.setCompanyBranches(data.branches);
        		this.setCompanyPolicies(data.policies);
        		this.setCompanySchedulesSelection(data.schedules);
        	}
        )
	}

	setCompanyBranches(data: any){
		let company_branches = [];
		let index = 0;

		for(let company_branch of data){
			company_branches[index] = company_branch;
			index++;
		}

		this.form_company_branches = company_branches;
	}

	changeBranchAndSched(company_id: any){
		this.primaryCompanyChanged();
		this.changeCompanyBranch(company_id);
	}

	changeCompanyBranch(company_id: any){
		this._company_service.getCompany(company_id).subscribe(
            data => {
            	this.setCompanyBranches(data.branches);
            },
            err => console.error(err)
        );
	}

	setCompanySchedulesSelection(data: any){
		let index = 0;
		let schedule_selection = [];
		let employee_assigned_schedule = this.employee.working_schedule;

		this.form_working_schedules = [];
		//someArray.indexOf("Some entry in array") > -1

		for(let sched of data){
			let days_in_sched = JSON.parse(sched.days);
			let selected_schedule = null;
			let schedule_approved = null;

			if(employee_assigned_schedule.indexOf( sched.id ) > -1){
				selected_schedule = true;
			}

			if(employee_assigned_schedule.indexOf( sched.approved ) > -1){
				schedule_approved = true;
			}

			schedule_selection[index] = { id: sched.id, name: sched.name, days: days_in_sched, time_in: sched.time_in, time_out: sched.time_out, selected_sched: selected_schedule, approved: schedule_approved };
			// schedule_selection[index] = { id: sched.id, text: sched.name + ' - Days:' + days_in_sched + ' - Time-in:' + sched.time_in + ' - Time-out' + sched.time_out };

			this.fetched_company_schedules[sched.id] = sched;
			index++;
		}

		this.form_working_schedules = schedule_selection;
	}

	setCompanyPolicies(data: any){
	}

	setCompanyGovernmentFields(data: any){
	}

	checkIfScheduleIsIncluded(schedule_id: any){
		let employee_working_schdules = this.employee.working_schedule;

		for(let employee_working_sched_id of employee_working_schdules){
			if(employee_working_sched_id == schedule_id){
				return true;
			}
		}
		return false;
	}

	renderScheduleData(schedule_id: any){
		this.selected_company_schedule = this.fetched_company_schedules[schedule_id];
		this.selected_company_schedule.days = JSON.parse(this.fetched_company_schedules[schedule_id].days);
	}

	private clearErrorMsg(){
		this.error_title = '';
		this.error_message = '';

		this.success_title = '';
		this.success_message = '';
	}

	public archiveEmployeeRecord(){
		this.form_confirm_archive = true;
	}

	public cancelTerminationOfEmployee(event: Event){
		this.form_confirm_terminate = false;
	}

	ngAfterViewInit(){
	}

	getAttachments(){
		let employee_id = this.id;

		this._empservice.getAttachments(employee_id).subscribe(
			data => {
				this.employee.attachments = data;
			},
			err => this.catchError(err)
		);
	}

	downloadAttachment(file_url: any){
		window.open(file_url);
	}


	deleteAttachment(attachment_id: any){
		let employee_model = this.updateEmployeeForm.value;

		this._empservice.deleteEmployeAttachment(attachment_id).subscribe(
			data => {
				this.getAttachments();

			},
			err => this.catchError(err)
		);
	}


	ngOnInit(){}

	handleImgUpload(data: any){
		this._zone.run(() => {
			this.response_image = data;
			if(data && data.response){
				this.response_image = JSON.parse(data.response);
				this.form_user_image = this.response_image.file_url;

				this.updateEmployeeForm.get('employee_image').setValue(this.form_user_image);
			}
		});
	}

	handleUpload(data: any) {
	    this._zone.run(() => {
	    	this.response = data;
	    	if (data && data.response) {
	    		this.response = JSON.parse(data.response);

	    		let employee_id = this.id;
				let employee_attachment = this.response.file_url;
				let employee_attachment_dir = this.response.file_dir;

				this._empservice.attachEmployeeFiles(employee_id, employee_attachment, employee_attachment_dir).subscribe(
					data => {
						this.getAttachments();
					},
					err => this.catchError(err)
				);
	    	}
	    });
	}

	beforeUploadImg(uploadingFile: UploadedFile){
		if (uploadingFile.size > this.sizeLimit){
			uploadingFile.setAbort();
      		this.errorUploadImageMessage = 'File is too large!';
  		}
	}

	beforeUploadAttachments(uploadingFile: UploadedFile){
		if (uploadingFile.size > this.sizeLimit){
			uploadingFile.setAbort();
      		this.errorUploadAttachmentMessage = 'File is too large!';
  		}
	}

	confirmArchiveEmployee(event: Event){
		event.preventDefault();
		this.form_confirm_archive = true;
	}

	confirmTermination(event: Event){
		event.preventDefault();
		this.form_confirm_terminate = true;
	}

	archiveEmployee(event: Event){
		event.preventDefault();
		let employee_id = this.id;

		this._empservice.archiveEmployee(employee_id).subscribe(
			data => {
				//this.setEmployee(data);
				alert('Record is successfully archived.');
				this._rt.navigateByUrl('admin/employee-list');
			},
			err => console.error(err)
		);
	}

	cancelArchiveOfRecord(event: Event){
		event.preventDefault();
		this.form_confirm_archive = false;
	}

	terminateEmployee(employee_id: any, event: Event){
		event.preventDefault();

		this._empservice.updateEmployee(employee_id, { id: this.id, date_terminated: 1 }).subscribe(
			data => {
				//this.setEmployee(data);
				alert('Employee Record is successfully marked as terminated.');
				this._rt.navigateByUrl('admin/employee-list');
			},
			err => console.error(err)
		);
	}

	onChangeDepartment(department_id: any){
		if(department_id != null){
			this.getJobTitles();
		}
	}

	private getDepartments(){
		this._empservice.getDepartments().subscribe(
			data => {
				this.setDepartments(data);
			},
			err => this.catchError(err)
		);
	}

	private setDepartments(data: any){
		let departments = [];
		data.forEach((values, index) => {
			departments[index] = { id: values.id, name: values.name };
		});

		this.form_departments = departments;
	}

	private getJobTitles(){
		this._empservice.getJobTitles().subscribe(
			data => {
				this.fetchJobTitles(data);
			},
			err => this.catchError(err)
		);
	}

	private fetchJobTitles(data: any){
		let job_titles = [];
		data.forEach((values, index) => {
			job_titles[index] = { id: values.id, name: values.name };
		});

		this.form_job_titles = job_titles;
	}

	private getSuperVisors(){
		this._empservice.getEmployees().subscribe(
			data => {
				this.setSuperVisors(data);
			},
			err => this.catchError(err)
		);
	}

	private addDependencyRow(){
		this.form_employee_dependency_row++;
		let index_count = this.form_employee_dependency_row;
		let counter = this.form_employee_dependent.length;//(this.form_emergency_contact.length > 1) ? (this.form_emergency_contact.length - 1) : this.form_emergency_contact.length;

		this.updateEmployeeDependencies.addControl('employee_dependent_name[' + index_count + ']', new FormControl());
		this.updateEmployeeDependencies.addControl('employee_dependent_age[' + index_count + ']', new FormControl());
		this.updateEmployeeDependencies.addControl('employee_dependent_relationship[' + index_count + ']', new FormControl());
		this.updateEmployeeDependencies.addControl('employee_dependent_pwd[' + index_count + ']', new FormControl());

		this.form_employee_dependent.push({ index:index_count });
	}

	private setSuperVisors(data: any){
		let supervisors = data;
		let fetched_supervisors = [];

		for(var _i = 0; _i < supervisors.length; _i++){
			fetched_supervisors[_i] = { id: supervisors[_i].id, name: supervisors[_i].lastname + ', ' + supervisors[_i].firstname + ' - Employee Code: ' + supervisors[_i].employee_code };
		}

		this.form_supervisors = fetched_supervisors;
	}

	private getCompanies(){
		this._empservice.getCompanies().subscribe(
			data => {
				this.setCompanies(data);
			},
			err => this.catchError(err)
		);
	}

	private getCompanyBranches(){
		// this._company_service.getCompanyBranchesByCompany(this.employee.primary_company).subscribe(
		// 	data => {
				
		// 	}
		// );
	}

	private setCompanies(data: any){
		let companies = data;
		let fetched_companies = [];

		for(var _i = 0; _i < companies.length; _i++){
			fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
		}

		this.form_companies = fetched_companies;
	}

	checkIfEmployeeHasThisSkill(skill: any){
		if(this.employee.skills != null){
			for(let employee_skill of this.employee.skills){
				if(skill == employee_skill){
					return true;
				}
			}
		}

	}

	checkifEmployeeSupervisorSelected(supervisor: any){
		if(this.employee.supervisor_id != null){
			for(let employee_supervisor of this.employee.supervisor_id){
				if(supervisor == employee_supervisor.id){
					return true;
				}
			}
		}
	}


	getOnBoarding(dat){
		
		this._common_service.getOnboarding().
		subscribe(
			data => {
				this.onboarding = data;

				if (this.onboarding != null && dat != null) {
					for (let i = 0; i < this.onboarding.length; ++i) {
						for (let x = 0; x < dat.length; ++x) {
							if (this.onboarding[i].id == dat[x].id) {
								this.onboarding[i].val = true;
							}
						}
					}
				}
			},
			err => console.error(err)
		);
	}

	onboardingCheckbox(index:any){

		if(this.onboarding[index].val == false){
			this.onboarding[index].val = true;
		}
		else{
			this.onboarding[index].val = false;
			this.checkAllValOnboarding = false;
		}
	}


	checkAllOnboarding(){

		if(this.checkAllValOnboarding == false){
			let leng = this.onboarding.length;
			for (let i = 0; i < leng; i++) {
				this.onboarding[i].val = true;
			}

			this.checkAllValOnboarding = true;
		}
		else{
			let len = this.onboarding.length;
			for (let i = 0; i < len; i++) {
				this.onboarding[i].val = false;
			}
			this.checkAllValOnboarding = false;
		}
	}

	onboardingCheck(){
		let len = this.onboarding.length;
		let temp = [];
		for (let i = 0; i < len; ++i) {
			if (this.onboarding[i].val == true) {
				temp.push(this.onboarding[i].id);
			}
		}

		this.updateEmployeeForm.value.onboarding = temp;
	}
}
