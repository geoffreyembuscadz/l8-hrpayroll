var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Configuration } from '../../app.config';
import { Employee } from '../../model/employee';
import { EmployeeService } from '../../services/employee.service';
import { CommonService } from '../../services/common.service';
var EmployeeAuditLogsComponent = /** @class */ (function () {
    function EmployeeAuditLogsComponent(_commonserv, _ar, _emp_service, _route, _fb, _conf) {
        var _this = this;
        this._commonserv = _commonserv;
        this._ar = _ar;
        this._emp_service = _emp_service;
        this._route = _route;
        this._fb = _fb;
        this._conf = _conf;
        this.dtOptions = {};
        this.employee = new Employee();
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filter_status_field = [{ 'value': 0, text: 'Inactive' }, { 'value': 1, text: 'Active' }];
        // add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.id = this._ar.snapshot.params['employee_id'];
        this._emp_service.getEmployee(this.id).subscribe(function (data) {
            _this.setEmployee(data);
        }, function (err) { return console.error(err); });
    }
    EmployeeAuditLogsComponent.prototype.ngAfterViewInit = function () {
    };
    EmployeeAuditLogsComponent.prototype.ngOnInit = function () {
        var authToken = localStorage.getItem('id_token');
    };
    EmployeeAuditLogsComponent.prototype.setEmployee = function (emp) {
        this.id = emp.id;
        this.employee = emp;
        this.employee.skills = emp.skills.split(',');
        if (this.employee.employee_image != null || this.employee.employee_image != '') {
            this.form_user_image = this.employee.employee_image;
            this.form_default_user_placeholder = this.employee.employee_image;
        }
        else if (this.employee.employee_image == '') {
            this.form_user_image = this._conf.frontEndUrl + 'assets/img/user-placeholder.png';
            this.form_default_user_placeholder = this._conf.frontEndUrl + 'assets/img/user-placeholder.png';
        }
        var employee_emergency_contacts = this.employee.emergency_contacts;
        // for(let _index_employee_emergency_contacts = 0; _index_employee_emergency_contacts < employee_emergency_contacts.length; _index_employee_emergency_contacts++){
        // 	this.updateEmergencyContacts.addControl('name[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].person_name);
        // 	this.updateEmergencyContacts.addControl('relationship[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].relationship);
        // 	this.updateEmergencyContacts.addControl('contact_number[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].contact_number);
        // }
        // this._company_service.getCompany(this.employee.primary_company).subscribe(
        //           data => {
        //           	this.setCompanyBranches(data.branches);
        //               this.setCompanyPolicies(data.policies);
        //               this.setCompanyGovernmentFields(data.emp_govrn_fields);
        //               this.setCompanySchedulesSelection(data.schedules);
        //           },
        //           err => console.error(err)
        //       );
    };
    EmployeeAuditLogsComponent.prototype.editemployee = function (id) {
    };
    EmployeeAuditLogsComponent.prototype.validateFilter = function () {
        this.dtOptions.ajax.url('asdasd').reload();
    };
    EmployeeAuditLogsComponent.prototype.getCompanySelectionFilter = function () {
        var _this = this;
        this._commonserv.getCompany().subscribe(function (data) {
            _this.setCompanySelectionFilter(data);
        }, function (err) { return console.error(err); });
    };
    EmployeeAuditLogsComponent.prototype.setCompanySelectionFilter = function (data) {
        this.filter_company_field = data;
    };
    EmployeeAuditLogsComponent = __decorate([
        Component({
            selector: 'admin-employee-audit-logs',
            templateUrl: './employee-audit-logs.component.html',
            styleUrls: ['./employee-audit-logs.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [CommonService, ActivatedRoute, EmployeeService, Router, FormBuilder, Configuration])
    ], EmployeeAuditLogsComponent);
    return EmployeeAuditLogsComponent;
}());
export { EmployeeAuditLogsComponent };
//# sourceMappingURL=employee-audit-logs.component.js.map