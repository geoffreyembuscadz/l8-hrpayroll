import 'rxjs/add/operator/catch';
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { NgUploaderOptions, UploadedFile, UploadRejected } from 'ngx-uploader';

import { Configuration } from '../../app.config';
import { Employee } from '../../model/employee';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';

const URL = '';
@Component({
  selector: 'admin-employee-remarks-view',
  templateUrl: './employee-remarks-view.component.html',
  styleUrls: ['./employee-remarks-view.component.css']
})


export class EmployeeRemarksViewComponent implements OnInit {
	private poststore: any;
	private post_data: any;
	private error_title: string;
	private error_message: string;
	private success_title: string;
	private success_message: string;
	private route_upload_attachment = 'upload?type=employee_attachment';
	private route_upload_img = 'upload?type=employee_img';
	private show_attachment_btn: any;
	public employee = new Employee();
	private employee_id: any;

	public form_companies = [];
	public form_employees: any;
	public employee_remarks: any;

	createEmployeeRemarksForm : FormGroup;

	constructor(private _conf: Configuration, private _fb: FormBuilder, private _ar: ActivatedRoute, private _companyserv: CompanyService, private _rt: Router, private _empservice: EmployeeService, private _zone: NgZone){
		this.employee_id = this._ar.snapshot.params['employee_id'];

		this._empservice.getEmployee(this.employee_id).subscribe(
			data => {
				this.setEmployee(data);
			},
			err => console.error(err)
		);

		
	}

	ngOnInit(){
		// console.log(this.employee_id);
	}

	setEmployee(data: any){
		this.employee = data;
		this.employee_remarks = data.remarks;
	}
}