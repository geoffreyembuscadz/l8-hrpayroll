import { Component, OnInit, Injectable } from '@angular/core';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Configuration } from '../../app.config';
import { Employee } from '../../model/employee';
import { EmployeeService } from '../../services/employee.service';
import { CommonService } from '../../services/common.service';

@Component({
	selector: 'admin-employee-audit-logs',
	templateUrl: './employee-audit-logs.component.html',
	styleUrls: ['./employee-audit-logs.component.css']
})

@Injectable()
export class EmployeeAuditLogsComponent implements OnInit {
	private id: string;
	private dtOptions: any = {};

	public api_employee: String;
	public employee = new Employee();

	private form_user_image: any;
	private form_default_user_placeholder: any;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	private filter_company_field: any;
	private filter_status_field = [{ 'value':0, text:'Inactive' }, { 'value':1, text:'Active' }];

	filterEmployeeList : FormGroup;

	constructor(private _commonserv: CommonService, private _ar: ActivatedRoute, private _emp_service: EmployeeService, private _route: Router, private _fb: FormBuilder, private _conf: Configuration){
		// add the the body classes
		this.body.classList.add("skin-blue");
		this.body.classList.add("sidebar-mini");
		
		this.id = this._ar.snapshot.params['employee_id'];
		this._emp_service.getEmployee(this.id).subscribe(
			data => {
				this.setEmployee(data);
			},
			err => console.error(err)
		);

	}

	ngAfterViewInit(){
	}

	ngOnInit() {
		let authToken = localStorage.getItem('id_token');
	}

	public setEmployee(emp: any){
		this.id = emp.id;
		this.employee = emp;
		this.employee.skills = emp.skills.split(',');

		if(this.employee.employee_image != null || this.employee.employee_image != ''){
			this.form_user_image = this.employee.employee_image;
			this.form_default_user_placeholder = this.employee.employee_image;
		} else if(this.employee.employee_image == ''){
			this.form_user_image = this._conf.frontEndUrl + 'assets/img/user-placeholder.png';
			this.form_default_user_placeholder = this._conf.frontEndUrl + 'assets/img/user-placeholder.png';
		}

		let employee_emergency_contacts = this.employee.emergency_contacts;

		// for(let _index_employee_emergency_contacts = 0; _index_employee_emergency_contacts < employee_emergency_contacts.length; _index_employee_emergency_contacts++){
		// 	this.updateEmergencyContacts.addControl('name[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].person_name);
		// 	this.updateEmergencyContacts.addControl('relationship[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].relationship);
		// 	this.updateEmergencyContacts.addControl('contact_number[' + _index_employee_emergency_contacts + ']', this.employee.emergency_contacts[_index_employee_emergency_contacts].contact_number);
		// }

		// this._company_service.getCompany(this.employee.primary_company).subscribe(
  //           data => {
  //           	this.setCompanyBranches(data.branches);
  //               this.setCompanyPolicies(data.policies);
  //               this.setCompanyGovernmentFields(data.emp_govrn_fields);
  //               this.setCompanySchedulesSelection(data.schedules);
  //           },
  //           err => console.error(err)
  //       );

	}

	editemployee(id: any){
	}

	validateFilter(){
		this.dtOptions.ajax.url('asdasd').reload();
	}

	getCompanySelectionFilter(){
		this._commonserv.getCompany().subscribe(
			data => {
				this.setCompanySelectionFilter(data);
			},
			err => console.error(err)
		);
	}

	setCompanySelectionFilter(data: any){
		this.filter_company_field = data;
	}
}
