import 'rxjs/add/operator/catch';
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { NgUploaderOptions, UploadedFile, UploadRejected } from 'ngx-uploader';

import { Configuration } from '../../app.config';
import { Employee } from '../../model/employee';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';

const URL = '';
@Component({
  selector: 'admin-employee-remarks-create',
  templateUrl: './employee-remarks-create.component.html',
  styleUrls: ['./employee-remarks-create.component.css']
})


export class EmployeeRemarksCreateComponent implements OnInit {
	private poststore: any;
	private post_data: any;
	private error_title: string;
	private error_message: string;
	private success_title: string;
	private success_message: string;
	private route_upload_attachment = 'upload?type=employee_attachment';
	private route_upload_img = 'upload?type=employee_img';
	private show_attachment_btn: any;
	public employee = new Employee();

	public form_companies = [];
	public form_employees: any;

	createEmployeeRemarksForm : FormGroup;

	constructor(private _conf: Configuration, private _fb: FormBuilder, private _ar: ActivatedRoute, private _companyserv: CompanyService, private _rt: Router, private _empservice: EmployeeService, private _zone: NgZone){
		this.getCompanies();

		this.createEmployeeRemarksForm = _fb.group({
			'company_id': ['', [Validators.required]],
			'employee_id': ['', [Validators.required]],
			'remarks': ['', [Validators.required]]
		});
	}

	ngOnInit(){

	}

	public getCompanies(){
		this._empservice.getCompanies().subscribe(
			data => {
				this.setCompanies(data);
			},
			err => this.catchError(err)
		);
	}

	private setCompanies(data: any){
		let companies = data;
		var fetched_companies = [];

		for(var _i = 0; _i < companies.length; _i++){
			this.form_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
		}

	}

	public validateCreation(){
		let employee_model = this.createEmployeeRemarksForm.value;
		// console.log(employee_model);
		
		this._empservice.addEmployeeRemark(employee_model).subscribe(
			data => {
				this.postStore(data);
			},
			err => this.catchError(err)
		);
	}

	public postStore(data: any){
		this.post_data = data;
		this.error_title = null;
		this.error_message = null;

		this.success_title = 'New Remark Added!';
		this.success_message = 'You have successfully added a new remark.';

		setTimeout(() => {
            this._rt.navigateByUrl('admin/employee-remarks-list');
        }, 4000);
	}

	private companySelectionChange(){
		let company_id = this.createEmployeeRemarksForm.get('company_id').value;

		this._empservice.getEmployees('company_id=' + company_id).subscribe(
			data => {
				this.setEmployeesSelection(data);
			},
			err => this.catchError(err)
		);
	}

	private setEmployeesSelection(data: any){
		this.form_employees = data;
		console.log(this.form_employees);
	}

	private getEmployeeByCompany(company_id: any){

	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
			window.scrollTo(0, 0);
		}
	}

	private clearErrorMsg(){
		this.error_title = '';
		this.error_message = '';
	}
}