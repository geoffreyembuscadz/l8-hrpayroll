import 'rxjs/add/operator/catch';
import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators, ReactiveFormsModule } from '@angular/forms';
import { NgUploaderOptions, UploadedFile, UploadRejected } from 'ngx-uploader';
import { Select2OptionData } from 'ng2-select2';
import { AuthUserService } from '../../services/auth-user.service';
import { Configuration } from '../../app.config';
import { Employee } from '../../model/employee';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { CommonService } from '../../services/common.service';

const URL = '';
@Component({
  selector: 'admin-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})


export class EmployeeCreateComponent implements OnInit {
	private poststore: any;
	private post_data: any;
	private error_title: string;
	private error_message: string;
	private success_title: string;
	private success_message: string;
	private route_upload_attachment = 'upload?type=employee_attachment';
	private route_upload_img = 'upload?type=employee_img';
	private show_attachment_btn: any;
	public employee = new Employee();

	public form_genders: any;
	public form_suffixes: any;
	public form_companies: any;
	public form_company_branches: any;
	public form_job_titles: any;
	public form_blood_types: any;
	public form_departments: any;
	public form_marital_statuses: any;
	public form_working_schedules: any;
	public form_supervisors: any;
	public form_skills: any;
	public form_employment_status: any;
	public form_emergency_contact = [{ index:0 }];
	public form_employee_dependent = [{ index:0 }];
	public form_employee_bank_name: any;
	public form_emergency_contact_row = 0;
	public form_employee_dependency_row = 0;
	public form_default_user_placeholder = '';
	public form_income_type: any;
	public form_user_image = '';
	public form_company_policies: any;
	public form_dependency_relationships: any;
	public fetched_company_schedules = [];
	public selected_company_schedule: any;
	public errorUploadImageMessage = '';
	public errorUploadAttachmentMessage = '';
	public error_basic_fields = [];

	public options: NgUploaderOptions;
	public optionsImg: NgUploaderOptions;
	public response: any;
	public response_image: any;
	public sizeLimit: number = 1000000;
	public hasBaseDropZoneOver: boolean;
	public onProcessSubmit: number = 0;

	createEmployeeForm : FormGroup;
	emergencyContacts: FormGroup;
	employeeDependencies: FormGroup;

	public onboarding=[];
	checkAllValOnboarding = false;


	constructor(
		private _conf: Configuration,
		private _fb: FormBuilder, 
		private _ar: ActivatedRoute, 
		private _rt: Router,
		private _companyserv: CompanyService, 
		private _empservice: EmployeeService, 
		private _zone: NgZone,
   		private _auth_service: AuthUserService,
   		private _common_service: CommonService
		){
	 	this.getCompanies();
	 	this.getDepartments();
	 	this.getSuperVisors();

	 	this.form_genders = this._empservice.getGenders();
		this.form_suffixes = this._empservice.getSuffixes();
		this.form_blood_types = this._empservice.getBloodTypes();
		this.form_skills = this._empservice.getEmployeeSkills();
		this.form_employee_bank_name = this._empservice.getBanks();
		this.form_marital_statuses = this._empservice.getEmployeeMaritalStatuses();
		this.form_employment_status = this._empservice.getEmploymentStatus();
		this.form_income_type = this._empservice.getIncomeType();
		this.form_dependency_relationships = this._empservice.getEmployeeDependencyRelationships();
		this.form_default_user_placeholder = this._conf.frontEndUrl + 'assets/img/user-placeholder.png';

		this.emergencyContacts = _fb.group({
			'emergency_person_name[0]': ['', [Validators.required]],
			'emergency_relationship[0]': ['', [Validators.required]],
			'emergency_contact_number[0]': ['', [Validators.required]]
		});

		this.employeeDependencies = _fb.group({
			'employee_dependent_name[0]': ['', [Validators.required]],
			'employee_dependent_age[0]': ['', [Validators.required]],
			'employee_dependent_relationship[0]': ['', [Validators.required]],
			'employee_dependent_pwd[0]': ['', [Validators.required]]
		});

	    this.createEmployeeForm = _fb.group({
	    	'firstname': ['', [Validators.required]],
	    	'middlename': ['-N/A-', [Validators.required]],
	    	'lastname': [ '', [Validators.required]],
	    	'suffix': ['', [Validators.required]],
	    	'primary_company': ['', [Validators.required]],
	    	'company_branch': ['', [Validators.required]],
	    	'gender': ['', [Validators.required]],
	    	'birthdate': ['', [Validators.required]],
	    	'blood_type': ['', [Validators.required]],
	    	'citizenship': ['', [Validators.required]],
	    	'employee_code': ['', [Validators.required]],
	    	'department': ['', [Validators.required]],
	    	'position': ['', [Validators.required]],
	    	'tel_no': ['', [Validators.required]],
	    	'cel_no': ['', [Validators.required]],
	    	'email': ['', [Validators.required]],
	    	'current_address': ['', [Validators.required]],
	    	'province_address': ['', [Validators.required]],
	    	'skills': ['', [Validators.required]],
	    	'marital_status': ['', [Validators.required]],
	    	'salary_receiving_type': ['', [Validators.required]],
	    	'income_rate': ['', [Validators.required]],
	    	'allowance_amount': ['', [Validators.required]],
	    	'nationality': ['Filipino', [Validators.required]],
	    	'employment_status': ['', [Validators.required]],
	    	'driving_license_no': ['', [Validators.required]],
	    	'sss': ['', [Validators.required]],
	    	'philhealth_no': ['', [Validators.required]],
	    	'tin_no': ['', [Validators.required]],
	    	'supervisor_id': '', //['', [Validators.required]],
	    	'pagibig_no': ['', [Validators.required]],
	    	'emergency_contacts': this.emergencyContacts,
	    	'employee_dependents': this.employeeDependencies,
	    	'employee_image': [this._conf.frontEndUrl + 'assets/img/user-placeholder.png', [Validators.required]],
	    	'starting_date': ['', [Validators.required]],
	    	'ending_date': ['', [Validators.required]],
	    	'user_credentials': ['', [Validators.required]],
	    	'sick_leave_credit': ['', [Validators.required]],
	    	'vacation_leave_credit': ['', [Validators.required]],
	    	'emergency_leave_credit': ['', [Validators.required]],
	    	'maternity_leave_credit': ['', [Validators.required]],
	    	'others_leave_credit': ['', [Validators.required]],
	    	'e_cola': ['', [Validators.required]],
	    	'working_schedule': ['', [Validators.required]],
	    	'bank_name': ['', [Validators.required]],
	    	'bank_code': ['', [Validators.required]],
	    	'bank_account_number': ['', [Validators.required]],
	    	'ip_address': [null],
	    	'mac_address': [null],
	    	'ddns': [null],
	    	'onboarding': [null]
	    });

	    // uploader ngzone
	    this.options = new NgUploaderOptions({
	    	url: this._conf.ServerWithApiUrl + this.route_upload_attachment,
	    	autoUpload: true,
	    	calculateSpeed: true
	    });

	    this.optionsImg = new NgUploaderOptions({
	    	url: this._conf.ServerWithApiUrl + this.route_upload_img,
	    	autoUpload: true,
	    	calculateSpeed: true,
	    	filterExtensions: true,
	    	allowedExtensions: ['jpg', 'png']
	    });
	}

	ngOnInit(){
		this.getOnBoarding();
	}

	handleImgUpload(data: any){
		this._zone.run(() => {
			this.response_image = data;
			if(data && data.response){
				this.response_image = JSON.parse(data.response);
				this.form_user_image = this.response_image.file_url;

				this.createEmployeeForm.get('employee_image').setValue(this.form_user_image);
			}
		});
	}

	handleUpload(data: any) {
	    this._zone.run(() => {
	    	this.response = data;
	    	if (data && data.response) {
	    		this.response = JSON.parse(data.response);

	    		let employee_id = this.post_data.id;
				let employee_attachment = this.response.file_url;
				let employee_attachment_dir = this.response.file_dir;

				this._empservice.attachEmployeeFiles(employee_id, employee_attachment, employee_attachment_dir).subscribe(
					data => {},
					err => this.catchError(err)
				);
	    	}
	    });

	    this.success_title = "Success!";
	    this.success_message = "A new employee record was successfully added.";
	    setTimeout(() => {
            this._rt.navigateByUrl('admin/employee-list');
        }, 60000);
	}

	beforeUploadImg(uploadingFile: UploadedFile){
		if (uploadingFile.size > this.sizeLimit){
			uploadingFile.setAbort();
      		this.errorUploadImageMessage = 'File is too large!';
  		}
	}

	primaryCompanyChanged(){
		let company_id = this.createEmployeeForm.get('primary_company').value;

        this._companyserv.getCompany(company_id).subscribe(
        	data => {
        		this.setCompanyBranches(data.branches);
        		this.setCompanyPolicies(data.policies);
        		this.setCompanySchedulesSelection(data.schedules);
        	}
        )
	}

	setCompanySchedulesSelection(data: any){
		let schedule_selection = [];
		let index = 0;

		for(let sched of data){
			let days = JSON.parse(sched.days);
			days = days.join();

			schedule_selection[index] = { id: sched.id, name: sched.name, time_in: sched.time_in, time_out: sched.time_out, days: days };
			this.fetched_company_schedules[sched.id] = sched;
			index++;
		}

		this.form_working_schedules = schedule_selection;
	}

	setCompanyBranches(data: any){
		let company_branches = [];
		let index = 0;

		for(let company_branch of data){
			company_branches[index] = company_branch;
			index++;
		}

		if(company_branches.length > 0){
			this.form_company_branches = company_branches;
			this.error_title = '';
			this.error_message = '';
		} else {
			alert('Please add a company branch for this company.');
			this.form_company_branches = [];
			this.error_title = 'Warning!';
			this.error_message = 'Please add a company branch for this company.';

		}
	}

	setCompanyPolicies(data: any){
		let company_policies_selection = [];
		let index = 0;

		for(let policy of data){
			company_policies_selection[index] = { id: policy.id, name: policy.name, policy_type: policy.company_policy_type };
			index++;
		}

		this.form_company_policies = company_policies_selection;
	}

	beforeUploadAttachments(uploadingFile: UploadedFile){
		if (uploadingFile.size > this.sizeLimit){
			uploadingFile.setAbort();
      		this.errorUploadAttachmentMessage = 'File is too large!';
  		}
	}

	exampleValidator(control: FormControl): {[s: string]: boolean} {
		if (control.value === '') {
			return {example: true};
		}

		return null;
	}

	public postStore(data: any){
		this.post_data = data;
		this.error_title = null;
		this.error_message = null;

		this.success_title = 'Success!';
		this.success_message = 'You have Successfully added a new record of employee.';

		setTimeout(() => {
			this.onProcessSubmit = 1;
			this._rt.navigateByUrl('admin/employee-list');
        }, 60000);
	}

	public addDependencyRow(){
		this.form_employee_dependency_row++;
		let index_count = this.form_employee_dependency_row;
		let counter = this.form_employee_dependent.length;//(this.form_emergency_contact.length > 1) ? (this.form_emergency_contact.length - 1) : this.form_emergency_contact.length;

		this.employeeDependencies.addControl('employee_dependent_name[' + index_count + ']', new FormControl());
		this.employeeDependencies.addControl('employee_dependent_age[' + index_count + ']', new FormControl());
		this.employeeDependencies.addControl('employee_dependent_relationship[' + index_count + ']', new FormControl());
		this.employeeDependencies.addControl('employee_dependent_pwd[' + index_count + ']', new FormControl());

		this.form_employee_dependent.push({ index:index_count });
	}

	public removeDependencyRow(row_count: any){
		let data_row_dependent_contacts = this.form_employee_dependent;
		let data_remove_row = data_row_dependent_contacts[row_count];
		let data_remove_row_index = data_remove_row.index;
		data_row_dependent_contacts.splice(row_count, 1);

		this.emergencyContacts.removeControl('employee_dependent_name[' + data_remove_row_index + ']');
		this.emergencyContacts.removeControl('employee_dependent_age[' + data_remove_row_index + ']');
		this.emergencyContacts.removeControl('employee_dependent_relationship[' + data_remove_row_index + ']');
		this.emergencyContacts.removeControl('employee_dependent_pwd[' + data_remove_row_index + ']');

		this.form_employee_dependent = data_row_dependent_contacts;

	}

	public addEmergencyContactRow(){
		this.form_emergency_contact_row++;
		let index_count = this.form_emergency_contact_row;
		let counter = this.form_emergency_contact.length//(this.form_emergency_contact.length > 1) ? (this.form_emergency_contact.length - 1) : this.form_emergency_contact.length;

		this.emergencyContacts.addControl('emergency_person_name[' + index_count + ']', new FormControl());
		this.emergencyContacts.addControl('emergency_relationship[' + index_count + ']', new FormControl());
		this.emergencyContacts.addControl('emergency_contact_number[' + index_count + ']', new FormControl());

		this.form_emergency_contact.push({ index:index_count });
	}

	public removeEmergencyContactRow(row_count: any){
		let data_row_emergency_contacts = this.form_emergency_contact;
		let data_remove_row = data_row_emergency_contacts[row_count];
		let data_remove_row_index = data_remove_row.index;
		data_row_emergency_contacts.splice(row_count, 1);

		this.emergencyContacts.removeControl('emergency_person_name[' + data_remove_row_index + ']');
		this.emergencyContacts.removeControl('emergency_relationship[' + data_remove_row_index + ']');
		this.emergencyContacts.removeControl('emergency_contact_number[' + data_remove_row_index + ']');

		this.form_emergency_contact = data_row_emergency_contacts;
	}

	public validateCreation() {
		this.onProcessSubmit = 1;
		this.onboardingCheck();

		let employee_model = this.createEmployeeForm.value;

		this._empservice.storeEmployee(employee_model).subscribe(
			data => {
				this.postStore(data);
				this.onProcessSubmit = 1;
				this.show_attachment_btn = true;
			},
			err => {
				this.onProcessSubmit = 0;
				this.catchError(err);
				console.log(err);
			}
		);

	}

	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
			window.scrollTo(0, 0);
		}
	}

	private clearErrorMsg(){
		this.error_title = '';
		this.error_message = '';
	}

	ngAfterViewInit(){
	}

	onChangeDepartment(department_id: any){
		if(department_id != null){
			this.getJobTitles();
		}
	}

	private getDepartments(){
		this._empservice.getDepartments().subscribe(
			data => {
				this.setDepartments(data);
			},
			err => this.catchError(err)
		);
	}

	private setDepartments(data: any){
		let departments = [];
		data.forEach((values, index) => {
			departments[index] = { id: values.id, name: values.name };
		});

		this.form_departments = departments;
	}

	private getJobTitles(){
		this._empservice.getJobTitles().subscribe(
			data => {
				this.fetchJobTitles(data);
			},
			err => this.catchError(err)
		);
	}

	private fetchJobTitles(data: any){
		let job_titles = [];
		data.forEach((values, index) => {
			job_titles[index] = { id: values.id, name: values.name };
		});

		this.form_job_titles = job_titles;
	}

	private getSuperVisors(){
		this._empservice.getEmployees().subscribe(
			data => {
				this.setSuperVisors(data);
			},
			err => this.catchError(err)
		);
	}

	private setSuperVisors(data: any){
		let supervisors = data;
		let fetched_supervisors = [];

		for(var _i = 0; _i < supervisors.length; _i++){
			fetched_supervisors[_i] = { id: supervisors[_i].id, name: '' + supervisors[_i].lastname + ', ' + supervisors[_i].firstname + ' - Employee Code: ' + supervisors[_i].employee_code };
		}

		this.form_supervisors = fetched_supervisors;
	}

	private getCompanies(){
		this._empservice.getCompanies().subscribe(
			data => {
				this.setCompanies(data);
			},
			err => this.catchError(err)
		);
	}

	private setCompanies(data: any){
		let companies = data;
		let fetched_companies = [];

		for(var _i = 0; _i < companies.length; _i++){
			fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
		}

		this.form_companies = fetched_companies;
	}

	getOnBoarding(){
		
		this._common_service.getOnboarding().
		subscribe(
			data => {
				this.onboarding = Array.from(data);
			},
			err => console.error(err)
		);
	}

	onboardingCheckbox(index:any){

		if(this.onboarding[index].val == false){
			this.onboarding[index].val = true;
		}
		else{
			this.onboarding[index].val = false;
			this.checkAllValOnboarding = false;
		}
	}

	checkAllOnboarding(){

		if(this.checkAllValOnboarding == false){
			let leng = this.onboarding.length;
			for (let i = 0; i < leng; i++) {
				this.onboarding[i].val = true;
			}

			this.checkAllValOnboarding = true;
		}
		else{
			let len = this.onboarding.length;
			for (let i = 0; i < len; i++) {
				this.onboarding[i].val = false;
			}
			this.checkAllValOnboarding = false;
		}
	}

	onboardingCheck(){
		let len = this.onboarding.length;
		let temp = [];
		for (let i = 0; i < len; ++i) {
			if (this.onboarding[i].val == true) {
				temp.push(this.onboarding[i].id);
			}
		}

		this.createEmployeeForm.value.onboarding = temp;
	}
}