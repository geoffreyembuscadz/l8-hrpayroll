var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeRouting } from './employee.routing';
import { EmployeeEditComponent } from './employee-edit.component';
import { EmployeeListComponent } from './employee-list.component';
import { EmployeeCreateComponent } from './employee-create.component';
import { EmployeeMassUploadComponent } from './employee-mass-upload.component';
import { EmployeeRemarksListComponent } from './employee-remarks-list.component';
import { EmployeeRemarksViewComponent } from './employee-remarks-view.component';
import { EmployeeRemarksCreateComponent } from './employee-remarks-create.component';
var EmployeeModule = /** @class */ (function () {
    function EmployeeModule() {
    }
    EmployeeModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                EmployeeRouting
            ],
            declarations: [
                EmployeeEditComponent,
                EmployeeListComponent,
                EmployeeCreateComponent,
                EmployeeMassUploadComponent,
                EmployeeRemarksListComponent,
                EmployeeRemarksViewComponent,
                EmployeeRemarksCreateComponent
            ]
        })
    ], EmployeeModule);
    return EmployeeModule;
}());
export { EmployeeModule };
//# sourceMappingURL=employee.module.js.map