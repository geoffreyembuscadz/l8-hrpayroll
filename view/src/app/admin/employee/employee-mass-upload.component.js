var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Router } from '@angular/router';
import { Component, Injectable, NgZone } from '@angular/core';
import { EmployeeMassUploadForm } from '../../model/employee_mass_upload_form';
import { FormBuilder, Validators } from '@angular/forms';
import { NgUploaderOptions } from 'ngx-uploader';
import { Configuration } from '../../app.config';
import { EmployeeService } from '../../services/employee.service';
import { CompanyService } from '../../services/company.service';
import { CommonService } from '../../services/common.service';
var EmployeeMassUploadComponent = /** @class */ (function () {
    function EmployeeMassUploadComponent(_commonserv, _company_service, _emp_service, _route, _fb, _conf, _zone) {
        this._commonserv = _commonserv;
        this._company_service = _company_service;
        this._emp_service = _emp_service;
        this._route = _route;
        this._fb = _fb;
        this._conf = _conf;
        this._zone = _zone;
        this.route_upload_attachment = 'upload?type=employee_mass_upload';
        this.employee_mass_upload = new EmployeeMassUploadForm();
        this.has_error = 0;
        this.form_suffixes = [];
        this.form_companies = [];
        this.form_job_titles = [];
        this.form_departments = [];
        this.form_marital_status = [];
        this.form_company_branches = [];
        this.form_employment_status = [];
        this.form_show_loading_status = 0;
        this.form_show_download_import = 0;
        this.form_company_branches_export = [];
        this.sizeLimit = 1000000;
        this.employee_response_upload = [];
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        var employee = 'employee';
        this.error_message = null;
        this.api_employee = this._conf.ServerWithApiUrl + 'employee/';
        this.form_employee_export_csv_fields = [
            { name: "employee_code", selected: true },
            { name: "firstname", selected: true },
            { name: "middlename", selected: true },
            { name: "lastname", selected: true },
            { name: "company", selected: true },
            { name: "branch", selected: true },
            { name: "gender", selected: true },
            { name: "birthdate", selected: true },
            { name: "e_cola", selected: true },
            { name: "salary_receiving_type", selected: true },
            { name: "rate", selected: true },
            { name: "allowance_amount", selected: true },
            { name: "sss", selected: true },
            { name: "philhealth_no", selected: true },
            { name: "pagibig_no", selected: true },
            { name: "tin_no", selected: true },
            { name: "nationality", selected: true },
            { name: "cel_no", selected: true },
            { name: "starting_date", selected: true },
            { name: "current_address", selected: true },
            { name: "status", selected: true },
            { name: "position_type", selected: null },
            { name: "suffix", selected: null },
            { name: "department", selected: null },
            { name: "position", selected: null },
            { name: "skills", selected: null },
            { name: "blood_type", selected: null },
            { name: "citizenship", selected: null },
            { name: "driving_license_no", selected: null },
            { name: "hdmf_no", selected: null },
            { name: "supervisor_id", selected: null },
            { name: "marital_status", selected: null },
            { name: "tel_no", selected: null },
            { name: "email", selected: null },
            { name: "employment_status", selected: null },
            { name: "date_hired", selected: null },
            { name: "regularization_date", selected: null },
            { name: "end_date", selected: null },
            { name: "bank_name", selected: null },
            { name: "bank_code", selected: null },
            { name: "bank_account_number", selected: null },
            { name: "permanent_address", selected: null },
            { name: "province_address", selected: null },
            { name: "employee_image", selected: null },
            { name: "date_terminated", selected: null },
            { name: "sick_leave_credit", selected: null },
            { name: "use_sick_leave_credit", selected: null },
            { name: "vacation_leave_credit", selected: null },
            { name: "use_vacation_leave_credit", selected: null },
            { name: "emergency_leave_credit", selected: null },
            { name: "use_emergency_leave_credit", selected: null },
            { name: "maternity_leave_credit", selected: null },
            { name: "use_maternity_leave_credit", selected: null },
            { name: "others_leave_credit", selected: null },
            { name: "use_others_leave_credit", selected: null }
        ];
        this.employee_mass_upload.mass_upload_fields = [
            "employee_code",
            "firstname",
            "middlename",
            "lastname",
            "company",
            "branch",
            "gender",
            "birthdate",
            "nationality",
            "sss",
            "philhealth_no",
            "pagibig_no",
            "tin_no",
            "e_cola",
            "salary_receiving_type",
            "rate",
            "allowance_amount",
            "status",
            "current_address",
        ];
        this.getCompanies();
        this.getDepartments();
        this.getJobTitles();
        this.form_genders = this._emp_service.getGenders();
        this.form_suffixes = this._emp_service.getSuffixes();
        this.form_marital_status = this._emp_service.getEmployeeMaritalStatuses();
        this.form_employment_status = this._emp_service.getEmploymentStatus();
        // uploader ngzone
        this.optionsCsv = new NgUploaderOptions({
            url: this._conf.ServerWithApiUrl + this.route_upload_attachment,
            autoUpload: true,
            calculateSpeed: true,
            filterExtensions: true,
            allowedExtensions: ['csv']
        });
        this.exportEmployeeForm = _fb.group({
            'primary_company': ['', [Validators.required]],
            'company_branch': ['', [Validators.required]]
        });
    }
    EmployeeMassUploadComponent.prototype.ngOnInit = function () {
    };
    EmployeeMassUploadComponent.prototype.ngAfterViewInit = function () {
    };
    EmployeeMassUploadComponent.prototype.getDepartments = function () {
        var _this = this;
        this._emp_service.getDepartments().subscribe(function (data) {
            _this.setDepartments(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeMassUploadComponent.prototype.setDepartments = function (data) {
        var departments = [];
        data.forEach(function (values, index) {
            departments[index] = { id: values.id, name: values.name };
        });
        this.form_departments = departments;
        console.log(this.form_departments);
    };
    EmployeeMassUploadComponent.prototype.getCompanies = function () {
        var _this = this;
        this._emp_service.getCompanies().subscribe(function (data) {
            _this.setCompanies(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeMassUploadComponent.prototype.setCompanies = function (data) {
        var companies = data;
        var fetched_companies = [];
        for (var _i = 0; _i < companies.length; _i++) {
            fetched_companies[_i] = { id: companies[_i].id, name: companies[_i].name };
        }
        this.form_companies = fetched_companies;
    };
    EmployeeMassUploadComponent.prototype.primaryCompanyChangedExport = function (selected_company) {
        var _this = this;
        var company_id = selected_company.value;
        this.form_company_selected_import = company_id;
        this._company_service.getCompany(company_id).subscribe(function (data) {
            _this.setCompanyBranchesExport(data.branches);
        });
    };
    EmployeeMassUploadComponent.prototype.displayDownloadFormButton = function () {
        this.form_show_download_import = 1;
    };
    EmployeeMassUploadComponent.prototype.primaryCompanyBranchChanged = function (company_branch) {
        var select_company_branch = company_branch;
        this.form_company_branch_selected_import = select_company_branch;
        console.log(this.form_company_branch_selected_import);
    };
    EmployeeMassUploadComponent.prototype.setCompanyBranchesExport = function (company_branches) {
        var data = [];
        var index = 0;
        this.form_company_branches = [];
        for (var _a = 0, company_branches_1 = company_branches; _a < company_branches_1.length; _a++) {
            var company_branch = company_branches_1[_a];
            data[index] = company_branch;
            index++;
        }
        this.form_company_branches_export = data;
    };
    EmployeeMassUploadComponent.prototype.downloadUploadForm = function (data, valid) {
        var form_valid = valid;
        var company = this.employee_mass_upload.company;
        var company_branch = this.employee_mass_upload.company_branch;
        var selected_fields = this.employee_mass_upload.mass_upload_fields;
        console.log([company, company_branch, selected_fields]);
        if (form_valid) {
            this._emp_service.exportEmployeeMassUploadform(company, company_branch, selected_fields);
        }
        else {
            alert('Please Select a Company, Branch & Fields for the upload form.');
        }
    };
    EmployeeMassUploadComponent.prototype.primaryCompanyChanged = function () {
        var _this = this;
        var company_id = this.exportEmployeeForm.get('primary_company').value;
        this._company_service.getCompany(company_id).subscribe(function (data) {
            _this.setCompanyBranches(data.branches);
        });
    };
    EmployeeMassUploadComponent.prototype.exportEmployeeListReport = function () {
        var form_data = this.exportEmployeeForm.value;
        this._emp_service.exportEmployeeList(form_data);
    };
    EmployeeMassUploadComponent.prototype.setCompanyBranches = function (company_branches) {
        var data = [];
        var index = 0;
        this.form_company_branches = [];
        for (var _a = 0, company_branches_2 = company_branches; _a < company_branches_2.length; _a++) {
            var company_branch = company_branches_2[_a];
            data[index] = company_branch;
            index++;
        }
        this.form_company_branches = data;
    };
    EmployeeMassUploadComponent.prototype.getJobTitles = function () {
        var _this = this;
        this._emp_service.getJobTitles().subscribe(function (data) {
            _this.fetchJobTitles(data);
        }, function (err) { return _this.catchError(err); });
    };
    EmployeeMassUploadComponent.prototype.fetchJobTitles = function (data) {
        var job_titles = [];
        data.forEach(function (values, index) {
            job_titles[index] = { id: values.id, name: values.name };
        });
        this.form_job_titles = job_titles;
    };
    EmployeeMassUploadComponent.prototype.consoleFunc = function () {
        console.log(123);
    };
    EmployeeMassUploadComponent.prototype.resetSelectionFields = function () {
        // Reset Selection fields
        this.form_employee_export_csv_fields = [
            { name: "employee_code", selected: true },
            { name: "firstname", selected: true },
            { name: "middlename", selected: true },
            { name: "lastname", selected: true },
            { name: "company", selected: true },
            { name: "branch", selected: true },
            { name: "gender", selected: true },
            { name: "birthdate", selected: true },
            { name: "e_cola", selected: true },
            { name: "salary_receiving_type", selected: true },
            { name: "rate", selected: true },
            { name: "allowance_amount", selected: true },
            { name: "sss", selected: true },
            { name: "philhealth_no", selected: true },
            { name: "pagibig_no", selected: true },
            { name: "tin_no", selected: true },
            { name: "nationality", selected: true },
            { name: "cel_no", selected: true },
            { name: "starting_date", selected: true },
            { name: "current_address", selected: true },
            { name: "status", selected: true },
            { name: "position_type", selected: null },
            { name: "suffix", selected: null },
            { name: "department", selected: null },
            { name: "position", selected: null },
            { name: "skills", selected: null },
            { name: "blood_type", selected: null },
            { name: "citizenship", selected: null },
            { name: "driving_license_no", selected: null },
            { name: "hdmf_no", selected: null },
            { name: "supervisor_id", selected: null },
            { name: "marital_status", selected: null },
            { name: "tel_no", selected: null },
            { name: "email", selected: null },
            { name: "employment_status", selected: null },
            { name: "date_hired", selected: null },
            { name: "regularization_date", selected: null },
            { name: "end_date", selected: null },
            { name: "bank_name", selected: null },
            { name: "bank_code", selected: null },
            { name: "bank_account_number", selected: null },
            { name: "permanent_address", selected: null },
            { name: "province_address", selected: null },
            { name: "employee_image", selected: null },
            { name: "date_terminated", selected: null },
            { name: "sick_leave_credit", selected: null },
            { name: "use_sick_leave_credit", selected: null },
            { name: "vacation_leave_credit", selected: null },
            { name: "use_vacation_leave_credit", selected: null },
            { name: "emergency_leave_credit", selected: null },
            { name: "use_emergency_leave_credit", selected: null },
            { name: "maternity_leave_credit", selected: null },
            { name: "use_maternity_leave_credit", selected: null },
            { name: "others_leave_credit", selected: null },
            { name: "use_others_leave_credit", selected: null }
        ];
    };
    EmployeeMassUploadComponent.prototype.beforeUploadCsv = function (uploadingFile) {
        this.form_show_loading_status = 1;
        if (uploadingFile.size > this.sizeLimit) {
            uploadingFile.setAbort();
            console.error('File is too large!');
        }
    };
    EmployeeMassUploadComponent.prototype.handleCsvUpload = function (data) {
        var _this = this;
        this._zone.run(function () {
            _this.response_csv = data;
            if (data && data.response) {
                _this.response_csv = JSON.parse(data.response);
                console.log(_this.response_csv); // dito 
                _this.renderEmployeeUpload(_this.response_csv);
            }
        });
    };
    EmployeeMassUploadComponent.prototype.handleUpload = function (data) {
        var _this = this;
        this._zone.run(function () {
            _this.response = data;
            if (data && data.response) {
                _this.response = JSON.parse(data.response);
                var employee_id = _this.post_data.id;
                var employee_attachment = _this.response.file_url;
                var employee_attachment_dir = _this.response.file_dir;
                _this._emp_service.attachEmployeeFiles(employee_id, employee_attachment, employee_attachment_dir).subscribe(function (data) {
                    console.log(1);
                }, function (err) { return _this.catchError(err); });
            }
        });
    };
    EmployeeMassUploadComponent.prototype.renderEmployeeUpload = function (data) {
        var _this = this;
        var form_data = { 'filename': data.filename, 'file_dir': data.file_dir };
        this._emp_service.uploadMassEmployeeCsv(form_data).subscribe(function (data) {
            _this.form_show_loading_status = 0;
            _this.renderErrorMessages(data.error);
            _this.renderEmployeeUploaded(Array.from(data));
            setTimeout(function () {
                _this._route.navigateByUrl('admin/employee-list');
            }, 3000);
        }, function (err) {
            _this.error_title = "Oops!";
            _this.error_message = "It seems there's a problem in the employee upload. Be sure that the filetype is a CSV file or report this biuug.";
            _this.form_show_loading_status = 0;
        });
    };
    EmployeeMassUploadComponent.prototype.renderErrorMessages = function (data) {
        this.error_message = false;
        this.has_error = 0;
        if (data.error_msg != null) {
            this.has_error = 1;
            this.error_message = data.error_msg;
            console.log(this.error_message);
        }
        else {
            this.has_error = 0;
            this.error_message = null;
            this.success_title = "Success!";
            this.success_message = "A new employee record was successfully added & updated the existing records.";
        }
    };
    EmployeeMassUploadComponent.prototype.renderEmployeeUploaded = function (data) {
        this.employee_response_upload = data;
        console.log(this.employee_response_upload);
    };
    EmployeeMassUploadComponent.prototype.validateFilter = function (employeeForm, eventEmployeeFilter) {
    };
    EmployeeMassUploadComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
            window.scrollTo(0, 0);
        }
    };
    EmployeeMassUploadComponent = __decorate([
        Component({
            selector: 'admin-employee-mass-upload',
            templateUrl: './employee-mass-upload.component.html',
            styleUrls: ['./employee-mass-upload.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [CommonService, CompanyService, EmployeeService, Router, FormBuilder, Configuration, NgZone])
    ], EmployeeMassUploadComponent);
    return EmployeeMassUploadComponent;
}());
export { EmployeeMassUploadComponent };
//# sourceMappingURL=employee-mass-upload.component.js.map