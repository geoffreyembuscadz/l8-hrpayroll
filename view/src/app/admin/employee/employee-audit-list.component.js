var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Configuration } from '../../app.config';
import { EmployeeService } from '../../services/employee.service';
import { CommonService } from '../../services/common.service';
var EmployeeAuditListComponent = /** @class */ (function () {
    function EmployeeAuditListComponent(_commonserv, _emp_service, _route, _fb, _conf) {
        this._commonserv = _commonserv;
        this._emp_service = _emp_service;
        this._route = _route;
        this._fb = _fb;
        this._conf = _conf;
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.filter_status_field = [{ 'value': 0, text: 'Inactive' }, { 'value': 1, text: 'Active' }];
        this.getCompanySelectionFilter();
        // add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        var employee = 'employee';
        this.api_employee = this._conf.ServerWithApiUrl + 'employee/';
        this.filterEmployeeList = _fb.group({
            'name': ['', [Validators.required]],
            'company': ['', [Validators.required]],
            'status': ['', [Validators.required]]
        });
    }
    EmployeeAuditListComponent.prototype.ngAfterViewInit = function () {
    };
    EmployeeAuditListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var authToken = localStorage.getItem('id_token');
        // DataTable Configuration
        this.dtOptions = {
            searching: true,
            ajax: {
                url: this.api_employee,
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + authToken);
                },
            },
            columns: [
                {
                    title: 'Employee Num.',
                    /*data: 'employee_code',*/
                    render: function (data, type, row, meta) {
                        return data == null ? '-N/A-' : data;
                    }
                }, {
                    title: 'Name',
                    data: 'text'
                }, {
                    title: 'Current Employer',
                    data: 'company'
                }, {
                    title: 'Job Title',
                    data: 'position'
                }, {
                    title: 'Status',
                    render: function (data, type, row, meta) {
                        return data == 1 ? 'Active' : 'Inactive';
                    }
                }
            ],
            rowCallback: function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
                var self = _this;
                /* Unbind first in order to avoid any duplicate handler*/
                /* (see https://github.com/l-lin/angular-datatables/issues/87)*/
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', function () {
                    _this._route.navigate(['admin/employee-audit-logs/', data.id]);
                });
                return nRow;
            }
        };
    };
    EmployeeAuditListComponent.prototype.editemployee = function (id) {
        console.log();
    };
    EmployeeAuditListComponent.prototype.validateFilter = function () {
        console.log(123123);
        this.dtOptions.ajax.url('asdasd').reload();
    };
    EmployeeAuditListComponent.prototype.getCompanySelectionFilter = function () {
        var _this = this;
        this._commonserv.getCompany().subscribe(function (data) {
            _this.setCompanySelectionFilter(data);
        }, function (err) { return console.error(err); });
    };
    EmployeeAuditListComponent.prototype.setCompanySelectionFilter = function (data) {
        this.filter_company_field = data;
    };
    EmployeeAuditListComponent = __decorate([
        Component({
            selector: 'admin-employee-audit-list',
            templateUrl: './employee-audit-list.component.html',
            styleUrls: ['./employee-audit-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [CommonService, EmployeeService, Router, FormBuilder, Configuration])
    ], EmployeeAuditListComponent);
    return EmployeeAuditListComponent;
}());
export { EmployeeAuditListComponent };
//# sourceMappingURL=employee-audit-list.component.js.map