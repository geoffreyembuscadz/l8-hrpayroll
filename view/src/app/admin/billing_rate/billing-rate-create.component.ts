import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { Observable } from "rxjs/Rx";

import { BillingRate } from '../../model/billing_rate';
import { BillingRateService } from '../../services/billing-rate.service';

@Component({
  selector: 'admin-billing-rate-create',
  templateUrl: './billing-rate-create.component.html'
})
export class BillingRateCreateComponent extends DialogComponent<null, boolean> {

  menu_rec: any;
  public error_title: string;
  public error_message: string;
  private success_title: string;
  private success_message: string;
  public poststore: any;
  public billingrate = new BillingRate();

  error = false;
  elem: any;
  errorMessage = '';
  createBillingRateForm : FormGroup;

  constructor(private _rt: Router,
    dialogService: DialogService, 
    private _fb: FormBuilder, 
    private _bill_service: BillingRateService) { 
  	super(dialogService);

  	this.createBillingRateForm = _fb.group({
	    'salary': ['', [Validators.required]],
	    'billing_rate': ['', [Validators.required]],
    });
  }

  onSubmit() {

	let  billing_rate_model = this.createBillingRateForm.value;
	this._bill_service
	.storeBillingRate(billing_rate_model)
	.subscribe(
		data => {
			this.poststore = Array.from(data);
			this.success_title = "Success";
      this.success_message = "A new billing rate record was successfully added.";
        alert('A new billing rate record was successfully added.')
      
		  setTimeout(() => {
             this.close();
            }, 2000);
      this._rt.navigate(['/admin/billing-rate-list']);

    },
		err => this.catchError(err)
	);
  }

  private catchError(error: any){
  	let response_body = error._body;
  	let response_status = error.status;

  	if( response_status == 500 ){
  		this.error_title = 'Error 500';
  		this.error_message = 'Something went wrong';
  	} else if( response_status == 200 ) {
  		this.error_title = '';
  		this.error_message = '';
  	}
  }

  ngOnInit() {
  }

}
