import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';

import { BillingRate } from '../../model/billing_rate';
import { BillingRateService } from '../../services/billing-rate.service';



@Component({
  selector: 'admin-billing-rate-edit',
  templateUrl: './billing-rate-edit.component.html'
})
export class BillingRateEditComponent extends DialogComponent<null, boolean> {
  
  title: string;
  message: string;

  public id: any;

  public salary: any;
  public billing_rate: string;
  public display_name: string;
  public description: string;
  private error_title: string;
  private error_message: string;
  public parent_id: string;

  public success_title;
  public success_message;
  private headers: Headers;
  public poststore: any;
  public billingrate = new BillingRate();

  menu_rec: any;

  updateBillingRateForm : FormGroup;

  constructor(private _rt: Router, private _bill_service: BillingRateService, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute) {
    super(dialogService);
   
    this.id = this._bill_service.getId();
    this._bill_service.getBillingRate(this.id).subscribe(
      data => {
        this.setRole(data);
      },
      err => console.error(err)
    );

    this.updateBillingRateForm = _fb.group({
      'salary': [this.billingrate.salary, [Validators.required]],
      'billing_rate': [this.billingrate.billing_rate, [Validators.required]],
    });  
  }
 
  public validateUpdate() {
    let billing_model = this.updateBillingRateForm.value;

    this._bill_service.updateBillingRate(this.id, billing_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The billing rate is successfully updated.";
        alert('The billing rate is successfully updated.')
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/billing-rate-list']);
        this.clearErrorMsg();
      },
      err => this.catchError(err)
    );
  }

  archiveBillingRate(event: Event){
    event.preventDefault();
    let role_id = this.id;
    this._bill_service.archiveBillingRate(role_id).subscribe(
      data => {
      
        alert('Record is successfully archived.');
        this.close();
        this._rt.navigateByUrl('admin/billing-rate-list');
        
      },
      err => console.error(err)
    );

  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'Permission name already exist.';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }
  }

  public setRole(billing: any){
    this.salary = billing.salary;
    this.billing_rate = billing.billing_rate;
  }

  private clearErrorMsg(){
    this.error_title = '';
    this.error_message = '';
  }


  ngOnInit() {
  }

}