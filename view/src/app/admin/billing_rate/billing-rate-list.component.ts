import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';

import { BillingRate } from '../../model/billing_rate';
import { BillingRateCreateComponent } from './billing-rate-create.component';
import { BillingRateEditComponent } from './billing-rate-edit.component';
import { BillingRateService } from '../../services/billing-rate.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'admin-billing-rate-list',
  templateUrl: './billing-rate-list.component.html'
})

@Injectable()
export class BillingRateListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	public id: string;
	public billingrate = new BillingRate();

	public error_title: string;
    public error_message: string;
    ad_bill: any;

	dtOptions: any = {};
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

	constructor(private _conf: Configuration, private modalService: DialogService, private _bill_service: BillingRateService){

	}

	ngOnInit() {

		let billing = this._bill_service.getBillingRates()
			.subscribe(data => {
				this.ad_bill = Array.from(data);
            	this.rerender();
			},
			err => this.catchError(err)
		);
		//add the the body classes
	    this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

		
	}

	clickRow(id) {
	    this._bill_service.setId(id);
	    this.openEditModal();
	}


	private catchError(error: any){
	    let response_body = error._body;
	    let response_status = error.status;

	    if( response_status == 500 ){
	      this.error_title = 'Error 500';
	      this.error_message = 'Something went wrong';
	    } else if( response_status == 200 ) {
	      this.error_title = '';
	      this.error_message = '';
	    }
	}

	openEditModal() {
		let disposable = this.modalService.addDialog(BillingRateEditComponent, {
            title:'Update Billing Rate'
        	}).subscribe((isConfirmed)=>{
        		let billingrate = this._bill_service.getBillingRates()
					.subscribe(data => {
						this.ad_bill = Array.from(data);
		            	this.rerender();
					},
					err => this.catchError(err)
				);
        });
	}

	openAddModal() {
		let disposable = this.modalService.addDialog(BillingRateCreateComponent, {
            title:'Add Billing Rate'
        	}).subscribe((isConfirmed)=>{
        		let billingrate = this._bill_service.getBillingRates()
					.subscribe(data => {
						this.ad_bill = Array.from(data);
		            	this.rerender();
					},
					err => this.catchError(err)
				);
        });
    }

	ngOnDestroy() {
	    //remove the the body classes
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	 }

	 ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      // Destroy the table first
	      dtInstance.destroy();
	      // Call the dtTrigger to rerender again
	      this.dtTrigger.next();
	    });
    }
}
