var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { BillingRate } from '../../model/billing_rate';
import { BillingRateService } from '../../services/billing-rate.service';
var BillingRateCreateComponent = /** @class */ (function (_super) {
    __extends(BillingRateCreateComponent, _super);
    function BillingRateCreateComponent(_rt, dialogService, _fb, _bill_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._fb = _fb;
        _this._bill_service = _bill_service;
        _this.billingrate = new BillingRate();
        _this.error = false;
        _this.errorMessage = '';
        _this.createBillingRateForm = _fb.group({
            'salary': ['', [Validators.required]],
            'billing_rate': ['', [Validators.required]],
        });
        return _this;
    }
    BillingRateCreateComponent.prototype.onSubmit = function () {
        var _this = this;
        var billing_rate_model = this.createBillingRateForm.value;
        this._bill_service
            .storeBillingRate(billing_rate_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            _this.success_title = "Success";
            _this.success_message = "A new billing rate record was successfully added.";
            alert('A new billing rate record was successfully added.');
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/billing-rate-list']);
        }, function (err) { return _this.catchError(err); });
    };
    BillingRateCreateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    BillingRateCreateComponent.prototype.ngOnInit = function () {
    };
    BillingRateCreateComponent = __decorate([
        Component({
            selector: 'admin-billing-rate-create',
            templateUrl: './billing-rate-create.component.html'
        }),
        __metadata("design:paramtypes", [Router,
            DialogService,
            FormBuilder,
            BillingRateService])
    ], BillingRateCreateComponent);
    return BillingRateCreateComponent;
}(DialogComponent));
export { BillingRateCreateComponent };
//# sourceMappingURL=billing-rate-create.component.js.map