var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';
import { BillingRate } from '../../model/billing_rate';
import { BillingRateCreateComponent } from './billing-rate-create.component';
import { BillingRateEditComponent } from './billing-rate-edit.component';
import { BillingRateService } from '../../services/billing-rate.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
var BillingRateListComponent = /** @class */ (function () {
    function BillingRateListComponent(_conf, modalService, _bill_service) {
        this._conf = _conf;
        this.modalService = modalService;
        this._bill_service = _bill_service;
        this.dtTrigger = new Subject();
        this.billingrate = new BillingRate();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
    }
    BillingRateListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var billing = this._bill_service.getBillingRates()
            .subscribe(function (data) {
            _this.ad_bill = Array.from(data);
            _this.rerender();
        }, function (err) { return _this.catchError(err); });
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    };
    BillingRateListComponent.prototype.clickRow = function (id) {
        this._bill_service.setId(id);
        this.openEditModal();
    };
    BillingRateListComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    BillingRateListComponent.prototype.openEditModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(BillingRateEditComponent, {
            title: 'Update Billing Rate'
        }).subscribe(function (isConfirmed) {
            var billingrate = _this._bill_service.getBillingRates()
                .subscribe(function (data) {
                _this.ad_bill = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    BillingRateListComponent.prototype.openAddModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(BillingRateCreateComponent, {
            title: 'Add Billing Rate'
        }).subscribe(function (isConfirmed) {
            var billingrate = _this._bill_service.getBillingRates()
                .subscribe(function (data) {
                _this.ad_bill = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    BillingRateListComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    BillingRateListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    BillingRateListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], BillingRateListComponent.prototype, "dtElement", void 0);
    BillingRateListComponent = __decorate([
        Component({
            selector: 'admin-billing-rate-list',
            templateUrl: './billing-rate-list.component.html'
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration, DialogService, BillingRateService])
    ], BillingRateListComponent);
    return BillingRateListComponent;
}());
export { BillingRateListComponent };
//# sourceMappingURL=billing-rate-list.component.js.map