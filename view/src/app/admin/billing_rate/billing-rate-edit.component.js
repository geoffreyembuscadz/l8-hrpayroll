var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { BillingRate } from '../../model/billing_rate';
import { BillingRateService } from '../../services/billing-rate.service';
var BillingRateEditComponent = /** @class */ (function (_super) {
    __extends(BillingRateEditComponent, _super);
    function BillingRateEditComponent(_rt, _bill_service, dialogService, _fb, _ar) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._bill_service = _bill_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.billingrate = new BillingRate();
        _this.id = _this._bill_service.getId();
        _this._bill_service.getBillingRate(_this.id).subscribe(function (data) {
            _this.setRole(data);
        }, function (err) { return console.error(err); });
        _this.updateBillingRateForm = _fb.group({
            'salary': [_this.billingrate.salary, [Validators.required]],
            'billing_rate': [_this.billingrate.billing_rate, [Validators.required]],
        });
        return _this;
    }
    BillingRateEditComponent.prototype.validateUpdate = function () {
        var _this = this;
        var billing_model = this.updateBillingRateForm.value;
        this._bill_service.updateBillingRate(this.id, billing_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The billing rate is successfully updated.";
            alert('The billing rate is successfully updated.');
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/billing-rate-list']);
            _this.clearErrorMsg();
        }, function (err) { return _this.catchError(err); });
    };
    BillingRateEditComponent.prototype.archiveBillingRate = function (event) {
        var _this = this;
        event.preventDefault();
        var role_id = this.id;
        this._bill_service.archiveBillingRate(role_id).subscribe(function (data) {
            alert('Record is successfully archived.');
            _this.close();
            _this._rt.navigateByUrl('admin/billing-rate-list');
        }, function (err) { return console.error(err); });
    };
    BillingRateEditComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Permission name already exist.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    BillingRateEditComponent.prototype.setRole = function (billing) {
        this.salary = billing.salary;
        this.billing_rate = billing.billing_rate;
    };
    BillingRateEditComponent.prototype.clearErrorMsg = function () {
        this.error_title = '';
        this.error_message = '';
    };
    BillingRateEditComponent.prototype.ngOnInit = function () {
    };
    BillingRateEditComponent = __decorate([
        Component({
            selector: 'admin-billing-rate-edit',
            templateUrl: './billing-rate-edit.component.html'
        }),
        __metadata("design:paramtypes", [Router, BillingRateService, DialogService, FormBuilder, ActivatedRoute])
    ], BillingRateEditComponent);
    return BillingRateEditComponent;
}(DialogComponent));
export { BillingRateEditComponent };
//# sourceMappingURL=billing-rate-edit.component.js.map