import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';

import { Loan } from '../../model/loan';
import { LoanCreateEmployeeComponent } from './loan-create-employee.component';
import { LoanCreateCompanyComponent } from './loan-create-company.component';
import { LoanCreateDepartmentComponent } from './loan-create-department.component';
import { LoanCreatePositionComponent } from './loan-create-position.component';
import { LoanEditComponent } from './loan-edit.component';
import { LoanService } from '../../services/loan.service';

@Component({
  selector: 'admin-loans-list',
  templateUrl: './loan-list-modal.component.html',
  styleUrls: ['./loan-list.component.css']
})

@Injectable()
export class LoanListModalComponent extends DialogComponent<null, boolean> implements OnInit, AfterViewInit {
  @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();
  public id: string;
  public loan_id: any;
  public loan = new Loan();

  public error_title: string;
  public error_message: string;


  loan_rec: any;
  att_id: any;
  start:any;
  end:any


  dtOptions: any = {};
  bodyClasses:string = "skin-blue sidebar-mini";
    body = document.getElementsByTagName('body')[0];

  constructor(dialogService: DialogService, private _conf: Configuration, private modalService: DialogService, private _loan_service: LoanService){
    super(dialogService);

  }

  ngOnInit() {
    
    let id = '';
    let loan  = this._loan_service.getLoans(this.att_id, this.start, this.end)
         .subscribe(data => {
            this.loan_rec = Array.from(data);
            this.rerender();
         },
        err => this.catchError(err)
      );

    //add the the body classes
      this.body.classList.add("skin-blue");
      this.body.classList.add("sidebar-mini");


     this.dtOptions = {
          responsive: true
      };

    
  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'Something went wrong';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }
  }

  openEditModal() {
    let disposable = this.modalService.addDialog(LoanEditComponent, {
            title:'Update Loan By Employee'
          }).subscribe((isConfirmed)=>{
                this.rerender();
                
        });
  }

  openAddModalEmployee() {
    let disposable = this.modalService.addDialog(LoanCreateEmployeeComponent, {
            title:'Add Loan By Employee'
          }).subscribe((isConfirmed)=>{
                this.rerender();
                
        });
    }
  openAddModalCompany() {
    let disposable = this.modalService.addDialog(LoanCreateCompanyComponent, {
            title:'Add Loan By Company'
          }).subscribe((isConfirmed)=>{
                this.rerender();
                
        });
    }

  openAddModalDepartment() {
    let disposable = this.modalService.addDialog(LoanCreateDepartmentComponent, {
            title:'Add Loan By Department'
          }).subscribe((isConfirmed)=>{
                this.rerender();
                
        });
  }


  openAddModalPosition() {
    let disposable = this.modalService.addDialog(LoanCreatePositionComponent, {
            title:'Add Loan By Position'
          }).subscribe((isConfirmed)=>{
                this.rerender();
                
        });
  }

  ngOnDestroy() {
      
   }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }
}
