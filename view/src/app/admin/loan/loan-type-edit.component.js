var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoanType } from '../../model/loan_type';
import { LoanTypeService } from '../../services/loan-type.service';
var LoanTypeEditComponent = /** @class */ (function (_super) {
    __extends(LoanTypeEditComponent, _super);
    function LoanTypeEditComponent(_rt, _loan_service, dialogService, _fb, _ar) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._loan_service = _loan_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.loan = new LoanType();
        _this.id = _this._loan_service.getId();
        _this._loan_service.getLoanType(_this.id).subscribe(function (data) {
            _this.setLoanType(data);
        }, function (err) { return console.error(err); });
        _this.updateLoanTypeForm = _fb.group({
            'name': [_this.loan.name, [Validators.required, _this.isAlphaNumeric]],
            'display_name': [_this.loan.display_name, [Validators.required]],
            'description': [_this.loan.description, [Validators.required]],
            'type': [_this.loan.type, [Validators.required]],
            'include_payslip': [_this.loan.include_payslip, [Validators.required]]
        });
        return _this;
    }
    LoanTypeEditComponent.prototype.validateUpdate = function () {
        var _this = this;
        var role_model = this.updateLoanTypeForm.value;
        this._loan_service.updateLoanType(this.id, role_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The loan type is successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/loan-type-list']);
        }, function (err) { return _this.catchError(err); });
    };
    LoanTypeEditComponent.prototype.archiveLoanType = function (event) {
        var _this = this;
        event.preventDefault();
        var role_id = this.id;
        console.log('prearchive', role_id);
        this._loan_service.archiveLoanType(role_id).subscribe(function (data) {
            alert('Record is successfully archived.');
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/loan-type-list']);
        }, function (err) { return console.error(err); });
        console.log('record archive');
    };
    LoanTypeEditComponent.prototype.catchError = function (error) {
    };
    LoanTypeEditComponent.prototype.setLoanType = function (loan) {
        this.id = loan.id;
        this.loan = loan;
    };
    LoanTypeEditComponent.prototype.isAlphaNumeric = function (control) {
        if (control.value === '') {
            return { noAlphaNumeric: true };
        }
        else if (control.value === null || control.value === undefined) {
            return { noAlphaNumeric: false };
        }
        else if (!control.value.match(/^[a-z_]+$/i)) {
            return { noAlphaNumeric: true };
        }
    };
    LoanTypeEditComponent.prototype.ngOnInit = function () {
    };
    LoanTypeEditComponent = __decorate([
        Component({
            selector: 'admin-loan-type-edit',
            templateUrl: './loan-type-edit.component.html'
        }),
        __metadata("design:paramtypes", [Router, LoanTypeService, DialogService, FormBuilder, ActivatedRoute])
    ], LoanTypeEditComponent);
    return LoanTypeEditComponent;
}(DialogComponent));
export { LoanTypeEditComponent };
//# sourceMappingURL=loan-type-edit.component.js.map