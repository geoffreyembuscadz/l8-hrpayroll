var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { LoanType } from '../../model/loan_type';
import { LoanTypeService } from '../../services/loan-type.service';
var LoanTypeCreateComponent = /** @class */ (function (_super) {
    __extends(LoanTypeCreateComponent, _super);
    function LoanTypeCreateComponent(_rt, dialogService, _fb, _loan_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._rt = _rt;
        _this._fb = _fb;
        _this._loan_service = _loan_service;
        _this.loan = new LoanType();
        _this.error = false;
        _this.errorMessage = '';
        _this.createLoanTypeForm = _fb.group({
            'name': ['', [Validators.required, _this.isAlphaNumeric]],
            'display_name': ['', [Validators.required]],
            'description': ['', [Validators.required]],
            'type': ['', [Validators.required]],
            'include_payslip': ['', [Validators.required]],
        });
        return _this;
    }
    LoanTypeCreateComponent.prototype.onSubmit = function () {
        var _this = this;
        var loan_model = this.createLoanTypeForm.value;
        console.log(loan_model);
        this._loan_service
            .storeLoanType(loan_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
            _this.success_title = "Success";
            _this.success_message = "A new loan was successfully added.";
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/loan-type-list']);
        }, function (err) { return _this.catchError(err); });
    };
    LoanTypeCreateComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Loan name already exist.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    LoanTypeCreateComponent.prototype.isAlphaNumeric = function (control) {
        if (control.value === '') {
            return { noAlphaNumeric: true };
        }
        else if (control.value === null || control.value === undefined) {
            return { noAlphaNumeric: false };
        }
        else if (!control.value.match(/^[a-z_]+$/i)) {
            return { noAlphaNumeric: true };
        }
    };
    LoanTypeCreateComponent.prototype.ngOnInit = function () {
    };
    LoanTypeCreateComponent = __decorate([
        Component({
            selector: 'admin-loan-type-create',
            templateUrl: './loan-type-create.component.html'
        }),
        __metadata("design:paramtypes", [Router, DialogService, FormBuilder, LoanTypeService])
    ], LoanTypeCreateComponent);
    return LoanTypeCreateComponent;
}(DialogComponent));
export { LoanTypeCreateComponent };
//# sourceMappingURL=loan-type-create.component.js.map