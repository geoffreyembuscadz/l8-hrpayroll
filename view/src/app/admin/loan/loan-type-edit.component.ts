import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';

import { LoanType } from '../../model/loan_type';
import { LoanTypeService } from '../../services/loan-type.service';

@Component({
  selector: 'admin-loan-type-edit',
  templateUrl: './loan-type-edit.component.html'
})
export class LoanTypeEditComponent extends DialogComponent<null, boolean> {
  
  title: string;
  message: string;

  public id: any;
  public name: string;
  public display_name: string;
  public description: string;
  public type: string;
  public success_title;
  public success_message;
  private headers: Headers;
  public poststore: any;
  public loan = new LoanType();

  updateLoanTypeForm : FormGroup;

  constructor(private _rt: Router, private _loan_service: LoanTypeService, dialogService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute) {
    super(dialogService);
   
    this.id = this._loan_service.getId();
    this._loan_service.getLoanType(this.id).subscribe(
      data => {
        this.setLoanType(data);
      },
      err => console.error(err)
    );

    this.updateLoanTypeForm = _fb.group({
      'name': [this.loan.name, [Validators.required, this.isAlphaNumeric]],
      'display_name': [this.loan.display_name, [Validators.required]],
      'description': [this.loan.description, [Validators.required]],
      'type': [this.loan.type, [Validators.required]],
      'include_payslip': [this.loan.include_payslip, [Validators.required]]
    });  
  }
 
  public validateUpdate() {
    let role_model = this.updateLoanTypeForm.value;

    this._loan_service.updateLoanType(this.id, role_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The loan type is successfully updated.";
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/loan-type-list']);
      },
      err => this.catchError(err)
    );
  }

  archiveLoanType(event: Event){
    event.preventDefault();
    let role_id = this.id;
    console.log('prearchive', role_id);
    this._loan_service.archiveLoanType(role_id).subscribe(
      data => {
      
        alert('Record is successfully archived.');
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/loan-type-list']);
        
      },
      err => console.error(err)
    );
    console.log('record archive');

  }

  public catchError(error: any) {

  }

  public setLoanType(loan: any){
    this.id = loan.id;
    this.loan = loan;
  }


  isAlphaNumeric(control: FormControl): {[s: string]: boolean} {      
    if(control.value === '') {
      return { noAlphaNumeric: true }
    } else if (control.value === null || control.value === undefined){
      return { noAlphaNumeric: false }
    } else if(!control.value.match(/^[a-z_]+$/i))  {
      return { noAlphaNumeric: true };
    }
    
  }

  ngOnInit() {
  }

}