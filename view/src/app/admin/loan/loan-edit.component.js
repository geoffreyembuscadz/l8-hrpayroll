var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Loan } from '../../model/loan';
import { LoanService } from '../../services/loan.service';
import { LoanTypeService } from '../../services/loan-type.service';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';
var LoanEditComponent = /** @class */ (function (_super) {
    __extends(LoanEditComponent, _super);
    function LoanEditComponent(_loan_type_serv, _rt, _loan_service, _fb, _ar, daterangepickerOptions, dialogService) {
        var _this = _super.call(this, dialogService) || this;
        _this._loan_type_serv = _loan_type_serv;
        _this._rt = _rt;
        _this._loan_service = _loan_service;
        _this._fb = _fb;
        _this._ar = _ar;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this.loan = new Loan();
        _this.recurring = false;
        _this.deleteBtn = true;
        _this.date_in = moment();
        _this.mainInput = {
            start: moment(),
            end: moment()
        };
        _this.id = _this._loan_service.getId();
        _this._loan_service.getLoan(_this.id).subscribe(function (data) {
            _this.setLoan(data);
            if (data.type == "REOCCURING") {
                _this.recurring = true;
            }
            if (data.max_amount != data.current_amount) {
                _this.deleteBtn = false;
            }
        }, function (err) { return console.error(err); });
        _this.updateLoanForm = _fb.group({
            'employee_id': ['', [Validators.required]],
            'loan_type': ['', [Validators.required]],
            'type': ['', [Validators.required]],
            'loan_amount': ['', [Validators.required]],
            'current_amount': ['', [Validators.required]],
            'max_amount': ['', [Validators.required]],
            'date_file': ['']
        });
        _this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            drops: 'up'
        };
        return _this;
    }
    LoanEditComponent.prototype.validateUpdate = function () {
        var _this = this;
        if (this.updateLoanForm.value.loan_amount > 0) {
            this.updateLoanForm.patchValue({
                'loan_amount': this.updateLoanForm.value.loan_amount *= -1
            });
        }
        else {
            this.updateLoanForm.patchValue({
                'loan_amount': this.updateLoanForm.value.loan_amount
            });
        }
        if (this.updateLoanForm.value.include_payslip = 0) {
            this.updateLoanForm.value.include_payslip = "";
        }
        this.updateLoanForm.patchValue({
            'date_file': moment(this.date_in).format("YYYY-MM-DD")
        });
        if (this.recurring == false) {
            this.updateLoanForm.patchValue({ 'max_amount': 0 });
            this.updateLoanForm.patchValue({ 'current_amount': 0 });
        }
        else {
            this.updateLoanForm.patchValue({ 'max_amount': this.updateLoanForm.value.max_amount });
            this.updateLoanForm.patchValue({ 'current_amount': this.updateLoanForm.value.max_amount });
        }
        var role_model = this.updateLoanForm.value;
        this._loan_service.updateLoan(this.id, role_model)
            .subscribe(function (data) {
            _this.poststore = Array.from(data); // fetched the records
            _this.success_title = "Success!";
            _this.success_message = "The loan is successfully updated.";
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/loan-list']);
        }, function (err) { return _this.catchError(err); });
    };
    LoanEditComponent.prototype.archiveloan = function (event) {
        var _this = this;
        event.preventDefault();
        var role_id = this.id;
        this._loan_service.archiveLoan(role_id).subscribe(function (data) {
            alert('Record is successfully archived.');
            setTimeout(function () {
                _this.close();
            }, 2000);
            _this._rt.navigate(['/admin/loan-list']);
        }, function (err) { return console.error(err); });
    };
    LoanEditComponent.prototype.onlyDecimalNumberKey = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
        return true;
    };
    LoanEditComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        this.date = dateInput.start;
        this.date_in = this.date;
    };
    LoanEditComponent.prototype.catchError = function (error) {
    };
    LoanEditComponent.prototype.setLoan = function (loan) {
        this.id = loan.id;
        this.loan = loan;
    };
    LoanEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        var start = '';
        var end = '';
        var id = '';
        var loans = this._loan_service.getLoans(id, start, end)
            .subscribe(function (data) {
            _this.loan_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
        var loan_type = this._loan_type_serv.getLoansType()
            .subscribe(function (data) {
            _this.loan_type_rec = Array.from(data);
        }, function (err) { return _this.catchError(err); });
    };
    LoanEditComponent.prototype.getLoanType = function (value) {
        var _this = this;
        this.loan_id = value;
        var adjustment = this._loan_type_serv.getLoanType(this.loan_id)
            .subscribe(function (data) {
            var adjust = data;
            _this.loan_type = data.type;
        });
    };
    LoanEditComponent.prototype.isAlphaNumeric = function (control) {
        if (control.value === '') {
            return { noAlphaNumeric: true };
        }
        else if (control.value === null || control.value === undefined) {
            return { noAlphaNumeric: false };
        }
        else if (!control.value.match(/^[a-z_]+$/i)) {
            return { noAlphaNumeric: true };
        }
    };
    LoanEditComponent.prototype.occurence = function (value) {
        if (value == 'REOCCURING') {
            this.recurring = true;
        }
        else {
            this.recurring = false;
        }
    };
    LoanEditComponent = __decorate([
        Component({
            selector: 'admin-loan-edit',
            templateUrl: './loan-edit.component.html'
        }),
        __metadata("design:paramtypes", [LoanTypeService,
            Router,
            LoanService,
            FormBuilder,
            ActivatedRoute,
            DaterangepickerConfig,
            DialogService])
    ], LoanEditComponent);
    return LoanEditComponent;
}(DialogComponent));
export { LoanEditComponent };
//# sourceMappingURL=loan-edit.component.js.map