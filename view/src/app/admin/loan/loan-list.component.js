var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { Loan } from '../../model/loan';
import { LoanCreateEmployeeComponent } from './loan-create-employee.component';
import { LoanCreateCompanyComponent } from './loan-create-company.component';
import { LoanCreateDepartmentComponent } from './loan-create-department.component';
import { LoanCreatePositionComponent } from './loan-create-position.component';
import { LoanEditComponent } from './loan-edit.component';
import { LoanService } from '../../services/loan.service';
var LoanListComponent = /** @class */ (function () {
    function LoanListComponent(_conf, modalService, _loan_service) {
        this._conf = _conf;
        this.modalService = modalService;
        this._loan_service = _loan_service;
        this.dtTrigger = new Subject();
        this.loan = new Loan();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
    }
    LoanListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.att_id = '';
        this.start = '';
        this.end = '';
        var loan = this._loan_service.getLoans(this.att_id, this.start, this.end)
            .subscribe(function (data) {
            _this.ad_loan = Array.from(data);
            _this.rerender();
        }, function (err) { return _this.catchError(err); });
        this.dtOptions = {
            order: [[8, 'desc']]
        };
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    };
    LoanListComponent.prototype.clickRow = function (id) {
        this._loan_service.setId(id);
        this.openEditModal();
    };
    LoanListComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    LoanListComponent.prototype.openEditModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanEditComponent, {
            title: 'Update Loan By Employee'
        }).subscribe(function (isConfirmed) {
            var loan = _this._loan_service.getLoans(_this.att_id, _this.start, _this.end)
                .subscribe(function (data) {
                _this.ad_loan = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    LoanListComponent.prototype.openAddModalEmployee = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanCreateEmployeeComponent, {
            title: 'Add Loan By Employee'
        }).subscribe(function (isConfirmed) {
            var loan = _this._loan_service.getLoans(_this.att_id, _this.start, _this.end)
                .subscribe(function (data) {
                _this.ad_loan = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    LoanListComponent.prototype.openAddModalCompany = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanCreateCompanyComponent, {
            title: 'Add Loan By Company'
        }).subscribe(function (isConfirmed) {
            var loan = _this._loan_service.getLoans(_this.att_id, _this.start, _this.end)
                .subscribe(function (data) {
                _this.ad_loan = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    LoanListComponent.prototype.openAddModalDepartment = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanCreateDepartmentComponent, {
            title: 'Add Loan By Department'
        }).subscribe(function (isConfirmed) {
            var loan = _this._loan_service.getLoans(_this.att_id, _this.start, _this.end)
                .subscribe(function (data) {
                _this.ad_loan = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    LoanListComponent.prototype.openAddModalPosition = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanCreatePositionComponent, {
            title: 'Add Loan By Position'
        }).subscribe(function (isConfirmed) {
            var loan = _this._loan_service.getLoans(_this.att_id, _this.start, _this.end)
                .subscribe(function (data) {
                _this.ad_loan = Array.from(data);
                _this.rerender();
            }, function (err) { return _this.catchError(err); });
        });
    };
    LoanListComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    LoanListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    LoanListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], LoanListComponent.prototype, "dtElement", void 0);
    LoanListComponent = __decorate([
        Component({
            selector: 'admin-loans-list',
            templateUrl: './loan-list.component.html',
            styleUrls: ['./loan-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration, DialogService, LoanService])
    ], LoanListComponent);
    return LoanListComponent;
}());
export { LoanListComponent };
//# sourceMappingURL=loan-list.component.js.map