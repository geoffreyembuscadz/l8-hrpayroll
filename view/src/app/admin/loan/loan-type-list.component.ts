import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';

import { LoanType } from '../../model/loan_type';
import { LoanTypeCreateComponent } from './loan-type-create.component';
import { LoanTypeEditComponent } from './loan-type-edit.component';
import { LoanTypeService } from '../../services/loan-type.service';

import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'admin-loans-type-list',
  templateUrl: './loan-type-list.component.html',
  styleUrls: ['./loan-list.component.css']
})

@Injectable()
export class LoanTypeListComponent implements OnInit, AfterViewInit {

  @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();

  public id: string;
  public loan_id: any;
  public Loan = new LoanType();

  dtOptions: any = {};
  bodyClasses:string = "skin-blue sidebar-mini";
    body = document.getElementsByTagName('body')[0];

  constructor(private _conf: Configuration, private modalService: DialogService, private _loan_service: LoanTypeService){

  }

  ngOnInit() {
    let authToken = localStorage.getItem('id_token');

    // DataTable Configuration
    this.dtOptions = {
      ajax: {
        url: this._conf.ServerWithApiUrl + 'loan_type',
        type: "GET",
        beforeSend: function(xhr){
                            xhr.setRequestHeader("Authorization",
                            "Bearer " + authToken);
                },
      },
      columns: [{
        title: 'ID',
        data: 'id'
      },{
        title: 'Name',
        data: 'name'
      }, {
        title: 'Display Name',
        data: 'display_name'
      }, {
        title: 'Description',
        data: 'description'
      }, {
        title: 'Type',
        data: 'type'
      }],
      rowCallback: (nRow: number, data: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
        let self = this;
        // Unbind first in order to avoid any duplicate handler
        // (see https://github.com/l-lin/angular-datatables/issues/87)
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', () => {
          this.loan_id = data.id;
            this._loan_service.setId(this.loan_id);
          this.openEditModal();
        });
        return nRow;
      }
    };

    //add the the body classes
      this.body.classList.add("skin-blue");
      this.body.classList.add("sidebar-mini");

    
  }

  openEditModal() {
    let disposable = this.modalService.addDialog(LoanTypeEditComponent, {
            title:'Update Loan'
          }).subscribe((isConfirmed)=>{
                this.rerender();
                
        });
  }

  openAddModal() {
    let disposable = this.modalService.addDialog(LoanTypeCreateComponent, {
            title:'Add Loan'
          }).subscribe((isConfirmed)=>{
                this.rerender();
                
        });
    }

  ngOnDestroy() {
      //remove the the body classes
      this.body.classList.remove("skin-blue");
      this.body.classList.remove("sidebar-mini");
   }

  ngAfterViewInit(): void {
     this.dtTrigger.next();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }
}
