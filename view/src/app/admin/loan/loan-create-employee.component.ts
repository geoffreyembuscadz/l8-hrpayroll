import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { Observable } from "rxjs/Rx";

import { Select2OptionData } from 'ng2-select2';

import { Loan } from '../../model/loan';
import { LoanService } from '../../services/loan.service';
import { LoanTypeService } from '../../services/loan-type.service';
import { EmployeeService } from '../../services/employee.service';
import { AuthUserService } from '../../services/auth-user.service';

import { DaterangepickerConfig } from 'ng2-daterangepicker';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TimepickerModule } from 'ngx-bootstrap';

@Component({
  selector: 'admin-loan-create',
  templateUrl: './loan-create-employee.component.html',
  styleUrls: ['./loan-list.component.css']
})
export class LoanCreateEmployeeComponent extends DialogComponent<null, boolean> {

  public error_title: string;
  public error_message: string;
  private success_title: string;
  private success_message: string;
  public poststore: any;
  public loan = new Loan();
  public loan_id: any;
  public emp_code: string[];

  public recurring: Boolean = false;

  loan_type: any;
  loan_type_rec: any;
  emp_rec: any;
  user_id: any;

  error = false;
  elem: any;
  errorMessage = '';
  createLoanForm : FormGroup;

  public LoanData: Array<Select2OptionData>;
  public value: string[];
  public current: any;
  public options: Select2Options;

  public date_in = moment();
  public mainInput = {
        start: moment(),
        end: moment()
    }
  date:any;

  constructor(
    private _auth_service: AuthUserService, 
    private _emp_service: EmployeeService, 
    private _fb: FormBuilder, 
    private _loan_service: LoanService, 
    private _loan_type_serv: LoanTypeService,
    private _rt: Router,
    private daterangepickerOptions: DaterangepickerConfig,
    dialogService: DialogService) { 
  	super(dialogService);

  	this.createLoanForm = _fb.group({
	    'employee_id': [''],
	    'loan_type': ['', [Validators.required]],
      'type': ['', [Validators.required]],
	    'loan_amount': ['', [Validators.required]],
      'current_amount': [''],
      'max_amount': [''],
      'date_file': [''],
      'created_by': [''],

    });

    this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            drops: 'up'
    }; 
  }

  onSubmit() {
  let emp_len = this.emp_rec.length;
  let emp_code = [];

  this.createLoanForm.patchValue({
      'date_file': moment(this.date_in).format("YYYY-MM-DD")
    });
  for (let i = 0; i < emp_len; i++) {
    if (this.current.indexOf(this.emp_rec[i].id.toString()) > -1) {
      emp_code.push(this.emp_rec[i].id);
    }
    this.emp_code = emp_code;
  }
  this.createLoanForm.patchValue({ 'employee_id': this.emp_code });
  if (this.loan_type == 'DEDUCTION') {
    this.createLoanForm.patchValue({
      'loan_amount': this.createLoanForm.value.loan_amount *= -1
    });
  }
  if (this.recurring == false) {
    this.createLoanForm.patchValue({ 'max_amount': 0 });
    this.createLoanForm.patchValue({ 'current_amount': 0 });
  } else {
    this.createLoanForm.patchValue({ 'max_amount': this.createLoanForm.value.max_amount });
    this.createLoanForm.patchValue({ 'current_amount': this.createLoanForm.value.max_amount });
  }
  this.createLoanForm.value.created_by = this.user_id;

	let  loan_model = this.createLoanForm.value;
  	this._loan_service
  	.storeLoan(loan_model)
  	.subscribe(
  		data => {
  			this.poststore = Array.from(data);
  			this.success_title = "Success";
      	this.success_message = "A new loan record was successfully added.";
  		  setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/loan-list']);
      },
  		err => this.catchError(err)
  	);
  }
  
  private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        this.date = dateInput.start;
        this.date_in = this.date;
  }

  private catchError(error: any){
  	let response_body = error._body;
  	let response_status = error.status;

  	if( response_status == 500 ){
  		this.error_title = 'Error 500';
  		this.error_message = 'Something went wrong.';
  	} else if( response_status == 200 ) {
  		this.error_title = '';
  		this.error_message = '';
  	}
  }

  onlyDecimalNumberKey(event) {
      let charCode = (event.which) ? event.which : event.keyCode;
      if (charCode != 46 && charCode > 31
          && (charCode < 48 || charCode > 57))
          return false;
      return true;
  }

  ngOnInit() {

    let loan_type = this._loan_type_serv.getLoansType()
        .subscribe(
          data => {
            this.loan_type_rec = Array.from(data);
          },

          err => this.catchError(err)

        );

    let employee_name = this._emp_service.getEmployees()
        .subscribe(
          data => {
            this.emp_rec = Array.from(data);
            this.LoanData = this.emp_rec;

            this.value = [];

            this.options = {
              multiple: true
            }

            this.current = this.value;
          },
          err => console.error(err)
        );

       let created_by = this._auth_service.getUser().
          subscribe(
            data => {
              let user = data; 
              this.user_id = user.id;
            },
            err => console.error(err)
          );
  

  }

  changed(data: any) {
      this.current = data.value;
  }

  getLoanType(value) {
    this.loan_id = value;

    let adjustment = this._loan_type_serv.getLoanType(this.loan_id)
        .subscribe(
          data => {
            let adjust = data;
            this.loan_type = data.type;
          }
        );
  }

  occurence(value) {
    if (value == 'REOCCURING') {
      this.recurring = true;
    } else {
      this.recurring = false;
    }
  }
}
