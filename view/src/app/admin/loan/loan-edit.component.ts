import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response, Headers } from '@angular/http';

import { Loan } from '../../model/loan';
import { LoanService } from '../../services/loan.service';
import { LoanTypeService } from '../../services/loan-type.service';

import { DaterangepickerConfig } from 'ng2-daterangepicker';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TimepickerModule } from 'ngx-bootstrap';

@Component({
  selector: 'admin-loan-edit',
  templateUrl: './loan-edit.component.html'
})
export class LoanEditComponent extends DialogComponent<null, boolean> {
  
  title: string;
  message: string;

  public id: any;
  public name: string;
  public display_name: string;
  public description: string;
  public type: string;
  public success_title;
  public success_message;
  private headers: Headers;
  public poststore: any;
  public loan = new Loan();
  public loan_id: any;

  public recurring: Boolean = false;
  public deleteBtn: Boolean = true;

  loan_type: any;
  loan_type_rec: any;
  loan_rec: any;

  updateLoanForm : FormGroup;

  public date_in = moment();
  public mainInput = {
        start: moment(),
        end: moment()
    }
  date:any;


  constructor(
    private _loan_type_serv: LoanTypeService, 
    private _rt: Router, 
    private _loan_service: LoanService, 
    private _fb: FormBuilder, 
    private _ar: ActivatedRoute,
    private daterangepickerOptions: DaterangepickerConfig,
    dialogService: DialogService) {
    super(dialogService);
   
    this.id = this._loan_service.getId();
    this._loan_service.getLoan(this.id).subscribe(
      data => {  
        this.setLoan(data);
        if (data.type == "REOCCURING") {
          this.recurring = true;
        }

        if (data.max_amount != data.current_amount) {
          this.deleteBtn = false;
        }
      },
      err => console.error(err)
    );

    this.updateLoanForm = _fb.group({
      'employee_id': ['', [Validators.required]],
      'loan_type': ['', [Validators.required]],
      'type': ['', [Validators.required]],
      'loan_amount': ['', [Validators.required]],
      'current_amount': ['', [Validators.required]],
      'max_amount': ['', [Validators.required]],
      'date_file': ['']

    }); 

    this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            singleDatePicker: true,
            showDropdowns: true,
            autoUpdateInput: false,
            drops: 'up'
    }; 
  }
 
  public validateUpdate() {

    if (this.updateLoanForm.value.loan_amount > 0) {
      this.updateLoanForm.patchValue({
        'loan_amount': this.updateLoanForm.value.loan_amount *= -1
      });
    } else {
      this.updateLoanForm.patchValue({
        'loan_amount': this.updateLoanForm.value.loan_amount
      });
    }

    if (this.updateLoanForm.value.include_payslip = 0) {
       this.updateLoanForm.value.include_payslip = "";
    }

    this.updateLoanForm.patchValue({
      'date_file': moment(this.date_in).format("YYYY-MM-DD")
    });

    if (this.recurring == false) {
      this.updateLoanForm.patchValue({ 'max_amount': 0 });
      this.updateLoanForm.patchValue({ 'current_amount': 0 });
    } else {
      this.updateLoanForm.patchValue({ 'max_amount': this.updateLoanForm.value.max_amount });
      this.updateLoanForm.patchValue({ 'current_amount': this.updateLoanForm.value.max_amount });
    }
 
    let role_model = this.updateLoanForm.value;

    this._loan_service.updateLoan(this.id, role_model)
    .subscribe(
      data => {
        this.poststore = Array.from(data); // fetched the records
        this.success_title = "Success!";
        this.success_message = "The loan is successfully updated.";
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/loan-list']);
      },
      err => this.catchError(err)
    );
  }

  archiveloan(event: Event){
    event.preventDefault();
    let role_id = this.id;
    this._loan_service.archiveLoan(role_id).subscribe(
      data => {
      
        alert('Record is successfully archived.');
        setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/loan-list']);
        
      },
      err => console.error(err)
    );

  }

  onlyDecimalNumberKey(event) {
      let charCode = (event.which) ? event.which : event.keyCode;
      if (charCode != 46 && charCode > 31
          && (charCode < 48 || charCode > 57))
          return false;
      return true;
  }

  private selectedDate(value: any, dateInput: any) {
        dateInput.start = value.start;
        this.date = dateInput.start;
        this.date_in = this.date;
  }

  public catchError(error: any) {

  }

  public setLoan(loan: any){
    this.id = loan.id;
    this.loan = loan;
  }


  ngOnInit() {
    let start ='';
    let end = '';
    let id = '';
    let loans = this._loan_service.getLoans(id, start, end)
         .subscribe(
           data => {
             this.loan_rec = Array.from(data); 
           },
           err => this.catchError(err)
         );

     let loan_type = this._loan_type_serv.getLoansType()
        .subscribe(
          data => {
            this.loan_type_rec = Array.from(data);
          },

          err => this.catchError(err)

        );
  }

  getLoanType(value) {
    this.loan_id = value;

    let adjustment = this._loan_type_serv.getLoanType(this.loan_id)
        .subscribe(
          data => {
            let adjust = data;
            this.loan_type = data.type;
      
          }
        );
  }

  isAlphaNumeric(control: FormControl): {[s: string]: boolean} {      
    if(control.value === '') {
      return { noAlphaNumeric: true }
    } else if (control.value === null || control.value === undefined){
      return { noAlphaNumeric: false }
    } else if(!control.value.match(/^[a-z_]+$/i))  {
      return { noAlphaNumeric: true };
    }
    
  }

  occurence(value) {
    if (value == 'REOCCURING') {
      this.recurring = true;
    } else {
      this.recurring = false;
    }
  }

}