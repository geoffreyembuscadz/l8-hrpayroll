var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';
import { LoanType } from '../../model/loan_type';
import { LoanTypeCreateComponent } from './loan-type-create.component';
import { LoanTypeEditComponent } from './loan-type-edit.component';
import { LoanTypeService } from '../../services/loan-type.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
var LoanTypeListComponent = /** @class */ (function () {
    function LoanTypeListComponent(_conf, modalService, _loan_service) {
        this._conf = _conf;
        this.modalService = modalService;
        this._loan_service = _loan_service;
        this.dtTrigger = new Subject();
        this.Loan = new LoanType();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
    }
    LoanTypeListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var authToken = localStorage.getItem('id_token');
        // DataTable Configuration
        this.dtOptions = {
            ajax: {
                url: this._conf.ServerWithApiUrl + 'loan_type',
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + authToken);
                },
            },
            columns: [{
                    title: 'ID',
                    data: 'id'
                }, {
                    title: 'Name',
                    data: 'name'
                }, {
                    title: 'Display Name',
                    data: 'display_name'
                }, {
                    title: 'Description',
                    data: 'description'
                }, {
                    title: 'Type',
                    data: 'type'
                }],
            rowCallback: function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
                var self = _this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', function () {
                    _this.loan_id = data.id;
                    _this._loan_service.setId(_this.loan_id);
                    _this.openEditModal();
                });
                return nRow;
            }
        };
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    };
    LoanTypeListComponent.prototype.openEditModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanTypeEditComponent, {
            title: 'Update Loan'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    LoanTypeListComponent.prototype.openAddModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanTypeCreateComponent, {
            title: 'Add Loan'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    LoanTypeListComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    LoanTypeListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    LoanTypeListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], LoanTypeListComponent.prototype, "dtElement", void 0);
    LoanTypeListComponent = __decorate([
        Component({
            selector: 'admin-loans-type-list',
            templateUrl: './loan-type-list.component.html',
            styleUrls: ['./loan-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [Configuration, DialogService, LoanTypeService])
    ], LoanTypeListComponent);
    return LoanTypeListComponent;
}());
export { LoanTypeListComponent };
//# sourceMappingURL=loan-type-list.component.js.map