import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';
import { Observable } from "rxjs/Rx";

import { LoanType } from '../../model/loan_type';
import { LoanTypeService } from '../../services/loan-type.service';

@Component({
  selector: 'admin-loan-type-create',
  templateUrl: './loan-type-create.component.html'
})
export class LoanTypeCreateComponent extends DialogComponent<null, boolean> {

  public error_title: string;
  public error_message: string;
  private success_title: string;
  private success_message: string;
  public poststore: any;
  public loan = new LoanType();

  error = false;
  elem: any;
  errorMessage = '';
  createLoanTypeForm : FormGroup;

  constructor(private _rt: Router, dialogService: DialogService, private _fb: FormBuilder, private _loan_service: LoanTypeService) { 
  	super(dialogService);

  	this.createLoanTypeForm = _fb.group({
	    'name': ['', [Validators.required, this.isAlphaNumeric]],
	    'display_name': ['', [Validators.required]],
      'description': ['', [Validators.required]],
      'type': ['', [Validators.required]],
	    'include_payslip': ['', [Validators.required]],
    });
  }

  onSubmit() {

	let  loan_model = this.createLoanTypeForm.value;
  console.log(loan_model);
	this._loan_service
	.storeLoanType(loan_model)
	.subscribe(
		data => {
			this.poststore = Array.from(data);
			this.success_title = "Success";
      this.success_message = "A new loan was successfully added.";
      setTimeout(() => {
             this.close();
            }, 2000);
        this._rt.navigate(['/admin/loan-type-list']);
		},
		err => this.catchError(err)
	);
  }

  private catchError(error: any){
  	let response_body = error._body;
  	let response_status = error.status;

  	if( response_status == 500 ){
  		this.error_title = 'Error 500';
  		this.error_message = 'Loan name already exist.';
  	} else if( response_status == 200 ) {
  		this.error_title = '';
  		this.error_message = '';
  	}
  }

  isAlphaNumeric(control: FormControl): {[s: string]: boolean} {      
    if(control.value === '') {
      return { noAlphaNumeric: true }
    } else if (control.value === null || control.value === undefined){
      return { noAlphaNumeric: false }
    } else if(!control.value.match(/^[a-z_]+$/i))  {
      return { noAlphaNumeric: true };
    }
    
  }

  ngOnInit() {
  }

}
