var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { Loan } from '../../model/loan';
import { LoanCreateEmployeeComponent } from './loan-create-employee.component';
import { LoanCreateCompanyComponent } from './loan-create-company.component';
import { LoanCreateDepartmentComponent } from './loan-create-department.component';
import { LoanCreatePositionComponent } from './loan-create-position.component';
import { LoanEditComponent } from './loan-edit.component';
import { LoanService } from '../../services/loan.service';
var LoanListModalComponent = /** @class */ (function (_super) {
    __extends(LoanListModalComponent, _super);
    function LoanListModalComponent(dialogService, _conf, modalService, _loan_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._conf = _conf;
        _this.modalService = modalService;
        _this._loan_service = _loan_service;
        _this.dtTrigger = new Subject();
        _this.loan = new Loan();
        _this.dtOptions = {};
        _this.bodyClasses = "skin-blue sidebar-mini";
        _this.body = document.getElementsByTagName('body')[0];
        return _this;
    }
    LoanListModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        var id = '';
        var loan = this._loan_service.getLoans(this.att_id, this.start, this.end)
            .subscribe(function (data) {
            _this.loan_rec = Array.from(data);
            _this.rerender();
        }, function (err) { return _this.catchError(err); });
        //add the the body classes
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.dtOptions = {
            responsive: true
        };
    };
    LoanListModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'Something went wrong';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    LoanListModalComponent.prototype.openEditModal = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanEditComponent, {
            title: 'Update Loan By Employee'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    LoanListModalComponent.prototype.openAddModalEmployee = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanCreateEmployeeComponent, {
            title: 'Add Loan By Employee'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    LoanListModalComponent.prototype.openAddModalCompany = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanCreateCompanyComponent, {
            title: 'Add Loan By Company'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    LoanListModalComponent.prototype.openAddModalDepartment = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanCreateDepartmentComponent, {
            title: 'Add Loan By Department'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    LoanListModalComponent.prototype.openAddModalPosition = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(LoanCreatePositionComponent, {
            title: 'Add Loan By Position'
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    LoanListModalComponent.prototype.ngOnDestroy = function () {
    };
    LoanListModalComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    LoanListModalComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], LoanListModalComponent.prototype, "dtElement", void 0);
    LoanListModalComponent = __decorate([
        Component({
            selector: 'admin-loans-list',
            templateUrl: './loan-list-modal.component.html',
            styleUrls: ['./loan-list.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService, Configuration, DialogService, LoanService])
    ], LoanListModalComponent);
    return LoanListModalComponent;
}(DialogComponent));
export { LoanListModalComponent };
//# sourceMappingURL=loan-list-modal.component.js.map