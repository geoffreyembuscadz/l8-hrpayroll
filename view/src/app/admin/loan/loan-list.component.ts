import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from './../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';

import { Loan } from '../../model/loan';
import { LoanCreateEmployeeComponent } from './loan-create-employee.component';
import { LoanCreateCompanyComponent } from './loan-create-company.component';
import { LoanCreateDepartmentComponent } from './loan-create-department.component';
import { LoanCreatePositionComponent } from './loan-create-position.component';
import { LoanEditComponent } from './loan-edit.component';
import { LoanService } from '../../services/loan.service';

@Component({
  selector: 'admin-loans-list',
  templateUrl: './loan-list.component.html',
  styleUrls: ['./loan-list.component.css']
})

@Injectable()
export class LoanListComponent implements OnInit, AfterViewInit {
  @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtTrigger: Subject<any> = new Subject();
  public id: string;
  public loan_id: any;
  public loan = new Loan();

  public error_title: string;
  public error_message: string;
  ad_loan: any;
  att_id: any;
  start:any;
  end:any


  dtOptions: any = {};
  bodyClasses:string = "skin-blue sidebar-mini";
    body = document.getElementsByTagName('body')[0];

  constructor(private _conf: Configuration, private modalService: DialogService, private _loan_service: LoanService){

  }

  ngOnInit() {

    this.att_id = '';
    this.start = '';
    this.end = '';
    let loan  = this._loan_service.getLoans(this.att_id, this.start, this.end)
         .subscribe(data => {
            this.ad_loan = Array.from(data);
            this.rerender();
         },
        err => this.catchError(err)
      );
    this.dtOptions = {
      order: [[8, 'desc']]
    };

    //add the the body classes
      this.body.classList.add("skin-blue");
      this.body.classList.add("sidebar-mini");

  }

   clickRow(id) {
    this._loan_service.setId(id);
    this.openEditModal();
  }


  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      this.error_message = 'Something went wrong';
    } else if( response_status == 200 ) {
      this.error_title = '';
      this.error_message = '';
    }
  }

  openEditModal() {
    let disposable = this.modalService.addDialog(LoanEditComponent, {
            title:'Update Loan By Employee'
          }).subscribe((isConfirmed)=>{
               let loan  = this._loan_service.getLoans(this.att_id, this.start, this.end)
                   .subscribe(data => {
                      this.ad_loan = Array.from(data);
                      this.rerender();
                   },
                  err => this.catchError(err)
                );
                
        });
  }

  openAddModalEmployee() {
    let disposable = this.modalService.addDialog(LoanCreateEmployeeComponent, {
            title:'Add Loan By Employee'
          }).subscribe((isConfirmed)=>{
               let loan  = this._loan_service.getLoans(this.att_id, this.start, this.end)
                   .subscribe(data => {
                      this.ad_loan = Array.from(data);
                      this.rerender();
                   },
                  err => this.catchError(err)
                );
                
        });
    }
  openAddModalCompany() {
    let disposable = this.modalService.addDialog(LoanCreateCompanyComponent, {
            title:'Add Loan By Company'
          }).subscribe((isConfirmed)=>{
               let loan  = this._loan_service.getLoans(this.att_id, this.start, this.end)
                   .subscribe(data => {
                      this.ad_loan = Array.from(data);
                      this.rerender();
                   },
                  err => this.catchError(err)
                );
                
        });
    }

  openAddModalDepartment() {
    let disposable = this.modalService.addDialog(LoanCreateDepartmentComponent, {
            title:'Add Loan By Department'
          }).subscribe((isConfirmed)=>{
               let loan  = this._loan_service.getLoans(this.att_id, this.start, this.end)
                   .subscribe(data => {
                      this.ad_loan = Array.from(data);
                      this.rerender();
                   },
                  err => this.catchError(err)
                );
                
        });
  }


  openAddModalPosition() {
    let disposable = this.modalService.addDialog(LoanCreatePositionComponent, {
            title:'Add Loan By Position'
          }).subscribe((isConfirmed)=>{
               let loan  = this._loan_service.getLoans(this.att_id, this.start, this.end)
                   .subscribe(data => {
                      this.ad_loan = Array.from(data);
                      this.rerender();
                   },
                  err => this.catchError(err)
                );
                
        });
  }

  ngOnDestroy() {
      //remove the the body classes
      this.body.classList.remove("skin-blue");
      this.body.classList.remove("sidebar-mini");
   }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }
}