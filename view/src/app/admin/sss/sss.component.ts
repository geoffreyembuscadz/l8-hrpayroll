import { Component, OnInit, Injectable } from '@angular/core';
import { SSSService } from '../../services/sss.service';

import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Configuration } from '../../app.config';
import { SSS } from '../../model/sss';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';



@Component({
  selector: 'app-sss',
  templateUrl: './sss.component.html',
  styleUrls: ['./sss.component.css']
})

@Injectable()
export class SSSComponent implements OnInit {


  	public id: string;
	public sss_id: any;
	public error_title: string;
	public error_message: string;
	public success_title;
	public success_message;
	public poststore: any;
	public sss = new SSS();
	sssForm: FormGroup;

	// TEMP variables
	
	public form


	private headers: Headers;
	dtOptions: any = {};
	sss_rec: any;
	// get permissions list
    perm_rec: any;
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	api_sss: String;

  	constructor(
  		dialogService: DialogService, 
  		private _perms_service: PermissionService, 
  		private modalService: DialogService, 
  		private _ar: ActivatedRoute, 
  		private _sss_service: SSSService, 
  		private _rt: Router, 
  		private _fb: FormBuilder, 
  		private _conf: Configuration
  		){
		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	      // Header Variables
		this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');

        this.api_sss = this._conf.ServerWithApiUrl + 'sss/';

	}

	ngAfterViewInit(){

	}

	showMe(ssss: any){
		this.sss_rec = ssss;
	}

	ngOnInit() {


		let ssss = this._perms_service.getPermissions().
		subscribe(
			data => {
				this.perm_rec = Array.from(data); // fetched record
			},
			err => console.error(err)
		);

		let authToken = localStorage.getItem('id_token');


		// DataTable Configuration
		this.dtOptions = {
			ajax:
			{
				url: this.api_sss,
				type: "GET",
				beforeSend: function(xhr){
                            xhr.setRequestHeader("Authorization",
                            "Bearer " + authToken);
                },
			},
			columns: [{
				
				title: 'Salary Base',
				data: 'salary_base'
				  },{
				title: 'Salary Ceiling',
				data: 'salary_ceiling'
				 },{
				title: 'Monthly Salary Credit',
				data: 'monthly_salary_credit'
				 },{
				title: 'Employer Share',
				data: 'er'
				 },{
				title: 'Employee Share',
				data: 'ee'
				 },{
				title: 'Total Contribution',
				data: 'total_contribution'
				 }],
			rowCallback: (nRow: number, data: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
				let self = this;
				// Unbind first in order to avoid any duplicate handler
				// (see https://github.com/l-lin/angular-datatables/issues/87)
				$('td', nRow).unbind('click');
				$('td', nRow).bind('click', () => {
				
					

				});
				return nRow;
			}
		};
		
	}

}
