var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable } from '@angular/core';
import { SSSService } from '../../services/sss.service';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Configuration } from '../../app.config';
import { SSS } from '../../model/sss';
import { DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import 'rxjs/add/operator/map';
var SSSComponent = /** @class */ (function () {
    function SSSComponent(dialogService, _perms_service, modalService, _ar, _sss_service, _rt, _fb, _conf) {
        this._perms_service = _perms_service;
        this.modalService = modalService;
        this._ar = _ar;
        this._sss_service = _sss_service;
        this._rt = _rt;
        this._fb = _fb;
        this._conf = _conf;
        this.sss = new SSS();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.api_sss = this._conf.ServerWithApiUrl + 'sss/';
    }
    SSSComponent.prototype.ngAfterViewInit = function () {
    };
    SSSComponent.prototype.showMe = function (ssss) {
        this.sss_rec = ssss;
    };
    SSSComponent.prototype.ngOnInit = function () {
        var _this = this;
        var ssss = this._perms_service.getPermissions().
            subscribe(function (data) {
            _this.perm_rec = Array.from(data); // fetched record
        }, function (err) { return console.error(err); });
        var authToken = localStorage.getItem('id_token');
        // DataTable Configuration
        this.dtOptions = {
            ajax: {
                url: this.api_sss,
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + authToken);
                },
            },
            columns: [{
                    title: 'Salary Base',
                    data: 'salary_base'
                }, {
                    title: 'Salary Ceiling',
                    data: 'salary_ceiling'
                }, {
                    title: 'Monthly Salary Credit',
                    data: 'monthly_salary_credit'
                }, {
                    title: 'Employer Share',
                    data: 'er'
                }, {
                    title: 'Employee Share',
                    data: 'ee'
                }, {
                    title: 'Total Contribution',
                    data: 'total_contribution'
                }],
            rowCallback: function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
                var self = _this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', function () {
                });
                return nRow;
            }
        };
    };
    SSSComponent = __decorate([
        Component({
            selector: 'app-sss',
            templateUrl: './sss.component.html',
            styleUrls: ['./sss.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            PermissionService,
            DialogService,
            ActivatedRoute,
            SSSService,
            Router,
            FormBuilder,
            Configuration])
    ], SSSComponent);
    return SSSComponent;
}());
export { SSSComponent };
//# sourceMappingURL=sss.component.js.map