import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { TaskService } from '../../../services/task.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TaskModalComponent } from '../task-modal/task-modal.component';
import { CommonService } from '../../../services/common.service';
import { Select2OptionData } from 'ng2-select2';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};
	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	task = [];
	user_id
	role_id
	supervisor_id
	employee_id
	filterForm:any;

	byDate=false;
	byEmployee=false;
	btnCreate = false

	public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public employee_current: any;
	public options: Select2Options;
	emp:any;

	public mainInput = {
        start: moment().subtract(12, 'month'),
        end: moment().subtract(11, 'month')
    }

  constructor(
  		private _task_service: TaskService,
   		private _auth_service: AuthUserService,
   		private _fb: FormBuilder,
   		private modalService: DialogService,
   		private _common_service: CommonService,
   		private daterangepickerOptions: DaterangepickerConfig
   	) {

  		this.filterForm = _fb.group({
			'start_date': 		[null],
			'end_date': 		[null],
			'employee_id': 		[null],
			'supervisor_id': 	[null],
			'role_id': 			[null]
   		});

  		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	    this.options = {
					multiple: true,
				      theme: 'classic',
				      closeOnSelect: false
				}
   	
   	}

   	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

  	ngOnInit() {
  		/**
  		 * process of task
  		 * admin can create task for employee and approved it
  		 * employee can create task for themselves and the admin will approved it
  		 */
  		this.getTask();
		this.getEmployee();
		this.dateOption();
  	}

  	dateOption(){
    	this.daterangepickerOptions.settings = {
        	singleDatePicker: false
		};
    }

	choiceFilter(id){

		if(id==1){
			this.byDate=true;
			this.byEmployee=true;
		}
		else if(id==2){
			if (this.byDate==false) { 
				this.byDate=true;
			} else {
				this.byDate=false;
				this.filterForm.value.start_date = null;
        		this.filterForm.value.end_date = null;
			}
		}
		else if(id==3){
			if (this.byEmployee==false) { 
				this.byEmployee=true;
			} else {
				this.byEmployee=false;
				this.filterForm.value.employee_id = null;
			}
		}
		else{
			this.byDate = false;
			this.byEmployee=false;

			this.filterForm.value.status_id = null;
        	this.filterForm.value.company_id = null;
        	this.filterForm.value.start_date = null;
        	this.filterForm.value.end_date = null;
        	this.filterForm.value.employee_id = null;
        	this.getTask();
		}

	}

  	getTask(){
  		let model = this.filterForm.value;
		this._task_service.getTask(model).
		subscribe(
			data => {
				this.task = data;
				this.rerender();
			},
			err => console.error(err)
		);
	}

	filterDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;

        let start_date = moment(dateInput.start).format("YYYY-MM-DD");
        let end_date = moment(dateInput.end).format("YYYY-MM-DD");

        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;

        this.getTask();

    }

	getEmployee(){
		
		this._common_service.getEmployee().
		subscribe(
			data => {
				this.emp = Array.from(data);
				this.employee = this.emp;
				this.employee_value = [];
				this.options = {
					multiple: true
				}
				this.employee_current = this.employee_value;

				this.btnCreate = true;
			},
			err => console.error(err)
		);
	}
	changedEmployee(data: any) {
		this.employee_current = data.value;

		if(this.employee_current == 0){
			this.filterForm.value.employee_id = null;
		}
		else{
			this.filterForm.value.employee_id = this.employee_current;
		}
			this.getTask();
	}

	editTask(x){

		let emp_id = x.employee_id;

		let v = [];
		v.push(emp_id);
		let employee = v;

		let disposable = this.modalService.addDialog(TaskModalComponent, {
            title:'Edit Task',
            button:'Update',
            edit:true,
            task_id:x.id,
            single:true,
            emp:this.emp,
            employee_id:employee
        	}).subscribe((isConfirmed)=>{
                this.getTask();
                this.dateOption();
        });

	}

	createTask() {
		let disposable = this.modalService.addDialog(TaskModalComponent, {
	        title:'Create Task',
	        button:'Add',
	        create:true,
	        multi:true,
	        emp:this.emp
	    	}).subscribe((isConfirmed)=>{
	            this.getTask();
	            this.dateOption();
	    });
	}

	checkTask(x) {

		let disposable = this.modalService.addDialog(TaskModalComponent, {
	        title:'Check Task',
	        checking:true,
	        task:x.comp_task,
	        id:x.id
	    	}).subscribe((isConfirmed)=>{
	            this.getTask();
	            this.dateOption();
	    });
	}

	viewDetails(x) {

		let start = '';
		let end = '';

		if (x.start_task == null) {
			start = 'No Start Time';
		}else{
			start = x.start_task;
		}

		if (x.end_task == null) {
			end = 'No End Time';
		}else{
			end = x.end_task;
		}

		let disposable = this.modalService.addDialog(TaskModalComponent, {
	        title:'Check Task',
	        viewAll:true,
	        name:x.name,
			task:x.comp_task,
			deadline:x.deadline,
			startTime:start,
			endTime:end,
			nameVal:true
	    	}).subscribe((isConfirmed)=>{
	            this.getTask();
	            this.dateOption();
	    });
	}

	/**
	 * pag done na ng employee yung task nya, it yung function para icheck ng admin kung tapos na nya ba talaga
	 * @param object x [description]
	 */
	approvedTask(x) {

		let data = [];

		data.push({
            id: x.id,
            task: x.comp_task,
            employee_id: x.employee_id
        }); 

		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
			title:'Approved Task',
			message:'Are you sure you want to Approved this Task?',
			action:'Approved',
			id:x.id,
			data:data[0],
            task:true
		}).subscribe((isConfirmed)=>{
			this.getTask();
			this.dateOption();
		});
	}


}
