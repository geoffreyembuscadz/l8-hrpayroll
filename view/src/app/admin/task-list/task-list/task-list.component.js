var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DialogService } from "ng2-bootstrap-modal";
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { TaskService } from '../../../services/task.service';
import { AuthUserService } from '../../../services/auth-user.service';
import * as moment from 'moment';
import { TaskModalComponent } from '../task-modal/task-modal.component';
import { CommonService } from '../../../services/common.service';
import { ConfirmModalComponent } from '../../confirm-modal/confirm-modal.component';
var TaskListComponent = /** @class */ (function () {
    function TaskListComponent(_task_service, _auth_service, _fb, modalService, _common_service, daterangepickerOptions) {
        this._task_service = _task_service;
        this._auth_service = _auth_service;
        this._fb = _fb;
        this.modalService = modalService;
        this._common_service = _common_service;
        this.daterangepickerOptions = daterangepickerOptions;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.task = [];
        this.byDate = false;
        this.byEmployee = false;
        this.btnCreate = false;
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.filterForm = _fb.group({
            'start_date': [null],
            'end_date': [null],
            'employee_id': [null],
            'supervisor_id': [null],
            'role_id': [null]
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.options = {
            multiple: true,
            theme: 'classic',
            closeOnSelect: false
        };
    }
    TaskListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    TaskListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    TaskListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    TaskListComponent.prototype.ngOnInit = function () {
        /**
         * process of task
         * admin can create task for employee and approved it
         * employee can create task for themselves and the admin will approved it
         */
        this.getTask();
        this.getEmployee();
        this.dateOption();
    };
    TaskListComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    TaskListComponent.prototype.choiceFilter = function (id) {
        if (id == 1) {
            this.byDate = true;
            this.byEmployee = true;
        }
        else if (id == 2) {
            if (this.byDate == false) {
                this.byDate = true;
            }
            else {
                this.byDate = false;
                this.filterForm.value.start_date = null;
                this.filterForm.value.end_date = null;
            }
        }
        else if (id == 3) {
            if (this.byEmployee == false) {
                this.byEmployee = true;
            }
            else {
                this.byEmployee = false;
                this.filterForm.value.employee_id = null;
            }
        }
        else {
            this.byDate = false;
            this.byEmployee = false;
            this.filterForm.value.status_id = null;
            this.filterForm.value.company_id = null;
            this.filterForm.value.start_date = null;
            this.filterForm.value.end_date = null;
            this.filterForm.value.employee_id = null;
            this.getTask();
        }
    };
    TaskListComponent.prototype.getTask = function () {
        var _this = this;
        var model = this.filterForm.value;
        this._task_service.getTask(model).
            subscribe(function (data) {
            _this.task = data;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    TaskListComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.getTask();
    };
    TaskListComponent.prototype.getEmployee = function () {
        var _this = this;
        this._common_service.getEmployee().
            subscribe(function (data) {
            _this.emp = Array.from(data);
            _this.employee = _this.emp;
            _this.employee_value = [];
            _this.options = {
                multiple: true
            };
            _this.employee_current = _this.employee_value;
            _this.btnCreate = true;
        }, function (err) { return console.error(err); });
    };
    TaskListComponent.prototype.changedEmployee = function (data) {
        this.employee_current = data.value;
        if (this.employee_current == 0) {
            this.filterForm.value.employee_id = null;
        }
        else {
            this.filterForm.value.employee_id = this.employee_current;
        }
        this.getTask();
    };
    TaskListComponent.prototype.editTask = function (x) {
        var _this = this;
        var emp_id = x.employee_id;
        var v = [];
        v.push(emp_id);
        var employee = v;
        var disposable = this.modalService.addDialog(TaskModalComponent, {
            title: 'Edit Task',
            button: 'Update',
            edit: true,
            task_id: x.id,
            single: true,
            emp: this.emp,
            employee_id: employee
        }).subscribe(function (isConfirmed) {
            _this.getTask();
            _this.dateOption();
        });
    };
    TaskListComponent.prototype.createTask = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(TaskModalComponent, {
            title: 'Create Task',
            button: 'Add',
            create: true,
            multi: true,
            emp: this.emp
        }).subscribe(function (isConfirmed) {
            _this.getTask();
            _this.dateOption();
        });
    };
    TaskListComponent.prototype.checkTask = function (x) {
        var _this = this;
        var disposable = this.modalService.addDialog(TaskModalComponent, {
            title: 'Check Task',
            checking: true,
            task: x.comp_task,
            id: x.id
        }).subscribe(function (isConfirmed) {
            _this.getTask();
            _this.dateOption();
        });
    };
    TaskListComponent.prototype.viewDetails = function (x) {
        var _this = this;
        var start = '';
        var end = '';
        if (x.start_task == null) {
            start = 'No Start Time';
        }
        else {
            start = x.start_task;
        }
        if (x.end_task == null) {
            end = 'No End Time';
        }
        else {
            end = x.end_task;
        }
        var disposable = this.modalService.addDialog(TaskModalComponent, {
            title: 'Check Task',
            viewAll: true,
            name: x.name,
            task: x.comp_task,
            deadline: x.deadline,
            startTime: start,
            endTime: end,
            nameVal: true
        }).subscribe(function (isConfirmed) {
            _this.getTask();
            _this.dateOption();
        });
    };
    /**
     * pag done na ng employee yung task nya, it yung function para icheck ng admin kung tapos na nya ba talaga
     * @param object x [description]
     */
    TaskListComponent.prototype.approvedTask = function (x) {
        var _this = this;
        var data = [];
        data.push({
            id: x.id,
            task: x.comp_task,
            employee_id: x.employee_id
        });
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Approved Task',
            message: 'Are you sure you want to Approved this Task?',
            action: 'Approved',
            id: x.id,
            data: data[0],
            task: true
        }).subscribe(function (isConfirmed) {
            _this.getTask();
            _this.dateOption();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], TaskListComponent.prototype, "dtElement", void 0);
    TaskListComponent = __decorate([
        Component({
            selector: 'app-task-list',
            templateUrl: './task-list.component.html',
            styleUrls: ['./task-list.component.css']
        }),
        __metadata("design:paramtypes", [TaskService,
            AuthUserService,
            FormBuilder,
            DialogService,
            CommonService,
            DaterangepickerConfig])
    ], TaskListComponent);
    return TaskListComponent;
}());
export { TaskListComponent };
//# sourceMappingURL=task-list.component.js.map