import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng2-select2';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { TaskService } from '../../../services/task.service';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';

@Component({
  selector: 'app-task-modal',
  templateUrl: './task-modal.component.html',
  styleUrls: ['./task-modal.component.css']
})
export class TaskModalComponent extends DialogComponent<null, boolean> {

 	public success_title;
	public success_message;
	public error_title: string
	public error_message: string;
	public employee : Array<Select2OptionData>;
	public employee_value: Array<Select2OptionData>;
	public options: Select2Options;
	public current: any;
	emp:any;
	public mainInput = {
        start: moment(),
        end: moment().subtract(6, 'month')
    }
    validate = false;
	employee_id:any;
	edit:any;
    create:any;
    multi:any;
    single:any;
    date:any;
    TaskForm:any;
    task:any;
    poststore:any;
    empStats = false;
    dupVal = false;
    task_id:any;
    user = false;
    updateTime = false;
    message:any;
    type:any;
    id:any;
    checking = false;
    receiver_id:any;

    public status=[];
	public status_current : any;
	status_value:any;

	reason = null;
	statVal = false;
	remVal = false;



  constructor(
  	dialogService: DialogService,
   	private _fb: FormBuilder,
   	private daterangepickerOptions: DaterangepickerConfig,
   	private _task_service: TaskService,
  ) {
  	super(dialogService);

  	this.TaskForm = _fb.group({
		'remarks': 			[null],
		'task': 			[null],
		'deadline': 		[null]
    });

    this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
    		showDropdowns: true,
    		timePicker: true,
    		opens: "center"
	}; 
  }

  	ngOnInit() {

  		if (this.updateTime == false) {
	  		if(this.user == true){
	    		this.empStats = true;
	    		this.getEmployee();
	    	}
			if(this.edit == true){
				this.getTask();
	    		this.validate = true;
	    		this.getEmployee();
	    	}
	    	else{
		   		this.getEmployee();
	    	}
  		}

  		if (this.checking == true) {
  			this.TaskForm.value.task = this.task;
  			this.getStatus();
  		}
  	}

  	private catchError(error: any){
		let response_body = error._body;
		let response_status = error.status;

		if( response_status == 500 ){
			this.error_title = 'Error 500';
			this.error_message = 'The given data failed to pass validation.';
		} else if( response_status == 200 ) {
			this.error_title = '';
			this.error_message = '';
		}
	}

	getTask(){

		this._task_service.getTaskById(this.task_id).subscribe(
	      data => {
	        let dat=(data);
	        this.task = dat.task;
	        this.employee_id= dat.employee_id;
	        this.mainInput.start = dat.deadline;
	      },
	      err => console.error(err)
    	);
    	
	}

	getEmployee(){

		this.employee = this.emp;
      	this.employee_value = [];

		if(this.edit == true) {
			this.employee_value = this.employee_id;
			this.validate = true;
	    } 
		this.options = {
			multiple: true
		}
		this.current = this.employee_value;
	}
	
	changed(data: any) {

	  	if(this.single == true){
			let v = [];
			v.push(data.value);
			this.current = v;
		}
		else{
			this.current = data.value;	
		}

	   	let len = this.current.length;

	      if(len >= 1){
	      	this.empStats = true;
	      }
	      else{
	      	this.empStats = false;
	      }

	    this.validation();
	    this.duplicateEntryValidation();
	}

	private selectedDate(value:any, dateInput:any) {
        dateInput.start = value.start;

       	this.date = moment(dateInput.start).format("YYYY-MM-DD HH:mm:ss");

    }

    submit(){

		this.duplicateEntryValidation();

		let model = this.TaskForm.value;

		if(this.edit == true){
			this.TaskForm.value.id= this.task_id;
			model = this.TaskForm.value;

			this._task_service
			.updateTask(model)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}

		if(this.create == true){
			this._task_service
			.createTask(model)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}

	}


    duplicateEntryValidation(){

    	this.TaskForm.value.employee_id = this.current;
		this.TaskForm.value.deadline = this.date;

		if (this.date == null) {
			this.TaskForm.value.deadline = moment(this.mainInput.start).format("YYYY-MM-DD HH:mm:ss");
		}

		if(this.user == true  && this.create == true || this.user == true && this.edit == true){
			let v = [];
			v.push(this.employee_id);
			this.TaskForm.value.employee_id = v;
		}

		if(this.edit == true){
			this.TaskForm.value.edit = this.edit;
			this.TaskForm.value.id= this.task_id;
		}
		else{
			this.TaskForm.value.edit = false;
		}

		this.validation();
		
		// let model = this.TaskForm.value;

		// if(this.TaskForm.value.employee_id != 0 && this.TaskForm.value.task != null || this.edit == true){
		// 	this._task_service
		// 		.duplicateEntryValidation(model)
		// 		.subscribe(
		// 			data => {
		// 				this.poststore = data;
		// 				if(this.poststore.message == "duplicate"){
		// 					this.dupVal = false;
		// 					this.errorCatch();
		// 				}
		// 				else{
		// 					this.dupVal = true;
		// 					this.error_title = '';
		// 					this.error_message = '';
		// 				}
		// 				this.validation();
		// 			},
		// 			err => this.catchError(err)
		// 		);
		// }
    }

    validation(){
    	this.dupVal = true;
		if(this.empStats == true && this.dupVal == true|| this.edit == true){
      		this.validate=true;
     	}
     	else{
     		this.validate=false;
     	}
	}

	errorCatch(){
		if(this.poststore.status_code == 200){
			this.success_title = '200 OK';
			this.success_message = 'No Changes Happen';
			setTimeout(() => {
	  		   this.close();
	      	}, 2000);
		}
		else if(this.poststore.status_code == 400){

			this.error_title = '400 Bad Request';
			this.error_message = 'Task is already exist';
		}
		else{
			this.validate = false;
			this.success_title = "Success";
			this.success_message = "New Task request was successfully added.";
			setTimeout(() => {
	  		   this.close();
	      	}, 2000);
		}
	}

	confirm(){

		if (this.updateTime == true) {
			this.TaskForm.value.id = this.id;
			this.TaskForm.value.type = this.type;
			this.TaskForm.value.employee_id = this.employee_id;
			this.TaskForm.value.datetime = moment().format('YYYY-MM-DD HH:mm:ss');

			let model = this.TaskForm.value;

			this._task_service
			.updateTaskTime(model)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.result=true;
					this.errorCatch();
				},
				err => this.catchError(err)
			);
		}

		if (this.checking == true) {

			this.TaskForm.value.id = this.id;
			this.TaskForm.value.status = this.status_current;
			this.TaskForm.value.remarks = this.reason;

			let model = this.TaskForm.value;

			this._task_service
			.updateStatus(model)
			.subscribe(
				data => {
					this.poststore = data;
					this.validate = false;
					this.result=true;
					this.success_title = "Success";
					this.success_message = "Task request was successfully updated.";
					setTimeout(() => {
			  		   this.close();
			      	}, 2000);
				},
				err => this.catchError(err)
			);
		}

	}

	getStatus(){

        let id = 1;
        let text = 'UNFINISHED';
        this.status.unshift({id,text});
        id = 2;
        text = 'DONE';
        this.status.unshift({id,text});
		id = 0;
        text = 'Select Status';
        this.status.unshift({id,text});
		
		this.status_value = ['0'];
		this.status_current = 0;
	}
	changedStatus(data: any) {
		this.status_current = data.value;

		if (this.status_current != 0) {
			this.statVal = true;
		}
		else{
			this.statVal = false;
		}

		if(this.status_current == 2){
			this.validate = true;
		}else{
			this.val();
		}

	}

	val(){

		if (this.reason != null && this.reason != '') {
			this.remVal = true;
		}else{
			this.remVal = false;
		}
		 
		if (this.statVal == true && this.remVal == true) {
			this.validate = true;
		}else{
			this.validate = false;
		}


	}

}
