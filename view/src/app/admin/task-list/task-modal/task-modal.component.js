var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormBuilder } from '@angular/forms';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { TaskService } from '../../../services/task.service';
import * as moment from 'moment';
var TaskModalComponent = /** @class */ (function (_super) {
    __extends(TaskModalComponent, _super);
    function TaskModalComponent(dialogService, _fb, daterangepickerOptions, _task_service) {
        var _this = _super.call(this, dialogService) || this;
        _this._fb = _fb;
        _this.daterangepickerOptions = daterangepickerOptions;
        _this._task_service = _task_service;
        _this.mainInput = {
            start: moment(),
            end: moment().subtract(6, 'month')
        };
        _this.validate = false;
        _this.empStats = false;
        _this.dupVal = false;
        _this.user = false;
        _this.updateTime = false;
        _this.checking = false;
        _this.status = [];
        _this.reason = null;
        _this.statVal = false;
        _this.remVal = false;
        _this.TaskForm = _fb.group({
            'remarks': [null],
            'task': [null],
            'deadline': [null]
        });
        _this.daterangepickerOptions.settings = {
            locale: { format: 'MM/DD/YYYY' },
            alwaysShowCalendars: false,
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: true,
            opens: "center"
        };
        return _this;
    }
    TaskModalComponent.prototype.ngOnInit = function () {
        if (this.updateTime == false) {
            if (this.user == true) {
                this.empStats = true;
                this.getEmployee();
            }
            if (this.edit == true) {
                this.getTask();
                this.validate = true;
                this.getEmployee();
            }
            else {
                this.getEmployee();
            }
        }
        if (this.checking == true) {
            this.TaskForm.value.task = this.task;
            this.getStatus();
        }
    };
    TaskModalComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    TaskModalComponent.prototype.getTask = function () {
        var _this = this;
        this._task_service.getTaskById(this.task_id).subscribe(function (data) {
            var dat = (data);
            _this.task = dat.task;
            _this.employee_id = dat.employee_id;
            _this.mainInput.start = dat.deadline;
        }, function (err) { return console.error(err); });
    };
    TaskModalComponent.prototype.getEmployee = function () {
        this.employee = this.emp;
        this.employee_value = [];
        if (this.edit == true) {
            this.employee_value = this.employee_id;
            this.validate = true;
        }
        this.options = {
            multiple: true
        };
        this.current = this.employee_value;
    };
    TaskModalComponent.prototype.changed = function (data) {
        if (this.single == true) {
            var v = [];
            v.push(data.value);
            this.current = v;
        }
        else {
            this.current = data.value;
        }
        var len = this.current.length;
        if (len >= 1) {
            this.empStats = true;
        }
        else {
            this.empStats = false;
        }
        this.validation();
        this.duplicateEntryValidation();
    };
    TaskModalComponent.prototype.selectedDate = function (value, dateInput) {
        dateInput.start = value.start;
        this.date = moment(dateInput.start).format("YYYY-MM-DD HH:mm:ss");
    };
    TaskModalComponent.prototype.submit = function () {
        var _this = this;
        this.duplicateEntryValidation();
        var model = this.TaskForm.value;
        if (this.edit == true) {
            this.TaskForm.value.id = this.task_id;
            model = this.TaskForm.value;
            this._task_service
                .updateTask(model)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.result = true;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        if (this.create == true) {
            this._task_service
                .createTask(model)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
    };
    TaskModalComponent.prototype.duplicateEntryValidation = function () {
        this.TaskForm.value.employee_id = this.current;
        this.TaskForm.value.deadline = this.date;
        if (this.date == null) {
            this.TaskForm.value.deadline = moment(this.mainInput.start).format("YYYY-MM-DD HH:mm:ss");
        }
        if (this.user == true && this.create == true || this.user == true && this.edit == true) {
            var v = [];
            v.push(this.employee_id);
            this.TaskForm.value.employee_id = v;
        }
        if (this.edit == true) {
            this.TaskForm.value.edit = this.edit;
            this.TaskForm.value.id = this.task_id;
        }
        else {
            this.TaskForm.value.edit = false;
        }
        this.validation();
        // let model = this.TaskForm.value;
        // if(this.TaskForm.value.employee_id != 0 && this.TaskForm.value.task != null || this.edit == true){
        // 	this._task_service
        // 		.duplicateEntryValidation(model)
        // 		.subscribe(
        // 			data => {
        // 				this.poststore = data;
        // 				if(this.poststore.message == "duplicate"){
        // 					this.dupVal = false;
        // 					this.errorCatch();
        // 				}
        // 				else{
        // 					this.dupVal = true;
        // 					this.error_title = '';
        // 					this.error_message = '';
        // 				}
        // 				this.validation();
        // 			},
        // 			err => this.catchError(err)
        // 		);
        // }
    };
    TaskModalComponent.prototype.validation = function () {
        this.dupVal = true;
        if (this.empStats == true && this.dupVal == true || this.edit == true) {
            this.validate = true;
        }
        else {
            this.validate = false;
        }
    };
    TaskModalComponent.prototype.errorCatch = function () {
        var _this = this;
        if (this.poststore.status_code == 200) {
            this.success_title = '200 OK';
            this.success_message = 'No Changes Happen';
            setTimeout(function () {
                _this.close();
            }, 2000);
        }
        else if (this.poststore.status_code == 400) {
            this.error_title = '400 Bad Request';
            this.error_message = 'Task is already exist';
        }
        else {
            this.validate = false;
            this.success_title = "Success";
            this.success_message = "New Task request was successfully added.";
            setTimeout(function () {
                _this.close();
            }, 2000);
        }
    };
    TaskModalComponent.prototype.confirm = function () {
        var _this = this;
        if (this.updateTime == true) {
            this.TaskForm.value.id = this.id;
            this.TaskForm.value.type = this.type;
            this.TaskForm.value.employee_id = this.employee_id;
            this.TaskForm.value.datetime = moment().format('YYYY-MM-DD HH:mm:ss');
            var model = this.TaskForm.value;
            this._task_service
                .updateTaskTime(model)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.result = true;
                _this.errorCatch();
            }, function (err) { return _this.catchError(err); });
        }
        if (this.checking == true) {
            this.TaskForm.value.id = this.id;
            this.TaskForm.value.status = this.status_current;
            this.TaskForm.value.remarks = this.reason;
            var model = this.TaskForm.value;
            this._task_service
                .updateStatus(model)
                .subscribe(function (data) {
                _this.poststore = data;
                _this.validate = false;
                _this.result = true;
                _this.success_title = "Success";
                _this.success_message = "Task request was successfully updated.";
                setTimeout(function () {
                    _this.close();
                }, 2000);
            }, function (err) { return _this.catchError(err); });
        }
    };
    TaskModalComponent.prototype.getStatus = function () {
        var id = 1;
        var text = 'UNFINISHED';
        this.status.unshift({ id: id, text: text });
        id = 2;
        text = 'DONE';
        this.status.unshift({ id: id, text: text });
        id = 0;
        text = 'Select Status';
        this.status.unshift({ id: id, text: text });
        this.status_value = ['0'];
        this.status_current = 0;
    };
    TaskModalComponent.prototype.changedStatus = function (data) {
        this.status_current = data.value;
        if (this.status_current != 0) {
            this.statVal = true;
        }
        else {
            this.statVal = false;
        }
        if (this.status_current == 2) {
            this.validate = true;
        }
        else {
            this.val();
        }
    };
    TaskModalComponent.prototype.val = function () {
        if (this.reason != null && this.reason != '') {
            this.remVal = true;
        }
        else {
            this.remVal = false;
        }
        if (this.statVal == true && this.remVal == true) {
            this.validate = true;
        }
        else {
            this.validate = false;
        }
    };
    TaskModalComponent = __decorate([
        Component({
            selector: 'app-task-modal',
            templateUrl: './task-modal.component.html',
            styleUrls: ['./task-modal.component.css']
        }),
        __metadata("design:paramtypes", [DialogService,
            FormBuilder,
            DaterangepickerConfig,
            TaskService])
    ], TaskModalComponent);
    return TaskModalComponent;
}(DialogComponent));
export { TaskModalComponent };
//# sourceMappingURL=task-modal.component.js.map