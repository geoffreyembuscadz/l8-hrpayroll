var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DialogService } from "ng2-bootstrap-modal";
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import 'rxjs/add/operator/map';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { TaskService } from '../../../services/task.service';
import { AuthUserService } from '../../../services/auth-user.service';
import * as moment from 'moment';
import { TaskModalComponent } from '../task-modal/task-modal.component';
import { CommonService } from '../../../services/common.service';
var MyTaskListComponent = /** @class */ (function () {
    function MyTaskListComponent(_task_service, _auth_service, _fb, modalService, _common_service, daterangepickerOptions) {
        this._task_service = _task_service;
        this._auth_service = _auth_service;
        this._fb = _fb;
        this.modalService = modalService;
        this._common_service = _common_service;
        this.daterangepickerOptions = daterangepickerOptions;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.task = [];
        this.mainInput = {
            start: moment().subtract(12, 'month'),
            end: moment().subtract(11, 'month')
        };
        this.filterForm = _fb.group({
            'start_date': [null],
            'end_date': [null],
            'employee_id': [null]
        });
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    MyTaskListComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    MyTaskListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    MyTaskListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    MyTaskListComponent.prototype.ngOnInit = function () {
        this.getTask();
        this.dateOption();
    };
    MyTaskListComponent.prototype.dateOption = function () {
        this.daterangepickerOptions.settings = {
            singleDatePicker: false
        };
    };
    MyTaskListComponent.prototype.getTask = function () {
        var _this = this;
        this.filterForm.value.employee_id = this.employee_id;
        var model = this.filterForm.value;
        this._task_service.getMyTask(model).
            subscribe(function (data) {
            _this.task = data;
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    MyTaskListComponent.prototype.filterDate = function (value, dateInput) {
        dateInput.start = value.start;
        dateInput.end = value.end;
        var start_date = moment(dateInput.start).format("YYYY-MM-DD");
        var end_date = moment(dateInput.end).format("YYYY-MM-DD");
        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;
        this.getTask();
    };
    MyTaskListComponent.prototype.createTask = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(TaskModalComponent, {
            title: 'Create Task',
            button: 'Add',
            create: true,
            employee_id: this.employee_id,
            single: false,
            user: true,
            user_id: this.user_id,
            supervisor_id: this.supervisor_id,
            messenger_id: this.employee_id
        }).subscribe(function (isConfirmed) {
            _this.ngOnInit();
        });
    };
    MyTaskListComponent.prototype.editTask = function (x) {
        var _this = this;
        var disposable = this.modalService.addDialog(TaskModalComponent, {
            title: 'Edit Task',
            button: 'Update',
            edit: true,
            task_id: x.id,
            task: x.task,
            user: true,
            employee_id: this.employee_id,
            user_id: this.user_id
        }).subscribe(function (isConfirmed) {
            _this.ngOnInit();
        });
    };
    MyTaskListComponent.prototype.updateTime = function (type, x) {
        var _this = this;
        var id = x.id;
        var start = x.start_task;
        var status = x.status;
        var employee_id = x.employee_id;
        if (status != 'FOR APPROVAL') {
            if (type == 'end' && start == null) {
            }
            else {
                var message = '';
                var types = '';
                if (type == 'start') {
                    types = 'start';
                    message = 'Are you sure you want to Start this task?';
                }
                else if (type == 'end') {
                    types = 'end';
                    message = 'Are you sure you want to End this task?';
                }
                var disposable = this.modalService.addDialog(TaskModalComponent, {
                    title: 'Task',
                    button: 'Add',
                    message: message,
                    type: types,
                    id: id,
                    updateTime: true,
                    validate: true,
                    employee_id: employee_id
                }).subscribe(function (isConfirmed) {
                    _this.ngOnInit();
                });
            }
        }
    };
    MyTaskListComponent.prototype.viewDetails = function (x) {
        var _this = this;
        var start = '';
        var end = '';
        if (x.start_task == null) {
            start = 'No Start Time';
        }
        else {
            start = x.start_task;
        }
        if (x.end_task == null) {
            end = 'No End Time';
        }
        else {
            end = x.end_task;
        }
        var disposable = this.modalService.addDialog(TaskModalComponent, {
            title: 'Check Task',
            viewAll: true,
            name: x.name,
            task: x.comp_task,
            deadline: x.deadline,
            startTime: start,
            endTime: end,
            user_id: this.user_id,
            supervisor_id: this.supervisor_id,
            messenger_id: this.employee_id
        }).subscribe(function (isConfirmed) {
            _this.ngOnInit();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], MyTaskListComponent.prototype, "dtElement", void 0);
    MyTaskListComponent = __decorate([
        Component({
            selector: 'app-my-task-list',
            templateUrl: './my-task-list.component.html',
            styleUrls: ['./my-task-list.component.css']
        }),
        __metadata("design:paramtypes", [TaskService,
            AuthUserService,
            FormBuilder,
            DialogService,
            CommonService,
            DaterangepickerConfig])
    ], MyTaskListComponent);
    return MyTaskListComponent;
}());
export { MyTaskListComponent };
//# sourceMappingURL=my-task-list.component.js.map