import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configuration } from '../../../app.config';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { TaskService } from '../../../services/task.service';
import { AuthUserService } from '../../../services/auth-user.service';
import { MomentModule } from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { TaskModalComponent } from '../task-modal/task-modal.component';
import { CommonService } from '../../../services/common.service';
import { Select2OptionData } from 'ng2-select2';

@Component({
  selector: 'app-my-task-list',
  templateUrl: './my-task-list.component.html',
  styleUrls: ['./my-task-list.component.css']
})
export class MyTaskListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

	dtOptions: any = {};
	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

	task = [];
	user_id
	supervisor_id
	employee_id
	filterForm:any;


	public mainInput = {
        start: moment().subtract(12, 'month'),
        end: moment().subtract(11, 'month')
    }

  constructor(
  		private _task_service: TaskService,
   		private _auth_service: AuthUserService,
   		private _fb: FormBuilder,
   		private modalService: DialogService,
   		private _common_service: CommonService,
   		private daterangepickerOptions: DaterangepickerConfig
   	) {

  		this.filterForm = _fb.group({
			'start_date': 		[null],
			'end_date': 		[null],
			'employee_id': 		[null]
   		});

  		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");
   	
   	}

   	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

  	ngOnInit() {
  		this.getTask();
  		this.dateOption();
  	}

  	dateOption(){
    	this.daterangepickerOptions.settings = {
        	singleDatePicker: false
		};
    }

  	getTask(){

  		this.filterForm.value.employee_id = this.employee_id;
  		let model = this.filterForm.value;

		this._task_service.getMyTask(model).
		subscribe(
			data => {
				this.task = data;
				this.rerender();
			},
			err => console.error(err)
		);
	}

	filterDate(value:any, dateInput:any) {
        dateInput.start = value.start;
        dateInput.end = value.end;

        let start_date = moment(dateInput.start).format("YYYY-MM-DD");
        let end_date = moment(dateInput.end).format("YYYY-MM-DD");

        this.filterForm.value.start_date = start_date;
        this.filterForm.value.end_date = end_date;

        this.getTask();

    }


	createTask() {
		let disposable = this.modalService.addDialog(TaskModalComponent, {
            title:'Create Task',
            button:'Add',
            create:true,
            employee_id:this.employee_id,
            single:false,
            user:true,
            user_id:this.user_id,
            supervisor_id:this.supervisor_id,
            messenger_id:this.employee_id
        	}).subscribe((isConfirmed)=>{
                this.ngOnInit();
            });
	}

	editTask(x){

		let disposable = this.modalService.addDialog(TaskModalComponent, {
            title:'Edit Task',
            button:'Update',
            edit:true,
            task_id:x.id,
            task:x.task,
            user:true,
            employee_id:this.employee_id,
            user_id:this.user_id
        	}).subscribe((isConfirmed)=>{
                this.ngOnInit();
        });

	}

	updateTime(type,x) {

		let id = x.id;
		let start = x.start_task;
		let status = x.status;
		let employee_id = x.employee_id;

		if (status != 'FOR APPROVAL') {
			
			if (type == 'end' && start == null) {

			}
			else{

				let message = '';
				let types = '';

				if (type == 'start') {
					types = 'start';
					message = 'Are you sure you want to Start this task?';
				}
				else if(type == 'end'){
					types = 'end';
					message = 'Are you sure you want to End this task?';
				}

				let disposable = this.modalService.addDialog(TaskModalComponent, {
		            title:'Task',
		            button:'Add',
		            message:message,
		            type:types,
		            id:id,
		            updateTime:true,
		            validate:true,
		            employee_id:employee_id
		        	}).subscribe((isConfirmed)=>{
		                this.ngOnInit();
		            });

			}
		}

	}

	viewDetails(x) {

		let start = '';
		let end = '';

		if (x.start_task == null) {
			start = 'No Start Time';
		}else{
			start = x.start_task;
		}

		if (x.end_task == null) {
			end = 'No End Time';
		}else{
			end = x.end_task;
		}

		let disposable = this.modalService.addDialog(TaskModalComponent, {
	        title:'Check Task',
	        viewAll:true,
	        name:x.name,
			task:x.comp_task,
			deadline:x.deadline,
			startTime:start,
			endTime:end,
	        user_id:this.user_id,
	        supervisor_id:this.supervisor_id,
	        messenger_id:this.employee_id
	    	}).subscribe((isConfirmed)=>{
	            this.ngOnInit();
	    });
	}


}
