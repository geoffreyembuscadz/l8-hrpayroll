var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Injectable, ViewChild } from '@angular/core'; //1
import { PositionService } from '../../services/position.service';
import { Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Position } from '../../model/position';
import { DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { TypeModalComponent } from '../type-modal/type-modal.component';
var PositionComponent = /** @class */ (function () {
    function PositionComponent(dialogService, _perms_service, modalService, _ar, _position_service, _rt, _fb, _conf) {
        this._perms_service = _perms_service;
        this.modalService = modalService;
        this._ar = _ar;
        this._position_service = _position_service;
        this._rt = _rt;
        this._fb = _fb;
        this._conf = _conf;
        this.dtTrigger = new Subject();
        this.position = new Position();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.api_position = this._conf.ServerWithApiUrl + 'position/';
    }
    PositionComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    PositionComponent.prototype.showMe = function (positions) {
        this.position_rec = positions;
    };
    PositionComponent.prototype.ngOnInit = function () {
        this.positions();
    };
    PositionComponent.prototype.ngOnDestroy = function () {
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    PositionComponent.prototype.createPosition = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Add Position',
            button: 'Add',
            url: 'position',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    PositionComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            dtInstance.destroy();
            _this.dtTrigger.next();
        });
    };
    PositionComponent.prototype.positions = function () {
        var _this = this;
        this._position_service.position().
            subscribe(function (data) {
            _this.pos = Array.from(data);
            _this.rerender();
        }, function (err) { return console.error(err); });
    };
    PositionComponent.prototype.editPosition = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Update Position',
            button: 'Update',
            edit: true,
            url: 'position',
            id: id
        }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    PositionComponent.prototype.archive = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Data',
            message: 'Are you sure you want to archive this data?',
            action: 'Delete',
            id: id,
            url: 'position'
        }, { backdropColor: 'rgba(238,238,238)' }).subscribe(function (isConfirmed) {
            _this.rerender();
            _this.ngOnInit();
        });
    };
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], PositionComponent.prototype, "dtElement", void 0);
    PositionComponent = __decorate([
        Component({
            selector: 'app-position',
            templateUrl: './position.component.html',
            styleUrls: ['./position.component.css']
        }),
        Injectable(),
        __metadata("design:paramtypes", [DialogService,
            PermissionService,
            DialogService,
            ActivatedRoute,
            PositionService,
            Router,
            FormBuilder,
            Configuration])
    ], PositionComponent);
    return PositionComponent;
}());
export { PositionComponent };
//# sourceMappingURL=position.component.js.map