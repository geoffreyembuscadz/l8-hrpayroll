import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';//1
import { PositionService } from '../../services/position.service';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Configuration } from '../../app.config';
import { Position } from '../../model/position';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { PermissionService } from '../../services/permission.service';
import { Observable } from 'rxjs/Observable';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { TypeModalComponent } from '../type-modal/type-modal.component';


@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.css']
})

@Injectable()
export class PositionComponent implements OnInit,  AfterViewInit {

	@ViewChild(DataTableDirective)
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	public id: string;
	public position_id: any;
	public error_title: string;
	public error_message: string;
	public success_title;
	public success_message;
	public poststore: any;
	public position = new Position();
	positionForm: FormGroup;
	pos: any;
	public confirm_archiving: any;
	public form
	private headers: Headers;
	dtOptions: any = {};
	position_rec: any;
	// get permissions list
    perm_rec: any;
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];

  	api_position: String;

  	constructor(
  				dialogService: DialogService,
  				private _perms_service: PermissionService,
  				private modalService: DialogService,
  				private _ar: ActivatedRoute,
  				private _position_service: PositionService,
  				private _rt: Router,
  				private _fb: FormBuilder,
  				private _conf: Configuration
  				){
		this.body.classList.add("skin-blue");
	    this.body.classList.add("sidebar-mini");

	      // Header Variables
		this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.api_position = this._conf.ServerWithApiUrl + 'position/';
	}

	ngAfterViewInit(){
		this.dtTrigger.next();
	}

	showMe(positions: any){
		this.position_rec = positions;
	}

	ngOnInit() {
		this.positions();

	}

	ngOnDestroy() {
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	 }


	createPosition() {
		let disposable = this.modalService.addDialog(TypeModalComponent, {
            title:'Add Position',
            button:'Add',
		    url:'position',
		    create:true
        	}).subscribe((isConfirmed)=>{
                this.rerender();
                this.ngOnInit();
            });
		}

	rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      dtInstance.destroy();
	      this.dtTrigger.next();
	    });
    }

    positions(){
    	this._position_service.position().
    	subscribe(
    		data => {
    			this.pos = Array.from(data);
    			this.rerender();
    		},
    		err => console.error(err)
    		);
    }
    editPosition(id:any) {
		let disposable = this.modalService.addDialog(TypeModalComponent, {
            title:'Update Position',
            button:'Update',
		    edit:true,
		    url:'position',
		    id:id
        	}).subscribe((isConfirmed)=>{
                this.rerender();
                this.ngOnInit();
        });
	}
	archive(id:any){

  		let disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title:'Archive Data',
            message:'Are you sure you want to archive this data?',
            action:'Delete',
            id:id,
            url:'position'
        	}, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
		       	this.rerender();
                this.ngOnInit();
        });
	}
}
