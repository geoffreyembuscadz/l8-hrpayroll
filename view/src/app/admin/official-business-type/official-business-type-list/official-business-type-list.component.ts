import { Component, OnInit, Injectable, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from '../../../app.config';
import { OfficialBusinessTypeService } from '../../../services/official-business-type.service';
import { TypeModalComponent } from '../../type-modal/type-modal.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';

@Component({
  selector: 'app-official-business-type-list',
  templateUrl: './official-business-type-list.component.html',
  styleUrls: ['./official-business-type-list.component.css']
})
export class OfficialBusinessTypeListComponent implements OnInit, AfterViewInit {

	@ViewChild(DataTableDirective)//4
	  dtElement: DataTableDirective;
	  dtTrigger: Subject<any> = new Subject();

  	dtOptions: any = {};
	api: String;
	ob_id:any;

	bodyClasses:string = "skin-blue sidebar-mini";
	body = document.getElementsByTagName('body')[0];

  constructor(
  	private _conf: Configuration,
  	private modalService: DialogService,
  	private _OBtypeservice: OfficialBusinessTypeService
  	) { 

  	this.body.classList.add("skin-blue");
	this.body.classList.add("sidebar-mini");

	this.api = this._conf.ServerWithApiUrl + 'official_business_type/';
  }

  ngOnInit() {

  	let authToken = localStorage.getItem('id_token');

		this.dtOptions = {
			ajax:{
				url: this.api,
				type: "GET",
				beforeSend: function(xhr){
                            xhr.setRequestHeader("Authorization",
                            "Bearer " + authToken);
            	},

	        },
			columns: [{
				title: 'ID',
				data: 'id'
			},{
				title: 'Name',
				data: 'name'
			},{
				title: 'Description',
				data: 'description'
			},{
				title: 'Action',
				defaultContent: '<button class="btn btn-primary">Edit</button>'
			}],
			rowCallback: (nRow: number, data: any, aData: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
				let self = this;
				// Unbind first in order to avoid any duplicate handler
				// (see https://github.com/l-lin/angular-datatables/issues/87)
				$('td', nRow).unbind('click');
				$('td', nRow).bind('click', () => {

					this.ob_id = data.id;
					this.editOBtype();
				});
				return nRow;
			},
			
		};

  }

	editOBtype() {
		let disposable = this.modalService.addDialog(TypeModalComponent, {
		    title:'Edit Official Business Type',
		    button:'Update',
		    edit:true,
		    url:'official_business_type',
		    id:this.ob_id
			}).subscribe((isConfirmed)=>{
		        this.rerender();
		    });
	}

	createOBType(){
		let disposable = this.modalService.addDialog(TypeModalComponent, {
		    title:'Create Official Business Type',
		    button:'Add',
		    url:'official_business_type',
		    create:true
			}).subscribe((isConfirmed)=>{
		        this.rerender();
		    });
	}

	ngOnDestroy() {
	    //remove the the body classes
	    this.body.classList.remove("skin-blue");
	    this.body.classList.remove("sidebar-mini");
	}

	ngAfterViewInit(): void {
	    this.dtTrigger.next();
	  }

    rerender(): void {
	    this.dtElement.dtInstance.then((dtInstance) => {
	      // Destroy the table first
	      dtInstance.destroy();
	      // Call the dtTrigger to rerender again
	      this.dtTrigger.next();
	    });
    }

}
