var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";
import { Configuration } from '../../../app.config';
import { OfficialBusinessTypeService } from '../../../services/official-business-type.service';
import { TypeModalComponent } from '../../type-modal/type-modal.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
var OfficialBusinessTypeListComponent = /** @class */ (function () {
    function OfficialBusinessTypeListComponent(_conf, modalService, _OBtypeservice) {
        this._conf = _conf;
        this.modalService = modalService;
        this._OBtypeservice = _OBtypeservice;
        this.dtTrigger = new Subject();
        this.dtOptions = {};
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        this.api = this._conf.ServerWithApiUrl + 'official_business_type/';
    }
    OfficialBusinessTypeListComponent.prototype.ngOnInit = function () {
        var _this = this;
        var authToken = localStorage.getItem('id_token');
        this.dtOptions = {
            ajax: {
                url: this.api,
                type: "GET",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + authToken);
                },
            },
            columns: [{
                    title: 'ID',
                    data: 'id'
                }, {
                    title: 'Name',
                    data: 'name'
                }, {
                    title: 'Description',
                    data: 'description'
                }, {
                    title: 'Action',
                    defaultContent: '<button class="btn btn-primary">Edit</button>'
                }],
            rowCallback: function (nRow, data, aData, iDisplayIndex, iDisplayIndexFull) {
                var self = _this;
                // Unbind first in order to avoid any duplicate handler
                // (see https://github.com/l-lin/angular-datatables/issues/87)
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', function () {
                    _this.ob_id = data.id;
                    _this.editOBtype();
                });
                return nRow;
            },
        };
    };
    OfficialBusinessTypeListComponent.prototype.editOBtype = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Edit Official Business Type',
            button: 'Update',
            edit: true,
            url: 'official_business_type',
            id: this.ob_id
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    OfficialBusinessTypeListComponent.prototype.createOBType = function () {
        var _this = this;
        var disposable = this.modalService.addDialog(TypeModalComponent, {
            title: 'Create Official Business Type',
            button: 'Add',
            url: 'official_business_type',
            create: true
        }).subscribe(function (isConfirmed) {
            _this.rerender();
        });
    };
    OfficialBusinessTypeListComponent.prototype.ngOnDestroy = function () {
        //remove the the body classes
        this.body.classList.remove("skin-blue");
        this.body.classList.remove("sidebar-mini");
    };
    OfficialBusinessTypeListComponent.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
    };
    OfficialBusinessTypeListComponent.prototype.rerender = function () {
        var _this = this;
        this.dtElement.dtInstance.then(function (dtInstance) {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            _this.dtTrigger.next();
        });
    };
    __decorate([
        ViewChild(DataTableDirective) //4
        ,
        __metadata("design:type", DataTableDirective)
    ], OfficialBusinessTypeListComponent.prototype, "dtElement", void 0);
    OfficialBusinessTypeListComponent = __decorate([
        Component({
            selector: 'app-official-business-type-list',
            templateUrl: './official-business-type-list.component.html',
            styleUrls: ['./official-business-type-list.component.css']
        }),
        __metadata("design:paramtypes", [Configuration,
            DialogService,
            OfficialBusinessTypeService])
    ], OfficialBusinessTypeListComponent);
    return OfficialBusinessTypeListComponent;
}());
export { OfficialBusinessTypeListComponent };
//# sourceMappingURL=official-business-type-list.component.js.map