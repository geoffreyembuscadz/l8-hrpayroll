import { Component, OnInit } from '@angular/core';

import { AuthUserService } from '../../services/auth-user.service';
import { MainSideService } from '../../services/main-side.service';

import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';



@Component({
  selector: 'employee-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard1.component.css']
})
export class DashboardComponent implements OnInit {
  user_rec: any;
  user_id_rec: any;
  user_side: any;

  bodyClasses:string = "skin-blue sidebar-mini";
  body = document.getElementsByTagName('body')[0];

  private isLoading: boolean = false;
  
  private roles: any;
  private role_id: any;
  private user_id: any;

  constructor(private _auth_user_service: AuthUserService, private rt: Router) { 
    
    this.getUser();
    this.getUserById();

    this.isLoading = false;

    this.body.classList.add("skin-blue");
    this.body.classList.add("sidebar-mini");
    
  }

  getUser() {
    this._auth_user_service.getUser()
      .subscribe(
      data => {
        this.user_id = data.id;
        this.getUserById();
      }
    );
  }

  getUserById() {
    this._auth_user_service.getUserById(this.user_id)
    .subscribe(
      data => {
          this.user_id_rec = data;
          this.roles = this.user_id_rec.role_name; 
          if (this.roles != undefined) {
            if (this.roles == 'user') {
                this.rt.navigate(['/admin/employee-dashboard']);
            } else {
                this.rt.navigate(['/admin/panel']);
            }
          }
        
      }
    );
  }

  ngOnInit() {




   
  }

}
