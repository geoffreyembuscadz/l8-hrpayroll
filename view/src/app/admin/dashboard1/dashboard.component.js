var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AuthUserService } from '../../services/auth-user.service';
import { Router } from '@angular/router';
var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(_auth_user_service, rt) {
        this._auth_user_service = _auth_user_service;
        this.rt = rt;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.isLoading = false;
        this.getUser();
        this.getUserById();
        this.isLoading = false;
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
    }
    DashboardComponent.prototype.getUser = function () {
        var _this = this;
        this._auth_user_service.getUser()
            .subscribe(function (data) {
            _this.user_id = data.id;
            _this.getUserById();
        });
    };
    DashboardComponent.prototype.getUserById = function () {
        var _this = this;
        this._auth_user_service.getUserById(this.user_id)
            .subscribe(function (data) {
            _this.user_id_rec = data;
            _this.roles = _this.user_id_rec.role_name;
            if (_this.roles != undefined) {
                if (_this.roles == 'user') {
                    _this.rt.navigate(['/admin/employee-dashboard']);
                }
                else {
                    _this.rt.navigate(['/admin/panel']);
                }
            }
        });
    };
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent = __decorate([
        Component({
            selector: 'employee-dashboard',
            templateUrl: './dashboard.component.html',
            styleUrls: ['./dashboard1.component.css']
        }),
        __metadata("design:paramtypes", [AuthUserService, Router])
    ], DashboardComponent);
    return DashboardComponent;
}());
export { DashboardComponent };
//# sourceMappingURL=dashboard.component.js.map