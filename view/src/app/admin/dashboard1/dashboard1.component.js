var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, NgZone } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { DialogService } from "ng2-bootstrap-modal";
import { Configuration } from '../../app.config';
import 'rxjs/add/operator/map';
import { EventComponent } from '../event/event.component';
import { EventService } from '../../services/event.service';
import { MemoService } from '../../services/memo.service';
import { AttendanceService } from '../../services/attendance.service';
import * as $ from 'jquery';
import { LeaveService } from '../../services/leave.service';
import { Dashboard1Service } from '../../services/dashboard1.service';
import * as moment from 'moment';
import { AuthUserService } from '../../services/auth-user.service';
import { MemoModalComponent } from '../memo/memo-modal/memo-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { RemarksModalComponent } from '../remarks-modal/remarks-modal.component';
import { ActionCenterModalComponent } from '../action-center/action-center-modal/action-center-modal.component';
var Dashboard1Component = /** @class */ (function () {
    function Dashboard1Component(_http, modalService, _ngZone, dialogService, _event_service, _attend_service, _leave_service, _dash_service, _memo_service, _auth_service, _conf) {
        this._http = _http;
        this.modalService = modalService;
        this._event_service = _event_service;
        this._attend_service = _attend_service;
        this._leave_service = _leave_service;
        this._dash_service = _dash_service;
        this._memo_service = _memo_service;
        this._auth_service = _auth_service;
        this.bodyClasses = "skin-blue sidebar-mini";
        this.body = document.getElementsByTagName('body')[0];
        this.calendarOptions = {};
        this.emp_request = [];
        this.today = moment();
        this.lineChartData = [];
        this.lineChartLabels = [];
        this.lineChartType = 'line';
        this.pieChartType = 'pie';
        this.lineChartOptions = {
            responsive: true,
            maintainAspectRatio: false
        };
        this.chartColors = [
            {
                backgroundColor: 'rgba(225,10,24,0.2)',
                borderColor: 'rgba(225,10,24,0.2)',
                pointBackgroundColor: 'rgba(225,10,24,0.2)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(225,10,24,0.2)'
            },
            {
                backgroundColor: 'rgba(225,10,24,0.2)',
                borderColor: 'rgba(225,10,24,0.2)',
                pointBackgroundColor: 'rgba(225,10,24,0.2)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(225,10,24,0.2)'
            }
        ];
        this.daterange = {};
        this.option = {
            locale: { format: 'YYYY-MM-DD' },
            alwaysShowCalendars: false,
        };
        var app_config = _conf;
        var authToken = localStorage.getItem('id_token');
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.body.classList.add("skin-blue");
        this.body.classList.add("sidebar-mini");
        var self = this;
        this.calendarOptions = {
            height: 'parent',
            fixedWeekCount: false,
            defaultDate: this.today,
            editable: true,
            eventLimit: true,
            background: '#007c08',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay',
                height: 650
            },
            events: function (start, end, timezone, callback) {
                $.ajax({
                    url: app_config.ServerWithApiUrl + 'event/show',
                    dataType: 'json',
                    data: {
                        // our hypothetical feed requires UNIX timestamps
                        start: start.unix(),
                        end: end.unix()
                    },
                    success: function (data) {
                        var events = data;
                        callback(events);
                    }
                });
            },
            eventClick: function (event, jsEvent, view, date) {
                this.event_id = event.id;
                self.editModal(this.event_id);
            },
            dayClick: function (date, jsEvent, view) {
                self.addModal();
            },
        };
    }
    Dashboard1Component.prototype.chartHovered = function (e) {
    };
    Dashboard1Component.prototype.selectedDate = function (value) {
        this.daterange.start = value.start;
        this.daterange.end = value.end;
    };
    Dashboard1Component.prototype.chartClicked = function (e) {
    };
    Dashboard1Component.prototype.ngOnInit = function () {
        // this.getEmpRequest();
        this.get_UserId();
        // this.getTotalEmployee();
        // this.getComputeAbsent();
        // this.showEvent();
        // this.showLeavePending();
        // this.getEmpLate();
        // this.getMemo();
        // this.getCompanyActive();
        // this.getEmployeeActive();
        this.getTotalAttendance();
        this.getPendingRequest();
        this.getEmpOnLeave();
        this.getEmpCount();
        this.filterTotalEmployee();
    };
    Dashboard1Component.prototype.get_UserId = function () {
        var _this = this;
        this._auth_service.getUser().
            subscribe(function (data) {
            var user = data; // fetched 
            _this.user_id = user.id;
            _this.getEmployeeId();
        }, function (err) { return console.error(err); });
    };
    Dashboard1Component.prototype.getEmployeeId = function () {
        var _this = this;
        this._auth_service.getUserById(this.user_id).
            subscribe(function (data) {
            var user = data; // fetched 
            _this.employee_id = user.employee_id;
        }, function (err) { return console.error(err); });
    };
    Dashboard1Component.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            this.error_message = 'The given data failed to pass validation.';
        }
        else if (response_status == 200) {
            this.error_title = '';
            this.error_message = '';
        }
    };
    Dashboard1Component.prototype.showEvent = function () {
        var _this = this;
        this._event_service.getEventList()
            .subscribe(function (data) {
            _this.events = data;
            var event = _this.events.length;
            for (var i = 0; i < event; i++) {
                _this.calendarOptions['events'].push(_this.events[i]);
            }
        }, function (err) { return _this.catchError(err); });
    };
    Dashboard1Component.prototype.getMemo = function () {
        var _this = this;
        this._memo_service.getMemoList().subscribe(function (data) {
            _this.memo = data;
        }, function (err) { });
    };
    Dashboard1Component.prototype.getEvents = function () {
        var _this = this;
        this._event_service.getEvents().subscribe(function (data) {
            _this.eventsList = Array.from(data);
        }, function (err) { });
    };
    Dashboard1Component.prototype.addModal = function () {
        var disposable = this.modalService.addDialog(EventComponent, {
            title: 'Add Event',
            button: 'Add',
            create: true,
            employee_id: this.employee_id
        }).subscribe(function (isConfirmed) {
        });
    };
    Dashboard1Component.prototype.editModal = function (id) {
        var disposable = this.modalService.addDialog(EventComponent, {
            title: 'Edit Event',
            button: 'Update',
            edit: true,
            event_id: id,
            employee_id: this.employee_id
        }).subscribe(function (isConfirmed) {
        });
    };
    Dashboard1Component.prototype.getTotalAttendance = function () {
        var _this = this;
        var pre;
        this._attend_service.getTotalPresent().subscribe(function (data) {
            pre = Array.from(data);
            _this.present = pre[0].present;
        }, function (err) { });
    };
    Dashboard1Component.prototype.getEmpOnLeave = function () {
        var _this = this;
        this.emp_leave = 0;
        this._dash_service.getOnLeave().subscribe(function (data) {
            _this.onLeave = data;
            _this.emp_leave = _this.onLeave[0].emp_on_leave;
        }, function (err) { });
    };
    Dashboard1Component.prototype.getEmpLate = function () {
        var _this = this;
        var late;
        this._dash_service.getLateEmp().subscribe(function (data) {
            late = data;
            _this.emp_late = late[0].emp_id;
        }, function (err) { });
    };
    Dashboard1Component.prototype.getEmpRequest = function () {
        var _this = this;
        this._dash_service.getEmpRequest().subscribe(function (data) {
            _this.emp_request = data;
        }, function (err) { });
    };
    Dashboard1Component.prototype.getPendingRequest = function () {
        var _this = this;
        this._dash_service.getPendingRequest().subscribe(function (data) {
            _this.pending_request = data;
        }, function (err) {
        });
    };
    Dashboard1Component.prototype.getTotalEmployee = function () {
        var _this = this;
        var totalEmp;
        var filter = "Active";
        this._attend_service.getTotalEmp(filter).subscribe(function (data) {
            totalEmp = data; // fetced record
            _this.total_emp = totalEmp[0].total_employee;
        }, function (err) { });
    };
    Dashboard1Component.prototype.getComputeAbsent = function () {
        var employee = parseInt(this.employee);
        var attendance = parseInt(this.attendance);
        this.totalAbsent = employee - attendance;
    };
    Dashboard1Component.prototype.showLeavePending = function () {
        var _this = this;
        var leave;
        this._leave_service.getLeavePending()
            .subscribe(function (data) {
            leave = Array.from(data);
            _this.ttlLeave = leave[0].pending_leave;
        }, function (err) { return _this.catchError(err); });
    };
    Dashboard1Component.prototype.eventDropped = function (id) {
        var _this = this;
        var evntId = this.event_id;
        this._event_service.updateEvent(evntId, event)
            .subscribe(function (data) {
            _this.poststore = Array.from(data);
        }, function (err) { return _this.catchError(err); });
    };
    Dashboard1Component.prototype.approveRequest = function (id, url, type) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Approved ' + type,
            message: 'Are you sure you want to Approved this Undertime?',
            action: 'Approved',
            id: id,
            url: url,
            request: true
        }).subscribe(function (isConfirmed) {
            _this.getEmpRequest();
        });
    };
    Dashboard1Component.prototype.rejectRequest = function (id, url, type) {
        var _this = this;
        var disposable = this.modalService.addDialog(RemarksModalComponent, {
            title: 'Reject ' + type,
            id: id,
            url: url,
            button: 'Reject'
        }).subscribe(function (isConfirmed) {
            _this.getEmpRequest();
        });
    };
    Dashboard1Component.prototype.addMemo = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(MemoModalComponent, {
            title: 'Add Memo',
            button: 'Add',
            create: true,
            employee_id: this.employee_id
        }).subscribe(function (isConfirmed) {
            _this.getMemo();
        });
    };
    Dashboard1Component.prototype.getEmpCount = function () {
        var _this = this;
        this.object = 0;
        this._dash_service.getEmpCount().subscribe(function (data) {
            _this.emp_count = data;
            _this.object = _this.emp_count[0].late_count;
        }, function (err) { });
    };
    Dashboard1Component.prototype.archiveEvent = function (id) {
        var _this = this;
        var disposable = this.modalService.addDialog(ConfirmModalComponent, {
            title: 'Archive Event',
            message: 'Are you sure you want to archive this Event?',
            action: 'Delete',
            id: id,
            url: 'event'
        }, { backdropColor: 'rgba(238,238,238)' }).subscribe(function (isConfirmed) {
            _this.ngOnInit();
        });
    };
    Dashboard1Component.prototype.viewDetails = function (id, url, type, status) {
        var _this = this;
        var type_and_time = false;
        var date_range = false;
        var leave = false;
        var sa = false;
        var ob = false;
        var start_end_time = false;
        var types = false;
        if (type == 'COA') {
            type = 'Certificate of Attendance';
            type_and_time = true;
        }
        else if (type == 'Undertime' || type == 'Overtime') {
            start_end_time = true;
        }
        else if (type == 'Official Business') {
            date_range = true;
            ob = true;
        }
        else if (type == 'Leave') {
            date_range = true;
            leave = true;
            types = true;
        }
        else if (type == 'SA') {
            type = 'Schedule Adjustment';
            date_range = true;
            sa = true;
        }
        var buttons = true;
        if (status == 'REJECTED' || status == 'APPROVED' || status == 'CANCELLED') {
            buttons = false;
        }
        var disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title: type,
            id: id,
            url: url,
            type_and_time: type_and_time,
            start_end_time: start_end_time,
            date_range: date_range,
            leave: leave,
            sa: sa,
            ob: ob,
            types: types,
            buttons: buttons
        }).subscribe(function (isConfirmed) {
            _this.getEmpRequest();
        });
    };
    Dashboard1Component.prototype.filterTotalEmployee = function () {
        var _this = this;
        this.filter_total_emp = 0;
        this._dash_service.filterTotalEmployee().subscribe(function (data) {
            _this.filter_total_employee = data;
            _this.filter_total_emp = _this.filter_total_employee[0].total_employee;
        }, function (err) { });
    };
    Dashboard1Component.prototype.getEmployeeActive = function () {
        var _this = this;
        this._dash_service.getEmployeeActive().subscribe(function (data) {
            _this.lineChartData = Array.from(data);
            if (_this.lineChartData.length == 0) {
                for (var i = 0; i < _this.lineChartLabels.length; i++) {
                    _this.lineChartData = [i];
                }
            }
        }, function (err) { });
    };
    Dashboard1Component.prototype.getCompanyActive = function () {
        var _this = this;
        this._dash_service.getCompanyActive().subscribe(function (data) {
            _this.lineChartLabels = Array.from(data);
        }, function (err) { });
    };
    Dashboard1Component = __decorate([
        Component({
            selector: 'admin-dashboard1',
            templateUrl: './dashboard1.component.html',
            styleUrls: ['./dashboard1.component.css']
        }),
        __metadata("design:paramtypes", [Http,
            DialogService,
            NgZone, DialogService,
            EventService,
            AttendanceService,
            LeaveService,
            Dashboard1Service,
            MemoService,
            AuthUserService,
            Configuration])
    ], Dashboard1Component);
    return Dashboard1Component;
}());
export { Dashboard1Component };
//# sourceMappingURL=dashboard1.component.js.map