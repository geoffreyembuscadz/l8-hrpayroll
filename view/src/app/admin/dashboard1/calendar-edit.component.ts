import 'rxjs/add/operator/catch'
import { Component, OnInit,  NgZone, Inject } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Response, Headers } from '@angular/http';
import { Configuration } from '../../app.config';

@Component({
  selector: 'app-calendar-edit-component',
  templateUrl: './calendar-edit.component.html',
  styleUrls: ['./calendar-edit.component.css']
})
export class CalendarEditComponent extends DialogComponent<null, boolean> {
	perm_rec: any;
	private headers: Headers;

 constructor( dialogService: DialogService){ 
 	super(dialogService);

 	this.headers = new Headers();
    let authToken = localStorage.getItem('id_token');
    this.headers.append('Authorization', `Bearer ${authToken}`);
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');

 }

      
      
    

}
