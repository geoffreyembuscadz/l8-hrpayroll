import { Component, NgZone, OnInit, Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { Configuration } from '../../app.config';
import { CalendarEditComponent } from './calendar-edit.component';
import { PermissionService } from '../../services/permission.service';
import 'rxjs/add/operator/map';
import { EventComponent } from '../event/event.component';
import { EventService } from '../../services/event.service';
import { MemoService } from '../../services/memo.service';
import { AttendanceService } from '../../services/attendance.service';
import * as $ from 'jquery';
import { Options } from "fullcalendar";
import { LeaveService } from '../../services/leave.service';
import { Dashboard1Service } from '../../services/dashboard1.service';
import { Event } from '../../model/event';
import * as moment from 'moment';
import { AuthUserService } from '../../services/auth-user.service';
import { ResolutionModalComponent } from '../resolution-modal/resolution-modal.component';
import { Observable } from 'rxjs/Observable';
import { MemoModalComponent } from '../memo/memo-modal/memo-modal.component';
import { ConfirmModalComponent } from '../confirm-modal/confirm-modal.component';
import { RemarksModalComponent } from '../remarks-modal/remarks-modal.component';
import { ActionCenterModalComponent } from '../action-center/action-center-modal/action-center-modal.component';

@Component({
  selector: 'admin-dashboard1',
  templateUrl: './dashboard1.component.html',
  styleUrls: ['./dashboard1.component.css']
})


export class Dashboard1Component implements OnInit {
  public ttlLeave: number;
  public totalAbsent: any;
  public present: number;
  public total_emp: number;
  public error_title: string;
  public error_message: string
  private headers: Headers;
  private eventsList: any;
  public poststore: any;
  events: any[];
  employee: any;
  attendance: any;
  bodyClasses:string = "skin-blue sidebar-mini";
  body = document.getElementsByTagName('body')[0];
  calendarOptions: any = {};
  user_id:any;
  employee_id:any;
  event_id:any;
  onLeave: any;
  emp_leave:any;
  emp_late:any;
  emp_request=[];
  memo: any;
  display_memo: any;
  emp_count: any;
  object: any;
  today=moment();
  filter_total_emp: any;
  filter_total_employee: any;
  pending_request: any;

  public lineChartData:Array<any> = [];

  public lineChartLabels:Array<any> = [];
  public lineChartType:string = 'line';
  public pieChartType:string = 'pie';

  public lineChartOptions: any = {
    responsive: true,
    maintainAspectRatio: false
  };

   public chartColors: Array<any> = [
    { // first color
      backgroundColor: 'rgba(225,10,24,0.2)',
      borderColor: 'rgba(225,10,24,0.2)',
      pointBackgroundColor: 'rgba(225,10,24,0.2)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(225,10,24,0.2)'
    },
    { // second color
      backgroundColor: 'rgba(225,10,24,0.2)',
      borderColor: 'rgba(225,10,24,0.2)',
      pointBackgroundColor: 'rgba(225,10,24,0.2)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(225,10,24,0.2)'
    }];


  public chartHovered(e:any):void {
  }



  public daterange: any = {};
  public option: any = {
    locale: { format: 'YYYY-MM-DD' },
    alwaysShowCalendars: false,
  };

  public selectedDate(value: any) {
    this.daterange.start = value.start;
    this.daterange.end = value.end;
  }
  public chartClicked(e:any):void {
  }


  constructor(
    private _http: Http,  
    private modalService: DialogService, 
    _ngZone: NgZone, dialogService: DialogService, 
    private _event_service: EventService,
    private _attend_service: AttendanceService,
    private _leave_service: LeaveService,
    private _dash_service: Dashboard1Service,
    private _memo_service: MemoService,
    private _auth_service: AuthUserService,
    _conf: Configuration
    ) {
    let app_config = _conf;
    let authToken = localStorage.getItem('id_token');
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Authorization', `Bearer ${authToken}`);

    this.body.classList.add("skin-blue");
    this.body.classList.add("sidebar-mini");

    var self = this; 
    this.calendarOptions = {
      height: 'parent',
      fixedWeekCount : false,
      defaultDate: this.today,
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      background: '#007c08',
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay',
        height: 650
      },
      events : function(start, end, timezone, callback) {
        $.ajax({
          url: app_config.ServerWithApiUrl + 'event/show',/*'http://localhost:8000/api/event/show', */
          dataType: 'json',
          data: {
              // our hypothetical feed requires UNIX timestamps
              start: start.unix(),
              end: end.unix()
            },
            success: function(data) {
              var events = data;
              callback(events);
            }
          });
      },
      eventClick: function(event, jsEvent, view, date) {
        this.event_id = event.id;
        self.editModal(this.event_id);
      },

      dayClick: function(date, jsEvent, view) {
        self.addModal();
      },
    }

  }  
  ngOnInit() {
    // this.getEmpRequest();
    this.get_UserId();
    // this.getTotalEmployee();
    // this.getComputeAbsent();
    // this.showEvent();
    // this.showLeavePending();
    // this.getEmpLate();
    // this.getMemo();
    // this.getCompanyActive();
    // this.getEmployeeActive();
    
    this.getTotalAttendance();
    this.getPendingRequest();
    this.getEmpOnLeave();
    this.getEmpCount();
    this.filterTotalEmployee();


    }

    get_UserId(){
      this._auth_service.getUser().
      subscribe(
        data => {
          let user = data; // fetched 
          this.user_id = user.id;
          this.getEmployeeId();
        },
        err => console.error(err)
        );
    }

    getEmployeeId(){
      this._auth_service.getUserById(this.user_id).
      subscribe(
        data => {
          let user = data; // fetched 
          this.employee_id = user.employee_id;
        },
        err => console.error(err)
        );
    }

    public catchError(error: any) {
      let response_body = error._body;
      let response_status = error.status;

      if( response_status == 500 ){
        this.error_title = 'Error 500';
        this.error_message = 'The given data failed to pass validation.';
      } else if( response_status == 200 ) {
        this.error_title = '';
        this.error_message = '';
      }
    }

    showEvent(){
      this._event_service.getEventList()
      .subscribe(
        data => {
          this.events = data;
          let event = this.events.length;
          for (var i = 0; i < event; i++) {
            this.calendarOptions['events'].push(this.events[i]);
          }           
        },
        err => this.catchError(err)
        );
    }

    getMemo(){
      this._memo_service.getMemoList().subscribe(
        data => {
          this.memo = data;        
        },
        err =>{}
        );
    }

    getEvents() {
      this._event_service.getEvents().subscribe(
        data => {
          this.eventsList = Array.from(data); 

        },
        err =>{}
        );
    }

    addModal() {
      let disposable = this.modalService.addDialog(EventComponent, {
        title:'Add Event',
        button:'Add',
        create:true,
        employee_id:this.employee_id
      }).subscribe((isConfirmed)=>{

      });
    }

    editModal(id: any) {
      let disposable = this.modalService.addDialog(EventComponent, {
        title:'Edit Event',
        button:'Update',
        edit:true,
        event_id:id,
        employee_id:this.employee_id
      }).subscribe((isConfirmed)=>{
      });
    }

    getTotalAttendance() {
      let pre;
      this._attend_service.getTotalPresent().subscribe(
        data => {
          pre = Array.from(data);
          this.present = pre[0].present;
        },
        err =>{}
        );
    }
    getEmpOnLeave() {
      this.emp_leave = 0;
      this._dash_service.getOnLeave().subscribe(
        data => {
          this.onLeave = data;
          this.emp_leave = this.onLeave[0].emp_on_leave;
        },
        err =>{}
        );
    }

    getEmpLate() {
      let late: any;
      this._dash_service.getLateEmp().subscribe(
        data => {
          late = data;
          this.emp_late = late[0].emp_id;
        },
        err =>{}
        );
    }

    getEmpRequest() {
      this._dash_service.getEmpRequest().subscribe(
        data => {
          this.emp_request = data;
        },
        err =>{}
        );
    }

    getPendingRequest() {
      this._dash_service.getPendingRequest().subscribe(
          data => {
            this.pending_request = data
          },
          err => {

          }
        );
    }

    getTotalEmployee() {
      let totalEmp;
      let filter = "Active";
      this._attend_service.getTotalEmp(filter).subscribe(
        data => {
          totalEmp = data; // fetced record
          this.total_emp = totalEmp[0].total_employee;
        },
        err =>{}
        );
    }

    getComputeAbsent(){
      let employee = parseInt(this.employee);
      let attendance = parseInt(this.attendance);
      this.totalAbsent = employee - attendance;
    }

    showLeavePending(){
      let leave;
      this._leave_service.getLeavePending()
      .subscribe(
        data => {
          leave = Array.from(data);
          this.ttlLeave = leave[0].pending_leave;
        },
        err => this.catchError(err)
        );
    }

    eventDropped(id: any){
      let evntId = this.event_id;
      this._event_service.updateEvent(evntId,event)
      .subscribe(
        data => {
          this.poststore = Array.from(data);
        },
        err => this.catchError(err)
        ); 
    }

    approveRequest(id:any,url:any,type:any) {

      let disposable = this.modalService.addDialog(ConfirmModalComponent, {
        title:'Approved ' + type,
        message:'Are you sure you want to Approved this Undertime?',
        action:'Approved',
        id:id,
        url:url,
        request:true
      }).subscribe((isConfirmed)=>{
        this.getEmpRequest();

      });
    }

    rejectRequest(id:any,url:any,type:any) {

      let disposable = this.modalService.addDialog(RemarksModalComponent, {
        title:'Reject ' + type ,
        id:id,
        url:url,
        button:'Reject'
      }).subscribe((isConfirmed)=>{
        this.getEmpRequest();

      });
    }

    addMemo(id: any) {
      let disposable = this.modalService.addDialog(MemoModalComponent, {
        title:'Add Memo',
        button:'Add',
        create:true,
        employee_id:this.employee_id
      }).subscribe((isConfirmed)=>{
        this.getMemo();
      });
    }

    getEmpCount() {
      this.object = 0;
      this._dash_service.getEmpCount().subscribe(
        data => {
          this.emp_count = data;
          this.object = this.emp_count[0].late_count;

        },
        err =>{}
        );
    }

    archiveEvent(id:any){

      let disposable = this.modalService.addDialog(ConfirmModalComponent, {
        title:'Archive Event',
        message:'Are you sure you want to archive this Event?',
        action:'Delete',
        id:id,
        url:'event'
      }, { backdropColor: 'rgba(238,238,238)' }).subscribe((isConfirmed)=>{
        this.ngOnInit();
      });
    }
    
    viewDetails(id:any,url:any,type:any,status:any) {
      let type_and_time = false;
      let date_range = false;
      let leave = false;
      let sa = false;
      let ob = false;
      let start_end_time = false;
      let types = false;

      if(type=='COA'){
        type = 'Certificate of Attendance';
        type_and_time = true;
      }
      else if(type=='Undertime' || type=='Overtime'){
        start_end_time = true;
      }
      else if (type=='Official Business'){
        date_range = true;
        ob = true;
      }
      else if (type=='Leave'){
        date_range = true;
        leave = true;
        types = true;
      }
      else if(type=='SA'){
        type='Schedule Adjustment';
        date_range = true;
        sa = true;
      }

      let buttons = true;
      if (status == 'REJECTED' || status == 'APPROVED' || status == 'CANCELLED') {
        buttons = false;
      }

      let disposable = this.modalService.addDialog(ActionCenterModalComponent, {
            title:type,
            id:id,
            url:url,
            type_and_time:type_and_time,
            start_end_time:start_end_time,
            date_range:date_range,
            leave:leave,
            sa:sa,
            ob:ob,
            types:types,
            buttons:buttons
          }).subscribe((isConfirmed)=>{
              this.getEmpRequest();
            });
    }


    filterTotalEmployee() {
      this.filter_total_emp = 0;
      this._dash_service.filterTotalEmployee().subscribe(
        data => {
          this.filter_total_employee = data;
          this.filter_total_emp = this.filter_total_employee[0].total_employee;

        },
        err =>{}
        );
    }

    getEmployeeActive() {
      this._dash_service.getEmployeeActive().subscribe(
        data => {
          this.lineChartData = Array.from(data);

          if (this.lineChartData.length == 0) {
            for (var i = 0; i < this.lineChartLabels.length; i++) {
              this.lineChartData = [i];
            }
          }

        },
        err =>{}
        );
    }

    getCompanyActive() {
      this._dash_service.getCompanyActive().subscribe(
        data => {
          this.lineChartLabels = Array.from(data);
        },
        err =>{}
        );
    }

  }
