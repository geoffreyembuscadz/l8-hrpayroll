var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
var Configuration = /** @class */ (function () {
    function Configuration() {
        this.Server = "http://localhost:8000/";
        this.ApiUrl = "api/";
        this.ServerWithApiUrl = this.Server + this.ApiUrl;
        this.ServerWithPublicDownloadAttendanceForm = this.Server + 'files/download/attendance_form.csv';
        this.ServerWithPublicDownloadEmployeeUploadForm = this.Server + 'api/employee_download_upload_form';
        this.frontEndUrl = 'http://localhost:4200/';
        this.socketUrl = 'http://localhost:3000';
    }
    Configuration = __decorate([
        Injectable()
    ], Configuration);
    return Configuration;
}());
export { Configuration };
//# sourceMappingURL=app.config.js.map