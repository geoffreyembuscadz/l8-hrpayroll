var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { _404Component } from './404/404.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './services/auth-guard.service';
import { PasswordResetComponent } from './login/password-reset.component';
import { Dashboard1Component } from './admin/dashboard1/dashboard1.component';
var APP_ROUTES = [
    { path: '', redirectTo: '/admin', pathMatch: 'full' },
    { path: 'admin', component: Dashboard1Component, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'password/reset/:token', component: PasswordResetComponent },
    { path: '404', component: _404Component },
    { path: 'auth', redirectTo: '/login' },
    { path: '**', redirectTo: '/404' },
    { path: '#', redirectTo: '/admin' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        NgModule({
            imports: [
                RouterModule.forRoot(APP_ROUTES)
            ],
            exports: [
                RouterModule
            ]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map