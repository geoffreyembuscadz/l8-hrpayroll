var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
var _404Component = /** @class */ (function () {
    function _404Component(_rt) {
        this._rt = _rt;
    }
    _404Component.prototype.ngOnInit = function () {
    };
    _404Component.prototype.logout = function () {
        localStorage.removeItem('id_token');
        localStorage.removeItem('role');
        this._rt.navigate(['auth']);
    };
    _404Component = __decorate([
        Component({
            selector: 'app-404',
            templateUrl: './404.component.html',
            styleUrls: ['./404.component.css']
        }),
        __metadata("design:paramtypes", [Router])
    ], _404Component);
    return _404Component;
}());
export { _404Component };
//# sourceMappingURL=404.component.js.map