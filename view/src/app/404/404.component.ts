import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-404',
  templateUrl: './404.component.html',
  styleUrls: ['./404.component.css']
})
export class _404Component implements OnInit {
  _rt: any;
  
    
  constructor(_rt: Router,) { 
    this._rt = _rt;

  }

  ngOnInit() {
  }

  logout() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('role');
    this._rt.navigate([ 'auth' ]); 
  }  


}
