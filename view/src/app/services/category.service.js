var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var CategoryService = /** @class */ (function () {
    function CategoryService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'category';
    }
    CategoryService.prototype.getCategorys = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    CategoryService.prototype.setId = function (category_id) {
        this.id = category_id;
        console.log('setId: ', category_id);
    };
    CategoryService.prototype.getId = function () {
        return this.id;
    };
    CategoryService.prototype.getCategory = function (categorys_id) {
        var get_url = this.action_url + this.route_link + '/' + categorys_id;
        return this._http.get(get_url).map(this.extractData);
    };
    CategoryService.prototype.storeCategory = function (category_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(category_model)).map(this.extractData);
    };
    CategoryService.prototype.updateCategory = function (id, category_model) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, category_model).map(this.extractData);
    };
    CategoryService.prototype.archiveCategory = function (category_id) {
        console.log('postarchive', category_id);
        var delete_url = this.action_url + this.route_link + '/' + category_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    CategoryService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    CategoryService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    };
    CategoryService.prototype.category_name = function () {
        var action_url = this.action_url + 'category/name';
        return this._http.get(action_url).map(this.extractData);
    };
    CategoryService.prototype.questionList = function () {
        var action_url = this.action_url + 'question/list';
        return this._http.get(action_url).map(this.extractData);
    };
    CategoryService.prototype.storeCategoryDetails = function (category_model) {
        var post_url = this.action_url + 'post/category';
        return this._http.post(post_url, JSON.stringify(category_model)).map(this.extractData);
    };
    CategoryService.prototype.getSubCat = function () {
        var action_url = this.action_url + 'get/category/list';
        return this._http.get(action_url).map(this.extractData);
    };
    CategoryService.prototype.storeSubCategory = function (category_model) {
        var post_url = this.action_url + 'sub/category';
        return this._http.post(post_url, JSON.stringify(category_model)).map(this.extractData);
    };
    CategoryService.prototype.updateSubCategory = function (id, category_model) {
        var put_url = this.action_url + 'subcategory' + '/' + id;
        return this._http.put(put_url, category_model).map(this.extractData);
    };
    CategoryService.prototype.getSubcategorys = function () {
        var get_url = this.action_url + 'subcategory';
        return this._http.get(get_url).map(this.extractData);
    };
    CategoryService.prototype.getSubcategory = function (subcategorys_id) {
        var get_url = this.action_url + 'subcategory' + '/' + subcategorys_id;
        return this._http.get(get_url).map(this.extractData);
    };
    CategoryService.prototype.getSegment = function () {
        var get_url = this.action_url + 'get/segments';
        return this._http.get(get_url).map(this.extractData);
    };
    CategoryService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient,
            Configuration])
    ], CategoryService);
    return CategoryService;
}());
export { CategoryService };
//# sourceMappingURL=category.service.js.map