var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Configuration } from '../app.config';
//import { Configuration } from '../app.config';
var DynastyLoanService = /** @class */ (function () {
    function DynastyLoanService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.route_employee = 'employee';
        this.route_dynasty_loan = 'dynasty_loan';
        this.route_admin_dynasty_loan = 'admin_dynasty_loan';
        this.payback_modes = [
            { id: 1, name: 'Weekly' },
            { id: 2, name: '5th & 20th' },
            { id: 3, name: '10th & 20th' },
            { id: 4, name: '15th & 30th' }
        ];
        this.types_of_loan = [
            { id: 1, name: 'emergency' },
            { id: 2, name: 'multi-purpose' },
            { id: 3, name: 'appliance / cellphone' },
            { id: 4, name: 'automotive vehicle' },
            { id: 5, name: 'livelihood' },
            { id: 6, name: 'motorcycle' },
            { id: 7, name: 'others' },
        ];
        // Static Selection Options
        this.name_suffixes = [
            { name: '-none-' },
            { name: 'Jr.' },
            { name: 'Sr.' },
            { name: 'III' },
            { name: 'IV' }
        ];
        this.action_url = this._conf.ServerWithApiUrl;
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        //console.log(authToken);
    }
    DynastyLoanService.prototype.getDynastyLoanRecord = function (id) {
        var get_url = this.action_url + this.route_admin_dynasty_loan + '/' + id;
        return this._http.get(get_url, { headers: this.headers }).map(this.extractData);
    };
    DynastyLoanService.prototype.getEmployee = function (employee_id) {
        var get_url = this.action_url + this.route_employee + '/' + employee_id;
        return this._http.get(get_url, { headers: this.headers }).map(this.extractData);
    };
    DynastyLoanService.prototype.getPaybackModes = function () {
        return this.payback_modes;
    };
    DynastyLoanService.prototype.getTypesOfLoan = function () {
        return this.types_of_loan;
    };
    DynastyLoanService.prototype.updateDynastyLoan = function (id, employee_dynasty_loan_model) {
        var put_url = this.action_url + this.route_admin_dynasty_loan + '/' + id;
        return this._http.put(put_url, employee_dynasty_loan_model, { headers: this.headers }).map(this.extractData);
    };
    DynastyLoanService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    DynastyLoanService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http, Configuration])
    ], DynastyLoanService);
    return DynastyLoanService;
}());
export { DynastyLoanService };
//# sourceMappingURL=dynastyloan.service.js.map