import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class AttendanceReportService {

	private action_url: string;
    private route_link: string;
    private actionUrl: string;

    constructor(private _http: HttpClient,
                private _conf: Configuration){

                    this.action_url = this._conf.ServerWithApiUrl;
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    public getAttendanceReport(){
      
        let action_url = this.action_url + 'report';
        return this._http.get(action_url).map(this.extractData);       
    }

    public getLateEarlyReport(){
      
        let action_url = this.action_url + 'late/early/report';
        return this._http.get(action_url).map(this.extractData);       
    }

    public statusReport(model: any){
        let start = model.start_date;
        let end = model.end_date;
        let emp = model.emp_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end + '&emp_id=' + emp;
        let action_url = this.action_url + 'status/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

    public hoursReport(model: any){        
        let start = model.start_date;
        let end = model.end_date;
        let emp = model.emp_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end + '&emp_id=' + emp;
        let action_url = this.action_url + 'hours/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

    public getDaySummaryReport(model: any){
        let start = model.start_date;
        let end = model.end_date;
        let emp = model.emp_id;
      
        let uri_data = 'start_date=' + start + '&end_date=' + end;
        let action_url = this.action_url + 'days/summary/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
        
    }

     public getHoursSummaryReport(model: any){
        let start = model.start_date;
        let end = model.end_date;

        let uri_data = 'start_date=' + start + '&end_date=' + end;

        let action_url = this.action_url + 'hours/summary/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
        
    }
     public lateErlyFltr(model: any){
        let start = model.start_date;
        let end = model.end_date;
        let emp = model.emp_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end + '&employee_id=' + emp;
        let action_url = this.action_url + 'late/erly/rprt/fltr?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

    public userReport(model: any){
        let emp = model.emp_id;
        let start = model.start_date;
        let end = model.end_date;
        let comp_id = model.comp_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end + '&company_id=' + comp_id + '&emp_id=' + emp;   
        let action_url = this.action_url + 'user/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

    public getEmpByComp(company_id){
        let comp = company_id;

        let uri_data = 'company_id=' + comp;   
        let action_url = this.action_url + 'getcomp/byemp?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

     public perEmpLateEarly(model: any){
        let emp = model.emp_id;
        let start = model.start_date;
        let end = model.end_date;

        let uri_data = 'start_date=' + start + '&end_date=' + end + '&emp_id=' + emp;   
        let action_url = this.action_url + 'perEmp/attendance/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

    public perEmpDaysSummary(model){
        let emp = model.emp_id;
        let start = model.start_date;
        let end = model.end_date;

        let uri_data = 'emp_id=' + emp + '&end_date=' + end + '&start_date=' + start;
        let action_url = this.action_url + 'per/emp/days/summary/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

    public perEmpHoursSummary(model){
        let emp = model.emp_id;
        let start = model.start_date;
        let end = model.end_date;

        let uri_data = 'emp_id=' + emp + '&end_date=' + end + '&start_date=' + start;
        let action_url = this.action_url + 'per/emp/hours/summary/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

    public workingSched(model){
    let emp = model.emp_id;

    let uri_data = 'emp_id=' + emp;
    let action_url = this.action_url + 'working/schedule?' + uri_data;
    return this._http.get(action_url).map(this.extractData);          
    }

}

