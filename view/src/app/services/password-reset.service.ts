import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Role } from '../model/role';
import { Configuration } from '../app.config';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Injectable()
export class PasswordResetService {
    private action_url: string;
    private route_link: string;
    private headers: Headers;
    public tokenParams: string;
    public error_message: Boolean;
    public reset_success: Boolean;

    constructor(private _http: Http, private _conf: Configuration, private route: ActivatedRoute){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'password';
        

        this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', `Bearer ${authToken}`);
    }

    public reset(reset_model: any){
        let reset_url = this.action_url + this.route_link + '/' + 'email';

        return this._http.post(reset_url, JSON.stringify(reset_model), { headers:this.headers }).map(this.extractData);
    }

    public resetPassword(reset_password_model: any, token: any) {
        let reset_url = this.action_url + this.route_link + '/reset/' + token;
        return this._http.post(reset_url, JSON.stringify(reset_password_model), { headers:this.headers }).map(this.extractData);

    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }
}