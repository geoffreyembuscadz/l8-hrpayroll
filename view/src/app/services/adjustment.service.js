var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from './http-extended.service';
import { Configuration } from '../app.config';
var AdjustmentService = /** @class */ (function () {
    function AdjustmentService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'adjustment';
    }
    AdjustmentService.prototype.getAdjustments = function (id, start, end, type, no_of_days) {
        var get_url = this.action_url + this.route_link + '/?emp_id=' + id + '&start=' + start + '&end=' + end + '&type=' + type + '&no_of_days=' + no_of_days;
        return this._http.get(get_url).map(this.extractData);
    };
    AdjustmentService.prototype.getAdjustment = function (adjustment_id) {
        var get_url = this.action_url + this.route_link + '/' + adjustment_id;
        return this._http.get(get_url).map(this.extractData);
    };
    AdjustmentService.prototype.setId = function (adjustment_id) {
        this.id = adjustment_id;
    };
    AdjustmentService.prototype.getId = function () {
        return this.id;
    };
    AdjustmentService.prototype.updateAdjustment = function (id, adjustment_id) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, adjustment_id).map(this.extractData);
    };
    AdjustmentService.prototype.archiveAdjustment = function (adjustment_id) {
        var delete_url = this.action_url + this.route_link + '/' + adjustment_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    AdjustmentService.prototype.storeAdjustment = function (adjustment_id) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(adjustment_id)).map(this.extractData);
    };
    AdjustmentService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    AdjustmentService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    AdjustmentService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], AdjustmentService);
    return AdjustmentService;
}());
export { AdjustmentService };
//# sourceMappingURL=adjustment.service.js.map