var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from './http-extended.service';
import { Configuration } from '../app.config';
var PermissionService = /** @class */ (function () {
    function PermissionService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'permission';
    }
    PermissionService.prototype.getPermissions = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    PermissionService.prototype.getPermission = function (permission_id) {
        var get_url = this.action_url + this.route_link + '/' + permission_id;
        return this._http.get(get_url).map(this.extractData);
    };
    PermissionService.prototype.setId = function (permission_id) {
        this.id = permission_id;
    };
    PermissionService.prototype.getId = function () {
        return this.id;
    };
    PermissionService.prototype.updatePermission = function (id, perms_model) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, perms_model).map(this.extractData);
    };
    PermissionService.prototype.archivePermission = function (role_id) {
        console.log('postarchive', role_id);
        var delete_url = this.action_url + this.route_link + '/' + role_id;
        console.log(delete_url);
        console.log(role_id);
        return this._http.delete(delete_url).map(this.extractData);
    };
    PermissionService.prototype.storePermission = function (perms_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(perms_model)).map(this.extractData);
    };
    PermissionService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    PermissionService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    PermissionService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], PermissionService);
    return PermissionService;
}());
export { PermissionService };
//# sourceMappingURL=permission.service.js.map