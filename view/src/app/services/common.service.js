var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var CommonService = /** @class */ (function () {
    function CommonService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'common';
        this.option_time_hr_24 = Array(25).fill(1).map(function (x, i) { return i; });
        this.option_time_mins = Array(60).fill(1).map(function (x, i) { return i; });
        this.option_time_hr_24[0] = 12; // Setting the zero index to 12thHR
    }
    CommonService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    CommonService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    CommonService.prototype.getEmployees = function (supervisor_id, company_id) {
        if (supervisor_id === void 0) { supervisor_id = ''; }
        if (company_id === void 0) { company_id = ''; }
        var uri_data = 'supervisor_id=' + supervisor_id + '&company_id=' + company_id;
        var get_url = this.action_url + this.route_link + '/' + 'employees?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getEmployee = function () {
        var get_url = this.action_url + this.route_link + '/' + 'employee';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getLeaveType = function () {
        var get_url = this.action_url + this.route_link + '/' + 'leave_type';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getOBType = function () {
        var get_url = this.action_url + this.route_link + '/' + 'ob_type';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getStatusType = function () {
        var action_url = this.action_url + this.route_link + '/' + 'status_type';
        return this._http.get(action_url).map(this.extractData);
    };
    CommonService.prototype.getCOAType = function () {
        var get_url = this.action_url + this.route_link + '/' + 'coa_type';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getDepartment = function () {
        var get_url = this.action_url + this.route_link + '/' + 'department';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getCompany = function () {
        var get_url = this.action_url + this.route_link + '/' + 'company';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getAttendanceType = function () {
        var get_url = this.action_url + this.route_link + '/' + 'attendance_type';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.updateStatus = function (id_status, status, url) {
        var put_url = this.action_url + url + '/' + id_status;
        return this._http.put(put_url, status).map(this.extractData);
    };
    CommonService.prototype.updateRequest = function (model) {
        var post_url = this.action_url + this.route_link + '/update/request';
        return this._http.post(post_url, JSON.stringify(model)).map(this.extractData);
    };
    CommonService.prototype.archiveData = function (url, id) {
        var urls = this.action_url + url + '/' + id;
        return this._http.delete(urls).map(this.extractData);
    };
    CommonService.prototype.getTypeData = function (id, url) {
        var get_url = this.action_url + url + '/' + id;
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.createTypeData = function (model, url) {
        var post_url = this.action_url + url;
        return this._http.post(post_url, JSON.stringify(model)).map(this.extractData);
    };
    CommonService.prototype.updateTypeData = function (id, model, url) {
        var put_url = this.action_url + url + '/' + id;
        return this._http.put(put_url, model).map(this.extractData);
    };
    CommonService.prototype.getEventType = function () {
        var get_url = this.action_url + this.route_link + '/' + 'event_type';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.remarksModal = function (id, url, model) {
        var urls = this.action_url + url + '/' + id;
        return this._http.put(urls, model).map(this.extractData);
    };
    CommonService.prototype.getHoliType = function () {
        var get_url = this.action_url + this.route_link + '/' + 'holi_type';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.resolutionModal = function (id, url) {
        var get_url = this.action_url + url + '/' + id;
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getAllRequest = function (model) {
        var supervisor_id = model.supervisor_id;
        var start = model.start_date;
        var end = model.end_date;
        var status_id = model.status_id;
        var company_id = model.company_id;
        var employee_id = model.employee_id;
        var type_id = model.type_id;
        var role_id = model.role_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&status_id=' + status_id + '&company_id=' + company_id + '&employee_id=' + employee_id + '&type_id=' + type_id + '&supervisor_id=' + supervisor_id + '&role_id=' + role_id;
        var get_url = this.action_url + this.route_link + '/' + 'all_request?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getRequest = function (id, url) {
        var get_url = this.action_url + url + '/' + id;
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getMemoType = function () {
        var action_url = this.action_url + 'type/memo';
        return this._http.get(action_url).map(this.extractData);
    };
    CommonService.prototype.getMySchedule = function (id) {
        var get_url = this.action_url + 'my_schedule/' + id;
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getFilterEmployee = function (company_id) {
        var get_url = this.action_url + this.route_link + '/' + 'filter_employee?' + '&company_id=' + company_id;
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getBranch = function () {
        var get_url = this.action_url + this.route_link + '/' + 'branch';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getBranchByCompany = function (id) {
        var get_url = this.action_url + this.route_link + '/' + 'branch_by_company/' + id;
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getBioDevice = function () {
        var get_url = this.action_url + this.route_link + '/' + 'bio_device';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getPosition = function () {
        var get_url = this.action_url + this.route_link + '/' + 'position';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getWorkingSchedule = function (id) {
        var get_url = this.action_url + this.route_link + '/' + 'working_sched/' + id;
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getSuggestedWorkingSchedule = function () {
        var get_url = this.action_url + this.route_link + '/suggested_working_sched';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getWorkingScheduleWithoutBreak = function () {
        var get_url = this.action_url + this.route_link + '/' + 'working_sched_without_break';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getOnboarding = function () {
        var get_url = this.action_url + this.route_link + '/' + 'onboarding';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getClearance = function () {
        var get_url = this.action_url + this.route_link + '/' + 'clearance';
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.checkDateHoliday = function (date) {
        var uri_data = 'date=' + date;
        var get_url = this.action_url + this.route_link + '/' + 'check_holiday?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    CommonService.prototype.getDates = function (startDate, endDate) {
        // Gets dates between from two different dates. Returns Array
        var dates = [], currentDate = startDate, addDays = function (days) {
            var date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        };
        while (currentDate <= endDate) {
            dates.push(currentDate);
            currentDate = addDays.call(currentDate, 1);
        }
        return dates;
    };
    ;
    CommonService.prototype.getDay = function (index_day) {
        if (index_day === void 0) { index_day = 0; }
        return this.days[index_day];
    };
    CommonService.prototype.specialArchiveData = function (url, id) {
        var model = {
            id: id
        };
        var post_url = this.action_url + url + '/archive';
        return this._http.post(post_url, model).map(this.extractData);
    };
    /*
    this.option_time_hr_24 = Array(12).fill(1).map((x,i)=>i);
    this.option_time_mins = Array(59).fill(1).map((x,i)=>i);
    */
    CommonService.prototype.getTimeHours = function () {
        return this.option_time_hr_24;
    };
    CommonService.prototype.getTimeMins = function () {
        return this.option_time_mins;
    };
    CommonService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], CommonService);
    return CommonService;
}());
export { CommonService };
//# sourceMappingURL=common.service.js.map