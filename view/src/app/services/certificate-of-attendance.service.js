var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var CertificateOfAttendanceService = /** @class */ (function () {
    function CertificateOfAttendanceService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'certificate_of_attendance';
    }
    CertificateOfAttendanceService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    CertificateOfAttendanceService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    CertificateOfAttendanceService.prototype.createCOA = function (coa_model) {
        var post_url = this.action_url + this.route_link + '/' + 'create';
        return this._http.post(post_url, JSON.stringify(coa_model)).map(this.extractData);
    };
    CertificateOfAttendanceService.prototype.createMyCOA = function (coa_model) {
        var post_url = this.action_url + this.route_link + '/' + 'create/my_coa';
        return this._http.post(post_url, JSON.stringify(coa_model)).map(this.extractData);
    };
    CertificateOfAttendanceService.prototype.getCOA = function (id) {
        var get_url = this.action_url + this.route_link + '/' + id;
        return this._http.get(get_url).map(this.extractData);
    };
    CertificateOfAttendanceService.prototype.updateCOA = function (update_model) {
        var post_url = this.action_url + this.route_link + '/update/coa';
        return this._http.post(post_url, JSON.stringify(update_model)).map(this.extractData);
    };
    CertificateOfAttendanceService.prototype.updateMyCOA = function (update_model) {
        var post_url = this.action_url + this.route_link + '/update/my_coa';
        return this._http.post(post_url, JSON.stringify(update_model)).map(this.extractData);
    };
    CertificateOfAttendanceService.prototype.getMyCOA = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var status_id = model.status_id;
        var type_id = model.type_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&status_id=' + status_id + '&type_id=' + type_id;
        var get_url = this.action_url + this.route_link + '/' + 'my_coa' + '/' + 'data?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    CertificateOfAttendanceService.prototype.getCOAList = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    CertificateOfAttendanceService.prototype.getFilteredCOA = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var status_id = model.status_id;
        var company_id = model.company_id;
        var employee_id = model.employee_id;
        var type_id = model.type_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&status_id=' + status_id + '&company_id=' + company_id + '&employee_id=' + employee_id + '&type_id=' + type_id;
        var get_url = this.action_url + this.route_link + '/' + 'filter' + '/' + 'data?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    CertificateOfAttendanceService.prototype.duplicateEntryValidation = function (coa) {
        var post_url = this.action_url + this.route_link + '/duplicate_entry_validation';
        return this._http.post(post_url, JSON.stringify(coa)).map(this.extractData);
    };
    CertificateOfAttendanceService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], CertificateOfAttendanceService);
    return CertificateOfAttendanceService;
}());
export { CertificateOfAttendanceService };
//# sourceMappingURL=certificate-of-attendance.service.js.map