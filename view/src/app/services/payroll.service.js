var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var PayrollService = /** @class */ (function () {
    function PayrollService(_http, http, _conf) {
        this._http = _http;
        this.http = http;
        this._conf = _conf;
        this.reload = false;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_payroll = 'payroll';
        this.route_payroll_list = 'payroll_list';
        this.route_payslip = 'payslip';
        this.route_payroll_allowance = 'payroll_allowance';
        this.route_payroll_billing = 'payroll_billing';
        this.route_soa = 'statement_of_account';
        this.route_soa_loc = 'statement_of_account_loc';
        this.route_payroll_get_employee = 'payroll_get_employee';
    }
    PayrollService.prototype.getPayrolls = function () {
        var get_url = this.action_url + this.route_payroll;
        return this._http.get(get_url).map(this.extractData);
    };
    PayrollService.prototype.getPayrollList = function (start, end, run_type, company, branch, department, position, cutoff, removed_employee) {
        var get_url = this.action_url + this.route_payroll_list +
            '?start=' + start + '&end=' + end + '&run_type=' + run_type + '&company='
            + company + '&branch=' + branch + '&department=' + department + '&position=' + position + '&cutoff=' + cutoff + '&removed_employee=' + removed_employee;
        return this._http.get(get_url).map(this.extractData);
    };
    PayrollService.prototype.getPayrollEmployee = function (start, end, run_type, company, branch, department, position, cutoff, removed_employee) {
        var get_url = this.action_url + this.route_payroll_get_employee +
            '?start=' + start + '&end=' + end + '&run_type=' + run_type + '&company='
            + company + '&branch=' + branch + '&department=' + department + '&position=' + position + '&cutoff=' + cutoff + '&removed_employee=' + removed_employee;
        return this._http.get(get_url).map(this.extractData);
    };
    PayrollService.prototype.getPayrollBilling = function (start, end, run_type, company, branch, department, position, cutoff, branch_location) {
        var get_url = this.action_url + this.route_payroll_billing +
            '?start=' + start + '&end=' + end + '&run_type=' + run_type + '&company='
            + company + '&branch=' + branch + '&department=' + department +
            '&position=' + position + '&cutoff=' + cutoff + '&branch_location=' + branch_location;
        console.log(get_url);
        return this._http.get(get_url).map(this.extractData);
    };
    PayrollService.prototype.generatePayslip = function (id, company, branch, start, end) {
        var get_url = this.action_url + this.route_payslip +
            '?id=' + id + '&company=' + company + '&branch=' + branch + '&start=' + start + '&end=' + end;
        return this._http.get(get_url);
    };
    PayrollService.prototype.sendPayslip = function (id) {
        var get_url = this.action_url + this.route_payslip + '/send?id=' + id;
        return this._http.get(get_url);
    };
    PayrollService.prototype.generateSOA = function (total_billable, admin_fee, vat, total_mandatory, total_admin, gross_pay, payroll_id) {
        var get_url = this.action_url + this.route_soa +
            '?total_billable=' + total_billable +
            '&admin_fee=' + admin_fee +
            '&vat=' + vat +
            '&total_mandated=' + total_mandatory +
            '&total_admin_fee=' + total_admin +
            '&gross_pay=' + gross_pay +
            '&payroll_id=' + payroll_id;
        return this._http.get(get_url);
    };
    PayrollService.prototype.generateSOAByLocation = function (total_billable, admin_fee, vat, total_mandatory, total_admin, company, start, end, prepared_by) {
        var get_url = this.action_url + this.route_soa_loc +
            '?total_billable=' + total_billable +
            '&admin_fee=' + admin_fee +
            '&vat=' + vat +
            '&total_mandated=' + total_mandatory +
            '&total_admin_fee=' + total_admin +
            '&company=' + company +
            '&start=' + start +
            '&end=' + end +
            '&prepared_by=' + prepared_by;
        return this._http.get(get_url);
    };
    PayrollService.prototype.getPayrollArchieve = function (payroll_id) {
        var get_url = this.action_url + this.route_payroll_list + '/' + payroll_id;
        return this._http.get(get_url).map(this.extractData);
    };
    PayrollService.prototype.updateAllowance = function (id, amount) {
        var put_url = this.action_url + this.route_payroll_allowance + '/' + id;
        return this._http.put(put_url, amount).map(this.extractData);
    };
    PayrollService.prototype.setId = function (payroll_id) {
        this.id = payroll_id;
    };
    PayrollService.prototype.getId = function () {
        return this.id;
    };
    PayrollService.prototype.getPayroll = function (payroll_id) {
        var get_url = this.action_url + this.route_payroll + '/' + payroll_id;
        return this._http.get(get_url).map(this.extractData);
    };
    PayrollService.prototype.storePayroll = function (payroll_model) {
        var post_url = this.action_url + this.route_payroll;
        return this._http.post(post_url, JSON.stringify(payroll_model)).map(this.extractData);
    };
    PayrollService.prototype.storePayrollList = function (payroll_model) {
        var post_url = this.action_url + this.route_payroll_list;
        return this._http.post(post_url, JSON.stringify(payroll_model)).map(this.extractData);
    };
    PayrollService.prototype.updatePayroll = function (id, payroll) {
        var put_url = this.action_url + this.route_payroll + '/' + id;
        return this._http.put(put_url, payroll).map(this.extractData);
    };
    PayrollService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    PayrollService.prototype.handleError = function (error) {
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    };
    PayrollService.prototype.archivePayroll = function (payroll_id) {
        var delete_url = this.action_url + this.route_payroll + '/' + payroll_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    PayrollService.prototype.archivePayrollList = function (payroll_id) {
        var delete_url = this.action_url + this.route_payroll_list + '/' + payroll_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    PayrollService.prototype.generatePayrollslip = function (id) {
        var get_url = this.action_url + this.route_payslip + '?id=' + id;
        return this._http.get(get_url);
    };
    PayrollService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, HttpClient, Configuration])
    ], PayrollService);
    return PayrollService;
}());
export { PayrollService };
//# sourceMappingURL=payroll.service.js.map