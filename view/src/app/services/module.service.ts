import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Menu } from '../model/menu';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class ModuleService {
    private action_url: string;
    private route_link: string;

    private module_id: string;
    public id:any;
    public name: any;



	constructor(private _http: HttpClient, private _conf: Configuration){
		this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'module';
	}


	public getModules(){
		let get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    }

    
    public getModule(module_id: any){
        let get_url = this.action_url + this.route_link + '/' + module_id;
        return this._http.get(get_url).map(this.extractData);
    }   

    public setId(module_id: any) {
        this.id = module_id;
    }

    public getId() {
        return this.id;
    }

    public storeModule(menu_model: any){
        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(menu_model)).map(this.extractData);
    }

    public updateModule(id: any, menu_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, menu_model).map(this.extractData);

    }

    public archiveModule(module_id: any){
        console.log('postarchive', module_id);
        let delete_url = this.action_url + this.route_link + '/' + module_id;
        console.log(delete_url);
        console.log(module_id);

        return this._http.delete(delete_url).map(this.extractData);
    }


    private extractData(res: Response) {
    	let body = res.json();
    	return body.data || { };
    }

    private handleError (error: Response | any){
    	// In a real world app, we might use a remote logging infrastructure
    	let errMsg: string;
    	if (error instanceof Response) {
    		const body = error.json() || '';
    		const err = body.error || JSON.stringify(body);
    		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    	} else {
    		errMsg = error.message ? error.message : error.toString();
    	}

    	console.error(errMsg);
    	return Observable.throw(errMsg);
    }

}