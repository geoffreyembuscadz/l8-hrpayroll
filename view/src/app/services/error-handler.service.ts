import { Injectable } from '@angular/core';
import { Request, XHRBackend, RequestOptions, Response, Http, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class ExtendedHttpService extends Http {
  body = document.getElementsByTagName('body')[0];
  constructor(backend: XHRBackend, defaultOptions: RequestOptions, private router: Router) {
    super(backend, defaultOptions);
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    return super.request(url, options).catch(this.catchErrors());
  }

  private catchErrors() {
    return (res: Response) => {
      
      if (res.status === 401) {
        
        let response = res.json();
         
        if( response.message != 'invalid_credentials' ){

          localStorage.removeItem('id_token');
          this.router.navigate(['/login']);
          location.reload();

        } else if (response.message == 'invalid_credentials') {
          localStorage.removeItem('id_token');
          this.router.navigate(['/login']);
        }
        
      } else if (res.status === 403) {
          this.router.navigate(['/404']);
      } else if (res.status === 404) {
          this.router.navigate(['/admin']);
      }
      return Observable.throw(res);
    };
  }




}