var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from './http-extended.service';
import { Configuration } from '../app.config';
var PhilhealthService = /** @class */ (function () {
    function PhilhealthService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'department';
    }
    PhilhealthService.prototype.getPhilhealth = function () {
        var philhealth_route = 'philhealth';
        var action_url = this.actionUrl + philhealth_route;
        return this._http.get(action_url).map(this.extractData);
        // let postData = JSON.stringify(headersData);
        // return this._http.get(this.actionUrl + employee_route).map((response: Response) => <Employee[]>response.json());
    };
    PhilhealthService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    PhilhealthService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    PhilhealthService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient,
            Configuration])
    ], PhilhealthService);
    return PhilhealthService;
}());
export { PhilhealthService };
//# sourceMappingURL=philhealth.service.js.map