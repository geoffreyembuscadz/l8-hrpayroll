var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var MenuService = /** @class */ (function () {
    function MenuService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'module_menu';
    }
    MenuService.prototype.getMenus = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    MenuService.prototype.getMenu = function (menu_id) {
        var get_url = this.action_url + this.route_link + '/' + menu_id;
        return this._http.get(get_url).map(this.extractData);
    };
    MenuService.prototype.setId = function (menu_id) {
        this.id = menu_id;
    };
    MenuService.prototype.getId = function () {
        return this.id;
    };
    MenuService.prototype.storeMenu = function (menu_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(menu_model)).map(this.extractData);
    };
    MenuService.prototype.updateMenu = function (id, menu_model) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, menu_model).map(this.extractData);
    };
    MenuService.prototype.archiveMenu = function (menu_id) {
        console.log('postarchive', menu_id);
        var delete_url = this.action_url + this.route_link + '/' + menu_id;
        console.log(delete_url);
        console.log(menu_id);
        return this._http.delete(delete_url).map(this.extractData);
    };
    MenuService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    MenuService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    MenuService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], MenuService);
    return MenuService;
}());
export { MenuService };
//# sourceMappingURL=menu.service.js.map