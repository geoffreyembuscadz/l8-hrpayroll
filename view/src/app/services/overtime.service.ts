import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';


@Injectable()
export class OvertimeService {

  	private action_url: string;
    private route_link: string;
    public id:any;

	constructor(private _http: HttpClient, private _conf: Configuration){
		this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'overtime';
	}

	private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    public createMyOT(model: any){

        let post_url = this.action_url + this.route_link + '/' + 'create/my_ot';

        return this._http.post(post_url, JSON.stringify(model)).map(this.extractData);
    }

    public createOT(model: any){

        let post_url = this.action_url + this.route_link + '/' + 'create';

        return this._http.post(post_url, JSON.stringify(model)).map(this.extractData);
    }

    public getOT(id: any){

        let get_url = this.action_url + this.route_link + '/' + id;

        return this._http.get(get_url).map(this.extractData);
    }

    public getMyOT(model){

        let start = model.start_date;
        let end = model.end_date;
        let status_id = model.status_id;
        let employee_id = model.employee_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end+ '&status_id=' + status_id+ '&employee_id=' + employee_id;

        let get_url = this.action_url + this.route_link + '/' + 'my_ot' + '/' + 'data?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    }

    public getOTList(model: any){

        let start = model.start_date;
        let end = model.end_date;
        let status_id = model.status_id;
        let company_id = model.company_id;
        let employee_id = model.employee_id;
        let supervisor_id = model.supervisor_id;
        let role_id = model.role_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end+ '&status_id=' + status_id+ '&company_id=' + company_id+ '&employee_id=' + employee_id + '&supervisor_id=' + supervisor_id+ '&role_id=' + role_id;

        let get_url = this.action_url + this.route_link + '/' + 'filter' + '/' + 'data?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public updateMyOT(otUpdate: any){

        let post_url = this.action_url + this.route_link + '/update/my_ot';
        return this._http.post(post_url, JSON.stringify(otUpdate)).map(this.extractData);
    }

    public updateOT(otUpdate: any){

        let post_url = this.action_url + this.route_link + '/update/ot';
        return this._http.post(post_url, JSON.stringify(otUpdate)).map(this.extractData);
    }

    public duplicateEntryValidation(ot: any){

        let post_url = this.action_url + this.route_link + '/duplicate_entry_validation';
        return this._http.post(post_url, JSON.stringify(ot)).map(this.extractData);
    }

}
