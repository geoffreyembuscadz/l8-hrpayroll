var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var MemoService = /** @class */ (function () {
    function MemoService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'memo';
    }
    MemoService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    MemoService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    MemoService.prototype.getMemos = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    MemoService.prototype.setId = function (memo_id) {
        this.id = memo_id;
        console.log('setId: ', memo_id);
    };
    MemoService.prototype.getId = function () {
        return this.id;
    };
    MemoService.prototype.getMemo = function (memos_id) {
        var get_url = this.action_url + this.route_link + '/' + memos_id;
        return this._http.get(get_url).map(this.extractData);
    };
    MemoService.prototype.storeMemo = function (memo_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(memo_model)).map(this.extractData);
    };
    MemoService.prototype.updateMemo = function (id, memo_model) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, memo_model).map(this.extractData);
    };
    MemoService.prototype.archiveMemo = function (memo_id) {
        console.log('postarchive', memo_id);
        var delete_url = this.action_url + this.route_link + '/' + memo_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    MemoService.prototype.getMemoList = function () {
        var get_url = this.action_url + this.route_link + '/list';
        return this._http.get(get_url).map(this.extractData);
    };
    MemoService.prototype.showFilterMemo = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var emp_id = model.emp_id;
        var company_id = model.company_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&emp_id=' + emp_id + '&company_id=' + company_id;
        var action_url = this.action_url + 'filter/memo?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    MemoService.prototype.createMemo = function (memo_model) {
        var post_url = this.action_url + this.route_link + '/create';
        return this._http.post(post_url, JSON.stringify(memo_model)).map(this.extractData);
    };
    MemoService.prototype.showMemo = function () {
        var get_url = this.action_url + this.route_link + '/show';
        return this._http.get(get_url).map(this.extractData);
    };
    MemoService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient,
            Configuration])
    ], MemoService);
    return MemoService;
}());
export { MemoService };
//# sourceMappingURL=memo.service.js.map