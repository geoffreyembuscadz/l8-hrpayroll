var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var MainSideService = /** @class */ (function () {
    function MainSideService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link1 = 'module';
        this.route_link2 = 'module_menu';
    }
    MainSideService.prototype.getModule = function () {
        var get_url = this.action_url + this.route_link1;
        return this._http.get(get_url).map(this.extractData);
    };
    MainSideService.prototype.getModuleById = function (user_id) {
        var get_url = this.action_url + this.route_link1 + '/' + user_id;
        return this._http.get(get_url).map(this.extractData);
    };
    MainSideService.prototype.getModuleMenu = function () {
        var get_url = this.action_url + this.route_link2;
        return this._http.get(get_url).map(this.extractData);
    };
    MainSideService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    MainSideService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], MainSideService);
    return MainSideService;
}());
export { MainSideService };
//# sourceMappingURL=main-side.service.js.map