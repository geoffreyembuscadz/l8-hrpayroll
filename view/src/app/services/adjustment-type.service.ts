import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from './http-extended.service';
import { Configuration } from '../app.config';

@Injectable()
export class AdjustmentTypeService {
    private action_url: string;
    private route_link: string;
    public id: any;

	constructor(private _http: HttpClient, private _conf: Configuration){
		this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'adjustment_type';
	}

	public getAdjustmentsType(){
		let get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    }

    public getAdjustmentType(adjustment_id: any){
        let get_url = this.action_url + this.route_link + '/' + adjustment_id;
        return this._http.get(get_url).map(this.extractData);
    }

    public setId(adjustment_id: any) {
        this.id = adjustment_id;
    }
    public getId() {
        return this.id;
    }

    public updateAdjustmentType(id: any, adjustment_id: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, adjustment_id).map(this.extractData);
    }

    public archiveAdjustmentType(adjustment_id: any){
        let delete_url = this.action_url + this.route_link + '/' + adjustment_id;

        return this._http.delete(delete_url ).map(this.extractData);
    }

    public storeAdjustmentType(adjustment_id: any){
        let post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(adjustment_id)).map(this.extractData);
    }

    private extractData(res: Response) {
    	let body = res.json();
    	return body.data || { };
    }

    private handleError (error: Response | any){
    	// In a real world app, we might use a remote logging infrastructure
    	let errMsg: string;
    	if (error instanceof Response) {
    		const body = error.json() || '';
    		const err = body.error || JSON.stringify(body);
    		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    	} else {
    		errMsg = error.message ? error.message : error.toString();
    	}

    	console.error(errMsg);
    	return Observable.throw(errMsg);
  }
}