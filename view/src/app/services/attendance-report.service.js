var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var AttendanceReportService = /** @class */ (function () {
    function AttendanceReportService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
    }
    AttendanceReportService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    AttendanceReportService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    AttendanceReportService.prototype.getAttendanceReport = function () {
        var action_url = this.action_url + 'report';
        return this._http.get(action_url).map(this.extractData);
    };
    AttendanceReportService.prototype.getLateEarlyReport = function () {
        var action_url = this.action_url + 'late/early/report';
        return this._http.get(action_url).map(this.extractData);
    };
    AttendanceReportService.prototype.statusReport = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var emp = model.emp_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&emp_id=' + emp;
        var action_url = this.action_url + 'status/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AttendanceReportService.prototype.hoursReport = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var emp = model.emp_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&emp_id=' + emp;
        var action_url = this.action_url + 'hours/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AttendanceReportService.prototype.getDaySummaryReport = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var emp = model.emp_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end;
        var action_url = this.action_url + 'days/summary/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AttendanceReportService.prototype.getHoursSummaryReport = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var uri_data = 'start_date=' + start + '&end_date=' + end;
        var action_url = this.action_url + 'hours/summary/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AttendanceReportService.prototype.lateErlyFltr = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var emp = model.emp_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&employee_id=' + emp;
        var action_url = this.action_url + 'late/erly/rprt/fltr?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AttendanceReportService.prototype.userReport = function (model) {
        var emp = model.emp_id;
        var start = model.start_date;
        var end = model.end_date;
        var comp_id = model.comp_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&company_id=' + comp_id + '&emp_id=' + emp;
        var action_url = this.action_url + 'user/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AttendanceReportService.prototype.getEmpByComp = function (company_id) {
        var comp = company_id;
        var uri_data = 'company_id=' + comp;
        var action_url = this.action_url + 'getcomp/byemp?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AttendanceReportService.prototype.perEmpLateEarly = function (model) {
        var emp = model.emp_id;
        var start = model.start_date;
        var end = model.end_date;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&emp_id=' + emp;
        var action_url = this.action_url + 'perEmp/attendance/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AttendanceReportService.prototype.perEmpDaysSummary = function (model) {
        var emp = model.emp_id;
        var start = model.start_date;
        var end = model.end_date;
        var uri_data = 'emp_id=' + emp + '&end_date=' + end + '&start_date=' + start;
        var action_url = this.action_url + 'per/emp/days/summary/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AttendanceReportService.prototype.perEmpHoursSummary = function (model) {
        var emp = model.emp_id;
        var start = model.start_date;
        var end = model.end_date;
        var uri_data = 'emp_id=' + emp + '&end_date=' + end + '&start_date=' + start;
        var action_url = this.action_url + 'per/emp/hours/summary/report?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AttendanceReportService.prototype.workingSched = function (model) {
        var emp = model.emp_id;
        var uri_data = 'emp_id=' + emp;
        var action_url = this.action_url + 'working/schedule?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AttendanceReportService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient,
            Configuration])
    ], AttendanceReportService);
    return AttendanceReportService;
}());
export { AttendanceReportService };
//# sourceMappingURL=attendance-report.service.js.map