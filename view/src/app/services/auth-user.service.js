var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var AuthUserService = /** @class */ (function () {
    function AuthUserService(http, _http, _conf) {
        this.http = http;
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link_in = 'in';
        this.route_link = 'users';
    }
    AuthUserService.prototype.setId = function (user_id) {
        this.id = user_id;
    };
    AuthUserService.prototype.getId = function () {
        return this.id;
    };
    AuthUserService.prototype.setRoles = function (roles) {
        this.roles = roles;
    };
    AuthUserService.prototype.getUser = function () {
        var get_url = this.action_url + this.route_link_in;
        return this._http.get(get_url).map(this.extractData);
    };
    AuthUserService.prototype.getUserById = function (user_id) {
        var get_url = this.action_url + this.route_link + '/' + user_id;
        return this._http.get(get_url).map(this.extractData);
    };
    AuthUserService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    AuthUserService.prototype.getListByIdLimited = function () {
        var get_url = this.action_url + this.route_link + '/list/limited';
        return this._http.get(get_url).map(this.extractData);
    };
    AuthUserService.prototype.getPermissionId = function () {
        var get_url = this.action_url + this.route_link + '/list/permission';
        return this._http.get(get_url).map(this.extractData);
    };
    AuthUserService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http, HttpClient, Configuration])
    ], AuthUserService);
    return AuthUserService;
}());
export { AuthUserService };
//# sourceMappingURL=auth-user.service.js.map