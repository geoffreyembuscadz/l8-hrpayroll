var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var EventService = /** @class */ (function () {
    function EventService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'event';
    }
    EventService.prototype.getEvents = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    EventService.prototype.getEventType = function () {
        var get_url = this.action_url + 'event_type';
        return this._http.get(get_url).map(this.extractData);
    };
    EventService.prototype.getEventStat = function () {
        var get_url = this.action_url + this.route_link + '/' + 'event_status';
        return this._http.get(get_url).map(this.extractData);
    };
    EventService.prototype.setId = function (event_id) {
        this.id = event_id;
        console.log('setId: ', this.id);
    };
    EventService.prototype.getId = function () {
        return this.id;
    };
    EventService.prototype.getEvent = function (events_id) {
        var get_url = this.action_url + this.route_link + '/' + events_id;
        return this._http.get(get_url).map(this.extractData);
    };
    EventService.prototype.storeEvent = function (event_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(event_model)).map(this.extractData);
    };
    EventService.prototype.storeEventDetails = function (event_model) {
        var post_url = this.action_url + this.route_link + '/' + 'event_details';
        return this._http.post(post_url, JSON.stringify(event_model)).map(this.extractData);
    };
    EventService.prototype.storeEventType = function (event_model) {
        var post_url = this.action_url + this.route_link + '/' + 'event_type';
        console.log(post_url);
        return this._http.post(post_url, JSON.stringify(event_model)).map(this.extractData);
    };
    EventService.prototype.updateEvent = function (id, event_model) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, event_model).map(this.extractData);
    };
    EventService.prototype.archiveEvent = function (event_id) {
        console.log('postarchive', event_id);
        var delete_url = this.action_url + this.route_link + '/' + event_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    EventService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    EventService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    EventService.prototype.getEventDetails = function () {
        var action_url = this.action_url + this.route_link;
        return this._http.get(action_url).map(this.extractData);
        // let postData = JSON.stringify(headersData);
        // return this._http.get(this.actionUrl + employee_route).map((response: Response) => <Employee[]>response.json());
    };
    EventService.prototype.getEventStatus = function (event_id) {
        var get_url = this.action_url + this.route_link + '/' + event_id;
        return this._http.get(get_url).map(this.extractData);
    };
    EventService.prototype.updateStatus = function (id_status, eventUpdate) {
        var post_url = this.action_url + this.route_link + '/' + id_status;
        return this._http.put(post_url, eventUpdate).map(this.extractData);
    };
    EventService.prototype.getStatus = function () {
        var leave_route = 'leavestatus';
        var action_url = this.action_url + leave_route;
        return this._http.get(action_url).map(this.extractData);
        // let postData = JSON.stringify(headersData);
        // return this._http.get(this.action_url + employee_route).map((response: Response) => <Employee[]>response.json());
    };
    EventService.prototype.updateEventStatus = function (id_status, update_model) {
        var post_url = this.action_url + this.route_link + '/' + id_status;
        console.log(post_url);
        return this._http.put(post_url, update_model).map(this.extractData);
    };
    EventService.prototype.getEventLists = function (filterEvent) {
        var get_url = this.action_url + 'event' + '/' + 'time_table' + '/' + filterEvent;
        return this._http.get(get_url).map(this.extractData);
    };
    EventService.prototype.getEventName = function (model) {
        var event = model.event_id;
        var uri_data = 'event_id=' + event;
        var action_url = this.action_url + this.route_link + '/' + 'name?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    EventService.prototype.getEventList = function () {
        var action_url = this.action_url + this.route_link + '/' + 'events';
        return this._http.get(action_url).map(this.extractData);
    };
    EventService.prototype.getEventTypeLists = function () {
        var action_url = this.action_url + this.route_link + '/' + 'et';
        return this._http.get(action_url).map(this.extractData);
    };
    EventService.prototype.getEventsList = function () {
        var action_url = this.action_url + 'list/event';
        return this._http.get(action_url).map(this.extractData);
    };
    EventService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], EventService);
    return EventService;
}());
export { EventService };
//# sourceMappingURL=event.service.js.map