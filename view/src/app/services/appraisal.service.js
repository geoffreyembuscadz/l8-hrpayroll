var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var AppraisalService = /** @class */ (function () {
    function AppraisalService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'appraisal';
    }
    AppraisalService.prototype.getAppraisals = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    AppraisalService.prototype.setId = function (appraisal_id) {
        this.id = appraisal_id;
        console.log('setId: ', appraisal_id);
    };
    AppraisalService.prototype.getId = function () {
        return this.id;
    };
    AppraisalService.prototype.getAppraisal = function (appraisals_id) {
        var get_url = this.action_url + this.route_link + '/' + appraisals_id;
        return this._http.get(get_url).map(this.extractData);
    };
    AppraisalService.prototype.storeAppraisal = function (appraisal_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(appraisal_model)).map(this.extractData);
    };
    AppraisalService.prototype.updateAppraisal = function (id, appraisal_model) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, appraisal_model).map(this.extractData);
    };
    AppraisalService.prototype.archiveAppraisal = function (appraisal_id) {
        console.log('postarchive', appraisal_id);
        var delete_url = this.action_url + this.route_link + '/' + appraisal_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    AppraisalService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    AppraisalService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    AppraisalService.prototype.getObjectives = function (model) {
        var employee_id = model.employee_id;
        var uri_data = 'employee_id=' + employee_id;
        var action_url = this.action_url + 'show/appraisal?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AppraisalService.prototype.updateObj = function (ot) {
        var post_url = this.action_url + 'appraisal/update';
        return this._http.post(post_url, JSON.stringify(ot)).map(this.extractData);
    };
    AppraisalService.prototype.getEmployee = function () {
        var action_url = this.action_url + 'all/employee';
        return this._http.get(action_url).map(this.extractData);
    };
    AppraisalService.prototype.getObjNow = function (model) {
        var employee_id = model.employee_id;
        var obj_id = model.obj_id;
        var uri_data = 'employee_id=' + employee_id;
        var action_url = this.action_url + 'get/obj?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AppraisalService.prototype.getObjRate = function (model) {
        var employee_id = model.employee_id;
        var id = model.id;
        var uri_data = 'employee_id=' + employee_id + '&id=' + id;
        var action_url = this.action_url + 'rate/obj?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    AppraisalService.prototype.saveRateObj = function (obj_model) {
        var post_url = this.action_url + 'save/obj/rate';
        return this._http.post(post_url, JSON.stringify(obj_model)).map(this.extractData);
    };
    AppraisalService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], AppraisalService);
    return AppraisalService;
}());
export { AppraisalService };
//# sourceMappingURL=appraisal.service.js.map