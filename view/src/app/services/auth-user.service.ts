import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Users } from '../model/users';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class AuthUserService {
    private action_url: string;
    private route_link: string;
    private route_link_in: string;
    public id: any;
    public roles: Array<any>;

	constructor(private http: Http, private _http: HttpClient, private _conf: Configuration){
		this.action_url = this._conf.ServerWithApiUrl;
        this.route_link_in = 'in';
        this.route_link = 'users';
	}

  public setId(user_id: any) {
      this.id = user_id;
  }

  public getId() {
      return this.id;
  }

  public setRoles(roles: any) {
      this.roles = roles;
  }

	public getUser() {
    let get_url = this.action_url + this.route_link_in;

    return this._http.get(get_url).map(this.extractData);
  }

  public getUserById(user_id: any) {
    let get_url = this.action_url + this.route_link + '/' + user_id;
    
    return this._http.get(get_url).map(this.extractData);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || { };
  }
  public getListByIdLimited() {
    let get_url = this.action_url + this.route_link + '/list/limited';

    return this._http.get(get_url).map(this.extractData);
  }

  public getPermissionId() {
    let get_url = this.action_url + this.route_link + '/list/permission';

    return this._http.get(get_url).map(this.extractData);
  }
}