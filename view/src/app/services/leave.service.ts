import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Leave } from '../model/leave';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';


@Injectable()
export class LeaveService {

    private action_url: string;
    private route_link: string;
    private headers: Headers;
    public id:any;
	constructor(private _http: HttpClient,  private _conf: Configuration){

        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'leave';

	}

    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    public storeLeave(leave_model: any){
        

        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(leave_model)).map(this.extractData);
        
    }

    public createLeave(leave_model: any){
        

        let post_url = this.action_url + this.route_link + '/' + 'create';

        return this._http.post(post_url, JSON.stringify(leave_model)).map(this.extractData);
    }

    public createMyLeave(leave_model: any){
        

        let post_url = this.action_url + this.route_link + '/' + 'create/my_leave';

        return this._http.post(post_url, JSON.stringify(leave_model)).map(this.extractData);
    }

	// public getLeave(){

	// 	let url = this.action_url + this.route_link;

	// 	return this._http.get(url).map(this.extractData);
 //        // let postData = JSON.stringify(headersData);
 //        // return this._http.get(this.actionUrl + employee_route).map((response: Response) => <Employee[]>response.json());
 //    }

    public updateLeave(leaveUpdate: any){

        let post_url = this.action_url + this.route_link + '/update/leave';
        return this._http.post(post_url, JSON.stringify(leaveUpdate)).map(this.extractData);
    }

    public updateMyLeave(leaveUpdate: any){

        let post_url = this.action_url + this.route_link + '/update/my_leave';
        return this._http.post(post_url, JSON.stringify(leaveUpdate)).map(this.extractData);
    }

    // public updateLeave(leave_model: any){

    //     let post_url = this.action_url + this.route_link + '/' + 'update';

    //     return this._http.post(post_url, JSON.stringify(leave_model)).map(this.extractData);
    // }

    public getLeave(leave_id: any){

        let get_url = this.action_url + this.route_link + '/' + leave_id;

        return this._http.get(get_url).map(this.extractData);
    }


    public getMyleave(model){

        let start = model.start_date;
        let end = model.end_date;
        let status_id = model.status_id;
        let employee_id = model.employee_id;
        let type_id = model.type_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end+ '&status_id=' + status_id+ '&employee_id=' + employee_id+ '&type_id=' + type_id;
        
        let get_url = this.action_url + this.route_link + '/' + 'my_leave' + '/' + 'data?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }


    public getLeavePending(){

        let action_url = this.action_url + 'attendancereport' + '/' + 'pendingleave';

        return this._http.get(action_url).map(this.extractData);
          
    }

    public getLeaves(model: any){

        let start = model.start_date;
        let end = model.end_date;
        let status_id = model.status_id;
        let company_id = model.company_id;
        let employee_id = model.employee_id;
        let type_id = model.type_id;
        let supervisor_id = model.supervisor_id;
        let role_id = model.role_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end+ '&status_id=' + status_id+ '&company_id=' + company_id+ '&employee_id=' + employee_id+ '&type_id=' + type_id+ '&supervisor_id=' + supervisor_id+ '&role_id=' + role_id;

        let get_url = this.action_url + this.route_link + '/' + 'filter' + '/' + 'data?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public duplicateEntryValidation(leave_model: any){
        
        let post_url = this.action_url + this.route_link + '/' + 'duplicate_entry_validation';

        return this._http.post(post_url, JSON.stringify(leave_model)).map(this.extractData);
    }

    public leaveCreate(leave_model: any){
        
        let post_url = this.action_url + this.route_link + '/' + 'leave_create';

        return this._http.post(post_url, JSON.stringify(leave_model)).map(this.extractData);
    }
    public leaveUpdate(leaveUpdate: any){

        let post_url = this.action_url + this.route_link + '/validate/leave';
        return this._http.post(post_url, JSON.stringify(leaveUpdate)).map(this.extractData);
    }

    public getWorkingSchedule(id){

        let uri_data = 'emp_id=' + id ;

        let get_url = this.action_url + this.route_link + '/' + 'working/sched?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }


}
