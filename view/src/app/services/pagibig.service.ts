import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from './http-extended.service';

@Injectable()
export class PagibigService {

 private actionUrl: string;
	private headers: Headers;

	constructor(private _http: HttpClient){
		this.actionUrl = 'http://localhost:8000/api/';

	
	}

	public getPagibig(){
		let pagibig_route = 'pagibig';
		let action_url = this.actionUrl + pagibig_route;

		return this._http.get(action_url).map(this.extractData);
        // let postData = JSON.stringify(headersData);
        // return this._http.get(this.actionUrl + employee_route).map((response: Response) => <Employee[]>response.json());
    }

    private extractData(res: Response) {
    	let body = res.json();
    	return body.data || { };
    }

    private handleError (error: Response | any){
    	// In a real world app, we might use a remote logging infrastructure
    	let errMsg: string;
    	if (error instanceof Response) {
    		const body = error.json() || '';
    		const err = body.error || JSON.stringify(body);
    		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    	} else {
    		errMsg = error.message ? error.message : error.toString();
    	}

    	console.error(errMsg);
    	return Observable.throw(errMsg);
  }

}
