import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Attendance } from '../model/attendance';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';


@Injectable()
export class AttendanceService {

      private action_url: string;
    private route_link: string;
    private headers: Headers;
    data=[];

    constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'attendance';
    }

    private extractData(res: Response){
        let body = res.json();
        return body.data || { };
    }

    private handleError(error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    public getEmployeeData(model){

        let post_url = this.action_url + this.route_link + '/search_employee';

        return this._http.post(post_url, model).map(this.extractData);
    }

    public storeAttendance(attendance_model: any){
        
        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(attendance_model)).map(this.extractData);
    }

    public createTimeAttendance(att_model: any){

        let post_url = this.action_url + this.route_link + '/' + 'create_time';

        return this._http.post(post_url, JSON.stringify(att_model)).map(this.extractData);
    }

    public createDayAttendanceDefault(att_model: any){

        let post_url = this.action_url + this.route_link + '/' + 'create_day_default';

        return this._http.post(post_url, JSON.stringify(att_model)).map(this.extractData);
    }

    public createDayAttendance(att_model: any){

        let post_url = this.action_url + this.route_link + '/' + 'create_day';

        return this._http.post(post_url, JSON.stringify(att_model)).map(this.extractData);
    }


    public getAttendanceDefault(id: any){
        let get_url = this.action_url + this.route_link + '/' + id;

        return this._http.get(get_url).map(this.extractData);
    }

    public getAttendance(id: any){
        let get_url = this.action_url + this.route_link + '/not/default/' + id;

        return this._http.get(get_url).map(this.extractData);
    }

    public archiveAttendance(employees_id: any){

        let delete_url = this.action_url + this.route_link + '/' + employees_id;

        return this._http.delete(delete_url ).map(this.extractData);
    }

    public updateAttendance(id: any, model: any){
        let put_url = this.action_url +  this.route_link + '/' + id;

        return this._http.put(put_url, model).map(this.extractData);
    }

    public updateAttendanceNotDefault(id: any, model: any){
        let put_url = this.action_url +  this.route_link + '/not/default/update/' + id;

        return this._http.put(put_url, model).map(this.extractData);
    }

    public attachAttendanceFiles(file: any){

        let post_url = this.action_url + this.route_link + '/' + 'import_file';

        return this._http.post(post_url, JSON.stringify(file)).map(this.extractData);
    }

    public getTotalPresent(){
        let get_url = this.action_url + 'present' + '/' + 'attendancereport';

        return this._http.get(get_url).map(this.extractData);
    }


    public getTotalEmp(filter){
        let get_url = this.action_url + 'totalemp' + '/' + 'attendancereport' + '/' + filter;

        return this._http.get(get_url).map(this.extractData);
    }

    public selectedDate(){
        let get_url = this.action_url + 'attendance' + '/' + 'date';

        return this._http.get(get_url).map(this.extractData);
    }

    public attendanceReport(employee:any, company:any,department:any, start_date: any, end_date: any){

        let uri_data = 'company_id=' + company + '&department_id=' + department + '&employee_id=' + employee + '&start_date=' + start_date + '&end_date=' + end_date;
        console.log(uri_data);
        let get_url = this.action_url  + 'attendancereport' + '/' + 'graph?'  + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public getAttendDefault(model: any){

        let start = model.start_date;
        let end = model.end_date;
        let type_id = model.type_id;
        let company_id = model.company_id;
        let employee_id = model.employee_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end+ '&type_id=' + type_id+ '&company_id=' + company_id+ '&employee_id=' + employee_id;

        let get_url = this.action_url + this.route_link + '/' + 'for/default/data?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public getAttend(model: any){

        let start = model.start_date;
        let end = model.end_date;
        let type_id = model.type_id;
        let company_id = model.company_id;
        let employee_id = model.employee_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end+ '&type_id=' + type_id+ '&company_id=' + company_id+ '&employee_id=' + employee_id;

        let get_url = this.action_url + this.route_link + '/' + 'filter' + '/'+ 'data?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public getMyAttendance(model){

        let start = model.start_date;
        let end = model.end_date;
        let type_id = model.type_id;
        let employee_id = model.employee_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end+ '&type_id=' + type_id+ '&employee_id=' + employee_id ;

        let get_url = this.action_url + this.route_link + '/' + 'my_attendance' + '/'+ 'data?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public getSchedule(model){

        let post_url = this.action_url + this.route_link + '/' + 'data/schedule';

        return this._http.post(post_url, model).map(this.extractData);
    }

    public convertFile(file: any){
       let file_url = file.file_url;
       let url_data = '&file_url=' + file_url;

       let post_url = this.action_url + this.route_link  + '/' + 'convert_file?' + url_data;

       return this._http.post(post_url, JSON.stringify(file)).map(this.extractData);
    }

    public getMyLogs(){

        let get_url = this.action_url + this.route_link + '/' + 'my/logs';

        return this._http.get(get_url).map(this.extractData);
    }

    public createAttendanceFromWeb(att_model: any){

        let post_url = this.action_url + this.route_link + '/' + 'create_from_web';

        return this._http.post(post_url, JSON.stringify(att_model)).map(this.extractData);
    }

    public attchAttendExcel(file: any){
       let file_url = file.file_url;
       let url_data = '&file_url=' + file_url;

       let post_url = this.action_url + this.route_link  + '/' + 'import_excel_1?' + url_data;
       return this._http.post(post_url, JSON.stringify(file)).map(this.extractData);
    }

    public attchAttendExcel2(file: any){
       let file_url = file.file_url;
       let url_data = '&file_url=' + file_url;

       let post_url = this.action_url + this.route_link  + '/' + 'import_excel_2?' + url_data;
       return this._http.post(post_url, JSON.stringify(file)).map(this.extractData);
    }
    
    public attchAttendExcel3(file: any){
       let file_url = file.file_url;
       let url_data = '&file_url=' + file_url;

       let post_url = this.action_url + this.route_link  + '/' + 'import_excel_3?' + url_data;
       return this._http.post(post_url, JSON.stringify(file)).map(this.extractData);
    }

    public getDataFromBiometrics(device){


        let uri_data = 'ip_address=' + device ;

        let get_url = this.action_url + this.route_link + '/' + 'import' + '/'+ 'bio?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public setData(data: any) {
        this.data = data;
    }
    public getData() {
        return this.data;
    }

    public logsValidation(){

        let get_url = this.action_url + this.route_link + '/' + 'logs/validation';

        return this._http.get(get_url).map(this.extractData);
    }

    public getEmployeeRate(id){

        let post_url = this.action_url + this.route_link + '/' + 'employee_rate';

        return this._http.post(post_url, JSON.stringify(id)).map(this.extractData);
    }

    public ignoreEmpInDayAttendance(att_model: any){

        let post_url = this.action_url + this.route_link + '/' + 'ignore_emp/day_attendance';

        return this._http.post(post_url, JSON.stringify(att_model)).map(this.extractData);
    }

    public getUploadHistoryFromAttendance(){
        let get_url = this.action_url + 'attendance_upload_history';

        return this._http.get(get_url).map(this.extractData);
    }


    public deleteAttendanceByBatch(id: any){

        let delete_url = this.action_url + 'attendance_upload_history' + '/' + id;

        return this._http.delete(delete_url ).map(this.extractData);
    }

    public getWorkingSchedulePerCompany(company_id: any){

        let get_url = this.action_url +  'attendance/working_schedule/company?company_id=' + company_id;

        return this._http.get(get_url).map(this.extractData);
    }

}