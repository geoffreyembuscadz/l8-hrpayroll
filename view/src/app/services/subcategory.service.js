var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var SubcategoryService = /** @class */ (function () {
    function SubcategoryService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'subcategory';
    }
    SubcategoryService.prototype.getSubcategorys = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    SubcategoryService.prototype.setId = function (subcategory_id) {
        this.id = subcategory_id;
        console.log('setId: ', subcategory_id);
    };
    SubcategoryService.prototype.getId = function () {
        return this.id;
    };
    SubcategoryService.prototype.getSubcategory = function (subcategorys_id) {
        var get_url = this.action_url + this.route_link + '/' + subcategorys_id;
        return this._http.get(get_url).map(this.extractData);
    };
    SubcategoryService.prototype.storeSubcategory = function (subcategory_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(subcategory_model)).map(this.extractData);
    };
    SubcategoryService.prototype.updateSubcategory = function (id, subcategory_model) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, subcategory_model).map(this.extractData);
    };
    SubcategoryService.prototype.archiveSubcategory = function (subcategory_id) {
        console.log('postarchive', subcategory_id);
        var delete_url = this.action_url + this.route_link + '/' + subcategory_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    SubcategoryService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    SubcategoryService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    };
    SubcategoryService.prototype.subcategory_name = function () {
        var action_url = this.action_url + 'subcategory/name';
        return this._http.get(action_url).map(this.extractData);
    };
    SubcategoryService.prototype.questionList = function () {
        var action_url = this.action_url + 'question/list';
        return this._http.get(action_url).map(this.extractData);
    };
    SubcategoryService.prototype.storeSubcategoryDetails = function (subcategory_model) {
        var post_url = this.action_url + 'post/subcategory';
        return this._http.post(post_url, JSON.stringify(subcategory_model)).map(this.extractData);
    };
    SubcategoryService.prototype.catName = function () {
        var action_url = this.action_url + 'cat/name';
        return this._http.get(action_url).map(this.extractData);
    };
    SubcategoryService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient,
            Configuration])
    ], SubcategoryService);
    return SubcategoryService;
}());
export { SubcategoryService };
//# sourceMappingURL=subcategory.service.js.map