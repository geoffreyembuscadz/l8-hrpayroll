import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Configuration } from '../app.config';
import { Employee } from '../model/employee';
//import { Configuration } from '../app.config';

@Injectable()
export class DynastyLoanService {
	private action_url: string;
    private route_employee = 'employee';
    private route_dynasty_loan = 'dynasty_loan';
    private route_admin_dynasty_loan = 'admin_dynasty_loan';
    private payback_modes = [
        { id: 1, name: 'Weekly' },
        { id: 2, name: '5th & 20th' },
        { id: 3, name: '10th & 20th' },
        { id: 4, name: '15th & 30th' }
    ];
    private types_of_loan = [
        { id: 1, name: 'emergency' },
        { id: 2, name: 'multi-purpose' },
        { id: 3, name: 'appliance / cellphone' },
        { id: 4, name: 'automotive vehicle' },
        { id: 5, name: 'livelihood' },
        { id: 6, name: 'motorcycle' },
        { id: 7, name: 'others' },
    ];
    private route_export : string;

	private headers: Headers;

    // Static Selection Options
    private name_suffixes = [
        { name: '-none-' },
        { name: 'Jr.' },
        { name: 'Sr.' },
        { name: 'III' },
        { name: 'IV' }
    ];

	constructor(private _http: Http, private _conf: Configuration){
		this.action_url = this._conf.ServerWithApiUrl;
        
        // Header Variables
		this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        //console.log(authToken);
	}

    public getDynastyLoanRecord(id: any){
        let get_url = this.action_url + this.route_admin_dynasty_loan + '/' + id;

        return this._http.get(get_url, { headers:this.headers }).map(this.extractData);
    }

    public getEmployee(employee_id: any){
        let get_url = this.action_url + this.route_employee + '/' + employee_id;

        return this._http.get(get_url, { headers:this.headers }).map(this.extractData);
    }

    public getPaybackModes(){
        return this.payback_modes;
    }

    public getTypesOfLoan(){
        return this.types_of_loan;
    }

    public updateDynastyLoan(id: any, employee_dynasty_loan_model: any){
        let put_url = this.action_url + this.route_admin_dynasty_loan + '/' + id;

        return this._http.put(put_url, employee_dynasty_loan_model, { headers:this.headers }).map(this.extractData);
    }

    private extractData(res: Response){
        let body = res.json();
        return body.data || { };
    }

    /*
    public getDepartments(){ // Dynamic of Departments soon.
        let department_url = this.action_url + 'department';

        return this._http.get(department_url, { headers:this.headers }).map(this.extractData);
    }

    public getJobTitles(){ // Dynamic of Positions soon.
        let positions_url = this.action_url + 'position';

        return this._http.get(positions_url, { headers:this.headers }).map(this.extractData);
    }

	public getEmployees(uri_data?: any){
		let get_url = this.action_url + this.route_link + '?' + uri_data;

		return this._http.get(get_url, { headers:this.headers }).map(this.extractData);
    }

    public getEmployee(employee_id: any){
        let get_url = this.action_url + this.route_link + '/' + employee_id;

        return this._http.get(get_url, { headers:this.headers }).map(this.extractData);
    }

    public getCompanies(){
        let route_company = 'company';
        let get_url = this.action_url + route_company;

        return this._http.get(get_url, { headers:this.headers }).map(this.extractData);
    }

    public storeEmployee(employee_model: any){
        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(employee_model), { headers:this.headers }).map(this.extractData);
    }

    public uploadMassEmployeeCsv(data: any){
        let post_url = this.action_url + this.route_upload;

        return this._http.post(post_url, JSON.stringify(data), {headers: this.headers}).map(this.extractData);
    }

    public addEmployeeRemark(employee_remarks_model: any){
        let post_url = this.action_url + 'employee_remark';

        return this._http.post(post_url, JSON.stringify(employee_remarks_model), { headers:this.headers }).map(this.extractData);
    }

    public getAttachments(employee_id: any){
        let get_attachment_url = this.action_url + this.route_attachment + '?employee_id=' + employee_id;

        return this._http.get(get_attachment_url, {headers:this.headers}).map(this.extractData);
    }
    public attachEmployeeFiles(employee_id: any, employee_attachment: any, employee_attachment_dir: any){
        let route_upload_attachment = this.route_attachment;
        let post_url = this.action_url + route_upload_attachment;

        let employee_pass_data = {
            employee_id: employee_id,

            file_url: employee_attachment,
            file_dir: employee_attachment_dir

        };

        return this._http.post(post_url, JSON.stringify(employee_pass_data), { headers:this.headers }).map(this.extractData);
    }

    public updateEmployee(id: any, employee_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, employee_model, { headers:this.headers }).map(this.extractData);
    }

    public exportEmployeeList(employee_form: any){
        let url = this.action_url + this.route_export;

        return this._http.post(url , employee_form, { headers:this.headers }).map(this.extractData);
    }

    private extractData(res: Response){
    	let body = res.json();
    	return body.data || { };
    }

    private handleError(error: Response | any){
    	// In a real world app, we might use a remote logging infrastructure
    	let errMsg: string;
    	if (error instanceof Response) {
    		const body = error.json() || '';
    		const err = body.error || JSON.stringify(body);
    		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    	} else {
    		errMsg = error.message ? error.message : error.toString();
    	}

    	return Observable.throw(errMsg);
    }

    public archiveEmployee(employee_id: any){

        console.log('postarchive', employee_id);
        let delete_url = this.action_url + this.route_link + '/' + employee_id;
        console.log(delete_url);
        console.log(employee_id);

        return this._http.delete(delete_url, { headers:this.headers } ).map(this.extractData);
    }

    public deleteEmployeAttachment(attachment_id: any){
        // Delete Employee File Attached
        let delete_url = this.action_url + this.route_attachment + '/' + attachment_id;

        return this._http.delete(delete_url).map(this.extractData);
    }
    */

}