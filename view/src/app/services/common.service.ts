import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Role } from '../model/role';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class CommonService {

    private action_url: string;
    private route_link: string;
    private days = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ];
    private option_time_hr_24: any;
    private option_time_mins: any;

    constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'common';

        this.option_time_hr_24 = Array(25).fill(1).map((x,i)=>i);
        this.option_time_mins = Array(60).fill(1).map((x,i)=>i);
        this.option_time_hr_24[0] = 12; // Setting the zero index to 12thHR
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    public getEmployees(supervisor_id:any = '', company_id:any = ''){

        let uri_data = 'supervisor_id=' + supervisor_id + '&company_id=' + company_id;

        let get_url = this.action_url + this.route_link + '/' + 'employees?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public getEmployee(){

        let get_url = this.action_url + this.route_link + '/' + 'employee';

        return this._http.get(get_url).map(this.extractData);
    }

    public getLeaveType(){
        let get_url = this.action_url + this.route_link + '/' + 'leave_type';

        return this._http.get(get_url).map(this.extractData);
    }

    public getOBType(){
        let get_url = this.action_url + this.route_link + '/' + 'ob_type';

        return this._http.get(get_url).map(this.extractData);
    }

    public getStatusType(){

        let action_url = this.action_url + this.route_link + '/' + 'status_type';

        return this._http.get(action_url).map(this.extractData);
    }

    public getCOAType(){
        let get_url = this.action_url + this.route_link + '/' + 'coa_type';

        return this._http.get(get_url).map(this.extractData);
    }
     public getDepartment(){
        let get_url = this.action_url + this.route_link + '/' + 'department';

        return this._http.get(get_url).map(this.extractData);
    }
    public getCompany(){
        let get_url = this.action_url + this.route_link + '/' + 'company';

        return this._http.get(get_url).map(this.extractData);
    }

    public getAttendanceType(){
        let get_url = this.action_url + this.route_link + '/' + 'attendance_type';

        return this._http.get(get_url).map(this.extractData);
    }

    public updateStatus(id_status: any ,status: any, url:any){

        let put_url = this.action_url + url + '/' + id_status;

        return this._http.put(put_url , status).map(this.extractData);
    }

    public updateRequest(model:any){

        let post_url = this.action_url + this.route_link + '/update/request';
        return this._http.post(post_url, JSON.stringify(model)).map(this.extractData);
    }

    public archiveData(url,id){

        let urls = this.action_url + url + '/' + id;
        return this._http.delete(urls).map(this.extractData);
    }

    public getTypeData(id: any,url:any){

        let get_url = this.action_url + url + '/' + id;
        return this._http.get(get_url).map(this.extractData);
    }

    public createTypeData(model:any, url:any){

        let post_url = this.action_url + url ;

        return this._http.post(post_url, JSON.stringify(model)).map(this.extractData);
    }

    public updateTypeData(id: any ,model: any, url:any){

        let put_url = this.action_url + url + '/' + id;

        return this._http.put(put_url , model).map(this.extractData);
    }

    public getEventType(){
        let get_url = this.action_url + this.route_link + '/' + 'event_type';

        return this._http.get(get_url).map(this.extractData);
    }

    public remarksModal(id: any,url:any,model:any){

        let urls = this.action_url + url + '/' + id;

        return this._http.put(urls , model).map(this.extractData);
    }

    public getHoliType(){
        let get_url = this.action_url + this.route_link + '/' + 'holi_type';

        return this._http.get(get_url).map(this.extractData);
    }

    public resolutionModal(id:any, url:any){
        let get_url = this.action_url + url + '/' + id;
        return this._http.get(get_url).map(this.extractData);
    }

    public getAllRequest(model: any){

        let supervisor_id = model.supervisor_id;
        let start = model.start_date;
        let end = model.end_date;
        let status_id = model.status_id;
        let company_id = model.company_id;
        let employee_id = model.employee_id;
        let type_id = model.type_id;
        let role_id = model.role_id;

        let uri_data ='start_date=' + start + '&end_date=' + end+ '&status_id=' + status_id+ '&company_id=' + company_id+ '&employee_id=' + employee_id+ '&type_id=' + type_id + '&supervisor_id=' + supervisor_id + '&role_id=' + role_id ;

        let get_url = this.action_url + this.route_link + '/' + 'all_request?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public getRequest(id:any, url:any){
        let get_url = this.action_url + url + '/' + id;
        return this._http.get(get_url).map(this.extractData);
    }

    public getMemoType(){

        let action_url = this.action_url + 'type/memo';

        return this._http.get(action_url).map(this.extractData);

    }

    public getMySchedule(id:any){

        let get_url = this.action_url + 'my_schedule/' + id;

        return this._http.get(get_url).map(this.extractData);
    }

    public getFilterEmployee(company_id:any){
         let get_url = this.action_url + this.route_link + '/' + 'filter_employee?' + '&company_id=' + company_id;
         return this._http.get(get_url).map(this.extractData);
    }

    public getBranch(){
        let get_url = this.action_url + this.route_link + '/' + 'branch';

        return this._http.get(get_url).map(this.extractData);
    }

    public getBranchByCompany(id){
        let get_url = this.action_url + this.route_link + '/' + 'branch_by_company/'+ id;

        return this._http.get(get_url).map(this.extractData);
    }

    public getBioDevice(){
        let get_url = this.action_url + this.route_link + '/' + 'bio_device';

        return this._http.get(get_url).map(this.extractData);
    }

    public getPosition(){
        let get_url = this.action_url + this.route_link + '/' + 'position';

        return this._http.get(get_url).map(this.extractData);
    }

    public getWorkingSchedule(id){
        let get_url = this.action_url + this.route_link + '/' + 'working_sched/'+id;

        return this._http.get(get_url).map(this.extractData);
    }

    public getSuggestedWorkingSchedule(){
        let get_url = this.action_url + this.route_link + '/suggested_working_sched';

        return this._http.get(get_url).map(this.extractData);
    }

    public getWorkingScheduleWithoutBreak(){
        let get_url = this.action_url + this.route_link + '/' + 'working_sched_without_break';

        return this._http.get(get_url).map(this.extractData);
    }

    public getOnboarding(){
        let get_url = this.action_url + this.route_link + '/' + 'onboarding';

        return this._http.get(get_url).map(this.extractData);
    }

    public getClearance(){
        let get_url = this.action_url + this.route_link + '/' + 'clearance';

        return this._http.get(get_url).map(this.extractData);
    }

    public checkDateHoliday(date: any){


        let uri_data ='date=' + date;

        let get_url = this.action_url + this.route_link + '/' + 'check_holiday?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public getDates(startDate: any, endDate: any){
        // Gets dates between from two different dates. Returns Array
        let dates = [],
        currentDate = startDate,
        addDays = function(days) {
            let date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        };
        while (currentDate <= endDate) {
            dates.push(currentDate);
            currentDate = addDays.call(currentDate, 1);
        }
        return dates;
    };

    public getDay(index_day: any = 0){
        return this.days[index_day];
    }

    public specialArchiveData(url,id){

        let model ={
            id:id
        }

        let post_url = this.action_url + url + '/archive';
        return this._http.post(post_url, model).map(this.extractData);
    }
    /*
    this.option_time_hr_24 = Array(12).fill(1).map((x,i)=>i);
    this.option_time_mins = Array(59).fill(1).map((x,i)=>i);
    */
    public getTimeHours(){
      return this.option_time_hr_24;
    }

    public getTimeMins(){
      return this.option_time_mins;
    }

}
