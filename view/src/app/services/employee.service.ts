import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Configuration } from '../app.config';
import { Employee } from '../model/employee';
//import { Configuration } from '../app.config';

@Injectable()
export class EmployeeService {
	private action_url: string;
    private route_link: string;
    private route_upload: string;
    private route_attachment = 'employee_attachment';
    private route_export : string;
    private route_mass_upload_form: string;

	private headers: Headers;

    // Static Selection Options
    private name_suffixes = [
        { name: '-none-' },
        { name: 'Jr.' },
        { name: 'Sr.' },
        { name: 'III' },
        { name: 'IV' }
    ];

    private genders = [
        { name: 'Male' },
        { name: 'Female' }
    ];

    private blood_types = [
        { name: 'A' },
        { name: 'AB' },
        { name: 'O'}
    ];

    private employee_marital_statuses = [
        { name: 'single' },
        { name: 'married' }
        // additional marital statuses: http://birtaxcalculator.com/calculator.php
    ];

    private employee_contact_statuses = [
        { code: 'contractual', name: 'Contractual' },
        { code: 'full-time', name: 'Full-time' },
        { code: 'part-time', name: 'Part-time' },
        { code: 'provisionary', name: 'Provisionary' },
        { code: 'regular', name: 'Regular' },
        { code: 'resigned', name: 'Resigned' },
        { code: 'awol', name: 'AWOL' }
    ];

    private employee_types = [
        { code: 'manager', name: 'Manager' },
        { code: 'officer', name: 'Officer' },
        { code: 'rank_and_file', name: 'Rank & File' }
    ];

    private employee_income_type = [
        { code: 'daily', name: 'Daily' },
        { code: 'weekly', name: 'Weekly'},
        { code: 'monthly', name: 'Monthly' }        
    ];

    private employee_residency_type = [
        { name: '' },
        { name: 'Rented' },
        { name: 'Owned' },
        { name: 'Others' }
    ];

    private employee_departments = [];

    private employee_positions = [];

    private employee_genders = [
        { name: '' },
        { name: 'Female' },
        { name: 'Male' }
    ];

    private employee_skills = [
        { name: 'MS Office' },
        { name: 'Photoshop' },
        { name: 'PC Troubleshooting' },
        { name: 'Customer Caring' }
    ];

    private employee_dependency_relationship = [
        { value: 'others', label: 'Others' },
        { value: 'father', label: 'Father' },
        { value: 'mother', label: 'Mother' },
        { value: 'sibling', label: 'Sibling' },
        { value: 'grandparent', label: 'Grandparents' },
        { value: 'familyrelative', label: 'Family Relative' }
    ];

    private banks = [
        { 'name': '', 'code': '' },
        { 'name': 'ALLIED BANK', 'code': '10320013' },
        { 'name': 'ASIA UNITED BANK', 'code': '11020011' },
        { 'name': 'ANZ BANK', 'code': '10700015' },
        { 'name': 'BANCO DE ORO', 'code': '10530667' },
        { 'name': 'BANGKO SENTRAL NG PILIPINAS', 'code': '10030015' },
        { 'name': 'BANGKOK BANK', 'code': '10670019' },
        { 'name': 'BANK OF AMERICA', 'code': '10120019' },
        { 'name': 'BANK OF CHINA', 'code': '11140014' },
        { 'name': 'COMBANK', 'code': '10440016' },
        { 'name': 'BPI', 'code': '10040018' },
        { 'name': 'BANK OF TOKYO', 'code': '10460012' },
        { 'name': 'CHINA BANK', 'code': '10100013' },
        { 'name': 'CHINA TRUST', 'code': '10690015' },
        { 'name': 'CITIBANK', 'code': '10070017' },
        { 'name': 'DEUTSCHE', 'code': '10650013' },
        { 'name': 'DBP', 'code': '10590018' },
        { 'name': 'EASTWEST BANK', 'code': '10620014' },
        { 'name': 'EXPORT', 'code': '10860010' },
        { 'name': 'HONGKONG BANK', 'code': '10060014' },
        { 'name': 'JP MORGAN', 'code': '10720011' },
        { 'name': 'KOREA EXCH BANK', 'code': '10710018' },
        { 'name': 'LAND BANK', 'code': '10350025' },
        { 'name': 'MAYBANK', 'code': '10220016' },
        { 'name': 'MEGA INTL COMML BANK OF CHINA', 'code': '10560019' },
        { 'name': 'METROBANK', 'code': '10269996' },
        { 'name': 'MIZUHO CORPORATE BANK', 'code': '10640010' },
        { 'name': 'PB COM', 'code': '10110016' },
        { 'name': 'PNB', 'code': '10080010' },
        { 'name': 'PHILTRUST', 'code': '10090039' },
        { 'name': 'VETERANS BANK', 'code': '10330016' },
        { 'name': 'RCBC', 'code': '10280014' },
        { 'name': 'ROYAL BANK OF SCOTLAND (PHILS)', 'code': '10770074' },
        { 'name': 'SECURITY BANK', 'code': '10140015' },
        { 'name': 'CHARTERED BANK', 'code': '10050011' },
        { 'name': 'UNION BANK', 'code': '10419995' },
        { 'name': 'COCOBANK', 'code': '10299995' },
        { 'name': 'UNITED OVERSEAS BANK', 'code': '10270341' }
    ];

	constructor(private _http: Http, private _conf: Configuration){
		this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'employee';
        this.route_upload = 'employee_upload';
        this.route_export = 'employee_export';
        this.route_mass_upload_form = 'employee_download_upload_form';

        
        // Header Variables
		this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', `Bearer ${authToken}`);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        //console.log(authToken);
	}

    public getBanks(){
        return this.banks;
    }

    public getGenders(){
        return this.employee_genders;
    }

    public getSuffixes(){
        return this.name_suffixes;
    }

    public getBloodTypes(){
        return this.blood_types;
    }

    public getEmploymentStatus(){
        return this.employee_contact_statuses;
    }

    public getEmployeeSkills(){
        return this.employee_skills;
    }

    public getEmployeeMaritalStatuses(){
        return this.employee_marital_statuses;
    }

    public getIncomeType(){
        return this.employee_income_type;
    }

    public getResidencyType(){
        return this.employee_residency_type;
    }

    public getEmployeeDependencyRelationships(){
        return this.employee_dependency_relationship;
    }

    public getDepartments(){ // Dynamic of Departments soon.
        let department_url = this.action_url + 'department';

        return this._http.get(department_url, { headers:this.headers }).map(this.extractData);
    }

    public getJobTitles(){ // Dynamic of Positions soon.
        let positions_url = this.action_url + 'position';

        return this._http.get(positions_url, { headers:this.headers }).map(this.extractData);
    }

	public getEmployees(uri_data?: any){
		let get_url = this.action_url + this.route_link + '?' + uri_data;

		return this._http.get(get_url, { headers:this.headers }).map(this.extractData);
    }

    public getEmployee(employee_id: any){
        let get_url = this.action_url + this.route_link + '/' + employee_id;

        return this._http.get(get_url, { headers:this.headers }).map(this.extractData);
    }

    public getCompanies(){
        let route_company = 'company';
        let get_url = this.action_url + route_company;

        return this._http.get(get_url, { headers:this.headers }).map(this.extractData);
    }

    public storeEmployee(employee_model: any){
        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(employee_model), { headers:this.headers }).map(this.extractData);
    }

    public uploadMassEmployeeCsv(data: any){
        let post_url = this.action_url + this.route_upload;

        return this._http.post(post_url, JSON.stringify(data), {headers: this.headers}).map(this.extractData);
    }

    public addEmployeeRemark(employee_remarks_model: any){
        let post_url = this.action_url + 'employee_remark';

        return this._http.post(post_url, JSON.stringify(employee_remarks_model), { headers:this.headers }).map(this.extractData);
    }

    public getAttachments(employee_id: any){
        let get_attachment_url = this.action_url + this.route_attachment + '?employee_id=' + employee_id;

        return this._http.get(get_attachment_url, {headers:this.headers}).map(this.extractData);
    }
    public attachEmployeeFiles(employee_id: any, employee_attachment: any, employee_attachment_dir: any){
        let route_upload_attachment = this.route_attachment;
        let post_url = this.action_url + route_upload_attachment;

        let employee_pass_data = {
            employee_id: employee_id,

            file_url: employee_attachment,
            file_dir: employee_attachment_dir

        };

        return this._http.post(post_url, JSON.stringify(employee_pass_data), { headers:this.headers }).map(this.extractData);
    }

    public updateEmployee(id: any, employee_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, employee_model, { headers:this.headers }).map(this.extractData);
    }

    public updateScheduleMaster(id: any, schedule_master_model: any){
        let put_url = this.action_url + 'employee_schedule_master' + '/' + id;

        return this._http.put(put_url, schedule_master_model, { headers:this.headers }).map(this.extractData);
    }

    public exportEmployeeList(employee_form: any){
        let url = this.action_url + this.route_export;
        let params = new URLSearchParams();
        for(let key in employee_form){
            params.set(key, employee_form[key]) 
        }

        window.location.href = url + '?' + params.toString();
        // return this._http.post(url , employee_form, { headers:this.headers }).map(this.extractData);
    }


    public exportEmployeeMassUploadform(company: any, company_branch: any, selected_fields: any){
        let selected_company = company;
        let selected_company_branch = company_branch;
        let selected_fields_for_upload = encodeURIComponent(JSON.stringify(selected_fields));
        let url_export = this.action_url + this.route_mass_upload_form + '?company_id=' + selected_company + '&branch_id=' + selected_company_branch + '&fields=' + selected_fields;

        window.location.href = url_export;
    }

    public getMasterSchedules(){
        let get_url = this.action_url + 'employee_schedule_master';

        return this._http.get(get_url, { headers:this.headers }).map(this.extractData);
    }

    public getMasterSchedule(id: any){
        let get_url = this.action_url + 'employee_schedule_master/' + id;

        return this._http.get(get_url, { headers:this.headers }).map(this.extractData);
    }

    public updateCompanyMasterSchedules(employee_master_schedule: any){
        let post_url = this.action_url + 'employee_schedule_master';

        return this._http.post(post_url, JSON.stringify(employee_master_schedule), { headers:this.headers }).map(this.extractData);
    }

    private extractData(res: Response){
    	let body = res.json();
    	return body.data || { };
    }

    private handleError(error: Response | any){
    	// In a real world app, we might use a remote logging infrastructure
    	let errMsg: string;
    	if (error instanceof Response) {
    		const body = error.json() || '';
    		const err = body.error || JSON.stringify(body);
    		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    	} else {
    		errMsg = error.message ? error.message : error.toString();
    	}

    	return Observable.throw(errMsg);
    }

    public archiveEmployee(employee_id: any){

        console.log('postarchive', employee_id);
        let delete_url = this.action_url + this.route_link + '/' + employee_id;
        console.log(delete_url);
        console.log(employee_id);

        return this._http.delete(delete_url, { headers:this.headers } ).map(this.extractData);
    }

    public deleteEmployeAttachment(attachment_id: any){
        // Delete Employee File Attached
        let delete_url = this.action_url + this.route_attachment + '/' + attachment_id;

        return this._http.delete(delete_url).map(this.extractData);
    }

}