var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var AttendanceService = /** @class */ (function () {
    function AttendanceService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.data = [];
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'attendance';
    }
    AttendanceService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    AttendanceService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    AttendanceService.prototype.getEmployeeData = function (model) {
        var post_url = this.action_url + this.route_link + '/search_employee';
        return this._http.post(post_url, model).map(this.extractData);
    };
    AttendanceService.prototype.storeAttendance = function (attendance_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(attendance_model)).map(this.extractData);
    };
    AttendanceService.prototype.createTimeAttendance = function (att_model) {
        var post_url = this.action_url + this.route_link + '/' + 'create_time';
        return this._http.post(post_url, JSON.stringify(att_model)).map(this.extractData);
    };
    AttendanceService.prototype.createDayAttendanceDefault = function (att_model) {
        var post_url = this.action_url + this.route_link + '/' + 'create_day_default';
        return this._http.post(post_url, JSON.stringify(att_model)).map(this.extractData);
    };
    AttendanceService.prototype.createDayAttendance = function (att_model) {
        var post_url = this.action_url + this.route_link + '/' + 'create_day';
        return this._http.post(post_url, JSON.stringify(att_model)).map(this.extractData);
    };
    AttendanceService.prototype.getAttendanceDefault = function (id) {
        var get_url = this.action_url + this.route_link + '/' + id;
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService.prototype.getAttendance = function (id) {
        var get_url = this.action_url + this.route_link + '/not/default/' + id;
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService.prototype.archiveAttendance = function (employees_id) {
        var delete_url = this.action_url + this.route_link + '/' + employees_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    AttendanceService.prototype.updateAttendance = function (id, model) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, model).map(this.extractData);
    };
    AttendanceService.prototype.updateAttendanceNotDefault = function (id, model) {
        var put_url = this.action_url + this.route_link + '/not/default/update/' + id;
        return this._http.put(put_url, model).map(this.extractData);
    };
    AttendanceService.prototype.attachAttendanceFiles = function (file) {
        var post_url = this.action_url + this.route_link + '/' + 'import_file';
        return this._http.post(post_url, JSON.stringify(file)).map(this.extractData);
    };
    AttendanceService.prototype.getTotalPresent = function () {
        var get_url = this.action_url + 'present' + '/' + 'attendancereport';
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService.prototype.getTotalEmp = function (filter) {
        var get_url = this.action_url + 'totalemp' + '/' + 'attendancereport' + '/' + filter;
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService.prototype.selectedDate = function () {
        var get_url = this.action_url + 'attendance' + '/' + 'date';
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService.prototype.attendanceReport = function (employee, company, department, start_date, end_date) {
        var uri_data = 'company_id=' + company + '&department_id=' + department + '&employee_id=' + employee + '&start_date=' + start_date + '&end_date=' + end_date;
        console.log(uri_data);
        var get_url = this.action_url + 'attendancereport' + '/' + 'graph?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService.prototype.getAttendDefault = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var type_id = model.type_id;
        var company_id = model.company_id;
        var employee_id = model.employee_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&type_id=' + type_id + '&company_id=' + company_id + '&employee_id=' + employee_id;
        var get_url = this.action_url + this.route_link + '/' + 'for/default/data?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService.prototype.getAttend = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var type_id = model.type_id;
        var company_id = model.company_id;
        var employee_id = model.employee_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&type_id=' + type_id + '&company_id=' + company_id + '&employee_id=' + employee_id;
        var get_url = this.action_url + this.route_link + '/' + 'filter' + '/' + 'data?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService.prototype.getMyAttendance = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var type_id = model.type_id;
        var employee_id = model.employee_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&type_id=' + type_id + '&employee_id=' + employee_id;
        var get_url = this.action_url + this.route_link + '/' + 'my_attendance' + '/' + 'data?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService.prototype.getSchedule = function (model) {
        var post_url = this.action_url + this.route_link + '/' + 'data/schedule';
        return this._http.post(post_url, model).map(this.extractData);
    };
    AttendanceService.prototype.convertFile = function (file) {
        var file_url = file.file_url;
        var url_data = '&file_url=' + file_url;
        var post_url = this.action_url + this.route_link + '/' + 'convert_file?' + url_data;
        return this._http.post(post_url, JSON.stringify(file)).map(this.extractData);
    };
    AttendanceService.prototype.getMyLogs = function () {
        var get_url = this.action_url + this.route_link + '/' + 'my/logs';
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService.prototype.createAttendanceFromWeb = function (att_model) {
        var post_url = this.action_url + this.route_link + '/' + 'create_from_web';
        return this._http.post(post_url, JSON.stringify(att_model)).map(this.extractData);
    };
    AttendanceService.prototype.attchAttendExcel = function (file) {
        var file_url = file.file_url;
        var url_data = '&file_url=' + file_url;
        var post_url = this.action_url + this.route_link + '/' + 'import_excel_1?' + url_data;
        return this._http.post(post_url, JSON.stringify(file)).map(this.extractData);
    };
    AttendanceService.prototype.attchAttendExcel2 = function (file) {
        var file_url = file.file_url;
        var url_data = '&file_url=' + file_url;
        var post_url = this.action_url + this.route_link + '/' + 'import_excel_2?' + url_data;
        return this._http.post(post_url, JSON.stringify(file)).map(this.extractData);
    };
    AttendanceService.prototype.attchAttendExcel3 = function (file) {
        var file_url = file.file_url;
        var url_data = '&file_url=' + file_url;
        var post_url = this.action_url + this.route_link + '/' + 'import_excel_3?' + url_data;
        return this._http.post(post_url, JSON.stringify(file)).map(this.extractData);
    };
    AttendanceService.prototype.getDataFromBiometrics = function (device) {
        var uri_data = 'ip_address=' + device;
        var get_url = this.action_url + this.route_link + '/' + 'import' + '/' + 'bio?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService.prototype.setData = function (data) {
        this.data = data;
    };
    AttendanceService.prototype.getData = function () {
        return this.data;
    };
    AttendanceService.prototype.logsValidation = function () {
        var get_url = this.action_url + this.route_link + '/' + 'logs/validation';
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService.prototype.getEmployeeRate = function (id) {
        var post_url = this.action_url + this.route_link + '/' + 'employee_rate';
        return this._http.post(post_url, JSON.stringify(id)).map(this.extractData);
    };
    AttendanceService.prototype.ignoreEmpInDayAttendance = function (att_model) {
        var post_url = this.action_url + this.route_link + '/' + 'ignore_emp/day_attendance';
        return this._http.post(post_url, JSON.stringify(att_model)).map(this.extractData);
    };
    AttendanceService.prototype.getUploadHistoryFromAttendance = function () {
        var get_url = this.action_url + 'attendance_upload_history';
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService.prototype.deleteAttendanceByBatch = function (id) {
        var delete_url = this.action_url + 'attendance_upload_history' + '/' + id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    AttendanceService.prototype.getWorkingSchedulePerCompany = function (company_id) {
        var get_url = this.action_url + 'attendance/working_schedule/company?company_id=' + company_id;
        return this._http.get(get_url).map(this.extractData);
    };
    AttendanceService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], AttendanceService);
    return AttendanceService;
}());
export { AttendanceService };
//# sourceMappingURL=attendance.service.js.map