import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Payroll } from '../model/payroll';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class PayrollService {
    private action_url: string;
    private route_payroll: string;
    private route_payroll_list: string;
    private route_payslip: string;
    private route_payroll_billing: string;
    private route_soa: string;
    private route_soa_loc: string;
    private route_payroll_allowance: string;
    private route_payroll_get_employee: string;
    public id:any;
    public reload: Boolean = false;

	constructor(private _http: HttpClient, private http: HttpClient, private _conf: Configuration){
		this.action_url = this._conf.ServerWithApiUrl;
        this.route_payroll = 'payroll';
        this.route_payroll_list = 'payroll_list';
        this.route_payslip = 'payslip';
        this.route_payroll_allowance = 'payroll_allowance';
        this.route_payroll_billing = 'payroll_billing';
        this.route_soa = 'statement_of_account';
        this.route_soa_loc = 'statement_of_account_loc';
        this.route_payroll_get_employee = 'payroll_get_employee';
    }

	public getPayrolls(){
		let get_url = this.action_url + this.route_payroll;
		return this._http.get(get_url).map(this.extractData);
    }

    public getPayrollList(start, end, run_type, company, branch, department, position, cutoff, removed_employee){
        let get_url = this.action_url + this.route_payroll_list + 
        '?start=' + start + '&end=' + end + '&run_type=' + run_type + '&company=' 
        + company + '&branch=' + branch + '&department=' + department + '&position=' + position + '&cutoff=' + cutoff + '&removed_employee=' + removed_employee;
        return this._http.get(get_url).map(this.extractData);
    }

    public getPayrollEmployee(start, end, run_type, company, branch, department, position, cutoff, removed_employee){
        let get_url = this.action_url + this.route_payroll_get_employee + 
        '?start=' + start + '&end=' + end + '&run_type=' + run_type + '&company=' 
        + company + '&branch=' + branch + '&department=' + department + '&position=' + position + '&cutoff=' + cutoff + '&removed_employee=' + removed_employee;
        return this._http.get(get_url).map(this.extractData);
    }

    public getPayrollBilling(start, end, run_type, company, branch, department, position, cutoff, removed_employee, branch_location){
        let get_url = this.action_url + this.route_payroll_billing + 
        '?start=' + start + '&end=' + end + '&run_type=' + run_type + '&company=' 
        + company + '&branch=' + branch + '&department=' + department + 
        '&position=' + position + '&cutoff=' + cutoff + '&removed_employee=' + removed_employee + '&branch_location=' + branch_location;
        
        return this._http.get(get_url).map(this.extractData);
    }

    public generatePayslip(id, company, branch, start, end){
        let get_url = this.action_url + this.route_payslip + 
        '?id=' + id + '&company=' + company + '&branch=' + branch + '&start=' + start   + '&end=' + end;
        return this._http.get(get_url);
    }

    public sendPayslip( id ) {
        let get_url = this.action_url + this.route_payslip + '/send?id=' + id;
        return this._http.get(get_url);
    }

    public generateSOA(payroll_id){
        let get_url = this.action_url + this.route_soa + '?payroll_id=' + payroll_id;

        return this._http.get(get_url);
    }

    public generateSOAByLocation( total_billable, admin_fee, vat, total_mandatory, total_admin, company, start, end, prepared_by){
        let get_url = this.action_url + this.route_soa_loc + 
        '?total_billable=' + total_billable + 
        '&admin_fee=' + admin_fee + 
        '&vat=' + vat + 
        '&total_mandated=' + total_mandatory + 
        '&total_admin_fee=' + total_admin + 
        '&company=' + company + 
        '&start=' + start + 
        '&end=' + end + 
        '&prepared_by=' + prepared_by 
        return this._http.get(get_url);
    }

    public getPayrollArchieve(payroll_id: any) {
        let get_url = this.action_url + this.route_payroll_list + '/' + payroll_id;
        return this._http.get(get_url).map(this.extractData);
    }

    public updateAllowance(id: any, amount: any) {
        let put_url = this.action_url + this.route_payroll_allowance + '/' + id;
        return this._http.put(put_url, amount).map(this.extractData);
    }

    public setId(payroll_id: any) {
        this.id = payroll_id;
    }

    public getId() {
        return this.id;
    }  

    public getPayroll(payroll_id: any){
        let get_url = this.action_url + this.route_payroll + '/' + payroll_id;
        return this._http.get(get_url).map(this.extractData);
    }

    public storePayroll(payroll_model: any){
        let post_url = this.action_url + this.route_payroll;
        return this._http.post(post_url, JSON.stringify(payroll_model)).map(this.extractData);
    }

    public storePayrollList(payroll_model: any){
        let post_url = this.action_url + this.route_payroll_list;
        return this._http.post(post_url, JSON.stringify(payroll_model)).map(this.extractData);
    }

    public updatePayroll(id: any, payroll: any){
        let put_url = this.action_url + this.route_payroll + '/' + id;
        return this._http.put(put_url, payroll).map(this.extractData);
    }

    private extractData(res: Response) {
    	let body = res.json();
    	return body.data || { };
    }

    private handleError (error: Response | any){
    	let errMsg: string;
    	if (error instanceof Response) {
    		const body = error.json() || '';
    		const err = body.error || JSON.stringify(body);
    		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    	} else {
    		errMsg = error.message ? error.message : error.toString();
    	}
    	return Observable.throw(errMsg);
    }

    public archivePayroll(payroll_id: any){
        let delete_url = this.action_url + this.route_payroll + '/' + payroll_id;
        return this._http.delete(delete_url).map(this.extractData);
    }

    public archivePayrollList(payroll_id: any){
        let delete_url = this.action_url + this.route_payroll_list + '/' + payroll_id;
        return this._http.delete(delete_url).map(this.extractData);
    }

    public generatePayrollslip(id){
        let get_url = this.action_url + this.route_payslip +  '?id=' + id ;
        return this._http.get(get_url);
    }
}