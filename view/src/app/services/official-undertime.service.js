var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var OfficialUndertimeService = /** @class */ (function () {
    function OfficialUndertimeService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'official_undertime';
    }
    OfficialUndertimeService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    OfficialUndertimeService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    OfficialUndertimeService.prototype.createOU = function (OU_model) {
        var post_url = this.action_url + this.route_link + '/' + 'create';
        return this._http.post(post_url, JSON.stringify(OU_model)).map(this.extractData);
    };
    OfficialUndertimeService.prototype.createMyOU = function (OU_model) {
        var post_url = this.action_url + this.route_link + '/' + 'create/my_ou';
        return this._http.post(post_url, JSON.stringify(OU_model)).map(this.extractData);
    };
    OfficialUndertimeService.prototype.getOU = function (id) {
        var get_url = this.action_url + this.route_link + '/' + id;
        return this._http.get(get_url).map(this.extractData);
    };
    // public updateOU(id_status: any ,update_model: any){
    //     let post_url = this.action_url + this.route_link + '/' + id_status;
    //     return this._http.put(post_url , update_model).map(this.extractData);
    // }
    OfficialUndertimeService.prototype.getMyOU = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var status_id = model.status_id;
        var employee_id = model.employee_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&status_id=' + status_id + '&employee_id=' + employee_id;
        var get_url = this.action_url + this.route_link + '/' + 'my_ou' + '/' + 'data?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    OfficialUndertimeService.prototype.getOUList = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var status_id = model.status_id;
        var company_id = model.company_id;
        var employee_id = model.employee_id;
        var supervisor_id = model.supervisor_id;
        var role_id = model.role_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&status_id=' + status_id + '&company_id=' + company_id + '&employee_id=' + employee_id + '&supervisor_id=' + supervisor_id + '&role_id=' + role_id;
        var get_url = this.action_url + this.route_link + '/' + 'filter' + '/' + 'data?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    OfficialUndertimeService.prototype.updateOU = function (ouUpdate) {
        var post_url = this.action_url + this.route_link + '/update/ou';
        return this._http.post(post_url, JSON.stringify(ouUpdate)).map(this.extractData);
    };
    OfficialUndertimeService.prototype.updateMyOU = function (ouUpdate) {
        var post_url = this.action_url + this.route_link + '/update/my_ou';
        return this._http.post(post_url, JSON.stringify(ouUpdate)).map(this.extractData);
    };
    OfficialUndertimeService.prototype.duplicateEntryValidation = function (undertime) {
        var post_url = this.action_url + this.route_link + '/duplicate_entry_validation';
        return this._http.post(post_url, JSON.stringify(undertime)).map(this.extractData);
    };
    OfficialUndertimeService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], OfficialUndertimeService);
    return OfficialUndertimeService;
}());
export { OfficialUndertimeService };
//# sourceMappingURL=official-undertime.service.js.map