var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var TaskService = /** @class */ (function () {
    function TaskService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'task';
    }
    TaskService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    TaskService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    TaskService.prototype.getTask = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var employee_id = model.employee_id;
        var role_id = model.role_id;
        var supervisor_id = model.supervisor_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&employee_id=' + employee_id + '&role_id=' + role_id + '&supervisor_id=' + supervisor_id;
        var get_url = this.action_url + this.route_link + '/filter/data?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    TaskService.prototype.duplicateEntryValidation = function (task) {
        var post_url = this.action_url + this.route_link + '/duplicate_entry_validation';
        return this._http.post(post_url, JSON.stringify(task)).map(this.extractData);
    };
    TaskService.prototype.createTask = function (model) {
        var post_url = this.action_url + this.route_link + '/' + 'create';
        return this._http.post(post_url, JSON.stringify(model)).map(this.extractData);
    };
    TaskService.prototype.getTaskById = function (id) {
        var get_url = this.action_url + this.route_link + '/' + id;
        return this._http.get(get_url).map(this.extractData);
    };
    TaskService.prototype.updateTask = function (data) {
        var post_url = this.action_url + this.route_link + '/update/data';
        return this._http.post(post_url, JSON.stringify(data)).map(this.extractData);
    };
    TaskService.prototype.getMyTask = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var employee_id = model.employee_id;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&employee_id=' + employee_id;
        var get_url = this.action_url + this.route_link + '/' + 'my_task' + '/' + 'data?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    TaskService.prototype.updateTaskTime = function (data) {
        var post_url = this.action_url + this.route_link + '/update/task/time';
        return this._http.post(post_url, JSON.stringify(data)).map(this.extractData);
    };
    TaskService.prototype.updateStatus = function (data) {
        var post_url = this.action_url + this.route_link + '/update/task/status';
        return this._http.post(post_url, JSON.stringify(data)).map(this.extractData);
    };
    TaskService.prototype.getMyTaskForDashboard = function () {
        var get_url = this.action_url + this.route_link + '/' + 'my_task' + '/' + 'dashboard';
        return this._http.get(get_url).map(this.extractData);
    };
    TaskService.prototype.approvedTask = function (data) {
        var post_url = this.action_url + this.route_link + '/approved/status';
        return this._http.post(post_url, JSON.stringify(data)).map(this.extractData);
    };
    TaskService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], TaskService);
    return TaskService;
}());
export { TaskService };
//# sourceMappingURL=task.service.js.map