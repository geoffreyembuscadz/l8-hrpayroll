var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from './http-extended.service';
import { Configuration } from '../app.config';
var AdjustmentTypeService = /** @class */ (function () {
    function AdjustmentTypeService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'adjustment_type';
    }
    AdjustmentTypeService.prototype.getAdjustmentsType = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    AdjustmentTypeService.prototype.getAdjustmentType = function (adjustment_id) {
        var get_url = this.action_url + this.route_link + '/' + adjustment_id;
        return this._http.get(get_url).map(this.extractData);
    };
    AdjustmentTypeService.prototype.setId = function (adjustment_id) {
        this.id = adjustment_id;
    };
    AdjustmentTypeService.prototype.getId = function () {
        return this.id;
    };
    AdjustmentTypeService.prototype.updateAdjustmentType = function (id, adjustment_id) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, adjustment_id).map(this.extractData);
    };
    AdjustmentTypeService.prototype.archiveAdjustmentType = function (adjustment_id) {
        var delete_url = this.action_url + this.route_link + '/' + adjustment_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    AdjustmentTypeService.prototype.storeAdjustmentType = function (adjustment_id) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(adjustment_id)).map(this.extractData);
    };
    AdjustmentTypeService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    AdjustmentTypeService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    AdjustmentTypeService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], AdjustmentTypeService);
    return AdjustmentTypeService;
}());
export { AdjustmentTypeService };
//# sourceMappingURL=adjustment-type.service.js.map