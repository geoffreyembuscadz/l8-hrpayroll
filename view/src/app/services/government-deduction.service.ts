import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';

@Injectable()
export class GovernmentDeductionService {

   private action_url: string;
   private route_link: string;
   private route_link_emp_share: string;

    private company_id: string;
    private headers: Headers;
    public id:any;


    constructor(private _http: Http, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'government_deduction';
        this.route_link_emp_share = 'government-deduction';

        this.headers = new Headers();
        let authToken = localStorage.getItem('id_token');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', `Bearer ${authToken}`);
    }

    public getEmployeeShare() {
          let get_url = this.action_url + this.route_link_emp_share;

         return this._http.get(get_url, { headers:this.headers }).map(this.extractData);       
    }
     public getEE() {
          let get_url = this.action_url + this.route_link_emp_share;

         return this._http.get(get_url, { headers:this.headers }).map(this.extractData);       
    }

    public computeDeduction(basicPay: number){
        let get_url = this.action_url + this.route_link + '/' + basicPay;
        return this._http.get(get_url, { headers:this.headers }).map(this.extractData);
    }


    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
  }


}