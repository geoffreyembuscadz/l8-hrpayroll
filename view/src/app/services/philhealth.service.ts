import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from './http-extended.service';
import { Configuration } from '../app.config';


@Injectable()
export class PhilhealthService {

    private actionUrl: string;
    private headers: Headers;

    private action_url: string;
    private route_link: string;

    constructor(private _http: HttpClient,
        private _conf: Configuration){
       this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'department';

    }

    public getPhilhealth(){
        let philhealth_route = 'philhealth';
        let action_url = this.actionUrl + philhealth_route;

        return this._http.get(action_url).map(this.extractData);
        // let postData = JSON.stringify(headersData);
        // return this._http.get(this.actionUrl + employee_route).map((response: Response) => <Employee[]>response.json());
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

}

