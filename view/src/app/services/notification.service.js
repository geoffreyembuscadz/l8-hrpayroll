var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
// import * as io from "socket.io-client";
var NotificationService = /** @class */ (function () {
    function NotificationService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'notification';
    }
    NotificationService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    NotificationService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    NotificationService.prototype.getNotification = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    NotificationService.prototype.updateNotif = function (model) {
        var post_url = this.action_url + this.route_link + '/' + 'update';
        return this._http.post(post_url, JSON.stringify(model)).map(this.extractData);
    };
    NotificationService.prototype.getNotif = function (data, user_id) {
        var uri_data = 'id=' + data + '&user_id=' + user_id;
        var get_url = this.action_url + this.route_link + '/' + 'notif' + '/' + 'data?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    NotificationService.prototype.getAllNotification = function (model) {
        var start_date = model.start_date;
        var end_date = model.end_date;
        var uri_data = 'start_date=' + start_date + '&end_date=' + end_date;
        var get_url = this.action_url + this.route_link + '/all/notif?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    NotificationService.prototype.markAllAsRead = function () {
        var id = 0;
        var post_url = this.action_url + this.route_link + '/' + 'markall';
        return this._http.post(post_url, JSON.stringify(id)).map(this.extractData);
    };
    NotificationService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], NotificationService);
    return NotificationService;
}());
export { NotificationService };
//# sourceMappingURL=notification.service.js.map