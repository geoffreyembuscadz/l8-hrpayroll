import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Event } from '../model/event';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class EventService {
    private action_url: string;
    private route_link: string;

    private event_id: string;
    private headers: Headers;
    public id:any;


    constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'event';

    }


    public getEvents(){
        let get_url = this.action_url + this.route_link;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public getEventType(){
        let get_url = this.action_url  + 'event_type';
        return this._http.get(get_url, ).map(this.extractData);
    }

    public getEventStat(){
        let get_url = this.action_url  + this.route_link + '/' + 'event_status';
        return this._http.get(get_url, ).map(this.extractData);
    }


    public setId(event_id: any) {
        this.id = event_id;
        console.log('setId: ', this.id);
    }
    public getId() {
        return this.id;
    }
    public getEvent(events_id: any){
        let get_url = this.action_url + this.route_link + '/' + events_id;
        return this._http.get(get_url).map(this.extractData);
    }

    public storeEvent(event_model: any){
        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(event_model), ).map(this.extractData);
    }

    public storeEventDetails(event_model: any){
        
        let post_url = this.action_url + this.route_link + '/' + 'event_details';
        
        return this._http.post(post_url, JSON.stringify(event_model), ).map(this.extractData);

    }

    public storeEventType(event_model: any){
        
        let post_url = this.action_url + this.route_link + '/' + 'event_type';
       console.log(post_url);
        return this._http.post(post_url, JSON.stringify(event_model), ).map(this.extractData);

    }

    public updateEvent(id: any, event_model: any){

        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, event_model, ).map(this.extractData);

    }
    public archiveEvent(event_id: any){

        console.log('postarchive', event_id);
        let delete_url = this.action_url + this.route_link + '/' + event_id;
        return this._http.delete(delete_url,  ).map(this.extractData);
    }


    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
  }

  public getEventDetails(){

        let action_url = this.action_url + this.route_link;

        return this._http.get(action_url).map(this.extractData);
        // let postData = JSON.stringify(headersData);
        // return this._http.get(this.actionUrl + employee_route).map((response: Response) => <Employee[]>response.json());
    }

  public getEventStatus(event_id: any){

        let get_url = this.action_url + this.route_link + '/' + event_id;

        return this._http.get(get_url, ).map(this.extractData);
    }

  public updateStatus(id_status: any ,eventUpdate: any){

        let post_url = this.action_url + this.route_link + '/' + id_status;
        return this._http.put(post_url , eventUpdate, ).map(this.extractData);
    }

    public getStatus(){
        let leave_route = 'leavestatus';
        let action_url = this.action_url + leave_route;

        return this._http.get(action_url).map(this.extractData);
        // let postData = JSON.stringify(headersData);
        // return this._http.get(this.action_url + employee_route).map((response: Response) => <Employee[]>response.json());
    }
    public updateEventStatus(id_status: any ,update_model: any){
        let post_url = this.action_url + this.route_link + '/' + id_status;
        console.log(post_url);
        return this._http.put(post_url , update_model, ).map(this.extractData);
    }

    public getEventLists(filterEvent){
        let get_url = this.action_url + 'event' + '/' + 'time_table' + '/' + filterEvent;

        return this._http.get(get_url, ).map(this.extractData);
    }

    public getEventName(model){
        let event = model.event_id;

        let uri_data = 'event_id=' + event;
      
        let action_url = this.action_url + this.route_link  + '/' + 'name?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
           
    }

    public getEventList(){
      
        let action_url = this.action_url + this.route_link  + '/' + 'events';

        return this._http.get(action_url).map(this.extractData);
        
    }

    public getEventTypeLists(){

        let action_url = this.action_url + this.route_link  + '/' + 'et';

        return this._http.get(action_url).map(this.extractData);

    }
    public getEventsList(){

        let action_url = this.action_url + 'list/event';

        return this._http.get(action_url).map(this.extractData);

    }

}