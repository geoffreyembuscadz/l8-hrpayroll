var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var UsersService = /** @class */ (function () {
    function UsersService(_http, http, _conf) {
        this._http = _http;
        this.http = http;
        this._conf = _conf;
        this.reload = false;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'users';
        this.password_change_link = 'password/change';
    }
    UsersService.prototype.getUsers = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    UsersService.prototype.setId = function (user_id) {
        this.id = user_id;
    };
    UsersService.prototype.getId = function () {
        return this.id;
    };
    UsersService.prototype.getReload = function () {
        return this.reload;
    };
    UsersService.prototype.setReload = function (reload) {
        this.reload = reload;
    };
    UsersService.prototype.getUser = function (user_id) {
        var get_url = this.action_url + this.route_link + '/' + user_id;
        return this._http.get(get_url).map(this.extractData);
    };
    UsersService.prototype.storeUser = function (users_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(users_model)).map(this.extractData);
    };
    UsersService.prototype.updateUser = function (id, users_modal) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, users_modal).map(this.extractData);
    };
    UsersService.prototype.changePassword = function (id, users_modal) {
        var put_url = this.action_url + this.password_change_link + '/' + id;
        return this._http.put(put_url, users_modal).map(this.extractData);
    };
    UsersService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    UsersService.prototype.handleError = function (error) {
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    UsersService.prototype.archiveUser = function (user_id) {
        var delete_url = this.action_url + this.route_link + '/' + user_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    UsersService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, HttpClient, Configuration])
    ], UsersService);
    return UsersService;
}());
export { UsersService };
//# sourceMappingURL=users.service.js.map