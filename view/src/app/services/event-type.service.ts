import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { EventType } from '../model/event_type';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';


@Injectable()
export class EventTypeService {
    private action_url: string;
    private route_link: string;

    private event_type_id: string;
    private headers: Headers;
    public id:any;


    constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'event_type';
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    public getEventType(){
        let get_url = this.action_url + this.route_link ;
        return this._http.get(get_url).map(this.extractData);
    }

    public getEventTypeById(id){

        let get_url = this.action_url + this.route_link + '/' + id;

        return this._http.get(get_url).map(this.extractData);
    }

    public updateEventType(id,model){

        let put_url = this.action_url  + this.route_link + '/' + id;

        return this._http.put(put_url,model).map(this.extractData);
    }

    public createEventType(model:any){

        let post_url = this.action_url + this.route_link ;

        return this._http.post(post_url, model).map(this.extractData);
    }

    public getEventsType(){
      
        let action_url = this.action_url + 'add/type';
        return this._http.get(action_url).map(this.extractData);       
    }



}