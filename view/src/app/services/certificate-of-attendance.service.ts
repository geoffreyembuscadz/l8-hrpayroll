import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Role } from '../model/role';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';


@Injectable()
export class CertificateOfAttendanceService {

    private action_url: string;
    private route_link: string;
    public id:any;

    constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'certificate_of_attendance';
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    public createCOA(coa_model: any){

        let post_url = this.action_url + this.route_link + '/' + 'create';

        return this._http.post(post_url, JSON.stringify(coa_model)).map(this.extractData);
    }

    public createMyCOA(coa_model: any){

        let post_url = this.action_url + this.route_link + '/' + 'create/my_coa';

        return this._http.post(post_url, JSON.stringify(coa_model)).map(this.extractData);
    }

    public getCOA(id: any){

        let get_url = this.action_url + this.route_link + '/' + id;

        return this._http.get(get_url).map(this.extractData);
    }

    public updateCOA(update_model: any){

        let post_url = this.action_url + this.route_link + '/update/coa';
        return this._http.post(post_url, JSON.stringify(update_model)).map(this.extractData);
    }

    public updateMyCOA(update_model: any){

        let post_url = this.action_url + this.route_link + '/update/my_coa';
        return this._http.post(post_url, JSON.stringify(update_model)).map(this.extractData);
    }

    public getMyCOA(model){

        let start = model.start_date;
        let end = model.end_date;
        let status_id = model.status_id;
        let type_id = model.type_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end+ '&status_id=' + status_id+ '&type_id=' + type_id;

        let get_url = this.action_url + this.route_link + '/' + 'my_coa' + '/' + 'data?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    }

    public getCOAList(){

        let get_url = this.action_url + this.route_link;

        return this._http.get(get_url).map(this.extractData);
    }

    public getFilteredCOA(model: any){

        let start = model.start_date;
        let end = model.end_date;
        let status_id = model.status_id;
        let company_id = model.company_id;
        let employee_id = model.employee_id;
        let type_id = model.type_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end+ '&status_id=' + status_id+ '&company_id=' + company_id+ '&employee_id=' + employee_id+ '&type_id=' + type_id;

        let get_url = this.action_url + this.route_link + '/' + 'filter' + '/' + 'data?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public duplicateEntryValidation(coa: any){

        let post_url = this.action_url + this.route_link + '/duplicate_entry_validation';
        return this._http.post(post_url, JSON.stringify(coa)).map(this.extractData);
    }

}
