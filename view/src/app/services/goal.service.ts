import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class GoalService {
    private action_url: string;
    private route_link: string;

    private goal_id: string;
    private headers: Headers;
    public id:any;


    constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'goal';
     
    }


    public getGoals(){
        let get_url = this.action_url + this.route_link;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public setId(goal_id: any) {
        this.id = goal_id;
        console.log('setId: ', goal_id);
    }
    public getId() {
        return this.id;
    }
    public getGoal(goals_id: any){
        let get_url = this.action_url + this.route_link + '/' + goals_id;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public storeGoal(goal_model: any){
        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(goal_model), ).map(this.extractData);
    }

    public updateGoal(id: any, goal_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, goal_model, ).map(this.extractData);

    }
    public archiveGoal(goal_id: any){

        console.log('postarchive', goal_id);
        let delete_url = this.action_url + this.route_link + '/' + goal_id;
        return this._http.delete(delete_url,  ).map(this.extractData);
    }


    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    public showGoal(model){ 
        let employee_id = model.employee_id;
        let uri_data = 'employee_id=' + employee_id;

        let action_url = this.action_url + 'show/goal?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

}