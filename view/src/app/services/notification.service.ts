import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
// import * as io from "socket.io-client";

@Injectable()
export class NotificationService {

    private action_url: string;
    private route_link: string;
    public id:any;
    

      constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'notification';
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    public getNotification(){

        let get_url = this.action_url + this.route_link;

        return this._http.get(get_url).map(this.extractData);
    }

    public updateNotif(model){

        let post_url = this.action_url + this.route_link + '/' + 'update';

        return this._http.post(post_url, JSON.stringify(model)).map(this.extractData);
    }

    public getNotif(data,user_id){

        let uri_data = 'id=' + data + '&user_id=' + user_id;

        let get_url = this.action_url + this.route_link + '/' + 'notif' + '/' + 'data?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public getAllNotification(model){

        let start_date = model.start_date;
        let end_date = model.end_date;

        let uri_data = 'start_date=' + start_date+ '&end_date=' + end_date;

        let get_url = this.action_url + this.route_link + '/all/notif?' + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public markAllAsRead(){
        let id = 0;
        let post_url = this.action_url + this.route_link + '/' + 'markall';

        return this._http.post(post_url, JSON.stringify(id)).map(this.extractData);
    }

}
