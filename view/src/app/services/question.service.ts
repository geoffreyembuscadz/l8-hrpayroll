import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class QuestionService {
    private action_url: string;
    private route_link: string;

    private question_id: string;
    private headers: Headers;
    public id:any;


    constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'question';
     
    }


    public getQuestions(){
        let get_url = this.action_url + this.route_link;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public setId(question_id: any) {
        this.id = question_id;
    }
    public getId() {
        return this.id;
    }
    public getQuestion(questions_id: any){
        let get_url = this.action_url + this.route_link + '/' + questions_id;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public storeQuestion(question_model: any){
        let post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(question_model), ).map(this.extractData);
    }

    public updateQuestion(id: any, question_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, question_model, ).map(this.extractData);

    }
    public archiveQuestion(question_id: any){

        let delete_url = this.action_url + this.route_link + '/' + question_id;
        return this._http.delete(delete_url,  ).map(this.extractData);
    }


    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }
    public questionList(){
      
        let action_url = this.action_url + 'question/list';
        return this._http.get(action_url).map(this.extractData);       
    }

    public storeEvaluation(evaluation_model: any){
        let post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(evaluation_model), ).map(this.extractData);
    }

    public getEmpByStatus(emp_stat){
        let employment_status = emp_stat;

        let uri_data = 'emp_stat=' + emp_stat;

        let action_url = this.action_url + 'employment/status?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

    public getEmployeeStatus(){
         
        let action_url = this.action_url + 'employee/type/status';
        return this._http.get(action_url).map(this.extractData);       
    }

    public rating(){

        let action_url = this.action_url + 'show/rating' ;
        return this._http.get(action_url).map(this.extractData);       
    }

    public addSegment(){

        let action_url = this.action_url + 'add/segment' ;
        return this._http.get(action_url).map(this.extractData);       
    }

    public addCategory(){       
        let action_url = this.action_url + 'add/category';
        return this._http.get(action_url).map(this.extractData);       
    }

    public addSubCategory(){       
        let action_url = this.action_url + 'add/subcategory';
        return this._http.get(action_url).map(this.extractData);       
    }

    public addPosition(){       
        let action_url = this.action_url + 'get/emp/position';
        return this._http.get(action_url).map(this.extractData);       
    }

    public storeEvaluationTemplate(evaluation_model: any){
        let post_url = this.action_url + 'evaluation/template';
        return this._http.post(post_url, JSON.stringify(evaluation_model), ).map(this.extractData);
    }

    public getEvaluationData(model){
        let employee_position = model.position;
        let id = model.id;

        let uri_data = 'position=' + employee_position + '&id=' + id;
        let action_url = this.action_url + 'get/evaluation?' + uri_data;
        return this._http.get(action_url).map(this.extractData);     
    }

    public getEvaluationTitle(position){
        let position_id = position;

        let uri_data = 'position=' + position_id;

        let action_url = this.action_url + 'show/title?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

    public getEmployeeByPosition(position){
        let position_id = position;

        let uri_data = 'position=' + position_id;

        let action_url = this.action_url + 'show/emp/by/pos?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

    public postEvaluationForm(evaluation_model: any){
        let post_url = this.action_url + 'evaluation/form';
        return this._http.post(post_url, JSON.stringify(evaluation_model), ).map(this.extractData);
    }

    public getEmployeeEvaluation(model){     
        let id = model.id;
        
        let uri_data = 'id=' + id;  
        let action_url = this.action_url + 'get/emp/evaluation?' + uri_data ;     
        return this._http.get(action_url).map(this.extractData);

    }

    public createEvaluationForm(evaluation_model: any){
        let post_url = this.action_url + 'create/evaluation/form';
        return this._http.post(post_url, JSON.stringify(evaluation_model), ).map(this.extractData);
    }

    public showAllEmployee(){       
        let action_url = this.action_url + 'show/all/employee';
        return this._http.get(action_url).map(this.extractData);       
    }


    public showEmployeeRate(){       
        let action_url = this.action_url + 'show/emp/rate';
        return this._http.get(action_url).map(this.extractData);       
    }

    public filterEmployee(model){
        let type = model.type;

        let uri_data = 'type=' + type;
        let action_url = this.action_url + 'filter/employee?' + uri_data;
        return this._http.get(action_url).map(this.extractData);     
    }


    public viewEmployeeEvaluation(model){
        let id = model.id;

        let uri_data = 'id=' + id;
        let action_url = this.action_url + 'view/employee/rate?' + uri_data;
        return this._http.get(action_url).map(this.extractData);     
    }


    public getEmployee(model){
        let supervisor_id = model.supervisor_id;

        let uri_data = 'supervisor_id=' + supervisor_id;
        let action_url = this.action_url + 'show/emp/under/sup?' + uri_data;
        return this._http.get(action_url).map(this.extractData);     
    }


}

