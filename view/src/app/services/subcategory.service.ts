import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';


@Injectable()
export class SubcategoryService {
    private action_url: string;
    private route_link: string;

    private subcategory_id: string;
    private headers: Headers;
    public id:any;


    constructor(
    			private _http: HttpClient,
    			private _conf: Configuration
    			){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'subcategory';

    }


    public getSubcategorys(){
        let get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    }

    public setId(subcategory_id: any) {
        this.id = subcategory_id;
        console.log('setId: ', subcategory_id);
    }
    public getId() {
        return this.id;
    }
    public getSubcategory(subcategorys_id: any){
        let get_url = this.action_url + this.route_link + '/' + subcategorys_id;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public storeSubcategory(subcategory_model: any){
        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(subcategory_model)).map(this.extractData);
    }

    public updateSubcategory(id: any, subcategory_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, subcategory_model, ).map(this.extractData);

    }
    public archiveSubcategory(subcategory_id: any){

        console.log('postarchive', subcategory_id);
        let delete_url = this.action_url + this.route_link + '/' + subcategory_id;
        return this._http.delete(delete_url,  ).map(this.extractData);
    }


    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }

    public subcategory_name(){
      
        let action_url = this.action_url + 'subcategory/name';
        return this._http.get(action_url).map(this.extractData);       
    }

    public questionList(){
      
        let action_url = this.action_url + 'question/list';
        return this._http.get(action_url).map(this.extractData);       
    }

    public storeSubcategoryDetails(subcategory_model: any){
        
        let post_url = this.action_url + 'post/subcategory';
        
        return this._http.post(post_url, JSON.stringify(subcategory_model), ).map(this.extractData);
    }

    public catName(){
      
        let action_url = this.action_url + 'cat/name';
        return this._http.get(action_url).map(this.extractData);       
    }

}