import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class AppraisalService {
    private action_url: string;
    private route_link: string;

    private appraisal_id: string;
    private headers: Headers;
    public id:any;


    constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'appraisal';
     
    }


    public getAppraisals(){
        let get_url = this.action_url + this.route_link;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public setId(appraisal_id: any) {
        this.id = appraisal_id;
        console.log('setId: ', appraisal_id);
    }
    public getId() {
        return this.id;
    }
    public getAppraisal(appraisals_id: any){
        let get_url = this.action_url + this.route_link + '/' + appraisals_id;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public storeAppraisal(appraisal_model: any){
        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(appraisal_model), ).map(this.extractData);
    }

    public updateAppraisal(id: any, appraisal_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, appraisal_model, ).map(this.extractData);

    }
    public archiveAppraisal(appraisal_id: any){

        console.log('postarchive', appraisal_id);
        let delete_url = this.action_url + this.route_link + '/' + appraisal_id;
        return this._http.delete(delete_url,  ).map(this.extractData);
    }


    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    public getObjectives(model){    
        let employee_id = model.employee_id;
        let uri_data = 'employee_id=' + employee_id; 

        let action_url = this.action_url + 'show/appraisal?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

    public updateObj(ot: any){

        let post_url = this.action_url + 'appraisal/update';
        return this._http.post(post_url, JSON.stringify(ot)).map(this.extractData);
    }

    public getEmployee(){    
        let action_url = this.action_url + 'all/employee';
        return this._http.get(action_url).map(this.extractData);       
    }

    public getObjNow(model){    
        let employee_id = model.employee_id;
        let obj_id = model.obj_id;

        let uri_data = 'employee_id=' + employee_id; 

        let action_url = this.action_url + 'get/obj?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }


    public getObjRate(model){    
        let employee_id = model.employee_id;
        let id = model.id;

        let uri_data = 'employee_id=' + employee_id +'&id=' + id; 

        let action_url = this.action_url + 'rate/obj?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

    public saveRateObj(obj_model: any){
        let post_url = this.action_url + 'save/obj/rate';
        return this._http.post(post_url, JSON.stringify(obj_model), ).map(this.extractData);
    }


  



}