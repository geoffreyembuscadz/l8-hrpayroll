import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from './http-extended.service';
import { Configuration } from '../app.config';

@Injectable()
export class PermissionService {
    private action_url: string;
    private route_link: string;
    private route_action: string;
    private permission_id: string;
    public id: any;

	constructor(private _http: HttpClient, private _conf: Configuration){
		this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'permission';
	}

	public getPermissions(){
		let get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    }

    public getPermission(permission_id: any){
        let get_url = this.action_url + this.route_link + '/' + permission_id;
        return this._http.get(get_url).map(this.extractData);
    }

    public setId(permission_id: any) {
        this.id = permission_id;
    }
    public getId() {
        return this.id;
    }

    public updatePermission(id: any, perms_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, perms_model).map(this.extractData);

    }

    public archivePermission(role_id: any){
        console.log('postarchive', role_id);
        let delete_url = this.action_url + this.route_link + '/' + role_id;
        console.log(delete_url);
        console.log(role_id);

        return this._http.delete(delete_url ).map(this.extractData);
    }

    public storePermission(perms_model: any){
        let post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(perms_model)).map(this.extractData);
    }

    private extractData(res: Response) {
    	let body = res.json();
    	return body.data || { };
    }

    private handleError (error: Response | any){
    	// In a real world app, we might use a remote logging infrastructure
    	let errMsg: string;
    	if (error instanceof Response) {
    		const body = error.json() || '';
    		const err = body.error || JSON.stringify(body);
    		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    	} else {
    		errMsg = error.message ? error.message : error.toString();
    	}

    	console.error(errMsg);
    	return Observable.throw(errMsg);
  }
}