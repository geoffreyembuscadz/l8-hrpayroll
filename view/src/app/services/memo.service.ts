import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';


@Injectable()
export class MemoService {
	private action_url: string;
	private route_link: string;

	private memo_id: string;
	private headers: Headers;
	public id:any;

	constructor(private _http: HttpClient, 
		private _conf: Configuration)
		 {
		this.action_url = this._conf.ServerWithApiUrl;
		this.route_link = 'memo';
		 }

		 private extractData(res: Response) {
		 	let body = res.json();
		 	return body.data || { };
		 }

		 private handleError (error: Response | any){
		 	// In a real world app, we might use a remote logging infrastructure
		 	let errMsg: string;
		 	if (error instanceof Response) {
		 		const body = error.json() || '';
		 		const err = body.error || JSON.stringify(body);
		 		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		 	} else {
		 		errMsg = error.message ? error.message : error.toString();
		 	}

		 	console.error(errMsg);
		 	return Observable.throw(errMsg);
		 }

		public getMemos(){
			let get_url = this.action_url + this.route_link;
			return this._http.get(get_url,).map(this.extractData);
		}

		public setId(memo_id: any){
			this.id = memo_id;
			console.log('setId: ', memo_id);
		}
		public getId(){
			return this.id;
		}
		public getMemo(memos_id: any){
			let get_url = this.action_url + this.route_link + '/' + memos_id;
			return this._http.get(get_url,).map(this.extractData);
		}

		public storeMemo(memo_model: any){
			let post_url = this.action_url + this.route_link;

			return this._http.post(post_url, JSON.stringify(memo_model),).map(this.extractData);
		}

		public updateMemo(id: any, memo_model: any){
			let put_url = this.action_url + this.route_link + '/' + id;

			return this._http.put(put_url, memo_model,).map(this.extractData);

		}
		public archiveMemo(memo_id: any){

			console.log('postarchive', memo_id);
			let delete_url = this.action_url + this.route_link + '/' + memo_id;
			return this._http.delete(delete_url, ).map(this.extractData);
		}

		public getMemoList(){
			let get_url = this.action_url + this.route_link + '/list';
			return this._http.get(get_url,).map(this.extractData);
		}

		public showFilterMemo(model: any){
	        let start = model.start_date;
	        let end = model.end_date;
	        let emp_id = model.emp_id;
	        let company_id = model.company_id;
	      
	        let uri_data = 'start_date=' + start + '&end_date=' + end+ '&emp_id=' + emp_id + '&company_id=' + company_id;

	        let action_url = this.action_url + 'filter/memo?' + uri_data;
	        
	        return this._http.get(action_url).map(this.extractData);
	        
	    }

		public createMemo(memo_model: any){
			let post_url = this.action_url + this.route_link + '/create';

			return this._http.post(post_url, JSON.stringify(memo_model),).map(this.extractData);
		}

		public showMemo(){
			let get_url = this.action_url + this.route_link + '/show';
			return this._http.get(get_url,).map(this.extractData);
		}


	   
}
