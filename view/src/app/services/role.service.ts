import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Role } from '../model/role';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class RoleService {
    private action_url: string;
    private route_link: string;

    private role_id: string;
    public id:any;
    public name: any;



	constructor(private _http: HttpClient, private _conf: Configuration){
		this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'role';
	}


	public getRoles(){
		let get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    }

    public setId(role_id: any) {
        this.id = role_id;
     
    }
    public getId() {
        return this.id;
    }

    public setName(role_name:any) {
        this.name = role_name;
    }

    public getName(){
        return this.name;
    }
    
    public getRole(role_id: any){
        let get_url = this.action_url + this.route_link + '/' + role_id;
        return this._http.get(get_url).map(this.extractData);
    }


    public storeRole(roles_model: any){
        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(roles_model)).map(this.extractData);
    }

    public updateRole(id: any, role_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, role_model).map(this.extractData);

    }

    public archiveRole(role_id: any){
        let delete_url = this.action_url + this.route_link + '/' + role_id;
        console.log(delete_url);
        console.log(role_id);

        return this._http.delete(delete_url).map(this.extractData);
    }


    private extractData(res: Response) {
    	let body = res.json();
    	return body.data || { };
    }

    private handleError (error: Response | any){
    	// In a real world app, we might use a remote logging infrastructure
    	let errMsg: string;
    	if (error instanceof Response) {
    		const body = error.json() || '';
    		const err = body.error || JSON.stringify(body);
    		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    	} else {
    		errMsg = error.message ? error.message : error.toString();
    	}

    	console.error(errMsg);
    	return Observable.throw(errMsg);
    }

}