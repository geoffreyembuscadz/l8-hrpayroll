import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';


@Injectable()
export class CategoryService {
    private action_url: string;
    private route_link: string;

    private category_id: string;
    private headers: Headers;
    public id:any;


    constructor(
    			private _http: HttpClient,
    			private _conf: Configuration
    			){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'category';

    }


    public getCategorys(){
        let get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    }

    public setId(category_id: any) {
        this.id = category_id;
        console.log('setId: ', category_id);
    }
    public getId() {
        return this.id;
    }
    public getCategory(categorys_id: any){
        let get_url = this.action_url + this.route_link + '/' + categorys_id;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public storeCategory(category_model: any){
        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(category_model)).map(this.extractData);
    }

    public updateCategory(id: any, category_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, category_model, ).map(this.extractData);

    }
    public archiveCategory(category_id: any){

        console.log('postarchive', category_id);
        let delete_url = this.action_url + this.route_link + '/' + category_id;
        return this._http.delete(delete_url,  ).map(this.extractData);
    }


    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }

    public category_name(){
      
        let action_url = this.action_url + 'category/name';
        return this._http.get(action_url).map(this.extractData);       
    }

    public questionList(){
      
        let action_url = this.action_url + 'question/list';
        return this._http.get(action_url).map(this.extractData);       
    }

    public storeCategoryDetails(category_model: any){
        
        let post_url = this.action_url + 'post/category';
        
        return this._http.post(post_url, JSON.stringify(category_model), ).map(this.extractData);

    }

    public getSubCat(){
      
        let action_url = this.action_url + 'get/category/list';
        return this._http.get(action_url).map(this.extractData);       
    }

    public storeSubCategory(category_model: any){        
        let post_url = this.action_url + 'sub/category';        
        return this._http.post(post_url, JSON.stringify(category_model), ).map(this.extractData);

    }

    public updateSubCategory(id: any, category_model: any){
        let put_url = this.action_url + 'subcategory' + '/' + id;

        return this._http.put(put_url, category_model ).map(this.extractData);

    }

    public getSubcategorys(){
        let get_url = this.action_url + 'subcategory';
        return this._http.get(get_url).map(this.extractData);
    }

    public getSubcategory(subcategorys_id: any){
        let get_url = this.action_url + 'subcategory' + '/' + subcategorys_id;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public getSegment(){
        let get_url = this.action_url + 'get/segments';
        return this._http.get(get_url).map(this.extractData);
    }

}