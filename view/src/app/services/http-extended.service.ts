import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export class HttpClient {
  private headers: Headers;
  constructor(private http: Http) {
    this.headers = new Headers();
    let authToken = localStorage.getItem('id_token');
    this.headers.append('Authorization', `Bearer ${authToken}`);
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  get(url) {
    return this.http.get(url, { headers:this.headers } );
  }

  post(url, data) {
    return this.http.post(url, data, { headers:this.headers } );
  }

  put(url, data) {
    return this.http.put(url, data, { headers:this.headers } );
  }

  delete(url) {
    return this.http.delete(url, { headers:this.headers } );
  }
}