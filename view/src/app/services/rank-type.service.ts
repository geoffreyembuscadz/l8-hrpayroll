import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Rank_Type } from '../model/rank_type';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class RankTypeService {
    private action_url: string;
    private route_link: string;

    private Rank_type_id: string;
    private headers: Headers;
    public id:any;


    constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'rank_type';

    }


    public getRanks(){
        let get_url = this.action_url + this.route_link;
        return this._http.get(get_url,).map(this.extractData);
    }

    public setId(Rank_type_id: any) {
        this.id = Rank_type_id;
        console.log('setId: ', Rank_type_id);
    }
    public getId() {
        return this.id;
    }
    public getRankType(ranks_id: any){
        let get_url = this.action_url + this.route_link + '/' + ranks_id;
        return this._http.get(get_url,).map(this.extractData);
    }

    public storeRankType(Rank_type_model: any){
        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(Rank_type_model),).map(this.extractData);
    }

    public updateRankType(id: any, Rank_type_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, Rank_type_model,).map(this.extractData);

    }
    public archiveRankType(Rank_type_id: any){

        console.log('postarchive', Rank_type_id);
        let delete_url = this.action_url + this.route_link + '/' + Rank_type_id;
        return this._http.delete(delete_url, ).map(this.extractData);
    }


    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

     public getRankTypes(){       
        let action_url = this.action_url + 'show/rank_type';
        return this._http.get(action_url).map(this.extractData);       
    }



}