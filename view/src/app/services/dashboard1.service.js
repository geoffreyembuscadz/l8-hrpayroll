var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var Dashboard1Service = /** @class */ (function () {
    function Dashboard1Service(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
    }
    Dashboard1Service.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    Dashboard1Service.prototype.handleError = function (error) {
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    Dashboard1Service.prototype.getOnLeave = function () {
        var action_url = this.action_url + 'onleave/attendancereport';
        return this._http.get(action_url).map(this.extractData);
    };
    Dashboard1Service.prototype.getLateEmp = function () {
        var action_url = this.action_url + 'attendance/late';
        return this._http.get(action_url).map(this.extractData);
    };
    Dashboard1Service.prototype.getEmpRequest = function () {
        var action_url = this.action_url + 'emp/request';
        return this._http.get(action_url).map(this.extractData);
    };
    Dashboard1Service.prototype.showEmpLate = function () {
        var action_url = this.action_url + 'rep/late';
        return this._http.get(action_url).map(this.extractData);
    };
    Dashboard1Service.prototype.showFilterLate = function (model) {
        var start = model.start_date;
        var end = model.end_date;
        var company_id = model.company_id;
        var employee_id = model.employee_id;
        var sort = model.sort;
        var uri_data = 'start_date=' + start + '&end_date=' + end + '&company_id=' + company_id + '&sort=' + sort + '&employee_id=' + employee_id;
        var action_url = this.action_url + 'show/filter/late?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    Dashboard1Service.prototype.getMemo = function () {
        var action_url = this.action_url + 'memo';
        return this._http.get(action_url).map(this.extractData);
    };
    Dashboard1Service.prototype.getEmpCount = function () {
        var action_url = this.action_url + 'emp/count';
        return this._http.get(action_url).map(this.extractData);
    };
    Dashboard1Service.prototype.filterTotalEmployee = function () {
        var action_url = this.action_url + 'total/emp/filter';
        return this._http.get(action_url).map(this.extractData);
    };
    Dashboard1Service.prototype.getCompanyActive = function () {
        var action_url = this.action_url + 'company/name';
        return this._http.get(action_url).map(this.extractData);
    };
    Dashboard1Service.prototype.getEmployeeActive = function () {
        var action_url = this.action_url + 'employee/per/company';
        return this._http.get(action_url).map(this.extractData);
    };
    Dashboard1Service.prototype.getPendingRequest = function () {
        var action_url = this.action_url + 'pending_request';
        return this._http.get(action_url).map(this.extractData);
    };
    Dashboard1Service = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient,
            Configuration])
    ], Dashboard1Service);
    return Dashboard1Service;
}());
export { Dashboard1Service };
//# sourceMappingURL=dashboard1.service.js.map