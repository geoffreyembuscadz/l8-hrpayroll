import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Login } from '../model/login';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Configuration } from '../app.config';

@Injectable()
export class AuthService {

	private action_url: string;
  private route_link: string;
  public error_title: string;
  public error_message: string;
  private headers: Headers;
  private error: any;
  private token_valid: any;


  constructor(private _http: Http, private router: Router, private _conf: Configuration) {
    this.action_url = this._conf.ServerWithApiUrl;
    this.route_link = 'auth/login';
    let authToken = localStorage.getItem('id_token');
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.headers.append('Authorization', `Bearer ${authToken}`);
  }


  login(login_model: any) {
    
  	let login_url = this.action_url + this.route_link;
    return this._http
      .post( login_url, JSON.stringify(login_model), { headers:this.headers } )
      .map(res => res.json())
      .map((res) => {
        if (res.status) {
          localStorage.setItem('id_token', res.data.token);
        } 
        return res.status;
      });
  }

  isLoggedIn() {
    let authToken = localStorage.getItem('id_token');
    if(authToken == '' || authToken == null || authToken === 'undefined' || this.token_valid == true) {
      localStorage.removeItem('id_token');
      this.router.navigate([ '/login' ]);
      return false;
    } 
    return true;
  }

 

}
