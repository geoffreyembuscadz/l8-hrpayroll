import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class RatingService {
    private action_url: string;
    private route_link: string;

    private rating_id: string;
    private headers: Headers;
    public id:any;


    constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'rating';
     
    }


    public getRatings(){
        let get_url = this.action_url + this.route_link;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public setId(rating_id: any) {
        this.id = rating_id;
        console.log('setId: ', rating_id);
    }
    public getId() {
        return this.id;
    }
    public getRating(ratings_id: any){
        let get_url = this.action_url + this.route_link + '/' + ratings_id;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public storeRating(rating_model: any){
        let post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(rating_model), ).map(this.extractData);
    }

    public updateRating(id: any, rating_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, rating_model, ).map(this.extractData);

    }
    public archiveRating(rating_id: any){

        console.log('postarchive', rating_id);
        let delete_url = this.action_url + this.route_link + '/' + rating_id;
        return this._http.delete(delete_url,  ).map(this.extractData);
    }


    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }
    public showRating(){
      
        let action_url = this.action_url + 'show/rating';
        return this._http.get(action_url).map(this.extractData);       
    }



}