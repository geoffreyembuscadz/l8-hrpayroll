import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Users } from '../model/users';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class UsersService {
    private action_url: string;
    private route_link: string;
    private password_change_link: string;
    public id:any;
    public reload: Boolean = false;

	constructor(private _http: HttpClient, private http: HttpClient, private _conf: Configuration){
		this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'users';
        this.password_change_link = 'password/change';
	}

	public getUsers(){
		let get_url = this.action_url + this.route_link;
		return this._http.get(get_url).map(this.extractData);
    }

    public setId(user_id: any) {
        this.id = user_id;
    }

    public getId() {
        return this.id;
    }  

    public getReload() {
        return this.reload;
    }
    
    public setReload(reload: boolean) {
        this.reload = reload;        
    }
    
    public getUser(user_id: any){
        let get_url = this.action_url + this.route_link + '/' + user_id;
        return this._http.get(get_url).map(this.extractData);
    }

    public storeUser(users_model: any){
        let post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(users_model)).map(this.extractData);
    }

    public updateUser(id: any, users_modal: any){
        let put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, users_modal).map(this.extractData);
    }

    public changePassword(id: any, users_modal: any){
        let put_url = this.action_url + this.password_change_link + '/' + id;
        return this._http.put(put_url, users_modal).map(this.extractData);
    }

    private extractData(res: Response) {
    	let body = res.json();
    	return body.data || { };
    }

    private handleError (error: Response | any){
    	let errMsg: string;
    	if (error instanceof Response) {
    		const body = error.json() || '';
    		const err = body.error || JSON.stringify(body);
    		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    	} else {
    		errMsg = error.message ? error.message : error.toString();
    	}

    	console.error(errMsg);
    	return Observable.throw(errMsg);
    }

    public archiveUser(user_id: any){
        let delete_url = this.action_url + this.route_link + '/' + user_id;
        return this._http.delete(delete_url).map(this.extractData);
    }
}