var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Configuration } from '../app.config';
import { ActivatedRoute } from '@angular/router';
var PasswordResetService = /** @class */ (function () {
    function PasswordResetService(_http, _conf, route) {
        this._http = _http;
        this._conf = _conf;
        this.route = route;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'password';
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', "Bearer " + authToken);
    }
    PasswordResetService.prototype.reset = function (reset_model) {
        var reset_url = this.action_url + this.route_link + '/' + 'email';
        return this._http.post(reset_url, JSON.stringify(reset_model), { headers: this.headers }).map(this.extractData);
    };
    PasswordResetService.prototype.resetPassword = function (reset_password_model, token) {
        var reset_url = this.action_url + this.route_link + '/reset/' + token;
        return this._http.post(reset_url, JSON.stringify(reset_password_model), { headers: this.headers }).map(this.extractData);
    };
    PasswordResetService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    PasswordResetService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http, Configuration, ActivatedRoute])
    ], PasswordResetService);
    return PasswordResetService;
}());
export { PasswordResetService };
//# sourceMappingURL=password-reset.service.js.map