import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class Dashboard1Service {

    private action_url: string;
    private route_link: string;
    private actionUrl: string;

    constructor(private _http: HttpClient,
        private _conf: Configuration){

        this.action_url = this._conf.ServerWithApiUrl;

    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    public getOnLeave(){

        let action_url = this.action_url + 'onleave/attendancereport';

        return this._http.get(action_url).map(this.extractData);

    }

    public getLateEmp(){

        let action_url = this.action_url + 'attendance/late';

        return this._http.get(action_url).map(this.extractData);

    }


    public getEmpRequest(){

        let action_url = this.action_url + 'emp/request';
        return this._http.get(action_url).map(this.extractData);

    }

    public showEmpLate(){

        let action_url = this.action_url + 'rep/late';        
        return this._http.get(action_url).map(this.extractData);

    }

    public showFilterLate(model: any){
        let start = model.start_date;
        let end = model.end_date;
        let company_id = model.company_id;
        let employee_id = model.employee_id;
        let sort = model.sort;

        let uri_data = 'start_date=' + start + '&end_date=' + end+ '&company_id=' + company_id + '&sort=' + sort + '&employee_id=' + employee_id;

        let action_url = this.action_url + 'show/filter/late?' + uri_data;
        return this._http.get(action_url).map(this.extractData);       
    }

    public getMemo(){
        let action_url = this.action_url + 'memo';
        return this._http.get(action_url).map(this.extractData);

    }

    public getEmpCount(){
        let action_url = this.action_url + 'emp/count';
        return this._http.get(action_url).map(this.extractData);

    }


    public filterTotalEmployee(){
        let action_url = this.action_url + 'total/emp/filter';
        return this._http.get(action_url).map(this.extractData);

    }

    public getCompanyActive(){
        let action_url = this.action_url + 'company/name';
        return this._http.get(action_url).map(this.extractData);

    }


    public getEmployeeActive(){
        let action_url = this.action_url + 'employee/per/company';
        return this._http.get(action_url).map(this.extractData);

    }

    public getPendingRequest() {
        let action_url = this.action_url + 'pending_request';

        return this._http.get(action_url).map(this.extractData);

    }

}
