import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Users } from '../model/users';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';


@Injectable()
export class MainSideService {
    private action_url: string;
    private route_link1: string;
    private route_link2: string;
    public id: any;
    public roles: Array<any>;
    public module: any;

	constructor(private _http: HttpClient, private _conf: Configuration){
		this.action_url = this._conf.ServerWithApiUrl;
        this.route_link1 = 'module';
        this.route_link2 = 'module_menu';
	}


  public getModule() {
    let get_url = this.action_url + this.route_link1;
    
    return this._http.get(get_url).map(this.extractData);
  }

  public getModuleById(user_id: any) {
    let get_url = this.action_url + this.route_link1 + '/' + user_id;
    
    return this._http.get(get_url).map(this.extractData);
  }

  public getModuleMenu() {
    let get_url = this.action_url + this.route_link2;
    
    return this._http.get(get_url).map(this.extractData);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || { };
  }
}