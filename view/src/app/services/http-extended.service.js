var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
var HttpClient = /** @class */ (function () {
    function HttpClient(http) {
        this.http = http;
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }
    HttpClient.prototype.get = function (url) {
        return this.http.get(url, { headers: this.headers });
    };
    HttpClient.prototype.post = function (url, data) {
        return this.http.post(url, data, { headers: this.headers });
    };
    HttpClient.prototype.put = function (url, data) {
        return this.http.put(url, data, { headers: this.headers });
    };
    HttpClient.prototype.delete = function (url) {
        return this.http.delete(url, { headers: this.headers });
    };
    HttpClient = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http])
    ], HttpClient);
    return HttpClient;
}());
export { HttpClient };
//# sourceMappingURL=http-extended.service.js.map