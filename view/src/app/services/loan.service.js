var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from './http-extended.service';
import { Configuration } from '../app.config';
var LoanService = /** @class */ (function () {
    function LoanService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'loan';
    }
    LoanService.prototype.getLoans = function (id, start, end) {
        var get_url = this.action_url + this.route_link + '/?emp_id=' + id + '&start=' + start + '&end=' + end;
        ;
        return this._http.get(get_url).map(this.extractData);
    };
    LoanService.prototype.getLoan = function (loan_id) {
        var get_url = this.action_url + this.route_link + '/' + loan_id;
        return this._http.get(get_url).map(this.extractData);
    };
    LoanService.prototype.setId = function (loan_id) {
        this.id = loan_id;
    };
    LoanService.prototype.getId = function () {
        return this.id;
    };
    LoanService.prototype.updateLoan = function (id, loan_id) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, loan_id).map(this.extractData);
    };
    LoanService.prototype.archiveLoan = function (loan_id) {
        console.log('postarchive', loan_id);
        var delete_url = this.action_url + this.route_link + '/' + loan_id;
        console.log(delete_url);
        console.log(loan_id);
        return this._http.delete(delete_url).map(this.extractData);
    };
    LoanService.prototype.storeLoan = function (loan_id) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(loan_id)).map(this.extractData);
    };
    LoanService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    LoanService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    LoanService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], LoanService);
    return LoanService;
}());
export { LoanService };
//# sourceMappingURL=loan.service.js.map