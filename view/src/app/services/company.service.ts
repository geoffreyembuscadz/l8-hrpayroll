import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Company } from '../model/company';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class CompanyService {
    private action_url: string;
    private route_link: string;
    private route_link_company_branch: string;
    private route_link_company_policy: string;
    private route_link_company_policy_equiv: string;
    private route_link_company_policy_type: string;
    private route_link_company_policy_update: string;
    private route_link_working_schedule: string;
    private route_link_company_multi_branch: string;
    private route_link_company_branches: string;

    private company_id: string;
    private headers: Headers;
    public id:any;
    public company_policy_type_id: any;

    constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'company';
        this.route_link_company_branch = 'company_branch';
        this.route_link_company_policy = 'company_policy';
        this.route_link_company_policy_equiv = 'company_policy_equivalent';
        this.route_link_company_policy_type = 'company_policy_type';
        this.route_link_company_policy_update = 'update_company_policy';
        this.route_link_company_multi_branch = 'company_branch/multiple_update';
        this.route_link_company_branches = 'company_branches';
    }

    public getCompanys(){
        let get_url = this.action_url + this.route_link;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public getCompanyBranch(){
        let get_url = this.action_url + this.route_link_company_branch;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public getCompanyBranches() {
        let get_url = this.action_url + this.route_link_company_branches;

        return this._http.get(get_url, ).map(this.extractData);
    }

    public setId(company_id: any) {
        this.id = company_id;
    }
    public getId() {
        return this.id;
    }
    
    public setCompanyPolicyTypeId(id: any){
        this.company_policy_type_id = id;
    }

    public getCompanyPolicies(route_company_policy_link: any){
        let get_url = this.action_url + route_company_policy_link;
        return this._http.get(get_url).map(this.extractData);
    }

    public getCompanyPolicyTypeId(){
        return this.company_policy_type_id;
    }

    public updateCompanyPolicy(company_policy_datas: any){
        let company_id = company_policy_datas.company_id;
        let post_url = this.action_url + this.route_link_company_policy_update + '/' + company_id;
        return this._http.put(post_url, JSON.stringify(company_policy_datas), ).map(this.extractData);
    }

    public deleteCompanyPolicy(company_policy_id: any){
        let delete_url = this.action_url + this.route_link_company_policy + '/' + company_policy_id;
        return this._http.delete(delete_url,  ).map(this.extractData);
    }

    public deleteCompanyPolicyEquivalent(company_policy_equiv_id: any){
        let delete_url = this.action_url + this.route_link_company_policy_equiv + '/' + company_policy_equiv_id;
        return this._http.delete(delete_url,  ).map(this.extractData);
    }

    public getCompany(companys_id: any){
        let get_url = this.action_url + this.route_link + '/' + companys_id;
        return this._http.get(get_url, ).map(this.extractData);
    }

    getAttendanceReport(){
        let get_url = this.action_url + 'attendancereport' + '/' + 'graph';

        return this._http.get(get_url ).map(this.extractData);
     }

    public storeCompany(company_model: any){
        let post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(company_model) ).map(this.extractData);
    }

    public getCompanyPolicyTypes(){
        let get_url = this.action_url + this.route_link_company_policy_type;
        return this._http.get(get_url).map(this.extractData);
    }

    public getCompanyPolicyType(id){
        let get_url = this.action_url + this.route_link_company_policy_type + '/' + id;
        return this._http.get(get_url).map(this.extractData);
    }

    public storeCompanyPolicyType(model: any){
        let post_url = this.action_url + this.route_link_company_policy_type;
        return this._http.post(post_url, model).map(this.extractData);
    }

    public updateCompanyPolicyType(id: any, model: any){
        let put_url = this.action_url + this.route_link_company_policy_type + '/' + id;
        return this._http.put(put_url, model).map(this.extractData);
    }

    public updateCompany(id: any, company_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, company_model ).map(this.extractData);
    }

    // public getCompanyBranchesByCompany(company_id: any){
    //     let get_url = this.action_url + 'company_branch?company_id=' + company_id;

    //     return this._http.get(get_url).map( this.extractData );
    // }

    public updateCompanyBranchesByCompany(id: any, company_branches: any){
        /* id is company_id */
        let put_url = this.action_url + this.route_link_company_multi_branch + '/' + id;

        return this._http.put(put_url, company_branches).map(this.extractData);
    }

    public getWorkingSchedule(route: string){
        let get_url = this.action_url + route;
        return this._http.get(get_url, ).map(this.extractData);
    }

    public updateCompanySchedules(route: string, company_id:any, company_working_schedules: any){
        let put_url = this.action_url + route;
        return this._http.put(put_url, JSON.stringify(company_working_schedules), ).map(this.extractData);
    }

    public removeCompanySchedule(delete_route: string, schedule_id: any){
        console.log("schedule archive");
        let delete_url = this.action_url + delete_route + '/' + schedule_id;
        return this._http.delete(delete_url,  ).map(this.extractData);
    }

    public archiveCompany(company_id: any){
        let delete_url = this.action_url + this.route_link + '/' + company_id;
        return this._http.delete(delete_url,  ).map(this.extractData);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    public companies(){

        let action_url = this.action_url + 'company/basic/info';
        return this._http.get(action_url).map(this.extractData);     
    }

    public getCompanyList(id: any,url:any){
        let uri_data = 'id=' + id

        let get_url = this.action_url + 'company/edit?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    }
}