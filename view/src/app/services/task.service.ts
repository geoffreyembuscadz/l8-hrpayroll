import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class TaskService {

       private action_url: string;
    private route_link: string;
    public id:any;

    constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'task';
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    public getTask(model){

        let start = model.start_date;
        let end = model.end_date;
        let employee_id = model.employee_id;
        let role_id = model.role_id;
        let supervisor_id = model.supervisor_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end+ '&employee_id=' + employee_id+ '&role_id=' + role_id+ '&supervisor_id=' + supervisor_id;
        
        let get_url = this.action_url + this.route_link + '/filter/data?'  + uri_data;

        return this._http.get(get_url).map(this.extractData);
    }

    public duplicateEntryValidation(task: any){

        let post_url = this.action_url + this.route_link + '/duplicate_entry_validation';
        return this._http.post(post_url, JSON.stringify(task)).map(this.extractData);
    }

    public createTask(model: any){

        let post_url = this.action_url + this.route_link + '/' + 'create';

        return this._http.post(post_url, JSON.stringify(model)).map(this.extractData);
    }

    public getTaskById(id: any){

        let get_url = this.action_url + this.route_link + '/' + id;

        return this._http.get(get_url).map(this.extractData);
    }

    public updateTask(data: any){

        let post_url = this.action_url + this.route_link + '/update/data';
        return this._http.post(post_url, JSON.stringify(data)).map(this.extractData);
    }

    public getMyTask(model){

        let start = model.start_date;
        let end = model.end_date;
        let employee_id = model.employee_id;

        let uri_data = 'start_date=' + start + '&end_date=' + end+ '&employee_id=' + employee_id;

        let get_url = this.action_url + this.route_link + '/' + 'my_task' + '/' + 'data?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    }

    public updateTaskTime(data: any){

        let post_url = this.action_url + this.route_link + '/update/task/time';
        return this._http.post(post_url, JSON.stringify(data)).map(this.extractData);
    }

    public updateStatus(data: any){

        let post_url = this.action_url + this.route_link + '/update/task/status';
        return this._http.post(post_url, JSON.stringify(data)).map(this.extractData);
    }

    public getMyTaskForDashboard(){

        let get_url = this.action_url + this.route_link + '/' + 'my_task' + '/' + 'dashboard';
        return this._http.get(get_url).map(this.extractData);
    }

    public approvedTask(data: any){

        let post_url = this.action_url + this.route_link + '/approved/status';
        return this._http.post(post_url, JSON.stringify(data)).map(this.extractData);
    }
}
