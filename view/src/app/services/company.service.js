var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var CompanyService = /** @class */ (function () {
    function CompanyService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'company';
        this.route_link_company_branch = 'company_branch';
        this.route_link_company_policy = 'company_policy';
        this.route_link_company_policy_equiv = 'company_policy_equivalent';
        this.route_link_company_policy_type = 'company_policy_type';
        this.route_link_company_policy_update = 'update_company_policy';
        this.route_link_company_multi_branch = 'company_branch/multiple_update';
        this.route_link_company_branches = 'company_branches';
    }
    CompanyService.prototype.getCompanys = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    CompanyService.prototype.getCompanyBranch = function () {
        var get_url = this.action_url + this.route_link_company_branch;
        return this._http.get(get_url).map(this.extractData);
    };
    CompanyService.prototype.getCompanyBranches = function () {
        var get_url = this.action_url + this.route_link_company_branches;
        return this._http.get(get_url).map(this.extractData);
    };
    CompanyService.prototype.setId = function (company_id) {
        this.id = company_id;
    };
    CompanyService.prototype.getId = function () {
        return this.id;
    };
    CompanyService.prototype.setCompanyPolicyTypeId = function (id) {
        this.company_policy_type_id = id;
    };
    CompanyService.prototype.getCompanyPolicies = function (route_company_policy_link) {
        var get_url = this.action_url + route_company_policy_link;
        return this._http.get(get_url).map(this.extractData);
    };
    CompanyService.prototype.getCompanyPolicyTypeId = function () {
        return this.company_policy_type_id;
    };
    CompanyService.prototype.updateCompanyPolicy = function (company_policy_datas) {
        var company_id = company_policy_datas.company_id;
        var post_url = this.action_url + this.route_link_company_policy_update + '/' + company_id;
        return this._http.put(post_url, JSON.stringify(company_policy_datas)).map(this.extractData);
    };
    CompanyService.prototype.deleteCompanyPolicy = function (company_policy_id) {
        var delete_url = this.action_url + this.route_link_company_policy + '/' + company_policy_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    CompanyService.prototype.deleteCompanyPolicyEquivalent = function (company_policy_equiv_id) {
        var delete_url = this.action_url + this.route_link_company_policy_equiv + '/' + company_policy_equiv_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    CompanyService.prototype.getCompany = function (companys_id) {
        var get_url = this.action_url + this.route_link + '/' + companys_id;
        return this._http.get(get_url).map(this.extractData);
    };
    CompanyService.prototype.getAttendanceReport = function () {
        var get_url = this.action_url + 'attendancereport' + '/' + 'graph';
        return this._http.get(get_url).map(this.extractData);
    };
    CompanyService.prototype.storeCompany = function (company_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(company_model)).map(this.extractData);
    };
    CompanyService.prototype.getCompanyPolicyTypes = function () {
        var get_url = this.action_url + this.route_link_company_policy_type;
        return this._http.get(get_url).map(this.extractData);
    };
    CompanyService.prototype.getCompanyPolicyType = function (id) {
        var get_url = this.action_url + this.route_link_company_policy_type + '/' + id;
        return this._http.get(get_url).map(this.extractData);
    };
    CompanyService.prototype.storeCompanyPolicyType = function (model) {
        var post_url = this.action_url + this.route_link_company_policy_type;
        return this._http.post(post_url, model).map(this.extractData);
    };
    CompanyService.prototype.updateCompanyPolicyType = function (id, model) {
        var put_url = this.action_url + this.route_link_company_policy_type + '/' + id;
        return this._http.put(put_url, model).map(this.extractData);
    };
    CompanyService.prototype.updateCompany = function (id, company_model) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, company_model).map(this.extractData);
    };
    // public getCompanyBranchesByCompany(company_id: any){
    //     let get_url = this.action_url + 'company_branch?company_id=' + company_id;
    //     return this._http.get(get_url).map( this.extractData );
    // }
    CompanyService.prototype.updateCompanyBranchesByCompany = function (id, company_branches) {
        /* id is company_id */
        var put_url = this.action_url + this.route_link_company_multi_branch + '/' + id;
        return this._http.put(put_url, company_branches).map(this.extractData);
    };
    CompanyService.prototype.getWorkingSchedule = function (route) {
        var get_url = this.action_url + route;
        return this._http.get(get_url).map(this.extractData);
    };
    CompanyService.prototype.updateCompanySchedules = function (route, company_id, company_working_schedules) {
        var put_url = this.action_url + route;
        return this._http.put(put_url, JSON.stringify(company_working_schedules)).map(this.extractData);
    };
    CompanyService.prototype.removeCompanySchedule = function (delete_route, schedule_id) {
        console.log("schedule archive");
        var delete_url = this.action_url + delete_route + '/' + schedule_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    CompanyService.prototype.archiveCompany = function (company_id) {
        var delete_url = this.action_url + this.route_link + '/' + company_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    CompanyService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    CompanyService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    CompanyService.prototype.companies = function () {
        var action_url = this.action_url + 'company/basic/info';
        return this._http.get(action_url).map(this.extractData);
    };
    CompanyService.prototype.getCompanyList = function (id, url) {
        var uri_data = 'id=' + id;
        var get_url = this.action_url + 'company/edit?' + uri_data;
        return this._http.get(get_url).map(this.extractData);
    };
    CompanyService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], CompanyService);
    return CompanyService;
}());
export { CompanyService };
//# sourceMappingURL=company.service.js.map