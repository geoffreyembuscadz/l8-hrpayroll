import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Menu } from '../model/menu';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class MenuService {
    private action_url: string;
    private route_link: string;

    private menu_id: string;
    public id:any;
    public name: any;



	constructor(private _http: HttpClient, private _conf: Configuration){
		this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'module_menu';
	}


	public getMenus(){
		let get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    }

    
    public getMenu(menu_id: any){
        let get_url = this.action_url + this.route_link + '/' + menu_id;
        return this._http.get(get_url).map(this.extractData);
    }   

    public setId(menu_id: any) {
        this.id = menu_id;
    }

    public getId() {
        return this.id;
    }

    public storeMenu(menu_model: any){
        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(menu_model)).map(this.extractData);
    }

    public updateMenu(id: any, menu_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, menu_model).map(this.extractData);

    }

    public archiveMenu(menu_id: any){
        console.log('postarchive', menu_id);
        let delete_url = this.action_url + this.route_link + '/' + menu_id;
        console.log(delete_url);
        console.log(menu_id);

        return this._http.delete(delete_url).map(this.extractData);
    }


    private extractData(res: Response) {
    	let body = res.json();
    	return body.data || { };
    }

    private handleError (error: Response | any){
    	// In a real world app, we might use a remote logging infrastructure
    	let errMsg: string;
    	if (error instanceof Response) {
    		const body = error.json() || '';
    		const err = body.error || JSON.stringify(body);
    		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    	} else {
    		errMsg = error.message ? error.message : error.toString();
    	}

    	console.error(errMsg);
    	return Observable.throw(errMsg);
    }

}