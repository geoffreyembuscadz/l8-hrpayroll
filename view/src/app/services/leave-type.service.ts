import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Leave_Type } from '../model/leave_type';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';

@Injectable()
export class LeaveTypeService {
    private action_url: string;
    private route_link: string;

    private leave_type_id: string;
    public id:any;


    constructor(private _http: HttpClient, private _conf: Configuration){
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'leave_type';
    
    }
    public getLeaves(){
        let get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    }
    public getLeaveType(leaves_id: any){
        let get_url = this.action_url + this.route_link + '/' + leaves_id;
        return this._http.get(get_url).map(this.extractData);
    }

    public storeLeaveType(leave_type_model: any){
        let post_url = this.action_url + this.route_link;

        return this._http.post(post_url, JSON.stringify(leave_type_model), ).map(this.extractData);
    }

    public updateLeaveType(id: any, leave_type_model: any){
        let put_url = this.action_url + this.route_link + '/' + id;

        return this._http.put(put_url, leave_type_model, ).map(this.extractData);

    }
    public archiveLeaveType(leave_type_id: any){
        let delete_url = this.action_url + this.route_link + '/' + leave_type_id;
        return this._http.delete(delete_url).map(this.extractData);
    }


    private extractData(res: Response) {
        let body = res.json();
        return body.data || { };
    }

    private handleError (error: Response | any){
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        console.error(errMsg);
        return Observable.throw(errMsg);
  }



}