var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
//import { Configuration } from '../app.config';
var EmployeeService = /** @class */ (function () {
    function EmployeeService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.route_attachment = 'employee_attachment';
        // Static Selection Options
        this.name_suffixes = [
            { name: '-none-' },
            { name: 'Jr.' },
            { name: 'Sr.' },
            { name: 'III' },
            { name: 'IV' }
        ];
        this.genders = [
            { name: 'Male' },
            { name: 'Female' }
        ];
        this.blood_types = [
            { name: 'A' },
            { name: 'AB' },
            { name: 'O' }
        ];
        this.employee_marital_statuses = [
            { name: 'single' },
            { name: 'married' }
            // additional marital statuses: http://birtaxcalculator.com/calculator.php
        ];
        this.employee_contact_statuses = [
            { code: 'contractual', name: 'Contractual' },
            { code: 'full-time', name: 'Full-time' },
            { code: 'part-time', name: 'Part-time' },
            { code: 'provisionary', name: 'Provisionary' },
            { code: 'regular', name: 'Regular' },
            { code: 'resigned', name: 'Resigned' },
            { code: 'awol', name: 'AWOL' }
        ];
        this.employee_types = [
            { code: 'manager', name: 'Manager' },
            { code: 'officer', name: 'Officer' },
            { code: 'rank_and_file', name: 'Rank & File' }
        ];
        this.employee_income_type = [
            { code: 'daily', name: 'Daily' },
            { code: 'weekly', name: 'Weekly' },
            { code: 'monthly', name: 'Monthly' }
        ];
        this.employee_residency_type = [
            { name: '' },
            { name: 'Rented' },
            { name: 'Owned' },
            { name: 'Others' }
        ];
        this.employee_departments = [];
        this.employee_positions = [];
        this.employee_genders = [
            { name: '' },
            { name: 'Female' },
            { name: 'Male' }
        ];
        this.employee_skills = [
            { name: 'MS Office' },
            { name: 'Photoshop' },
            { name: 'PC Troubleshooting' },
            { name: 'Customer Caring' }
        ];
        this.employee_dependency_relationship = [
            { value: 'others', label: 'Others' },
            { value: 'father', label: 'Father' },
            { value: 'mother', label: 'Mother' },
            { value: 'sibling', label: 'Sibling' },
            { value: 'grandparent', label: 'Grandparents' },
            { value: 'familyrelative', label: 'Family Relative' }
        ];
        this.banks = [
            { 'name': '', 'code': '' },
            { 'name': 'ALLIED BANK', 'code': '10320013' },
            { 'name': 'ASIA UNITED BANK', 'code': '11020011' },
            { 'name': 'ANZ BANK', 'code': '10700015' },
            { 'name': 'BANCO DE ORO', 'code': '10530667' },
            { 'name': 'BANGKO SENTRAL NG PILIPINAS', 'code': '10030015' },
            { 'name': 'BANGKOK BANK', 'code': '10670019' },
            { 'name': 'BANK OF AMERICA', 'code': '10120019' },
            { 'name': 'BANK OF CHINA', 'code': '11140014' },
            { 'name': 'COMBANK', 'code': '10440016' },
            { 'name': 'BPI', 'code': '10040018' },
            { 'name': 'BANK OF TOKYO', 'code': '10460012' },
            { 'name': 'CHINA BANK', 'code': '10100013' },
            { 'name': 'CHINA TRUST', 'code': '10690015' },
            { 'name': 'CITIBANK', 'code': '10070017' },
            { 'name': 'DEUTSCHE', 'code': '10650013' },
            { 'name': 'DBP', 'code': '10590018' },
            { 'name': 'EASTWEST BANK', 'code': '10620014' },
            { 'name': 'EXPORT', 'code': '10860010' },
            { 'name': 'HONGKONG BANK', 'code': '10060014' },
            { 'name': 'JP MORGAN', 'code': '10720011' },
            { 'name': 'KOREA EXCH BANK', 'code': '10710018' },
            { 'name': 'LAND BANK', 'code': '10350025' },
            { 'name': 'MAYBANK', 'code': '10220016' },
            { 'name': 'MEGA INTL COMML BANK OF CHINA', 'code': '10560019' },
            { 'name': 'METROBANK', 'code': '10269996' },
            { 'name': 'MIZUHO CORPORATE BANK', 'code': '10640010' },
            { 'name': 'PB COM', 'code': '10110016' },
            { 'name': 'PNB', 'code': '10080010' },
            { 'name': 'PHILTRUST', 'code': '10090039' },
            { 'name': 'VETERANS BANK', 'code': '10330016' },
            { 'name': 'RCBC', 'code': '10280014' },
            { 'name': 'ROYAL BANK OF SCOTLAND (PHILS)', 'code': '10770074' },
            { 'name': 'SECURITY BANK', 'code': '10140015' },
            { 'name': 'CHARTERED BANK', 'code': '10050011' },
            { 'name': 'UNION BANK', 'code': '10419995' },
            { 'name': 'COCOBANK', 'code': '10299995' },
            { 'name': 'UNITED OVERSEAS BANK', 'code': '10270341' }
        ];
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'employee';
        this.route_upload = 'employee_upload';
        this.route_export = 'employee_export';
        this.route_mass_upload_form = 'employee_download_upload_form';
        // Header Variables
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Authorization', "Bearer " + authToken);
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        //console.log(authToken);
    }
    EmployeeService.prototype.getBanks = function () {
        return this.banks;
    };
    EmployeeService.prototype.getGenders = function () {
        return this.employee_genders;
    };
    EmployeeService.prototype.getSuffixes = function () {
        return this.name_suffixes;
    };
    EmployeeService.prototype.getBloodTypes = function () {
        return this.blood_types;
    };
    EmployeeService.prototype.getEmploymentStatus = function () {
        return this.employee_contact_statuses;
    };
    EmployeeService.prototype.getEmployeeSkills = function () {
        return this.employee_skills;
    };
    EmployeeService.prototype.getEmployeeMaritalStatuses = function () {
        return this.employee_marital_statuses;
    };
    EmployeeService.prototype.getIncomeType = function () {
        return this.employee_income_type;
    };
    EmployeeService.prototype.getResidencyType = function () {
        return this.employee_residency_type;
    };
    EmployeeService.prototype.getEmployeeDependencyRelationships = function () {
        return this.employee_dependency_relationship;
    };
    EmployeeService.prototype.getDepartments = function () {
        var department_url = this.action_url + 'department';
        return this._http.get(department_url, { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.getJobTitles = function () {
        var positions_url = this.action_url + 'position';
        return this._http.get(positions_url, { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.getEmployees = function (uri_data) {
        var get_url = this.action_url + this.route_link + '?' + uri_data;
        return this._http.get(get_url, { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.getEmployee = function (employee_id) {
        var get_url = this.action_url + this.route_link + '/' + employee_id;
        return this._http.get(get_url, { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.getCompanies = function () {
        var route_company = 'company';
        var get_url = this.action_url + route_company;
        return this._http.get(get_url, { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.storeEmployee = function (employee_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(employee_model), { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.uploadMassEmployeeCsv = function (data) {
        var post_url = this.action_url + this.route_upload;
        return this._http.post(post_url, JSON.stringify(data), { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.addEmployeeRemark = function (employee_remarks_model) {
        var post_url = this.action_url + 'employee_remark';
        return this._http.post(post_url, JSON.stringify(employee_remarks_model), { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.getAttachments = function (employee_id) {
        var get_attachment_url = this.action_url + this.route_attachment + '?employee_id=' + employee_id;
        return this._http.get(get_attachment_url, { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.attachEmployeeFiles = function (employee_id, employee_attachment, employee_attachment_dir) {
        var route_upload_attachment = this.route_attachment;
        var post_url = this.action_url + route_upload_attachment;
        var employee_pass_data = {
            employee_id: employee_id,
            file_url: employee_attachment,
            file_dir: employee_attachment_dir
        };
        return this._http.post(post_url, JSON.stringify(employee_pass_data), { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.updateEmployee = function (id, employee_model) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, employee_model, { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.updateScheduleMaster = function (id, schedule_master_model) {
        var put_url = this.action_url + 'employee_schedule_master' + '/' + id;
        return this._http.put(put_url, schedule_master_model, { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.exportEmployeeList = function (employee_form) {
        var url = this.action_url + this.route_export;
        var params = new URLSearchParams();
        for (var key in employee_form) {
            params.set(key, employee_form[key]);
        }
        window.location.href = url + '?' + params.toString();
        // return this._http.post(url , employee_form, { headers:this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.exportEmployeeMassUploadform = function (company, company_branch, selected_fields) {
        var selected_company = company;
        var selected_company_branch = company_branch;
        var selected_fields_for_upload = encodeURIComponent(JSON.stringify(selected_fields));
        var url_export = this.action_url + this.route_mass_upload_form + '?company_id=' + selected_company + '&branch_id=' + selected_company_branch + '&fields=' + selected_fields;
        window.location.href = url_export;
    };
    EmployeeService.prototype.getMasterSchedules = function () {
        var get_url = this.action_url + 'employee_schedule_master';
        return this._http.get(get_url, { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.getMasterSchedule = function (id) {
        var get_url = this.action_url + 'employee_schedule_master/' + id;
        return this._http.get(get_url, { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.updateCompanyMasterSchedules = function (employee_master_schedule) {
        var post_url = this.action_url + 'employee_schedule_master';
        return this._http.post(post_url, JSON.stringify(employee_master_schedule), { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    EmployeeService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    };
    EmployeeService.prototype.archiveEmployee = function (employee_id) {
        console.log('postarchive', employee_id);
        var delete_url = this.action_url + this.route_link + '/' + employee_id;
        console.log(delete_url);
        console.log(employee_id);
        return this._http.delete(delete_url, { headers: this.headers }).map(this.extractData);
    };
    EmployeeService.prototype.deleteEmployeAttachment = function (attachment_id) {
        // Delete Employee File Attached
        var delete_url = this.action_url + this.route_attachment + '/' + attachment_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    EmployeeService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http, Configuration])
    ], EmployeeService);
    return EmployeeService;
}());
export { EmployeeService };
//# sourceMappingURL=employee.service.js.map