var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var RankTypeService = /** @class */ (function () {
    function RankTypeService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'rank_type';
    }
    RankTypeService.prototype.getRanks = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    RankTypeService.prototype.setId = function (Rank_type_id) {
        this.id = Rank_type_id;
        console.log('setId: ', Rank_type_id);
    };
    RankTypeService.prototype.getId = function () {
        return this.id;
    };
    RankTypeService.prototype.getRankType = function (ranks_id) {
        var get_url = this.action_url + this.route_link + '/' + ranks_id;
        return this._http.get(get_url).map(this.extractData);
    };
    RankTypeService.prototype.storeRankType = function (Rank_type_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(Rank_type_model)).map(this.extractData);
    };
    RankTypeService.prototype.updateRankType = function (id, Rank_type_model) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, Rank_type_model).map(this.extractData);
    };
    RankTypeService.prototype.archiveRankType = function (Rank_type_id) {
        console.log('postarchive', Rank_type_id);
        var delete_url = this.action_url + this.route_link + '/' + Rank_type_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    RankTypeService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    RankTypeService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    RankTypeService.prototype.getRankTypes = function () {
        var action_url = this.action_url + 'show/rank_type';
        return this._http.get(action_url).map(this.extractData);
    };
    RankTypeService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], RankTypeService);
    return RankTypeService;
}());
export { RankTypeService };
//# sourceMappingURL=rank-type.service.js.map