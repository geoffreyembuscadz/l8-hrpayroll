var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var QuestionService = /** @class */ (function () {
    function QuestionService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'question';
    }
    QuestionService.prototype.getQuestions = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    QuestionService.prototype.setId = function (question_id) {
        this.id = question_id;
    };
    QuestionService.prototype.getId = function () {
        return this.id;
    };
    QuestionService.prototype.getQuestion = function (questions_id) {
        var get_url = this.action_url + this.route_link + '/' + questions_id;
        return this._http.get(get_url).map(this.extractData);
    };
    QuestionService.prototype.storeQuestion = function (question_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(question_model)).map(this.extractData);
    };
    QuestionService.prototype.updateQuestion = function (id, question_model) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, question_model).map(this.extractData);
    };
    QuestionService.prototype.archiveQuestion = function (question_id) {
        var delete_url = this.action_url + this.route_link + '/' + question_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    QuestionService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    QuestionService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    QuestionService.prototype.questionList = function () {
        var action_url = this.action_url + 'question/list';
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.storeEvaluation = function (evaluation_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(evaluation_model)).map(this.extractData);
    };
    QuestionService.prototype.getEmpByStatus = function (emp_stat) {
        var employment_status = emp_stat;
        var uri_data = 'emp_stat=' + emp_stat;
        var action_url = this.action_url + 'employment/status?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.getEmployeeStatus = function () {
        var action_url = this.action_url + 'employee/type/status';
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.rating = function () {
        var action_url = this.action_url + 'show/rating';
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.addSegment = function () {
        var action_url = this.action_url + 'add/segment';
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.addCategory = function () {
        var action_url = this.action_url + 'add/category';
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.addSubCategory = function () {
        var action_url = this.action_url + 'add/subcategory';
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.addPosition = function () {
        var action_url = this.action_url + 'get/emp/position';
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.storeEvaluationTemplate = function (evaluation_model) {
        var post_url = this.action_url + 'evaluation/template';
        return this._http.post(post_url, JSON.stringify(evaluation_model)).map(this.extractData);
    };
    QuestionService.prototype.getEvaluationData = function (model) {
        var employee_position = model.position;
        var id = model.id;
        var uri_data = 'position=' + employee_position + '&id=' + id;
        var action_url = this.action_url + 'get/evaluation?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.getEvaluationTitle = function (position) {
        var position_id = position;
        var uri_data = 'position=' + position_id;
        var action_url = this.action_url + 'show/title?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.getEmployeeByPosition = function (position) {
        var position_id = position;
        var uri_data = 'position=' + position_id;
        var action_url = this.action_url + 'show/emp/by/pos?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.postEvaluationForm = function (evaluation_model) {
        var post_url = this.action_url + 'evaluation/form';
        return this._http.post(post_url, JSON.stringify(evaluation_model)).map(this.extractData);
    };
    QuestionService.prototype.getEmployeeEvaluation = function (model) {
        var id = model.id;
        var uri_data = 'id=' + id;
        var action_url = this.action_url + 'get/emp/evaluation?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.createEvaluationForm = function (evaluation_model) {
        var post_url = this.action_url + 'create/evaluation/form';
        return this._http.post(post_url, JSON.stringify(evaluation_model)).map(this.extractData);
    };
    QuestionService.prototype.showAllEmployee = function () {
        var action_url = this.action_url + 'show/all/employee';
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.showEmployeeRate = function () {
        var action_url = this.action_url + 'show/emp/rate';
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.filterEmployee = function (model) {
        var type = model.type;
        var uri_data = 'type=' + type;
        var action_url = this.action_url + 'filter/employee?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.viewEmployeeEvaluation = function (model) {
        var id = model.id;
        var uri_data = 'id=' + id;
        var action_url = this.action_url + 'view/employee/rate?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService.prototype.getEmployee = function (model) {
        var supervisor_id = model.supervisor_id;
        var uri_data = 'supervisor_id=' + supervisor_id;
        var action_url = this.action_url + 'show/emp/under/sup?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    QuestionService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], QuestionService);
    return QuestionService;
}());
export { QuestionService };
//# sourceMappingURL=question.service.js.map