var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
import { HttpClient } from './http-extended.service';
var DevelopmentService = /** @class */ (function () {
    function DevelopmentService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'development';
    }
    DevelopmentService.prototype.getDevelopments = function () {
        var get_url = this.action_url + this.route_link;
        return this._http.get(get_url).map(this.extractData);
    };
    DevelopmentService.prototype.setId = function (development_id) {
        this.id = development_id;
        console.log('setId: ', development_id);
    };
    DevelopmentService.prototype.getId = function () {
        return this.id;
    };
    DevelopmentService.prototype.getDevelopment = function (developments_id) {
        var get_url = this.action_url + this.route_link + '/' + developments_id;
        return this._http.get(get_url).map(this.extractData);
    };
    DevelopmentService.prototype.storeDevelopment = function (development_model) {
        var post_url = this.action_url + this.route_link;
        return this._http.post(post_url, JSON.stringify(development_model)).map(this.extractData);
    };
    DevelopmentService.prototype.updateDevelopment = function (id, development_model) {
        var put_url = this.action_url + this.route_link + '/' + id;
        return this._http.put(put_url, development_model).map(this.extractData);
    };
    DevelopmentService.prototype.archiveDevelopment = function (development_id) {
        console.log('postarchive', development_id);
        var delete_url = this.action_url + this.route_link + '/' + development_id;
        return this._http.delete(delete_url).map(this.extractData);
    };
    DevelopmentService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    DevelopmentService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    DevelopmentService.prototype.showDevelopment = function (model) {
        var employee_id = model.employee_id;
        var uri_data = 'employee_id=' + employee_id;
        var action_url = this.action_url + 'show/development?' + uri_data;
        return this._http.get(action_url).map(this.extractData);
    };
    DevelopmentService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Configuration])
    ], DevelopmentService);
    return DevelopmentService;
}());
export { DevelopmentService };
//# sourceMappingURL=development.service.js.map