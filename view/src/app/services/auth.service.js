var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Configuration } from '../app.config';
var AuthService = /** @class */ (function () {
    function AuthService(_http, router, _conf) {
        this._http = _http;
        this.router = router;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'auth/login';
        var authToken = localStorage.getItem('id_token');
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', "Bearer " + authToken);
    }
    AuthService.prototype.login = function (login_model) {
        var login_url = this.action_url + this.route_link;
        return this._http
            .post(login_url, JSON.stringify(login_model), { headers: this.headers })
            .map(function (res) { return res.json(); })
            .map(function (res) {
            if (res.status) {
                localStorage.setItem('id_token', res.data.token);
            }
            return res.status;
        });
    };
    AuthService.prototype.isLoggedIn = function () {
        var authToken = localStorage.getItem('id_token');
        if (authToken == '' || authToken == null || authToken === 'undefined' || this.token_valid == true) {
            localStorage.removeItem('id_token');
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    };
    AuthService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http, Router, Configuration])
    ], AuthService);
    return AuthService;
}());
export { AuthService };
//# sourceMappingURL=auth.service.js.map