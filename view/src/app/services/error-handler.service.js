var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { XHRBackend, RequestOptions, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
var ExtendedHttpService = /** @class */ (function (_super) {
    __extends(ExtendedHttpService, _super);
    function ExtendedHttpService(backend, defaultOptions, router) {
        var _this = _super.call(this, backend, defaultOptions) || this;
        _this.router = router;
        _this.body = document.getElementsByTagName('body')[0];
        return _this;
    }
    ExtendedHttpService.prototype.request = function (url, options) {
        return _super.prototype.request.call(this, url, options).catch(this.catchErrors());
    };
    ExtendedHttpService.prototype.catchErrors = function () {
        var _this = this;
        return function (res) {
            if (res.status === 401) {
                var response = res.json();
                if (response.message != 'invalid_credentials') {
                    localStorage.removeItem('id_token');
                    _this.router.navigate(['/login']);
                    location.reload();
                }
                else if (response.message == 'invalid_credentials') {
                    localStorage.removeItem('id_token');
                    _this.router.navigate(['/login']);
                }
            }
            else if (res.status === 403) {
                _this.router.navigate(['/404']);
            }
            else if (res.status === 404) {
                _this.router.navigate(['/admin']);
            }
            return Observable.throw(res);
        };
    };
    ExtendedHttpService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [XHRBackend, RequestOptions, Router])
    ], ExtendedHttpService);
    return ExtendedHttpService;
}(Http));
export { ExtendedHttpService };
//# sourceMappingURL=error-handler.service.js.map