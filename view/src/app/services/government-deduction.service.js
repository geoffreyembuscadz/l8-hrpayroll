var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Configuration } from '../app.config';
var GovernmentDeductionService = /** @class */ (function () {
    function GovernmentDeductionService(_http, _conf) {
        this._http = _http;
        this._conf = _conf;
        this.action_url = this._conf.ServerWithApiUrl;
        this.route_link = 'government_deduction';
        this.route_link_emp_share = 'government-deduction';
        this.headers = new Headers();
        var authToken = localStorage.getItem('id_token');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
        this.headers.append('Authorization', "Bearer " + authToken);
    }
    GovernmentDeductionService.prototype.getEmployeeShare = function () {
        var get_url = this.action_url + this.route_link_emp_share;
        return this._http.get(get_url, { headers: this.headers }).map(this.extractData);
    };
    GovernmentDeductionService.prototype.getEE = function () {
        var get_url = this.action_url + this.route_link_emp_share;
        return this._http.get(get_url, { headers: this.headers }).map(this.extractData);
    };
    GovernmentDeductionService.prototype.computeDeduction = function (basicPay) {
        var get_url = this.action_url + this.route_link + '/' + basicPay;
        return this._http.get(get_url, { headers: this.headers }).map(this.extractData);
    };
    GovernmentDeductionService.prototype.extractData = function (res) {
        var body = res.json();
        return body.data || {};
    };
    GovernmentDeductionService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    GovernmentDeductionService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http, Configuration])
    ], GovernmentDeductionService);
    return GovernmentDeductionService;
}());
export { GovernmentDeductionService };
//# sourceMappingURL=government-deduction.service.js.map