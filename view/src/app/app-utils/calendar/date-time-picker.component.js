var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { getSeconds, getMinutes, getHours, getDate, getMonth, getYear, setSeconds, setMinutes, setHours, setDate, setMonth, setYear } from 'date-fns';
var DateTimePickerComponent = /** @class */ (function () {
    function DateTimePickerComponent() {
        this.dateChange = new EventEmitter();
    }
    DateTimePickerComponent.prototype.ngOnChanges = function (changes) {
        if (changes['date']) {
            this.dateStruct = {
                day: getDate(this.date),
                month: getMonth(this.date) + 1,
                year: getYear(this.date)
            };
            this.timeStruct = {
                second: getSeconds(this.date),
                minute: getMinutes(this.date),
                hour: getHours(this.date)
            };
        }
    };
    DateTimePickerComponent.prototype.updateDate = function () {
        var newDate = setYear(setMonth(setDate(this.date, this.dateStruct.day), this.dateStruct.month), this.dateStruct.year);
        this.dateChange.next(newDate);
    };
    DateTimePickerComponent.prototype.updateTime = function () {
        var newDate = setHours(setMinutes(setSeconds(this.date, this.timeStruct.second), this.timeStruct.minute), this.timeStruct.hour);
        this.dateChange.next(newDate);
    };
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], DateTimePickerComponent.prototype, "placeholder", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Date)
    ], DateTimePickerComponent.prototype, "date", void 0);
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], DateTimePickerComponent.prototype, "dateChange", void 0);
    DateTimePickerComponent = __decorate([
        Component({
            selector: 'mwl-date-time-picker',
            template: "\n    <form class=\"form-inline\">\n      <div class=\"form-group\">\n        <div class=\"input-group\">\n          <input\n            readonly\n            class=\"form-control\"\n            [placeholder]=\"placeholder\"\n            name=\"date\"\n            [(ngModel)]=\"dateStruct\"\n            (ngModelChange)=\"updateDate()\"\n            ngbDatepicker\n            #datePicker=\"ngbDatepicker\">\n            <div class=\"input-group-addon\" (click)=\"datePicker.toggle()\" >\n              <i class=\"fa fa-calendar\"></i>\n            </div>\n        </div>\n      </div>\n    </form>\n    <ngb-timepicker [(ngModel)]=\"timeStruct\" (ngModelChange)=\"updateTime()\" [meridian]=\"true\"></ngb-timepicker>\n  ",
            styles: ["\n    .form-group {\n      width: 100%;\n    }\n  "]
        })
    ], DateTimePickerComponent);
    return DateTimePickerComponent;
}());
export { DateTimePickerComponent };
//# sourceMappingURL=date-time-picker.component.js.map