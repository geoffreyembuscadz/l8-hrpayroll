var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbDatepickerModule, NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule } from 'angular-calendar';
import { CalendarHeaderComponent } from './calendar/calendar-header.component';
import { DateTimePickerComponent } from './calendar/date-time-picker.component';
var AppUtilsModule = /** @class */ (function () {
    function AppUtilsModule() {
    }
    AppUtilsModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                NgbDatepickerModule.forRoot(),
                NgbTimepickerModule.forRoot(),
                CalendarModule
            ],
            declarations: [
                CalendarHeaderComponent,
                DateTimePickerComponent
            ],
            exports: [
                CalendarHeaderComponent,
                DateTimePickerComponent
            ]
        })
    ], AppUtilsModule);
    return AppUtilsModule;
}());
export { AppUtilsModule };
//# sourceMappingURL=module.js.map