// Login Model

export class PasswordReset {
	email: string;
	password: string;
	confirm_password:string;
	token: string;
}