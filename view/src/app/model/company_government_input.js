var CompanyGovernmentInput = /** @class */ (function () {
    function CompanyGovernmentInput() {
        this.id = [];
        this.company_id = [];
        this.code = [];
        this.active = [];
        this.tax_term = [];
        this.first_cutoff = [];
        this.second_cutoff = [];
        this.default_value = [];
        this.is_default = [];
        this.min_days = [];
    }
    return CompanyGovernmentInput;
}());
export { CompanyGovernmentInput };
//# sourceMappingURL=company_government_input.js.map