// Company Policy Model
var CompanyBranch = /** @class */ (function () {
    function CompanyBranch() {
        this.id = [];
        this.branch_name = [];
        this.branch_address = [];
        this.contact_number = [];
        this.contact_person = [];
    }
    return CompanyBranch;
}());
export { CompanyBranch };
//# sourceMappingURL=company_branch.js.map