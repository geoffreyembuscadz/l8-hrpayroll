// Company Policy Model

export class CompanyPolicy {
	company_id: number;
	default_payroll_period: any;
	attendance_default: any;
	billing_breakdown: any;
	undertime_rule: number;
	billing_rate: number;
	billing_asf: number;
	billing_vat: number;
	billing_13th: number;
	night_shift: number;
	cola_amount: number;
	flexi_late: number;
	late_per_hour: number;
	

	// Policy Records
	policies = {
		id: [],
		company_policy: [],
		company_policy_type_id: [],
		format_value: [],
		default_value: [],
		start_value: [],
		end_value: []
	};

	cutoff: {
		date_start: any,
		date_end: any,
		days_before_cutoff: number
	};
	
	// id = [];
	// company_policy = [];
	// company_policy_type_id = [];
	// format_value = [];
	// default_value = [];
	// start_value = [];
	// end_value = [];
}
