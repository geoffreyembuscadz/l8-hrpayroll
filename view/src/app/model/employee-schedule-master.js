var EmployeeScheduleMaster = /** @class */ (function () {
    function EmployeeScheduleMaster() {
        this.employee_master_schedule = {
            employee_id: [],
            company_id: [],
            schedule_date: [],
            timein: [],
            timeout: [],
            breakin: [],
            breakout: [],
            lunch_mins_break: []
        };
    }
    return EmployeeScheduleMaster;
}());
export { EmployeeScheduleMaster };
//# sourceMappingURL=employee-schedule-master.js.map