// Permission Model

export class Permission {
	name: string;
	display_name: string;
	description: string;
	parent_id: string;
}