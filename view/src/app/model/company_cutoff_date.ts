// Company Policy Model

export class CompanyCutOffDate {
	id: any;
	days_before_notif: number;

	first_cutoff_date: any;
	second_cutoff_date: any;

	first_cutoff_first_date: any;
	first_cutoff_second_date: any;
	second_cutoff_first_date: any;
	second_cutoff_second_date: any;
}
