
export class EmployeeDynastyLoan {
	id : number;
	employee_id : any;
	company_id : any;
	employee_fullname: any;
	employee_resident_type: any;

	amount_applied : any;
	term_in_months  : any;
	purpose_of_loan : any;
	type_of_loan  : any;
	payback_mode  : any;
	co_maker_name  : any;
	co_maker_contact_number  : any;
	co_maker_client_assignment  : any;
	co_maker_job_title : any;
	created_at : any;
	created_by : any;
	updated_at : any;
	updated_by : any;
	deleted_at : any;
	deleted_by: any;
}
