export class Payroll {
	id: string;
	name: string;
	start_time: string;
	end_time: string;
	type_of_run: string;
	prepared_by: string;

}