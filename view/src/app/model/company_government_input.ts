
export class CompanyGovernmentInput
{
	id = [];
	company_id = [];
	code = [];
	active = [];
	tax_term= [];
	first_cutoff= [];
	second_cutoff= [];
	default_value = [];
	is_default = [];
	min_days= [];
}
