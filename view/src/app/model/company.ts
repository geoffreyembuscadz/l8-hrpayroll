// Company Model


export class Company {
	id: number;
	name: string;
	email: string;
	address: string;
	phone_number: number;
	contact_person: string;

	branches: any;

	admin_fee: any;
	flexi_late: any;
	late_per_hour: any;
	night_shift: any;
	holiday_rule: any;
	is_admin_fee: any;
	approver_name: any;
	overtime_rule: any;
	undertime_rule: any;
	attendance_default:any;
	late_convertion: any;
	fixed_halfday: any;
	holiday_allowance: any;
	overtime_allowance: any;
	thirteen_month_rule: any;
	default_trainee_rate: any;

}