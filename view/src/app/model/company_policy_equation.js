// Company Policy Equation Model
var CompanyPolicyEquation = /** @class */ (function () {
    function CompanyPolicyEquation() {
        // Policy Records
        this.policy_equations = {
            id: [],
            company_id: [],
            name: [],
            description: [],
            value: [],
            value_unit: [],
            equivalent_value: [],
            equivalent_unit: []
        };
    }
    return CompanyPolicyEquation;
}());
export { CompanyPolicyEquation };
//# sourceMappingURL=company_policy_equation.js.map