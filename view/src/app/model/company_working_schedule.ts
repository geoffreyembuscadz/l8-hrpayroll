// Company Policy Model

export class CompanyWorkingSchedule {
	company_id: number;

	// Policy Records
	schedules = {
		id: [],
		name: [],
		flexible_time: [],
		grace_period_mins: [],
		lunch_break_time: [],
		lunch_mins_break: [],
		time_in: [],
		time_out: [],
		days: []
	};
	
	// id = [];
	// company_policy = [];
	// company_policy_type_id = [];
	// format_value = [];
	// default_value = [];
	// start_value = [];
	// end_value = [];
}
