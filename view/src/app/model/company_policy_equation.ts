// Company Policy Equation Model

export class CompanyPolicyEquation {
	company_id: number;

	// Policy Records
	policy_equations = {
		id: [],
		company_id: [],
		name: [],
		description: [],

		value: [],
		value_unit: [],
		equivalent_value: [],
		equivalent_unit: []
	};
}
