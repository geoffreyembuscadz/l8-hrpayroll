// Role Model

export class Role {
	name: string;
	display_name: string;
	description: string;
}