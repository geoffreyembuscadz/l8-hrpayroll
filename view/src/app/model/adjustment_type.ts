export class AdjustmentType {
	name: string;
	display_name: string;
	description: string;
	type: string;
	taxable: string;
	billable: string;
	frequency: string;
}