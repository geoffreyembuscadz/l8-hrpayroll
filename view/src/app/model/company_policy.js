// Company Policy Model
var CompanyPolicy = /** @class */ (function () {
    function CompanyPolicy() {
        // Policy Records
        this.policies = {
            id: [],
            company_policy: [],
            company_policy_type_id: [],
            format_value: [],
            default_value: [],
            start_value: [],
            end_value: []
        };
        // id = [];
        // company_policy = [];
        // company_policy_type_id = [];
        // format_value = [];
        // default_value = [];
        // start_value = [];
        // end_value = [];
    }
    return CompanyPolicy;
}());
export { CompanyPolicy };
//# sourceMappingURL=company_policy.js.map