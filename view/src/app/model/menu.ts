// Role Model

export class Menu {
	name: string;
	display_name: string;
	description: string;
	parent_id: string;
	level: string;
	action_url: string;
}