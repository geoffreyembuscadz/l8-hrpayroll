export class EmployeeMassUploadForm {
	company: string;
	company_branch: string;
	mass_upload_fields: any;
}