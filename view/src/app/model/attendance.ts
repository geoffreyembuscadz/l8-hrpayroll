export class Attendance {
	id: number;
	employees_id: number;
	type: string;
	time_in: string;
	time_out: string;
}