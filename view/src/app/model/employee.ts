// Employee Model

export class Employee {
	id: number;
	skills: any;
	email: string;
	tel_no: string;
	cel_no: string;
	suffix: string;
	gender: string;
	sss: string;
	tin_no: string;
	hdmf_no: string;
	attachments: any;
	lastname: string;
	position: string;
	birthdate: string;
	firstname: string;
	job_title: string;
	middlename: string;
	blood_type: string;
	department: string;
	pagibig_no: string;
	nationality: string;
	citizenship: string;
	marital_status: any;
	philhealth_no: string;
	employee_code: string;
	supervisor_id: any;
	employee_image: string;
	emergency_contacts: any;
	current_address: string;
	province_address:string;
	primary_company: number;
	company_branch: any;
	employment_status: string;
	driving_license_no: string;
	salary_receiving_type: string;
	income_rate: number;
	allowance_amount: number;
	working_schedule: any;
	date_terminated: any;
	starting_date: any;
	ending_date: any;
	e_cola: number;
	permanent_address: any;

	bank_code: any;
	bank_name: any;
	bank_account_number: any
	
	sick_leave_credit: any;
	use_sick_leave_credit: any;

	vacation_leave_credit: any;
	use_vacation_leave_credit: any;
	
	emergency_leave_credit: any;
	use_emergency_leave_credit: any;

	maternity_leave_credit: any;
	use_maternity_leave_credit: any;

	others_leave_credit: any;
	use_others_leave_credit: any;

	employee_dependents: any;
	ip_address: any;
	mac_address: any;
	ddns: any;
}
