export class LoanType {
	name: string;
	display_name: string;
	description: string;
	type: string;
	include_payslip: string;
}