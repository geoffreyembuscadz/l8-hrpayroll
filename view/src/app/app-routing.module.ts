import { NgModule }     from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { _404Component } from './404/404.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './services/auth-guard.service';
import { AttendanceComponent } from './attendance/attendance.component';
import { PasswordResetComponent } from './login/password-reset.component';
import { Dashboard1Component } from './admin/dashboard1/dashboard1.component';

const APP_ROUTES : Routes = [
  { path: '', redirectTo: '/admin', pathMatch: 'full'},
  { path: 'admin', component: Dashboard1Component, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'password/reset/:token', component: PasswordResetComponent },
  { path: '404', component: _404Component },
  { path: 'auth',  redirectTo: '/login'},
  { path: '**', redirectTo: '/404'},
  { path: '#', redirectTo: '/admin'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(APP_ROUTES)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
