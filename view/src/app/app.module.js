var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';
import { CalendarModule } from 'angular-calendar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragAndDropModule } from 'angular-draggable-droppable';
import { AppUtilsModule } from './app-utils/module';
import { AppComponent } from './app.component';
import { AdminModule } from './admin/admin.module';
import { Configuration } from './app.config';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { PasswordResetComponent } from './login/password-reset.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { AuthService } from './services/auth.service';
import { PasswordResetService } from './services/password-reset.service';
import { _404Component } from './404/404.component';
import { AuthUserService } from './services/auth-user.service';
import { MainSideService } from './services/main-side.service';
import { CompanyModalComponent } from './admin/company/company-modal/company-modal.component';
// import { CalendarComponent } from "angular2-fullcalendar/src/calendar/calendar";  
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                AppComponent,
                LoginComponent,
                _404Component,
                AttendanceComponent,
                PasswordResetComponent,
                CompanyModalComponent
                // CalendarComponent
            ],
            imports: [
                BrowserModule,
                FormsModule,
                NgbModule.forRoot(),
                CalendarModule.forRoot(),
                AppUtilsModule,
                DragAndDropModule,
                HttpModule,
                JsonpModule,
                AdminModule,
                AppRoutingModule,
                ReactiveFormsModule,
            ],
            providers: [Configuration, AuthService, PasswordResetService, AuthUserService, MainSideService],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map