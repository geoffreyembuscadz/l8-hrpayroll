var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import 'rxjs/add/operator/catch';
import { Router, ActivatedRoute } from '@angular/router';
import { Login } from '../model/login';
import { AuthService } from '../services/auth.service';
import { PasswordResetService } from '../services/password-reset.service';
import { FormBuilder, Validators } from '@angular/forms';
import { DialogService } from "ng2-bootstrap-modal";
var LoginComponent = /** @class */ (function () {
    function LoginComponent(_reset_service, modalService, _fb, _ar, auth, router) {
        this._reset_service = _reset_service;
        this.modalService = modalService;
        this._fb = _fb;
        this._ar = _ar;
        this.auth = auth;
        this.router = router;
        this.login = new Login();
        // redirect to / if the user is already logged in
        var authToken = localStorage.getItem('id_token');
        if (!(authToken == '' || authToken == null || authToken === 'undefined')) {
            this.router.navigate(['/admin']);
        }
        this.loginForm = this._fb.group({
            'email': ['', [
                    Validators.required,
                    this.isEmail
                ]],
            'password': ['', [Validators.required]]
        });
        this.resetForm = this._fb.group({
            'email': ['', [
                    Validators.required,
                    this.isEmail
                ]],
        });
    }
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        var users_model = this.loginForm.value;
        this.auth
            .login(users_model)
            .subscribe(function (result) {
            if (result) {
                _this.router.navigate(['/admin']);
            }
        }, function (err) { return _this.invalidCredentials(err); });
    };
    LoginComponent.prototype.onReset = function () {
        // console.log('FORM Values:', this.resetForm.value);
        var _this = this;
        var reset_model = this.resetForm.value;
        this._reset_service
            .reset(reset_model)
            .subscribe(function (data) {
            _this.postreset = Array.from(data);
            _this.reset_succes = true;
        }, function (err) { return _this.catchError(err); });
    };
    LoginComponent.prototype.resetSuccess = function () {
        return this.reset_succes;
    };
    LoginComponent.prototype.isEmail = function (control) {
        if (control.value === '') {
            return { noEmail: true };
        }
        else if (!control.value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            return { noEmail: true };
        }
    };
    LoginComponent.prototype.invalidCredentials = function (error) {
        var response = error.json();
        var response_status = error.status;
        if (response.message == 'invalid_credentials') {
            this.error_message = true;
            return this.error_message;
        }
        else if (response_status == 500) {
            this.error_message = true;
            return this.error_message;
        }
    };
    LoginComponent.prototype.clearInvalid = function () {
        this.error_title = '';
        this.error_message = false;
    };
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.ngOnDestroy = function () {
    };
    LoginComponent.prototype.ngAfterViewInit = function () {
    };
    LoginComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        if (response_status == 500) {
            this.error_title = 'Error 500';
            window.scrollTo(0, 0);
        }
    };
    LoginComponent = __decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css']
        }),
        __metadata("design:paramtypes", [PasswordResetService, DialogService, FormBuilder, ActivatedRoute, AuthService, Router])
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=login.component.js.map