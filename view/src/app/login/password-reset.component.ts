import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import 'rxjs/add/operator/catch';
import { Headers, Response } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PasswordReset } from '../model/password_reset';
import { AuthService } from '../services/auth.service';
import { PasswordResetService } from '../services/password-reset.service'
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Observable } from "rxjs/Rx";
import { matchingPasswords } from '../validators/validators';



@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./login.component.css']
})
export class PasswordResetComponent implements OnInit {

  public PasswordReset = new PasswordReset();
  public postlogin: any;
  public postreset: any; 
  public error_title: string;
  public token: any;
  private res: any;
  private hal: any;

  public error_message: Boolean;
  public reset_success: Boolean;
  private headers: Headers;
  passwordResetForm : FormGroup;


  

  constructor(private route: ActivatedRoute, private _reset_service: PasswordResetService, private _fb: FormBuilder, private _ar: ActivatedRoute, private auth: AuthService, private router:Router) { 
    // redirect to / if the user is already logged in

    this.token = this.route.snapshot.params['token']

    let authToken = localStorage.getItem('id_token');
    if(!(authToken == '' || authToken == null || authToken === 'undefined')) {
      this.router.navigate([ '/admin' ])
    }

    this.passwordResetForm = _fb.group({
     
      'email': ['', [
        Validators.required,
        this.isEmail
      ]],

      'password': ['', Validators.required],
      'password_confirmation': ['', 
                [ Validators.required,
                 this.isEqualPassword.bind(this) ]
      ],

      'token' : ['']

   });
  }


    isEqualPassword(control: FormControl): {[s: string]: boolean} {
        if (!this.passwordResetForm) {
            return {passwordsNotMatch: true};

        }
        if (control.value !== this.passwordResetForm.controls['password'].value) {
            return {passwordsNotMatch: true};
        }
    }



   onSubmit() {
     this.passwordResetForm.patchValue({
      'token': this.token, 
    });

    let  password_reset = this.passwordResetForm.value;
    console.log(password_reset);

    this._reset_service
    .resetPassword(password_reset, this.token)
    .subscribe(data => {
        this.postreset = Array.from(data);
        this.reset_success = true;
      },
      err => this.catchError(err)

    );
  } 

  resetSuccess() {
    return this.reset_success;
  }

  isEmail(control: FormControl): {[s: string]: boolean} {      
    if(control.value === '') {
      return { noEmail: true }
    } else if(!control.value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/))  {
      return { noEmail: true };
    }
  }



  private clearInvalid(){
    this.error_title = '';
    this.error_message = false;
  }
  
  ngOnInit() {
  }

  private handleResponse(res: any) {
    let response_body = res.false;

    let response_status = res.message;

    if(response_status == false) {
      this.error_message = true;
    }
  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;
    console.log(error.message);
    if( response_status == 500 ){
      this.error_message = true;
      window.scrollTo(0, 0);
    } else if (error.message == 'invalid_input') {
      this.error_message = true;
      window.scrollTo(0, 0);
    }
  }
}
