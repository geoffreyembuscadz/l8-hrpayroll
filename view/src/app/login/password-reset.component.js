var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import 'rxjs/add/operator/catch';
import { Router, ActivatedRoute } from '@angular/router';
import { PasswordReset } from '../model/password_reset';
import { AuthService } from '../services/auth.service';
import { PasswordResetService } from '../services/password-reset.service';
import { FormBuilder, Validators } from '@angular/forms';
var PasswordResetComponent = /** @class */ (function () {
    function PasswordResetComponent(route, _reset_service, _fb, _ar, auth, router) {
        // redirect to / if the user is already logged in
        this.route = route;
        this._reset_service = _reset_service;
        this._fb = _fb;
        this._ar = _ar;
        this.auth = auth;
        this.router = router;
        this.PasswordReset = new PasswordReset();
        this.token = this.route.snapshot.params['token'];
        var authToken = localStorage.getItem('id_token');
        if (!(authToken == '' || authToken == null || authToken === 'undefined')) {
            this.router.navigate(['/admin']);
        }
        this.passwordResetForm = _fb.group({
            'email': ['', [
                    Validators.required,
                    this.isEmail
                ]],
            'password': ['', Validators.required],
            'password_confirmation': ['',
                [Validators.required,
                    this.isEqualPassword.bind(this)]
            ],
            'token': ['']
        });
    }
    PasswordResetComponent.prototype.isEqualPassword = function (control) {
        if (!this.passwordResetForm) {
            return { passwordsNotMatch: true };
        }
        if (control.value !== this.passwordResetForm.controls['password'].value) {
            return { passwordsNotMatch: true };
        }
    };
    PasswordResetComponent.prototype.onSubmit = function () {
        var _this = this;
        this.passwordResetForm.patchValue({
            'token': this.token,
        });
        var password_reset = this.passwordResetForm.value;
        console.log(password_reset);
        this._reset_service
            .resetPassword(password_reset, this.token)
            .subscribe(function (data) {
            _this.postreset = Array.from(data);
            _this.reset_success = true;
        }, function (err) { return _this.catchError(err); });
    };
    PasswordResetComponent.prototype.resetSuccess = function () {
        return this.reset_success;
    };
    PasswordResetComponent.prototype.isEmail = function (control) {
        if (control.value === '') {
            return { noEmail: true };
        }
        else if (!control.value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            return { noEmail: true };
        }
    };
    PasswordResetComponent.prototype.clearInvalid = function () {
        this.error_title = '';
        this.error_message = false;
    };
    PasswordResetComponent.prototype.ngOnInit = function () {
    };
    PasswordResetComponent.prototype.handleResponse = function (res) {
        var response_body = res.false;
        var response_status = res.message;
        if (response_status == false) {
            this.error_message = true;
        }
    };
    PasswordResetComponent.prototype.catchError = function (error) {
        var response_body = error._body;
        var response_status = error.status;
        console.log(error.message);
        if (response_status == 500) {
            this.error_message = true;
            window.scrollTo(0, 0);
        }
        else if (error.message == 'invalid_input') {
            this.error_message = true;
            window.scrollTo(0, 0);
        }
    };
    PasswordResetComponent = __decorate([
        Component({
            selector: 'app-password-reset',
            templateUrl: './password-reset.component.html',
            styleUrls: ['./login.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute, PasswordResetService, FormBuilder, ActivatedRoute, AuthService, Router])
    ], PasswordResetComponent);
    return PasswordResetComponent;
}());
export { PasswordResetComponent };
//# sourceMappingURL=password-reset.component.js.map