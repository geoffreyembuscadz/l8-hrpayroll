import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import 'rxjs/add/operator/catch';
import { Headers, Response } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Login } from '../model/login';
import { AuthService } from '../services/auth.service';
import { PasswordResetService } from '../services/password-reset.service'
import { FormControl, FormArray, FormGroup, NgForm, FormBuilder, FormsModule, Validators } from '@angular/forms';
import { Observable } from "rxjs/Rx";
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public login = new Login();
  public postlogin: any;
  public postreset: any; 
  public error_title: string;
  public error_message: Boolean;
  public reset_succes: Boolean;
  private headers: Headers;
  loginForm : FormGroup;
  resetForm : FormGroup;

  constructor(private _reset_service: PasswordResetService, private modalService: DialogService, private _fb: FormBuilder, private _ar: ActivatedRoute, private auth: AuthService, private router:Router) { 
    // redirect to / if the user is already logged in
    let authToken = localStorage.getItem('id_token');
    if(!(authToken == '' || authToken == null || authToken === 'undefined')) {
      this.router.navigate([ '/admin' ])
    }
    this.loginForm = this._fb.group({
      'email': ['', [
        Validators.required,
        this.isEmail
      ]],
      'password': ['', [Validators.required]]
    });

    this.resetForm = this._fb.group({
      'email': ['', [
        Validators.required,
        this.isEmail
      ]],
    })
  }

  onSubmit() {
    let  users_model = this.loginForm.value;
    this.auth
    .login(users_model)
    .subscribe((result) => {
      if (result) {
        this.router.navigate(['/admin']);
      }
    },
      err => this.invalidCredentials(err)
    );
  }

  onReset() {
      // console.log('FORM Values:', this.resetForm.value);

      let reset_model = this.resetForm.value;

      this._reset_service
      .reset(reset_model)
      .subscribe(
        data => {
          this.postreset = Array.from(data);
          this.reset_succes = true;

        },
        err => this.catchError(err)
      );
  }

  resetSuccess() {
    return this.reset_succes;
  }

  isEmail(control: FormControl): {[s: string]: boolean} {      
    if(control.value === '') {
      return { noEmail: true }
    } else if(!control.value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/))  {
      return { noEmail: true };
    }
  }

  private invalidCredentials(error: Response | any){
    let response = error.json();
    let response_status = error.status;
    if( response.message == 'invalid_credentials' ){
      this.error_message = true;
      return this.error_message;
    } else if (response_status == 500) {
      this.error_message = true;
      return this.error_message;
    }

  }

  private clearInvalid(){
    this.error_title = '';
    this.error_message = false;
  }
  
  ngOnInit() {
   
  }

  ngOnDestroy() {
  }

  ngAfterViewInit() {
  }

  private catchError(error: any){
    let response_body = error._body;
    let response_status = error.status;

    if( response_status == 500 ){
      this.error_title = 'Error 500';
      window.scrollTo(0, 0);
    }
  }
}
