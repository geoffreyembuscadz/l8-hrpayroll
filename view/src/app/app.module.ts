import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule, Http } from '@angular/http';
import { RouterModule }   from '@angular/router';
import { CalendarModule } from 'angular-calendar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormsModule, NgForm, ReactiveFormsModule } from '@angular/forms';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { DragAndDropModule } from 'angular-draggable-droppable';
import {ArchwizardModule} from 'ng2-archwizard';
import { AppUtilsModule } from './app-utils/module';
import { AppComponent } from './app.component';
import { AdminModule } from './admin/admin.module';
import { Configuration } from './app.config';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { PasswordResetComponent } from './login/password-reset.component';
import { AttendanceComponent } from './attendance/attendance.component'
import { AuthService } from './services/auth.service';
import { PasswordResetService } from './services/password-reset.service';
import { ExtendedHttpService } from './services/error-handler.service';
import { _404Component } from './404/404.component';
import { AuthUserService } from './services/auth-user.service';
import { MainSideService } from './services/main-side.service';
import { CompanyModalComponent } from './admin/company/company-modal/company-modal.component';
// import { CalendarComponent } from "angular2-fullcalendar/src/calendar/calendar";  

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    _404Component,
    AttendanceComponent,
    PasswordResetComponent,
    CompanyModalComponent
    // CalendarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule.forRoot(),
    CalendarModule.forRoot(),
    AppUtilsModule,
    DragAndDropModule,
    HttpModule,
    JsonpModule,
    AdminModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [ Configuration, AuthService, PasswordResetService, AuthUserService, MainSideService ],
  bootstrap: [AppComponent]
})
export class AppModule { }  
