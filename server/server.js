var server = require('http').createServer();
var io = require('socket.io')(server);


const port = process.env.PORT || '3000';
const host = process.env.HOST || '127.0.0.1';


io.on('connection', function(client){
	// room
	client.on('join', function(data) {
	    console.log('Server: ', data);
	});
	client.on('event', function(data){});
	client.on('disconnect', function(){});

	client.on('user', function(data) {
        client.broadcast.emit('admin', data);
        console.log('To admin: ', data);
    });

	client.on('admin', function(data) {
        client.broadcast.emit('user', data);
        console.log('To user: ', data);
    });

    client.on('notify', function(data) {
        client.broadcast.emit('receiveNotif', data);
        console.log('To mainheader: ', data);
    });

    client.on('markAll', function(data) {
        client.broadcast.emit('notification_list', data);
        console.log('To notif: ', data);
    });


});
server.listen(port, () => console.log(`API running on http://${host}:${port}`));