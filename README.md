Project Description
====================

This project aims to provide the business solution for MJM Inc. in HR Management & Payroll System.

What I Learned
--------------------

In this project, I learned how to develop an angular 2 project as the front-end application to our client & develop an API backend script with Lumen Framework.

Introduction
--------------------

This is the **Angular5** version of **AdminLTE** -- is a fully responsive admin template. Based on **[Bootstrap 3](https://github.com/twbs/bootstrap)** framework. Highly customizable and easy to use.

**Download & Preview AndminLTE on [Almsaeed Studio](https://almsaeedstudio.com)**

!["AdminLTE Presentation"](https://almsaeedstudio.com/AdminLTE2.png "AdminLTE Presentation")

Installation
------------

*Required
- node v6.9+

- Fork the repository (git@bitbucket.org:logic8hris/hrpayroll.git).
- Clone to your machine
```
git clone git@bitbucket.org:logic8hris/hrpayroll.git
```
- Install Angular 2 Client.
```
npm install -g angular-cli@latest

- Install packages
npm install
```
Generating and serving an Angular2 project via a development server

ng new PROJECT_NAME
cd PROJECT_NAME
npm start or ng serve

Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

You can configure the default HTTP port and the one used by the LiveReload server with two command-line options :

ng serve --host 0.0.0.0 --port 4201 --live-reload-port 49153


Generating Components, Directives, Pipes and Services
-----------------------------------------------------

You can use the ng generate (or just ng g) command to generate Angular components:

ng generate component my-new-component
ng g component my-new-component # using the alias

# components support relative path generation
# if in the directory src/app/feature/ and you run
ng g component new-cmp
# your component will be generated in src/app/feature/new-cmp
# but if you were to run
ng g component ../newer-cmp
# your component will be generated in src/app/newer-cmp
You can find all possible blueprints in the table below:

**Scaffold Usage**
```
- Component ng g component my-new-component
- Directive ng g directive my-new-directive
- Pipe ng g pipe my-new-pipe
- Service ng g service my-new-service
- Class ng g class my-new-class
- Interface ng g interface my-new-interface
- Enum ng g enum my-new-enum
- Module ng g module my-module
```

Updating Angular CLI
---------------------
To update Angular CLI to a new version, you must update both the global package and your project's local package.

**Global package:**
```
npm uninstall -g angular-cli @angular/cli
npm cache clean
npm install -g @angular/cli@latest
```
**Local project package:**
```
rm -rf node_modules dist # use rmdir on Windows
npm install --save-dev @angular/cli@latest
npm install
ng update
```

 Theme Credits
-------------
[Almsaeed Studio](https://almsaeedstudio.com)

 Nginx Configurations
-----------------------
When using an Angular2 App
```
location / {
	try_files $uri $uri/ /index.html?$query_string
	fastcgi_index index.html
}
```

When using a lumen Framework
```
location ~ $ {
    try_files $uri $uri/ /index.php?;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
    fastcgi_index index.php;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    include fastcgi_params;
}
```

BOOTSTRAP 4 CHANGES

	- Change md to lg in grid system
	- Change pull to float
	- Change label to badge



