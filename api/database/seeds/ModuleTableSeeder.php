<?php

use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('modules')->insert([
            'name'          => 'Dashboard',
            'display_name'  => 'Dashboard',
            'description'   => 'Dashboard',
            'icons'         => 'fa-tachometer',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('modules')->insert([
            'name'          => 'Company',
            'display_name'  => 'Company',
            'description'   => 'Company',
            'icons'         => 'fa-building-o',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('modules')->insert([
            'name'          => 'Employee',
            'display_name'  => 'Employee',
            'description'   => 'Employee',
            'icons'         => 'fa-users',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('modules')->insert([
            'name'          => 'Attendance',
            'display_name'  => 'Attendance',
            'description'   => 'Attendance',
            'icons'         => 'fa-sign-in',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);
        
         DB::table('modules')->insert([
            'name'          => 'Request',
            'display_name'  => 'Request',
            'description'   => 'Request',
            'icons'         => 'fa-pencil-square-o',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);
         
        DB::table('modules')->insert([
            'name'          => 'My Request',
            'display_name'  => 'My Request',
            'description'   => 'My Request',
            'icons'         => 'fa-pencil-square',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('modules')->insert([
            'name'          => 'Payroll',
            'display_name'  => 'Payroll',
            'description'   => 'Payroll',
            'icons'         => 'fa-usd',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('modules')->insert([
            'name'          => 'Users',
            'display_name'  => 'Users Controls',
            'description'   => 'Users',
            'icons'         => 'fa-user-o',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

         DB::table('modules')->insert([
            'name'          => 'Settings',
            'display_name'  => 'Settings',
            'description'   => 'Settings',
            'icons'         => 'fa-cog',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('modules')->insert([
            'name'          => 'Goverment Table',
            'display_name'  => 'Goverment Table',
            'description'   => 'Goverment Table',
            'icons'         => 'fa-university',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);


        DB::table('module_menus')->insert([
            'name'          => 'Admin Dashboard',
            'display_name'  => 'Dashboard',
            'description'   => 'Dashboard',
            'parent_id'     => '1',
            'level'         => '1',
            'action_url'    => 'admin',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Employee Dashboard',
            'display_name'  => 'Dashboard',
            'description'   => 'Dashboard',
            'parent_id'     => '1',
            'level'         => '1',
            'action_url'    => 'admin',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        /* /1 */

        /* 2 */

        DB::table('module_menus')->insert([
            'name'          => 'Company',
            'display_name'  => 'Company List',
            'description'   => 'Company',
            'parent_id'     => '2',
            'level'         => '1',
            'action_url'    => 'company-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Company Branch',
            'display_name'  => 'Company Branch',
            'description'   => 'Company Branch',
            'parent_id'     => '2',
            'level'         => '1',
            'action_url'    => 'company-branch',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Company Policy',
            'display_name'  => 'Company Policy',
            'description'   => 'Company Policy',
            'parent_id'     => '2',
            'level'         => '1',
            'action_url'    => 'company-policy',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Company Working Schedule',
            'display_name'  => 'Company Working Schedule',
            'description'   => 'Company Working Schedule',
            'parent_id'     => '2',
            'level'         => '1',
            'action_url'    => 'company-working-schedule',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Employee',
            'display_name'  => 'Employee List',
            'description'   => 'Employee',
            'parent_id'     => '3',
            'level'         => '1',
            'action_url'    => 'employee-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Employee Mass Upload',
            'display_name'  => 'Employee Profile Upload',
            'description'   => 'Employee Mass Upload',
            'parent_id'     => '3',
            'level'         => '1',
            'action_url'    => 'employee-mass-upload',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Attendance Default',
            'display_name'  => 'Attendance',
            'description'   => 'Attendance Default',
            'parent_id'     => '4',
            'level'         => '1',
            'action_url'    => 'attendance-list-default',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Attendance',
            'display_name'  => 'Attendance (Static)',
            'description'   => 'Attendance',
            'parent_id'     => '4',
            'level'         => '1',
            'action_url'    => 'attendance-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'My Attendance',
            'display_name'  => 'My Attendance',
            'description'   => 'My Attendance',
            'parent_id'     => '4',
            'level'         => '1',
            'action_url'    => 'my-attendance-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);


        DB::table('module_menus')->insert([
            'name'          => 'Schedule Master',
            'display_name'  => 'Schedule Master',
            'description'   => 'Schedule Master',
            'parent_id'     => '5',
            'level'         => '1',
            'action_url'    => 'schedule-master-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Schedule Adjustment',
            'display_name'  => 'Schedule Adjustment',
            'description'   => 'Schedule Adjustment',
            'parent_id'     => '5',
            'level'         => '1',
            'action_url'    => 'schedule-adjustments-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Certificate Of Attendance',
            'display_name'  => 'Certificate Of Attendance',
            'description'   => 'Certificate Of Attendance',
            'parent_id'     => '5',
            'level'         => '1',
            'action_url'    => 'certificate-of-attendance-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Official Business',
            'display_name'  => 'Official Business',
            'description'   => 'Official Business',
            'parent_id'     => '5',
            'level'         => '1',
            'action_url'    => 'official-business-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Leave',
            'display_name'  => 'Leave',
            'description'   => 'Leave',
            'parent_id'     => '5',
            'level'         => '1',
            'action_url'    => 'leave-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Overtime',
            'display_name'  => 'Overtime',
            'description'   => 'Overtime',
            'parent_id'     => '5',
            'level'         => '1',
            'action_url'    => 'overtime-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);        

        DB::table('module_menus')->insert([
            'name'          => 'Undertime',
            'display_name'  => 'Undertime',
            'description'   => 'Undertime',
            'parent_id'     => '5',
            'level'         => '1',
            'action_url'    => 'undertime-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);
    


        /* 7 */

        DB::table('module_menus')->insert([
            'name'          => 'My Schedule Adjustment',
            'display_name'  => 'My Schedule Adjustment',
            'description'   => 'My Schedule Adjustment',
            'parent_id'     => '6',
            'level'         => '1',
            'action_url'    => 'my-schedule-adjustments-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'My Certificate Of Attendance',
            'display_name'  => 'My Certificate Of Attendance',
            'description'   => 'My Certificate Of Attendance',
            'parent_id'     => '6',
            'level'         => '1',
            'action_url'    => 'my-coa-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'My Official Business',
            'display_name'  => 'My Official Business',
            'description'   => 'My Official Business',
            'parent_id'     => '6',
            'level'         => '1',
            'action_url'    => 'my-official-business-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'My Leave',
            'display_name'  => 'My Leave',
            'description'   => 'My Leave',
            'parent_id'     => '6',
            'level'         => '1',
            'action_url'    => 'my-leave-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);        

        DB::table('module_menus')->insert([
            'name'          => 'My Overtime',
            'display_name'  => 'My Overtime',
            'description'   => 'My Overtime',
            'parent_id'     => '6',
            'level'         => '1',
            'action_url'    => 'my-overtime-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'My Undertime',
            'display_name'  => 'My Undertime',
            'description'   => 'My Undertime',
            'parent_id'     => '6',
            'level'         => '1',
            'action_url'    => 'my-undertime-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Adjustment',
            'display_name'  => 'Adjustments & Deductions',
            'description'   => 'Adjustment',
            'parent_id'     => '7',
            'level'         => '1',
            'action_url'    => 'adjustment-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Payroll',
            'display_name'  => 'Payroll Generate',
            'description'   => 'Payroll',
            'parent_id'     => '7',
            'level'         => '1',
            'action_url'    => 'payroll',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Loan',
            'display_name'  => 'File Loan',
            'description'   => 'Loan',
            'parent_id'     => '7',
            'level'         => '1',
            'action_url'    => 'loan-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);
      

        DB::table('module_menus')->insert([
            'name'          => 'Users',
            'display_name'  => 'Users',
            'description'   => 'Users',
            'parent_id'     => '8',
            'level'         => '1',
            'action_url'    => 'user-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Role',
            'display_name'  => 'Roles',
            'description'   => 'Role',
            'parent_id'     => '8',
            'level'         => '1',
            'action_url'    => 'role-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);


        DB::table('module_menus')->insert([
            'name'          => 'Permission',
            'display_name'  => 'Permissions',
            'description'   => 'Permission',
            'parent_id'     => '8',
            'level'         => '1',
            'action_url'    => 'permission-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

              
  
        DB::table('module_menus')->insert([
            'name'          => 'Adjustment Type',
            'display_name'  => 'Adjustment Type',
            'description'   => 'Adjustment Type',
            'parent_id'     => '9',
            'level'         => '1',
            'action_url'    => 'adjustment-type-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

         DB::table('module_menus')->insert([
            'name'          => 'Department',
            'display_name'  => 'Department Type',
            'description'   => 'Department',
            'parent_id'     => '9',
            'level'         => '1',
            'action_url'    => 'department-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Holiday Type List',
            'display_name'  => 'Holiday Type List',
            'description'   => 'Holiday Type List',
            'parent_id'     => '9',
            'level'         => '1',
            'action_url'    => 'holiday-type-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Leave Type',
            'display_name'  => 'Leave Type',
            'description'   => 'Leave Type',
            'parent_id'     => '9',
            'level'         => '1',
            'action_url'    => 'leave-type-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Loan Type',
            'display_name'  => 'Loan Type',
            'description'   => 'Loan Type',
            'parent_id'     => '9',
            'level'         => '1',
            'action_url'    => 'loan-type-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Menu Configuration',
            'display_name'  => 'Menu Configuration',
            'description'   => 'Menu Configuration',
            'parent_id'     => '9',
            'level'         => '1',
            'action_url'    => 'menu-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Module Configuration',
            'display_name'  => 'Module Configuration',
            'description'   => 'Module Configuration',
            'parent_id'     => '9',
            'level'         => '1',
            'action_url'    => 'module-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'Position',
            'display_name'  => 'Position',
            'description'   => 'Position',
            'parent_id'     => '9',
            'level'         => '1',
            'action_url'    => 'position-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('module_menus')->insert([
            'name'          => 'SSS',
            'display_name'  => 'SSS Table',
            'description'   => 'SSS',
            'parent_id'     => '10',
            'level'         => '1',
            'action_url'    => 'sss-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);        

        DB::table('module_menus')->insert([
            'name'          => 'Holiday List',
            'display_name'  => 'List of Holidays',
            'description'   => 'Holiday List',
            'parent_id'     => '10',
            'level'         => '1',
            'action_url'    => 'holiday-list',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);
    }
}
