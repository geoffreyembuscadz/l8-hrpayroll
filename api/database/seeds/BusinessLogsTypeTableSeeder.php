<?php

use Illuminate\Database\Seeder;

class BusinessLogsTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('business_logs_type')->insert([
            'name' => 'Meeting',
            'description' => 'Meeting',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('business_logs_type')->insert([
            'name' => 'Seminar',
            'description' => 'seminar',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('business_logs_type')->insert([
            'name' => 'Out of the Office',
            'description' => 'Out of the Office',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}

