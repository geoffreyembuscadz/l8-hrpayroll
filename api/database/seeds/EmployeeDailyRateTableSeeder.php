<?php

use Illuminate\Database\Seeder;

class EmployeeDailyRateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employee_rate')->insert([
            'employee_id' => 1,
            'daily_rate' => 12575.25,
            'company_id' => 1,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
        ]);

        DB::table('employee_rate')->insert([
            'employee_id' => 2,
            'daily_rate' => 12555.25,
            'company_id' => 2,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
        ]);
    }


}
