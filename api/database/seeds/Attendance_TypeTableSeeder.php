<?php

use Illuminate\Database\Seeder;

class Attendance_TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('attendance_type')->insert([
            'name' => 'Manual Input',
            'description' => 'None',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('attendance_type')->insert([
            'name' => 'Upload',
            'description' => 'None',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('attendance_type')->insert([
            'name' => 'Biometrics',
            'description' => 'None',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('attendance_type')->insert([
            'name' => 'Leave',
            'description' => 'None',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('attendance_type')->insert([
            'name' => 'Official Business',
            'description' => 'None',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('attendance_type')->insert([
            'name' => 'Web Log',
            'description' => 'None',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}

