<?php

use Illuminate\Database\Seeder;

class RoleModuleMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // superAdmin
    	DB::table('role_module')->insert([ 'module_id' => '1', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '2', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '3', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '4', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '5', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '6', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '7', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '8', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '9', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '10', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);

        DB::table('role_menu')->insert([ 'menu_id' => '1', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '2', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '3', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '4', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '5', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '6', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '7', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '8', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '9', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '10', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '11', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '12', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '13', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '14', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '15', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '16', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '17', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '18', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '19', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '20', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '21', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '22', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '23', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '24', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '25', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '26', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '27', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '28', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '29', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '30', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '31', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '32', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '33', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '34', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '35', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '36', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '37', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '38', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '39', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '40', 'role_id' => '1', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);

        // users
        DB::table('role_module')->insert([ 'module_id' => '1', 'role_id' => '3', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '4', 'role_id' => '3', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '6', 'role_id' => '3', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]); 

        DB::table('role_menu')->insert([ 'role_id' => '3', 'menu_id' => '2',  'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_menu')->insert([ 'role_id' => '3', 'menu_id' => '11', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_menu')->insert([ 'role_id' => '3', 'menu_id' => '19', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_menu')->insert([ 'role_id' => '3', 'menu_id' => '20', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_menu')->insert([ 'role_id' => '3', 'menu_id' => '21', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_menu')->insert([ 'role_id' => '3', 'menu_id' => '22', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_menu')->insert([ 'role_id' => '3', 'menu_id' => '23', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_menu')->insert([ 'role_id' => '3', 'menu_id' => '24', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);


        // Admin
        DB::table('role_module')->insert([ 'module_id' => '1', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '2', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '3', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '4', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '5', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '6', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '7', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '8', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '9', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('role_module')->insert([ 'module_id' => '10', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);

        DB::table('role_menu')->insert([ 'menu_id' => '1', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '2', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '3', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '4', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '5', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '6', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '7', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '8', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '9', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '10', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '11', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '12', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '13', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '14', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '15', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '16', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '17', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '18', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '19', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '20', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '21', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '22', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '23', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '24', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '25', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '26', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '27', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '28', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '29', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '30', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '31', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '32', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '33', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '34', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '35', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        // DB::table('role_menu')->insert([ 'menu_id' => '36', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        // DB::table('role_menu')->insert([ 'menu_id' => '37', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '38', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '39', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        DB::table('role_menu')->insert([ 'menu_id' => '40', 'role_id' => '2', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s')]);
       
    }
}

