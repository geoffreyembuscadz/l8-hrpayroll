<?php

use Illuminate\Database\Seeder;

class SSSTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	DB::table('sss')->insert([
            'salary_base' => 1000,
            'salary_ceiling' => 1249.99,
            'monthly_salary_credit' => 1000,
            'er' => 73.70,
            'ee' => 36.30,
            'total_contribution' => 110,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 1250,
            'salary_ceiling' => 1749.99,
            'monthly_salary_credit' =>1500,
            'er' => 110.50,
            'ee' => 54.50,
            'total_contribution' => 165.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 1750,
            'salary_ceiling' => 2249.99,
            'monthly_salary_credit' => 2000,
            'er' => 147.30,
            'ee' => 72.70,
            'total_contribution' => 220,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

         ]);
    	DB::table('sss')->insert([
            'salary_base' => 2250,
            'salary_ceiling' => 2749.99,
            'monthly_salary_credit' => 2500,
            'er' => 184.20,
            'ee' => 90.80,
            'total_contribution' => 275.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

         ]);

    	DB::table('sss')->insert([
            'salary_base' => 2750,
            'salary_ceiling' => 3249.99,
            'monthly_salary_credit' => 3000,
            'er' => 221.00,
            'ee' => 109.00,
            'total_contribution' => 330.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 3250,
            'salary_ceiling' => 3749.99,
            'monthly_salary_credit' => 3500,
            'er' =>257.80,
            'ee' => 127.20,
            'total_contribution' => 385,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 3750,
            'salary_ceiling' => 4249.99,
            'monthly_salary_credit' => 4000,
            'er' => 294.70,
            'ee' => 145.30,
            'total_contribution' => 440.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 4250,
            'salary_ceiling' => 4749.99,
            'monthly_salary_credit' => 4500,
            'er' => 331.50,
            'ee' => 163.50,
            'total_contribution' => 395.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 4750,
            'salary_ceiling' => 5249.99,
            'monthly_salary_credit' => 5000,
            'er' => 368.30,
            'ee' => 181.70,
            'total_contribution' => 550.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 5250,
            'salary_ceiling' => 5749.99,
            'monthly_salary_credit' => 5500,
            'er' => 405.20,
            'ee' => 199.80,
            'total_contribution' => 605.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 5750,
            'salary_ceiling' => 6249.99,
            'monthly_salary_credit' => 6000,
            'er' => 442.00,
            'ee' => 218.00,
            'total_contribution' => 660.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 6250,
            'salary_ceiling' => 6749.99,
            'monthly_salary_credit' => 6500,
            'er' => 478.80,
            'ee' => 236.20,
            'total_contribution' => 715,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 6750,
            'salary_ceiling' => 7249.99,
            'monthly_salary_credit' => 7000,
            'er' => 515.70,
            'ee' => 254.30,
            'total_contribution' => 770,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 7250,
            'salary_ceiling' => 7749.99,
            'monthly_salary_credit' => 7500,
            'er' => 552.50,
            'ee' => 272.50,
            'total_contribution' => 825.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 7750,
            'salary_ceiling' => 8249.99,
            'monthly_salary_credit' => 8000,
            'er' => 589.30,
            'ee' => 290.70,
            'total_contribution' => 880,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 8250,
            'salary_ceiling' => 8749.99,
            'monthly_salary_credit' => 8500,
            'er' => 626.20,
            'ee' => 308.80,
            'total_contribution' => 935.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 8750,
            'salary_ceiling' => 9249,
            'monthly_salary_credit' => 9000,
            'er' => 663.00,
            'ee' => 327.00,
            'total_contribution' => 990,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 9250,
            'salary_ceiling' => 9749.99,
            'monthly_salary_credit' => 9500,
            'er' => 699.80,
            'ee' => 345.20,
            'total_contribution' => 1045,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 9750,
            'salary_ceiling' => 10249.99,
            'monthly_salary_credit' => 10000,
            'er' => 736.70,
            'ee' => 363.30,
            'total_contribution' => 1100,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 10250,
            'salary_ceiling' => 10749.99,
            'monthly_salary_credit' => 10500,
            'er' => 773.50,
            'ee' => 381.50,
            'total_contribution' => 1155.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 10750,
            'salary_ceiling' => 11249.99,
            'monthly_salary_credit' => 11000,
            'er' => 810.30,
            'ee' => 399.70,
            'total_contribution' => 1210.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 11250,
            'salary_ceiling' => 11500,
            'monthly_salary_credit' => 11500,
            'er' => 847.20,
            'ee' => 417.80,
            'total_contribution' => 1265,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 11750,
            'salary_ceiling' => 12249.99,
            'monthly_salary_credit' => 12000,
            'er' => 884.00,
            'ee' => 436.00,
            'total_contribution' => 1320,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 12250,
            'salary_ceiling' => 12749.99,
            'monthly_salary_credit' => 12500,
            'er' => 920.80,
            'ee' => 454.20,
            'total_contribution' => 1375.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 12750,
            'salary_ceiling' => 13249.99,
            'monthly_salary_credit' => 13000,
            'er' => 957.70,
            'ee' => 472.30,
            'total_contribution' => 1430.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 13250,
            'salary_ceiling' => 13749.99,
            'monthly_salary_credit' => 13500,
            'er' => 994.50,
            'ee' => 1495,
            'total_contribution' => 1485,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 13750,
            'salary_ceiling' => 14249.99,
            'monthly_salary_credit' => 14000,
            'er' => 1031.30,
            'ee' => 508.70,
            'total_contribution' => 1540.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

    	DB::table('sss')->insert([
            'salary_base' => 14250,
            'salary_ceiling' => 14749.99,
            'monthly_salary_credit' => 14500,
            'er' => 1068.20,
            'ee' => 526.80,
            'total_contribution' => 1595,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

         DB::table('sss')->insert([
            'salary_base' => 14750,
            'salary_ceiling' => 15249.99,
            'monthly_salary_credit' => 15000,
            'er' => 1105.00,
            'ee' => 545.00,
            'total_contribution' => 1650,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

         DB::table('sss')->insert([
            'salary_base' => 15250,
            'salary_ceiling' => 15749.99,
            'monthly_salary_credit' => 15500,
            'er' => 1105.80,
            'ee' => 563.20,
            'total_contribution' => 1650,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);
         
         DB::table('sss')->insert([
            'salary_base' => 15750,
            'salary_ceiling' => 100000,
            'monthly_salary_credit' => 16000,
            'er' => 1178.70,
            'ee' => 581.30,
            'total_contribution' => 1760,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
         ]);

        


 }


}
