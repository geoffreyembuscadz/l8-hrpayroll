<?php

use Illuminate\Database\Seeder;

class BillingRateTableseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('billing_rate')->insert([
            'salary' => 502,
            'billing_rate' => 609.29,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('billing_rate')->insert([
            'salary' => 517,
            'billing_rate' => 610.78,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('billing_rate')->insert([
            'salary' => 527,
            'billing_rate' => 611.77,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('billing_rate')->insert([
            'salary' => 537,
            'billing_rate' => 612.77,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('billing_rate')->insert([
            'salary' => 540,
            'billing_rate' => 613.07,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('billing_rate')->insert([
            'salary' => 561,
            'billing_rate' => 615.16,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('billing_rate')->insert([
            'salary' => 571,
            'billing_rate' => 616.15,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('billing_rate')->insert([
            'salary' => 671,
            'billing_rate' => 626.07,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('billing_rate')->insert([
            'salary' => 760,
            'billing_rate' => 634.92,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    	
    }
}

