<?php

use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('department')->insert([
            'name' => 'Admin Department',
            'description' => 'Admin people',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

    	DB::table('department')->insert([
            'name' => 'IT Department',
            'description' => 'IT people',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('department')->insert([
            'name' => 'Accounting',
            'description' => 'Accounting Department',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('department')->insert([
            'name' => 'COM 101',
            'description' => 'IT people',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('department')->insert([
            'name' => 'Construction Department',
            'description' => 'Construction people',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('department')->insert([
            'name' => 'Human Resource Department',
            'description' => 'HR people',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('department')->insert([
            'name' => 'Financial Department',
            'description' => 'finance people',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('department')->insert([
            'name' => 'Marketing Department',
            'description' => 'marketing people',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('department')->insert([
            'name' => 'Operation Department',
            'description' => 'operation people',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('department')->insert([
            'name' => 'Quality Control Department',
            'description' => 'marketing people',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('department')->insert([
            'name' => 'COO Department',
            'description' => 'company ownership people',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}

