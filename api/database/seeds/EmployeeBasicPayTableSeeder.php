<?php

use Illuminate\Database\Seeder;

class EmployeeBasicPayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('employee_basic_pay')->insert([
            'employee_id' => 1,
            'company_id' => 1,
            'basic_pay' => 12575.25,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
        ]);

        DB::table('employee_basic_pay')->insert([
            'employee_id' => 2,
            'company_id' => 2,
            'basic_pay' => 12555.25,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
        ]);
    }


}
