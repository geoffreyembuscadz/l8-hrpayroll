<?php

use App\Role;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
// use Laracasts\TestDummy\Factory as TestDummy;

class RolesTableSeeder extends Seeder
{
    public function run()
    {


        DB::table('roles')->insert([
            'name'          => 'superAdmin',
            'display_name'  => 'Super Administrator',
            'description'   => 'All access',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);
        
        DB::table('roles')->insert([
            'name'          => 'admin',
            'display_name'  => 'Administrator',
            'description'   => 'Admin access',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

         DB::table('roles')->insert([
            'name'          => 'user',
            'display_name'  => 'User',
            'description'   => 'User access',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        

    }
}
