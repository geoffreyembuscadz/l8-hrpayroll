<?php

use Illuminate\Database\Seeder;

class Employees_LogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees_logs')->insert([
            'employees_id' => '1',
            'type'         => 'in';
        ]);
    }
}
