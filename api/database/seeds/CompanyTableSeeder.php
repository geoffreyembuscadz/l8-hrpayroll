<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// MJM Companies
        DB::table('company')->insert([
        'name' => 'ACCESS SAFEWORKS INC.',
        'description' => 'ACCESS SAFEWORKS INC.',
        'address' => 'UNIT 1114 MEDICAL PLAZA BLDG., AMORSOLO ST. COR., DELA ROSA LEGAZPI VILLAGE MAKATI CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '843-5624',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'AKARI LIGHTING & TECHNOLOGY',
        'description' => 'AKARI LIGHTING & TECHNOLOGY',
        'address' => '#97 TOMAS ARGUELLES ST. CORNER BAYANI ST. BRGY. DONA EMELDA QUEZON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '(02) 712-8888',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
            'name' => 'Bramante Supplier',
            'description' => 'Bramante Supplier',
            'address' => 'Pasig City',
            'email' => 'company@gmail.com',
            'phone_number' => '000-0000',
            'contact_person' => 'Company Owner',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'CHRISTIAN COSMETICS INC./ PUREFORM COSMETICS PRODUCT COMPANY',
        'description' => 'CHRISTIAN COSMETICS INC./ PUREFORM COSMETICS PRODUCT COMPANY',
        'address' => '#610 BLUMENTRITT EXTENSION MANILA',
        'email' => 'company@gmail.com',
        'phone_number' => '(02) 749-63-53',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'FRESH ONE INC./UBM',
        'description' => 'FRESH ONE INC./UBM',
        'address' => '#312 SHAW BOULEVARD 2/F LIBERTY CENTER MANDALUYONG CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '(02) 475 – 83-86',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'HIGHLY EXCLUSIVE INC.',
        'description' => 'HIGHLY EXCLUSIVE INC. ',
        'address' => '#1011 BON-BON ST. BRGY UGONG VALENZUELA CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '(02) 432-57-52',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'MARLOCH ENTERPRISE CORPORATION',
        'description' => 'MARLOCH ENTERPRISE CORPORATION',
        'address' => '#610 BLUMENTRITT EXTENSION MANILA',
        'email' => 'company@gmail.com',
        'phone_number' => '(02) 749-63-53',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'M5-MART INC.',
        'description' => 'M5-MART INC. ',
        'address' => '#281 JP RIZAL ST. PROJECT 4 QUEZON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '437-1980',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'PARES RETIRO TUAZON/VISAYAS/RCBC',
        'description' => 'PARES RETIRO TUAZON/VISAYAS/RCBC',
        'address' => 'PARES TUAZON: # 279 P.TUAZON AVE. QUEZON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '277-1070',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'RAMESH TRADING CORP.',
        'description' => 'RAMESH TRADING CORP.',
        'address' => '#0530 A. ARNAIZ., PASAY CITY ',
        'email' => 'company@gmail.com',
        'phone_number' => '(02) 844-1558M (02) 843-9091',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'TAGGO BAR',
        'description' => 'TAGGO BAR ',
        'address' => '17TH P. TUAZON AVE., CUBAO QUEZON CITY LANDMARK CALTEX NEAR CHILLTOP ',
        'email' => 'company@gmail.com',
        'phone_number' => '0943-451-7600',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'TRAVEL GEAR',
        'description' => 'TRAVEL GEAR ',
        'address' => '#779 BETWEEN 7-8TH ST. 9TH AVE. CALOOCAN CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '362-3326 ',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'TSG COMMI 101',
        'description' => 'TSG COMMI 101',
        'address' => '#42 FLORES ST. BAGONG ILOG PASIG CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '(02) 571-3628',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);


        DB::table('company')->insert([
        'name' => 'THE SANDWICH GUY HEAD OFFICE',
        'description' => 'THE SANDWICH GUY HEAD OFFICE',
        'address' => '',
        'email' => 'company@gmail.com',
        'phone_number' => '634-4236',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'UBM CORPORATION',
        'description' => 'UBM CORPORATION',
        'address' => 'MANDALUYONG: #312 SHAW BLVD. 2/F LIBERTY CENTER MANDALUYONG CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '531-7820 ',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);



        DB::table('company')->insert([
        'name' => 'WATER CARE INTERNATIONAL SALES INC.',
        'description' => 'WATER CARE INTERNATIONAL SALES INC.',
        'address' => '#484 ISOMETEIC BLDG., G. ARANETA QUEZON CITY ',
        'email' => 'company@gmail.com',
        'phone_number' => '(02) 716-3333',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'X-TRADE PAPER PACKING CORP.',
        'description' => 'X-TRADE PAPER PACKING CORP. ',
        'address' => '# 110 OLD SAMSON RD. BALINTAWAK QUEZON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '990-6598',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);


        //Jobs88 Companies
        DB::table('company')->insert([
        'name' => 'ACCUPOINT SYSTEMS INC.',
        'description' => '§  ACCUPOINT SYSTEMS INC. ',
        'address' => '# 53 NEW YORK ST. CUBAO QUEZON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '912-4457',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'API INC.',
        'description' => '§  API INC. ',
        'address' => '#7 SAN MARTIN ST. MANDALUYONG CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '631-44-89',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'ASIANIC DISTRIBUTOR’S INC.',
        'description' => '§  ASIANIC DISTRIBUTOR’S INC. ',
        'address' => 'SM SOUTHMALL, SM SUCAT SM BICUTAN, SM DASMA, SM ROSARIO, SM BACOOR',
        'email' => 'company@gmail.com',
        'phone_number' => '0932-5385584',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);



        DB::table('company')->insert([
        'name' => 'CONLINS COFFEE WORLD',
        'description' => '§  CONLINS COFFEE WORLD',
        'address' => '#2753 PARK AVE. PASAY CITY ',
        'email' => 'company@gmail.com',
        'phone_number' => '871-6123',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'DAKILA TRADING CORPORATION',
        'description' => '§  DAKILA TRADING CORPORATION',
        'address' => '#613 CALDERON ST. SHAW BLVD. ANDALUYONG CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '724-7511 LOC 205',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'DASMA PCKG. & PLASTIC PRODUCTS',
        'description' => '§  DASMA PCKG. & PLASTIC PRODUCTS ',
        'address' => 'AGUINALDO HIGHWAY SAMPALOK UNO DASMARINAS CAVITE',
        'email' => 'company@gmail.com',
        'phone_number' => '0917-8432762',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'DECOREA INC.',
        'description' => '§  DECOREA INC.',
        'address' => '#3203-B EAST TEKTITE TOWER EXCHANGE RD., ORTIGAS CENTER PASIG CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '655-99-44',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'D’LAKE HOUSE COMM’L',
        'description' => '§  D’LAKE HOUSE COMM’L',
        'address' => 'UNIT 21 #1630 ANTONIO RIVERA ST. BINONDO MANILA',
        'email' => 'company@gmail.com',
        'phone_number' => '311-71-18/ 4419-255-8367',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'DEUNG SEUNG INC.',
        'description' => '§  DEUNG SEUNG INC. ',
        'address' => 'EPZA ROSARIO.,CAVITE',
        'email' => 'company@gmail.com',
        'phone_number' => '0915-9923001',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'EVERGOOD UNLIMITED CORP. (IMARFLEX)',
        'description' => '§  EVERGOOD UNLIMITED CORP. (IMARFLEX) ',
        'address' => '4TH FLOOR BEE LU BLDG. 103-113 GIL PUYAT PASAY CITY ',
        'email' => 'company@gmail.com',
        'phone_number' => '736-52-02/935-79-91',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'EXCEL PCKG. IND. INC.',
        'description' => '§  EXCEL PCKG. IND. INC.',
        'address' => '#136 SAN JUAN ST. PASAY CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '536-46-41 TO 44',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'GOURDO’S INC.',
        'description' => '§  GOURDO’S INC.  ',
        'address' => '2ND FLOOR GOURDO’S WAREHOUSE , #800 QUEENSWAY AVE., PASCOR DRIVE, PARANAQUE',
        'email' => 'company@gmail.com',
        'phone_number' => '852-48-16',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);


        DB::table('company')->insert([
        'name' => 'IBG GROUP OF COMPANIES',
        'description' => '§  IBG GROUP OF COMPANIES ',
        'address' => '2ND FLOOR ROSARIO ARCADE, #42 ORTOGAS AVE., PASIG CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '640-05-23/ 628-00-00 LOC 204',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'JALBY REFINISHING PLASTIC SERVICES',
        'description' => '§  JALBY REFINISHING PLASTIC SERVICES',
        'address' => '#145 6TH ST. 10TH-11TH AVE. GRACE PARK CALOOCAN CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '365-46-86',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'JFRL ENTERPRISES',
        'description' => '§  JFRL ENTERPRISES',
        'address' => 'UNIT 5 3RD FLOOR TOPY’S PLACE NO. 3 ECONOMIA ST. BAGUMBAYAN QUEZON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '470-88-99',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'KLN FOOD SERVICES',
        'description' => '§  KLN FOOD SERVICES ',
        'address' => 'UNIT 16 3RD FLOOR TOPY’S PLACE, ECONOMIA ST. BAGUMBAYAN QUEZON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '633-36-01',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'LIMKACO INDUSTRIES',
        'description' => '§  LIMKACO INDUSTRIES',
        'address' => '#168 IGNACIO ST. PASAY CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '551-7269/ 0943-7023201',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'LYS MARKETING',
        'description' => '§  LYS MARKETING',
        'address' => '#187 ROOSEVELT AVE., SFDM 1105 QUEZON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '372-60-20 TO 21',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'MONTRELEC INC.',
        'description' => '§  MONTRELEC INC.',
        'address' => '',
        'email' => 'company@gmail.com',
        'phone_number' => '373-53-36',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'MONSI CHEMICAL MFD. IND.',
        'description' => '§  MONSI CHEMICAL MFD. IND. ',
        'address' => '#122 OLD SAMSUN RD. BALINTAWAK QUEZON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '41632-39/0925-8313906',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'NEW PLASTI-MATE MFG. INC.',
        'description' => '§  NEW PLASTI-MATE MFG. INC. ',
        'address' => '#129 SAN JUAN ST. PASAY CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '525-18-86',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'OUTBACK STEAKHOUSE',
        'description' => '§  OUTBACK STEAKHOUSE',
        'address' => 'UNIT 5B1 TRANSPHIL HOUSE, 1177 CHINO ROCES CORNER BAGTIKAN ST. SAN ANTONIO VILLAGE MAKATI CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '890-04-81',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'PACK-RITE MFG. INC.',
        'description' => '§  PACK-RITE MFG. INC.',
        'address' => '#145 SAN JUAN ST. PASAY CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '536-46-45',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'PHARMA DYNAMIC INC.',
        'description' => '§  PHARMA DYNAMIC INC.',
        'address' => '#71 MAYSILO ST. MANDALUYONG CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '532-20-21 LOC 304',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'RCP LOGISTICS',
        'description' => '§  RCP LOGISTICS',
        'address' => '#118 MOANA ST. BRGY 70 PASAY CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '551-08-07/556-55-20',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'RIZE INNOVATIONS INC.',
        'description' => '§  RIZE INNOVATIONS INC.',
        'address' => '#8 HOBBIES OF ASIA PRES. DIOSDADO MACAPAGAL BLVD., PASAY CITY ',
        'email' => 'company@gmail.com',
        'phone_number' => '831-70-38/835-70-38',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'STYLERIGHT GLOBAL CORP.',
        'description' => '§  STYLERIGHT GLOBAL CORP. ',
        'address' => 'UNIT 16 3RD FLOOR TOPY’S PLACE, ECONOMIA ST, BAGUMBAYAN QUEZON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '633-03-45/633-36-01',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'TOY MASTER INC.',
        'description' => '§  TOY MASTER INC. ',
        'address' => '#2566 TAFT AVE., PASAY CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '831-91-49/0912-4201640',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'TRIPLE-M MKTG. INC.',
        'description' => '§  TRIPLE-M MKTG. INC. ',
        'address' => '3RD FLOOR RM. 308 WEB-JET ACROPOLIS BLDG. LIBIS QUEZON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '655-67-16',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'UNITY MARKETING CORPORATION',
        'description' => '§  UNITY MARKETING CORPORATION',
        'address' => '#1463 STO. SEPULCRO CORNER FIGUEROA ST. PACO MANILA',
        'email' => 'company@gmail.com',
        'phone_number' => '563-90-20/0922-8205212',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'UNIFAB METAL IND. INC.',
        'description' => '§  UNIFAB METAL IND. INC. ',
        'address' => 'SOUTH COAST INDUSTRIAL ESTATE, GOVERNOR’S DRIVE BANCAL CARMONA CAVITE',
        'email' => 'company@gmail.com',
        'phone_number' => '584-48-20 OR 21',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'UNI-FREEZE DAVAO',
        'description' => '§  UNI-FREEZE DAVAO ',
        'address' => 'PUROK 13-A SAN VICENTE FERRER BAYAN DAVAO',
        'email' => 'company@gmail.com',
        'phone_number' => '584-48-20 OR 21',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'VORTECHNOLOGIE LABS., INC.',
        'description' => '§  VORTECHNOLOGIE LABS., INC.',
        'address' => '11TH FLOOR UNIT 1104 ATLANTA CENTER, ANNAPOLIS',
        'email' => 'company@gmail.com',
        'phone_number' => '',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'VERLEO CATERING SERVICES',
        'description' => '§  VERLEO CATERING SERVICES',
        'address' => '#17 JASMINE ST. CIRCUMFERENTIAL RD. ARANETA VILLAGE, MALABON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '361-93-79/0176532609',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'WESTERN MKTG. CORP.',
        'description' => '§  WESTERN MKTG. CORP.',
        'address' => '#34 MORATO ST. QUEZON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '373-45-01',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'YAKIMIX BUFFET RESTAURANT',
        'description' => '§  YAKIMIX BUFFET RESTAURANT ',
        'address' => 'LOT 2 PASCOR DRIVE QUEENSWAY SUBD. PARANAQUE',
        'email' => 'company@gmail.com',
        'phone_number' => '853-08-88',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company')->insert([
        'name' => 'SEACOM INC.',
        'description' => '§  SEACOM INC. ',
        'address' => '#721 AURORA BLVD., NEW MANILA QUEZON CITY',
        'email' => 'company@gmail.com',
        'phone_number' => '722-24-40 LOC. 121',
        'contact_person' => 'Company Owner',
        'created_by' => 1,
        'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}

