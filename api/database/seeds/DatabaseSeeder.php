<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // $this->call('CompanyTableSeeder');
      // $this->call('CompanyPolicyTableSeeder');
      // $this->call('CompanyPolicyTypeTableSeeder');
      // $this->call('CompanyPolicyEquivalentTableSeeder');
      // $this->call('CompanyCutoffDatesTableSeeder');
      // $this->call('CompanyGovernmentFieldSeeder');
      // $this->call('WorkingScheduleTableSeeder');
      // $this->call('DepartmentTableSeeder');
      // $this->call('PositionTableSeeder');
      // $this->call('Status_TypeTableSeeder');
      // $this->call('EmployeeTableSeeder');
      // $this->call('EmployeeDailyRateTableSeeder');
      // $this->call('UserTableSeeder');
      // $this->call('Attendance_TypeTableSeeder');
      // $this->call('RolesTableSeeder');
      $this->call('PermissionsTableSeeder');
      $this->call('UserRolePermsTableSeeder');
      $this->call('ModuleTableSeeder');
      $this->call('RoleModuleMenuTableSeeder');
      // $this->call('BillingRateTableseeder');
      // $this->call('SSSTableSeeder');
      // $this->call('LeaveTypeModelTableSeeder');
      // $this->call('PhilhealthTableSeeder');
      // $this->call('COA_TableSeeder');
      // $this->call('Memo_TypeTableSeeder');
      // $this->call('HolidayTypeTableSeeder');
      // $this->call('BusinessLogsTypeTableSeeder');
      // $this->call('BillingRateTableseeder');
      // $this->call('OvertimeTypeTableSeeder');
    }
}
