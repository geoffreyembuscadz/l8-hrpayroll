<?php
use Illuminate\Database\Seeder;

class CompanyCutoffDatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds. Yung magkakaibigan kayu na may batian na "Amen!" o "Amen Pastor!" -feeling blessedO:)
     *
     * @return void
     */
    public function run()
    {
    	DB::table('company_cutoff_date')->insert([
            'company_id' => 1,
            'first_cutoff_date' => '["1","15"]',
            'second_cutoff_date' => '["16","last_day_of_month"]',
            'days_before_notif' => 3,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_cutoff_date')->insert([
            'company_id' => 2,
            'first_cutoff_date' => '["1","15"]',
            'second_cutoff_date' => '["16","last_day_of_month"]',
            'days_before_notif' => 3,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_cutoff_date')->insert([
            'company_id' => 3,
            'first_cutoff_date' => '["1","15"]',
            'second_cutoff_date' => '["16","last_day_of_month"]',
            'days_before_notif' => 4,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}

