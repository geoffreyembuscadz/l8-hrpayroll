<?php

use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank')->insert([ 'bank_code' => '10320013', 'bank_name' => 'ALLIED BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '11020011', 'bank_name' => 'ASIA UNITED BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10700015', 'bank_name' => 'ANZ BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10530667', 'bank_name' => 'BANCO DE ORO' ]);
        DB::table('bank')->insert([ 'bank_code' => '10030015', 'bank_name' => 'BANGKO SENTRAL NG PILIPINAS' ]);
        DB::table('bank')->insert([ 'bank_code' => '10670019', 'bank_name' => 'BANGKOK BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10120019', 'bank_name' => 'BANK OF AMERICA' ]);
        DB::table('bank')->insert([ 'bank_code' => '11140014', 'bank_name' => 'BANK OF CHINA' ]);
        DB::table('bank')->insert([ 'bank_code' => '10440016', 'bank_name' => 'COMBANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10040018', 'bank_name' => 'BPI' ]);
        DB::table('bank')->insert([ 'bank_code' => '10460012', 'bank_name' => 'BANK OF TOKYO' ]);
        DB::table('bank')->insert([ 'bank_code' => '10100013', 'bank_name' => 'CHINA BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10690015', 'bank_name' => 'CHINA TRUST' ]);
        DB::table('bank')->insert([ 'bank_code' => '10070017', 'bank_name' => 'CITIBANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10650013', 'bank_name' => 'DEUTSCHE' ]);
        DB::table('bank')->insert([ 'bank_code' => '10590018', 'bank_name' => 'DBP' ]);
        DB::table('bank')->insert([ 'bank_code' => '10620014', 'bank_name' => 'EASTWEST BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10860010', 'bank_name' => 'EXPORT' ]);
        DB::table('bank')->insert([ 'bank_code' => '10060014', 'bank_name' => 'HONGKONG BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10720011', 'bank_name' => 'JP MORGAN' ]);
        DB::table('bank')->insert([ 'bank_code' => '10710018', 'bank_name' => 'KOREA EXCH BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10350025', 'bank_name' => 'LAND BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10220016', 'bank_name' => 'MAYBANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10560019', 'bank_name' => 'MEGA INTL COMML BANK OF CHINA' ]);
        DB::table('bank')->insert([ 'bank_code' => '10269996', 'bank_name' => 'METROBANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10640010', 'bank_name' => 'MIZUHO CORPORATE BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10110016', 'bank_name' => 'PB COM' ]);
        DB::table('bank')->insert([ 'bank_code' => '10080010', 'bank_name' => 'PNB' ]);
        DB::table('bank')->insert([ 'bank_code' => '10090039', 'bank_name' => 'PHILTRUST' ]);
        DB::table('bank')->insert([ 'bank_code' => '10330016', 'bank_name' => 'VETERANS BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10280014', 'bank_name' => 'RCBC' ]);
        DB::table('bank')->insert([ 'bank_code' => '10770074', 'bank_name' => 'ROYAL BANK OF SCOTLAND (PHILS)' ]);
        DB::table('bank')->insert([ 'bank_code' => '10140015', 'bank_name' => 'SECURITY BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10050011', 'bank_name' => 'CHARTERED BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10419995', 'bank_name' => 'UNION BANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10299995', 'bank_name' => 'COCOBANK' ]);
        DB::table('bank')->insert([ 'bank_code' => '10270341', 'bank_name' => 'UNITED OVERSEAS BANK' ]);

    }
}

