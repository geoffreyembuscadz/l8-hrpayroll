<?php

use App\Http\Models\holiday_typeModel;
use Illuminate\Database\Seeder;
    
class HolidayTypeTableSeeder extends Seeder
{
    public function run()
    {


         DB::table('holiday_type')->insert([
            'name' => 'Rest day', 
            'description' => '', 
            'computation' => '.3',
            'created_by' => 1,    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('holiday_type')->insert([
            'name' => 'Special Holiday', 
            'description' => '', 
            'computation' => '.3',
            'created_by' => 1,    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('holiday_type')->insert([
            'name' => 'Special Holiday And Rest Day', 
            'description' => '', 
            'computation' => '.5',
            'created_by' => 1,    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('holiday_type')->insert([
            'name' => 'Legal/Regular Holiday', 
            'description' => '', 
            'computation' => '1',
            'created_by' => 1,    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('holiday_type')->insert([
            'name' => 'Legal/Regular Holiday and Rest Day', 
            'description' => '', 
            'computation' => '1.6',
            'created_by' => 1,    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('holiday_type')->insert([
            'name' => 'Double Holiday', 
            'description' => '', 
            'computation' => '2',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('holiday_type')->insert([
            'name' => 'Double Holiday and Rest Day', 
            'description' => '', 
            'computation' => '2',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);
    }
}