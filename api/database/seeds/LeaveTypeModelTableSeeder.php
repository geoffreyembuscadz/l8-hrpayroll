<?php

use App\Http\Models\leave_typeModel;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
// use Laracasts\TestDummy\Factory as TestDummy;

class LeaveTypeModelTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('leave_type')->insert([
            'name' => 'Sick', 
            'description' => 'none', 
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('leave_type')->insert([
            'name' => 'Vacation', 
            'description' => 'none',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);
        
        DB::table('leave_type')->insert([
            'name' => 'Emergency', 
            'description' => 'none',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('leave_type')->insert([
            'name' => 'Maternity', 
            'description' => 'none',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);


       
        
    }
}
