<?php

use App\Role;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
// use Laracasts\TestDummy\Factory as TestDummy;

class Status_TypeTableSeeder extends Seeder
{
    public function run()
    {


         DB::table('status_type')->insert([
            'name' => 'PENDING', 
            'description' => 'none',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('status_type')->insert([
            'name' => 'REJECTED', 
            'description' => 'none',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('status_type')->insert([
            'name' => 'CANCELLED',
            'description' => 'none',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('status_type')->insert([
            'name' => 'APPROVED', 
            'description' => 'none',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('status_type')->insert([
            'name' => 'FOR APPROVAL', 
            'description' => 'none',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('status_type')->insert([
            'name' => 'ON-GOING', 
            'description' => 'none',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('status_type')->insert([
            'name' => 'CHECKING', 
            'description' => 'none',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('status_type')->insert([
            'name' => 'DONE', 
            'description' => 'none',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);



        

    }
}
