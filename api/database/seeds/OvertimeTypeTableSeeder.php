<?php

use App\Permission;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
// use Laracasts\TestDummy\Factory as TestDummy;

class OvertimeTypeTableSeeder extends Seeder
{
    public function run()
    {   

        DB::table('overtime_type')->insert([
            'name' => 'Ordinary day Overtime', 
            'description' => '', 
            'computation' => '1.25',
            'created_by' => 1,    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('overtime_type')->insert([
            'name' => 'On Rest day Overtime', 
            'description' => '', 
            'computation' => '1.69',
            'created_by' => 1,    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('overtime_type')->insert([
            'name' => 'On Special Holiday Overtime', 
            'description' => '', 
            'computation' => '1.69',
            'created_by' => 1,    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('overtime_type')->insert([
            'name' => 'On Special Holiday and at the same time rest day overtime', 
            'description' => '', 
            'computation' => '1.95',
            'created_by' => 1,    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('overtime_type')->insert([
            'name' => 'On Regular Holiday Overtime', 
            'description' => '', 
            'computation' => '2.60',
            'created_by' => 1,    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('overtime_type')->insert([
            'name' => 'On Regular Holiday and at the same time Rest day Overtime', 
            'description' => '', 
            'computation' => '3.38',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('overtime_type')->insert([
            'name' => 'On Double Holiday Overtime', 
            'description' => '', 
            'computation' => '3.90',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('overtime_type')->insert([
            'name' => 'On Double Holiday and at the same time Rest day Overtime', 
            'description' => '', 
            'computation' => '5.07',
            'created_by' => '1',    
            'created_at' => DB::raw('NOW()'),
        ]);
    }
}
