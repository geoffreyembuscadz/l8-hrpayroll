<?php
use Illuminate\Database\Seeder;

class CompanyPolicyEquivalentTableSeeder extends Seeder
{
    /**
     * Run the database seeds. Yung magkakaibigan kayu na may batian na "Amen!" o "Amen Pastor!" -feeling blessedO:)
     *
     * @return void
     */
    public function run()
    {
    	DB::table('company_policy_equation')->insert([
            'company_id' => 1,
            'name' => 'Late',
            'description' => 'Late equivalent if 10mins or more late.',
            'value' => 10,
            'value_unit' => 'MINUTE',
            'equivalent_value' => 1,
            'equivalent_unit' => 'HOUR',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}

