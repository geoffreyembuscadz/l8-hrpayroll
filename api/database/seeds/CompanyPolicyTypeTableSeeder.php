<?php
use Illuminate\Database\Seeder;

class CompanyPolicyTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('company_policy_type')->insert([
            'name' => 'Benefit - Leaves',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy_type')->insert([
            'name' => 'Benefit - Paided Holiday',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy_type')->insert([
            'name' => 'Benefit - Health Insurance Coverage',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy_type')->insert([
            'name' => 'Grace Period(Mins.)',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy_type')->insert([
            'name' => 'Major Violation',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy_type')->insert([
            'name' => 'Minor Violation',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy_type')->insert([
            'name' => 'Payroll Cutoff Dates',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy_type')->insert([
        	'name' => 'Tardiness Violation',
        	'created_by' => 1,
        	'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}

