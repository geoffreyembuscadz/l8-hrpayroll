<?php
use Illuminate\Database\Seeder;

class CompanyPolicyTableSeeder extends Seeder
{
	/**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('company_policy')->insert([
            'company_id' => 1,
            'company_policy_type_id' => 1,
            'name' => 'Sick Leave',
            'default_value' => 5,
            'format_value' => 'number',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy')->insert([
            'company_id' => 1,
            'company_policy_type_id' => 1,
            'name' => 'Vacation Leave',
            'default_value' => 5,
            'format_value' => 'number',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy')->insert([
            'company_id' => 1,
            'company_policy_type_id' => 4,
            'name' => 'Company Grace Period(Mins.)',
            'default_value' => 10,
            'format_value' => 'number',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy')->insert([
            'company_id' => 2,
            'company_policy_type_id' => 1,
            'name' => 'Sick Leave',
            'default_value' => 5,
            'format_value' => 'number',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy')->insert([
            'company_id' => 2,
            'company_policy_type_id' => 1,
            'name' => 'Vacation Leave',
            'default_value' => 5,
            'format_value' => 'number',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy')->insert([
            'company_id' => 2,
            'company_policy_type_id' => 4,
            'name' => 'Company Grace Period(Mins.)',
            'default_value' => 10,
            'format_value' => 'number',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy')->insert([
            'company_id' => 3,
            'company_policy_type_id' => 1,
            'name' => 'Sick Leave',
            'default_value' => 5,
            'format_value' => 'number',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy')->insert([
            'company_id' => 3,
            'company_policy_type_id' => 1,
            'name' => 'Vacation Leave',
            'default_value' => 5,
            'format_value' => 'number',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('company_policy')->insert([
            'company_id' => 3,
            'company_policy_type_id' => 4,
            'name' => 'Company Grace Period(Mins.)',
            'default_value' => 10,
            'format_value' => 'number',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}