<?php

use App\Permission;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
// use Laracasts\TestDummy\Factory as TestDummy;

class WorkingScheduleTableSeeder extends Seeder
{
    public function run()
    {   
        DB::table('working_schedule')->insert([
            'company_id' => 1,
            'name' => 'Default Company Schedule',
            'flexible_time' => 0,
            'grace_period_mins' => 10,
            'time_in' => '09:00:00',
            'time_out' => '17:00:00',
            'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]',
            'policy_id' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => 1
        ]);

        DB::table('working_schedule')->insert([
            'company_id' => 1,
            'name' => 'Default Company Monday Schedule',
            'flexible_time' => 0,
            'grace_period_mins' => 10,
            'time_in' => '09:00:00',
            'time_out' => '16:00:00',
            'days' => '["monday"]',
            'policy_id' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => 1
        ]);

        DB::table('working_schedule')->insert([
            'company_id' => 1,
            'name' => 'Company IT Department Schedule',
            'flexible_time' => 0,
            'grace_period_mins' => 10,
            'time_in' => '09:00:00',
            'time_out' => '19:00:00',
            'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]',
            'policy_id' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => 1
        ]);

        DB::table('working_schedule')->insert([
            'company_id' => 1,
            'name' => 'Company Marketing Department Schedule',
            'flexible_time' => 0,
            'grace_period_mins' => 10,
            'time_in' => '09:00:00',
            'time_out' => '19:30:00',
            'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]',
            'policy_id' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => 1
        ]);

        // Working Schedule
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '2', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '3', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '4', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '5', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '6', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '7', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '8', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '9', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '10', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '11', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '12', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '13', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '14', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '15', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '16', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '17', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '18', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '19', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '20', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '21', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '22', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '23', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '24', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '25', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '26', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '27', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '28', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '29', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '30', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '31', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '32', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '33', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '34', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '35', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '36', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '37', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '38', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '39', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '40', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '41', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '42', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '43', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '44', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '45', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '46', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '47', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '48', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '49', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '50', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '51', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '52', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('working_schedule')->insert(['name' => 'Regular Schedule', 'company_id' => '53', 'grace_period_mins' => '10', 'time_in' => '09:00:00', 'time_out' => '19:30:00', 'days' => '["monday", "tuesday", "wednesday", "thursday", "friday"]', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
    }
}
