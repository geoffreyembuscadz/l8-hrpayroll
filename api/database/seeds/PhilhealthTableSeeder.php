<?php

use Illuminate\Database\Seeder;

class PhilhealthTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         DB::table('philhealth')->insert([
            'bracket_id' => 1,
            'salary_base' => 0,
            'salary_ceiling' => 8999.99,
            'employee_share' => 100.00,
            'employer_share' => 100.00,
            'total_monthly_premium' => 200.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);

        DB::table('philhealth')->insert([
            'bracket_id' => 2,
            'salary_base' => 9000.00,
            'salary_ceiling' => 9999.99,
            'employee_share' => 112.50,
            'employer_share' => 112.50,
            'total_monthly_premium' => 225.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 3,
            'salary_base' => 10000.00,
            'salary_ceiling' => 10999.99,
            'employee_share' => 125.00,
            'employer_share' => 125.00,
            'total_monthly_premium' => 250.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 4,
            'salary_base' => 11000.00,
            'salary_ceiling' => 11999.99,
            'employee_share' => 137.50,
            'employer_share' => 137.50  ,
            'total_monthly_premium' => 275.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 5,
            'salary_base' => 12000.00,
            'salary_ceiling' => 12999.99,
            'employee_share' => 150.00,
            'employer_share' => 150.00,
            'total_monthly_premium' => 300.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 6,
            'salary_base' => 13000.00,
            'salary_ceiling' => 13999.99,
            'employee_share' => 162.50,
            'employer_share' => 162.50,
            'total_monthly_premium' => 325.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 7,
            'salary_base' => 14000.00,
            'salary_ceiling' => 14999.99,
            'employee_share' => 175.00,
            'employer_share' => 175.00,
            'total_monthly_premium' => 350.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 8,
            'salary_base' => 15000,
            'salary_ceiling' => 15999.99,
            'employee_share' => 187.50,
            'employer_share' => 187.50,
            'total_monthly_premium' => 375.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 9,
            'salary_base' => 16000.00,
            'salary_ceiling' => 16999.99,
            'employee_share' => 200.00,
            'employer_share' => 200.00,
            'total_monthly_premium' => 400.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 10,
            'salary_base' => 17000.00,
            'salary_ceiling' => 17999.99,
            'employee_share' => 212.50,
            'employer_share' => 212.50,
            'total_monthly_premium' => 425.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 11,
            'salary_base' => 18000.00,
            'salary_ceiling' => 18999.99,
            'employee_share' => 225.00,
            'employer_share' => 225.00,
            'total_monthly_premium' => 450.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 12,
            'salary_base' => 19000.00,
            'salary_ceiling' => 19999.99,
            'employee_share' => 237.50,
            'employer_share' => 237.50,
            'total_monthly_premium' => 475.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 13,
            'salary_base' => 20000.00,
            'salary_ceiling' => 20999.99,
            'employee_share' => 250.00,
            'employer_share' => 250.00,
            'total_monthly_premium' => 500.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 14,
            'salary_base' => 21000.00,
            'salary_ceiling' => 21999.99,
            'employee_share' => 262.50,
            'employer_share' => 262.50,
            'total_monthly_premium' => 525.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 15,
            'salary_base' => 22000.00,
            'salary_ceiling' => 22999.99,
            'employee_share' => 275.00,
            'employer_share' => 275.00,
            'total_monthly_premium' => 550.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 16,
            'salary_base' => 23000.00,
            'salary_ceiling' => 23999.99,
            'employee_share' => 287.50,
            'employer_share' => 287.50,
            'total_monthly_premium' => 575.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 17,
            'salary_base' => 24000.00,
            'salary_ceiling' => 24999.99,
            'employee_share' => 300,
            'employer_share' => 300,
            'total_monthly_premium' => 600.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 18,
            'salary_base' => 25000.00,
            'salary_ceiling' => 25999.99,
            'employee_share' => 312.50,
            'employer_share' => 312.50,
            'total_monthly_premium' => 625.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
        DB::table('philhealth')->insert([
            'bracket_id' => 19,
            'salary_base' => 26000.00,
            'salary_ceiling' => 26999.99,
            'employee_share' => 325.00,
            'employer_share' => 325.00,
            'total_monthly_premium' => 650.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
        ]);

         DB::table('philhealth')->insert([
            'bracket_id' => 20,
            'salary_base' => 27000.00,
            'salary_ceiling' => 27999.99,
            'employee_share' => 350.00,
            'employer_share' => 350.00,
            'total_monthly_premium' => 675.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);


    
        DB::table('philhealth')->insert([
            'bracket_id' => 21,
            'salary_base' => 28000.00,
            'salary_ceiling' => 28999.99,
            'employee_share' => 350.00,
            'employer_share' => 350.00,
            'total_monthly_premium' => 675.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
         
         DB::table('philhealth')->insert([
            'bracket_id' => 22,
            'salary_base' => 29000.00,
            'salary_ceiling' => 29999.99,
            'employee_share' => 362.50,
            'employer_share' => 362.50,
            'total_monthly_premium' => 700.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
         DB::table('philhealth')->insert([
            'bracket_id' => 23,
            'salary_base' => 30000.00,
            'salary_ceiling' => 30999.99,
            'employee_share' => 375.00,
            'employer_share' => 375.00,
            'total_monthly_premium' => 725.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
         DB::table('philhealth')->insert([
            'bracket_id' => 24,
            'salary_base' => 31000.00,
            'salary_ceiling' => 31999.99,
            'employee_share' => 387.50,
            'employer_share' => 387.50,
            'total_monthly_premium' => 750.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
         DB::table('philhealth')->insert([
            'bracket_id' => 25,
            'salary_base' => 32000.00,
            'salary_ceiling' => 32999.99,
            'employee_share' => 400.00,
            'employer_share' => 400.00,
            'total_monthly_premium' => 775.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
         DB::table('philhealth')->insert([
            'bracket_id' => 26,
            'salary_base' => 33000.00,
            'salary_ceiling' => 33999.99,
            'employee_share' => 412.50,
            'employer_share' => 412.50,
            'total_monthly_premium' => 800.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);
         DB::table('philhealth')->insert([
            'bracket_id' => 27,
            'salary_base' => 34000.00,
            'salary_ceiling' => 34999.99,
            'employee_share' => 425.00,
            'employer_share' => 425.00,
            'total_monthly_premium' => 825.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')

        ]);

           DB::table('philhealth')->insert([
            'bracket_id' => 28,
            'salary_base' => 35000.00,
            'salary_ceiling' => 999999.99,
            'employee_share' => 437.50,
            'employer_share' => 437.50,
            'total_monthly_premium' => 850.00,
            'created_by' => 1,
            'created_at' => DB::raw('NOW()')
        ]);
    
               
    }
}

