<?php

use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        // Temporary commented Out Employee
        // START: Sample Datas
        /*
    	DB::table('employee')->insert([
    		'employee_code' => '01-0001',
    		'firstname' => 'Joan',
    		'middlename' => 'Santos',
    		'lastname' => 'Carieon',
    		'suffix' => '-none-',
    		'department' => '1',
    		'position' => '1',
    		'skills'=>'Customer CAring',
    		'gender'=>'Female',
    		'birthdate' => '1992-02-03',
    		'blood_type' => 'O',
    		'nationality' => 'Filipino',
    		'citizenship' => 'Filipino',
    		'driving_license_no' => '31231323123213', 'salary_receiving_type' => 'daily',
    		'sss' => '344234432424',
    		'philhealth_no' => '234324324234234324',
    		'pagibig_no' => '345345353534543',
    		'tin_no' => '54423423432423423',
    		'hdmf_no' => NULL,
    		'supervisor_id' => 0,
    		'marital_status' => 'single',
    		'tel_no' => '6714344',
    		'cel_no' => '09335424212',
    		'email' => 'joan_carieon@gmail.com',
    		'employment_status' => 'provisionary',
    		'position_type' => 'subordinate', 'status' => 0,
    		'current_address' => 'Pasig City',
    		'province_address' => 'Taguig City',
    		'employee_image' => '',
    		'created_by' => 1,
            'starting_date' => date('Y-m-d H:i:s'),
    		'created_at' => date('Y-m-d H:i:s'),
    	]);

    	DB::table('employee')->insert([
    		'employee_code' => '01-0002',
    		'firstname' => 'Yao',
    		'middlename' => 'Yaoyao',
    		'lastname' => 'Yomishuga',
    		'suffix' => '-none-',
    		'department' => '1',
    		'position' => '1',
    		'skills'=>'Photoshop',
    		'gender'=>'Male',
    		'birthdate' => '1993-12-03',
    		'blood_type' => 'O',
    		'nationality' => 'Filipino',
    		'citizenship' => 'Filipino',
    		'driving_license_no' => '31231323123213', 'salary_receiving_type' => 'daily',
    		'sss' => '344234432424',
    		'philhealth_no' => '234324324234234324',
    		'pagibig_no' => '345345353534543',
    		'tin_no' => '54423423432423423',
    		'hdmf_no' => NULL,
    		'supervisor_id' => 0,
    		'marital_status' => 'single',
    		'tel_no' => '6714344',
    		'cel_no' => '09335424212',
    		'email' => 'yao_carieon@gmail.com',
    		'employment_status' => 'provisionary',
    		'position_type' => 'subordinate', 'status' => 0,
    		'current_address' => 'Manaluyong City',
    		'province_address' => 'Taguig City',
    		'employee_image' => '',
    		'created_by' => 1,
            'starting_date' => date('Y-m-d H:i:s'),
    		'created_at' => date('Y-m-d H:i:s'),
    	]);

        DB::table('employee_company')->insert([
            'company_id' => 1,
            'employee_id' => '0608',
            'company_level' => 'primary',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

          DB::table('employee_company')->insert([
            'company_id' => 1,
            'employee_id' => '0609',
            'company_level' => 'primary',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_company')->insert([
            'company_id' => 1,
            'employee_id' => '0610',
            'company_level' => 'primary',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_company')->insert([
            'company_id' => 1,
            'employee_id' => '0670',
            'company_level' => 'primary',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_company')->insert([
            'company_id' => 1,
            'employee_id' => '0606',
            'company_level' => 'primary',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_position')->insert([
            'department' => 1,
            'position' => 2,
            'employee_id' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_position')->insert([
            'department' => 2,
            'position' => 6,
            'employee_id' => 2,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_dependent')->insert([
            'employee_id' => 1,
            'name'  => 'Monica Carieon',
            'relationship' => 'mother',
            'age' => 52,
            'birthdate' => '1960-12-11',
            'created_by' => 1,
            'created_at' => date('Y-m-d')
        ]);

        DB::table('employee_dependent')->insert([
            'employee_id' => 1,
            'name'  => 'Rhonny Carieon',
            'relationship' => 'mother',
            'age' => 55,
            'birthdate' => '1962-04-01',
            'created_by' => 1,
            'created_at' => date('Y-m-d')
        ]);

        DB::table('employee_dependent')->insert([
            'employee_id' => 2,
            'name'  => 'Maeanne Yomishuga',
            'relationship' => 'mother',
            'age' => 55,
            'birthdate' => '1962-02-01',
            'created_by' => 1,
            'created_at' => date('Y-m-d')
        ]);

        DB::table('employee_working_schedule')->insert([
            'employee_id' => 1,
            'working_schedule_id' => 1,
            'approved' => 1,
            'approved_by' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_working_schedule')->insert([
            'employee_id' => 1,
            'working_schedule_id' => 2,
            'approved' => 1,
            'approved_by' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_working_schedule')->insert([
            'employee_id' => 2,
            'working_schedule_id' => 2,
            'approved' => 1,
            'approved_by' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_working_schedule')->insert([
            'employee_id' => 2,
            'working_schedule_id' => 2,
            'approved' => 1,
            'approved_by' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        // END: Sample Datas

        // ACCESS SAFEWORKS Employees
        DB::table('employee')->insert([
        'employee_code' => '0608',
        'firstname' => 'ADAMSON BERNARDO',
        'middlename' => 'ADAMSON BERNARDO',
        'lastname' => 'ENCISO',
        'suffix' => '-none-',
        'department' => '1',
        'position' => '16',
        'skills'=>'Customer CAring',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('APR 22, 1986'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-3490509-5',
        'philhealth_no' => '22-000055355-7',
        'pagibig_no' => '1211-7839-9318',
        'tin_no' => 'N/A',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0933-1346100',
        'cel_no' => '0933-1346100',
        'email' => 'ADAMSONBERNARDO.ENCISO@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '#60 SAMONTE ST. DILIMAN QUEZON CITY',
        'province_address' => '#60 SAMONTE ST. DILIMAN QUEZON CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('MAR 02, 2015'), 'Y-m-d'),
        'end_date' => date_format(date_create('AUG 02, 2015'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee')->insert([
        'employee_code' => '0609',
        'firstname' => 'ALFRED ISRAEL',
        'middlename' => 'ALFRED ISRAEL',
        'lastname' => 'QUIOGUE',
        'suffix' => '-none-',
        'department' => '1',
        'position' => '16',
        'skills'=>'Customer CAring',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('JUNE 08, 1987'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-5012378-3',
        'philhealth_no' => '03-200209831-7',
        'pagibig_no' => '1211-4555-1991',
        'tin_no' => '457-968-368',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0907-9788932',
        'cel_no' => '0907-9788932',
        'email' => 'ALFREDISRAEL.QUIOGUE@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '#60 SAMONTE ST. BRGY. HOLY SPIRIT QUEZON CITY',
        'province_address' => '#60 SAMONTE ST. BRGY. HOLY SPIRIT QUEZON CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('MAR 02, 2015'), 'Y-m-d'),
        'end_date' => date_format(date_create('AUG 02, 2015'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee')->insert([
        'employee_code' => '0610',
        'firstname' => 'MARCIAL CRUZ',
        'middlename' => 'MARCIAL CRUZ',
        'lastname' => 'ROSAS',
        'suffix' => '-none-',
        'department' => '1',
        'position' => '16',
        'skills'=>'Customer CAring',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('AUG 09, 1975'),'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '33-5260269-0',
        'philhealth_no' => '19-090437786-8',
        'pagibig_no' => '1400-0104-8973',
        'tin_no' => '240-752-889',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0912-6858856',
        'cel_no' => '0912-6858856',
        'email' => 'MARCIALCRUZ.ROSAS@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'PLATTER ST. SAN ANDRES FLOODWAY CAINTA RIZAL',
        'province_address' => 'PLATTER ST. SAN ANDRES FLOODWAY CAINTA RIZAL',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('MAR 11, 2015'),'Y-m-d'),
        'end_date' => date_format(date_create('AUG 11, 2015'),'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee')->insert([
        'employee_code' => '0670',
        'firstname' => 'LUDRICK ROY',
        'middlename' => 'LUDRICK ROY',
        'lastname' => 'RESUELLO',
        'suffix' => '-none-',
        'department' => '1',
        'position' => '16',
        'skills'=>'Customer CAring',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('FEB 14, 1992'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-3911393-4',
        'philhealth_no' => '09-201435910-0',
        'pagibig_no' => '1211-4567-2986',
        'tin_no' => 'N/A',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0949-6130664',
        'cel_no' => '0949-6130664',
        'email' => 'LUDRICKROY.RESUELLO@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '116 BLK. 35WELL COMPD., MANDALUYONG CITY',
        'province_address' => '116 BLK. 35WELL COMPD., MANDALUYONG CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('MAR 28, 2015'), 'Y-m-d'),
        'end_date' => date_format(date_create('AUG 28, 2015'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee')->insert([
        'employee_code' => '0606',
        'firstname' => 'CHRISTOPHER LORETO',
        'middlename' => 'CHRISTOPHER LORETO',
        'lastname' => 'ROSAS',
        'suffix' => '-none-',
        'department' => '1',
        'position' => '16',
        'skills'=>'Customer CAring',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('OCT 23, 1976'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '33-4124259-5',
        'philhealth_no' => '19-201027175-0',
        'pagibig_no' => '1211-4565-1282',
        'tin_no' => '195-264-763',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0909-3741972',
        'cel_no' => '0909-3741972',
        'email' => 'CHRISTOPHERLORETO.ROSAS@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'L3 B4 PARKLANE SUBD. GBII SAN MATEO RIZAL',
        'province_address' => 'L3 B4 PARKLANE SUBD. GBII SAN MATEO RIZAL',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('MAR 12, 2015'), 'Y-m-d'),
        'end_date' => date_format(date_create('AUG 12, 2015'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_position')->insert([
            'department' => 1,
            'position' => 16,
            'employee_id' => 3,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_position')->insert([
            'department' => 1,
            'position' => 16,
            'employee_id' => 4,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_position')->insert([
            'department' => 1,
            'position' => 16,
            'employee_id' => 5,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_position')->insert([
            'department' => 1,
            'position' => 16,
            'employee_id' => 6,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_position')->insert([
            'department' => 1,
            'position' => 16,
            'employee_id' => 7,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_rate')->insert([
            'employee_id' => 3,
            'daily_rate' => 481,
            'company_id' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_rate')->insert([
            'employee_id' => 4,
            'daily_rate' => 481,
            'company_id' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_rate')->insert([
            'employee_id' => 5,
            'daily_rate' => 481,
            'company_id' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_rate')->insert([
            'employee_id' => 6,
            'daily_rate' => 481,
            'company_id' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_rate')->insert([
            'employee_id' => 7,
            'daily_rate' => 481,
            'company_id' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        // AKARI Lightning & Tech Corp
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'SHIELLA',
        'middlename' => 'BORDEJOS',
        'lastname' => 'CORTEZA','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('JAN. 19, 1986'), "Y-m-d"),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '33-7982384-7',
        'philhealth_no' => '01-050776944-3',
        'pagibig_no' => '1210-3476-8741',
        'tin_no' => '270-404-158',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0928-7480592',
        'cel_no' => '0928-7480592',
        'email' => 'SHIELLA.CORTEZA@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '6786 ALBATROS ST. DON MARIANO CAINTA RIZAL',
        'province_address' => '6786 ALBATROS ST. DON MARIANO CAINTA RIZAL',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('SEPT. 23, 2015'), 'Y-m-d'),
        'end_date' => date_format(date_create('FEB. 23, 2016'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '1656',
        'firstname' => 'CHRISTIAN',
        'middlename' => 'EGLESIA',
        'lastname' => 'BERMUDES','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('NOV. 17, 1986'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '33-9411483-2',
        'philhealth_no' => '0305-0177-5211',
        'pagibig_no' => '1211-4748-8533',
        'tin_no' => 'N/A',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0921-4373167',
        'cel_no' => '0921-4373167',
        'email' => 'CHRISTIAN.BERMUDES@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '19 Ilang-Ilang St. Marikina City',
        'province_address' => '19 Ilang-Ilang St. Marikina City',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('SEPT. 03, 2015'), 'Y-m-d'),
        'end_date' => date_format(date_create('FEB. 03, 2016'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '1656',
        'firstname' => 'ELJHON',
        'middlename' => 'YALUNG',
        'lastname' => 'JAVIER','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('FEB 14, 1995'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-4446133-8',
        'philhealth_no' => '0305-1205-9240',
        'pagibig_no' => '1211-4051-4719',
        'tin_no' => '468-256-926',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0912-8185240',
        'cel_no' => '0912-8185240',
        'email' => 'ELJHON.JAVIER@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'BLK 136 LOT 1 PHASE 5 UPPER BICUTAN, TAGUIG CITY',
        'province_address' => 'BLK 136 LOT 1 PHASE 5 UPPER BICUTAN, TAGUIG CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('APR 11, 2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('SEPT 11, 2016'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        
        
        DB::table('employee')->insert([
        'employee_code' => '1725',
        'firstname' => 'JACKYLIN',
        'middlename' => 'OLIGAN',
        'lastname' => 'HALOG','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('MAR 18, 1988'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-5817541-6',
        'philhealth_no' => '0102-6011-5311',
        'pagibig_no' => '1211-8111-5633',
        'tin_no' => '500-631-989',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0908-5882540',
        'cel_no' => '0908-5882540',
        'email' => 'JACKYLIN.HALOG@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'ZONE 7 SITIO KAUNLARAN WESTERN BICUTAN,TAGUIG CITY',
        'province_address' => 'ZONE 7 SITIO KAUNLARAN WESTERN BICUTAN,TAGUIG CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('APR 29, 2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('SEPT 29, 2016'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'CHRISTIAN',
        'middlename' => 'ZOMIL',
        'lastname' => 'ZULUETA','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('OCT.2,1992'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-2856715-1',
        'philhealth_no' => '02-050869762-8',
        'pagibig_no' => '1210-2452-1358',
        'tin_no' => '311-053-325',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0923-313-6648',
        'cel_no' => '0923-313-6648',
        'email' => 'CHRISTIAN.ZULUETA@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'BLK 145 LOT 6 CENTRAL BICUTAN, TAGUIG CITY',
        'province_address' => 'BLK 145 LOT 6 CENTRAL BICUTAN, TAGUIG CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('JUNE 28,2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('NOV 27,2016'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        
        DB::table('employee')->insert([
        'employee_code' => '1761',
        'firstname' => 'ANISA',
        'middlename' => 'CASTILLO',
        'lastname' => 'SALIKULA','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format( date_create('DEC 10,1991'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '09-3123555-9',
        'philhealth_no' => '02-026715988-3',
        'pagibig_no' => '1211-6880-8237',
        'tin_no' => 'N/A',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0946-231-3437',
        'cel_no' => '0946-231-3437',
        'email' => 'ANISA.SALIKULA@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '2423 CUENCA ST., PASAY CITY',
        'province_address' => '2423 CUENCA ST., PASAY CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('JUNE 21,2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('NOV 20,2016'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '1797',
        'firstname' => 'HARLEY',
        'middlename' => 'HERRERA',
        'lastname' => 'MORICO','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('SEPT. 10, 1991'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-1628799-3',
        'philhealth_no' => '10-050176815-9',
        'pagibig_no' => '1211-6331-9378',
        'tin_no' => '325-886-983',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0916-292-5570',
        'cel_no' => '0916-292-5570',
        'email' => 'HARLEY.MORICO@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'BRGY. PINAGKAISAHAN DANLIA ST., MAKATI CITY',
        'province_address' => 'BRGY. PINAGKAISAHAN DANLIA ST., MAKATI CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('AUG 2, 2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('JAN 1, 2017'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        
        
        DB::table('employee')->insert([
        'employee_code' => '1815',
        'firstname' => 'NOEME ',
        'middlename' => 'KAAMINO',
        'lastname' => 'OLOR','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => '1990-01-01',
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-5014371-8',
        'philhealth_no' => '01-200147949-9',
        'pagibig_no' => ' 1211-6172-1773',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '',
        'cel_no' => '',
        'email' => 'NOEME .OLOR@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '',
        'province_address' => '',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('8/1/2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('JAN 10,2017'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'ALLAN',
        'middlename' => 'OSIO',
        'lastname' => 'CAICDOY','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('DEC 10, 1993'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '01-2174877-5',
        'philhealth_no' => '01-051359337-1',
        'pagibig_no' => '1210-2925-2274',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '',
        'cel_no' => '',
        'email' => 'ALLAN.CAICDOY@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'SITIO II MANGGAHAN MERVILLE, PARAÑAQUE',
        'province_address' => 'SITIO II MANGGAHAN MERVILLE, PARAÑAQUE',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => '2016-10-01',
        'end_date' => '2017-12-31',
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'RENE BOY',
        'middlename' => 'MARATA',
        'lastname' => 'RISMUNDO','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('OCT 29 1992'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-6099693-9',
        'philhealth_no' => '11-025490607-7',
        'pagibig_no' => 'N/A',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '9773027612',
        'cel_no' => '9773027612',
        'email' => 'RENE BOY.RISMUNDO@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '1 NUEVA VISCAYA BAGONG BANTAY QUEZON CITY',
        'province_address' => '1 NUEVA VISCAYA BAGONG BANTAY QUEZON CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => '2016-10-01',
        'end_date' => '2017-12-31',
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'KATRINA',
        'middlename' => 'DENOLO',
        'lastname' => 'NORTE','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('6/1/1993'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-2574472-8',
        'philhealth_no' => '02-050902103-2',
        'pagibig_no' => '1210-6836-1674',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '9480831244',
        'cel_no' => '9480831244',
        'email' => 'KATRINA.NORTE@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '548 T. SULIT ST. BRGYAGUHO PATEROS',
        'province_address' => '548 T. SULIT ST. BRGYAGUHO PATEROS',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => '2016-10-01',
        'end_date' => '2017-12-31',
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'JEFFREY ',
        'middlename' => 'DE GUZMAN',
        'lastname' => 'CRUZ','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('DEC 05 1997'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-5575829-2',
        'philhealth_no' => '0102-5887-2326',
        'pagibig_no' => '1211-6096-9941',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '91774470965',
        'cel_no' => '91774470965',
        'email' => 'JEFFREY .CRUZ@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '27 LIBIS ST ZONE 5 CENTRAL SIGNAL TAGUIG CITY',
        'province_address' => '27 LIBIS ST ZONE 5 CENTRAL SIGNAL TAGUIG CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('SEPT 13 2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('4/18/2017'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'OLIVER ',
        'middlename' => 'GALIMBA',
        'lastname' => 'MORANTE','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('JAN 08 1997'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-5596881-7',
        'philhealth_no' => '11-252702794-5',
        'pagibig_no' => '',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0907-1991843',
        'cel_no' => '0907-1991843',
        'email' => 'OLIVER .MORANTE@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'BLK 5 DELOS SANTOS DAMAYAN LAGI, QUEZON CITY',
        'province_address' => 'BLK 5 DELOS SANTOS DAMAYAN LAGI, QUEZON CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('Jul 1 2016'), 'Y-m-d'),
        'end_date' => date_format( date_create('Nov 30 2016'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'ALLAN',
        'middlename' => 'MIDTIMBANG',
        'lastname' => 'SEMA','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('9/9/1993'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-5790443-9',
        'philhealth_no' => '01-025928636-6',
        'pagibig_no' => '1211-46434713',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0995-9951-608',
        'cel_no' => '0995-9951-608',
        'email' => 'ALLAN.SEMA@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'BLK147 LOT 26 CENTERAL BICUTAN TAGUIG CITY',
        'province_address' => 'BLK147 LOT 26 CENTERAL BICUTAN TAGUIG CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('10/7/2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('4/6/2017'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'MONTABNI',
        'middlename' => 'BARCENAS',
        'lastname' => 'SIONILO','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format( date_create('7/21/1990'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-2880495-3',
        'philhealth_no' => '03-050830278-5',
        'pagibig_no' => '1210-4042-5762',
        'tin_no' => '4141-86830',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0926-4752-705',
        'cel_no' => '0926-4752-705',
        'email' => 'MONTABNI.SIONILO@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'P.RODRIGUEA ST., BRGY SAN RAFAEL , RODRIGUEZ RIZAL',
        'province_address' => 'P.RODRIGUEA ST., BRGY SAN RAFAEL , RODRIGUEZ RIZAL',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('5/16/2016'), 'Y-m-d'),
        'end_date' => date_format( date_create('10/17/2016'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'DAHREN',
        'middlename' => 'OBLEFIAS',
        'lastname' => 'ADAYO','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('11/24/1990'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-3809829-6',
        'philhealth_no' => '2200-0163-2458',
        'pagibig_no' => '1211-3303-4453',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0935-898-2623',
        'cel_no' => '0935-898-2623',
        'email' => 'DAHREN.ADAYO@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'BLK LOT2 VIVA HOMES SUBD. DAMARIÑS CAVITE ',
        'province_address' => 'BLK LOT2 VIVA HOMES SUBD. DAMARIÑS CAVITE ',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => '2016-10-01',
        'end_date' => '2017-12-31',
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'DIOSA',
        'middlename' => 'AVILA ',
        'lastname' => 'TOMENIO','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format( date_create('3/7/1994'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-4062424-7',
        'philhealth_no' => '0102-5615-3748',
        'pagibig_no' => '1211-1669-4091',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0946-705-3018',
        'cel_no' => '0946-705-3018',
        'email' => 'DIOSA.TOMENIO@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'ZONE 12, UNIT II BAYBAY SAPA, MAYAMOT ANTIPOLO CITY',
        'province_address' => 'ZONE 12, UNIT II BAYBAY SAPA, MAYAMOT ANTIPOLO CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('11/17/2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('5/16/2017'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'MARIA CECILIA',
        'middlename' => 'BELTRAN',
        'lastname' => 'SALASBAR','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('12/26/1986'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '33-9678744-5',
        'philhealth_no' => '08-050542413-5',
        'pagibig_no' => '',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0910-922-1880',
        'cel_no' => '0910-922-1880',
        'email' => 'MARIA CECILIA.SALASBAR@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '082 C.P GAMBA ST., BRGY STO. NIÑO LOPES CALAPAN CITY',
        'province_address' => '082 C.P GAMBA ST., BRGY STO. NIÑO LOPES CALAPAN CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('11/26/2017'), 'Y-m-d'),
        'end_date' => date_format(date_create('5/25/2017'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'JENYROSE',
        'middlename' => 'VISCA',
        'lastname' => 'MACALALAD','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('10/12/1993'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '04-2504858-2',
        'philhealth_no' => '0102-5474-2037',
        'pagibig_no' => '1210-7748-8172',
        'tin_no' => '500-521-745',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0930-032-0093',
        'cel_no' => '0930-032-0093',
        'email' => 'JENYROSE.MACALALAD@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '#64 PUROK 2, BRGY TANYAG TAGUIG CITY',
        'province_address' => '#64 PUROK 2, BRGY TANYAG TAGUIG CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('11/26/2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('5/25/2017'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'MERCILITO',
        'middlename' => 'ROSAS',
        'lastname' => 'GOLOSINO','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('9/4/1993'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-6452202-4',
        'philhealth_no' => '',
        'pagibig_no' => '1211-8923-4374',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '',
        'cel_no' => '',
        'email' => 'MERCILITO.GOLOSINO@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'CALOOCAN CITY, METRO MANILA(NCR) ',
        'province_address' => 'CALOOCAN CITY, METRO MANILA(NCR) ',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('12/2/2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('6/1/2017'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'CLARIZA',
        'middlename' => 'PALMA',
        'lastname' => 'ISIDRO','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('11/20/1996'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '04-3556035-1',
        'philhealth_no' => '08-025966925-5',
        'pagibig_no' => '1211-4480-1436',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '',
        'cel_no' => '',
        'email' => 'CLARIZA.ISIDRO@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => 'BRGY.LANDAYAN SAN PEDRO LAGUNA',
        'province_address' => 'BRGY.LANDAYAN SAN PEDRO LAGUNA',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('12/22/2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('6/22/2017'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'CLEO',
        'middlename' => 'MELCHOR',
        'lastname' => 'CANSINO','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('10/28/2017'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '02-2436173-2',
        'philhealth_no' => '08-051452114-3',
        'pagibig_no' => '',
        'tin_no' => '426-216-721-000',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0908-2424-665',
        'cel_no' => '0908-2424-665',
        'email' => 'CLEO.CANSINO@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '189 LOMBOY',
        'province_address' => '189 LOMBOY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('2016-10-01'), 'Y-m-d'),
        'end_date' => date_format(date_create('2017-12-31'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'CHRISTIAN',
        'middlename' => 'SABRIDO',
        'lastname' => 'CALOLOT','suffix' => '-none-',
        'department' => '1',
        'position' => '1','skills'=>'Customer Service',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('8/23/2017'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '04-2972361-2',
        'philhealth_no' => '0805-1427-8184',
        'pagibig_no' => '1211-4006-7822',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0947-7052-105',
        'cel_no' => '0947-7052-105',
        'email' => 'CHRISTIAN.CALOLOT@gmail.com',
        'employment_status' => 'provisionary',
        'position_type' => 'subordinate', 'status' => 1,
        'current_address' => '# 85 UNIT 5 SIMREYS BLDG ALMAMANZO SUBD. BRGY. SAN ANTONIO BINAN LAGUNA',
        'province_address' => '# 85 UNIT 5 SIMREYS BLDG ALMAMANZO SUBD. BRGY. SAN ANTONIO BINAN LAGUNA',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('12/6/2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('6/21/2017'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);

        //Bramante Employees
        DB::table('employee')->insert([ 'employee_code' => '101', 'firstname' => 'JESUS RAYMUNDO', 'middlename' => 'PRESBITERO', 'lastname' => 'AGUILAR', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('09/03/1992'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-3141604-0', 'philhealth_no' => '03-025326052-8', 'pagibig_no' => '1210-8641-3135', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'tsgtech015@gmail.com', 'position_type' => 'rank and file', 'tin_no' => '', 'date_hired' => date_format(date_create('01/16/2017'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('01/01/2017'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => '167 ALFONSO XIII, BRGY. PASADENA, SAN JUAN CITY', 'cel_no' => '9331331885', 'tel_no' => '9331331885', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '102', 'firstname' => 'JENNIBE', 'middlename' => 'MAÑEBO', 'lastname' => 'BALDEO', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'Female', 'birthdate' => date_format(date_create('10/12/1990'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-1289815-3', 'philhealth_no' => '05-025291065-1', 'pagibig_no' => '', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'accounting2@thesandwichguy.com', 'position_type' => 'rank and file', 'tin_no' => '', 'date_hired' => date_format(date_create('12/05/2011'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('06/05/2011'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => '1648 AVOCADO ST. CENTINNIAL II PINAGBUHATAN PASIG CITY', 'cel_no' => '9256331887', 'tel_no' => '9256331887', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '103', 'firstname' => 'RONALYN', 'middlename' => 'PALMERO', 'lastname' => 'BARUNIA', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'Female', 'birthdate' => date_format(date_create('10/06/1995'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-3704194-3', 'philhealth_no' => '01-051673721-8', 'pagibig_no' => '1210-8379-8354', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'ronalynbarunia@yahoo.com', 'position_type' => 'rank and file', 'tin_no' => '', 'date_hired' => date_format(date_create('04/04/2016'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('09/05/2016'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => 'PHASE 1 BLK. 1 LOT 11 SAGRADA FAMILIA VILLAGE BAGUMBAYAN TAGUIG CITY', 'cel_no' => '9073356071', 'tel_no' => '9073356071', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '104', 'firstname' => 'EUNICE', 'middlename' => 'BATAC', 'lastname' => 'BONZO', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'Female', 'birthdate' => date_format(date_create('06/02/1994'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-3540539-0', 'philhealth_no' => '22-000071808-4', 'pagibig_no' => '1210-7291-1679', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'eunicebonzo.hrd@gmail.com', 'position_type' => 'rank and file', 'tin_no' => '', 'date_hired' => date_format(date_create('06/26/2015'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('06/01/2016'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => 'BLK. 8 LOT 10 SEPHVOAI IBAYO TIPAS TAGUIG CITY', 'cel_no' => '9424092693', 'tel_no' => '9424092693', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '105', 'firstname' => 'CLARK', 'middlename' => 'ARZADON', 'lastname' => 'CAPARAS', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('12/05/1975'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '', 'philhealth_no' => '', 'pagibig_no' => '', 'employment_status' => 'full-time', 'marital_status' => 'Separated', 'email' => 'hr@thesandwichguy.com', 'position_type' => 'manager', 'tin_no' => '', 'date_hired' => date_format(date_create('01/24/2014'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('07/24/2014'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => '37 ANONAS ST. PROJ. 2 QUEZON CITY', 'cel_no' => '9222922413', 'tel_no' => '9222922413', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '106', 'firstname' => 'MELISSA MAY', 'middlename' => 'CERDA', 'lastname' => 'CASTILLO', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'Female', 'birthdate' => date_format(date_create('02/21/1984'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-0485042-6', 'philhealth_no' => '01-050557061-3', 'pagibig_no' => '1020-0260-5287', 'employment_status' => 'full-time', 'marital_status' => 'Married', 'email' => 'accounting@thesandwichguy.com', 'position_type' => 'manager', 'tin_no' => '', 'date_hired' => date_format(date_create('06/02/2016'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('08/02/2016'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => '731 ANTIPORDA ST/ CALUMPANG, BINANGONAN, RIZAL', 'cel_no' => '9228190210', 'tel_no' => '9228190210', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '107', 'firstname' => 'TEE JAY', 'middlename' => 'MENDOZA', 'lastname' => 'CASTILLO', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('09/08/1980'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '33-8477176-4', 'philhealth_no' => '', 'pagibig_no' => '1210-0597-0725', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'tmcpurchaser@gmail.com', 'position_type' => 'officer', 'tin_no' => '', 'date_hired' => date_format(date_create('01/23/2017'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('01/01/2017'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => '29 VIOLA ST. SAINT DOMINIC 4 TANDANG SORA, QUEZON CITY', 'cel_no' => '9429109947', 'tel_no' => '9429109947', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '108', 'firstname' => 'EDLEEN DANIELLE', 'middlename' => 'MENDOZA', 'lastname' => 'CONCEPCION', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'Female', 'birthdate' => date_format(date_create('09/15/1989'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-3149121-4', 'philhealth_no' => '01-051362266-5', 'pagibig_no' => '1210-2751-5718', 'employment_status' => 'full-time', 'marital_status' => 'Married', 'email' => 'edleenconcepcion@gmail.com', 'position_type' => 'manager', 'tin_no' => '', 'date_hired' => date_format(date_create('06/01/2016'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('12/01/2016'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => '7 JADE ST. EMERALD VILLAGE MALANDAY MARIKINA CITY', 'cel_no' => '9274916592', 'tel_no' => '9274916592', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '109', 'firstname' => 'MILTON', 'middlename' => 'RAQUEL', 'lastname' => 'DE LOS SANTOS', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('12/10/1988'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '02-2593013-3', 'philhealth_no' => '44-265772300-0', 'pagibig_no' => '1420-0010-2850', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'milo.artistcad@gmail.com', 'position_type' => 'rank and file', 'tin_no' => '', 'date_hired' => date_format(date_create('04/29/2016'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('11/29/2016'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => '1537 UPPER BIGTE, IBAYO UPPER ROAD COC, NORZAGARAY BULACAN', 'cel_no' => '9169774904', 'tel_no' => '9169774904', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '111', 'firstname' => 'MARIA THERESA', 'middlename' => 'BASABE', 'lastname' => 'FELIX', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'Female', 'birthdate' => date_format(date_create('05/28/1986'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '33-8905949-4', 'philhealth_no' => '01-050575314-9', 'pagibig_no' => '', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'thesafelix.goldmanandsons@gmail.com', 'position_type' => 'manager', 'tin_no' => '', 'date_hired' => date_format(date_create('09/13/2016'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('03/13/2017'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => 'UNIT 403-2 AREZZO PLACE, SANDOVAL AVE. PINAGBUHATAN, PASIG CITY', 'cel_no' => '9169788559', 'tel_no' => '9169788559', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '115', 'firstname' => 'ERVIN', 'middlename' => 'INCLINO', 'lastname' => 'PATRON', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('08/20/1989'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-2645221-3', 'philhealth_no' => '01-051193425-2', 'pagibig_no' => '', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'patronervin@yahoo.com', 'position_type' => 'manager', 'tin_no' => '', 'date_hired' => date_format(date_create('01/11/2011'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('07/11/2011'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => 'Phase 8B Blk. 19 Lot 21 Pkg. 3, bagong Silang Caloocan City', 'cel_no' => '9076208928', 'tel_no' => '9076208928', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '116', 'firstname' => 'FAIRY GRACE', 'middlename' => 'HONOR', 'lastname' => 'RUADIEL', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'Female', 'birthdate' => date_format(date_create('05/26/1988'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-1718399-9', 'philhealth_no' => '02-050521743-9', 'pagibig_no' => '1211-5407-3386', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'fgruadiel_26@yahoo.com', 'position_type' => 'manager', 'tin_no' => '', 'date_hired' => date_format(date_create('07/23/2008'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('01/23/2009'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => 'PHASE 1, PINAGSAMA TAGUIG CITY', 'cel_no' => '9364640032', 'tel_no' => '9364640032', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '120', 'firstname' => 'ROMMEL RICARDO', 'middlename' => 'NAPAY', 'lastname' => 'TAGUPA', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('05/29/1991'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-2028900-0', 'philhealth_no' => '01-051340770-5', 'pagibig_no' => '', 'employment_status' => 'full-time', 'marital_status' => 'SINGLE', 'email' => 'sdirommel@gmail.com', 'position_type' => 'Rank and file', 'tin_no' => '', 'date_hired' => date_format(date_create('09/19/2016'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('06/19/2017'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => 'BLK. 3 LOT 2 LOREMAR HOMES BRGY. SAN ISIDRO ANGONO RIZAL', 'cel_no' => '9755191600', 'tel_no' => '9755191600', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '117', 'firstname' => 'NICHIE MAN', 'middlename' => 'TUBIGON', 'lastname' => 'TESIO', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('01/17/1987'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-0332613-9', 'philhealth_no' => '02-050769702-0', 'pagibig_no' => '1210-3746-5728', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'tesionichie@gmail.com', 'position_type' => 'manager', 'tin_no' => '', 'date_hired' => date_format(date_create('03/30/2010'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('10/01/2010'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => 'Blk. 18 Lot 90 San Isidro South Vill. B Montalban Rizal', 'cel_no' => '9233648811', 'tel_no' => '9233648811', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '118', 'firstname' => 'DEN MARK', 'middlename' => 'SINGSON', 'lastname' => 'VARGAS', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('04/24/1989'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-2787952-3', 'philhealth_no' => '02-050810526-7', 'pagibig_no' => '1211-5417-1268', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'vargasdenmark@yahoo.com', 'position_type' => 'manager', 'tin_no' => '', 'date_hired' => date_format(date_create('05/18/2009'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('11/19/2009'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => 'PHASE 1, PINAGSAMA TAGUIG CITY', 'cel_no' => '9354283424', 'tel_no' => '9354283424', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '119', 'firstname' => 'MARK JASON', 'middlename' => 'QUIATCHON', 'lastname' => 'VIVAS', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('06/04/1993'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-3805619-9', 'philhealth_no' => '01-051639799-9', 'pagibig_no' => '1210-9263-6749', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'markjasonvivas@gmail.com', 'position_type' => 'manager', 'tin_no' => '', 'date_hired' => date_format(date_create('05/31/2016'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('12/31/2016'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => '84 UNANG HAKBANG ST. GALAS QUEZON CITY', 'cel_no' => '9273096328', 'tel_no' => '9273096328', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '126', 'firstname' => 'JESICA', 'middlename' => 'DUMAWAL', 'lastname' => 'PRISTO', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'Female', 'birthdate' => date_format(date_create('07/07/1988'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-3111010-0', 'philhealth_no' => '‎01-051682765-9', 'pagibig_no' => '', 'employment_status' => 'full-time', 'marital_status' => 'Separated', 'email' => 'hrd.jpristo@gmail.com', 'position_type' => 'officer', 'tin_no' => '‎‎431-152-636', 'date_hired' => date_format(date_create('05/23/2017'), 'Y-m-d') ,  'regularization_date' => null , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => '734 Villa raymundo palatiw pasig city', 'cel_no' => '9276889863', 'tel_no' => '9276889863', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '127', 'firstname' => 'JULIE ANN', 'middlename' => 'SOMBILLO', 'lastname' => 'FRANCO', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'Female', 'birthdate' => date_format(date_create('07/06/1996'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-5092925-9', 'philhealth_no' => '02-251915820-9', 'pagibig_no' => '', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'Hrd.jfranco@gmail.com', 'position_type' => 'rank and file', 'tin_no' => '329-785-649', 'date_hired' => date_format(date_create('05/22/2017'), 'Y-m-d') ,  'regularization_date' => null , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => 'PH4 PKG4 BLK29 LOT15 BAGONG SILANG CALOOCAN CITY', 'cel_no' => '9979347037', 'tel_no' => '9979347037', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '124', 'firstname' => 'ARVIN', 'middlename' => 'TOPUE', 'lastname' => 'FAJARDO', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('04/02/1993'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '05-1223043-6', 'philhealth_no' => '03-025693525-9', 'pagibig_no' => '9150-6947-0429', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'arvsfajardo02@gmail.com', 'position_type' => 'rank and file', 'tin_no' => '', 'date_hired' => date_format(date_create('11/18/2014'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('05/18/2015'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => '42 A FLORES ST. BAGONG ILOG PASIG CITY', 'cel_no' => '9225276198', 'tel_no' => '9225276198', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '201', 'firstname' => 'BERNARD', 'middlename' => 'BINUCAS', 'lastname' => 'ALBAO', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('05/18/1989'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '02-2498506-6', 'philhealth_no' => '02-050603681-0', 'pagibig_no' => '1211-1600-1062', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'bern_great@yahoo.com', 'position_type' => 'rank and file', 'tin_no' => '', 'date_hired' => date_format(date_create('04/01/2013'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('10/01/2015'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => 'A. FLORES ST.BAGONG ILOG PASIG CITY', 'cel_no' => '9323896043', 'tel_no' => '9323896043', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '202', 'firstname' => 'JONATHAN', 'middlename' => 'PRADO', 'lastname' => 'BERMILLO', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('08/21/1987'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-0039588-8', 'philhealth_no' => '02-050554909-1', 'pagibig_no' => '', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'atanbermillo@gmail.com', 'position_type' => 'manager', 'tin_no' => '', 'date_hired' => date_format(date_create('01/01/2008'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('07/01/2008'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => '78 E. RODRIGUEZ ST. PUROK 3 BUNGBONG, BINANGONAN RIZAL', 'cel_no' => '9322141451', 'tel_no' => '9322141451', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '203', 'firstname' => 'JOHN ALDRIN', 'middlename' => 'LACASTE', 'lastname' => 'DELA CRUZ', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('09/06/1993'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '04-3637834-8', 'philhealth_no' => '08-025995668-8', 'pagibig_no' => '1211-5165-4020', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'john.aldrin06@gmail.com', 'position_type' => 'officer', 'tin_no' => '', 'date_hired' => date_format(date_create('01/16/2017'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('01/01/2017'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => 'BLK. 10 LOT 27 SOUTH 2 CAMELLA SPRINGVILLE, MOLINO BACOOR, CAVITE', 'cel_no' => '9178522987', 'tel_no' => '9178522987', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '205', 'firstname' => 'JOSE III', 'middlename' => 'BUTAC', 'lastname' => 'MEDINA', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('01/07/1993'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '34-3533641-4', 'philhealth_no' => '01-051581753-6', 'pagibig_no' => '1211-1690-8109', 'employment_status' => 'full-time', 'marital_status' => 'Single', 'email' => 'josemedina.tsg@gmail.com', 'position_type' => 'officer', 'tin_no' => '', 'date_hired' => date_format(date_create('11/12/2012'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('03/01/2016'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => '39 FR (C0 GEN. ESPINO ST. ZONES SIGNAL VILL. TAGUIG CITY', 'cel_no' => '9998393369', 'tel_no' => '9998393369', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);
        DB::table('employee')->insert([ 'employee_code' => '206', 'firstname' => 'CHRISTIAN BRYAN', 'middlename' => 'TOLENTINO', 'lastname' => 'POLICARPIO', 'suffix' => 'none','department'=>1,'position'=>1,'skills'=> '["customer service"]', 'gender' => 'MALE', 'birthdate' => date_format(date_create('11/27/1986'), 'Y-m-d'), 'blood_type' => 'O', 'sss' => '', 'philhealth_no' => '', 'pagibig_no' => '', 'employment_status' => 'full-time', 'marital_status' => 'Married', 'email' => 'criz_racingdevelopment@yahoo.com.ph', 'position_type' => 'officer', 'tin_no' => '', 'date_hired' => date_format(date_create('11/29/2014'), 'Y-m-d') ,  'regularization_date' => date_format(date_create('05/29/2015'), 'Y-m-d H:i:s') , 'citizenship' => 'Filipino', 'nationality' => 'Filipino', 'province_address' => 'none', 'current_address' => 'UNIT 405 2529 NRM BLDG. LEGARDA ST. SAMPALOC, MANILA', 'cel_no' => '9178603935', 'tel_no' => '9178603935', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => '1' ]);

        // Inactive employees
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'RAYMUND',
        'middlename' => 'AGGULLEN',
        'lastname' => 'GUPAAL',
        'suffix' => '-none-',
        'department' => '1',
        'position' => '1',
        'skills'=>'Customer CAring',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('01/01/1980'),'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '33-0228070-3',
        'philhealth_no' => 'N/A',
        'pagibig_no' => 'N/A',
        'tin_no' => 'N/A',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0928-5594507',
        'cel_no' => '0928-5594507',
        'email' => 'RAYMUND.GUPAAL@gmail.com',
        'employment_status' => 'Full-time',
        'position_type' => 'subordinate', 'status' => 0,
        'current_address' => '82-D BROADWAY QUEZON,CITY',
        'province_address' => '82-D BROADWAY QUEZON,CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('SEPT. 09, 2015'), 'Y-m-d'),
        'end_date' => date_format(date_create('FEB. 09, 2015'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'JOBERT',
        'middlename' => 'ELOPRE',
        'lastname' => 'ODOÑO',
        'suffix' => '-none-',
        'department' => '1',
        'position' => '1',
        'skills'=>'Customer CAring',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('JULY 16, 1988'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-1955649-8',
        'philhealth_no' => '0105-0865-9976',
        'pagibig_no' => '1210-8686-8536',
        'tin_no' => '307-957-730',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0908-9421447',
        'cel_no' => '0908-9421447',
        'email' => 'JOBERT.ODOÑO@gmail.com',
        'employment_status' => 'Full-time',
        'position_type' => 'subordinate', 'status' => 0,
        'current_address' => 'BLK 4 LOT 32 SEASIDE SQUARE TAMBO, PARAÑAQUE CITY',
        'province_address' => 'BLK 4 LOT 32 SEASIDE SQUARE TAMBO, PARAÑAQUE CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('MAR 18, 2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('AUG 18, 2016'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('employee')->insert([
        'employee_code' => '',
        'firstname' => 'EMERSON JOHN',
        'middlename' => 'BAGARES',
        'lastname' => 'BESINA',
        'suffix' => '-none-',
        'department' => '1',
        'position' => '1',
        'skills'=>'Customer CAring',
        'gender'=>'Male',
        'birthdate' => date_format(date_create('6/1/1996'), 'Y-m-d'),
        'blood_type' => 'O',
        'nationality' => 'Filipino',
        'citizenship' => 'Filipino',
        'sss' => '34-4703020-3',
        'philhealth_no' => '0102-5660-1473',
        'pagibig_no' => '1210-9599-8811',
        'tin_no' => '',
        'supervisor_id' => 0,
        'marital_status' => 'single',
        'tel_no' => '0915-639-5578',
        'cel_no' => '0915-639-5578',
        'email' => 'EMERSON JOHN.BESINA@gmail.com',
        'employment_status' => 'Full-time',
        'position_type' => 'subordinate', 'status' => 0,
        'current_address' => 'BRGY 36 LOT 20 LOZAÑES ST. BRGY CENTRAL TAGUIG CITY',
        'province_address' => 'BRGY 36 LOT 20 LOZAÑES ST. BRGY CENTRAL TAGUIG CITY',
        'employee_image' => '',
        'created_by' => 1,
        'starting_date' => date_format(date_create('10/24/2016'), 'Y-m-d'),
        'end_date' => date_format(date_create('4/27/2017'), 'Y-m-d'),
        'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 8, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 9, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 18, 'department' => 1, 'employee_id' => 10, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 18, 'department' => 1, 'employee_id' => 11, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 18, 'department' => 1, 'employee_id' => 12, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 18, 'department' => 1, 'employee_id' => 13, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 18, 'department' => 1, 'employee_id' => 14, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 18, 'department' => 1, 'employee_id' => 15, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 16, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 17, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 18, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 19, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 20, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 21, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 22, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 23, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 24, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 25, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 26, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 27, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 28, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 29, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 30, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 31, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 32, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);
        DB::table('employee_position')->insert(['position' => 17, 'department' => 1, 'employee_id' => 33, 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1]);

DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '8', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '1656', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '1656', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '1725', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '12', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '1797', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '1815', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '15', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '16', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '17', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '18', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '19', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '20', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '21', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '22', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '23', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '24', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '25', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '26', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '27', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '28', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '29', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '30', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '31', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '32', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        DB::table('employee_company')->insert([ 'company_id' => 2, 'employee_id' => '33', 'company_level' => 'primary', 'created_by' => 1, 'created_at' => date('Y-m-d H:i:s') ]);
        */
    }
}
