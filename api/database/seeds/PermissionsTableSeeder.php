<?php

use App\Permission;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
// use Laracasts\TestDummy\Factory as TestDummy;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {

        DB::table('permissions')->insert([
            'name'          => 'admin-dashboard',
            'display_name'  => 'Admin Dashboard',
            'description'   => 'admin-dashboard',
            'parent_id'     => '1',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name'          => 'admin-dashboard-create',
            'display_name'  => 'Admin Dashboard Create',
            'description'   => 'admin-dashboard-create',
            'parent_id'     => '1',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name'          => 'admin-dashboard-edit',
            'display_name'  => 'Admin Dashboard Edit',
            'description'   => 'admin-dashboard-edit',
            'parent_id'     => '1',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name'          => 'admin-dashboard-delete',
            'display_name'  => 'Admin Dashboard Delete',
            'description'   => 'admin-dashboard-delete',
            'parent_id'     => '1',
            'created_by'    => '1',
            'created_at'    => DB::raw('NOW()'),
        ]);

         DB::table('permissions')->insert([
            'name' => 'employee-dashboard',
            'display_name' => 'Employee Dashboard ',
            'description' => 'employee-dashboard',
            'parent_id' => '2',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        
       /* company */
        DB::table('permissions')->insert([ 
            'name' => 'company-view', 
            'display_name' => 'Company List', 
            'description' => 'company-view', 
            'parent_id' => '3', 
            'created_by' => '1', 
            'created_at' => DB::raw('NOW()')
        ]);

        DB::table('permissions')->insert([ 
            'name' => 'company-create', 
            'display_name' => 'Company Create', 
            'description' => 'company-create', 
            'parent_id' => '3', 
            'created_by' => '1', 
            'created_at' => DB::raw('NOW()')
        ]);

        DB::table('permissions')->insert([ 
            'name' => 'company-edit', 
            'display_name' => 'Company Edit', 
            'description' => 'company-edit', 
            'parent_id' => '3', 
            'created_by' => '1', 
            'created_at' => DB::raw('NOW()')
        ]);

        DB::table('permissions')->insert([ 
            'name' => 'company-delete', 
            'display_name' => 'Company Delete', 
            'description' => 'company-delete', 
            'parent_id' => '3', 
            'created_by' => '1', 
            'created_at' => DB::raw('NOW()')
        ]);

       /* company */

       /*  company branch */
        DB::table('permissions')->insert([ 
            'name' => 'company-branch-view', 
            'display_name' => 'Company Branch List', 
            'description' => 'company-branch-view', 
            'parent_id' => '4', 
            'created_by' => '1', 
            'created_at' => DB::raw('NOW()')
        ]);
        DB::table('permissions')->insert([ 
            'name' => 'company-branch-create', 
            'display_name' => 'Company Branch Create', 
            'description' => 'company-branch-create', 
            'parent_id' => '4', 
            'created_by' => '1', 
            'created_at' => DB::raw('NOW()')
        ]);
        DB::table('permissions')->insert([ 
            'name' => 'company-branch-edit', 
            'display_name' => 'Company Branch Edit', 
            'description' => 'company-branch-edit', 
            'parent_id' => '4', 
            'created_by' => '1', 
            'created_at' => DB::raw('NOW()')
        ]);
        DB::table('permissions')->insert([ 
            'name' => 'company-branch-delete', 
            'display_name' => 'Company Branch Delete', 
            'description' => 'company-branch-delete', 
            'parent_id' => '4', 
            'created_by' => '1', 
            'created_at' => DB::raw('NOW()')
        ]);
       /*  company branch */


       /*  company policy */ 
         DB::table('permissions')->insert([ 
            'name' => 'update-company-policy-edit', 
            'display_name' => 'Company Policy Edit', 
            'description' => 'update-company-policy-edit', 
            'parent_id' => '5', 
            'created_by' => '1', 
            'created_at' => DB::raw('NOW()')
        ]);

        DB::table('permissions')->insert([ 
            'name' => 'company-policy-delete', 
            'display_name' => 'Company Policy Delete', 
            'description' => 'company-policy-delete', 
            'parent_id' => '5', 
            'created_by' => '1', 
            'created_at' => DB::raw('NOW()')
        ]);

       /*  company policy */

       /*  company working schedule */
       DB::table('permissions')->insert([
            'name' => 'company-working-schedule-view',
            'display_name' => 'Company Working Schedule List',
            'description' => 'company-working-schedule-view',
            'parent_id' => '6',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'company-working-schedule-create',
            'display_name' => 'Company Working Schedule Create',
            'description' => 'company-working-schedule-create',
            'parent_id' => '6',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'company-working-schedule-edit',
            'display_name' => 'Company Working Schedule Edit',
            'description' => 'company-working-schedule-edit',
            'parent_id' => '6',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'company-working-schedule-delete',
            'display_name' => 'Company Working Schedule Delete',
            'description' => 'company-working-schedule-delete',
            'parent_id' => '6',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'company-update-schedule-edit',
            'display_name' => 'Company Update Schedule Edit',
            'description' => 'company-update-schedule-edit',
            'parent_id' => '6',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
       /*  company working schedule */

       /* employee */
       DB::table('permissions')->insert([
            'name' => 'employee-view',
            'display_name' => 'Employee List',
            'description' => 'employee-view',
            'parent_id' => '7',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'employee-create',
            'display_name' => 'Employee Create',
            'description' => 'employee-create',
            'parent_id' => '7',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'employee-edit',
            'display_name' => 'Employee Edit',
            'description' => 'employee-edit',
            'parent_id' => '7',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'employee-delete',
            'display_name' => 'Employee Delete',
            'description' => 'employee-delete',
            'parent_id' => '7',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'employee-attachment-view',
            'display_name' => 'Employee Attachment List',
            'description' => 'employee-attachment-view',
            'parent_id' => '7',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'employee-attachment-create',
            'display_name' => 'Employee Attachment Create',
            'description' => 'employee-attachment-create',
            'parent_id' => '7',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'employee-attachment-edit',
            'display_name' => 'Employee Attachment Edit',
            'description' => 'employee-attachment-edit',
            'parent_id' => '7',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'employee-attachment-delete',
            'display_name' => 'Employee Attachment Delete',
            'description' => 'employee-attachment-delete',
            'parent_id' => '7',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
       /* employee */

       /* attendance default */
        DB::table('permissions')->insert([
            'name' => 'attendance-view',
            'display_name' => 'Attendance List',
            'description' => 'attendance-view',
            'parent_id' => '9',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'attendance-create',
            'display_name' => 'Attendance Create',
            'description' => 'attendance-create',
            'parent_id' => '9',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'attendance-edit',
            'display_name' => 'Attendance Edit',
            'description' => 'attendance-edit',
            'parent_id' => '9',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'attendance-delete',
            'display_name' => 'Attendance Delete',
            'description' => 'attendance-delete',
            'parent_id' => '9',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
       /* attendance default */


       /* my attendance */
        DB::table('permissions')->insert([
            'name' => 'my-attendance-view',
            'display_name' => 'My Attendance List',
            'description' => 'my-attendance-view',
            'parent_id' => '11',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* my attendance */


    

        /* sa */
        DB::table('permissions')->insert([
            'name' => 'schedule-adjustments-view',
            'display_name' => 'Schedule Adjustments List',
            'description' => 'schedule-adjustments-view',
            'parent_id' => '13',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'schedule-adjustments-create',
            'display_name' => 'Schedule Adjustments Create',
            'description' => 'schedule-adjustments-create',
            'parent_id' => '13',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'schedule-adjustments-edit',
            'display_name' => 'Schedule Adjustments Edit',
            'description' => 'schedule-adjustments-edit',
            'parent_id' => '13',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'schedule-adjustments-delete',
            'display_name' => 'Schedule Ajustments Delete',
            'description' => 'schedule-adjustments-delete',
            'parent_id' => '13',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'canCreateSA',
            'display_name' => 'Can Create SA',
            'description' => 'canCreateSA',
            'parent_id' => '13',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'canEditSA',
            'display_name' => 'Can Edit SA',
            'description' => 'canEditSA',
            'parent_id' => '13',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'canApprovedSA',
            'display_name' => 'Can Approved SA',
            'description' => 'canApprovedSA',
            'parent_id' => '13',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /*  sa */

        /*  coa */
        DB::table('permissions')->insert([
            'name' => 'certificate-of-attendance-view',
            'display_name' => 'Certificate Of Attendance List',
            'description' => 'certificate-of-attendance-view',
            'parent_id' => '14',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'certificate-of-attendance-create',
            'display_name' => 'Certificate Of Attendance Create',
            'description' => 'certificate-of-attendance-create',
            'parent_id' => '14',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'certificate-of-attendance-edit',
            'display_name' => 'Certificate Of Attendance Edit',
            'description' => 'certificate-of-attendance-edit',
            'parent_id' => '14',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'certificate-of-attendance-delete',
            'display_name' => 'Certificate Of Attendance Delete',
            'description' => 'certificate-of-attendance-delete',
            'parent_id' => '14',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'canCreateCOA',
            'display_name' => 'Can Create COA',
            'description' => 'canCreateCOA',
            'parent_id' => '14',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'canEditCOA',
            'display_name' => 'Can Edit COA',
            'description' => 'canEditCOA',
            'parent_id' => '14',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'canApprovedCOA',
            'display_name' => 'Can Approved COA',
            'description' => 'canApprovedCOA',
            'parent_id' => '14',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /*   coa */

        /* ob */

        DB::table('permissions')->insert([
            'name' => 'official-business-view',
            'display_name' => 'Official Business List',
            'description' => 'official-business-view',
            'parent_id' => '15',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        
        DB::table('permissions')->insert([
            'name' => 'official-business-create',
            'display_name' => 'Official Business Create',
            'description' => 'official-business-create',
            'parent_id' => '15',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'official-business-edit',
            'display_name' => 'Official Business Edit',
            'description' => 'official-business-edit',
            'parent_id' => '15',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'official-business-delete',
            'display_name' => 'Official Business Delete',
            'description' => 'official-business-delete',
            'parent_id' => '15',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]); 
        DB::table('permissions')->insert([
            'name' => 'canCreateOB',
            'display_name' => 'Can Create OB',
            'description' => 'canCreateOB',
            'parent_id' => '15',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'canEditOB',
            'display_name' => 'Can Edit OB',
            'description' => 'canEditOB',
            'parent_id' => '15',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'canApprovedOB',
            'display_name' => 'Can Approved OB',
            'description' => 'canApprovedOB',
            'parent_id' => '15',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* ob */

        /* leave */
        DB::table('permissions')->insert([
            'name' => 'leave-view',
            'display_name' => 'Leave List',
            'description' => 'leave-view',
            'parent_id' => '16',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'leave-create',
            'display_name' => 'Leave Create',
            'description' => 'leave-create',
            'parent_id' => '16',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'leave-edit',
            'display_name' => 'Leave Edit',
            'description' => 'leave-edit',
            'parent_id' => '16',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'leave-delete',
            'display_name' => 'Leave Delete',
            'description' => 'leave-delete',
            'parent_id' => '16',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'canCreateLeave',
            'display_name' => 'Can Create Leave',
            'description' => 'canCreateLeave',
            'parent_id' => '16',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'canEditLeave',
            'display_name' => 'Can Edit Leave',
            'description' => 'canEditLeave',
            'parent_id' => '16',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'canApprovedLeave',
            'display_name' => 'Can Approved Leave',
            'description' => 'canApprovedLeave',
            'parent_id' => '16',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* leave */

        /*  ot */
        DB::table('permissions')->insert([
            'name' => 'overtime-view',
            'display_name' => 'Overtime List',
            'description' => 'overtime-view',
            'parent_id' => '17',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'overtime-create',
            'display_name' => 'Overtime Create',
            'description' => 'overtime-create',
            'parent_id' => '17',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'overtime-edit',
            'display_name' => 'Overtime Edit',
            'description' => 'overtime-edit',
            'parent_id' => '17',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'overtime-delete',
            'display_name' => 'Overtime Delete',
            'description' => 'overtime-delete',
            'parent_id' => '17',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'canCreateOvertime',
            'display_name' => 'Can Create Overtime',
            'description' => 'canCreateOvertime',
            'parent_id' => '17',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'canEditOvertime',
            'display_name' => 'Can Edit Overtime',
            'description' => 'canEditOvertime',
            'parent_id' => '17',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'canApprovedOvertime',
            'display_name' => 'Can Approved Overtime',
            'description' => 'canApprovedOvertime',
            'parent_id' => '17',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* ot */

        /* ut */

        DB::table('permissions')->insert([
            'name' => 'official-undertime-view',
            'display_name' => 'Official Undertime List',
            'description' => 'official-undertime-view',
            'parent_id' => '18',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'official-undertime-create',
            'display_name' => 'Official Undertime Create',
            'description' => 'official-undertime-create',
            'parent_id' => '18',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'official-undertime-edit',
            'display_name' => 'Official Undertime Edit',
            'description' => 'official-undertime-edit',
            'parent_id' => '18',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'official-undertime-delete',
            'display_name' => 'Official Undertime Delete',
            'description' => 'official-undertime-delete',
            'parent_id' => '18',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* ut */

        /* my sa */
        DB::table('permissions')->insert([
            'name' => 'my-sa-view',
            'display_name' => 'My Schedule Adjustment List',
            'description' => 'my-sa-view',
            'parent_id' => '19',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'my-sa-create',
            'display_name' => 'My Schedule Adjustment Create',
            'description' => 'my-sa-create',
            'parent_id' => '19',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'my-sa-edit',
            'display_name' => 'My Schedule Adjustment Edit',
            'description' => 'my-sa-edit',
            'parent_id' => '19',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* my sa */

        /* my COA */
        DB::table('permissions')->insert([
            'name' => 'my-coa-view',
            'display_name' => 'My Certificate of Attendance List',
            'description' => 'my-coa-view',
            'parent_id' => '20',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'my-coa-create',
            'display_name' => 'My Certificate of Attendance Create',
            'description' => 'my-coa-create',
            'parent_id' => '20',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'my-coa-edit',
            'display_name' => 'My Certificate of Attendance Edit',
            'description' => 'my-coa-edit',
            'parent_id' => '20',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* my COA */

        /* my OB */
        DB::table('permissions')->insert([
            'name' => 'my-ob-view',
            'display_name' => 'My Official Business List',
            'description' => 'my-ob-view',
            'parent_id' => '21',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'my-ob-create',
            'display_name' => 'My Official Business Create',
            'description' => 'my-ob-create',
            'parent_id' => '21',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'my-ob-edit',
            'display_name' => 'My Official Business Edit',
            'description' => 'my-ob-edit',
            'parent_id' => '21',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* my OB */

        /* my leave */
        DB::table('permissions')->insert([
            'name' => 'my-leave-view',
            'display_name' => 'My Leave List   ',
            'description' => 'my-leave-view',
            'parent_id' => '22',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'my-leave-create',
            'display_name' => 'My Leave Create',
            'description' => 'my-leave-create',
            'parent_id' => '22',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'my-leave-edit',
            'display_name' => 'My Leave Edit',
            'description' => 'my-leave-edit',
            'parent_id' => '22',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);


        /* my leave */

        /* my ot */
        DB::table('permissions')->insert([
            'name' => 'my-ot-view',
            'display_name' => 'My Overtime List',
            'description' => 'my-ot-view',
            'parent_id' => '23',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'my-ot-create',
            'display_name' => 'My Overtime Create',
            'description' => 'my-ot-create',
            'parent_id' => '23',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'my-ot-edit',
            'display_name' => 'My Overtime Edit',
            'description' => 'my-ot-edit',
            'parent_id' => '23',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* my ot */

        /* my ut*/
        DB::table('permissions')->insert([
            'name' => 'my-ou-view',
            'display_name' => 'My Official Undertime List',
            'description' => 'my-ou-view',
            'parent_id' => '24',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'my-ou-create',
            'display_name' => 'My Official Undertime Create',
            'description' => 'my-ou-create',
            'parent_id' => '24',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'my-ou-edit',
            'display_name' => 'My Official Undertime Edit',
            'description' => 'my-ou-edit',
            'parent_id' => '24',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'canCreateUndertime',
            'display_name' => 'Can Create Undertime',
            'description' => 'canCreateUndertime',
            'parent_id' => '18',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'canEditUndertime',
            'display_name' => 'Can Edit Undertime',
            'description' => 'canEditUndertime',
            'parent_id' => '18',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'canApprovedUndertime',
            'display_name' => 'Can Approved Undertime',
            'description' => 'canApprovedUndertime',
            'parent_id' => '18',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* my  ut */

        /* adjustment & deductions */
        DB::table('permissions')->insert([
            'name' => 'adjustment-view',
            'display_name' => 'Adjustment List',
            'description' => 'adjustment-view',
            'parent_id' => '25',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'adjustment-create',
            'display_name' => 'Adjustment Create',
            'description' => 'adjustment-create',
            'parent_id' => '25',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'adjustment-edit',
            'display_name' => 'Adjustment Edit',
            'description' => 'adjustment-edit',
            'parent_id' => '25',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'adjustment-delete',
            'display_name' => 'Adjustment Delete',
            'description' => 'adjustment-delete',
            'parent_id' => '25',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* adjustment & deductions */

        /* payroll */
        DB::table('permissions')->insert([
            'name' => 'payroll-view',
            'display_name' => 'Payroll List',
            'description' => 'payroll-view',
            'parent_id' => '26',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);


        DB::table('permissions')->insert([
            'name' => 'payroll-create',
            'display_name' => 'Payroll Create',
            'description' => 'payroll-create',
            'parent_id' => '26',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'payroll-edit',
            'display_name' => 'Payroll Edit',
            'description' => 'payroll-edit',
            'parent_id' => '26',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'payroll-delete',
            'display_name' => 'Payroll Delete',
            'description' => 'payroll-delete',
            'parent_id' => '26',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'payroll-list-view',
            'display_name' => 'Payroll List List',
            'description' => 'payroll-list-view',
            'parent_id' => '26',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);


        DB::table('permissions')->insert([
            'name' => 'payroll-list-create',
            'display_name' => 'Payroll List Create',
            'description' => 'payroll-list-create',
            'parent_id' => '26',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'payroll-list-edit',
            'display_name' => 'Payroll List Edit',
            'description' => 'payroll-list-edit',
            'parent_id' => '26',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'payroll-list-delete',
            'display_name' => 'Payroll List Delete',
            'description' => 'payroll-list-delete',
            'parent_id' => '26',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* payroll*/

        /* loan */
        DB::table('permissions')->insert([
            'name' => 'loan-view',
            'display_name' => 'Loan List',
            'description' => 'loan-view',
            'parent_id' => '27',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);


        DB::table('permissions')->insert([
            'name' => 'loan-create',
            'display_name' => 'Loan Create',
            'description' => 'loan-create',
            'parent_id' => '27',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'loan-edit',
            'display_name' => 'Loan Edit',
            'description' => 'loan-edit',
            'parent_id' => '27',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'loan-delete',
            'display_name' => 'Loan Delete',
            'description' => 'loan-delete',
            'parent_id' => '27',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* loan */

        /* users, role & permissions */
        DB::table('permissions')->insert([
            'name' => 'user-view',
            'display_name' => 'User List',
            'description' => 'user-view',
            'parent_id' => '28',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'user-create',
            'display_name' => 'User Create',
            'description' => 'user-create',
            'parent_id' => '28',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'user-edit',
            'display_name' => 'User Edit',
            'description' => 'user-edit',
            'parent_id' => '28',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'user-delete',
            'display_name' => 'User Delete',
            'description' => 'user-delete',
            'parent_id' => '28',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'role-view',
            'display_name' => 'Role List',
            'description' => 'role-view',
            'parent_id' => '29',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);


        DB::table('permissions')->insert([
            'name' => 'role-create',
            'display_name' => 'Role Create',
            'description' => 'role-create',
            'parent_id' => '29',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'role-edit',
            'display_name' => 'Role Edit',
            'description' => 'role-edit',
            'parent_id' => '29',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'role-delete',
            'display_name' => 'Role Delete',
            'description' => 'role-delete',
            'parent_id' => '29',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'permission-view',
            'display_name' => 'Permission List',
            'description' => 'permission-view',
            'parent_id' => '30',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);


        DB::table('permissions')->insert([
            'name' => 'permission-create',
            'display_name' => 'Permission Create',
            'description' => 'permission-create',
            'parent_id' => '30',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'permission-edit',
            'display_name' => 'Permission Edit',
            'description' => 'permission-edit',
            'parent_id' => '30',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'permission-delete',
            'display_name' => 'Permission Delete',
            'description' => 'permission-delete',
            'parent_id' => '30',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* users, role & permissions */

        /* adjustment type */
        DB::table('permissions')->insert([
            'name' => 'adjustment-type-view',
            'display_name' => 'Adjustment Type List',
            'description' => 'adjustment-type-view',
            'parent_id' => '31',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'adjustment-type-create',
            'display_name' => 'Adjustment Type Create',
            'description' => 'adjustment-type-create',
            'parent_id' => '31',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'adjustment-type-edit',
            'display_name' => 'Adjustment Type Edit',
            'description' => 'adjustment-type-edit',
            'parent_id' => '31',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'adjustment-type-delete',
            'display_name' => 'Adjustment Type Delete',
            'description' => 'adjustment-type-delete',
            'parent_id' => '31',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* adjustment type */

        /* department */
        DB::table('permissions')->insert([
            'name' => 'department-view',
            'display_name' => 'Department List',
            'description' => 'department-view',
            'parent_id' => '32',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'department-create',
            'display_name' => 'Department Create',
            'description' => 'department-create',
            'parent_id' => '32',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'department-edit',
            'display_name' => 'Department Edit',
            'description' => 'department-edit',
            'parent_id' => '32',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'department-delete',
            'display_name' => 'Department Delete',
            'description' => 'department-delete',
            'parent_id' => '32',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* department */

        /* holiday typr */
        DB::table('permissions')->insert([
            'name' => 'holiday-type-view',
            'display_name' => 'Holiday-type List',
            'description' => 'holiday-type-view',
            'parent_id' => '33',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'holiday-type-create',
            'display_name' => 'Holiday-type Create',
            'description' => 'holiday-type-create',
            'parent_id' => '33',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'holiday-type-edit',
            'display_name' => 'Holiday-type Edit',
            'description' => 'holiday-type-edit',
            'parent_id' => '33',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'holiday-type-delete',
            'display_name' => 'Holiday-type Delete',
            'description' => 'holiday-type-delete',
            'parent_id' => '33',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* holiday type */
        /* leave type */
        DB::table('permissions')->insert([
            'name' => 'leave-type-view',
            'display_name' => 'Leave Type List',
            'description' => 'leave-type-view',
            'parent_id' => '34',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'leave-type-create',
            'display_name' => 'Leave Type Create',
            'description' => 'leave-type-create',
            'parent_id' => '34',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'leave-type-edit',
            'display_name' => 'Leave Type Edit',
            'description' => 'leave-type-edit',
            'parent_id' => '34',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'leave-type-delete',
            'display_name' => 'Leave Type Delete',
            'description' => 'leave-type-delete',
            'parent_id' => '34',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* leave type */

        /* loan type */
        DB::table('permissions')->insert([
            'name' => 'loan-type-view',
            'display_name' => 'Loan Type List',
            'description' => 'loan-type-view',
            'parent_id' => '35',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'loan-type-create',
            'display_name' => 'Loan Type Create',
            'description' => 'loan-type-create',
            'parent_id' => '35',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'loan-type-edit',
            'display_name' => 'Loan Type Edit',
            'description' => 'loan-type-edit',
            'parent_id' => '35',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'loan-type-delete',
            'display_name' => 'Loan Type Delete',
            'description' => 'loan-type-delete',
            'parent_id' => '35',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* loan type */

        /* menu & module */
        DB::table('permissions')->insert([
            'name' => 'menu-view',
            'display_name' => 'Menu List',
            'description' => 'menu-view',
            'parent_id' => '36',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);


        DB::table('permissions')->insert([
            'name' => 'menu-create',
            'display_name' => 'Menu Create',
            'description' => 'menu-create',
            'parent_id' => '36',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'menu-edit',
            'display_name' => 'Menu Edit',
            'description' => 'menu-edit',
            'parent_id' => '36',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'menu-delete',
            'display_name' => 'Menu Delete',
            'description' => 'menu-delete',
            'parent_id' => '36',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);


        DB::table('permissions')->insert([
            'name' => 'module-view',
            'display_name' => 'Module List',
            'description' => 'module-view',
            'parent_id' => '37',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);


        DB::table('permissions')->insert([
            'name' => 'module-create',
            'display_name' => 'Module Create',
            'description' => 'module-create',
            'parent_id' => '37',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'module-edit',
            'display_name' => 'Module Edit',
            'description' => 'module-edit',
            'parent_id' => '37',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'module-delete',
            'display_name' => 'Module Delete',
            'description' => 'module-delete',
            'parent_id' => '37',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* menu * module */

        /* position */
        DB::table('permissions')->insert([
            'name' => 'position-view',
            'display_name' => 'Position List',
            'description' => 'position-view',
            'parent_id' => '38',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);


        DB::table('permissions')->insert([
            'name' => 'position-create',
            'display_name' => 'Position Create',
            'description' => 'position-create',
            'parent_id' => '38',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'position-edit',
            'display_name' => 'Position Edit',
            'description' => 'position-edit',
            'parent_id' => '38',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'position-delete',
            'display_name' => 'Position Delete',
            'description' => 'position-delete',
            'parent_id' => '38',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        /* position */
        /* sss */
        DB::table('permissions')->insert([
            'name' => 'sss-view',
            'display_name' => 'SSS List',
            'description' => 'sss-view',
            'parent_id' => '39',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'sss-create',
            'display_name' => 'SSS Create',
            'description' => 'sss-create',
            'parent_id' => '39',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'sss-edit',
            'display_name' => 'SSS Edit',
            'description' => 'sss-edit',
            'parent_id' => '39',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'sss-delete',
            'display_name' => 'SSS Delete',
            'description' => 'sss-delete',
            'parent_id' => '39',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'holiday-view',
            'display_name' => 'Holiday List',
            'description' => 'holiday-view',
            'parent_id' => '40',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'holiday-create',
            'display_name' => 'Holiday Create',
            'description' => 'holiday-create',
            'parent_id' => '40',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'holiday-edit',
            'display_name' => 'Holiday Edit',
            'description' => 'holiday-edit',
            'parent_id' => '40',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('permissions')->insert([
            'name' => 'holiday-delete',
            'display_name' => 'Holiday Delete',
            'description' => 'holiday-delete',
            'parent_id' => '40',
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);


    }
}

