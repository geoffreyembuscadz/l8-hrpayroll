<?php

use Illuminate\Database\Seeder;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('position')->insert([
            'name' => 'Accounting Manager',
            'description' => 'Accounting Manager',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Accounting Officer',
            'description' => 'Accounting Officer',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Accounting Staff',
            'description' => 'Accounting Staff',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Admin Assistant',
            'description' => 'Accounting Staff',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Admin Head',
            'description' => 'Accounting Staff',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Architech',
            'description' => 'Architech',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Area Manager',
            'description' => 'Area Manager',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Bakery Supervisor',
            'description' => 'Bakery Supervisor',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Corporate Chef',
            'description' => 'Corporate Chef',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

    	DB::table('position')->insert([
            'name' => 'COO',
            'description' => 'chief of operations',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Graphic Artist',
            'description' => 'Graphic Artist',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'HR Assistant',
            'description' => 'HR Assistant',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'HR Manager',
            'description' => 'HR Manager',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'HR Officer',
            'description' => 'HR Officer',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Comissary Manager',
            'description' => 'chief of operations',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'IT Head Manager',
            'description' => 'Head of IT Department',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'IT Tech Suppoer Associate',
            'description' => 'IT Tech support associate',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'IT Software Engineer',
            'description' => 'Full Stack Programmer',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Industrial Engineer',
            'description' => 'Industrial Engineer',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'UX/UI Designer',
            'description' => 'UX/UI Designer',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Maintenance Officer',
            'description' => 'Maintenance Officer',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Human Resource Manager',
            'description' => 'HR Head',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Human Resource Associate Assistant',
            'description' => 'HR assistant',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Financial Head Manager',
            'description' => 'Head of finance department',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'CPA',
            'description' => 'finance department',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Bookeeper',
            'description' => 'finance department',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Marketing Manager',
            'description' => 'Head of marketing department',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Marketing Officer',
            'description' => 'marketing officer',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'RDU Officer',
            'description' => 'RDU Officer',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Purchasing Officer',
            'description' => 'purchasing officer',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Sales Associate',
            'description' => 'marketing department',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Product Manager',
            'description' => 'marketing department',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Quality Assurance Engineer',
            'description' => 'quality control department',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Quality Assurance Lead',
            'description' => 'quality control department',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Window Cleaner',
            'description' => 'Window Cleaner Staffs',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Roving Promodiser',
            'description' => 'Roving Promodiser',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('position')->insert([
            'name' => 'Promodiser',
            'description' => 'Promodiser',
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}

