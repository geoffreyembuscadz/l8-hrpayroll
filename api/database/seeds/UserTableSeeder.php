
<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Boss Admin',
            'email' => 'admin@boss.me',
            'password' => app('hash')->make('adminadmin'),
            'remember_token' => str_random(10),
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
         DB::table('users')->insert([
            'name' => 'John Paolo',
            'email' => 'me@johnpaolosantos.ph',
            'password' => app('hash')->make('adminadmin'),
            'remember_token' => str_random(10),
            'created_by' => '1',
            'created_at' => DB::raw('NOW()'),
        ]);
    

    }
}
