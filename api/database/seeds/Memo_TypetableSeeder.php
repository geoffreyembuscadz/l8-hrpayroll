<?php

use Illuminate\Database\Seeder;

class Memo_TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('memo_type')->insert([
            'name' => 'Company Memo',
            'description' => 'A record or written statement of something. 3. an informal message, especially one sent between two or more employees of the same company, concerning company business: an interoffice memorandum.',
            'created_by' => 1,
            'created_at' => DB::raw('NOW()'),
        ]);

        DB::table('memo_type')->insert([
            'name' => 'Employee Memo',
            'description' => 'An informal message, especially one sent between two or more employees of the same company, concerning company business: an interoffice memorandum. 4. Law. a writing, usually informal, containing the terms of a transaction.',
            'created_by' => 1,
            'created_at' => DB::raw('NOW()'),
        ]);

     
    }
}

