<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeDailyRate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_rate', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_id');
            $table->integer('company_id');
            $table->float('allowance_amount')->default(0);
            $table->float('hourly_rate')->default(0);
            $table->float('daily_rate')->default(0);
            $table->float('weekly_rate')->default(0);
            $table->float('monthly_rate')->default(0);
            
            $table->datetime('effectivity_date_start')->default(date('Y-m-d H:i:s'))->nullable();
            $table->datetime('effectivity_date_end')->nullable();

            $table->integer('created_by');
            $table->datetime('created_at');
            $table->integer('updated_by')->nullable();
            $table->datetime('updated_at')->nullable(); 
            $table->integer('deleted_by')->nullable();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
