<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeRateLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employee_rate_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_id');
            $table->integer('company_id');
            $table->float('hourly_rate')->default(0);
            $table->float('daily_rate')->default(0);
            $table->float('weekly_rate')->default(0);
            $table->float('monthly_rate')->default(0);

            $table->integer('created_by');
            $table->datetime('created_at');
            $table->integer('updated_by')->nullable();
            $table->datetime('updated_at')->nullable(); 
            $table->integer('deleted_by')->nullable();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
