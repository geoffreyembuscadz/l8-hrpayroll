<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ScheduleAdjustmentsRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_adjustments_request', function (Blueprint $table) {
            $table->increments('id');

            $table->string('employee_id', 200); // new

            $table->date('date');

            $table->time('start_time');

            $table->time('end_time');

            $table->time('break_start');

            $table->time('break_end');

            $table->integer('SA_id');

            $table->string('created_by', 100);

            $table->string('updated_by', 100)->nullable();

            $table->string('deleted_by', 100)->nullable();

            $table->softDeletes();

            $table->datetime('created_at');

            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
