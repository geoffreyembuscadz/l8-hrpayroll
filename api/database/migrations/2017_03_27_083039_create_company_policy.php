<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyPolicy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('company_policy', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('company_policy_type_id'); // table: company_policy_type; example values: Benefits, Employee Schedule, Salary
            $table->string('name', 250); // Sick Leave, Meal Reimbursement
            $table->text('description')->nullable();
            $table->string('start_value', 100)->nullable(); // can be treated as Minimum. 
            $table->string('end_value', 100)->nullable(); // can be treated as Maximum
            $table->string('default_value', 100)->nullable();
            $table->string('format_value', 14)->default('string'); // String, Number, Date
            $table->tinyInteger('dynamic')->default(1);

            $table->integer('created_by');
            $table->datetime('created_at');
            $table->integer('updated_by')->nullable();
            $table->datetime('updated_at')->nullable(); 
            $table->integer('deleted_by')->nullable();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
