<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrolllistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_list', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emp_id', 100)->nullable();
            $table->string('payroll_id', 100)->nullable();
            $table->string('firstname', 100)->nullable();
            $table->string('lastname', 100)->nullable();
            $table->string('branch', 100)->nullable();
            $table->float('bill_rate',8,2)->nullable();
            $table->float('asf',8,2)->nullable();
            $table->float('cola',8,2)->nullable();
            $table->float('daily_rate',8,2)->nullable();
            $table->float('no_of_days',8,2)->nullable();
            $table->float('ttl_basic_pay',8,2)->nullable();
            $table->float('allowance',8,2)->nullable();
            $table->float('total_allowance',8,2)->nullable();
            $table->float('ecola',8,2)->nullable();
            $table->float('thirteen_month',8,2)->nullable();
            $table->float('total_late',8,2)->nullable();
            $table->float('late_amount',8,2)->nullable();
            $table->float('total_undertime',8,2)->nullable();
            $table->float('undertime_amount',8,2)->nullable();
            $table->float('undertime_rule',8,2)->nullable();
            $table->float('reg_ot',8,2)->nullable();
            $table->float('pay_reg_ot',8,2)->nullable();
            $table->float('restday',8,2)->nullable();
            $table->float('pay_restday',8,2)->nullable();
            $table->float('rd_ot',8,2)->nullable();
            $table->float('pay_rd_ot',8,2)->nullable();
            $table->float('special_hol',8,2)->nullable();
            $table->float('pay_special_hol',8,2)->nullable();
            $table->float('special_hol_ot',8,2)->nullable();
            $table->float('pay_special_hol_ot',8,2)->nullable();
            $table->float('special_hol_rd',8,2)->nullable();
            $table->float('pay_special_hol_rd',8,2)->nullable();
            $table->float('special_hol_rd_ot',8,2)->nullable();
            $table->float('pay_special_hol_rd_ot',8,2)->nullable();
            $table->float('legal_hol',8,2)->nullable();
            $table->float('pay_legal_hol',8,2)->nullable();
            $table->float('legal_hol_ot',8,2)->nullable();
            $table->float('pay_legal_hol_ot',8,2)->nullable();
            $table->float('legal_hol_restday',8,2)->nullable();
            $table->float('pay_legal_hol_restday',8,2)->nullable();
            $table->float('legal_hol_rd_ot',8,2)->nullable();
            $table->float('pay_legal_hol_rd_ot',8,2)->nullable();
            $table->float('double_hol',8,2)->nullable();
            $table->float('pay_double_hol',8,2)->nullable();
            $table->float('dbl_hol_ot',8,2)->nullable();
            $table->float('pay_dbl_hol_ot',8,2)->nullable();
            $table->float('double_hol_rd',8,2)->nullable();
            $table->float('pay_double_hol_rd',8,2)->nullable();
            $table->float('dbl_hol_rd_ot',8,2)->nullable();
            $table->float('pay_dbl_hol_rd_ot',8,2)->nullable();
            $table->float('reg_nd',8,2)->nullable();
            $table->float('pay_reg_nd',8,2)->nullable();
            $table->float('rd_nd',8,2)->nullable();
            $table->float('pay_rd_nd',8,2)->nullable();
            $table->float('special_hol_nd',8,2)->nullable();
            $table->float('pay_special_hol_nd',8,2)->nullable();
            $table->float('special_hol_rd_nd',8,2)->nullable();
            $table->float('pay_special_hol_rd_nd',8,2)->nullable();
            $table->float('legal_hol_nd',8,2)->nullable();
            $table->float('pay_legal_hol_nd',8,2)->nullable();
            $table->float('legal_hol_rd_nd',8,2)->nullable();
            $table->float('pay_legal_hol_rd_nd',8,2)->nullable();
            $table->float('dbl_hol_nd',8,2)->nullable();
            $table->float('pay_dbl_hol_nd',8,2)->nullable();
            $table->float('dbl_hol_rd_nd',8,2)->nullable();
            $table->float('pay_dbl_hol_rd_nd',8,2)->nullable();
            $table->float('total_adjustment',8,2)->nullable();
            $table->float('total_deduction',8,2)->nullable();
            $table->float('total_loan',8,2)->nullable();
            $table->float('total_sss',8,2)->nullable();
            $table->float('total_philhealth',8,2)->nullable();
            $table->float('total_pagibig',8,2)->nullable();
            $table->float('total_sss_employer',8,2)->nullable();
            $table->float('total_philhealth_employer',8,2)->nullable();
            $table->float('total_pagibig_employer',8,2)->nullable();
            $table->float('ecc',8,2)->nullable();
            $table->float('total_mandatory',8,2)->nullable();
            $table->float('admin_fee',8,2)->nullable();
            $table->float('total_billable',8,2)->nullable();
            $table->float('ttl_basic_pay_billing',8,2)->nullable();
            $table->float('vat',8,2)->nullable();
            $table->float('gross_pay',8,2)->nullable();
            $table->float('net_pay',8,2)->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();

        });
    }

    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_list');
    }
}
