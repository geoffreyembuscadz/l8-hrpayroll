<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjustmentsTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adjustment_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('display_name', 100)->nullable();
            $table->string('description', 500)->nullable();
            $table->string('type', 100)->nullable();
            $table->smallInteger('taxable')->nullable()->default(0);
            $table->smallInteger('billable')->nullable()->default(0);
            $table->smallInteger('frequency')->nullable()->default(0);
            $table->smallInteger('include_payslip')->nullable()->default(0);
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();    
        });


        // Schema::create('adjustment', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('employee_id');
        //     $table->string('adjustment_type', 100)->nullable();
        //     $table->integer('adjustment_amount');
        //     $table->integer('created_by');
        //     $table->integer('updated_by')->nullable();
        //     $table->integer('deleted_by')->nullable();
        //     $table->softDeletes();
        //     $table->datetime('created_at');
        //     $table->datetime('updated_at')->nullable();    
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('adjustment');
        Schema::dropIfExists('adjustment_type');

    }
}
