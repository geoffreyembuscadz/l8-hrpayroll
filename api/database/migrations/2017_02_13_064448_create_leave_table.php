<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave', function (Blueprint $table) {
            
            $table->increments('id');

            $table->string('emp_id');

            $table->integer('status_id');

            $table->date('date_from');

            $table->date('date_to');

            $table->date('date_applied');

            $table->string('attachment')->nullable();

            $table->integer('leave_type_id');

            $table->decimal('total_hours');

            $table->decimal('total_days');

            $table->integer('total_with_pay');

            $table->text('remarks');

            $table->text('final_remarks')->nullable();

            $table->string('created_by', 100);

            $table->string('updated_by', 100)->nullable();

            $table->string('deleted_by', 100)->nullable();

            $table->softDeletes();

            $table->datetime('created_at');

            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave');
    }
}
