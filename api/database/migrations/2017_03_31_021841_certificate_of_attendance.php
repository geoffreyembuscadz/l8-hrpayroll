<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CertificateOfAttendance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('certi_of_atten', function (Blueprint $table) {
            $table->increments('id');

            $table->string('emp_id');

            $table->integer('employee_id')->nullable();

            $table->date('date');

            $table->integer('COA_type_id');

            $table->integer('status_id');

            $table->time('time');

            $table->date('date_applied');

            $table->text('remarks');

            $table->text('final_remarks')->nullable();

            $table->string('created_by', 100);

            $table->string('updated_by', 100)->nullable();

            $table->string('deleted_by', 100)->nullable();

            $table->softDeletes();

            $table->datetime('created_at');

            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
