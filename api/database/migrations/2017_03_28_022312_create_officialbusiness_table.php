<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficialbusinessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('official_business', function (Blueprint $table) {

            $table->increments('id');

            $table->string('emp_id');

            $table->integer('employee_id')->nullable();

            $table->date('start_date');

            $table->date('end_date');

            $table->integer('status_id');

            $table->date('date_applied');

            $table->string('attachment')->nullable();

            $table->text('remarks');

            $table->text('final_remarks')->nullable();

            // $table->integer('OB_type_id');

            $table->string('created_by', 100);

            $table->string('updated_by', 100)->nullable();

            $table->string('deleted_by', 100)->nullable();

            $table->softDeletes();

            $table->datetime('created_at');

            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('officialbusiness');
    }
}
