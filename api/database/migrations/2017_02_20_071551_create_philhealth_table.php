<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhilhealthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('philhealth', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bracket_id');
            $table->decimal('salary_base');
            $table->decimal('salary_ceiling');
            $table->decimal('employee_share');
            $table->decimal('employer_share');
            $table->decimal('total_monthly_premium');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('philhealth');
    }
}
