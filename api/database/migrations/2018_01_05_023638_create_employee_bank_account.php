<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeBankAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /*
        create table employee_bank_account (
            id int auto_increment, primary key(id),
            employee_id varchar(250),
            bank_name varchar(100) default null,
            bank_code varchar(100) default null,
            bank_account_number varchar(100) default null,
            created_at datetime,
            created_by int,
            updated_at datetime default null,
            updated_by int default null,
            deleted_at datetime default null,
            deleted_by int default null
            )auto_increment = 1;
        */
        Schema::create('employee_bank_account', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_id');
            $table->string('employee_code')->nullable();
            $table->string('bank_name');
            $table->string('bank_code');
            $table->string('bank_account_number');
            $table->integer('current_bank_account')->default(1);

            $table->integer('created_by');
            $table->datetime('created_at');
            $table->integer('updated_by')->nullable();
            $table->datetime('updated_at')->nullable(); 
            $table->integer('deleted_by')->nullable();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
