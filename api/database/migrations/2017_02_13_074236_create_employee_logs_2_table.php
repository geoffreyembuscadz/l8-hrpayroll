<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeLogs2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_logs2', function (Blueprint $table) {

            $table->increments('id');

            $table->string('employee_id', 100)->nullable();

            $table->datetime('start_date')->nullable();

            $table->datetime('end_date')->nullable();

            $table->integer('number_of_days')->nullable();

            $table->integer('branch_id')->nullable();

            $table->float('late',8,2)->default(0);

            $table->float('undertime',8,2)->default(0);
   
            $table->string('holiday_id')->nullable();

            $table->string('holiday_ids')->nullable();

            $table->float('overtime',8,2)->nullable();
            
            $table->float('night_differential',8,2)->nullable();

            $table->float('restday',8,2)->default(0);

            $table->string('created_by', 100);

            $table->string('updated_by', 100)->nullable();

            $table->string('deleted_by', 100)->nullable();

            $table->softDeletes();

            $table->datetime('created_at');

            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_logs2');
    }
}
