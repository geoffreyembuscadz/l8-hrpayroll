<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollLoanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_loan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payroll_id', 100)->nullable();
            $table->string('loan_id', 100)->nullable();
            $table->string('employee_id', 100)->nullable();
            $table->integer('loan_type');
            $table->integer('loan_amount');
            $table->string('type', 100)->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();    
            $table->datetime('deleted_at')->nullable();    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_loan');
    }
}
