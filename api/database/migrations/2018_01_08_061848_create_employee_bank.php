<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employee_bank', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_id', 255);
            $table->string('employee_code', 255);

            $table->string('bank_name', 255);

            $table->string('bank_code', 255);

            $table->string('bank_account_number', 255);

            $table->string('created_by', 100);

            $table->string('updated_by', 100)->nullable();

            $table->string('deleted_by', 100)->nullable();

            $table->datetime('created_at');

            $table->datetime('updated_at')->nullable();

            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
