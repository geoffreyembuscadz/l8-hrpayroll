<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHolidayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holiday', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('holiday_type_id');
            $table->string('location');
            $table->date('date');

            $table->string('created_by', 100);

            $table->string('updated_by', 100)->nullable();

            $table->string('deleted_by', 100)->nullable();

            $table->softDeletes();

            $table->datetime('created_at');

            $table->datetime('updated_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('holiday');
    }
}
