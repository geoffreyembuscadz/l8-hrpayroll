<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TemporaryStorageFromBiometrics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_stor_fr_bio', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bio_id');
            $table->string('device_name');
            $table->integer('uid');
            $table->integer('type');
            $table->time('time');
            $table->date('date');
            $table->datetime('date_time');

            $table->string('updated_by', 100)->nullable();

            $table->string('deleted_by', 100)->nullable();

            $table->datetime('created_at');

            $table->datetime('updated_at')->nullable();

            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
