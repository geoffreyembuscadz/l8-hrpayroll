<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCompanyCutoffDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_cutoff_date', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('first_cutoff_date', 30)->nullable(); // [1, 15]
            $table->string('second_cutoff_date', 30)->nullable(); // [1, lastdaymonth]
            $table->tinyInteger('days_before_notif');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();
            $table->datetime('deleted_at')->nullable();
            // A website is never been done. I wish people would understand this.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
