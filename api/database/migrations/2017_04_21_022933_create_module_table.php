<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create table for storing modules
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->unique(100);
            $table->string('display_name', 100)->nullable();
            $table->string('description', 500)->nullable();
            $table->string('icons', 100)->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('modules');
    }
}
