<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCompanyRequiredGovernmentFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_government_field', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('code', 25);

            $table->string('tax_term', 25)->nullable();
            $table->float('default_value')->nullable(); // alter table company_government_field add column default_value float default null;
            $table->tinyInteger('is_default')->default(0)->nullable(); // for default checkbox provided on the form of company policy
            $table->integer('first_cutoff')->default(0)->nullable();
            $table->integer('second_cutoff')->default(0)->nullable();
            $table->tinyInteger('daily_min_days')->nullable();

            $table->tinyInteger('active');
            
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
