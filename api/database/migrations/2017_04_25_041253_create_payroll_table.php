<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->datetime('start_time')->nullable();
            $table->datetime('end_time')->nullable();
            $table->string('type_of_run', 150)->nullable();
            $table->string('cutoff', 150)->nullable();
            $table->string('prepared_by', 100);
            $table->integer('approved')->nullable();
            $table->integer('approved_by')->nullable();
            $table->string('company', 100)->nullable();
            $table->string('branch', 100)->nullable();
            $table->string('department', 100)->nullable();
            $table->string('position', 100)->nullable();
            $table->string('removed_employee', 100)->nullable();
            // $table->string('checked_and_approved_by');
            // $table->integer('employee_id');
            // $table->integer('company_id');
            // $table->float('basic_pay')->default(0);
            // $table->integer('13th');
            // $table->integer('no_of_days');
            // $table->time('total_late')->nullable();
            // $table->float('total_late_pay')->default(0);
            // $table->time('total_undertime')->nullable();
            // $table->float('total_undertime_pay')->default(0);
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll');
    }
}
