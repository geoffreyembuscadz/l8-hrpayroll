<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_logs', function (Blueprint $table) {

            $table->increments('id');

            $table->string('employee_id', 100)->nullable();

            $table->string('attendance_type_id', 100)->nullable();

            $table->datetime('time_in')->nullable();

            $table->datetime('time_out')->nullable();

            $table->datetime('lunch_in')->nullable();

            $table->datetime('lunch_out')->nullable();

            $table->integer('branch_id');

            $table->float('late',8,2)->nullable()->default(0);

            $table->float('undertime',8,2)->nullable()->default(0);
   
            $table->integer('holiday_id')->default(0)->nullable();

            $table->integer('halfday')->nullable()->default(0);

            $table->smallInteger('overtime')->default(0)->nullable();
            
            $table->smallInteger('night_differential')->default(0)->nullable();

            $table->string('created_by', 100);

            $table->string('updated_by', 100)->nullable();

            $table->string('deleted_by', 100)->nullable();

            $table->softDeletes();

            $table->datetime('created_at');

            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_logs');
    }
}
