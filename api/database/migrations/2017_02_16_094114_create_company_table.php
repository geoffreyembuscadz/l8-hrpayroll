<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->float('cola_amount')->default(0);
            $table->string('address');
            $table->string('email')->nullable();
            $table->string('phone_number');
            $table->string('contact_person')->nullable();
            $table->string('is_admin_fee', 100)->nullable();
            $table->float('billing_vat')->nullable();
            $table->float('billing_13th')->nullable();
            $table->float('is_admin')->nullable();
            $table->float('is_admin_p')->nullable();
            $table->string('admin_fee', 100)->nullable();
            $table->integer('mother_company')->default(0);
            $table->integer('mother_company_id')->nullable();
            $table->string('default_payroll_period', 20)->nullable();
            $table->float('billing_rate')->nullable();
            $table->integer('billing_asf')->default(0);
            $table->integer('billing_breakdown')->default(0);
            $table->integer('bill_vat')->default(0);
			$table->integer('attendance_default')->default(0);
            $table->integer('late_convertion')->default(0);
            $table->integer('late_per_hour')->default(0);
            $table->integer('flexi_late')->default(0);
            $table->integer('fixed_halfday')->default(0);
            $table->tinyInteger('night_shift')->default(1);
            $table->tinyInteger('overtime_rule')->default(1);
            $table->tinyInteger('undertime_rule')->default(1);
            $table->tinyInteger('holiday_rule')->default(0);
            $table->tinyInteger('holiday_allowance')->default(0);
            $table->tinyInteger('overtime_allowance')->default(0);
            $table->tinyInteger('thirteen_month_rule')->default(0);
            $table->float('default_trainee_rate')->default(0);
            $table->text('approver_name')->nullable();
            
            // alter table company add column night_shift tinyint default 0;

            $table->integer('created_by');
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
