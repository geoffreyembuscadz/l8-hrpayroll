<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employee', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_code', 50)->nullable();
            $table->string('firstname', 75);
            $table->string('middlename', 75)->default('N/A')->nullable();
            $table->string('lastname', 75);
            $table->string('suffix', 10)->nullable();
            $table->string('department', 14)->nullable();
            $table->string('position', 24)->nullable();
            $table->string('position_type', 50)->nullable();
            $table->string('branch', 120)->nullable();
            $table->text('skills')->nullable();
            $table->string('gender', 8)->nullable();
            $table->date('birthdate')->nullable();
            $table->string('blood_type', 5)->nullable();
            $table->string('nationality', 75)->nullable();
            $table->string('citizenship', 25)->nullable();
            $table->string('driving_license_no', 50)->nullable();
            $table->string('sss', 75)->nullable();
            $table->string('philhealth_no', 50)->nullable();
            $table->string('pagibig_no', 50)->nullable();
            $table->string('tin_no', 50)->nullable();
            $table->string('hdmf_no', 50)->nullable();
            $table->text('supervisor_id')->nullable();
            $table->string('marital_status', 120)->nullable();
            $table->string('tel_no', 50)->nullable();
            $table->string('cel_no', 50)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('employment_status', 120)->nullable();
            $table->date('date_hired')->nullable();
            $table->date('starting_date')->nullable();
            $table->date('regularization_date')->nullable();
            $table->date('end_date')->nullable();

            $table->string('bank_name')->nullable();
            $table->string('bank_code')->nullable();
            $table->string('bank_account_number')->nullable();

            $table->float('e_cola')->nullable(); // alter table employee modify column e_cola float null;
            $table->string('salary_receiving_type')->nullable();
            $table->smallInteger('status')->default(1);
            $table->string('current_address', 200);
            $table->string('permanent_address', 200)->nullable();
            $table->string('province_address', 200);
            $table->string('employee_image', 255)->nullable();
            $table->date('date_terminated')->nullable();

            $table->float('sick_leave_credit', 8, 2)->default(0);
            $table->float('use_sick_leave_credit', 8, 2)->default(0);
            $table->float('vacation_leave_credit', 8, 2)->default(0);
            $table->float('use_vacation_leave_credit', 8, 2)->default(0);
            $table->float('emergency_leave_credit', 8, 2)->default(0);
            $table->float('use_emergency_leave_credit', 8, 2)->default(0);
            $table->float('maternity_leave_credit', 8, 2)->default(0);
            $table->float('use_maternity_leave_credit', 8, 2)->default(0);
            $table->float('others_leave_credit', 8, 2)->default(0);
            $table->float('use_others_leave_credit', 8, 2)->default(0);

            $table->integer('created_by');
            $table->datetime('created_at');
            $table->integer('updated_by')->nullable();
            $table->datetime('updated_at')->nullable(); 
            $table->integer('deleted_by')->nullable();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
