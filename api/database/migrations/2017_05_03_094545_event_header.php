<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_header', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->string('height');
            $table->string('fixed_week_count');
            $table->datetime('default_date');
            $table->boolean('editable');
            $table->boolean('event_limit');

            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable(); 
            $table->datetime('deleted_at')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
