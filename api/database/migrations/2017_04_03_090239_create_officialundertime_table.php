<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficialundertimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('official_undertime', function (Blueprint $table) {
            $table->increments('id');

            $table->string('emp_id');

            $table->date('date');

            $table->time('start_time');

            $table->time('end_time');

            $table->decimal('total_hours');

            $table->integer('status_id');

            $table->date('date_applied');

           $table->text('remarks');

            $table->text('final_remarks')->nullable();
            
            $table->string('created_by', 100);

            $table->string('updated_by', 100)->nullable();

            $table->string('deleted_by', 100)->nullable();

            $table->softDeletes();

            $table->datetime('created_at');

            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('official_undertime');
    }
}
