<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeDynastyLoan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        /*
        create table employee_dynasty_loan(
        id int auto_increment, primary key(id),
        employee_id varchar(255),
        company_id varchar(255),
        amount_applied float default 0.0,
        term_in_months varchar(255) default null,
        purpose_of_loan text default null,
        type_of_loan varchar(255) default null,
        payback_mode varchar(255) default null,
        co_maker_name varchar(255) default null,
        co_maker_contact_number varchar(255) default null,
        co_maker_client_assignment varchar(255) default null,
        co_maker_job_title varchar(255) default null,
        created_at datetime,
        created_by varchar(255),
        updated_at datetime default null,
        updated_by varchar(255) default null,
        deleted_at varchar(255) default null,
        deleted_by varchar(255) default null
        )auto_increment = 1;


        alter table employee_dynasty_loan 
        add column spouse_fullname text default null after co_maker_job_title;

        alter table employee_dynasty_loan 
        add column spouse_employer_name text default null after co_maker_job_title;

        alter table employee_dynasty_loan 
        add column spouse_employee_address text default null after co_maker_job_title;
        */
        Schema::create('employee_dynasty_loan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->integer('company_id');
            $table->integer('term_in_months')->default(0);
            $table->float('amount_applied',8,2)->default(0);

            $table->float('current_amount',8,2);
            $table->float('max_amount',8,2);
            $table->string('purpose_of_loan', 255)->nullable();
            $table->string('type_of_loan', 255)->nullable();
            $table->string('payback_mode', 255)->nullable();
            $table->string('type_of_residence', 255)->nullable();
            $table->text('employee_resident_type')->nullable();
            
            $table->string('co_maker_name', 255)->nullable();
            $table->string('co_maker_contact_number', 255)->nullable();
            $table->string('co_maker_client_assignment', 255)->nullable();
            $table->string('co_maker_job_title', 255)->nullable();

            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
