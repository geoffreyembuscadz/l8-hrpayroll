<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_list', function (Blueprint $table) {
            $table->increments('id');

            $table->string('employee_id', 100);

            $table->text('task');

            $table->datetime('start_task')->nullable();

            $table->datetime('end_task')->nullable();

            $table->datetime('deadline');

            $table->integer('status_id')->default(5);

            $table->string('check_by', 100)->nullable();

            $table->integer('check_status')->nullable()->default(0);

            $table->text('remarks')->nullable();

            $table->string('created_by', 100);

            $table->string('updated_by', 100)->nullable();

            $table->string('deleted_by', 100)->nullable();

            $table->softDeletes();

            $table->datetime('created_at');

            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task');
    }
}
