<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkingSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('working_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('name', 150);
            $table->tinyInteger('flexible_time')->default(0);
            $table->integer('grace_period_mins')->default(0);
            $table->time('flexi_start_min')->nullable();
            $table->time('flexi_start_max')->nullable();
            $table->time('flexi_end_min')->nullable();
            $table->time('flexi_end_max')->nullable();
            $table->time('time_in');
            $table->time('lunch_break_time')->nullable();
            $table->integer('lunch_mins_break')->nullable();
            $table->time('second_break_time')->nullable();
            $table->integer('second_mins_break')->nullable();
            $table->time('time_out');
            $table->string('days', 120)->nullable();
            $table->integer('policy_id')->nullable();
            
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
