<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulemasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('schedule_master', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->date('start_date');
        //     $table->date('end_date');

        //     $table->string('created_by', 100);
        //     $table->string('updated_by', 100)->nullable();
        //     $table->string('deleted_by', 100)->nullable();
        //     $table->softDeletes();
        //     $table->datetime('created_at');
        //     $table->datetime('updated_at')->nullable();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedulemaster');
    }
}
