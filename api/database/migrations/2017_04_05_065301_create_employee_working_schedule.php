<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeWorkingSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('employee_working_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_id', 200);
            $table->integer('working_schedule_id');
            $table->text('remarks')->nullable();
            $table->integer('approved')->default(0);
            $table->integer('approved_by')->nullable();
            
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
