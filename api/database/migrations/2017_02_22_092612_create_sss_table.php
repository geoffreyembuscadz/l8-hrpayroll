<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sss', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('salary_base');
            $table->decimal('salary_ceiling');
            $table->decimal('monthly_salary_credit');
            $table->decimal('er');
            $table->decimal('ee');
            $table->decimal('total_contribution');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable();
           


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sss');
    }
}
