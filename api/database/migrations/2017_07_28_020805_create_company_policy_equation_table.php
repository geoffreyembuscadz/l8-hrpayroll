<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyPolicyEquationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_policy_equation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('name', 250); // Sick Leave, Meal Reimbursement
            $table->text('description')->nullable();
            $table->string('value');
            $table->string('value_unit');

            $table->string('equivalent_value');
            $table->string('equivalent_unit');

            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->datetime('created_at');
            $table->datetime('updated_at')->nullable(); 
            $table->datetime('deleted_at')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
