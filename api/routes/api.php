<?php


    $api = $app->make(Dingo\Api\Routing\Router::class);

    $api->version('v1', function ($api) {

   
    $api->post('/auth/login', [
        'as' => 'api.auth.login',
        'uses' => 'App\Http\Controllers\Auth\AuthController@postLogin',
    ]);

    $api->post('/password/email',           'App\Http\Controllers\PasswordController@postEmail');
    $api->post('/password/reset/{token}',   'App\Http\Controllers\PasswordController@postReset');

    // Upload File
    $api->any('upload/',                        [ 'uses' => 'App\Http\Controllers\UploadController@postUpload']);
    // Export Pdf
    $api->get('payslip',                        [ 'uses' => 'App\Http\Controllers\PayslipExportController@exportPayslip']);
    $api->get('payslip/send',                   [ 'uses' => 'App\Http\Controllers\PayslipExportController@sendPayslip']);
    $api->get('payslip_user',                   [ 'uses' => 'App\Http\Controllers\PayslipExportByEmployeetController@exportPayslip']);
    
    $api->get('employee_export/',              [ 'uses' => 'App\Http\Controllers\EmployeeController@exportList']);
    $api->get('employee_download_upload_form/',  [ 'uses' => 'App\Http\Controllers\EmployeeController@downloadMassUploadForm' ]);
    
    $api->get('statement_of_account',           [ 'uses' => 'App\Http\Controllers\StatementOfAccountController@exportSOA']);
    $api->get('statement_of_account_loc',       [ 'uses' => 'App\Http\Controllers\StatementOfAccountByLocationController@exportSOA']);

    // $api->get('event/show',          ['uses'  => 'App\Http\Controllers\EventController@showEvent']);
    $api->get('clearance/print/data',               [ 'uses' => 'App\Http\Controllers\ClearanceController@printClearance']);

    $api->group([
        'middleware' => 'api.auth',
    ], function ($api) {
        $api->get('/', [
            'uses' => 'App\Http\Controllers\APIController@getIndex',
            'as' => 'api.index'
        ]);

        $api->get('/in', 'App\Http\Controllers\Auth\AuthController@getUser');

        //attendance-report (employee, user)

        
        $api->get('rep/late',            ['uses'  =>   'App\Http\Controllers\DashboardController@showLate']);
        $api->get('emp/count',           ['uses'  =>   'App\Http\Controllers\DashboardController@employeeCount']);
        $api->get('per/emp/rprt',        ['uses'  =>   'App\Http\Controllers\AttendanceReportController@perEmpRprt']);
        $api->get('show/filter/late',    ['uses'  =>   'App\Http\Controllers\DashboardController@getLateFilter']);
        $api->get('late/erly/rprt/fltr', ['uses'  => 'App\Http\Controllers\AttendanceReportController@lateErlyFltr']);
        $api->get('report',              ['uses'  => 'App\Http\Controllers\AttendanceReportController@report']);

        //dashboard
        $api->get('onleave/attendancereport', [ 'middleware' => ['permission:admin-dashboard'], 'uses' => 'App\Http\Controllers\LeaveTypeController@getOnLeave']);
        $api->get('attendancereport/active',  [ 'middleware' => ['permission:admin-dashboard'], 'uses' => 'App\Http\Controllers\AttendanceReportController@searchActive']);
        $api->get('present/attendancereport', [ 'middleware' => ['permission:admin-dashboard'], 'uses' => 'App\Http\Controllers\AttendanceReportController@searchTotalPresent']);
        $api->get('total/emp/filter',         [ 'uses' => 'App\Http\Controllers\AttendanceReportController@filterTotalEmployee']);
        $api->get('employee/per/company',     [ 'uses' => 'App\Http\Controllers\DashboardController@getEmployeeActive']);
        $api->get('company/name',             [ 'uses' => 'App\Http\Controllers\DashboardController@getCompanyActive']);


        //dashboard user
        $api->get('attendance/my/logs',      [ 'middleware' => ['permission:employee-dashboard'], 'uses'  => 'App\Http\Controllers\AttendanceController@getMyLogs']);
        $api->post('attendance/create_from_web', ['middleware' => ['permission:employee-dashboard'], 'uses'  => 'App\Http\Controllers\AttendanceController@createAttendanceFromWeb']);
        $api->get('attendance/logs_validation/{id}', ['middleware' => ['permission:employee-dashboard'], 'uses'  => 'App\Http\Controllers\AttendanceController@logsValidation']);
        $api->get('task/my_task/dashboard',  ['middleware' => ['permission:employee-dashboard'], 'uses'  => 'App\Http\Controllers\TaskController@getMyTaskForDashboard' ]);
        $api->get('onboarding/my_list/data',   ['middleware' => ['permission:employee-dashboard'], 'uses'  => 'App\Http\Controllers\OnboardingController@getMyOnboardingList']);
        $api->post('onboarding/update/my_status',   ['middleware' => ['permission:employee-dashboard'], 'uses'  => 'App\Http\Controllers\OnboardingController@updateMyStatus']);
        $api->get('clearance/my_list/data',   ['middleware' => ['permission:employee-dashboard'], 'uses'  => 'App\Http\Controllers\ClearanceController@getMyClearanceList']);
        $api->post('clearance/update/my_status',   ['middleware' => ['permission:employee-dashboard'], 'uses'  => 'App\Http\Controllers\ClearanceController@updateMyStatus']);


        $api->get('totalemp/attendancereport/{data}', [ 'middleware' => ['permission:admin-dashboard'], 'uses' => 'App\Http\Controllers\AttendanceReportController@searchEmp']);
        $api->get('attendance/date',                  [ 'middleware' => ['permission:admin-dashboard'], 'uses' => 'App\Http\Controllers\AttendanceReportController@selectDate']);
        $api->get('attendancereport/inactive',        [ 'middleware' => ['permission:admin-dashboard'], 'uses' => 'App\Http\Controllers\AttendanceReportController@getInactive']);
        $api->get('attendancereport/pendingleave',    [ 'middleware' => ['permission:admin-dashboard'], 'uses' => 'App\Http\Controllers\AttendanceReportController@getPendingLeave']);
        $api->get('attendancereport/graph',           [ 'middleware' => ['permission:admin-dashboard'], 'uses' => 'App\Http\Controllers\AttendanceReportController@searchReport']);

        $api->get('event/et',          [ 'middleware' => ['permission:admin-dashboard'], 'uses'  => 'App\Http\Controllers\EventController@getEventTypes']);
        $api->get('event/name',        [ 'middleware' => ['permission:admin-dashboard'], 'uses'  => 'App\Http\Controllers\EventController@getEventName']);
        $api->get('emp/request',       [ 'middleware' => ['permission:admin-dashboard'], 'uses'  => 'App\Http\Controllers\DashboardController@getAllRequest']);
        $api->get('attendance/late',   [ 'middleware' => ['permission:admin-dashboard'], 'uses'  => 'App\Http\Controllers\DashboardController@getLateEmp']);
        $api->get('pending_request',   [ 'middleware' => ['permission:admin-dashboard'], 'uses'  => 'App\Http\Controllers\DashboardController@getPendingAllRequestCount']);


        $api->post('memo/create',  [ 'uses'  => 'App\Http\Controllers\MemoController@createMemo']);
        $api->get('filter/memo',   [ 'uses'  => 'App\Http\Controllers\MemoController@getMemoFilter']);
        $api->get('memo/show',     [ 'uses'  => 'App\Http\Controllers\MemoController@getMemo']);
        $api->get('employee/memo', [ 'uses'  => 'App\Http\Controllers\MemoController@getEmpMemo']);
        $api->get('type/memo',     [ 'uses'  => 'App\Http\Controllers\MemoController@getMemoType']);
        $api->get('memo/list',     [ 'uses'  => 'App\Http\Controllers\MemoController@getMemoList']);
        $api->get('memo/{id}',     [ 'middleware' => ['permission:admin-dashboard'], 'uses'  => 'App\Http\Controllers\MemoController@show']);
        $api->post('memo',         [ 'middleware' => ['permission:admin-dashboard-create'], 'uses' => 'App\Http\Controllers\MemoController@store']);
        $api->put('memo/{id}',     [ 'middleware' => ['permission:admin-dashboard-edit'], 'uses' => 'App\Http\Controllers\MemoController@update']);
        $api->delete('memo/{id}',  [ 'middleware' => ['permission:admin-dashboard-delete'], 'uses' => 'App\Http\Controllers\MemoController@destroy']);

        $api->get('add/type',             [ 'uses'  => 'App\Http\Controllers\EventTypeController@eventType']);
        $api->get('list/event',           [ 'uses'  => 'App\Http\Controllers\EventController@eventList']);
        $api->get('event/{id}',           [ 'uses'  => 'App\Http\Controllers\EventController@show']);
        $api->post('event',               [ 'middleware' => ['permission:event-create'],'uses' => 'App\Http\Controllers\EventControlle@rstore']);
        $api->put('event/{id}',           [ 'middleware' => ['permission:event-edit'],'uses' => 'App\Http\Controllers\EventController@update']);
        $api->delete('event/{id}',        [ 'middleware' => ['permission:event-delete'],'uses' => 'App\Http\Controllers\EventController@destroy']);
        $api->post('event/event_details', [ 'middleware' => ['permission:event-create'],'uses'  => 'App\Http\Controllers\EventController@postEventDetails']);
        $api->post('event/event_type',    [ 'middleware' => ['permission:event-create'],'uses'  => 'App\Http\Controllers\EventController@postEventType']);

        // users
        $api->get('users/',        [ 'middleware' => ['permission:user-view'],'uses' => 'App\Http\Controllers\UserController@index'  ]);
        $api->get('users/{id}',    [ 'uses' => 'App\Http\Controllers\UserController@show'   ]);
        $api->post('users',        [ 'middleware' => ['permission:user-create'], 'uses' => 'App\Http\Controllers\UserController@store'  ]);
        $api->put('users/{id}',    [ 'middleware' => ['permission:user-edit'], 'uses' => 'App\Http\Controllers\UserController@update' ]);
        $api->delete('users/{id}', [ 'middleware' => ['permission:user-delete'], 'uses' => 'App\Http\Controllers\UserController@destroy']);
         $api->get('users/list/limited',    [ 'uses' => 'App\Http\Controllers\UserController@getListByIdLimit'   ]);
         $api->get('users/list/permission',    [ 'uses' => 'App\Http\Controllers\UserController@getPermissionId'   ]);

        // change password
        $api->put('password/change/{id}',    [ 'middleware' => ['permission:user-edit'], 'uses' => 'App\Http\Controllers\UserController@changePassword' ]);

        //  roles
        $api->get('role',         [ 'middleware' => ['permission:role-view'],'uses' => 'App\Http\Controllers\RoleController@index'  ]);
        $api->get('role/{id}',    [ 'uses' => 'App\Http\Controllers\RoleController@show'   ]);
        $api->post('role/',       [ 'middleware' => ['permission:role-create'], 'uses' => 'App\Http\Controllers\RoleController@store'  ]);
        $api->put('role/{id}',    [ 'middleware' => ['permission:role-edit'],   'uses' => 'App\Http\Controllers\RoleController@update' ]);
        $api->delete('role/{id}', [ 'middleware' => ['permission:role-delete'], 'uses' => 'App\Http\Controllers\RoleController@destroy']);

        //holiday_type
        
        $api->get('show/holiday_type',   [ 'uses' => 'App\Http\Controllers\Holiday_TypeController@getHolidayType' ]); 
        $api->get('holiday_type',        [ 'uses' => ['permission:permission-view'],'uses' => 'App\Http\Controllers\Holiday_TypeController@index' ]); 
        $api->get('holiday_type/{id}',   [ 'uses' => ['permission:permission-view'],'uses' => 'App\Http\Controllers\Holiday_TypeController@show' ]);
        $api->post('holiday_type',       [ 'uses' => ['permission:permission-view'],'uses' => 'App\Http\Controllers\Holiday_TypeController@store', ]);
        $api->put('holiday_type/{id}',   [ 'uses' => ['permission:permission-view'],'uses' => 'App\Http\Controllers\Holiday_TypeController@update', ]);
        $api->delete('holiday_type/{id}',[ 'uses' => ['permission:permission-view'],'uses' => 'App\Http\Controllers\Holiday_TypeController@destroy', ]);

    

        //  permissions
        $api->get('permission',         [ 'middleware' => ['permission:permission-view'],'uses' => 'App\Http\Controllers\PermissionController@index'  ]);
        $api->get('permission/{id}',    [ 'uses' => 'App\Http\Controllers\PermissionController@show'   ]);
        $api->post('permission',        [ 'middleware' => ['permission:permission-create'], 'uses' => 'App\Http\Controllers\PermissionController@store'  ]);
        $api->put('permission/{id}',    [ 'middleware' => ['permission:permission-edit'], 'uses' => 'App\Http\Controllers\PermissionController@update' ]);
        $api->delete('permission/{id}', [ 'middleware' => ['permission:permission-delete'], 'uses' => 'App\Http\Controllers\PermissionController@destroy']);

        // module
        $api->get('module',         [ 'uses'  => 'App\Http\Controllers\ModuleMainController@index' ]); 
        $api->get('module/{id}',    [ 'uses'  => 'App\Http\Controllers\ModuleMainController@show' ]);
        $api->post('module',        [ 'middleware' => ['permission:module-create'], 'uses'  => 'App\Http\Controllers\ModuleMainController@store', ]);
        $api->put('module/{id}',    [ 'middleware' => ['permission:module-edit'], 'uses'  => 'App\Http\Controllers\ModuleMainController@update', ]);
        $api->delete('module/{id}', [ 'middleware' => ['permission:module-delete'], 'uses'  => 'App\Http\Controllers\ModuleMainController@destroy', ]);

        // module
        $api->get('module_menu',        [ 'uses'  => 'App\Http\Controllers\ModuleMenuController@index' ]); 
        $api->get('module_menu/{id}',   [ 'uses'  => 'App\Http\Controllers\ModuleMenuController@show' ]);
        $api->post('module_menu',       [ 'middleware' => ['permission:menu-create'],   'uses' => 'App\Http\Controllers\ModuleMenuController@store', ]);
        $api->put('module_menu/{id}',   [ 'middleware' => ['permission:menu-edit'],     'uses' => 'App\Http\Controllers\ModuleMenuController@update', ]);
        $api->delete('module_menu/{id}',[ 'middleware' => ['permission:menu-delete'],   'uses' => 'App\Http\Controllers\ModuleMenuController@destroy', ]);

        // role module
        $api->get('role_module',         [ 'uses'  => 'App\Http\Controllers\RoleModuleController@index' ]); 
        $api->get('role_module/{id}',    [ 'uses'  => 'App\Http\Controllers\RoleModuleController@show' ]);
        $api->post('role_module',        [ 'uses' => 'App\Http\Controllers\RoleModuleController@store', ]);
        $api->put('role_module/{id}',    [ 'uses' => 'App\Http\Controllers\RoleModuleController@update', ]);
        $api->delete('role_module/{id}', [ 'uses' => 'App\Http\Controllers\RoleModuleController@destroy', ]);

        // role menu
        $api->get('role_menu',         [ 'uses'  => 'App\Http\Controllers\RoleMenuController@index' ]); 
        $api->get('role_menu/{id}',    [ 'uses'  => 'App\Http\Controllers\RoleMenuController@show' ]);
        $api->post('role_menu',        [ 'uses' => 'App\Http\Controllers\RoleMenuController@store', ]);
        $api->put('role_menu/{id}',    [ 'uses' => 'App\Http\Controllers\RoleMenuController@update', ]);
        $api->delete('role_menu/{id}', [ 'uses' => 'App\Http\Controllers\RoleMenuController@destroy', ]);

        // adjustment
        $api->get('adjustment',         [ 'middleware' => ['permission:adjustment-view'],'uses'  => 'App\Http\Controllers\AdjustmentController@index' ]); 
        $api->get('adjustment/{id}',    [ 'uses'  => 'App\Http\Controllers\AdjustmentController@show' ]);
        $api->post('adjustment',        [ 'middleware' => ['permission:adjustment-create'],'uses' => 'App\Http\Controllers\AdjustmentController@store', ]);
        $api->put('adjustment/{id}',    [ 'middleware' => ['permission:adjustment-edit'],'uses' => 'App\Http\Controllers\AdjustmentController@update', ]);
        $api->delete('adjustment/{id}', [ 'middleware' => ['permission:adjustment-delete'],'uses' => 'App\Http\Controllers\AdjustmentController@destroy', ]);

        // adjustment type
        $api->get('adjustment_type',         [ 'middleware' => ['permission:adjustment-type-view'],'uses'  => 'App\Http\Controllers\AdjustmentTypeController@index' ]); 
        $api->get('adjustment_type/{id}',    [ 'uses'  => 'App\Http\Controllers\AdjustmentTypeController@show' ]);
        $api->post('adjustment_type',        [ 'middleware' => ['permission:adjustment-type-create'],'uses' => 'App\Http\Controllers\AdjustmentTypeController@store', ]);
        $api->put('adjustment_type/{id}',    [ 'middleware' => ['permission:adjustment-type-edit'],'uses' => 'App\Http\Controllers\AdjustmentTypeController@update', ]);
        $api->delete('adjustment_type/{id}', [ 'middleware' => ['permission:adjustment-type-delete'],'uses' => 'App\Http\Controllers\AdjustmentTypeController@destroy', ]);

        // loan 
        $api->get('loan',        [ 'middleware' => ['permission:loan-view'],'uses'  => 'App\Http\Controllers\LoanController@index' ]); 
        $api->get('loan/{id}',   [ 'uses'  => 'App\Http\Controllers\LoanController@show' ]);
        $api->post('loan',       [ 'middleware' => ['permission:loan-create'],'uses' => 'App\Http\Controllers\LoanController@store', ]);
        $api->put('loan/{id}',   [ 'middleware' => ['permission:loan-edit'],'uses' => 'App\Http\Controllers\LoanController@update', ]);
        $api->delete('loan/{id}',[ 'middleware' => ['permission:loan-delete'],'uses' => 'App\Http\Controllers\LoanController@destroy', ]);

        // loan type
        $api->get('loan_type',            [ 'middleware' => ['permission:loan-type-view'],'uses'  => 'App\Http\Controllers\LoanTypeController@index' ]); 
        $api->get('loan_type/{id}',       [ 'uses'  => 'App\Http\Controllers\LoanTypeController@show' ]);
        $api->post('loan_type',           [ 'middleware' => ['permission:loan-type-create'],'uses' => 'App\Http\Controllers\LoanTypeController@store', ]);
        $api->put('loan_type/{id}',       [ 'middleware' => ['permission:loan-type-edit'],'uses' => 'App\Http\Controllers\LoanTypeController@update', ]);
        $api->delete('loan_type/{id}',    [ 'middleware' => ['permission:loan-type-delete'],'uses' => 'App\Http\Controllers\LoanTypeController@destroy', ]);

        // Dynasty Loan Application
        $api->get('admin_dynasty_loan',            [ 'middleware' => ['permission:dynasty-loan-list'],'uses'  => 'App\Http\Controllers\DynastyLoanController@index' ]);
        $api->get('admin_dynasty_loan/{id}',            [ 'middleware' => ['permission:dynasty-loan-show'],'uses'  => 'App\Http\Controllers\DynastyLoanController@show' ]);
        $api->put('admin_dynasty_loan/{id}',       [ 'middleware' => ['permission:dynasty-loan-edit'],'uses' => 'App\Http\Controllers\DynastyLoanController@update', ]);
        // $api->get('loan_type/{id}',       [ 'middleware' => ['permission:dynasty-loan-show'], 'uses'  => 'App\Http\Controllers\LoanTypeController@show' ]);
        // $api->post('loan_type',           [ 'middleware' => ['permission:dynasty-loan-create'],'uses' => 'App\Http\Controllers\LoanTypeController@store', ]);
        // $api->put('loan_type/{id}',       [ 'middleware' => ['permission:dynasty-loan-edit'],'uses' => 'App\Http\Controllers\LoanTypeController@update', ]);
        // $api->delete('loan_type/{id}',    [ 'middleware' => ['permission:dynasty-loan-delete'],'uses' => 'App\Http\Controllers\LoanTypeController@destroy', ]);

        // employee
        $api->get('employee/',          [ 'middleware' => ['permission:employee-view'],'uses' => 'App\Http\Controllers\EmployeeController@index']);
        $api->get('employee/{id}',      [ 'uses' => 'App\Http\Controllers\EmployeeController@show']);
        $api->post('employee/',         [ 'middleware' => ['permission:employee-create'],'uses' => 'App\Http\Controllers\EmployeeController@store']);
        $api->put('employee/{id}',      [ 'middleware' => ['permission:employee-edit'],'uses' => 'App\Http\Controllers\EmployeeController@update']);
        $api->delete('employee/{id}',   [ 'middleware' => ['permission:employee-delete'],'uses' => 'App\Http\Controllers\EmployeeController@destroy']);
        $api->post('employee/update/employee/status',         [ 'uses' => 'App\Http\Controllers\EmployeeController@updateEmployeeStatus']);


        // Employee Attachment
        $api->get('employee_attachment/',       [ 'middleware' => ['permission:employee-attachment-view'],'uses' => 'App\Http\Controllers\EmployeeAttachmentController@index']);
        $api->get('employee_attachment/{id}',   [ 'uses' => 'App\Http\Controllers\EmployeeAttachmentController@show']);
        $api->post('employee_attachment/',      [ 'middleware' => ['permission:employee-attachment-create'],'uses' => 'App\Http\Controllers\EmployeeAttachmentController@store']);
        $api->put('employee_attachment/{id}',   [ 'middleware' => ['permission:employee-attachment-edit'],'uses' => 'App\Http\Controllers\EmployeeAttachmentController@update']);
        $api->delete('employee_attachment/{id}',[ 'middleware' => ['permission:employee-attachment-delete'],'uses' => 'App\Http\Controllers\EmployeeAttachmentController@destroy']);

        // Employee Remarks
        $api->get('employee_remark/',       [ 'middleware' => ['permission:employee-view'],'uses' => 'App\Http\Controllers\EmployeeRemarkController@index']);
        $api->get('employee_remark/{id}',   [ 'uses' => 'App\Http\Controllers\EmployeeRemarkController@show']);
        $api->post('employee_remark/',      [ 'middleware' => ['permission:employee-create'],'uses' => 'App\Http\Controllers\EmployeeRemarkController@store']);
        $api->put('employee_remark/{id}',   [ 'middleware' => ['permission:employee-edit'],'uses' => 'App\Http\Controllers\EmployeeRemarkController@update']);
        $api->delete('employee_remark/{id}',[ 'middleware' => ['permission:employee-delete'],'uses' => 'App\Http\Controllers\EmployeeRemarkController@destroy']);
        $api->post('employee_upload/',      [ 'middleware' => ['permission:employee-create'], 'uses' => 'App\Http\Controllers\EmployeeController@postUpload' ]);

        // Schedule MAster
        $api->get('employee_schedule_master/', [ 'middleware' => ['permission:schedule-adjustments-view'],'uses' => 'App\Http\Controllers\EmployeeScheduleMasterController@index']);
        $api->get('employee_schedule_master/{id}', [ 'uses' => 'App\Http\Controllers\EmployeeScheduleMasterController@show']);
        $api->post('employee_schedule_master/', [ 'middleware' => ['permission:schedule-adjustments-create'],'uses' => 'App\Http\Controllers\EmployeeScheduleMasterController@store']);
        $api->put('employee_schedule_master/{id}', [ 'middleware' => ['permission:schedule-adjustments-edit'],'uses' => 'App\Http\Controllers\EmployeeScheduleMasterController@update']);
        $api->delete('employee_schedule_master/{id}', [ 'middleware' => ['permission:schedule-adjustments-delete'],'uses' => 'App\Http\Controllers\EmployeeScheduleMasterController@destroy']);

        // attendance
        $api->get('attendance',                     [ 'middleware' => ['permission:attendance-view'],'uses'  => 'App\Http\Controllers\AttendanceController@index']);
        $api->post('attendance/search_employee',    [ 'uses'  =>'App\Http\Controllers\AttendanceController@searchEmployee']);
        $api->get('attendance/{id}',                [ 'middleware' => ['permission:attendance-edit'], 'uses'  => 'App\Http\Controllers\AttendanceController@show']);

        $api->post('attendance',                    [ 'middleware' => ['permission:attendance-create'], 'uses' => 'App\Http\Controllers\AttendanceController@store']);
        $api->put('attendance/{id}',                [ 'middleware' => ['permission:attendance-edit'],'uses' => 'App\Http\Controllers\AttendanceController@update']);
        $api->delete('attendance/{id}',             [ 'middleware' => ['permission:attendance-delete'],'uses' => 'App\Http\Controllers\AttendanceController@destroy']);
        $api->post('attendance/create_time',        [ 'middleware' => ['permission:attendance-create'], 'uses'  => 'App\Http\Controllers\AttendanceController@createTimeAttendance']);
        $api->post('attendance/create_day',         [ 'middleware' => ['permission:attendance-create'], 'uses'  => 'App\Http\Controllers\AttendanceController@createDayAttendance']);
        $api->post('attendance/create_day_default',         [ 'middleware' => ['permission:attendance-create'], 'uses'  => 'App\Http\Controllers\AttendanceController@createDayAttendanceDefault']);
        $api->get('attendance/filter/data',         [ 'middleware' => ['permission:attendance-view'], 'uses'  => 'App\Http\Controllers\AttendanceController@getAttendance']);
        $api->get('attendance/for/default/data',         [ 'middleware' => ['permission:attendance-view'], 'uses'  => 'App\Http\Controllers\AttendanceController@getAttendanceDefault']);
        $api->get('attendance/my_attendance/data',  [ 'uses'  => 'App\Http\Controllers\AttendanceController@getMyAttendance']);
        $api->get('attendance/working_schedule/company',  [ 'uses'  => 'App\Http\Controllers\AttendanceController@getWorkingScheduleListPerCompany']);
        $api->post('attendance/data/schedule',      [ 'uses'  => 'App\Http\Controllers\AttendanceController@getSchedules']);

        $api->get('attendance_upload_history',  ['middleware' => ['permission:attendance-view'], 'uses'  => 'App\Http\Controllers\AttendanceController@getUploadHistoryFromAttendance']);

        $api->delete('attendance_upload_history/{id}',  ['middleware' => ['permission:attendance-view'], 'uses'  => 'App\Http\Controllers\AttendanceController@deleteAttendanceByBatch']);

        $api->post('attendance/convert_file',  [ 'middleware' => ['permission:attendance-create'], 'uses'  => 'App\Http\Controllers\AttendanceController@convertFile']);
        $api->post('attendance/import_file',  [ 'middleware' => ['permission:attendance-create'], 'uses'  => 'App\Http\Controllers\AttendanceController@importFile']);
        $api->post('attendance/import_excel_1',  [ 'middleware' => ['permission:attendance-create'], 'uses'  => 'App\Http\Controllers\AttendanceController@importFromBioFile']);
        $api->post('attendance/import_excel_2',  [ 'middleware' => ['permission:attendance-create'], 'uses'  => 'App\Http\Controllers\AttendanceController@importFromBioFile2']);
        $api->post('attendance/import_excel_3',  [ 'middleware' => ['permission:attendance-create'], 'uses'  => 'App\Http\Controllers\AttendanceController@importFromBioFile3']);
        $api->get('attendance/import/bio',            ['uses'  => 'App\Http\Controllers\AttendanceController@fetch']);

        $api->get('attendance/logs/validation',      [ 'uses'  => 'App\Http\Controllers\AttendanceController@logsValidation']);
        $api->post('attendance/employee_rate',                 ['uses'  => 'App\Http\Controllers\AttendanceController@getEmployeeRate']);
        $api->post('attendance/ignore_emp/day_attendance',                 [ 'middleware' => ['permission:attendance-create'], 'uses'  => 'App\Http\Controllers\AttendanceController@ignoreEmpInDayAttendance']);
        $api->get('attendance/not/default/{id}',                [ 'uses'  => 'App\Http\Controllers\AttendanceController@getAttendanceNotDefaultbyId']);
        $api->put('attendance/not/default/update/{id}',                [ 'middleware' => ['permission:attendance-edit'],'uses' => 'App\Http\Controllers\AttendanceController@updateAttendanceNotDefault']);
        $api->post('attendance/archive',                 ['uses'  => 'App\Http\Controllers\AttendanceController@archiveAttendanceNotDefault']);

        // leave
        $api->get('leave',                 [ 'middleware' => ['permission:leave-view'],'uses'  => 'App\Http\Controllers\LeaveController@index']);
        $api->get('leave/{id}',            [ 'uses'  => 'App\Http\Controllers\LeaveController@show']);
        $api->post('leave',                [ 'middleware' => ['permission:leave-create'],'uses' => 'App\Http\Controllers\LeaveController@store']);
        $api->put('leave/{id}',            [ 'middleware' => ['permission:leave-edit'],'uses' => 'App\Http\Controllers\LeaveController@update']);
        $api->delete('leave/{id}',         [ 'middleware' => ['permission:leave-delete'],'uses' => 'App\Http\Controllers\LeaveController@destroy']);
        $api->post('leave/create',         [ 'middleware' => ['permission:leave-create'],'uses' => 'App\Http\Controllers\LeaveController@createLeave']);  
        $api->get('leave/my_leave/data',   [ 'middleware' => ['permission:my-leave-view'], 'uses' => 'App\Http\Controllers\LeaveController@getMyLeave']);
        $api->get('leave/filter/data',     [ 'middleware' => ['permission:my-leave-view'], 'uses'  => 'App\Http\Controllers\LeaveController@getLeave']);
        $api->post('leave/update/leave',   [ 'middleware' => ['permission:leave-edit'],'uses' => 'App\Http\Controllers\LeaveController@updateData']);
        $api->post('leave/duplicate_entry_validation',  [ 'uses' => 'App\Http\Controllers\LeaveController@duplicateEntryValidation']);
        $api->post('leave/leave_create',                [ 'middleware' => ['permission:leave-create'],'uses' => 'App\Http\Controllers\LeaveController@leaveCreate']);
        $api->post('leave/validate/leave',              [ 'uses' => 'App\Http\Controllers\LeaveController@updateLeave']);
        $api->get('leave/working/sched',                [ 'uses'  => 'App\Http\Controllers\LeaveController@getWorkingSchedule']);
        $api->post('leave/create/my_leave',             [ 'middleware' => ['permission:my-leave-create'], 'uses' => 'App\Http\Controllers\LeaveController@createLeave']);
        $api->post('leave/update/my_leave',             [ 'middleware' => ['permission:my-leave-edit'], 'uses' => 'App\Http\Controllers\LeaveController@updateData']);

        // leave type
        $api->get('leave_type',            [ 'middleware' => ['permission:leave-type-view'],'uses'  => 'App\Http\Controllers\LeaveTypeController@index']);
        $api->get('leave_type/{id}',       [ 'uses'  => 'App\Http\Controllers\LeaveTypeController@show']);
        $api->post('leave_type',           [ 'middleware' => ['permission:leave-type-create'],'uses' => 'App\Http\Controllers\LeaveTypeController@store']);
        $api->put('leave_type/{id}',       [ 'middleware' => ['permission:leave-type-edit'],'uses' => 'App\Http\Controllers\LeaveTypeController@update']);
        $api->delete('leave_type/{id}',    [ 'middleware' => ['permission:leave-type-delete'],'uses' => 'App\Http\Controllers\LeaveTypeController@destroy']);

        // official business
        $api->get('official_business',             [ 'middleware' => ['permission:official-business-view'],'uses'  => 'App\Http\Controllers\OfficialBusinessController@index']);
        $api->get('official_business/{id}',        [ 'uses'  => 'App\Http\Controllers\OfficialBusinessController@show']);
        $api->post('official_business',            [ 'middleware' => ['permission:official-business-create'],'uses' => 'App\Http\Controllers\OfficialBusinessController@store']);
        $api->put('official_business/{id}',        [ 'middleware' => ['permission:official-business-edit'],'uses' => 'App\Http\Controllers\OfficialBusinessController@update']);
        $api->delete('official_business/{id}',     [ 'middleware' => ['permission:official-business-delete'],'uses' => 'App\Http\Controllers\OfficialBusinessController@destroy']);
        $api->post('official_business/create',     [ 'middleware' => ['permission:official-business-create'],'uses' => 'App\Http\Controllers\OfficialBusinessController@createOB']);
        $api->get('official_business/my_ob/data',  [ 'middleware' => ['permission:my-ob-view'],'uses'  => 'App\Http\Controllers\OfficialBusinessController@getMyOB']);
        $api->get('official_business/filter/data', [ 'middleware' => ['permission:my-ob-view'],'uses'  => 'App\Http\Controllers\OfficialBusinessController@getOB']);
        $api->post('official_business/update/ob',                   [ 'middleware' => ['permission:official-business-edit'],'uses' => 'App\Http\Controllers\OfficialBusinessController@updateData']);
        $api->post('official_business/duplicate_entry_validation',  [ 'uses' => 'App\Http\Controllers\OfficialBusinessController@duplicateEntryValidation']);
        $api->post('official_business/create/my_ob',                [ 'middleware' => ['permission:my-ob-create'], 'uses' => 'App\Http\Controllers\OfficialBusinessController@createOB']);
        $api->post('official_business/update/my_ob',                [ 'middleware' => ['permission:my-ob-edit'], 'uses' => 'App\Http\Controllers\OfficialBusinessController@updateData']);

        // official business type
        $api->get('official_business_type',         [ 'middleware' => ['permission:official-business-type-view'],'uses'  => 'App\Http\Controllers\OfficialBusinessTypeController@index']);
        $api->get('official_business_type/{id}',    [ 'uses'  => 'App\Http\Controllers\OfficialBusinessTypeController@show']);
        $api->post('official_business_type',        [ 'middleware' => ['permission:official-business-type-create'],'uses' => 'App\Http\Controllers\OfficialBusinessTypeController@store']);
        $api->put('official_business_type/{id}',    [ 'middleware' => ['permission:official-business-type-edit'],'uses' => 'App\Http\Controllers\OfficialBusinessTypeController@update']);
        $api->delete('official_business_type/{id}', [ 'middleware' => ['permission:official-business-type-delete'],'uses' => 'App\Http\Controllers\OfficialBusinessTypeController@destroy']);  

        // status type
        $api->get('status_type',        [ 'uses'  => 'App\Http\Controllers\Status_TypeController@index']);
        $api->get('status_type/{id}',   [ 'uses'  => 'App\Http\Controllers\Status_TypeController@show']);
        $api->post('status_type',       [ 'uses' => 'App\Http\Controllers\Status_TypeController@store']);
        $api->put('status_type/{id}',   [ 'uses' => 'App\Http\Controllers\Status_TypeController@update']);
        $api->delete('status_type/{id}',[ 'uses' => 'App\Http\Controllers\Status_TypeController@destroy']);

        // event type
        $api->get('event_type',        [ 'middleware' => ['permission:event-type-view'],'uses'  => 'App\Http\Controllers\EventTypeController@index']);
        $api->get('event_type/{id}',   [  'uses'  => 'App\Http\Controllers\EventTypeController@show']);
        $api->post('event_type',       [ 'middleware' => ['permission:event-type-create'],    'uses' => 'App\Http\Controllers\EventTypeController@store']);
        $api->put('event_type/{id}',   [ 'middleware' => ['permission:event-type-edit'],  'uses' => 'App\Http\Controllers\EventTypeController@update']);
        $api->delete('event_type/{id}',[ 'middleware' => ['permission:event-type-delete'],    'uses' => 'App\Http\Controllers\EventTypeController@destroy']);

        // company
        $api->get('company',            [ 'middleware' => ['permission:company-view'],'uses'  => 'App\Http\Controllers\CompanyController@index']);
        $api->get('company/basic/info', [ 'uses'  => 'App\Http\Controllers\CompanyController@showComInfo']);
        $api->get('company/{id}',       [ 'uses'  => 'App\Http\Controllers\CompanyController@show']);
        $api->post('company',           [ 'middleware' => ['permission:company-create'],   'uses' => 'App\Http\Controllers\CompanyController@store']);
        $api->put('company/{id}',       [ 'middleware' => ['permission:company-edit'], 'uses' => 'App\Http\Controllers\CompanyController@update']);
        $api->delete('company/{id}',    [ 'middleware' => ['permission:company-delete'],   'uses' => 'App\Http\Controllers\CompanyController@destroy']);

        // company policy and policy equivalents
        $api->get('company_policy',                     [ 'middleware' => [ 'permission:company-view' ], 'uses' => 'App\Http\Controllers\CompanyPolicyController@index' ]);
        $api->get('company_policy_equivalent',          [ 'middleware' => [ 'permission:company-policy-policy-view' ], 'uses' => 'App\Http\Controllers\CompanyPolicyEquivalentController@index' ]);
        $api->get('company_policy_equivalent/{id}',     [ 'middleware' => [ 'permission:company-policy-policy-show' ], 'uses' => 'App\Http\Controllers\CompanyPolicyEquivalentController@show' ]);
        $api->post('company_policy_equivalent',         [ 'middleware' => [ 'permission:company-policy-policy-create' ], 'uses' => 'App\Http\Controllers\CompanyPolicyEquivalentController@store' ]);
        $api->put('company_policy_equivalent/{id}',     [ 'middleware' => [ 'permission:company-policy-policy-update' ], 'uses' => 'App\Http\Controllers\CompanyPolicyEquivalentController@index' ]);
        $api->delete('company_policy_equivalent/{id}',  [ 'middleware' => [ 'permission:company-policy-policy-delete' ], 'uses' => 'App\Http\Controllers\CompanyPolicyEquivalentController@destroy' ]);

        // company branches
        $api->get('company_branch',                     [ 'middleware' => ['permission:company-branch-view'],'uses' => 'App\Http\Controllers\CompanyBranchController@index' ]);
        $api->get('company_branch/{id}',                [ 'uses'  => 'App\Http\Controllers\CompanyBranchController@show']);
        $api->post('company_branch',                    [ 'middleware' => ['permission:company-branch-create'],   'uses' => 'App\Http\Controllers\CompanyBranchController@store']);
        $api->put('company_branch/{id}',                [ 'middleware' => ['permission:company-branch-edit'],   'uses' => 'App\Http\Controllers\CompanyBranchController@update']);
        $api->delete('company_branch/{id}',             [ 'middleware' => ['permission:company-branch-delete'],   'uses' => 'App\Http\Controllers\CompanyBranchController@destroy']);
        $api->put('company_branch/multiple_update/{id}',[ 'middleware' => ['permission:company-branch-edit'],   'uses' => 'App\Http\Controllers\CompanyBranchController@updateMultiple']);
        $api->get('company_branches',                   [ 'middleware' => ['permission:company-branch-view'],'uses' => 'App\Http\Controllers\CompanyBranchController@getCompanyBranches' ]);

        // company working schedule
        $api->get('company_working_schedule',            [ 'middleware' => ['permission:company-working-schedule-view'],'uses' => 'App\Http\Controllers\CompanyWorkingScheduleController@index' ]);
        $api->get('company_working_schedule/{id}',       [ 'uses' => 'App\Http\Controllers\CompanyWorkingScheduleController@show' ]);
        $api->post('company_working_schedule',           [ 'middleware' => ['permission:company-working-schedule-create'], 'uses' => 'App\Http\Controllers\CompanyWorkingScheduleController@store' ]);
        $api->put('company_working_schedule/{id}',       [ 'middleware' => ['permission:company-working-schedule-edit'], 'uses' => 'App\Http\Controllers\CompanyWorkingScheduleController@update' ]);
        $api->delete('company_working_schedule/{id}',    [ 'middleware' => ['permission:company-working-schedule-delete'], 'uses' => 'App\Http\Controllers\CompanyWorkingScheduleController@destroy' ]);
        $api->put('company_update_schedule/{company_id}',[ 'middleware' => ['permission:company-update-schedule-edit'], 'uses' => 'App\Http\Controllers\CompanyController@updateWorkingSchedules' ]); 
        $api->put('update_company_policy/{id}',          [ 'middleware' => ['permission:update-company-policy-edit'], 'uses' => 'App\Http\Controllers\CompanyController@updatePolicy' ]);
        $api->delete('company_policy/{id}',              [ 'middleware' => ['permission:company-policy-delete'], 'uses' => 'App\Http\Controllers\CompanyPolicyController@destroy' ]);

        // working schedule
        $api->get('working_schedule',     [  'middleware' => ['permission:working-schedule-view'],'uses' => 'App\Http\Controllers\WorkingScheduleController@index' ]);
        $api->get('working_schedule/{id}',[  'uses' => 'App\Http\Controllers\WorkingScheduleController@show' ]);
        $api->post('working_schedule',    [ 'middleware' => ['permission:working-schedule-create'], 'uses' => 'App\Http\Controllers\WorkingScheduleController@store' ]);
        $api->put('working_schedule',     [ 'middleware' => ['permission:working-schedule-edit'], 'uses' => 'App\Http\Controllers\WorkingScheduleController@update' ]);
        $api->delete('working_schedule',  [ 'middleware' => ['permission:working-schedule-delete'], 'uses' => 'App\Http\Controllers\WorkingScheduleController@destroy' ]);

        // department
        $api->get('department',        [ 'middleware' => ['permission:department-view'],'uses'  => 'App\Http\Controllers\DepartmentController@index']);
        $api->get('department/{id}',   [ 'uses'  => 'App\Http\Controllers\DepartmentController@show']);
        $api->post('department',       [ 'middleware' => ['permission:department-create'],'uses' => 'App\Http\Controllers\DepartmentController@store']);
        $api->put('department/{id}',   [ 'middleware' => ['permission:department-edit'],'uses' => 'App\Http\Controllers\DepartmentController@update']);
        $api->delete('department/{id}',[ 'middleware' => ['permission:department-delete'],'uses' => 'App\Http\Controllers\DepartmentController@destroy']);    

        // philhealth
        $api->get('philhealth',        [ 'uses' => 'App\Http\Controllers\PhilhealthController@index']);
        $api->get('philhealth/{id}',   [ 'uses' => 'App\Http\Controllers\PhilhealthController@show']);
        $api->post('philhealth',       [ 'uses' => 'App\Http\Controllers\PhilhealthController@store']);
        $api->put('philhealth/{id}',   [ 'uses' => 'App\Http\Controllers\PhilhealthController@update']);
        $api->delete('philhealth/{id}',[ 'uses' => 'App\Http\Controllers\PhilhealthController@destroy']);

        // sss
        $api->get('sss',        [ 'middleware' => ['permission:sss-view'],'uses'  => 'App\Http\Controllers\SSSController@index']);
        $api->get('sss/{id}',   [ 'uses'       => 'App\Http\Controllers\SSSController@show']);
        $api->post('sss',       [ 'middleware' => ['permission:sss-create'],'uses' => 'App\Http\Controllers\SSSController@store']);
        $api->put('sss/{id}',   [ 'middleware' => ['permission:sss-edit'],'uses' => 'App\Http\Controllers\SSSController@update']);
        $api->delete('sss/{id}',[ 'middleware' => ['permission:sss-delete'],'uses' => 'App\Http\Controllers\SSSController@destroy']);

        // government deduction
        $api->get('government_deduction/{data}',   [ 'uses'  => 'App\Http\Controllers\GovernmentDeductionController@computeDeduction']);

        // holiday
        $api->get('holiday',        [ 'middleware' => ['permission:holiday-view'],'uses'  => 'App\Http\Controllers\HolidayController@index']);
        $api->get('holiday/{id}',   [ 'uses'  => 'App\Http\Controllers\HolidayController@show']);
        $api->post('holiday',       [ 'middleware' => ['permission:holiday-create'],'uses' => 'App\Http\Controllers\HolidayController@store']);
        $api->put('holiday/{id}',   [ 'middleware' => ['permission:holiday-edit'],'uses' => 'App\Http\Controllers\HolidayController@update']);
        $api->delete('holiday/{id}',[ 'middleware' => ['permission:holiday-delete'],'uses' => 'App\Http\Controllers\HolidayController@destroy']); 

        // attendance
        $api->get('attendance_type',        [ 'middleware' => ['permission:attendance-type-view'],'uses'  => 'App\Http\Controllers\AttendanceTypeController@index']);
        $api->get('attendance_type/{id}',   [ 'uses'  => 'App\Http\Controllers\AttendanceTypeController@show']);
        $api->post('attendance_type',       [ 'middleware' => ['permission:attendance-type-create'], 'uses' => 'App\Http\Controllers\AttendanceTypeController@store']);
        $api->put('attendance_type/{id}',   [ 'middleware' => ['permission:attendance-type-edit'], 'uses' => 'App\Http\Controllers\AttendanceTypeController@update']);
        $api->delete('attendance_type/{id}',[ 'middleware' => ['permission:attendance-type-delete'], 'uses' => 'App\Http\Controllers\AttendanceTypeController@destroy']);

        //
        $api->get('show/rank_type',   [ 'uses'  => 'App\Http\Controllers\RankTypeController@getRankType' ]); 
        $api->get('rank_type',        [ 'uses'  => 'App\Http\Controllers\RankTypeController@index' ]); 
        $api->get('rank_type/{id}',   [ 'uses'  => 'App\Http\Controllers\RankTypeController@show' ]);
        $api->post('rank_type',       [ 'middleware' => ['permission:rank-type-create'],'uses' => 'App\Http\Controllers\RankTypeController@store', ]);
        $api->put('rank_type/{id}',   [ 'middleware' => ['permission:rank-type-edit'],'uses' => 'App\Http\Controllers\RankTypeController@update', ]);
        $api->delete('rank_type/{id}',[ 'middleware' => ['permission:rank-type-delete'],'uses' => 'App\Http\Controllers\RankTypeController@destroy', ]);

        // deduction
        $api->get('deduction',        [ 'middleware' => ['permission:deduction-view'],'uses'  => 'App\Http\Controllers\DeductionController@index' ]); 
        $api->get('deduction/{id}',   [ 'uses'  => 'App\Http\Controllers\DeductionController@show' ]);
        $api->post('deduction',       [ 'middleware' => ['permission:deduction-create'],'uses' => 'App\Http\Controllers\DeductionController@store', ]);
        $api->put('deduction/{id}',   [ 'middleware' => ['permission:deduction-edit'],'uses' => 'App\Http\Controllers\DeductionController@update', ]);
        $api->delete('deduction/{id}',[ 'middleware' => ['permission:deduction-delete'],'uses' => 'App\Http\Controllers\DeductionController@destroy', ]);

        // position
        $api->get('show/position',   [ 'uses'  => 'App\Http\Controllers\PositionController@position']);
        $api->get('position',        [ 'middleware' => ['permission:position-view'],'uses'  => 'App\Http\Controllers\PositionController@index']);
        $api->get('position/{id}',   [ 'uses'  => 'App\Http\Controllers\PositionController@show']);
        $api->post('position',       [ 'middleware' => ['permission:position-create'],'uses' => 'App\Http\Controllers\PositionController@store']);
        $api->put('position/{id}',   [ 'middleware' => ['permission:position-edit'],'uses' => 'App\Http\Controllers\PositionController@update']);
        $api->delete('position/{id}',[ 'middleware' => ['permission:position-delete'],'uses' => 'App\Http\Controllers\PositionController@destroy']);

        // certicate of attendance
        $api->get('certificate_of_attendance',             [ 'middleware' => ['permission:certificate-of-attendance-view'],'uses'  => 'App\Http\Controllers\CertificateOfAttendanceController@index']);
        $api->get('certificate_of_attendance/{id}',        [ 'uses'  => 'App\Http\Controllers\CertificateOfAttendanceController@show']);
        $api->post('certificate_of_attendance',            [ 'middleware' => ['permission:certificate-of-attendance-create'],'uses' => 'App\Http\Controllers\CertificateOfAttendanceController@store']);
        $api->put('certificate_of_attendance/{id}',        [ 'middleware' => ['permission:certificate-of-attendance-edit'],'uses' => 'App\Http\Controllers\CertificateOfAttendanceController@update']);
        $api->delete('certificate_of_attendance/{id}',     [ 'middleware' => ['permission:certificate-of-attendance-delete'],'uses' => 'App\Http\Controllers\CertificateOfAttendanceController@destroy']);
        $api->post('certificate_of_attendance/create',     [ 'middleware' => ['permission:certificate-of-attendance-create'], 'uses' => 'App\Http\Controllers\CertificateOfAttendanceController@createCOA']);
        $api->get('certificate_of_attendance/my_coa/data', [ 'middleware' => ['permission:my-coa-view'], 'uses' => 'App\Http\Controllers\CertificateOfAttendanceController@getMyCOA']);
        $api->get('certificate_of_attendance/filter/data', [ 'middleware' => ['permission:my-coa-view'], 'uses' => 'App\Http\Controllers\CertificateOfAttendanceController@getCOAFilter']);
        $api->post('certificate_of_attendance/update/coa', [ 'middleware' => ['permission:certificate-of-attendance-edit'],'uses' => 'App\Http\Controllers\CertificateOfAttendanceController@updateData']);
        $api->post('certificate_of_attendance/duplicate_entry_validation',  ['uses' => 'App\Http\Controllers\CertificateOfAttendanceController@duplicateEntryValidation']);
        $api->post('certificate_of_attendance/create/my_coa',               [ 'middleware' => ['permission:my-coa-create'], 'uses' => 'App\Http\Controllers\CertificateOfAttendanceController@createCOA']);
        $api->post('certificate_of_attendance/update/my_coa',               [ 'middleware' => ['permission:my-coa-edit'], 'uses' => 'App\Http\Controllers\CertificateOfAttendanceController@updateData']);

        // certificate of attendance type
        $api->get('certificate_of_attendance_type',        [ 'middleware' => ['permission:certificate-of-attendance-type-view'],'uses'  => 'App\Http\Controllers\CertificateOfAttendanceTypeController@index']);
        $api->get('certificate_of_attendance_type/{id}',   [ 'uses'  => 'App\Http\Controllers\CertificateOfAttendanceTypeController@show']);
        $api->post('certificate_of_attendance_type',       [ 'middleware' => ['permission:certificate-of-attendance-type-create'],'uses' => 'App\Http\Controllers\CertificateOfAttendanceTypeController@store']);
        $api->put('certificate_of_attendance_type/{id}',   [ 'middleware' => ['permission:certificate-of-attendance-type-edit'],'uses' => 'App\Http\Controllers\CertificateOfAttendanceTypeController@update']);
        $api->delete('certificate_of_attendance_type/{id}',[ 'middleware' => ['permission:certificate-of-attendance-type-delete'],'uses' => 'App\Http\Controllers\CertificateOfAttendanceTypeController@destroy']);

        // official undertime 
        $api->get('official_undertime',             [ 'middleware' => ['permission:official-undertime-view'],'uses'  => 'App\Http\Controllers\OfficialUndertimeController@index']);
        $api->get('official_undertime/{id}',        [ 'uses'  => 'App\Http\Controllers\OfficialUndertimeController@show']);
        $api->post('official_undertime',            [ 'middleware' => ['permission:official-undertime-create'],'uses' => 'App\Http\Controllers\OfficialUndertimeController@store']);
        $api->put('official_undertime/{id}',        [ 'middleware' => ['permission:official-undertime-edit'],'uses' => 'App\Http\Controllers\OfficialUndertimeController@update']);
        $api->delete('official_undertime/{id}',     [ 'middleware' => ['permission:official-undertime-delete'],'uses' => 'App\Http\Controllers\OfficialUndertimeController@destroy']);
        $api->post('official_undertime/create',     [ 'middleware' => ['permission:official-undertime-create'],'uses' => 'App\Http\Controllers\OfficialUndertimeController@createOU']);
        $api->get('official_undertime/my_ou/data',  [ 'middleware' => ['permission:my-ou-view'],'uses'  => 'App\Http\Controllers\OfficialUndertimeController@getMyOU']);
        $api->get('official_undertime/filter/data', [ 'middleware' => ['permission:my-ou-view'],'uses'  =>'App\Http\Controllers\OfficialUndertimeController@getOU']);
        $api->post('official_undertime/update/ou',  [ 'middleware' => ['permission:official-undertime-edit'],'uses' => 'App\Http\Controllers\OfficialUndertimeController@updateData']);
        $api->post('official_undertime/duplicate_entry_validation', [ 'uses' => 'App\Http\Controllers\OfficialUndertimeController@duplicateEntryValidation']);
        $api->post('official_undertime/create/my_ou',               [ 'middleware' => ['permission:my-ou-create'], 'uses' => 'App\Http\Controllers\OfficialUndertimeController@createOU']);
        $api->post('official_undertime/update/my_ou',               [ 'middleware' => ['permission:my-ou-edit'], 'uses' => 'App\Http\Controllers\OfficialUndertimeController@updateData']);
        
        $api->get('schedule_adjustments',             [ 'middleware' => ['permission:schedule-adjustments-view'],'uses'  => 'App\Http\Controllers\ScheduleAdjustmentsController@index']);
        $api->get('schedule_adjustments/{id}',        [ 'uses'  => 'App\Http\Controllers\ScheduleAdjustmentsController@show']);
        $api->get('schedule_adjustments_request/{id}', [ 'uses' => 'App\Http\Controllers\ScheduleAdjustmentsController@getScheduleAdjustmentRequest' ]);
        $api->put('schedule_adjustments_request', [ 'uses' => 'App\Http\Controllers\ScheduleAdjustmentsController@updateScheduleAdjustmentRequest' ]);
        $api->post('schedule_adjustments',            [ 'middleware' => ['permission:schedule-adjustments-create'],'uses' => 'App\Http\Controllers\ScheduleAdjustmentsController@store']);
        $api->put('schedule_adjustments/{id}',        [ 'middleware' => ['permission:schedule-adjustments-edit'],'uses' => 'App\Http\Controllers\ScheduleAdjustmentsController@update']);
        $api->delete('schedule_adjustments/{id}',     [ 'middleware' => ['permission:schedule-adjustments-delete'],'uses' => 'App\Http\Controllers\ScheduleAdjustmentsController@destroy']);
        $api->post('schedule_adjustments/create',     [ 'middleware' => ['permission:schedule-adjustments-create'],'uses' => 'App\Http\Controllers\ScheduleAdjustmentsController@createSA']);
        $api->get('schedule_adjustments/my_sa/data',  [ 'middleware' => ['permission:my-sa-view'],'uses'  => 'App\Http\Controllers\ScheduleAdjustmentsController@getMySA']);
        $api->get('schedule_adjustments/filter/data', [ 'middleware' => ['permission:my-sa-view'],'uses'  =>'App\Http\Controllers\ScheduleAdjustmentsController@getSA']);
        $api->post('schedule_adjustments/update/sa',  [ 'middleware' => ['permission:schedule-adjustments-edit'],'uses' => 'App\Http\Controllers\ScheduleAdjustmentsController@updateData']);
        $api->post('schedule_adjustments/duplicate_entry_validation',   ['uses' => 'App\Http\Controllers\ScheduleAdjustmentsController@duplicateEntryValidation']);
        $api->post('schedule_adjustments/create/my_sa',                 [ 'middleware' => ['permission:my-sa-create'], 'uses' => 'App\Http\Controllers\ScheduleAdjustmentsController@createSA']);
        $api->post('schedule_adjustments/update/my_sa',                 [ 'middleware' => ['permission:my-sa-edit'], 'uses' => 'App\Http\Controllers\ScheduleAdjustmentsController@updateData']);

        // overtime
        $api->get('overtime',             [ 'middleware' => ['permission:overtime-view'],'uses'  => 'App\Http\Controllers\OvertimeController@index' ]); 
        $api->get('overtime/{id}',        [ 'uses'  => 'App\Http\Controllers\OvertimeController@show' ]);
        $api->post('overtime',            [ 'middleware' => ['permission:overtime-create'],'uses' => 'App\Http\Controllers\OvertimeController@store', ]);
        $api->put('overtime/{id}',        [ 'middleware' => ['permission:overtime-edit'],'uses' => 'App\Http\Controllers\OvertimeController@update', ]);
        $api->delete('overtime/{id}',     [ 'middleware' => ['permission:overtime-delete'],'uses' => 'App\Http\Controllers\OvertimeController@destroy', ]);
        $api->post('overtime/create',     [ 'middleware' => ['permission:overtime-create'],'uses' => 'App\Http\Controllers\OvertimeController@createOvertime']);
        $api->get('overtime/my_ot/data',  [ 'middleware' => ['permission:my-ot-view'], 'uses'  => 'App\Http\Controllers\OvertimeController@getMyOT' ]);
        $api->get('overtime/filter/data', [ 'middleware' => ['permission:my-ot-view'], 'uses'  => 'App\Http\Controllers\OvertimeController@getOvertime' ]);
        $api->post('overtime/update/ot',  [ 'middleware' => ['permission:overtime-edit'],'uses' => 'App\Http\Controllers\OvertimeController@updateData']);
        $api->post('overtime/duplicate_entry_validation',  [ 'uses' => 'App\Http\Controllers\OvertimeController@duplicateEntryValidation']);
        $api->post('overtime/create/my_ot',                [ 'middleware' => ['permission:my-ot-create'], 'uses' => 'App\Http\Controllers\OvertimeController@createOvertime']);
        $api->post('overtime/update/my_ot',                [ 'middleware' => ['permission:my-ot-edit'], 'uses' => 'App\Http\Controllers\OvertimeController@updateData']);

        // COMMON

        $api->get('common/employee',            [ 'uses'  => 'App\Http\Controllers\CommonController@getEmployee']);
        $api->get('common/employees',           [ 'uses'  => 'App\Http\Controllers\CommonController@getEmployees']);
        $api->get('common/leave_type',          [ 'uses'  => 'App\Http\Controllers\CommonController@getLeaveType']);
        $api->get('common/ob_type',             [ 'uses'  => 'App\Http\Controllers\CommonController@getOBType']);
        $api->get('common/status_type',         [ 'uses'  => 'App\Http\Controllers\CommonController@getStatusType']);
        $api->get('common/coa_type',            [ 'uses'  => 'App\Http\Controllers\CommonController@getCOAType']);
        $api->get('common/holi_type',           [ 'uses'  => 'App\Http\Controllers\CommonController@getHoliType']);
        $api->get('common/department',          [ 'uses'  => 'App\Http\Controllers\CommonController@getDepartment']);
        $api->get('common/company',             [ 'uses'  => 'App\Http\Controllers\CommonController@getCompany']);
        $api->get('common/attendance_type',     [ 'uses'  => 'App\Http\Controllers\CommonController@getAttendanceType']);
        $api->get('common/event_type',          [ 'uses'  => 'App\Http\Controllers\CommonController@getEventType']);
        $api->get('common/all_request',         [ 'uses'  => 'App\Http\Controllers\CommonController@getAllRequest']);
        $api->get('common/my_schedule/{id}',    [ 'uses'  => 'App\Http\Controllers\CommonController@getMySchedule']);
        $api->get('common/filter_employee',     [ 'uses'  => 'App\Http\Controllers\CommonController@getFilterEmployee']);
        $api->get('common/branch',              [ 'uses'  => 'App\Http\Controllers\CommonController@getBranch']);
        $api->get('common/bio_device',          [ 'uses'  => 'App\Http\Controllers\CommonController@getBioDevice']);
        $api->post('common/update/request',     [ 'uses' => 'App\Http\Controllers\CommonController@updateRequest', ]);
        $api->get('common/position',            [ 'uses'  => 'App\Http\Controllers\CommonController@getPosition']);
        $api->get('common/working_sched/{id}',       [ 'uses'  => 'App\Http\Controllers\CommonController@getWorkingSchedule']);
        $api->get('common/working_sched_without_break',       [ 'uses'  => 'App\Http\Controllers\CommonController@getWorkingScheduleWithoutBreak']);
        $api->get('common/suggested_working_sched', [ 'uses' => 'App\Http\Controllers\CommonController@getSuggestedWorkingSchedule' ]);
        $api->get('common/onboarding',             [ 'uses'  => 'App\Http\Controllers\CommonController@getOnboarding']);
        $api->get('common/clearance',             [ 'uses'  => 'App\Http\Controllers\CommonController@getClearance']);
        $api->get('common/check_holiday',             [ 'uses'  => 'App\Http\Controllers\CommonController@checkHoliday']);
        $api->get('common/branch_by_company/{id}',              [ 'uses'  => 'App\Http\Controllers\CommonController@getBranchByCompany']);



        // payroll
        $api->get('payroll',        [ 'middleware' => ['permission:payroll-view'],'uses'  => 'App\Http\Controllers\PayrollController@index' ]); 
        $api->get('payroll/{id}',   [ 'uses'  => 'App\Http\Controllers\PayrollController@show' ]);
        $api->post('payroll',       [ 'middleware' => ['permission:payroll-create'],'uses' => 'App\Http\Controllers\PayrollController@store', ]);
        $api->put('payroll/{id}',   [ 'middleware' => ['permission:payroll-edit'],'uses' => 'App\Http\Controllers\PayrollController@update', ]);
        $api->delete('payroll/{id}',[ 'middleware' => ['permission:payroll-delete'],'uses' => 'App\Http\Controllers\PayrollController@destroy', ]);

        // payroll list
        $api->get('payroll_list',            [ 'middleware' => ['permission:payroll-view'],'uses'  => 'App\Http\Controllers\PayrollListController@index' ]); 
        $api->get('payroll_list/{id}',       [ 'uses'  => 'App\Http\Controllers\PayrollListController@show' ]);
        $api->post('payroll_list',           [ 'middleware' => ['permission:payroll-create'],'uses' => 'App\Http\Controllers\PayrollListController@store', ]);
        $api->put('payroll_list/{id}',       [ 'middleware' => ['permission:payroll-edit'],'uses' => 'App\Http\Controllers\PayrollListController@update', ]);
        $api->delete('payroll_list/{id}',    [ 'middleware' => ['permission:payroll-delete'],'uses' => 'App\Http\Controllers\PayrollListController@destroy', ]);
        $api->get('payroll_details',         [ 'uses'  => 'App\Http\Controllers\PayrollListController@payroll' ]); 
        $api->put('payroll_allowance/{id}',  [ 'uses'  => 'App\Http\Controllers\PayrollListController@updateAllowance' ]); 

        $api->get('payroll_get_employee',            [ 'middleware' => ['permission:payroll-view'],'uses'  => 'App\Http\Controllers\PayrollListController@getEmployeeForPayroll' ]);
        

        // payroll billing by location
        $api->get('payroll_billing',   [ 'middleware' => ['permission:payroll-view'],'uses'  => 'App\Http\Controllers\PayrollBillingByLocationController@index' ]);

        // payroll_billing
        $api->get('billing_rate',        [ 'uses'  => 'App\Http\Controllers\BillingRateController@index' ]); 
        $api->get('billing_rate/{id}',   [ 'uses'  => 'App\Http\Controllers\BillingRateController@show' ]);
        $api->post('billing_rate',       [ 'uses' => 'App\Http\Controllers\BillingRateController@store', ]);
        $api->put('billing_rate/{id}',   [ 'uses' => 'App\Http\Controllers\BillingRateController@update', ]);
        $api->delete('billing_rate/{id}',[ 'uses' => 'App\Http\Controllers\BillingRateController@destroy', ]); 

        // payroll reports
        $api->get('payroll_report/{id}',     [ 'uses'  => 'App\Http\Controllers\PayrollReportController@getPayrollReport' ]); 
        $api->get('payroll_adjustment/{id}', [ 'uses'  => 'App\Http\Controllers\PayrollReportController@getPayrollAdjustment' ]); 
        $api->get('payroll_deduction/{id}',  [ 'uses'  => 'App\Http\Controllers\PayrollReportController@getPayrollDeduction' ]); 
        $api->get('payroll_loan/{id}',       [ 'uses'  => 'App\Http\Controllers\PayrollReportController@getPayrollLoan' ]); 
        $api->get('payroll_employee/',       [ 'uses'  => 'App\Http\Controllers\PayrollReportController@getEmployee' ]); 

        // $api->get('report',                   ['uses' => 'App\Http\Controllers\AttendanceReportController@report']);
        $api->get('hours/report',                [ 'uses'  => 'App\Http\Controllers\AttendanceReportController@hoursReport']);
        // $api->get('days/summary/report',      [ 'uses'  => 'App\Http\Controllers\AttendanceReportController@daysSummaryReport']);
        $api->get('hours/summary/report',        [ 'uses'  => 'App\Http\Controllers\AttendanceReportController@hoursSummaryReport']);
        $api->get('absnt/get',                   [ 'uses'  => 'App\Http\Controllers\AttendanceReportController@getAbsnt']);
        $api->get('getcomp/byemp',               [ 'uses'  => 'App\Http\Controllers\AttendanceReportController@getEmpByComp']);
        $api->get('abs',                         [ 'uses'  => 'App\Http\Controllers\AttendanceReportController@abs']);
        $api->get('perEmp/attendance/report',    [ 'uses'  => 'App\Http\Controllers\AttendanceReportController@perEmpLateEarly']);
        $api->get('user/report',                 [ 'uses'  => 'App\Http\Controllers\AttendanceReportController@userReport']);
        $api->get('days/summary/report',         [ 'uses'  => 'App\Http\Controllers\AttendanceReportController@daysSummaryReport']);
        $api->get('per/emp/days/summary/report', [ 'uses'  => 'App\Http\Controllers\AttendanceReportController@perEmpDaysSummary']);
        $api->get('per/emp/hours/summary/report',[ 'uses'  => 'App\Http\Controllers\AttendanceReportController@perEmpHoursSummary']);
        $api->get('status/report',               [ 'uses'  => 'App\Http\Controllers\AttendanceReportController@statusReport']);
        $api->get('late/early/report',           [ 'uses'  => 'App\Http\Controllers\AttendanceReportController@lateErlyRprt']);
        $api->get('working/schedule',            [ 'uses'  => 'App\Http\Controllers\AttendanceReportController@working_schedule']);

        $api->get('category/name',   [ 'uses'  => 'App\Http\Controllers\CategoryController@categoryList']);
        $api->get('cat/name',        [ 'uses'  => 'App\Http\Controllers\CategoryController@catName']);
        $api->get('category',        [ 'uses'  => 'App\Http\Controllers\CategoryController@index' ]); 
        $api->get('category/{id}',   [ 'uses'  => 'App\Http\Controllers\CategoryController@show' ]);
        $api->post('category',       [ 'uses' => 'App\Http\Controllers\CategoryController@store', ]);
        $api->post('post/category',  [ 'uses'  => 'App\Http\Controllers\CategoryController@postCategoryDetails']);
        $api->put('category/{id}',   [ 'uses' => 'App\Http\Controllers\CategoryController@update', ]);
        $api->delete('category/{id}',[ 'uses' => 'App\Http\Controllers\CategoryController@destroy', ]);

        $api->get('question/list',   [ 'uses'  => 'App\Http\Controllers\QuestionController@question' ]); 
        $api->get('question',        [ 'uses'  => 'App\Http\Controllers\QuestionController@index' ]); 
        $api->get('question/{id}',   [ 'uses'  => 'App\Http\Controllers\QuestionController@show' ]);
        $api->post('question',       [ 'uses' => 'App\Http\Controllers\QuestionController@store', ]);
        $api->put('question/{id}',   [ 'uses' => 'App\Http\Controllers\QuestionController@update', ]);
        $api->delete('question/{id}',[ 'uses' => 'App\Http\Controllers\QuestionController@destroy', ]);

        $api->get('get/category/list',  [ 'uses'  => 'App\Http\Controllers\SubCategoryController@getSubCat']);
        $api->get('subcategory',        [ 'uses'  => 'App\Http\Controllers\SubcategoryController@index' ]); 
        $api->get('subcategory/{id}',   [ 'uses'  => 'App\Http\Controllers\SubcategoryController@show' ]);
        $api->post('subcategory',       [ 'uses' => 'App\Http\Controllers\SubcategoryController@store', ]);
        $api->post('sub/category',      [ 'uses'  => 'App\Http\Controllers\SubcategoryController@postSubCategory']);
        $api->put('subcategory/{id}',   [ 'uses' => 'App\Http\Controllers\SubcategoryController@update', ]);
        $api->delete('subcategory/{id}',[ 'uses' => 'App\Http\Controllers\SubcategoryController@destroy', ]);

        $api->get('view/employee/rate',     [ 'uses'  => 'App\Http\Controllers\EvaluationController@viewEmployeeEvaluation' ]); 
        $api->get('filter/employee',        [ 'uses'  => 'App\Http\Controllers\EvaluationController@filterEmployee' ]); 
        $api->get('show/emp/rate',          [ 'uses'  => 'App\Http\Controllers\EvaluationController@showEmployeeRate' ]); 
        $api->get('get/emp/position',       [ 'uses'  => 'App\Http\Controllers\EvaluationController@getEmployeePosition' ]); 
        $api->get('show/all/employee',      [ 'uses'  => 'App\Http\Controllers\EvaluationController@showAllEmployee' ]); 
        $api->get('save/evaluation/answer', [ 'uses'  => 'App\Http\Controllers\EvaluationController@postEvaluationAnswer' ]); 
        $api->get('get/emp/evaluation',     [ 'uses'  => 'App\Http\Controllers\EvaluationController@getEmployeeEvaluation' ]); 
        $api->post('create/evaluation/form',[ 'uses'  => 'App\Http\Controllers\EvaluationController@postEvaluationAnswer' ]); 
        $api->get('get/evaluation',         [ 'uses'  => 'App\Http\Controllers\EvaluationController@getEvaluation' ]); 
        $api->get('show/title',             [ 'uses'  => 'App\Http\Controllers\EvaluationController@getEvaluationTitle' ]); 
        $api->get('show/emp/by/pos',        [ 'uses'  => 'App\Http\Controllers\EvaluationController@getEmployeeByPosition' ]); 
        $api->get('employment/status',      [ 'uses'  => 'App\Http\Controllers\EvaluationController@getEmpByStatus' ]); 
        $api->get('employee/type/status',   [ 'uses'  => 'App\Http\Controllers\EvaluationController@employmentStatus' ]); 
        $api->get('add/segment',            [ 'uses'  => 'App\Http\Controllers\EvaluationController@addSegment']); 
        $api->get('add/category',           [ 'uses'  => 'App\Http\Controllers\EvaluationController@addCategory']); 
        $api->get('add/subcategory',        [ 'uses'  => 'App\Http\Controllers\EvaluationController@addSubCategory']); 
        $api->get('segment/list',           [ 'uses'  => 'App\Http\Controllers\EvaluationController@segment' ]); 
        $api->get('evaluation',             [ 'uses'  => 'App\Http\Controllers\EvaluationController@index' ]); 
        $api->get('evaluation/{id}',        [ 'uses'  => 'App\Http\Controllers\EvaluationController@show' ]);
        $api->post('evaluation',            [ 'uses' => 'App\Http\Controllers\EvaluationController@store', ]); 
        $api->post('evaluation/form',       [ 'uses' => 'App\Http\Controllers\EvaluationController@postEvaluationForm', ]); 
        $api->post('evaluation/template',   [ 'uses'  => 'App\Http\Controllers\EvaluationController@postEvaluationTemplate']);      
        $api->put('evaluation/{id}',        [ 'uses' => 'App\Http\Controllers\EvaluationController@update', ]);
        $api->delete('evaluation/{id}',     [ 'uses' => 'App\Http\Controllers\EvaluationController@destroy', ]);

         // segment
        $api->get('show/segment',   [ 'uses'  => 'App\Http\Controllers\SegmentController@showSegment']);
        $api->get('get/segments',   [ 'uses'  => 'App\Http\Controllers\SegmentController@getSegment']);
        $api->get('segment',        [ 'uses'  => 'App\Http\Controllers\SegmentController@index']);
        $api->get('segment/{id}',   [ 'uses'  => 'App\Http\Controllers\SegmentController@show']);
        $api->post('segment',       [ 'uses' => 'App\Http\Controllers\SegmentController@store']);
        $api->put('segment/{id}',   [ 'uses' => 'App\Http\Controllers\SegmentController@update']);
        $api->delete('segment/{id}',[ 'uses' => 'App\Http\Controllers\SegmentController@destroy']);

        //rating
        $api->get('show/rating',   [ 'uses'  => 'App\Http\Controllers\RatingController@showRating']);
        $api->get('rating',        [ 'uses'  => 'App\Http\Controllers\RatingController@index']);
        $api->get('rating/{id}',   [ 'uses'  => 'App\Http\Controllers\RatingController@show']);
        $api->post('rating',       [ 'uses' => 'App\Http\Controllers\RatingController@store']);
        $api->put('rating/{id}',   [ 'uses' => 'App\Http\Controllers\RatingController@update']);
        $api->delete('rating/{id}',[ 'uses' => 'App\Http\Controllers\RatingController@destroy']);

        //notification
        $api->get('notification',      [ 'uses'  => 'App\Http\Controllers\NotificationController@getNotification']);
        $api->post('notification/update',   [ 'uses'  => 'App\Http\Controllers\NotificationController@updateNotification']);
        $api->get('notification/notif/data',[ 'uses'  => 'App\Http\Controllers\NotificationController@getNotif']);
        $api->get('notification/all/{id}',  [ 'uses'  => 'App\Http\Controllers\NotificationController@getAllNotification']);
        $api->post('notification/markall',  [ 'uses'  => 'App\Http\Controllers\NotificationController@updateAllNotif']);

        // task
        $api->get('task',            [ 'uses'  => 'App\Http\Controllers\TaskController@index']);
        $api->get('task/{id}',       [ 'uses'  => 'App\Http\Controllers\TaskController@show']);
        $api->post('task',           [ 'uses' => 'App\Http\Controllers\TaskController@store']);
        $api->put('task/{id}',       [ 'uses' => 'App\Http\Controllers\TaskController@update']);
        $api->delete('task/{id}',    [ 'uses' => 'App\Http\Controllers\TaskController@destroy']);
        $api->get('task/filter/data',[ 'uses'  => 'App\Http\Controllers\TaskController@getTask']);
        $api->post('task/duplicate_entry_validation',[ 'uses' => 'App\Http\Controllers\TaskController@duplicateEntryValidation']);
        $api->post('task/create',            [ 'uses' => 'App\Http\Controllers\TaskController@createTask']);
        $api->post('task/update/data',       [ 'uses' => 'App\Http\Controllers\TaskController@updateTask']);
        $api->get('task/my_task/data',       [ 'uses'  => 'App\Http\Controllers\TaskController@getMyTask' ]);
        $api->post('task/update/task/time',  [ 'uses' => 'App\Http\Controllers\TaskController@updateTaskTime']);
        $api->post('task/update/task/status',[ 'uses' => 'App\Http\Controllers\TaskController@updateTaskStatus']);
        $api->post('task/sample',            [ 'uses' => 'App\Http\Controllers\TaskController@notifyTaskDeadline']);
        $api->post('task/approved/status',   [ 'uses' => 'App\Http\Controllers\TaskController@approvedTask']);


       
        // appraisal objective part

        $api->get('show/emp/under/sup',['uses'  => 'App\Http\Controllers\EvaluationController@getEmployee']);
        $api->get('show/appraisal',    ['uses'  => 'App\Http\Controllers\AppraisalController@appraisal']);
        $api->get('appraisal',         ['uses'  => 'App\Http\Controllers\AppraisalController@index']);
        $api->get('appraisal/{id}',    ['uses'  => 'App\Http\Controllers\AppraisalController@show']);
        $api->post('appraisal',        ['uses' => 'App\Http\Controllers\AppraisalController@store']);
        $api->post('appraisal/update', ['uses' => 'App\Http\Controllers\AppraisalController@updateObj']);
        $api->put('appraisal/{id}',    ['uses' => 'App\Http\Controllers\AppraisalController@update']);
        $api->delete('appraisal/{id}', ['uses' => 'App\Http\Controllers\AppraisalController@destroy']);

        // appraisal development part

        $api->get('show/development',   ['uses'  => 'App\Http\Controllers\DevelopmentController@showDevelopment']);
        $api->get('development',        ['uses'  => 'App\Http\Controllers\DevelopmentController@index']);
        $api->get('development/{id}',   ['uses'  => 'App\Http\Controllers\DevelopmentController@show']);
        $api->post('development',       ['uses' => 'App\Http\Controllers\DevelopmentController@store']);
        $api->put('development/{id}',   ['uses' => 'App\Http\Controllers\DevelopmentController@update']);
        $api->delete('development/{id}',['uses' => 'App\Http\Controllers\DevelopmentController@destroy']);

        // appraisal admin part

        $api->get('all/employee',         ['uses'  => 'App\Http\Controllers\AppraisalController@getEmployeeByAdmin']);
        $api->get('get/obj',              ['uses'  => 'App\Http\Controllers\AppraisalController@getObjNow']);
        $api->get('rate/obj',             ['uses'  => 'App\Http\Controllers\AppraisalController@rateObj']);
        $api->get('pending/appraisal',    ['uses'  => 'App\Http\Controllers\AppraisalController@pendingAppraisal']);
        $api->get('view/rate',            ['uses'  => 'App\Http\Controllers\AppraisalController@viewRate']);
        $api->post('save/obj/rate',       ['uses' => 'App\Http\Controllers\AppraisalController@saveRateObj']);
         // onboarding
        $api->get('onboarding',        [ 'uses'  => 'App\Http\Controllers\OnboardingController@index']);
        $api->get('onboarding/{id}',   [ 'uses'  => 'App\Http\Controllers\OnboardingController@show']);
        $api->post('onboarding',       [ 'uses' => 'App\Http\Controllers\OnboardingController@store']);
        $api->put('onboarding/{id}',   [ 'uses' => 'App\Http\Controllers\OnboardingController@update']);
        $api->delete('onboarding/{id}',[ 'uses' => 'App\Http\Controllers\OnboardingController@destroy']);

        $api->get('onboarding/list/data',   [ 'uses'  => 'App\Http\Controllers\OnboardingController@getOnboardingList']);
        $api->post('onboarding/update/status',   [ 'uses'  => 'App\Http\Controllers\OnboardingController@updateStatus']);
        

         // clearance
        $api->get('clearance',        [ 'uses'  => 'App\Http\Controllers\ClearanceController@index']);
        $api->get('clearance/{id}',   [ 'uses'  => 'App\Http\Controllers\ClearanceController@show']);
        $api->post('clearance',       [ 'uses' => 'App\Http\Controllers\ClearanceController@store']);
        $api->post('clearance/duplicate_entry_validation',  [ 'uses' => 'App\Http\Controllers\ClearanceController@duplicateEntryValidation']);
        $api->put('clearance/{id}',   [ 'uses' => 'App\Http\Controllers\ClearanceController@update']);
        $api->delete('clearance/{id}',[ 'uses' => 'App\Http\Controllers\ClearanceController@destroy']);
        $api->get('clearance/list/data',   [ 'uses'  => 'App\Http\Controllers\ClearanceController@getClearanceList']);
        $api->post('clearance/update/status',   [ 'uses'  => 'App\Http\Controllers\ClearanceController@updateStatus']);
        $api->post('clearance/create/data',   [ 'uses'  => 'App\Http\Controllers\ClearanceController@createClearance']);
        $api->post('clearance/update/clearance',  [ 'uses' => 'App\Http\Controllers\ClearanceController@updateClearance']);

        //goal_setting
        $api->get('show/goal',   [ 'uses'  => 'App\Http\Controllers\GoalController@showGoal']);
        $api->get('goal',        [ 'uses'  => 'App\Http\Controllers\GoalController@index']);
        $api->get('goal/{id}',   [ 'uses'  => 'App\Http\Controllers\GoalController@show']);
        $api->post('goal',       [ 'uses' => 'App\Http\Controllers\GoalController@store']);
        $api->put('goal/{id}',   [ 'uses' => 'App\Http\Controllers\GoalController@update']);
        $api->delete('goal/{id}',[ 'uses' => 'App\Http\Controllers\GoalController@destroy']);

        $api->get('biometrics',        [ 'uses'  => 'App\Http\Controllers\BiometricsController@index']);
        $api->get('biometrics/{id}',   [ 'uses'  => 'App\Http\Controllers\BiometricsController@show']);
        $api->post('biometrics',       [ 'uses' => 'App\Http\Controllers\BiometricsController@store']);
        $api->put('biometrics/{id}',   [ 'uses' => 'App\Http\Controllers\BiometricsController@update']);
        $api->delete('biometrics/{id}',[ 'uses' => 'App\Http\Controllers\BiometricsController@destroy']);

        $api->get('company_policy_type',        [ 'uses'  => 'App\Http\Controllers\CompanyPolicyTypeController@index']);
        $api->get('company_policy_type/{id}',   [ 'uses'  => 'App\Http\Controllers\CompanyPolicyTypeController@show']);
        $api->post('company_policy_type',       [ 'uses' => 'App\Http\Controllers\CompanyPolicyTypeController@store']);
        $api->put('company_policy_type/{id}',   [ 'uses' => 'App\Http\Controllers\CompanyPolicyTypeController@update']);
        $api->delete('company_policy_type/{id}',[ 'uses' => 'App\Http\Controllers\CompanyPolicyTypeController@destroy']);

        // $api->get('schedule_master',        [ 'uses'  => 'App\Http\Controllers\ScheduleMasterController@index']);
        // $api->get('schedule_master/{id}',   [ 'uses'  => 'App\Http\Controllers\ScheduleMasterController@show']);
        // $api->post('schedule_master',       [ 'uses' => 'App\Http\Controllers\ScheduleMasterController@store']);
        // $api->put('schedule_master/{id}',   [ 'uses' => 'App\Http\Controllers\ScheduleMasterController@update']);
        // $api->delete('schedule_master/{id}',[ 'uses' => 'App\Http\Controllers\ScheduleMasterController@destroy']);

    });
});

