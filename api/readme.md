# Lumen with JWT Authentication
Basically this is a starter kit for you to integrate Lumen with [JWT Authentication](https://jwt.io/).
If you want to Lumen + Dingo + JWT for your current application, please check [here](https://github.com/krisanalfa/lumen-dingo-adapter).

## What's Added

- [Lumen 5.3](https://github.com/laravel/lumen/tree/v5.3.0).
- [JWT Auth](https://github.com/tymondesigns/jwt-auth) for Lumen Application. <sup>[1]</sup>
- [Dingo](https://github.com/dingo/api) to easily and quickly build your own API. <sup>[1]</sup>
- [Lumen Generator](https://github.com/flipboxstudio/lumen-generator) to make development even easier and faster.
- [CORS and Preflight Request](https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS) support.

> [1] Added via [this package](https://packagist.org/packages/krisanalfa/lumen-dingo-adapter).

## Quick Start

- Clone this repo or download it's release archive and extract it somewhere
- You may delete `.git` folder if you get this code via `git clone`
- Run `composer install`
- Run `php artisan jwt:generate`
- Configure your `.env` file for authenticating via database
- Set the `API_PREFIX` parameter in your .env file (usually `api`).
- Run `php artisan migrate --seed`

## A Live PoC

- Run a PHP built in server from your root project:

```sh
php -S localhost:8000 -t public/
```

Or via artisan command:

```sh
php artisan serve
```

To authenticate a user, make a `POST` request to `/api/auth/login` with parameter as mentioned below:

```
email: johndoe@example.com
password: johndoe
```

Request:

```sh
curl -X POST -F "email=johndoe@example.com" -F "password=johndoe" "http://localhost:8000/api/auth/login"
```

Response:

```
{
  "success": {
    "message": "token_generated",
    "token": "a_long_token_appears_here"
  }
}
```

- With token provided by above request, you can check authenticated user by sending a `GET` request to: `/api/auth/user`.

Request:

```sh
curl -X GET -H "Authorization: Bearer a_long_token_appears_here" "http://localhost:8000/api/auth/user"
```

Response:

```
{
  "success": {
    "user": {
      "id": 1,
      "name": "John Doe",
      "email": "johndoe@example.com",
      "created_at": null,
      "updated_at": null
    }
  }
}
```

- To refresh your token, simply send a `PATCH` request to `/api/auth/refresh`.
- Last but not least, you can also invalidate token by sending a `DELETE` request to `/api/auth/invalidate`.
- To list all registered routes inside your application, you may execute `php artisan route:list`

```
⇒  php artisan route:list
+--------+----------------------+---------------------+------------------------------------------+------------------+------------+
| Verb   | Path                 | NamedRoute          | Controller                               | Action           | Middleware |
+--------+----------------------+---------------------+------------------------------------------+------------------+------------+
| POST   | /api/auth/login      | api.auth.login      | App\Http\Controllers\Auth\AuthController | postLogin        |            |
| GET    | /api                 | api.index           | App\Http\Controllers\APIController       | getIndex         | jwt.auth   |
| GET    | /api/auth/user       | api.auth.user       | App\Http\Controllers\Auth\AuthController | getUser          | jwt.auth   |
| PATCH  | /api/auth/refresh    | api.auth.refresh    | App\Http\Controllers\Auth\AuthController | patchRefresh     | jwt.auth   |
| DELETE | /api/auth/invalidate | api.auth.invalidate | App\Http\Controllers\Auth\AuthController | deleteInvalidate | jwt.auth   |
+--------+----------------------+---------------------+------------------------------------------+------------------+------------+
```

## Future

I will make a standalone package that would works on your current project.

## License

```
Laravel and Lumen is a trademark of Taylor Otwell
Sean Tymon officially holds "Laravel JWT" license
```

## Sample API URLs to Test(Geoffrey):

- API Start: api>php -S localhost:8000 -t public

- php artisan migrate

- php artisan db:seed

- APP Start: view>npm start

-Sample URL: http://localhost:8000/api/example

-User Authentication - Test URL: localhost:8000/api/auth/login
```
=====================================
|Method: POST                       |
|Data: [                            |
|email: johndoe@example.com,        |
|password: johndoe                  |
|]                                  |
====================================|
```
  
3. CRUD API tests.
  - Create CRUD = php artisan make:crud Item => ItemController
  - Create: http://localhost:8000/api/main_category
    - Method: POST
    - { body->form data: { name: 'Gadgets' } }

  - Read: http://localhost:8000/api/main_category and  http::/localhost:8000/api/main_category/1
    -- Method: GET
    -- 1 in the second url defines the id of data
  - Update: http://localhost:8000/api/main_category/1
    -- Method: PUT
    -- 1 in the url defines the ID value of data
  - Delete: http://localhost:8000/api/main_category/1
    -- Method: Delete
    -- 1 in the url defines the ID value of data

4. Responses Use 'data' as primary object to get records from the database.

## ERROR on vendor/zizaco/entrust/src/Entrust/EntrustServiceProvider.php

Modify vendor/zizaco/entrust/src/Entrust/EntrustServiceProvider.php to match https://github.com/mauvm/entrust/commit/394dbb71b997538e235fe711ab075d6f765a6736

- Line 31: __DIR__.'/../config/config.php' => config_path('entrust.php'), to __DIR__.'/../config/config.php' => app()->basePath() . '/config/entrust.php',
- Line 61: paste if (!class_exists('\Blade')) return;