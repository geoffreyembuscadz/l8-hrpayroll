<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\ZKAttendance;
use ZK\ZKLib;
use App\Http\Controllers\AttendanceController;
use App\Http\Models\CommonModel;
use App\Http\Controllers\TaskController;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\CreateCrud::class,
        \Laravelista\LumenVendorPublish\VendorPublishCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {

        //fecth data in bio then save it to temp_stor_fr_bio table
        $schedule->call(function () {

            $data = CommonModel::getBioDevice();

            foreach ($data as $key => $d) {
                $ip = $d->id;
                $status = '';
                $zk = new ZKLib($ip, 4370);
                $ret = $zk->connect();
                $attendance = $zk->getAttendance();

                $x = trim($zk->deviceName(), '~DeviceName=');
                $deviceName = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $x);
                
                $len = count($attendance);
                for ($i=0; $i <79 ; $i++) { 
                    $table = new ZKAttendance;
                    $table->uid = $attendance[$i][0];
                    $table->bio_id = $attendance[$i][1];
                    $table->device_name = $deviceName;
                    $table->type =  $attendance[$i][2];
                    $table->date = date( "Y-m-d", strtotime( $attendance[$i][3] ) );
                    $table->time = date( "H:i:s", strtotime( $attendance[$i][3] ) );
                    $table->date_time = $attendance[$i][3];
                    $table->save();
                }

            //note delete attendance after you get it
            // // $zk->clearAttendance();
            }
            
        })->everyTenMinutes();

        //same as above but it is only a tester for a specific biometrics
        // $schedule->call(function () {
        //     $ip = "192.168.3.202";
        //     $status = '';
        //     $zk = new ZKLib($ip, 4370);
        //     $ret = $zk->connect();
        //     $attendance = $zk->getAttendance();

        //     $x = trim($zk->deviceName(), '~DeviceName=');
        //     $deviceName = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $x);
            
        //     $len = count($attendance);
        //     for ($i=0; $i <79 ; $i++) { 
        //         $table = new ZKAttendance;
        //         $table->uid = $attendance[$i][0];
        //         $table->bio_id = $attendance[$i][1];
        //         $table->device_name = $deviceName;
        //         $table->type =  $attendance[$i][2];
        //         $table->date = date( "Y-m-d", strtotime( $attendance[$i][3] ) );
        //         $table->time = date( "H:i:s", strtotime( $attendance[$i][3] ) );
        //         $table->date_time = $attendance[$i][3];
        //         $table->save();
        //     }

        //     //note delete attendance after you get it
        //     // // $zk->clearAttendance();

        // })->everyThirtyMinutes();

        // fetch data from temp_stor_fr_bio table to be computed and save in employees_logs
        $schedule->call(function () {
            $contr = new AttendanceController;
            $contr->fetchDataFromBioTable();
            
        })->everyThirtyMinutes();

        //notify task deadline and save to notification table
        $schedule->call(function () {
            $contr = new TaskController;
            $contr->notifyTaskDeadline();
        })->everyTenMinutes();
    }

}