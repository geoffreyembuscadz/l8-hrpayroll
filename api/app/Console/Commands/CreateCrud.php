<?php

namespace App\Console\Commands;

use \Lang;
use \Config;
use Illuminate\Console\Command;
use \Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Artisan;

class CreateCrud extends Command
{
    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:crud {class} {--n=default} {--m=true}'; // Can be treated as table name or controller name to be made.

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It create\'s a RESTful Controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      //
      $args     = $this->arguments();
      $options  = $this->options();
      $class    = $args['class'];
      $confirm  = $this->confirm('Do you wish to continue on creating a CRUD for ' . $class . '?');

      if(!empty($class) && $confirm){
        exec('composer dump-autoload');
        sleep(1);

        // Handles Optional Values
        if(isset($options['m']) && $options['m'] == 'true'){
          // Creates an Migration File
          Artisan::call('make:migration', [ 'name' => 'create_' . strtolower($class) . '_table', '--create' => strtolower($class), '--table' => strtolower($class) ]);
          $this->line('Controller for ' . $class . ' has been successfully created.');
        }

        // Creates a Controller Class
        $this->createController($class, $options['n']);

        // Creates a Model Class
        $this->createModel($class, $options['n']);

        $this->line('Controller for ' . $class . 'Controller is successfully created!');
        $this->line('Model for ' . $class . 'Model is successfully created!');
      } else {
        $this->line('Creating a CRUD has cancelled.');
      }
    }

    private function createController($class, $namespace = ''){
      $controller = $class . 'Controller';
      $model = ucfirst($class) . 'Model';
      $destination_controller = __DIR__ . '\..\..\Http\Controllers\\';
      $curr_destination_controller = __DIR__ . '\..\..\Http\Controllers\\' . $controller . '.php';

      // Artisan::call('make:controller', [ 'name' => $controller ]);
      $handle_controller_file = fopen( $curr_destination_controller, 'w' );
      $handle_controller_file = fclose( $handle_controller_file );

      $script_content = $this->files->get( __DIR__ . '/../../..//resources/stubs/crudcontroller.stub' );

      $script_content = str_replace( 'WalaController', $controller, $script_content );
      $script_content = str_replace( 'WalaTable', strtolower($class), $script_content );

      if($namespace != 'default'){
        $model = ucfirst($namespace) . '\\' . $model;
        $new_destination_controller = $destination_controller . ucfirst(trim($namespace));

        if( !$this->files->isDirectory($new_destination_controller) ){
          $this->files->makeDirectory($new_destination_controller);
        }

        $script_content = str_replace('namespace App\Http\Controllers', 'namespace App\Http\Controllers\\' . ucfirst(trim($namespace)), $script_content);
        $script_content = str_replace( 'WalaModel', $model, $script_content );

        $this->files->put( $curr_destination_controller, $script_content );

        $this->files->move($curr_destination_controller, $new_destination_controller . '\\' . $controller . '.php');

      } else {
        $script_content = str_replace( 'WalaModel', $model, $script_content );

        $this->files->put( $destination_controller . $controller . '.php', $script_content );
      }
    }

    private function createModel($class, $namespace = ''){
      $curr_model_dir = __DIR__ . '\..\..\\' . $class . 'Model.php';

      $handle_app_model_file = fopen( $curr_model_dir, 'w' );
      $stub_model = file_get_contents( __DIR__ . '/../../../resources/stubs/crudmodel.stub' );
      file_put_contents($curr_model_dir, $stub_model);
      $handle_app_model_file = fclose( $handle_app_model_file );

      if($this->files->exists( $curr_model_dir )){
        $dir_models = __DIR__ . '\..\..\Http\Models\\';

        $model_content = $this->files->sharedGet( $curr_model_dir );

        $old_new_model_content = [
          'namespace App' => 'namespace App\Http\Models',
          'use Illuminate\Database\Eloquent\Model;' => 'use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;',
          '//' => 'protected $table = "' . strtolower($class) . '"; use SoftDeletes;',
          'DummyModel' => $class . 'Model'
        ];

        if( !$this->files->isDirectory( $dir_models ) ){
          $this->files->makeDirectory( $dir_models );

          if( $namespace != 'default' ){
            $dir_models = $dir_models . '\\' . ucfirst($namespace) . '\\';

            $this->files->makeDirectory( $dir_models );

            $old_new_model_content['namespace App'] = 'namespace App\Http\Models\\' . ucfirst($namespace);
          }
        }

        foreach($old_new_model_content AS $old_content => $new_content){
          $model_content = str_replace($old_content, $new_content, $model_content);
        }

        $this->files->put($curr_model_dir, $model_content);

        $this->files->move($curr_model_dir, $dir_models . $class . 'Model.php' );
      }
    }
}
