<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use \Illuminate\Http\Request;
use App\Http\Models\GovernmentDeductionModel;

class GovernmentDeductionController extends Controller{

    public function computeDeduction($basicPay){
       
        $basic_pay = $basicPay; 
        $data = GovernmentDeductionModel::computeThisDeduction($basic_pay);
        

        $data = [ 'data' => $data ];
        
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }





} 
