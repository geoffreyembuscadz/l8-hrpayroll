<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;

class PositionController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'name', 'created_at' ];

	public $model = 'PositionModel';

	public $rules = [ 'name' => 'required' ];

	public $table = 'position';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }
    public function getList (){
        $model=$this->model->select(
        'position.id',
        'position.name',
        'position.description');
        return $model->get();
   
    }
    public function position(){

        $data = $this->model->position();
        $data = [ 'data' => $data ];
        
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }


}
