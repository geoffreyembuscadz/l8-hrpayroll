<?php

namespace App\Http\Controllers;

use App\ResetsPasswords;
use Illuminate\Support\Facades\Notifications;

class PasswordController extends Controller
{
    use ResetsPasswords;

    public function __construct()
    {
        $this->broker = 'users';
    }
}