<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\NotificationModel;

class NotificationController extends Controller
{

    public function getNotification(){
 
        $data = NotificationModel::getNotification();

        $data = [ 'data' => $data ];
        
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function updateNotification(Request $request){

        $data=$request->all();
 
        $data = NotificationModel::updateNotification($data);
        

        $data = [ 'data' => $data ];
        
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function getNotif(Request $request){

        $data=$request->all();

        $data['id'] = explode(",",$data['id']);
 
        $data = NotificationModel::getNotif($data);
        

        $data = [ 'data' => $data ];
        
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function getAllNotification(Request $request){

        $data = $request->all();
 
        $data = NotificationModel::getAllNotification($data);
        

        $data = [ 'data' => $data ];
        
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function updateAllNotif(Request $request){

        $data=$request->all();

        $data = NotificationModel::updateAllNotif($data);
        
        $data = [ 'data' => $data ];
        
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }
}
