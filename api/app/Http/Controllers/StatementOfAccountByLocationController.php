<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use \App\Http\Models\PayrollListDataModel as PayrollList;
use \App\Http\Models\CompanyModel as Company;
use \App\Http\Models\UserModel as User;
use Carbon\Carbon as Carbon;
use DB;

class StatementOfAccountByLocationController extends Controller
{
     public function exportSOA(Request $request) {

      $total_billable = $request->input('total_billable');
      $admin_fee      = $request->input('admin_fee');
      $total_mandated = $request->input('total_mandated');
      $vat             = $admin_fee * .12;
      $less2           = $admin_fee * .02;
      $company        = $request->input('company');
      $branch         = $request->input('branch');
      $start          = $request->input('start');
      $end            = $request->input('end');
      $prepared_by    = $request->input('prepared_by');

      
      $data = array(
        'total_billable' => $total_billable,
        'admin_fee'      => $admin_fee,
        'vat'            => $vat,
        'total_mandated' => $total_mandated,
        'less2'          => $less2,
        'date'           => Carbon::now(),
        'company'        => $company,
        'branch'         => $branch,
        'start'          => $start,
        'end'            => $end

      );


      $company_list = DB::table('company')
                      ->select('company.name', 'company.address', 'company.phone_number')
                      ->join('company_branch', 'company.id', '=', 'company_branch.company_id')
                      ->where('company_branch.id', '=', $company)
                      ->first();

      $user_list    = DB::table('users')
                      ->select('users.name')
                      ->where('users.id', '=', $prepared_by)
                      ->first();


      PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif'])->setPaper('a4');

      $pdf = PDF::loadView('export.statement_of_account_loc', ['company_list' => $company_list, 'user_list' => $user_list] ,array('data' => $data));
      return $pdf->stream('statement_of_account.pdf');
    }
}
