<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use \Illuminate\Http\Request;


class PayrollReportController extends CrudController{
	public $auth = false;

	public $list_columns = [];

	public $model = 'PayrollReportModel';

	public $rules = [];

	public $table = 'payroll';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        return $model;
    }

    public function postList($model){
        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }

    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    /**
     * get employee total adjustment, total deductions, total sss, total philhealth, total pagibig etc.. per employee
     * @param  Request emp_id
     * @return object       
     */
    public function getPayrollReport($id){
        $data = [];

        $data['data'] = $this->model->getPayrollReport($id);
        
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    /**
     * get employee list of adjustment per payroll
     * @param  Request payroll_id and emp_id
     * @return object         
     */
    public function getPayrollAdjustment($id) {
        $data = [];

        $data['data'] = $this->model->getPayrollAdjustment($id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    /**
     * get employee deduction per payroll
     * @param  Request payroll_id and emp_id
     * @return object
     */
    public function getPayrollDeduction($id) {
        $data = [];

        $data['data'] = $this->model->getPayrollDeduction($id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    /**
     * get employee loan per payroll
     * @param  Request payroll_id and emp_id
     * @return object         
     */
    public function getPayrollLoan($id) {
        $data = [];

        $data['data'] = $this->model->getPayrollLoan($id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    public function getEmployee() {
        $data = [];

        $data['data'] = $this->model->getEmployee();

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

}
