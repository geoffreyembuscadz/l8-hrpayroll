<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use \Illuminate\Http\Request;
use Carbon\Carbon;

class TaskController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'created_at' ];

	public $model = 'TaskModel';

	public $rules = [];

	public $table = 'task_list';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function getTask (Request $request){

        $data = $request->all();

        $data['employee_id'] = explode(",",$data['employee_id']); 

        $data = $this->model->getTask($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function duplicateEntryValidation(Request $request){
        $data=($request->all());

        $duplication=$this->model->duplicateEntryValidation($data);
        if($duplication != []){

            $response['data'] = array(
                        'message'      => 'duplicate',
                        'status_code'  => 400,
                        'data'  => $duplication
                    );
            return $response;
        }
        else{
            $response['data'] = array(
                        'message'      => 'not duplicate'
                    );
            return $response;
        }
    }

    public function createTask(Request $request){
        $data=[];
        $array=($request->all());
        
        $len = count($array["employee_id"]);

       
        for ($i=0; $i < $len; $i++) { 
            array_push(
                $data,
                array(
                    'employee_id'   => $array["employee_id"][$i], 
                    'task'          => $array["task"], 
                    'deadline'      => $array["deadline"]
                    )
                );
        }

        $return=$this->model->createTask($data);

        $response=[
          'data' => $return
        ];
        return $response;
    }

    public function show ($id){
        $data = [];
        $data['data'] = $this->model->show($id);
        $data = $this->addShowData($data, $id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function updateTask(Request $request){

        $data=($request->all());

        $data = $this->model->updateTask($data);
            
        return $data;
    }

    public function getMyTask(Request $request){

        $data = $request->all();
        
        $data =  $this->model->getMyTask($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function updateTaskTime(Request $request){

        $data=($request->all());

        $data = $this->model->updateTaskTime($data);
            
        return $data;
    } 

    public function updateTaskStatus(Request $request){

        $data=($request->all());

        $return = $this->model->updateTaskStatus($data);

        $response=[
          'data' => $return
        ];
        return $response;
    }

    public function notifyTaskDeadline(){ 

        $return = $this->model->notifyTaskDeadline();

        $response=[
          'data' => $return
        ];
        return $response;
    }

    public function getMyTaskForDashboard(){
        
        $data =  $this->model->getMyTaskForDashboard();
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function approvedTask(Request $request){

        $data=($request->all());

        $return = $this->model->approvedTask($data);

        $response=[
          'data' => $return
        ];
        return $response;
    }

}
