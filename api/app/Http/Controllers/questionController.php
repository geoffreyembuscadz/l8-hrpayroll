<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use \Illuminate\Http\Request;
use App\Http\Models\QuestionModel;

class questionController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'question' ];

	public $model = 'QuestionModel';

	public $rules = [ 'question' => 'required' ];

	public $table = 'question';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        return $model;
    }

    public function postList($model){

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function question(){

        $data = QuestionModel::question();
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }



}
