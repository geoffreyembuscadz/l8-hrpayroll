<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use App\Http\Models\PayrollListModel as PayrollListModel;
use \DB AS DB;


class PayrollController extends CrudController{
	public $auth = false;

	public $list_columns = [];

	public $model = 'PayrollModel';

	public $rules = [];

	public $table = 'payroll';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        $model = $this->model->preList($model);
        return $model;
    }

    public function postList($model){
        $model = $this->model->postList($model);
        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }


}
