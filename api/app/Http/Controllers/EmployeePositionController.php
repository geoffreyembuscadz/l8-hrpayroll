<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use App\Http\Controllers\EmployeePositionController;

class EmployeePositionController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'created_at' ];

	public $model = 'EmployeePositionModel';

	public $rules =  [ ];

	public $table = 'employee_position';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        // dd($data);
        // exit;
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

   public function getList(){
        $data = $this->model->getRecords();
            return $data;
   }

   // public function getList (){
   //      $model=$this->model->select('employee_position.department_id', 'employee_position.position');
   //      return $model->get();

   //   }


}
