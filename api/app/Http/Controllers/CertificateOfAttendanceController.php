<?php

namespace App\Http\Controllers;

use \DB;
use App\Http\Controllers\CrudController;
use \Illuminate\Http\Request;
use App\Services\PayUService\Exception;

class CertificateOfAttendanceController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'created_at' ];

	public $model = 'CertificateOfAttendanceModel';

	public $rules = [];

	public $table = 'certi_of_atten';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){

        return $data;

    }

    public function updateData(Request $request){

        $data=($request->all());

        $duplication=$this->model->preUpdateDuplication($data);
        if($duplication != []){
            $response['data'] = array(
                        'message'      => 'no changes',
                        'status_code'  => 200
                    );
            return $response;
        }
        else{
            $employee_logs_data=$this->model->checkLogs($data);
            if(count($employee_logs_data) < 1){
                $final_data = $this->model->typeValidation($data);
                $return = $this->model->updateData($final_data);
            }
            else{
                $getEmployee =$this->model->getEmployeeName($employee_logs_data);
                $response['data'] = array(
                        'message'      => 'Not Found',
                        'status_code'  => 404,
                        'data'         => $getEmployee
                    );
                return $response;
            }
        }
            
        return $return;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function getList (){

        $model =  $this->model->getCOAData();
        return $model;
    }

    public function createCOA(Request $request){
        $data=[];
        $error = [];
        $array=($request->all());
        $len = count($array["employee_id"]);
        $response = [];
        $employee_logs_data = [];

        for ($i=0; $i < $len; $i++) { 
            array_push(
                $data,
                    array(
                        'emp_id'          =>$array["employee_id"][$i], 
                        'coa_type_id'     =>$array["coa_type_id"], 
                        'date'            =>$array["date"], 
                        'time'            =>$array["time"], 
                        'remarks'         =>$array["remarks"]
                    )   
            );
        }

        //check kung totoo ba na my attendance sya nung araw na yun kaso nakalimutan nya mag logIn or logOut
        $employee_logs_data=$this->model->checkEmployeeLogs($data);
 
        if(count($employee_logs_data) == 0){
            //pag IN yung pinili nyang type kaso OUT pala sya walang in log, itong function na to ang mag aayos nun
            $final_data=$this->model->coaTypeValidation($data);
            $return=$this->model->createCOA($final_data);
        }
        else{
            //kukunin nya yung mga name nung mga taong walang attendance nung araw yun pero nag file ng coa
            $getEmployee =$this->model->getEmployeeName($employee_logs_data);
            $response['data'] = array(
                    'message'      => 'Not Found',
                    'status_code'  => 404,
                    'data'  => $getEmployee
                );
            return $response;
        }

        $response=[
          'data' => $return
        ];
        return $response;
    }


    public function show ($id){
        $data = [];
        $data['data'] = $this->model->show($id);
        $data = $this->addShowData($data, $id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function getMyCOA(Request $request){
        $data = $request->all();
 
        $data['status_id'] = explode(",",$data['status_id']); 
        $data['type_id'] = explode(",",$data['type_id']); 

        $data =  $this->model->getMyCOA($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    public function getCOAFilter(Request $request){
        $data = $request->all();

        $data['employee_id'] = explode(",",$data['employee_id']); 
        $data['company_id'] = explode(",",$data['company_id']); 
        $data['status_id'] = explode(",",$data['status_id']); 
        $data['type_id'] = explode(",",$data['type_id']); 

        $dat = $this->model->getCOAFilter($data);
        $da = [ 'data' => $dat ];

        return response()->json($da)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function duplicateEntryValidation(Request $request){
        $data=($request->all());

        $duplication=$this->model->duplicateEntryValidation($data);
        if($duplication != []){

            $response['data'] = array(
                        'message'      => 'duplicate',
                        'status_code'  => 400,
                        'data'  => $duplication
                    );
            return $response;
        }
        else{
            $response['data'] = array(
                        'message'      => 'not duplicate'
                    );
            return $response;
        }
    }

    public function updateApprovedRequest($id){
        $data = $this->model->approvedData($id);

        return $data;
    }
}
