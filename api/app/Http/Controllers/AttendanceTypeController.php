<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;


class AttendanceTypeController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id','created_at' ];

	public $model = 'AttendanceTypeModel';

	public $rules = [ ];

	public $table = 'attendance_type';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function getList (){
        $model=$this->model->select('attendance_type.id',
            'attendance_type.name',
            'attendance_type.description');
        return $model->get();
    }

    public function example(){
        return 'LL';
    }


}
