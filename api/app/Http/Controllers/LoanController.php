<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use \Illuminate\Http\Request;
use DB;


class LoanController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'name', 'created_at' ];

	public $model = 'LoanModel';

	public $rules = [];

	public $table = 'loan';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        $model = $this->model->preListJoin($model);
        return $model;
    }

    public function postList($model){
        $emp_id = $this->request['emp_id'];
        $start  = $this->request['start'];
        $end    = $this->request['end'];
        $model  = $this->model->postListJoin($model, $emp_id, $start, $end);
        return $model;
    }

    public function store(Request $request){
        $this->validationRuleStore();

        $errors = (count($this->rules)) ? $this->validate($request, $this->rules) : [];

        $column_primary_key = $this->model_col_pk;

            if( !$errors ){
                $data = [];

                $data['data'] = $request->all();

                $data = $this->preStore($data);

                $data['data'] = [];

                $data['message'] = 'Record is successfully added.';
            }

            $data['errors'] = $errors ? $errors : [];

            return $errors ? $errors : response()->json($data)->withHeaders([
              'Access-Control-Allow-Origin' => '*', 
              'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
              'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
            ]);

    }

	public function preStore($data = []){
        $model = $this->model->preStore($data);     
        return $model;         
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }


}
