<?php

namespace App\Http\Controllers;

use \DB;
use \Illuminate\Http\Request;
use App\Http\Controllers\CrudController;


class ScheduleAdjustmentsController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'created_at' ];

	public $model = 'ScheduleAdjustmentsModel';

	public $rules = [];

	public $table = 'schedule_adjustments';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        $status = $data['data']['status_id'];

        if($status == null){
            // $this->model->preUpdate($id,$data);
        }
        return $data;
    }

    public function updateData(Request $request){

        $data=($request->all());

        $data = $this->model->updateData($data);
            
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function getList(){
        $model = $this->model->getList();

        return $model;
    }

    public function createSA(Request $request){
        $data=[];
        $array=($request->all());
       
        $len = count($array["employee_id"]);

       
        for ($i=0; $i < $len; $i++) { 

            array_push(
                $data,
                    array(
                        'emp_id'        => $array["employee_id"][$i],
                        'start_date'    => $array["start_date"], 
                        'end_date'      => $array["end_date"],
                        'remarks'       => (!empty($array["remarks"])) ? $array['remarks'] : null,
                        'details'       => $array["details"]
                    )
            );
        }

        $return=$this->model->createSA($data);
   
        $response=[
          'data' => $return
        ];
        return $response;
    }

    // Gets Schedule Adjustment request
    public function getScheduleAdjustmentRequest($id){
        $data = [ 'data' => [] ];
        
        if($id){
            $data['data'] = $this->model->getSArequest($id);
        }

        return $data;
    }

    public function updateScheduleAdjustmentRequest(Request $request){
        $data = $request->all();
        $id = $data['id'];
        $data['break_mins'] = $data['lunch_mins_break'];
        unset($data['id']);
        unset($data['company_id']);
        unset($data['lunch_mins_break']);

        $data['break_end'] = date("H:i", strtotime('+'.(int)$data['break_mins'].' minutes', strtotime($data['break_start'])));
        unset($data['break_mins']);

        DB::table('schedule_adjustments_request')->where('id', $id)->update($data);

        return [ 'data' => $data ];

    }

    public function show($id){
        $data = [];
        $data['data'] = $this->model->show($id);
        $data = $this->addShowData($data, $id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function getMySA(Request $request){
        $data = $request->all();
        $data['status_id'] = explode(",",$data['status_id']); 

        $data =  $this->model->getMySA($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    public function getSA(Request $request){
        $data = $request->all();

        $data['employee_id'] = explode(",",$data['employee_id']); 
        $data['company_id'] = explode(",",$data['company_id']); 
        $data['status_id'] = explode(",",$data['status_id']); 

        $data = $this->model->getSA($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function duplicateEntryValidation(Request $request){
        $data=($request->all());

        $duplication=$this->model->duplicateEntryValidation($data);
        if($duplication != []){

            $response['data'] = array(
                        'message'      => 'duplicate',
                        'status_code'  => 400,
                        'data'  => $duplication
                    );
            return $response;
        }
        else{
            $response['data'] = array(
                        'message'      => 'not duplicate'
                    );
            return $response;
        }
    }

}
