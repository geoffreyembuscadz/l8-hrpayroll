<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;


class PagibigController extends CrudController{
    public $auth = false;

	public $list_columns = [ 'id','created_at' ];

	public $model = 'PagibigModel';


	public $rules = [ ];


	public $table = 'pagibig';

    // get specific list of data
    

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }


    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	

	public function preStore($data = []){
        //pre means bago pumasok
        $data['data']['created_at'] = date('Y-m-d H:i:s');

        return $data;
    }

	public function postStore($id, $data = []){

        // insert
        // $params = [
        // 'id' => $id,
        // 'emp_id' =>$data['data']['employees_id']
        // ];

        // $model = LeaveModel::leaveStatus($params);


        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }


    public function getList (){
        $model=$this->model->select('leave.type');
        return $model->get();

        // $model = MenuModel::getRecords();
        // return $model;
    }


}
