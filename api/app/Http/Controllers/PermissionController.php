<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;

class PermissionController extends CrudController{
	public $auth = false;

    public $list_columns = [ 'name', 'display_name', 'description' ];

    public $model = 'PermissionModel';

    public $rules = [ 'display_name' => 'required' ];

    public $table = 'permissions';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        $model = $this->model->preList($model);
        return $model;
    }

    public function postList($model){
        $model = $this->model->postList($model);
        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }

    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }


}
