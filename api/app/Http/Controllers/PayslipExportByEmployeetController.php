<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use \App\Http\Models\PayrollListDataModel as PayrollListModel;
use \App\Http\Models\PayrollModel as PayrollModel;
use App\Http\Models\AdjustmentModel as Adjustment;
use App\Http\Models\LoanModel as Loan;
use Carbon\Carbon as Carbon;
use DB;

class PayslipExportByEmployeetController extends Controller
{
    public function exportPayslip(Request $request) {
      	$id          = $request->input('id');
      	$company     = $request->input('company');
      	$station     = $request->input('station');
        $start       = $request->input('start');
      	$end         = $request->input('end');


      	$data = array(
  		    'company'         => $company,
  		    'station'  	      => $station,
          'start'           => $start,
          'end'             => $end,
          'date'            => Carbon::now()
  		);
  		    	
  		$payrollList = new PayrollListModel;
      $payrollList = DB::table('payroll_list')
                        ->select('*', 'payroll.start_time', 'payroll.end_time', 'company.name as company_name', 'company_branch.branch_name as branch_name')
                        ->join('payroll','payroll_list.payroll_id', '=', 'payroll.id')
                        ->join('employee_company', 'payroll_list.emp_id', '=', 'employee_company.employee_id')
                        ->join('company', 'employee_company.company_id', '=', 'company.id')
                        ->join('employee', 'payroll_list.emp_id', '=', 'employee.employee_code')
                        ->join('company_branch', 'employee.branch', '=', 'company_branch.id')
                        ->where('emp_id', '=', $id)
                        ->whereNull('payroll_list.deleted_at')
                        ->get();


      $adjustment = new Adjustment;
      $adjustment = DB::table('payroll_adjustment')
                    ->select(
                      'payroll_adjustment.id', 
                      'payroll_adjustment.adjustment_type', 
                      'payroll_adjustment.employee_id', 
                      'adjustment_type.display_name as display_name', 
                      DB::raw("(select if(adjustment_type.taxable = 1, 'Yes', 'No' )) AS taxable"),
                      DB::raw("(select if(adjustment_type.billable = 1, 'Yes', 'No' )) AS billable"),
                      'adjustment_type.type as adjustment_type', 
                      'payroll_adjustment.type', 
                      'payroll_adjustment.created_at',
                      'payroll_adjustment.adjustment_amount'

                      )
                    ->join('adjustment_type', 'payroll_adjustment.adjustment_type', '=', 'adjustment_type.id')
                    ->whereNull('payroll_adjustment.deleted_at')
                    ->where('payroll_id', '=', $id)
                    ->get();

      $loans = new Loan;
      $loans = DB::table('payroll_loan')
              ->select(
                  'payroll_loan.id', 
                  'payroll_loan.loan_type', 
                  'payroll_loan.employee_id', 
                  'loan_type.display_name', 
                  'loan_type.type as loan_type', 
                  'payroll_loan.loan_amount', 
                  'payroll_loan.type', 
                  'payroll_loan.created_at'
              )
              ->join('loan_type', 'payroll_loan.loan_type', '=', 'loan_type.id')
              ->whereNull('payroll_loan.deleted_at')
              ->where('payroll_id', '=', $id)
                ->get();
                


      PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif'])->setPaper('a4');

    	$pdf = PDF::loadView('export.payslip_user', ['payrollList' => $payrollList, 'adjustment' => $adjustment, 'loans' => $loans],  array('data' => $data));
    	return $pdf->stream('payslip_user.pdf');
    }
}
