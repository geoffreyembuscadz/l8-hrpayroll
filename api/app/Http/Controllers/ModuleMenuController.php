<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;

class ModuleMenuController extends CrudController{
	public $auth = false;

	public $list_columns = [ ];

	public $model = 'ModuleMenuModel';

	public $rules = [ ];

	public $table = 'module_menus';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        $model = $this->model->preListJoin($model);

        return $model;
    }

    public function postList($model){
        $model = $this->model->postListJoin($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }


}
