<?php

namespace App\Http\Controllers;
use \Illuminate\Http\Request;
use App\Http\Controllers\CrudController;
use App\Http\Models\EventModel;

class EventController extends CrudController{
   public $auth = false;

   public $list_columns = [ 'id','created_at' ];

   public $model = 'EventModel';

   public $rules = [ ];

   public $table = 'event';
   public $details; 

   public function addShowData($data, $id = null){
      return $data;
   }

   public function preList($model){
// $model = $this->model->functionJoinAndWhere($model);

      return $model;
   }

   public function postList($model){
// $model = $this->model->functionSelect($model);

      return $model;
   }

   public function preStore($data = []){

      $details = $data;
      return $data;
   }

   public function postStore($id, $data = []){
      return $data;
   }
// -- /Store Data --//

// -- Update Data --//
   public function preUpdate($id, $data = []){
      return $data;
   }

   public function postUpdate($id, $data = []){
      return $data;
   }

   public function show($id){
      $data = [];
      $data['data'] = $this->model->show($id);
      $data = $this->addShowData($data, $id);

      return response()->json($data)->withHeaders([
         'Access-Control-Allow-Origin' => '*', 
         'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
         'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
         ]);
   }


   public function postEventDetails(Request $request){

      $data = $request->all();


      $return = $this->model->postEventDetails($data);
      $response=[
      'data' => $return
      ];
      return $response;
   }

   public function getList (){

      $model = $this->model->getRecords();
      return $model;
   }

   public function showEvent(Request $request){


      $queryString = $request->all();

      $start_date = date("Y-m-d",$queryString['start']);
      $end_date = date("Y-m-d",$queryString['end']);

      $data = EventModel::eventLists($start_date, $end_date);
      $data = $data;


      return response()->json($data)->withHeaders([
         'Access-Control-Allow-Origin' => '*', 
         'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
         'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
         ]);

   }


   public function eventLists($filterEvent){

      $data = EventModel::filterRecords($filterEvent);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
         'Access-Control-Allow-Origin' => '*', 
         'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
         'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
         ]);

   }

   public function getEventName(Request $request){
      $data = $request->all();

      $data = $this->model->getEventName($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
         'Access-Control-Allow-Origin' => '*', 
         'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
         'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
         ]);

   }

   public function getEventTypes(){


      $data = EventModel::getET();


      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
         'Access-Control-Allow-Origin' => '*', 
         'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
         'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
         ]);

   }
   public function getEventStat(){


      $data = EventModel::getEventStat();


      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
         'Access-Control-Allow-Origin' => '*', 
         'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
         'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
         ]);

   }
   public function eventList(){


      $data = EventModel::eventList();


      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
         'Access-Control-Allow-Origin' => '*', 
         'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
         'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
         ]);

   }


}
