<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use App\Http\Controllers\CrudController;
use App\Http\Models\CompanyPolicyModel;

class CompanyController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'name', 'created_at' ];

	public $model = 'CompanyModel';

	public $rules = [];

	public $table = 'company';

    public function addShowData($data, $id = null){
        if(!empty($id)){
            $data['data']['policies'] = $this->model->getCompanyPolicies($id);
            $data['data']['policy_equations'] = $this->model->getCompanyPolicyEquations($id);
            $data['data']['branches'] = $this->model->getBranchesByCompany($id);
            $data['data']['cutoff_dates'] = $this->model->getCompanyCutoffDates($id);
            $data['data']['emp_govrn_fields'] = $this->model->getGovernmentFields($id);
            $data['data']['schedules'] = $this->model->getCompanyWorkingSchedules($id);
        }
        return $data;
    }

    public function preList($model){
        $model = $this->model->preList($model);
        return $model;
    }

    public function postList($model){
        $model = $this->model->postListJoin($model);
        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        $this->model->setDefaultPolicies($id);
        $this->model->setDefaultCutOffDates($id);
        $this->model->addDefaultMainBranch($id);
        $this->model->addDefaultMainSchedule($id);
        $this->model->addDefaultGovernmentField($id);
        return $data;
    }

    public function preUpdate($id, $data = []){

        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function updatePolicy(Request $request, $company_id){
        $input = $request->all();
        $data = [];
        $policies = [];
        $company_cutoff_date = [];
        $company_government_field = [];

        $errors = [];

        foreach($input AS $key_index => $input_val){
            $key_index = str_replace(']', '', $key_index);
            $index_pieces = explode('[', $key_index);

            if($index_pieces[0] == 'policies'){
                $policies[$index_pieces[1]][$index_pieces[2]] = $input_val;
            } elseif($index_pieces[0] == 'company_cutoff_date'){
                $company_cutoff_date[$index_pieces[1]] = $input_val;
            } elseif($index_pieces[0] == 'company_government_field'){
                $company_government_field[$index_pieces[1]][$index_pieces[2]] = $input_val;
            } else {
                $data[$key_index] = $input_val;
            }
        }

        $return = [];
        $data['policies'] = $policies;
        $data['company_government_field'] = $company_government_field;
        $data['company_cutoff_date'] = $company_cutoff_date;

        $return['data'] = $this->model->updateCompanyPolicy($company_id, $data);

        $return['message'] = "Record is successfully updated.";

        return $errors ? $errors : response()->json($return)->withHeaders([
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function updateWorkingSchedules(Request $request, $company_id){
        $input = $request->all();
        $data = [];
        $schedules = [];
        $errors = [];
        $return = [];

        foreach($input AS $key_index => $input_val){
            $key_index = str_replace(']', '', $key_index);
            $index_pieces = explode('[', $key_index);

            if($index_pieces[0] == 'schedules'){
                $schedules[$index_pieces[1]][$index_pieces[2]] = $input_val;
            } else {
                $data[$key_index] = $input_val;
            }
        }

        $data['schedules'] = $schedules;

        // $return['data'] = $data;
        $return['data'] = $this->model->updateCompanyWorkingSchedules($company_id, $data['schedules']);

        $return['message'] = "Record is successfully updated.";

        return $errors ? $errors : response()->json($return)->withHeaders([
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }


    public function getList(){
        $model=$this->model->select
            (
            'company.id', 
            'company.name',
            'company.description',
            'company.address',
            'company.email',
            'company.phone_number',
            'company.contact_person',
            'company.thirteen_month_rule'
            );
        return $model->get();
    }

    public function showComInfo(){

        $data = $this->model->showComInfo();
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
            'Access-Control-Allow-Origin' => '*', 
            'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
            ]);
    }

    public function postDestroy($id){
        DB::table('company_branch')->where('company_id', $id)->update([
            'deleted_at' => date('Y-m-d H:i:s'),
            'deleted_by' => 1
        ]);

        DB::table('company_cutoff_date')->where('company_id', $id)->update([
            'deleted_at' => date('Y-m-d H:i:s'),
            'deleted_by' => 1
        ]);

        DB::table('company_government_field')->where('company_id', $id)->update([
            'deleted_at' => date('Y-m-d H:i:s'),
            'deleted_by' => 1
        ]);

        DB::table('company_policy')->where('company_id', $id)->update([
            'deleted_at' => date('Y-m-d H:i:s'),
            'deleted_by' => 1
        ]);
    }
}
