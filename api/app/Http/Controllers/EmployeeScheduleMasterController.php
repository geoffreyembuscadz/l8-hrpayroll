<?php
namespace App\Http\Controllers;
use \DB;
use \Illuminate\Http\Request;
use App\Http\Controllers\CrudController;
use App\Http\Models\CompanyModel;
use App\Http\Models\EmployeeModel;
use App\Http\Models\ScheduleAdjustmentsModel;
use App\Http\Models\ScheduleAdjustmentsRequestModel;

class EmployeeScheduleMasterController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'created_at' ];

	public $model = 'EmployeeScheduleMasterModel';

	public $rules = [];

	public $table = 'employee_schedule_master';

    public $table_columns = [ 'employee_id', 'company_id', 'schedule_id', 'schedule_date', 'timein', 'timeout', 'breakin', 'breakout', 'lunch_mins_break' ];

    public function addShowData($data, $id = null){
        $data['data']['employee_record']  = (!empty($id)) ? EmployeeModel::where('id', $data['data']['employee_id'])->first(): [];
        $data['data']['employee_company']   = (!empty($id)) ? CompanyModel::where('id', $data['data']['company_id'])->first(): [];

        return $data;
    }

    public function preList($model){
        $model = $model->preListJoin($model);

        // if(!empty($this->request['company_id'])){
        //     $model = $model->where('employee_company.company_id', $this->request['company_id']);
        // }

        return $model;
    }

    public function postList($model){
        $model = $this->model->postListJoin($model);

        return $model;
    }

	public function preStore($data = []){
        $count = 0;
        $new_data = [ 'data' => null ];
        $search_preferences = ['company_id', 'schedule_date', 'employee_id', 'breakin', 'lunch_mins_break', 'timein', 'breakout', 'timeout'];
        $dates_to_be_save = [];
        $polished_data = [];

        foreach($data['data'] AS $raw_key_index => $input_val){
            foreach($search_preferences AS $search_preference){
                if (strpos($raw_key_index, $search_preference) !== false) {
                    $polished_data[$count][$search_preference] = $input_val;
                }
            }
            $count++;
        }

        $new_data['data'] = array_chunk($polished_data, 7);

        return $new_data;
    }

    public function store(Request $request){
        $data_records = [];
        $time_columns = [ 'breakin', 'breakout', 'timein', 'timeout' ];

        $this->validationRuleStore();

        $errors = (count($this->rules)) ? $this->validate($request, $this->rules, [], $this->messages) : [];

        $column_primary_key = $this->model_col_pk;

        if( !$errors ){
            $data = [];
            $data['data'] = $request->all();
            $data = $this->preStore($data);

            // return $data;

            foreach($data['data'] AS $record_index => $record_row){
                $start_date = null;
                $end_date = null;

                // Adding EmployeeScheduleMasterModel
                $insert_data = new $this->model();
                foreach($record_row AS $record_col => $record_row_data){
                    $record_date = null;
                    foreach($record_row_data AS $column_col => $record_val){
                        foreach($this->table_columns AS $column){
                            $record_date = ( $column == 'schedule_date' ) ? $record_val : null;
                            $break_in = ( $column == 'breakin' ) ? $record_val : null;
                            if($record_col == 0) $start_date = $record_date;

                            if($column_col == $column){
                                if(in_array($column_col, $time_columns)){
                                    $insert_data->{$column} = date_format(date_create($record_date . ' ' . $record_val), 'Y-m-d H:i:s');
                                } elseif($column_col == 'schedule_date'){
                                    $record_date = date_format(date_create($record_date), 'Y-m-d');
                                    $insert_data->{$column} = date_format(date_create($record_val), 'Y-m-d');
                                } else {
                                    $insert_data->{$column} = $record_val;
                                }
                            }

                            if($column_col == 'lunch_mins_break'){
                                $insert_data->breakout = date('Y-m-d H:i:s', strtotime($insert_data->breakin .' + ' . $record_val . ' minute'));
                            }
                        }
                    }
                }
                $insert_data->created_by = 1;
                $insert_data->created_at = date('Y-m-d H:i:s');
                $insert_data->save();

                $schedule_master_id = $insert_data->id;
								$schedule_master_new_data = DB::table('schedule_adjustments_request')->where('id', $schedule_master_id)->first();

                // Addings ScheduleAdjustmentsModel
								$new_schedule_adjustment_insert = new ScheduleAdjustmentsModel();
								$new_schedule_adjustment_insert->emp_id = $insert_data->employee_id;
								$new_schedule_adjustment_insert->start_date = date_format(date_create( $schedule_master_new_data['time_in']), "Y-m-d");
								$new_schedule_adjustment_insert->end_date = date_format(date_create($schedule_master_new_data['time_out']), "Y-m-d");
								$new_schedule_adjustment_insert->status_id = 1;
								$new_schedule_adjustment_insert->date_applied = $insert_data->schedule_date;
								$new_schedule_adjustment_insert->remarks = "Filed in schedule master.";
								$new_schedule_adjustment_insert->created_at = date('Y-m-d H:i:s');
								$new_schedule_adjustment_insert->created_by = 1;
								$new_schedule_adjustment_insert->save();

								// Addings ScheduleAdjustmentsRequestModel
								$new_schedule_request_data = [];
								$new_schedule_request_data['employee_id'] = $new_schedule_adjustment_insert->emp_id;
								$new_schedule_request_data['date'] = $insert_data->schedule_date;
								$new_schedule_request_data['start_time'] = $insert_data->timein;
								$new_schedule_request_data['end_time'] = $insert_data->timeout;
								$new_schedule_request_data['break_start'] = $insert_data->breakin;
								$new_schedule_request_data['break_end'] = $insert_data->breakout;
								$new_schedule_request_data['SA_id'] = $new_schedule_adjustment_insert->id;

								$new_schedule_request_data['created_at'] = date('Y-m-d H:i:s');
								$new_schedule_request_data['created_by'] = 1;

								DB::table('schedule_adjustments_request')->insert($new_schedule_request_data);
            }

            // $data['data'] = $this->model->whereIn('id', $primary_keys)->get();
            $data['data'] = $data['data'];
            $data['message'] = 'You have successfully add the following schedules for the selected employees.';
        }

        $data['errors'] = $errors ? $errors : [];

        return $errors ? $errors : response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*',
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

        // return $errors ? $errors : $data;
  }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        $data['data']['timein'] = $data['data']['schedule_date'] . ' ' . $data['data']['timein'];
        $data['data']['timeout'] = $data['data']['schedule_date'] . ' ' . $data['data']['timeout'];
        $data['data']['breakin'] = $data['data']['schedule_date'] . ' ' . $data['data']['breakin'];
        $data['data']['breakout'] = date('Y-m-d H:i:s', strtotime($data['data']['breakin'] .' + ' . $data['data']['lunch_min_break'] . ' minute'));
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }


}
