<?php

namespace App\Http\Controllers;

use \DB;
use \Input AS Input;
use \Validator AS Validator;
use \Illuminate\Http\Request;
use App\Http\Controllers\CrudController;

class EmployeeAttachmentController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id' ];

	public $model = 'EmployeeAttachmentModel';

	public $rules = [];

	public $table = 'employee_attachment';

    public function preList($model){
    	$employee_id = ($this->request->input('employee_id')) ? $this->request->input('employee_id') : false;

    	if($employee_id){
    		$model = $this->model->whereEmployee($model, $employee_id);
    	}

        return $model;
    }

    public function postList($model){
        return $model;
    }

	public function preStore($data = []){
		$filename_pieces = explode('\\', $data['data']['file_dir']);
		$data['data']['filename'] = end($filename_pieces);

        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }
}
