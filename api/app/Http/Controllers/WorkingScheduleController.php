<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;

class WorkingScheduleController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'name',];

	public $model = 'WorkingScheduleModel';

	public $rules = [ 'name' => 'required' ];

	public $table = 'working_schedule';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);
        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);
        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }
}
