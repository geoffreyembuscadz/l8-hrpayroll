<?php
namespace App\Http\Controllers;

use DB;
use \Illuminate\Http\Request;
use App\Http\Models\EmployeeModel;
use App\Http\Controllers\CrudController;

class DynastyLoanController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'name', 'created_at' ];

	public $model = 'DynastyLoanModel';

	public $rules = [];

	public $table = 'employee_dynasty_loan';

    public function addShowData($data, $id = null){
        $data['data']['employee_fullname'] = EmployeeModel::where('id', $data['data']['employee_id'])->select(DB::raw('UCASE(CONCAT(firstname, " ", middlename, " ", lastname)) AS employee_fullname'))->first()->employee_fullname;
        return $data;
    }

    public function preList($model){
        $model = $this->model->preListJoin($model);
        return $model;
    }

    public function postList($model){
        return $model;
    }

	public function preStore($data = []){
        return $data;         
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }
}
