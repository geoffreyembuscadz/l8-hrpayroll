<?php
namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;

class CompanyWorkingScheduleController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'name', 'created_at' ];

	public $model = 'CompanyScheduleModel';

	public $rules = [ 'name' => 'required' ];

	public $table = 'company_schedule';

    public function preList($model){
        if(!empty($this->request['company_id'])){
            $model = $model->where('working_schedule.company_id', $this->request['company_id']);
        }
        return $model;
    }

    public function postList($model){
        $model = $this->model->formatTimeOnSelect($model);
        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }
}
