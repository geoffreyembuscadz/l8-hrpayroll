<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use Illuminate\Database\Eloquent\Model;

class ModuleMainController extends CrudController{
	public $auth = false;

	public $list_columns = [ ];

	// public $model = 'ModuleMainModel';

    public $model = 'ModuleMainModel';

	public $rules = [ ];

	public $table = 'modules';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function getList() {
      $menu_obj = new \App\Http\Models\ModuleMainModel();
      $model    = $menu_obj->with('menu')->with('menu.permission')->get();
      return $model;
        
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }

    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }


}
