<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;

class PayrollBillingByLocationController extends CrudController{
	public $auth = false;

	public $list_columns = [ ];

	public $model = 'PayrollBillingByLocationModel';

	public $rules = [  ];

	public $table = 'payroll_list';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        
        $start      = $this->request['start'];
        $end        = $this->request['end'];
        $run_type   = $this->request['run_type'];
        $company    = $this->request['company'];
        $branch     = $this->request['branch'];
        $department = $this->request['department'];
        $position   = $this->request['position'];
        $cutoff     = $this->request['cutoff'];
        $removed_employee  = $this->request['removed_employee'];
        $model      = $this->model->preList($model, $start, $end, $run_type, $company, 
                            $branch, $department, $position, $cutoff, $removed_employee);

        return $model;
    }

    public function postList($model){

        $start             = $this->request['start'];
        $end               = $this->request['end'];
        $run_type          = $this->request['run_type'];
        $company           = $this->request['company'];
        $branch            = $this->request['branch'];
        $department        = $this->request['department'];
        $position           = $this->request['position'];
        $cutoff            = $this->request['cutoff'];
        $removed_employee  = $this->request['removed_employee'];
        $branch_location   = $this->request['branch_location'];
        $model             = $this->model->postList($model, $start, $end, $run_type, $company, 
                                $branch, $department, $position, $cutoff, $removed_employee, $branch_location);
        return $model;
            
    }

	public function preStore($data = []){
        return $data;
    }
	public function postStore($id, $data = []){
        return $data;
    }
    public function preUpdate($id, $data = []){
        return $data;
    }
    public function postUpdate($id, $data = []){
        return $data;
    }


}
