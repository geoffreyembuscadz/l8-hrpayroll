<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use \Illuminate\Http\Request;

class OvertimeController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'created_at' ];

	public $model = 'OvertimeModel';

	public $rules = [ ];

	public $table = 'overtime';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        $status = $data['data']['status_id'];

        if($status == null){
            // $data = $this->model->preUpdate($id,$data);
        }
        return $data;
    }

    public function updateData(Request $request){

        $data=($request->all());

            $duplication=$this->model->preUpdateDuplication($data);
            if($duplication != []){
                $response['data'] = array(
                            'message'      => 'no changes',
                            'status_code'  => 200
                        );
                return $response;
            }
            else{
                $data = $this->model->updateData($data);
            }
            
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function getList (){

        $model =  $this->model->getList();
        return $model;
    }

    public function show ($id){
        $data = [];
        $data['data'] = $this->model->show($id);
        $data = $this->addShowData($data, $id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function createOvertime(Request $request){
        $data=[];
        $array=($request->all());

        $len = count($array["employee_id"]);

        for ($i=0; $i < $len; $i++) {

            array_push(
                $data,
                array(
                    'emp_id'       =>  $array["employee_id"][$i], 
                    'start_time'   =>  $array["start_time"], 
                    'end_time'     =>  $array["end_time"], 
                    'remarks'      =>  $array["remarks"], 
                    'total_hours'  =>  abs($array["total_hours"]),
                    'date'         =>  $array["date"]
                    )
                );
        }

        $return=$this->model->createOvertime($data);

        $response=[
          'data' => $return
        ];
        return $response;
    }

    public function getMyOT(Request $request){

        $data = $request->all();
        $data['status_id'] = explode(",",$data['status_id']);
        
        $data =  $this->model->getMyOT($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    public function getOvertime(Request $request){
        $data = $request->all();

        $data['employee_id'] = explode(",",$data['employee_id']); 
        $data['company_id'] = explode(",",$data['company_id']); 
        $data['status_id'] = explode(",",$data['status_id']);

        $data = $this->model->getOvertime($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function duplicateEntryValidation(Request $request){
        $data=($request->all());

        $duplication=$this->model->duplicateEntryValidation($data);
        if($duplication != []){

            $response['data'] = array(
                        'message'      => 'duplicate',
                        'status_code'  => 400,
                        'data'  => $duplication
                    );
            return $response;
        }
        else{
            $response['data'] = array(
                        'message'      => 'not duplicate'
                    );
            return $response;
        }
    }

    public function updateOT($id,$data){

        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function updateApprovedRequest($id){
        $data = $this->model->approvedData($id);

        return $data;
    }

}
