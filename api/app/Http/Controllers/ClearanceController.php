<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use \Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon as Carbon;

class ClearanceController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'name', 'created_at' ];

	public $model = 'ClearanceModel';

	public $rules = [];

	public $table = 'clearance';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function getClearanceList(Request $request){
        $data = $request->all();

        // $data['status_id'] = explode(",",$data['status_id']); 
        // $data['type_id'] = explode(",",$data['type_id']); 

        $data =  $this->model->getClearanceList($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

     public function updateStatus(Request $request){

        $data=($request->all());

        $return =  $this->model->updateStatus($data);

        $response=[
          'data' => $return
        ];
        return $response;
    }


    public function createClearance(Request $request){

        $temp=($request->all());
        $data = [];

        $len = count($temp["employee_id"]);


        for ($i=0; $i < $len; $i++) { 

            array_push(
                $data,array(
                    'emp_id'        =>$temp["employee_id"][$i], 
                    'clearance'     =>$temp["clearance"]
                )
            );
        }

        $data =  $this->model->createClearance($data);

        return $data;
    }

    public function duplicateEntryValidation(Request $request){
        $data=($request->all());

        $duplication=$this->model->duplicateEntryValidation($data);

        if($duplication != []){

            $response['data'] = array(
                        'message'      => 'duplicate',
                        'status_code'  => 400,
                        'data'  => $duplication
                    );
            return $response;
        }
        else{
            $response['data'] = array(
                        'message'      => 'not duplicate'
                    );
            return $response;
        }

    }

    public function getMyClearanceList(){

        $data =  $this->model->getMyClearanceList();
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function updateMyStatus(Request $request){

        $data=($request->all());

        $return =  $this->model->updateMyStatus($data);

        $response=[
          'data' => $return
        ];
        return $response;
    }

    public function printClearance(Request $request){
        $data=($request->all());
        $id = $data['employee_id'];

        $clearance = $this->model->getClear($id);
        $empInfo = $this->model->employeeInfo($id);

        $data = array(
          'date'    => Carbon::now()
        );

        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif'])->setPaper('a4');

        $pdf = PDF::loadView('export.clearance', 
          [ 

            'clearance'   => $clearance,
            'empInfo'     => $empInfo

          ],  array( 'data' => $data )
        );

        return $pdf->stream('clearance.pdf');

    }

    public function show2 ($id){
        $data = [];
        $data['data'] = $this->model->show2($id);
        $data = $this->addShowData($data, $id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function updateClearance(Request $request){

        $data=($request->all());

        $data = $this->model->updateClearance($data);
            
        return $data;
    }


}
