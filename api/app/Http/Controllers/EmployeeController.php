<?php
namespace App\Http\Controllers;

use \DB;
use \Input as Input;
use \Validator AS Validator;
use \Illuminate\Http\Request;
use App\Http\Models\CompanyModel;
use App\Http\Models\CompanyBranchModel;
use App\Http\Controllers\CrudController;
use Illuminate\Support\Facades\File;

class EmployeeController extends CrudController{
    public $auth = false;

    public $list_columns = [ 'id', 'firstname', 'lastname' ];

    public $messages = [
        'sss.required' => 'SSS Number is required.'
    ];

    public $model = 'EmployeeModel';

    public $rules = [
        //'employee_code' => 'filled',
        'firstname'     => 'filled',
        'middlename'    => 'filled',
        'lastname'      => 'filled'
        //,'sss'           => 'required'
    ];

    public $table = 'employee';

    public function addShowData($data, $id = null){
        $data['data']['attachments']        = (isset($id)) ? $this->model->getEmployeeAttachments($id)->toArray() : [];
        $data['data']['primary_company']    = (isset($id)) ? $this->model->getEmployeeCompany($id) : null;
        $data['data']['company_branch'] = (isset($id)) ? $this->model->getEmployeeCompanyBranch($id) : null;
        $data['data']['emergency_contacts'] = (isset($id)) ? $this->model->getEmergencyContacts($id) : [];
        $data['data']['working_schedule']   = (isset($id)) ? $this->model->getEmployeeWorkingSchedule($id) : [];
        $data['data']['date_terminated']    = (isset($id)) ? date_format(date_create($data['data']['date_terminated']), 'm/d/Y') : '';
        $data['data']['employee_dependents'] = (isset($id)) ? $this->model->getEmployeeDependencies($id) : [];
        $data['data']['remarks'] = (isset($id)) ? $this->model->getEmployeeRemarks($id) : [];

        $data['data']['income_rate'] = (isset($id)) ? $this->model->getEmployeeRate($id) : 0;
        $data['data']['allowance_amount'] = (isset($id)) ? $this->model->getEmployeeAllowance($id) : 0;

        $data['data']['onboarding'] = (isset($id)) ? $this->model->getOnboarding($id) : 0;
        $data['data']['user_time_setting'] = (isset($id)) ? $this->model->getUserTimeSetting($id) : 0;

        return $data;
    }

    public function preList($model){
        $model = $model->preListJoin($model);

        if(!empty($this->request['company_id'])){
            $model = $model->where('employee_company.company_id', $this->request['company_id']);
        }

        return $model;
    }

    public function postList($model){
        $model = $this->model->postListJoin($model);

        return $model;
    }

    public function preStore($data = []){
        $supervisor_ids = null;

        if(!empty(trim($data['data']['middlename']))){
            $data['data']['middlename'] = '-N/A-';
        }

        if(isset($data['data']['supervisor_id'])){
            $supervisor_ids = [ $data['data']['supervisor_id'] ];

            if(is_array($data['data']['supervisor_id'])){
                $employee_users = DB::table('users')->join('employee_user', 'employee_user.user_id', '=', 'users.id')->pluck('users.id');
                $users_id = DB::table('users')->whereNotIn('id', $employee_users)->whereRaw('deleted_at IS NULL')->pluck('id')->toArray();
                
                $supervisor_ids = array_merge($users_id, $data['data']['supervisor_id']);
            }
        }

        $data['data']['supervisor_id'] = json_encode($supervisor_ids);
        $data['data']['salary_receiving_type'] = strtolower($data['data']['salary_receiving_type']);
        
        if(is_array($data['data']['supervisor_id'])){
            $data['data']['supervisor_id'] = json_encode($data['data']['supervisor_id']);
        }

        $employee_dependencies_rec = [];

        if(count($data['data']['employee_dependents'])){
            foreach($data['data']['employee_dependents'] AS $dependent_index => $employee_dependent){
                $key_index = str_replace(']', '', $dependent_index);
                $index_pieces = explode('[', $dependent_index);

                $employee_dependencies_rec[(int)$index_pieces[1]][$index_pieces[0]] = $employee_dependent;
            }
            $data['data']['employee_dependents'] = $employee_dependencies_rec;
        }

        $emergency_contacts_rec = [];

        if(isset($data['data']['emergency_contacts'])){
            if(is_array($data['data']['emergency_contacts'])){
                foreach($data['data']['emergency_contacts'] AS $key_index => $emergency_contact_val){
                    $key_index = str_replace(']', '', $key_index);
                    $index_pieces = explode('[', $key_index);

                    $emergency_contacts_rec[$index_pieces[1]][$index_pieces[0]] = $emergency_contact_val;
                }
            }

            $data['data']['emergency_contacts'] = $emergency_contacts_rec;
        }

        $data['data']['skills'] = isset($data['data']['skills']) ? implode(',', $data['data']['skills']) : [];
        $data['data']['maternity_leave_credit'] = (!empty($data['data']['maternity_leave_credit'])) ? $data['data']['maternity_leave_credit'] : 0;
        $data['data']['others_leave_credit'] = (!empty($data['data']['others_leave_credit'])) ? $data['data']['others_leave_credit'] : 0;
        $data['data']['emergency_leave_credit'] = (!empty($data['data']['emergency_leave_credit'])) ? $data['data']['emergency_leave_credit'] : 0;
        $data['data']['sick_leave_credit'] = (!empty($data['data']['sick_leave_credit'])) ? $data['data']['sick_leave_credit'] : 0;
        $data['data']['vacation_leave_credit'] = (!empty($data['data']['vacation_leave_credit'])) ? $data['data']['vacation_leave_credit'] : 0;
        $data['data']['branch'] = (!empty($data['data']['company_branch'])) ? $data['data']['company_branch'] : null;
        $data['data']['position_type'] = 'associate';

        return $data;
    }

    public function postStore($id, $data = []){
        // Insertion of Employee Companies
        $employee_companies = [];

        $employee_primary_company = [
            'company_id' => $data['data']['primary_company'],
            'branch_id' => (isset($data['data']['company_branch'])) ? $data['data']['company_branch'] : null,
            'company_level' => 'primary',
            'employee_id' => $id,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ];

        $employee_bank_account = [
            'employee_id' => $id,
            'bank_name' => $data['data']['bank_name'],
            'bank_code' => $data['data']['bank_code'],
            'employee_code' => $data['data']['employee_code'],
            'bank_account_number' => $data['data']['bank_account_number'],
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => 1
        ];

        array_push($employee_companies, $employee_primary_company);

        $this->model->setEmployeeCompanies($id, $employee_companies);

        $this->model->setEmployeeBankAccount($id, $employee_bank_account);

        $this->model->setEmployeeDependencies($id, $data['data']['employee_dependents']);

        $this->model->setEmployeeEmergencyContacts($id, $data['data']['emergency_contacts']);

        $this->model->setEmployeePosition($id);

        $this->model->appendEmployeeWorkingSchedule($id, $data['data']['working_schedule']);

        $this->model->setEmployeeRate($id, $data['data']);

        $this->model->appendEmployeeLog($id, $data['data']);

        if($data['data']['ip_address'] != null){
            $this->model->createUserTimeSetting($id, $data);
        }

        if($data['data']['onboarding'] != null){
            $this->model->createOnboarding($id, $data);
        }


        if($data['data']['user_credentials'] != 0){
            $this->model->addUser($id, $data['data']);
        }

        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        $supervisor_ids = null;
        if(isset($data['data']['supervisor_id'])){
            $supervisor_ids = [ $data['data']['supervisor_id'] ];

            if(is_array($data['data']['supervisor_id'])){
                $employee_users = DB::table('users')->join('employee_user', 'employee_user.user_id', '=', 'users.id')->pluck('users.id');
                $users_id = DB::table('users')->whereNotIn('id', $employee_users)->whereRaw('deleted_at IS NULL')->pluck('id')->toArray();
                
                $supervisor_ids = array_merge($users_id, $data['data']['supervisor_id']);
            }
        }

        $data['data']['supervisor_id'] = json_encode($supervisor_ids); // value: [1,2,3] - collection of user_ids who can view this certain employee.
        $data['data']['date_terminated'] = (isset($data['data']['date_terminated'])) ? date('Y-m-d') : null;
        $data['data']['skills'] = (isset($data['data']['skills'])) ? implode(',', $data['data']['skills']) : [];

        $this->model->appendEmployeeLog($id, $data['data']);

        return $data;
    }

    public function postUpdate($id, $data = []){
        $delete_attachment = (isset($data['data']['delete_attachment'])) ? $data['data']['delete_attachment'] : false;

        if(isset($data['data']['working_schedule'])){
            $this->model->updateEmployeeWorkingSchedule($id, $data['data']['working_schedule']);
        }

        $this->model->updateEmployeeRate($id, $data);

        $this->model->updateEmployeeCompany($id, $data);

        $this->model->updateEmployeeBankAccount($id, $data);

        $this->model->updateUserTimeSetting($id, $data);

        $this->model->updateOnboarding($id, $data);

        if($delete_attachment){
            $this->model->archiveAttachment($delete_attachment);
        }

        return $data;
    }

    /* Customized functions */
    public function tagEmployeeAttachments(Request $request){
        $employee_id = $request->get('employee_id');
        $attachment = $request->get('url_attachment');
        $dir        = $request->get('dir_attachment');
        
        $attachment_pieces = explode('/', $attachment);
        $filename = end($attachment_pieces);

        $data = [
            'employee_id'   => $employee_id,
            'file_url'      => $attachment,
            'file_dir'      => $dir,
            'filename'      => $filename,
            'created_at'    => DB::raw('NOW()'),
            'created_by'    => 1
        ];

        $this->model->tagEmployeeAttachments($data);
    }

    public function postUpload(Request $request){
        mb_regex_encoding('UTF-16');
        $csv_datas = [];
        $render_data = [];
        $form_filename = $request->get('file_dir');

        if(!file_exists($form_filename)){
            $render_data = 'File does not exists.';
        } else {
            $index = 0;
            $csv_file = file($form_filename);
            $csv_datas = array_map('str_getcsv', $csv_file);

            $headers = $csv_datas[0];
            // return $csv_datas;
            if(is_array($csv_datas)){
                foreach($csv_datas AS $index_data => $data){
                    if($index_data == 0) continue;

                    foreach($headers AS $h_i => $header_index){
                        if(isset($data[$h_i])){
                            $render_data[$index][$header_index] = $data[$h_i];
                        } else {
                            $render_data[$index][$header_index] = null;
                        }
                    }
                    $index++;
                }

                $response = [];
                $response = $this->model->insertDirect($render_data);
                return $response;
            }
        }
    }

    public function updateEmployeeStatus(Request $request){
        $data=($request->all());

        $return =$this->model->updateEmployeeStatus($data);

        $response=[
          'data' => $return
        ];
        return $response;
    }

    public function downloadMassUploadForm(Request $request){
        $header = "";
        $contents = [];
        $row_content = [];
        $company_name = '*required';
        $company_branch_name = '*required';
        $required_headers = "employee_code,firstname,middlename,lastname,company,branch,salary_receiving_type,rate,e_cola,allowance_amount,starting_date,sss,philhealth_no,pagibig_no,tin_no,birthdate,gender,nationality,current_address,cel_no,status";
        $required_headers_in_array = explode(",", $required_headers);

        $company_id = (!empty(trim($request->get('company_id')))) ? trim($request->get('company_id')) : null;
        $company_branch_id =(!empty(trim($request->get('branch_id')))) ? trim($request->get('branch_id')) : null;
        $upload_form_fields = (!empty(trim($request->get('fields')))) ? trim($request->get('fields')) : null;

        $upload_form_fields = (!empty($upload_form_fields)) ? explode(",", $upload_form_fields) : [];
        
        if(!empty($company_id)){
            $company_name = CompanyModel::where('id', $company_id)->first()->name;
        }

        if(!empty($company_branch_id)){
            $company_branch_name = CompanyBranchModel::where('id', $company_branch_id)->first()->branch_name;
        }

        if(count($upload_form_fields)){
            foreach($upload_form_fields AS $counter_field => $upload_form_field){
                $header .= ($counter_field == 0) ? $upload_form_field : "," . $upload_form_field ;
            }
        }
        
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=employee_mass_upload' . date('Y-m-d') . '.csv');

        foreach($upload_form_fields AS $counter_field_for_content => $upload_form_field_for_content){
            if(in_array($upload_form_field_for_content, $required_headers_in_array)){
                if($upload_form_field_for_content == 'company'){
                    $row_content[$counter_field_for_content] = $company_name;
                } else if($upload_form_field_for_content == 'branch'){
                    $row_content[$counter_field_for_content] = $company_branch_name;
                } else if($upload_form_field_for_content == 'salary_receiving_type'){
                    $row_content[$counter_field_for_content] = '*required[daily/weekly/monthly]';
                } else if($upload_form_field_for_content == 'starting_date'){
                    $row_content[$counter_field_for_content] = '*required (' . date('m/d/Y') . ')';
                } else {
                    $row_content[$counter_field_for_content] = "*required";
                }
            } else {
                $row_content[$counter_field_for_content] = "N/A";
            }
        }

        $contents = [
            $row_content,
            $row_content,
            $row_content
        ];

        $file = fopen("php://output", "w");
        fputcsv($file,explode(',', $header));
        foreach($contents as $content){
            $array_content = $content;
            fputcsv($file, $array_content);
        }

        fclose($file);
        /*
        OLD Source code to get the external file
        $my_file = 'files/upload_forms/employee_mass_upload_form.csv';
        $return  = [ 'data' => [ 'success' => 1, 'message' => 'Employee List is successfully exported.', 'file_name' => $my_file ]];
        return json_encode( $return );
        */
    }

    public function exportList(Request $request){
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=employee_mass_upload' . date('Y-m-d') . '.csv');

        $data_export = date('Y-m-d_His');
        $branch = $request->get('branch');
        $company = $request->get('primary_company');
        $requests = [ 'company' => $company, 'company_branch' => $branch ];

        $datas = $this->model->selectExportBasicEmployeeListQuery($requests);

        $header = "employee_code,firstname,middlename,lastname,company,branch,gender,birthdate,nationality,sss,philhealth_no,pagibig_no,tin_no,e_cola,salary_receiving_type,rate,allowance_amount,status,current_address";

        $file = fopen("php://output", "w");

        fputcsv($file,explode(',', $header));
        
        foreach($datas as $data){
            $array_content = (array)$data;
            fputcsv($file, $array_content);
        }

        fclose($file);

        // $data_export = date('Y-m-d_His');
        // $branch = $request->get('branch');
        // $company = $request->get('primary_company');
        // $requests = [ 'company' => $company, 'company_branch' => $branch ];

        // $datas = $this->model->selectExportBasicEmployeeListQuery($requests);

        // $my_file = 'files/employee_export/employee_export-list.csv';
        // $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
        // $file_content = "";

        // if(count($datas)){
        //     foreach($datas[0] AS $header => $value){
        //         $file_content .= $header . ",";
        //     }

        //     $file_content .= "\n";

        //     foreach($datas AS $data_row){
        //         $file_content .= "";
        //         foreach($data_row AS $column => $data_value){
        //             if(in_array($column, ['supervisor_id'])) $file_content .= "";

        //             $file_content .= str_replace(",", "", $data_value);
        //             $file_content .= ",";
        //         }
        //         $file_content .= "\n";
        //     }
        // }

        // fwrite($handle, $file_content);

        // $return  = [ 'data' => [ 'success' => 1, 'message' => 'Employee List is successfully exported.', 'file_name' => $my_file ]];

        // return json_encode( $return );
    }

    public function preDestroy($id){
        $this->model->where('id', $id)->update([
            'status' => 0
        ]);
    }
}