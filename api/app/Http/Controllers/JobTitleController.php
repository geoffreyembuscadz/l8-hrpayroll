<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;

class JobTitleController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'name', 'description', 'department_id' ];

	public $model = 'JobTitleModel';

	public $rules = [ 'name' => 'required', 'department_id' => 'required', 'description' => 'required' ];

	public $table = 'jobtitle';

    public function preList($model){
        $model = $this->model->joins($model);

        return $model;
    }

    public function postList($model){
        $model = $this->model->moderateSelection($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }
}
