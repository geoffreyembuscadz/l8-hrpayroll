<?php
namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use Laravel\Lumen\Application;
use Illuminate\Http\JsonResponse;
use App\Http\Models\EmployeeModel;

class UploadController extends Controller {

	public function postUpload(Request $request){
		$return = [
			'file_dir' => '',
			'file_url' => ''
		];
		$file = $request->file('file');
		$type = $request->get('type');
		$filename = $file->getClientOriginalName();

		if($type == 'employee_attachment'){
			$return['filename'] = $filename;
			$return['file_dir'] = getcwd() . '/files/employee_attachments/' . $filename;
			$return['file_url'] = url('files/employee_attachments/' . $filename);

			$file->move(getcwd() . '/files/employee_attachments', $filename);
		} else if($type == 'attendance'){
			$return['filename'] = $filename;
			$return['file_dir'] = getcwd() . '/files/attendance/' . $filename;
			$return['file_url'] = url('files/attendance/' . $filename);

			$file->move(getcwd() . '/files/attendance', $filename);
		} else if($type == 'employee_img'){
			$return['filename'] = $filename;
			$return['file_dir'] = getcwd() . '/img/users/' . $filename;
			$return['file_url'] = url('img/users/' . $filename);

			$file->move(getcwd() . '/img/users', $filename);
		} else if($type == 'employee_mass_upload'){
			$return['filename'] = $filename;
			$return['file_dir'] = getcwd() . '/files/employee_attachments/records_uploaded/' . $filename;
			$return['file_url'] = url('files/employee_attachments/records_uploaded/' . $filename);

			$file->move(getcwd() . '/files/employee_attachments/records_uploaded', $filename);
		} else {
			$return['filename'] = $filename;
			$return['file_dir'] = getcwd() . '/files/' . $filename;
			$return['file_url'] = url('files/' . $filename);

			$file->move(getcwd() . '/files', $filename);
		}

		return response()->json($return)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
	}
}
