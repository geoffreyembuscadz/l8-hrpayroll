<?php

namespace App\Http\Controllers;
use \Illuminate\Http\Request;
use App\Http\Controllers\CrudController;

use \App\Http\Models\PayrollListDataModel as Payroll;
use \App\Http\Models\PayrollAdjustmentModel as PayrollAdjustment;
use \App\Http\Models\PayrollLoanModel as PayrollLoan;
use DB;


class PayrollListController extends CrudController{
	public $auth = false;

	public $list_columns = [];

	public $model = 'PayrollListModel';

	public $rules = [];

	public $table = 'payroll_list';


    public $payroll;


    public function show($id) {
        $data = [];

        $data['data'] = $this->model->getPayrollById($id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){

        $start                = $this->request['start'];
        $end                  = $this->request['end'];
        $run_type             = $this->request['run_type'];
        $company              = $this->request['company'];
        $branch               = $this->request['branch'];
        $department           = $this->request['department'];
        $position             = $this->request['position'];
        $cutoff               = $this->request['cutoff'];
        $cutoff               = $this->request['cutoff'];
        $removed_employee     = $this->request['removed_employee'];
        $model                = $this->model->preList($model, $start, $end, $run_type, $company, $branch, $department, $position, $cutoff, $removed_employee);

        return $model;
    }

    public function postList($model){

        $start             = $this->request['start'];
        $end               = $this->request['end'];
        $run_type          = $this->request['run_type'];
        $company           = $this->request['company'];
        $branch            = $this->request['branch'];
        $department        = $this->request['department'];
        $position          = $this->request['position'];
        $cutoff            = $this->request['cutoff'];
        $removed_employee  = $this->request['removed_employee'];
        $model             = $this->model->postList($model, $start, $end, $run_type, $company, $branch, $department, $position, $cutoff, $removed_employee);
        return $model;
            
    }


    public function store(Request $request){
        $this->validationRuleStore();
        $errors = (count($this->rules)) ? $this->validate($request, $this->rules, [], $this->messages) : [];

        $column_primary_key = $this->model_col_pk;

            if( !$errors ){
                $data = [];
                $data['data'] = $request->all();
                $start              = $data['data']['start_time']; 
                $end                = $data['data']['end_time']; 
                $run_type           = $data['data']['type_of_run']; 
                $company            = $data['data']['company']; 
                $branch             = $data['data']['branch']; 
                $department         = $data['data']['department']; 
                $position           = $data['data']['position'];
                $cutoff             = $data['data']['cutoff'];
                $removed_employee   = $data['data']['removed_employee'];
                $model        = $this->model;
                $model        = $this->model->preList($model, $start, $end, $run_type, $company, $branch, $department, $position, $cutoff, $removed_employee);
                $model        = $this->model->postList($model, $start, $end, $run_type, $company, $branch, $department, $position, $cutoff, $removed_employee)->get()->toArray();
                $model        = $this->model->storeList($model, $start, $end, $data);       

            }

            $data['message'] = $model;
            $data['errors'] = $errors ? $errors : [];

            return $errors ? $errors : response()->json($data)->withHeaders([
              'Access-Control-Allow-Origin' => '*', 
              'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
              'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
            ]);

      }

    public function destroy( $id ){
        $data = [];
        if($this->auth){
          Payroll::where('deleted_by', $id);
          PayrollAdjustment::where('deleted_by', $id);
          PayrollLoan::where('deleted_by', $id);
        }

        // $this->preDestroy($id);

        Payroll::where('payroll_id', $id )->update([ 'deleted_at' => date('Y-m-d h:i:s'), 'deleted_by' => 1 ]);
        PayrollAdjustment::where('payroll_id', $id )->update([ 'deleted_at' => date('Y-m-d h:i:s'), 'deleted_by' => 1 ]);
        PayrollLoan::where('payroll_id', $id )->update([ 'deleted_at' => date('Y-m-d h:i:s'), 'deleted_by' => 1 ]);

        // $this->postDestroy($id);

        $data = Payroll::where('payroll_id', $id)->first();

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
        return $data;
    }

    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function updateAllowance($id, Request $request) {

        $data = [];
        $data['data'] = $request->all();

        $model = $this->model->updateAllowance($id, $data['data']['amount']);
        
        return $model;
    }

    public function getEmployeeForPayroll(Request $request) {

        $data = [];
        $data['data']   = $request->all();
        $start          = $data['data']['start'];
        $end            = $data['data']['end'];
        $run_type       = $data['data']['run_type'];
        $company        = $data['data']['company'];
        $branch         = $data['data']['branch'];
        $department     = $data['data']['department'];
        $position       = $data['data']['position'];
        $cutoff         = $data['data']['cutoff'];
        $removed_employee     = $data['data']['removed_employee'];
        
        $data['data'] = $this->model->getEmployeeForPayroll($start, $end, $run_type, $company, $branch, $department, $position, $cutoff, $removed_employee);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

}
