<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;

class Event_DetailsController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id','created_at' ];

	public $model = 'Event_DetailsModel';

	public $rules = [ ];

	public $table = 'event_details';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

     public function getList (){
        $model=$this->model->select( 
            'event_details.id',
            'event_details.event_type', 
            'event_details.location',
            'event_details.notification');
        return $model->get();

    }


}
