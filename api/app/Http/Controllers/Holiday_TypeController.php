<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use App\Http\Models\Holiday_TypeModel;
use \Illuminate\Http\Request;

class Holiday_TypeController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'name', 'created_at' ];

	public $model = 'Holiday_TypeModel';

	public $rules = [ 'name' => 'required' ];

	public $table = 'holiday_type';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function getList (){
            $model=$this->model->select('holiday_type.id','holiday_type.name', 'holiday_type.description');
            return $model->get();

    }

    public function getHolidayType(){
       $data = Holiday_TypeModel::getHolidayType();
       $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }


    


}
