<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use \Illuminate\Http\Request;
use Carbon\Carbon;
use Excel;
use ZK\ZKLib;
use App\ZKAttendance;
use Response;

class AttendanceController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'created_at' ];

	public $model = 'AttendanceModel';

	public $rules = [];

	public $table = 'employees_logs';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){

        return $model;
    }

    public function postList($model){

        return $model;
    }

	public function preStore($data = []){

        return $data;
    }

	public function postStore($id, $data = []){

        return $data;
    }

    /**
     * update data and computation when an admin edit an attendance
     * 
     * @param  string $id 
     * @param  array
     * @return object
     */
    public function preUpdate($id, $data = []) {

        $array = [];

        $data["data"]["emp_id"] = $data["data"]["employee_id"];
        $data["data"]["date"] = substr($data["data"]["time_in"], 0,-9);
        $data['data']['start_time'] = substr($data["data"]["time_in"], 11);
        $data['data']['start_lunch'] = $data["data"]["lunch_in"] !== null ? substr($data["data"]["lunch_in"], 11) : null;
        $data['data']['end_lunch'] = $data["data"]["lunch_out"] !== null ? substr($data["data"]["lunch_out"], 11) : null;
        $data['data']['end_time'] = substr($data["data"]["time_out"], 11);
        $data["data"]["schedule"] = $data["data"]["schedule"];

        $final_data['data']=$this->computationOfAttendance($data["data"]);
 
        $data = $this->model->preUpdate($id,$final_data);

        return $data;
    }

    public function postUpdate($id, $data = []){
       
        return $data;
    }

    /**
     * show especific attendance according to id, it will be use in attendance-list-default
     * 
     * @param  string $id
     * @return object
     */
    public function show ($id){
        $data = [];
        $data['data'] = $this->model->show($id);
        $data = $this->addShowData($data, $id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }
    
    /**
     * use in search employee in attendance-create and download-file-modal
     * 
     * @param  object Request , (department,company,branch_id,position_id),list ito nga mga hinahanap
     * @return object
     */
    public function searchEmployee(Request $request){

        $data=($request->all());

        $data = $this->model->searchEmployee($data);
        $data = [ 'data' => $data ];
        
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    /**
     * imported file from attendance-import-review and save
     * 
     * @param  obj Request data from imported file
     * @return object
     */
    public function importFile(Request $request) {

        $data=($request->all());
        $new_data = [];

        foreach ($data as $key => $da) {
            foreach ($da['details'] as $key => $d) {
                $new_data[]=$d;
            }
        }

        $final_data = [];

        $len = count($new_data);
        for ($i=0; $i < $len; $i++) {
            $final_data[]=$this->computationOfAttendance($new_data[$i]);
        }

        $return =     $this->model->storeCSVFile($final_data);

        $response = [
          'data' => $return
        ];
        return $response;
    }

    /**
     * nightshift computation, may sample po sa baba na mga nakakacomment kung gusto nyo po itry itest yung code
     * 
     * @param  $time_in datetime
     * @param  $time_out datetime
     * @return int
     */
    public function computeNightShift($time_in, $time_out){

        $nightDifferenciaCount = 0;
        // $time_in = "2017-07-11 12:00:00";
        // $time_out = "2017-07-11 18:00:00";

        //fix datetime format
        $time_in = date( 'Y-m-d H:i:s', strtotime($time_in));
        $time_out = date( 'Y-m-d H:i:s', strtotime($time_out));

        $convert1 = substr($time_in, 11);
        $start_time = strtotime($convert1);

        $convert2 = substr($time_out, 11);
        $end_time = strtotime($convert2);

        $night_start = strtotime('22:00:00'); // 10pm
        $night_end = strtotime('06:00:00'); // 6am
        $night_end2 = strtotime('23:59:00'); // 11:59pm
        $midnight = strtotime('00:00:00'); // 12am

        $date_in = substr($time_in, 0,-9);
        $date_out = substr($time_out, 0,-9);

        $night_diff_start = $date_in . ' ' . '22:00:00';
        $night_diff_end = $date_out . ' ' . '06:00:00';

        //6am <= 9am && 10pm >=6pm
        //6am <= 7am && 10pm >= 6pm //mga hindi pasok sa night diff
        if ($night_end <= $start_time && $night_start >= $end_time && $date_in == $date_out) {
            return $nightDifferenciaCount;
        }
    
        //same date
        if ($date_in == $date_out) {
            //1am < 6am
            //time_in = 2017-07-11 01:00:00 time_out = 2017-07-11 06:00:00
            if($start_time < $night_end){
                $date1 = Carbon::parse($time_in);
                $date2 = Carbon::parse($night_diff_end);
                $nightDifferenciaCount = $date1->diffInHours($date2);
            }
            //6pm-10pm
            //10pm >= 6pm && 11:59pm >= 10pm
            else if ($night_start >= $start_time && $night_end2 >= $end_time) {
                $date1 = Carbon::parse($night_diff_start);
                $date2 = Carbon::parse($time_out);
                $nightDifferenciaCount = $date1->diffInHours($date2);
            }
        }
        else{
            //start_time 22:00:00 <= night_start 22:00:00 //10pm pababa
            if ($start_time <= $night_start) {
                //time_in = 2017-07-11 22:00:00 time_out = 2017-07-12 06:00:00 //6am pababa
                if ($time_out >= $night_diff_start && $end_time <= $night_end) {
                    $date1 = Carbon::parse($night_diff_start);
                    $date2 = Carbon::parse($time_out);
                    $nightDifferenciaCount = $date1->diffInHours($date2);
                }
                //time_in = 2017-07-11 22:00:00 time_out = 2017-07-12 09:00:00 //6am pataas
                else if ($time_out >= $night_diff_start && $end_time >= $night_end) {
                    $date1 = Carbon::parse($night_diff_start);
                    $date2 = Carbon::parse($night_diff_end);
                    $nightDifferenciaCount = $date1->diffInHours($date2);
                }
            } 
            //start_time 22:00:00 >= night_start 22:00:00 10 pataas
            else if ($start_time >= $night_start) {
                //time_in =2017-07-11 24:00:00 time_out = 2017-07-12 03:00:00 //6am pababa
                if ($end_time <= $night_end) {
                    $date1 = Carbon::parse($time_out);
                    $date2 = Carbon::parse($time_in);
                    $nightDifferenciaCount = $date1->diffInHours($date2);
                }
                //time_in =2017-07-11 24:00:00 time_out = 2017-07-12 07:00:00 //6am pataas
                else if ($end_time >= $night_end){
                    $date1 = Carbon::parse($night_diff_end);
                    $date2 = Carbon::parse($time_in);
                    $nightDifferenciaCount = $date1->diffInHours($date2);
                }
            }
        }

      return $nightDifferenciaCount;
    }

    /**
     * gamit ito sa attendance create time view, my computation na to ng late,overtime,night diff
     * 
     * @param  obj Request data of attendance that will be save in employees_logs
     * @return object
     */
    public function createTimeAttendance(Request $request){

        $temp_data = ($request->all());

        $date = date('Y-m-d H:i:s');
        $data = [];
        $final_data = [];

        foreach ($temp_data as $key => $da) {
            foreach ($da['details'] as $key => $d) {
                $data[] = $d;
            }
        }

        // hindi ko ginamit yung computationOfAttendance function kasi my special validation sya, yun yung 'sa' variable, ask paolo kung para saan yun
        $len = count($data);
        for ($i=0; $i < $len ; $i++) { 

            $att = 1;
            $sched = [];
            
            // once na pumili ang user sa suggested working sched sa time view, yun na yung gagamitin para pang compare na schedule
            // kung hindi naman sya pumili dun yung arrangeDateData yung gagamitin
            if ($data[$i]['sa'] == 0) {
                $id = $data[$i]["id"];
                $d = $data[$i]["start_time"];
                $date =$data[$i]["date"];
                $sched = $this->model->arrangeDateData($id,$d,$date);
            }
            else if ($data[$i]['sa'] != 0) {
                $sched = $this->model->getWorkingScheduleById($data[$i]);
            }

            $data[$i]["restday"] = $sched[0]["restday"];
            $data[$i]["overtime"] = $sched[0]["overtime"];

            $time_in = Carbon::parse($data[$i]["start_time"]);
            $sched_time_in = Carbon::parse($sched[0]["time_in"]);

            $time_out = Carbon::parse($data[$i]["end_time"]);
            $sched_time_out = Carbon::parse($sched[0]["time_out"]);

            if (isset($time_out)) {
              if ($time_in->greaterThan($sched_time_in)) {
                $total_number_of_hours = $sched_time_in->diffInMinutes($time_out);
              } else {
                $total_number_of_hours = $time_in->diffInMinutes($time_out);
              }
            }
            
            if ($total_number_of_hours <= 480 && $total_number_of_hours >= 420) {
                // 7 hours
                $data["halfday"] = 6;
                $undertime = 0;
            } elseif ($total_number_of_hours <= 419 && $total_number_of_hours >= 360) {
                // 6 hours
                $data["halfday"] = 5;
                $undertime = 0;
            } elseif ($total_number_of_hours <= 359 && $total_number_of_hours >= 300) {
                // 5 hours 
                $data["halfday"] = 4;
                $undertime = 0;
            } elseif ($total_number_of_hours <= 299 && $total_number_of_hours >= 240) {
                // 4 hours
                $data["halfday"] = 4;
                $undertime = 0;
            } elseif ($total_number_of_hours <= 239 && $total_number_of_hours >= 180) {
                // 3 hours
                $data["halfday"] = 3;
                $undertime = 0;
            } elseif ($total_number_of_hours <= 179 && $total_number_of_hours >= 120) {
                // 2 hours
                $data["halfday"] = 2;
                $undertime = 0;
            } elseif ($total_number_of_hours <= 119) {
                // 1 hour
                $data["halfday"] = 1;
                $undertime = 0;
            } else {
                $data["halfday"] = 0;
                $undertime_hours = $time_out->diffInMinutes($sched_time_out);
                $undertime = ($time_out <= $sched_time_out ? $undertime_hours : 0);
            }

            if($time_in >= $sched_time_in){
                $late = $time_in->diffInMinutes($sched_time_in);
                $data[$i]["late"] = $late;
            } else {
                $data[$i]["late"] = 0;
            }

            $undertime['undertime'] = $undertime;

            $date = $data[$i]["time_in"];
            $restday = $data[$i]["restday"];
            $holiday = $this->model->getHoliday($date,$restday);
            $data[$i]["holiday_id"] = (int)$holiday;

            $night_count = $this->computeNightShift($data[$i]['time_in'],$data[$i]['time_out']);
            $data[$i]["night_differential"] = (int)$night_count;
        }
        
        $return=$this->model->createTimeAttendance($data);
        $response=[
          'data' => $return
        ];
        return $response;
    }

    public function getList (){

        $model =  $this->model->getList();
        return $model;
    }

    /**
     * use in attendance-list-default
     * 
     * @param  object Request
     * @return object get requested data
     */
    public function getAttendanceDefault(Request $request){
        $data = $request->all();

        $data['employee_id'] = explode(",",$data['employee_id']); 
        $data['company_id'] = explode(",",$data['company_id']); 
        $data['type_id'] = explode(",",$data['type_id']); 

        $data = $this->model->getAttendanceDefault($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    /**
     * use in attendance-list
     * 
     * @param  object Request
     * @return object get requested data
     */
    public function getAttendance(Request $request){
        $data = $request->all();

        $data['employee_id'] = explode(",",$data['employee_id']); 
        $data['company_id'] = explode(",",$data['company_id']); 
        $data['type_id'] = explode(",",$data['type_id']); 

        $data = $this->model->getAttendance($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    /**
     * use in my-attendance-list
     * 
     * @param  object Request
     * @return object get requested data
     */
    public function getMyAttendance(Request $request){

        $dat = $request->all();
        $dat['type_id'] = explode(",",$dat['type_id']); 

        $data=  $this->model->getMyAttendance($dat);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    /**
     * show specific data according to id use in attendance-modal for edit
     * @param  int $id
     * @return object
     */
    public function getAttendanceNotDefaultbyId($id){

        $data=  $this->model->getAttendanceNotDefaultbyId($id);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    /**
     * use to get schedule of employee that will be display in attendance-create time view
     * 
     * @param  obj Request schedule according to employee_id and date
     * @return obj
     */
    public function getSchedules(Request $request){
        $data = $request->all();

        $data = $this->model->getSchedules($data[0]);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    /**
     * convert csv file from import button of attendance-list and attendance-list-default, and return in to be display in attendance-import-review
     * 
     * @param  Request csv file
     * @return obj converted data
     */
    public function convertFile(Request $request){

        $csvFile=($request->get('file_dir'));

        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle) ) {
            $line_of_text[] = fgetcsv($file_handle, 1024);
        }
        fclose($file_handle);
        
        $data =  $line_of_text;

        $array = [];
        $new_data = [];
        $id = [];
        $len = count($data);
        for ($i=1; $i < $len-1; $i++) { 
            if($data[$i][0] != null){
                array_push($id, $data[$i][0]);
                array_push(
                    $new_data,array(
                        'emp_id'        =>$data[$i][0], 
                        'name'          =>$data[$i][1], 
                        'time_in'       =>$data[$i][2], 
                        'time_out'      =>$data[$i][3]
                    )
                );
            }
        }

        //validate employee if existed
        $emp = $this->model->validateEmpId($id);

        //if not exsisted a modal with list of id that is not existed
        if ($emp != null) {
            $response['data'] = array(
                        'message'      => 'Not Exist',
                        'data'         => $emp
                    );
            return $response;
        }

        //para malaman kung yung employee na kinicreate nya ay sa hawak or under nya 
        $val = $this->model->validateCredentialOfCreator($id);

        if ($val != null && $val != [] && count($val) != 0) {
            $response['data'] = array(
                        'message'      => 'Not Allow',
                        'data'         => $val
                    );
            return $response;
        }

        //get branch of employee
        $location = $this->model->getBranch($id);

        $lent = count($location);
        $leng = count($new_data);
        for ($i=0; $i < $leng; $i++) { 
            for ($x=0; $x <$lent ; $x++) { 
                if($location[$x]->id == $new_data[$i]['emp_id']){
                    $new_data[$i]['branch_id']=$location[$x]->branch_id;
                    $new_data[$i]['name']=$location[$x]->name;
                }
            }
        }

        $response=[
          'data' => $new_data
        ];
        return $response;
    }

    /**
     * 1st ko pong code sa pag create ng attendance sa day view save data in employees_logs,undertime,overtime table
     * 
     * @param  obj Request
     * @return obj
     */
    public function createDayAttendance2(Request $request){

        $data=($request->all());
        $len = count($data);
        $validateEmp = [];

        $this->model->updateRate($data);

        //450 is 7.5
        //480 is 8
        //pag ang undertime nya ay 450 means my 30 minutes sya na late at pumasok sya ng araw na yun kaso nirecord binwas na lang yun sa late nya
        //pag ang late nya ay mas mababa sa 450 means hindi sya nakawholeday kay hindi nag aadd ng 1

        for ($i = 0; $i < $len; ++$i) {
            // $data[$i]['undertime'] =  $data[$i]['undertime'] * 60;
            $data[$i]['ot'] = $data[$i]['overtime'];
            $data[$i]['overtime'] =  $data[$i]['overtime'] * 60;
            $data[$i]['ut'] = $data[$i]['undertime'];

            $x =  $data[$i]['undertime'] / 450;

            //how many times the whole number and half
            //if x is float it means we need to add 
            if (is_numeric( $x ) && (floor( $x ) != $x)) {
                $data[$i]['utdays'] = floor($x) + 1;    
                $data[$i]['unddays'] = floor($x) + 1;

                $data[$i]['wholenum'] = floor($x);
                $data[$i]['wholeno'] = floor($x);
            }else{
                $data[$i]['utdays'] = $x; 
                $data[$i]['unddays'] = $x;

                $data[$i]['wholenum'] = $x;
                $data[$i]['wholeno'] = $x;
            }
        }
        
        foreach ($data as $d) {
            if ($d['days'] == 0) {
                $validateEmp[] = array(
                    'emp_id'   => $d['emp_id'],
                    'name'     => $d['name']
                );
            }
        }

        if($validateEmp != []){
            $response['data'] = array(
                'message'      => 'Unprocessable Entity',
                'status_code'  => 422,
                'data'  => $validateEmp,
                'model' => $data
            );
            return $response;
        }else{
            $return=$this->model->createDayAttendance2($data);
            $response=[
              'data' => $return
            ];
            return $response;
        }

    }

    /**
     * 2nd ko pong code sa pag create ng attendance sa day view save data in employees_logs,overtime table
     * @param  data Request
     * @return obj
     */
    public function createDayAttendanceDefault(Request $request){

        $data=($request->all());
        $validateEmp = [];

        $this->model->updateRate($data);

        foreach ($data as $d) {
            if ($d['days'] == 0) {
                $validateEmp[] = array(
                    'emp_id'   => $d['emp_id'],
                    'name'     => $d['name']
                );
            }
        }

        if($validateEmp != []){
            $response['data'] = array(
                'message'      => 'Unprocessable Entity',
                'status_code'  => 422,
                'data'  => $validateEmp,
                'model' => $data
            );
            return $response;
        }else{
            $return=$this->model->createDayAttendanceDefault($data);
            $response=[
              'data' => $return
            ];
            return $response;
        }
    }

    /**
     * use to attendance create save data both in employees_logs and employee_logs2 table
     * 
     * @param  obj Request
     * @return obj
     */
    public function createDayAttendance(Request $request){

        $data=($request->all());
        $validateEmp = [];

        $this->model->updateRate($data);

        foreach ($data as $d) {
            if ($d['days'] == 0) {
                $validateEmp[] = array(
                    'emp_id'   => $d['emp_id'],
                    'name'     => $d['name']
                );
            }
        }

        if($validateEmp != []){
            $response['data'] = array(
                'message'      => 'Unprocessable Entity',
                'status_code'  => 422,
                'data'  => $validateEmp,
                'model' => $data
            );
            return $response;
        }else{
            $return=$this->model->createDayAttendance($data);
            $response=[
              'data' => $return
            ];
            return $response;
        }
    }

    /**
     * ignore employee that doesn't have total days
     * use in attendance-create day view validation and save in createDayAttendance
     * @param  obj Request
     * @return obj
     */
    public function ignoreEmpInDayAttendance(Request $request){

        $temp=($request->all());
        $data = [];

        foreach ($temp as $key => $d) {
            if ($d['days'] != 0) {
                $data[]=$d;
            }
        }

        $return=$this->model->createDayAttendance($data);
        $response=[
          'data' => $return
        ];
        return $response;

    }

    /**
     * get logs and display in employee-dashboard
     * 
     * @return obj
     */
    public function getMyLogs(){

        $data = [];
        $data['data'] =  $this->model->getMyLogs();

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    /**
     * create attendance from employee-dashboard
     * online logIn and logOut
     * 
     * @param  obj Request
     * @return obj
     */
    public function createAttendanceFromWeb(Request $request){

        $data=($request->all());
        $user = $this->model->getUser();

        $data['employee_id'] = $user->employee_id;
        $data['created_by'] = $user->user_id;

        $new_data = [];


        if ($data['type'] ==  'logIn') {

            $dup=$this->model->duplicateLogIn($data);

            if($dup == []){
                $type = 6;
                $return=$this->model->createLogIn($data,$type);
            }
            else{
                $response['data'] = array(
                    'message'      => 'no changes',
                    'status_code'  => 200
                );
                return $response;
            }
        }
        else if($data['type'] ==  'logOut'){
            $in = $this->model->getTimeIn($data);

            $new_data = array(
                    'emp_id'    => $data['employee_id'], 
                    'id'        => $in['id'], 
                    'date'      => $data['date'], 
                    'time_in'   => $in['time_in'], 
                    'time_out'  => $data['datetime'],
                    'branch_id' => $data['branch_id'],
                    'created_by'=> $data['created_by']
                );

            $num = substr($new_data['time_in'], 11);
            $new_data['start_time'] = Carbon::parse($num);

            $num2 = substr($new_data['time_out'], 11);
            $new_data['end_time'] = Carbon::parse($num2);

            $final_data=$this->computationOfAttendance($new_data);

            $return=$this->model->createLogOut($final_data);

        }
        else if($data['type'] ==  'lunchIn' || $data['type'] ==  'lunchOut' || $data['type'] ==  'meetingIn' || $data['type'] ==  'meetingOut'|| $data['type'] ==  'seminarIn' || $data['type'] ==  'seminarOut'|| $data['type'] ==  'outOffice' || $data['type'] ==  'inOffice'){
            $return=$this->model->updateLogs($data);
        }

        $response=[
          'data' => $return
        ];
        return $response;
    }

    /**
     * convert csv file from attendance-import-modal for biometrics and save in employees_logs
     * @param  Request csv
     * @return obj
     */
    public function importFromBioFile(Request $request){

        $csvFile=($request->get('file_dir'));

        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle) ) {
            $line_of_text[] = fgetcsv($file_handle, 1024);
        }
        fclose($file_handle);
        
        $data =  $line_of_text;
        $arr = [];
        $len = count($data);
        $key = '';
        $uid = '';
        $date;

        for ($i = 0; $i < $len; $i++) { 
          // get date
          if(!isset($data[$i][2]))
            continue;
            $tmpKey = preg_replace('/\s+/', '', $data[$i][2]);
            if(!empty($tmpKey)){
                $x = explode(" ",$tmpKey);
                if(!isset($x[2]))
                $x = explode("/",$tmpKey);
                if(!isset($x[2]))
                $x = explode("-",$tmpKey);
                if(!isset($x[2]))
                  continue;
                // convert date
                // x[0] = month
                // x[1] = Day
                // x[2] = Year
                $date = date( 'Y-m-d', strtotime( $x[2] . '-' . $x[0] .'-' . $x[1] ));

                $d = $data[$i][2];
          
                $id = preg_replace('/\s+/', '', $data[$i][1]);
                if(!empty($id)){
                    $uid = $id;
                }
                $key = $tmpKey . $uid;
                $arr[$key]['id'] = $uid;
            }
            // if date > 0
            if(!count($data[$i][2]) > 0)
                continue;
            // time in
            $time_in = trim($data[$i][4]);

            if(isset($time_in) && $time_in != null){

                $temp_time_in = $this->timeFormat($time_in);

                $in = $temp_time_in->format('H:i');

                if(!isset($arr[$key]['in'])){
                    $arr[$key]['in'] = $in;
                    $arr[$key]['id'] = $uid;
                    $arr[$key]['date'] = $date;
                }
            } 

            // time out
            $time_out = trim($data[$i][5]);
            
            if(isset($time_out) && $time_out != null){

                $temp_time_out = $this->timeFormat($time_out);
              
                $out = $temp_time_out->format('H:i');
                  if(!empty($out)){
                      $arr[$key]['out'] = $out; 
                      $arr[$key]['id'] = $uid;
                      $arr[$key]['date'] = $date;
                  }
            } 
        }

        // return $arr; exit;
        $return = $this->createAttendanceFromBioFile($arr);
        $response=[
          'data' =>  $return
        ];
        return $response;
    }

    public function importFromBioFile2(Request $request){

        $csvFile=($request->get('file_dir'));

        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle) ) {
            $line_of_text[] = fgetcsv($file_handle, 1024);
        }
        fclose($file_handle);
        
        $data =  $line_of_text;
        $arr = [];
        $temp_arr = [];
        $len = count($data);
        $key = '';
        $uid = '';
        $date;

        for ($i=0; $i < $len; $i++) { 
              /* 
              formats
                 1. 12-25-20000 8:00    - m-d-Y H:i
                 2. 12-25-2000 8:00:01  - m-d-Y H:i:s
                 3. 12-25-20000 8:00 AM - m-d-Y H:i a
                 4. 25-12-2000 8:00     - d-m-Y H:i
                 5. 25-12-2000 8:00:01  - d-m-Y H:i:s
                 6. 25-12-2000 8:00 AM  - d-m-Y H:i a
                 7. 12/25/2000 8:30     - m/d/Y H:i
                 8. 12/25/2000 8:00:01  - m/d/Y H:i:s
                 9. 12/25/2000 8:00 AM  - m/d/Y H:i a
                 10. 25/12/2000 8:00    - d/m/Y H:i
                 11. 25/12/2000 8:00:01 - d/m/Y H:i:s
                 12. 25/12/2000 8:00 AM - d/m/Y H:i a
                 13. 2000-12-25 8:00    - Y-m-d H:i
                 14. 2000-12-25 8:00:01 - Y-m-d H:i:s
                 15. 2000-12-25 8:00 AM - Y-m-d H:i a
              */
              $d = $data[$i][1];

              $temp_date = $this->dateFormat($d);

              $id = preg_replace('/\s+/', '', $data[$i][0]);
              // Format date to 2000-12-25 8:00
              $date = $temp_date->format('Y-m-d');
              // get the time
              $time = $temp_date->format('H:i');
              
              // setting the datas
              $temp_arr[$i]['id'] = $id;
              $temp_arr[$i]['date'] = $date;
              $temp_arr[$i]['time'] = $time;
        }
        /* REMOVE DUPLICATES */ 
        // collect duplicate
        $duplicate_array = [];
        foreach($temp_arr as $value){
          $duplicate_array[$value['id'].'_'.$value['date']][] = $value;  
        }
        // change index
        $temp_duplicate_array = array_values($duplicate_array);
        $temp_duplicate_array_length = count($temp_duplicate_array) - 1;
        // Change the array list to by row format
        for ($y=0; $y < $temp_duplicate_array_length; $y++) { 
          if (count($temp_duplicate_array[$y] > 2)) {
            // get the first in and last out of the duplicate data
            $new_data[] = array_shift($temp_duplicate_array[$y]);
            $new_data[] = array_pop($temp_duplicate_array[$y]);
            continue;
          } elseif (count($temp_duplicate_array[$y]) == 2) {
            // set the non duplicated data
            $new_data[] = $temp_duplicate_array[$y][0]; 
            $new_data[] = $temp_duplicate_array[$y][1]; 
          } elseif (count($temp_duplicate_array[$y]) == 1) {
            //  no out
            $new_data[] = $temp_duplicate_array[$y][0]; 
          }
        }
        
        // set the data to $arr        
        $arr = $new_data;
        $ctr1 = count($arr);
        // make a row for no time in or time out
        // replace to null value */ 
        for ($z = 0; $z < $ctr1; $z++) { 
          // no out 
          // index 1, 3, 5 etc..
          if ($z % 2 != 0) {
            if ($arr[$z] == null) {
              // no time out
              // use the id and date from previous data
              $prev = $z - 1;
              $arr[$z]['id'] = $arr[$prev]['id'];
              $arr[$z]['date'] = $arr[$prev]['date'];
              $arr[$z]['time'] = null;
            }
          } else {
            // no time in 
            // use the id and date from next data
            // index 0, 2, 4 etc
            if ($arr[$z] == null) {
              $next = $z + 1;
              $arr[$z]['id'] = $arr[$next]['id'];
              $arr[$z]['date'] = $arr[$next]['date'];
              $arr[$z]['time'] = null;
            }
          } 
        }

        // loop the array as base 
        // $i+=2 to skip the time_out
        for ($i=0; $i < $ctr1; $i+=2) { 
          // $arr should be complete here
          $ctr2 = count($arr);
          // loop the other arrays for comparison
          for ($x=0; $x < $ctr2; $x++) { 
            // check if the id & date is the same
            // $arr[$i] should not be equals $arr[x]
            if ($arr[$i]['id'] == $arr[$x]['id'] && $arr[$i]['date'] == $arr[$x]['date'] && $arr[$i]['time'] != $arr[$x]['time']) {
              // setting the values
              $sorted_data[$i]['id'] = $arr[$i]['id'];
              $sorted_data[$i]['date'] = $arr[$i]['date'];
              $sorted_data[$i]['in'] = $arr[$i]['time'];
              $sorted_data[$i]['out'] = $arr[$x]['time'];
              // $sorted_data[$i]['lunch_in'] = $arr[$x]['time'];
              // $sorted_data[$i]['lunch'] = $arr[$x]['time'];
            } 
          }
        }

        // reuse the createAttendaceFromBioFile method
        $return = $this->createAttendanceFromBioFile($sorted_data);
        $response = [
          'data' =>  $return
        ];
        return $response;
    }

    public function importFromBioFile3(Request $request){

          $csvFile=($request->get('file_dir'));

          $file_handle = fopen($csvFile, 'r');
          while (!feof($file_handle) ) {
              $line_of_text[] = fgetcsv($file_handle, 1024);
          }
          fclose($file_handle);
          
          $data =  $line_of_text;
          $arr = [];
          $temp_arr = [];
          $len = count($data);
          $key = '';
          $uid = '';
          $date;

          for ($i=0; $i < $len; $i++) { 
              
                $d = $data[$i][1];

                $temp_date = $this->dateFormat($d);

                $id = preg_replace('/\s+/', '', $data[$i][0]);
                // Format date to 2000-12-25 8:00
                $date = $temp_date->format('Y-m-d');
                // get the time
                $time = $temp_date->format('H:i:s');
                
                // setting the datas
                $temp_arr[$i]['id'] = $id;
                $temp_arr[$i]['date'] = $date;
                $temp_arr[$i]['time'] = $time;
          }
          /* REMOVE DUPLICATES */ 
          // collect duplicate
         
          //  $duplicate_array = [];
          //   foreach($temp_arr as $value){
          //     $duplicate_array[$value['id'].'_'.$value['date']][] = $value;  
          //   }
          // $duplicate_array = [];



          $duplicate_array = [];
          foreach($temp_arr as $value){
            if ($value['id']) {
              $duplicate_array[$value['id'].'_'.$value['date']][] = $value;  
            }
          }

          // NEED I LOOP YUNG TEMP TIME IN
          

          // converting to array and changing index
          $temp_time_in = array_values($duplicate_array);
          $temp_duplicate_array_length = count($temp_time_in);
          
          $new_data = [];
          for ($i=0; $i < $temp_duplicate_array_length; $i++) { 
            // getting time in
            // $temp_time_in = array_values($duplicate_array);
            // $temp_duplicate_array_length = count($temp_time_in);
            // 
            $new_data[] = $this->getTimeIn($temp_time_in[$i]);
            $removed_duplicate_time_in = $this->removeExtraTimeWithinSixtySeconds($temp_time_in[$i]);
            $removed_duplicate_time_in_len = count($removed_duplicate_time_in);
            if ($removed_duplicate_time_in_len == 1) {
              // if the remaining time in is 1 then it's time out
              $new_data[] = null; // lunch in
              $new_data[] = null; // lunch out 

              // set the last time in to time out
              $removed_duplicate_lunch_out = $this->removeExtraTimeWithinSixtySeconds($temp_time_in[$i]);
              $array_length = count($temp_time_in);
              $temp_time_out = array_values($removed_duplicate_lunch_out);
              $new_data[] = $this->getTimeIn($temp_time_out);
            } else {

              $time_in = array_slice( $new_data, -1 );
              // getting lunch
              $temp_lunch_in = array_values($removed_duplicate_time_in);

              $array_length = count($temp_lunch_in);

              $next_date =$this->getTimeIn($temp_lunch_in);
              // get the difference
              $results = $this->getTheDifferenceInSeconds($time_in[0]['time'], $next_date['time']);
              $new_data[] = $this->getTimeIn($temp_lunch_in);
              $removed_extra_lunch_in = $this->removeExtraTimeWithinSixtySeconds($temp_lunch_in);
              // lunch
              $lunch_in = array_slice( $new_data, -1 );
              // get the next date
              
              // getting lunch out 
              $temp_lunch_out = array_values($removed_extra_lunch_in);
              $array_length = count($temp_lunch_out);
              $next_date = $this->getTimeIn($temp_lunch_out);

              // get the difference
              $results = $this->getTheDifferenceInSeconds($lunch_in[0]['time'], $next_date['time']);
              if ($results >= 0 && $results <= 7200) {
                $new_data[] = $this->getTimeIn($temp_lunch_out);
                $removed_duplicate_lunch_out = $this->removeExtraTimeWithinSixtySeconds($temp_lunch_out);
                $temp_time_out = array_values($removed_duplicate_lunch_out);
              } else {
                $new_data[] = null;
                $temp_time_out = array_values($removed_extra_lunch_in);
              }
              // getting time out
              $array_length = count($temp_time_out);
              $new_data[] = $this->getTimeIn($temp_time_out);
            }

          }

          /* $new data format
                $new_data[0] = time_in
                $new_data[1] = lunch_in
                $new_data[2] = lunch_out
                $new_data[3] = time_out
          */
          // set the data to $arr        
          $arr = $new_data;

          $ctr1 = count($arr);
          // make a row for no time in or time out
          // replace to null value */ 
          for ($z = 0; $z < $ctr1; $z++) { 
            if ($arr[$z] == null) {
              $len = $ctr1; 
              for ($x=0; $x < $len; $x++) { 
                  if ( ((4 * $x) + 1) == $z) { //lunch in
                    // sequence 1, 5, 9, 13, 17, 21, 25, 29, 33, 37 etc
                    $prev = $z - 1;
                    $arr[$z]['id'] = $arr[$prev]['id'];
                    $arr[$z]['date'] = $arr[$prev]['date'];
                    $arr[$z]['time'] = null;
                  } elseif ( (4 * $x) + 2 == $z) { // lunch out
                   # sequence 2, 6, 10, 14, 16, 20.. etc..
                   $prev = $z - 2; // always get the time in
                   $arr[$z]['id'] = $arr[$prev]['id'];
                   $arr[$z]['date'] = $arr[$prev]['date'];
                   $arr[$z]['time'] = null;
                  } elseif ( (4 * $x) + 3 == $z ) { // time out
                   // sequence 3, 7, 11, 15 etc...
                   $prev = $z - 3; // always get the time in
                   $arr[$z]['id'] = $arr[$prev]['id'];
                   $arr[$z]['date'] = $arr[$prev]['date'];
                   $arr[$z]['time'] = null;
                 } // if
              } // for len
            } // if arr[z] null
          } // for ctr1

        // loop the array as base 
        // $i+=2 to skip the time_out
        for ($i = 0; $i < $ctr1; $i+=4) { 
          // $arr should be complete here
          $ctr2 = count($arr);
          // loop the other arrays for comparison
          for ($x = 0; $x < $ctr2; $x++) { 
            // check if the id & date is the same
            // $arr[$i] should not be equals $arr[x]
            if ($arr[$i]['id'] == $arr[$x]['id'] && $arr[$i]['date'] == $arr[$x]['date']) {
              // setting the values
              $sorted_data[$i]['id'] = $arr[$i]['id'];
              $sorted_data[$i]['date'] = $arr[$i]['date'];
              $sorted_data[$i]['in'] = $arr[$i]['time'];
              $sorted_data[$i]['lunch_in'] = $arr[$i+1]['time'];
              $sorted_data[$i]['lunch_out'] = $arr[$i+2]['time'];
              $sorted_data[$i]['out'] = $arr[$x]['time'];
            } 
          }
        }

        // reuse the createAttendaceFromBioFile method
        $return = $this->createAttendanceFromBioFile($sorted_data);
        $response = [
          'data' =>  $return
        ];
        return $response;
    }

    public function createAttendanceFromBioFile($temp){

        $data = [];
        $i = 0;
        $return = [];
        foreach ($temp as $key => $d) {
            if (isset($d['in']) && isset($d['out'])) {
              if ($d['in'] != null && $d['out'] != null) {
                  $new_data = [];
                  if (array_key_exists('lunch_in', $d)) {
                    $lunch_in = ($d['lunch_in'] != null ? $d["date"].' '.$d["lunch_in"] : null);
                    $start_lunch = date('H:i:s', strtotime($d['lunch_in']));
                  } else {
                    $lunch_in = null;
                    $start_lunch = null;
                  }
                  if (array_key_exists('lunch_out', $d)) {
                    $lunch_out = ($d['lunch_out'] != null ? $d["date"].' '.$d["lunch_out"] : null);
                    $end_lunch = date('H:i:s', strtotime($d['lunch_out']));
                  } else {
                    $lunch_out = null;
                    $end_lunch = null;
                  }
                  $new_data = array(
                          'emp_id'     => $d["id"],  
                          'date'       => $d["date"], 
                          'time_in'    => $d["date"].' '.$d["in"], 
                          'time_out'   => $d["date"].' '.$d["out"],
                          'lunch_in'   => $lunch_in,
                          'lunch_out'  => $lunch_out,
                          'start_time' => date('H:i:s', strtotime($d['in'])),
                          'end_time'   => date('H:i:s', strtotime($d['out'])),
                          'start_lunch'=> $start_lunch,
                          'end_lunch'  => $end_lunch
                      );

                  $data[] = $this->computationOfAttendance($new_data);
              } else if($d['in'] != null && $d['out'] == null){
                  $type = 'in';
                  // store the ids
                  $return = array_merge($return, $this->model->createLoginOrLogout($d, $type)->toArray());
              } else if($d['out'] != null && $d['in'] == null){
                  $type = 'out';
                  // store the ids
                  $return = array_merge($return, $this->model->createLoginOrLogout($d,$type)->toArray());
              }
            }
        }
        // store the ids
        $return = array_merge($return, $this->model->createAttendanceFromBioFile($data)->toArray());

        $this->model->getUploadHistoryID($return);

        $response = [
          'data' => $return
        ];

        return $response;

    }

    /**
     * convert excel file from attendance-import-modal for biometrics and save in employees_logs
     * @param  Request $request excel file
     * @return obj           [description]
     */
    public function importFromBioFilefirstversion(Request $request){
        $file=($request->get('file_dir'));

        $exc = Excel::load($file, function ($reader) {
            foreach ($reader->toArray() as $key => $row) {
            }
        });

        $dat = $exc->parsed;

         foreach ($dat as $d) {
            $temp[] = array(
                'bio_id'           => $d->personnel_id, 
                'datetime'         => $d->date_and_time->format('Y-m-d H:i:s'),
                'date'             => $d->date_and_time->format('Y-m-d'),
                'time'             => $d->date_and_time->format('H:i:s'),
                'device_name'      => $d->device_name
            );
        }
        
        $return = $this->createAttendanceFromBioFilefirstversion($temp);
    }

    /**
     * for computation, arrangement, and saving of data from excel
     * 
     * @param  array $temp data from importExcel
     * @return obj
     */
    public function createAttendanceFromBioFilefirstversion($temp){

        $user = $this->model->getUser();

        //collecting duplicate data
        $newTemp = [];
        foreach($temp as $value){
          $newTemp[$value['bio_id'].'_'.$value['date']][] = $value;  
        }
        //converting index elemet
        $tem = array_values($newTemp);
         

        $lent = count($tem);
        for ($i=0; $i <$lent ; $i++) { 
            //removing excess data, only first and last element remain
            if (count($tem[$i]) > 2) {
                $da[] = array_shift($tem[$i]);
                $da[] = array_pop($tem[$i]);
            }
            elseif(count($tem[$i]) == 2){
                //validation if 2 in but no out
                $item1 = Carbon::parse($tem[$i][0]['time']);
                $item2 = Carbon::parse($tem[$i][1]['time']);
                $time = $item1->diffInMinutes($item2);

                if ($time >= 30) {
                    $da[] = $tem[$i][0];
                    $da[] = $tem[$i][1];
                }else{
                    $da[] = $tem[$i][0];
                }
            }
            elseif(count($tem[$i]) == 1){
                $da[] = $tem[$i][0];
            }
        }

        $val = 2;

        $data=$this->model->identifyTypeEmpId($da,$val);

        $len = count($data);
        for ($x=0; $x <$len ; $x++) { 

            $dat = $this->model->checkTypeOfLog($data[$x]);

            if(count($dat) == 1){
                $in = $this->model->getBioTimeIn($data[$x]);

                $new_data=array(
                        'emp_id'    => $data[$x]['employee_id'], 
                        'id'        => $in['id'], 
                        'date'      => $data[$x]['date'], 
                        'time_in'   => $in['time_in'], 
                        'time_out'  => $data[$x]['datetime'],
                        'branch_id' => $data[$x]['branch_id'],
                        'created_by'=> $user->user_id
                    );

                $num = substr($new_data['time_in'], 11);
                $new_data['start_time'] = Carbon::parse($num);

                $num2 = substr($new_data['time_out'], 11);
                $new_data['end_time'] = Carbon::parse($num2);

                $final_data = $this->computationOfAttendance($new_data);

                $return=$this->model->createLogOut($final_data);

            }else{
                $type = 3;
                $return=$this->model->createLogIn($data[$x],$type);
            }
        }
    }

    /**
     * use in biometrics button in attendance-list and attendance-list-default
     * 
     * @param  obj Request
     * @return obj
     */
    public function fetch(Request $request){ 

        $d = $request->all();
        $ip = $d['ip_address'];

        $zk = new ZKLib($ip, 4370);
        $ret = $zk->connect();
        $attendance = $zk->getAttendance();
        
        $x = trim($zk->deviceName(), '~DeviceName=');
        $deviceName = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $x);

        $date = date('Y-m-d H:i:s');
        $len = count($attendance);
        for ($i=9787; $i <9793 ; $i++) { 
            $table = new ZKAttendance;
            $table->uid = $attendance[$i][0];
            $table->bio_id = $attendance[$i][1];
            $table->type =  $attendance[$i][2];
            $table->device_name =  $deviceName;
            $table->date = date( "Y-m-d", strtotime( $attendance[$i][3] ) );
            $table->time = date( "H:i:s", strtotime( $attendance[$i][3] ) );
            $table->date_time = $attendance[$i][3];
            $table->deleted_by = 1;
            $table->deleted_at = $date;
            $table->save();
        }

        //note delete attendance after you get it
        // // $zk->clearAttendance();

        $return = $this->arrangeBioMetData($attendance,$deviceName);

        $response=[
          'data' =>  $return
        ];
        return $response;
    }

    /**
     * arrange data from fetch function
     * 
     * @param  array $attendance
     * @param  string $deviceName
     * @return obj
     */
    public function arrangeBioMetData($attendance,$deviceName){

        $temp=[];
        $data=[];

        foreach ($attendance as $d) {
            $temp[]=array(
                'bio_id'        => $d[1], 
                'datetime'      => $d[3],
                'device_name'   => $deviceName,
                'date'          => date( "Y-m-d", strtotime( $d[3] ) ),
                'time'          => date( "H:i:s", strtotime( $d[3] ) )
            );
        }

        $temp = $this->unique_multidim_array($temp,'bio_id');

        //arranging data 0-so on
        foreach ($temp as $d) {
            $data[]=array(
                'bio_id'        => $d['bio_id'], 
                'datetime'      => $d['datetime'],
                'device_name'   => $d['device_name'],
                'date'          => $d['date'],
                'time'          => $d['time']
            );
        }

        $val = 1;

        $data=$this->model->identifyTypeEmpId($data,$val);

        $len = count($data);

        for ($i=0; $i <$len ; $i++) { 
            $type = 3;

            if ($data[$i]['type'] == 'in') {
                $return=$this->model->createLogIn($data[$i],$type);
            }
            elseif ($data[$i]['type'] == 'out') {
                $in = $this->model->getBioTimeIn($data[$i]);

                $new_data=array(
                        'emp_id'    => $data[$i]['employee_id'], 
                        'id'        => $in['id'], 
                        'date'      => $data[$i]['date'], 
                        'time_in'   => $in['time_in'], 
                        'time_out'  => $data[$i]['datetime'],
                        'branch_id'  => $data[$i]['branch_id'],
                        'created_by'=> $data[$i]['created_by']
                    );

                $num = substr($new_data['time_in'], 11);
                $new_data['start_time'] = Carbon::parse($num);

                $num2 = substr($new_data['time_out'], 11);
                $new_data['end_time'] = Carbon::parse($num2);

                $final_data = $this->computationOfAttendance($new_data);


                $this->model->createLogOut($final_data);
            }
        }

        return $data;
    }

    /**
     * use in arrangeBioMetData to remove duplicate data
     * 
     * @param  array $array
     * @param  string $key
     * @return array
     */
    function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 
        
        foreach($array as $val) { 
            if (!in_array($val[$key], $key_array)) { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    } 

    /**
     * fetch data from temp_stor_fr_bio and compute, save in employees_logs
     * 
     * @return obj
     */
    public function fetchDataFromBioTable(){ 

        $data=$this->model->fetchDataFromBioTable();

        foreach ($data as $d) {
            $temp[] = array(
                'bio_id'           => $d->bio_id, 
                'datetime'         => $d->date_time,
                'date'             => $d->date,
                'time'             => $d->time,
                'device_name'      => $d->device_name
            );
        }

        $return = $this->createAttendanceFromBioFilefirstversion($temp);
     
        $response=[
          'data' =>  $return
        ];
        
        return $response;
    }

    /**
     * validation for manual logIn in the browser use in employee-dashboard
     * 
     * @param  string $emp_id
     * @return obj
     */
    public function logsValidation(Request $request){ 
        $all            = null; // shell_exec('ipconfig -all');
        $mac_address    = null;
        $ddns           = null;

        //for ddns and mac_address
        // if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        //     $all = shell_exec('ipconfig -all');
        // } else {
        //     $all = shell_exec("netstat -ie");
        // }

        // $address = preg_split("/\\r\\n|\\r|\\n/", $all);
        // $convert = substr($address[35], 39);

        // foreach($address AS $addr_row){
        //     if(strpos(strtolower($addr_row), 'physical address') !== false){
        //         $mac_address = str_replace([ '.', ' ', 'Physical', 'Address' ], '', $addr_row);
        //         $mac_address = str_replace(':', '', $mac_address);
        //         break;
        //     }
        // }

        // foreach($address AS $addr_row){
        //     if(strpos(strtolower($addr_row), 'dns servers') !== false){
        //         $ddns = str_replace([ '. ', 'DNS', 'Servers' ], '', $addr_row);
        //         $ddns = str_replace(':', '', $ddns);
        //         break;
        //     }
        // }

        // //delete white space
        // $ddns = preg_replace('/\s+/', '', $ddns);
        $ip_address = $request->ip(); //substr($convert, 0,-12);


        $data =  $this->model->logsValidation($ip_address, $mac_address, $ddns);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    /**
     * get employee rate and display in attendance create day view
     * 
     * @param  string Request list of employee
     * @return obj
     */
    public function getEmployeeRate(Request $request){

        $data=($request->all());

        $return = $this->model->getEmployeeRate($data);


        $response=[
          'data' => $return
        ];
        return $response;
    }

    /**
     * update attendance in edit button of attendance-list
     * 
     * @param  string $id
     * @param  obj Request
     * @return obj
     */
    public function updateAttendanceNotDefault($id,Request $request){

        $model=($request->all());

        $data['data'] = $this->model->updateAttendanceNotDefault($id,$model);
        $data = $this->addShowData($data, $id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    /**
     * archive attendance in remove button of attendance-list
     * 
     * @param  obj Request
     * @return obj
     */
    public function archiveAttendanceNotDefault(Request $request){

        $data = ($request->all());

        $id = $data['id'];

        $return = $this->model->archiveAttendanceNotDefault($id);

        $response=[
          'data' => $return
        ];
        
        return $response;
    }

    public function updateAttendanceFromApproveCOA($d){

        if ($d['type'] == 'In') {
            //datetime
            $d['time_in'] = $d['date'] .' '. $d['time'];
            //time
            $d['start_time'] = $d['time'];
            $temp = $this->model->getAttendanceByIdAndLoginLogout($d);
            $d['employees_logs_id'] = $temp->id;   
            //time
            $d['end_time'] = $temp->end_time;   
            //datetime
            $d['time_out'] = $temp->time_out;   
        }
        else if ($d['type'] == 'Out') {
            //datetime
            $d['time_out'] = $d['date'] .' '. $d['time'];
            //time
            $d['end_time'] = $d['time'];
            $temp = $this->model->getAttendanceByIdAndLoginLogout($d);
            $d['employees_logs_id'] = $temp->id;
            //time
            $d['start_time'] = $temp->start_time;
            //datetime
            $d['time_in'] = $temp->time_in;
        }

        $final_data= $this->computationOfAttendance($d);

        $data = $this->model->updateAttendanceFromApproveCOA($final_data);

        return $data;
    }

    public function updateAttendanceFromApproveOB($temp){

        $d = [];
        $d['time_in'] = $temp->date.' '. $temp->start_time;
        $d['time_out'] = $temp->date .' '. $temp->end_time;
        $d['start_time'] = $temp->start_time;
        $d['end_time'] = $temp->end_time;
        $d['branch_id'] = $temp->branch_id;
        $d['emp_id'] = $temp->emp_id;
        $d['date'] = $temp->date;

        $final_data= $this->computationOfAttendance($d);

        $data = $this->model->updateAttendanceFromApproveOB($final_data);

        return $data;
    }

    public function computationOfAttendance($data){
        //string
        $id = $data["emp_id"];
        //time
        $d = $data["start_time"];
        $date = $data["date"];
        
        if (isset($data['schedule'])) {
          $sched_id = $data['schedule'];
          $current_sched_id = $this->model->getScheduleId($id);

          if ($current_sched_id->id == $sched_id) {
            $sched = $this->model->arrangeDateData($id,$d,$date);
          } else {
            $sched = $this->model->getWorkingScheduleListById($sched_id);
          }
        } else {
            $sched = $this->model->arrangeDateData($id,$d,$date);
        }

        //string 1 or 0
        $data["restday"] = $sched[0]["restday"];
        //int 1 or 0
        $data["overtime"] = $sched[0]["overtime"];

        $time_in = Carbon::parse($data["start_time"]);
        $sched_time_in = Carbon::parse($sched[0]["time_in"]);

        $time_out = Carbon::parse($data["end_time"]);
        $sched_time_out = Carbon::parse($sched[0]["time_out"]);

        $lunch_in = Carbon::parse($data['start_lunch']);
        $sched_lunch_in = Carbon::parse($sched[0]["lunch_in"]);

        $lunch_out =  Carbon::parse($data["end_lunch"]);
        // sched[lunchout]  is break_in_mins 
        $sched_lunch_out = $lunch_in->addMinutes($sched[0]["lunch_out"]);


        $total_number_of_hours = $this->totalNumberOfHours($time_in, $time_out, $sched_time_in, $sched_time_out, $lunch_in, $lunch_out);
        $total_number_of_working_hours = $sched_time_in->diffInMinutes($sched_time_out);

        // get halfday
        $halfday = $this->getHalfdayHours($total_number_of_hours, $total_number_of_working_hours, $data['start_lunch'], $data['end_lunch']);

        // get undertime 
        if ($halfday != 0) {
          $undertime = 0;
        } else {
          $undertime_hours = $time_out->diffInMinutes($sched_time_out);
          $undertime = ($time_out <= $sched_time_out ? $undertime_hours : 0);
        }
        //compute late on time in
        if($time_in >= $sched_time_in){
          $late = $time_in->diffInMinutes($sched_time_in);
        } else {
          $late = 0;
        }
        // compute late on lunch in
        if ($lunch_out == null || $sched_lunch_out == null) {
          $lunch_late = 0;
        } else {
          if ($lunch_out >= $sched_lunch_out) {
            $lunch_late = $lunch_out->diffInMinutes($sched_lunch_out);
          } else {
            $lunch_late = 0;
          }
        }
        
        $data['halfday'] = $halfday;
        $data['late'] = $lunch_late + $late; 
        $data["undertime"] = $undertime;

        $date = $data["time_in"];
        $restday = $data["restday"];
        $holiday = $this->model->getHoliday($date,$restday);
        //int anong type ng holiday sya, see holiday_type table
        $data["holiday_id"]= (int)$holiday;

        // $night_count = $this->computeNightShift($data["start_time"],$data["end_time"]);
        //int compute hours of night_diff
        $data["night_differential"] = 0;

        return $data;
    }

    public function getUploadHistoryFromAttendance(){
        $data = [];
        $data['data'] = '';
        $data['data'] =  $this->model->getUploadHistoryFromAttendance();
        return $data;
    }

    public function deleteAttendanceByBatch($id){
        $data = [];
        $data['data'] = '';
        $data['data'] =  $this->model->deleteAttendanceByBatch($id);
        return $data;
    }

    /**
     * Remove extra time within sixty seconds
     * @param  array $array 
     * @return $array        clean list of time
     */
    public function removeExtraTimeWithinSixtySeconds($array) {
        $new_date[] = array_shift($array);
        // removing multiple time in
        $first_time_in = new Carbon($new_date[0]['time']);
        $arr2 = $array;
        $ctr = count($arr2);
        for ($i=0; $i < $ctr; $i++) { 
          $temp_time_ins = $arr2[$i]['time'];
          $temp_ins = new Carbon($temp_time_ins);
          $result = $first_time_in->diffInSeconds($temp_ins);
          if ($result >= 0 && $result <= 60) {
            unset($array[$i]);
          }
        }
        return $array;
    }

    /**
     * get the first element of the array as Time In
     * @param  array $array 
     * @return $time_in    time in array..    
     */
    public function getTimeIn($array) {
        $time_in = array_shift($array);
        return $time_in;
    } 

    /**
     * Subtract the time for checking if it's within the time
     * @param  time/string $first This is the first time to be subtracted
     * @param  time/string $next  This is the next time to be subtracted
     * @return $results this is the difference... 
     */
    public function getTheDifferenceInSeconds($first, $next) {
      $date_converted1 = new Carbon($first);
      $date_converted2 = new Carbon($next);
      $results = $date_converted1->diffInSeconds($date_converted2);

      return $results;
    }

    public function getWorkingScheduleListPerCompany(Request $request){
        $data = $request->all();

        $data['company_id'] = $data['company_id']; 

        $data = $this->model->getWorkingScheduleListPerCompany($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    public function getTotalNumberOfHours($time_in, $sched_time_in, $subtrahend) {
      // check if time in is greater than the schudle
      if ($time_in->greaterThan($sched_time_in)) {
        $total_number_of_hours = $time_in->diffInMinutes($subtrahend);
      } else {
        // if less than compute the total number of hours using schedule time in - time out
        $total_number_of_hours = $sched_time_in->diffInMinutes($subtrahend);
      }

      return $total_number_of_hours;
    }


    public function timeFormat($time) {

      if (\DateTime::createFromFormat('H:i', $time)) {
        $temp_time = \DateTime::createFromFormat('H:i', $time);
      } elseif (\DateTime::createFromFormat('H:i:s', $time)) {
        $temp_time = \DateTime::createFromFormat('H:i:s', $time);
      } elseif(\DateTime::createFromFormat('H:i a', $time)) {
        $temp_time = \DateTime::createFromFormat('H:i a', $time);
      } elseif(\DateTime::createFromFormat('H:i:s a', $time)) {
        $temp_time = \DateTime::createFromFormat('H:i:s a', $time);
      } elseif (\DateTime::createFromFormat('h:i', $time)) {
        $temp_time = \DateTime::createFromFormat('h:i', $time);
      } elseif(\DateTime::createFromFormat('h:i a', $time)) {
        $temp_time = \DateTime::createFromFormat('h:i a', $time);
      } elseif(\DateTime::createFromFormat('h:i:s a', $time)) {
        $temp_time = \DateTime::createFromFormat('h:i:s a', $time);
      } elseif (\DateTime::createFromFormat('h:i:s', $time)) {
        $temp_time = \DateTime::createFromFormat('h:i:s', $time);
      }

      return $temp_time;
    }


    public function dateFormat($date) {
      if (\DateTime::createFromFormat('m-d-Y H:i', $date)) {
        $temp_date = \DateTime::createFromFormat('m-d-Y H:i', $date);
      } elseif (\DateTime::createFromFormat('m-d-Y H:i:s', $date)) {
        $temp_date = \DateTime::createFromFormat('m-d-Y H:i:s', $date);
      } elseif (\DateTime::createFromFormat('m-d-Y H:i a', $date)) {
        $temp_date = \DateTime::createFromFormat('m-d-Y H:i a', $date);
      } elseif (\DateTime::createFromFormat('m-d-y H:i a', $date)) {
        $temp_date = \DateTime::createFromFormat('m-d-y H:i a', $date);
      } elseif (\DateTime::createFromFormat('d-m-Y H:i', $date)) {
        $temp_date = \DateTime::createFromFormat('d-m-Y H:i', $date);
      } elseif (\DateTime::createFromFormat('d-m-Y H:i:s', $date)) {
        $temp_date = \DateTime::createFromFormat('d-m-Y H:i:s', $date);
      } elseif (\DateTime::createFromFormat('d-m-Y H:i a', $date)) {
        $temp_date = \DateTime::createFromFormat('d-m-Y H:i a', $date);
      } elseif (\DateTime::createFromFormat('m/d/Y H:i', $date)) {
        $temp_date = \DateTime::createFromFormat('m/d/Y H:i', $date);
      } elseif (\DateTime::createFromFormat('m/d/Y H:i:s', $date)) {
        $temp_date = \DateTime::createFromFormat('m/d/Y H:i:s', $date);
      } elseif (\DateTime::createFromFormat('m/d/Y H:i a', $date)) {
        $temp_date = \DateTime::createFromFormat('m/d/Y H:i a', $date);
      } elseif (\DateTime::createFromFormat('d/m/Y H:i', $date)) {
        $temp_date = \DateTime::createFromFormat('d/m/Y H:i', $date);
      } elseif (\DateTime::createFromFormat('d/m/Y H:i:s', $date)) {
        $temp_date = \DateTime::createFromFormat('d/m/Y H:i:s', $date);
      } elseif (\DateTime::createFromFormat('d/m/Y H:i a', $date)) {
        $temp_date = \DateTime::createFromFormat('d/m/Y H:i a', $date);
      } elseif (\DateTime::createFromFormat('Y-m-d H:i', $date)) {
        $temp_date = \DateTime::createFromFormat('Y-m-d H:i', $date);
      } elseif (\DateTime::createFromFormat('Y-m-d H:i:s', $date)) {
        $temp_date = \DateTime::createFromFormat('Y-m-d H:i:s', $date);
      } elseif (\DateTime::createFromFormat('Y-m-d H:i a', $date)) {
        $temp_date = \DateTime::createFromFormat('Y-m-d H:i a', $date);
      } 

      return $temp_date;
    }



    public function totalNumberOfHours($time_in, $time_out, $sched_time_in, $sched_time_out, $lunch_in, $lunch_out){
      /* IF NO TIME OUT NUMBER OF HOURS WILL NOT BASE ON TIME OUT AS SUBTRAHEND */
      // check if time out exists
      if (isset($time_out)) {
        // IF IN > OUT 
        // AM yung out dito parang next day na
        if ($time_in->greaterThan($time_out)) {
          // minus 12 hours sa time in
          // add 12 hours naman sa out 
          // virtual binabaliktad yung oras para pag minanus di lumiit or mag negative
          $time_in_ = $time_in->subHours(12);
          $time_out_ = $time_out->addHours(12);
          $sched_time_in_ = $sched_time_in->subHours(12);
          $sched_time_out_ = $sched_time_out->subHours(12);
          if ($time_out_->lessThanOrEqualTo($sched_time_out_)) {
            $total_number_of_hours = $this->getTotalNumberOfHours($time_in_, $sched_time_in_, $time_out_);
          } else {
            $total_number_of_hours = $this->getTotalNumberOfHours($time_in_, $sched_time_in_, $sched_time_out_);
          }  
        } else {
          // check if time out is less than or equal to schedule time out
          if ($time_out->lessThanOrEqualTo($sched_time_out)) {
            $total_number_of_hours = $this->getTotalNumberOfHours($time_in, $sched_time_in, $time_out);
          } else {
            $total_number_of_hours = $this->getTotalNumberOfHours($time_in, $sched_time_in, $sched_time_out);
          }  
        }
      } else {
        if (isset($lunch_out)) {
          if ($time_in->greaterThan($lunch_out)) {
            $time_in_ = $time_in->subHours(12);
            $sched_time_in_ = $sched_time_in->subHours(12);
            $lunch_out_ = $lunch_out->addHours(12);
            $total_number_of_hours = $this->getTotalNumberOfHours($time_in_, $sched_time_in_, $lunch_out_);
          } else {
            $total_number_of_hours = $this->getTotalNumberOfHours($time_in, $sched_time_in, $lunch_out);
          }
        } else {
          if ($time_in->greaterThan($time_in)) {
            $time_in_ = $time_in->subHours(12);
            $sched_time_in_ = $sched_time_in->subHours(12);
            $lunch_in_ = $lunch_in->addHours(12);
            $total_number_of_hours = $this->getTotalNumberOfHours($time_in_, $sched_time_in_, $lunch_in_);
          } else {
            $total_number_of_hours = $this->getTotalNumberOfHours($time_in, $sched_time_in, $lunch_in);
          }
        }
      }

      return $total_number_of_hours;
    }

    public function getHalfdayHours($number_of_hours, $number_of_working_hours, $start_lunch, $end_lunch) {

      if ($number_of_hours <= 479 && $number_of_hours >= 420) {
            // 7 hours +
            $halfday = $this->setTotalNumberOfHours($number_of_hours, 7, 6, $number_of_working_hours, $start_lunch, $end_lunch);
        } elseif ($number_of_hours <= 419 && $number_of_hours >= 360) {
             // 6 hours 
            $halfday = $this->setTotalNumberOfHours($number_of_hours, 6, 5, $number_of_working_hours, $start_lunch, $end_lunch);
        } elseif ($number_of_hours <= 359 && $number_of_hours >= 300) {
            // 5 hours 
            $halfday = $this->setTotalNumberOfHours($number_of_hours, 5, 4, $number_of_working_hours, $start_lunch, $end_lunch);
        } elseif ($number_of_hours <= 299 && $number_of_hours >= 240) {
            // 4 hours
            $halfday = $this->setTotalNumberOfHours($number_of_hours, 4, 3, $number_of_working_hours, $start_lunch, $end_lunch);
        } elseif ($number_of_hours <= 239 && $number_of_hours >= 180) {
            // 3 HOURS
            $halfday = $this->setTotalNumberOfHours($number_of_hours, 3, 2, $number_of_working_hours, $start_lunch, $end_lunch);
        } elseif ($number_of_hours <= 179 && $number_of_hours >= 120) {
            // 2 hours
            $halfday = $this->setTotalNumberOfHours($number_of_hours, 2, 1, $number_of_working_hours, $start_lunch, $end_lunch);
            $undertime = 0;
        } elseif ($number_of_hours <= 119) {
            // 1 hour
            $halfday = 1;
        } else {
            $halfday = 0;
        }

        return $halfday;
    }


    public function setTotalNumberOfHours($hours, $val1, $val2, $number_hours, $start_lunch, $end_lunch) {
      if ($number_hours <= $hours) {
        $halfday = 0;
      } else {
        if ($start_lunch == NULL && $end_lunch == NULL) {
          $halfday = $val1;
        } else {
          $halfday = $val2;
        }
      }
      return $halfday;
    }

}


