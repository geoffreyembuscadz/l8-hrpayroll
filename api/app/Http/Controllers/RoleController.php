<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use App\Role;
use App\Permission;



class RoleController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'name', 'display_name', 'description' ];

	public $model = 'RoleModel';

	public $rules = [];

	public $table = 'roles';

    public function addShowData($data, $id = null){
        return $data;
    }

    // use getListById($id) from RoleModel
    public function show($id) {
        $data = [];

        $data['data'] = $this->model->getListById($id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }
    
    // use getList() from RoleModel
    public function getList(){
        $model = $this->model->getList();
        return $model;
    }

    public function preList($model){
        return $model;
    }

    // user postList function from RoleModel
    public function postList($model){
        $model = $this->model->postList($model);
        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

    // user preStore function from RoleModel
	public function postStore($id, $data = []){
        $model = $this->model->preStore($id, $data);
        return $model;
    }

    public function preUpdate($id, $data = []){
        return $data;
    }

    // user postUpdate function from RoleModel
    public function postUpdate($id, $data = []){
       $model = $this->model->postUpdate($id, $data);
       return $model;
    }

}


