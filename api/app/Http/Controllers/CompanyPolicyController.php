<?php
namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use App\Http\Models\CompanyPolicyModel;

class CompanyPolicyController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'name', 'created_at' ];

	public $model = 'CompanyPolicyModel';

	public $rules = [ 'name' => 'required' ];

	public $table = 'company_policy';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        $model = $this->model->joinCompanyPolicyType($model);

        return $model;
    }

    public function postList($model){
        $model = $this->model->functionSelect($model, $this->request);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }
}
