<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;

class AdjustmentTypeController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'name', 'created_at' ];

	public $model = 'AdjustmentTypeModel';

	public $rules = [];

	public $table = 'adjustment_type';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        return $model;
    }

    public function postList($model){
        $model = $this->model->postListJoin($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }

    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        $model = $this->model->preUpdate($id, $data);
        return $data;
    }


}
