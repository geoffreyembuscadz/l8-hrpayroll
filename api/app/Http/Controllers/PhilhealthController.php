<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;

class PhilhealthController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'created_at' ];

	public $model = 'PhilhealthModel';

	public $rules = [ ];

	public $table = 'philhealth';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function getList (){
        $model=$this->model->select('philhealth.bracket_id',
            'philhealth.salary_base',
            'philhealth.salary_ceiling',
            'philhealth.employee_share',
            'philhealth.employer_share',
            'philhealth.total_monthly_premium');
        return $model->get();
    }


}
