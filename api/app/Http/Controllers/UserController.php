<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CrudController;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;
use \DB AS DB;
use auth;


class UserController extends CrudController
{


	public $auth = false;

	public $list_columns = [ 'name', 'email', 'password' ];

	public $model = 'UserModel';

	public $rules = [ 'password' => 'sometimes' ];

	public $table = 'users';

    public function addShowData($data, $id = null){
        return $data;
    }

    // overwrite the postList and preList function using show() and getList()
    // show() for single view per id
    // getList() for all the users
    // use a function getListById($id) from UserModel   
    public function show($id) {
        $data = [];

        $data['data'] = $this->model->getListById($id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function getListByIdLimited() {

        $id = Auth::user()->id;

        $data = $this->model->getListByIdLimited($id);

        return $data;
    }

    public function getUserId($id) {

        $data = $this->model->getUserId($id);
        return $data;
    }

    public function getEmployeeBranch($id) {

        $data = $this->model->getEmployeeBranch($id);

        return $data;
    }

    public function getPermissionId() {

        $id = Auth::user()->id;

        $data['data'] = $this->model->getPermissionId($id);

        return $data;
    }

    public function getListByIdLimit() {

        $id = Auth::user()->id;

        $data['data'] = $this->model->getListByIdLimited($id);

        return $data;
    }
    
    public function getList(){
        $model = $this->model->getList();
        return $model;
    }

    // Make the password on hash before saving to the database
    // Hash::make is a default laravel Hash
	public function preStore($data = []){
        $data['data']['password'] = Hash::make($data['data']['passwords']['password']);
        
        return $data;
    }

    // Overwrite the make:crud default create using the postStore function from UserModel
	public function postStore($id, $data = []){
        $model = $this->model->postStore($id, $data);
        return $model;
    }

    // Overwrite the make:crud default edit using the preUpdate function from UserModel
    public function preUpdate($id, $data = []){
        $model = $this->model->preUpdate($id, $data);
        return $model;
    }
    // Overwrite the make:crud default edit using the postUpdate function from UserModel
    public function postUpdate($id, $data = []){
        $model = $this->model->postUpdate($id, $data);
        return $model;
    }  

    public function preDestroy($id){ 
        $model = $this->model->deleteUsersCompany($id);

        return $model;
    }

    // Change the user password
    public function changePassword(Request $request){
        $data = $request->all();
        $newPassword = $data['passwords']['password'];
        $currentPassword = $data['current_password'];
        $model = $this->model->changePassword($newPassword, $currentPassword);
        return $model;
    }


}
