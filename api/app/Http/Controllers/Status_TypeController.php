<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;

class Status_TypeController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id','created_at' ];

	public $model = 'Status_TypeModel';

	public $rules = [ ];

	public $table = 'status_type';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }
    public function getList (){
        $model=$this->model->select(
            'status_type.id',
            'status_type.name',
            'status_type.description' );
        return $model->get();

        
    }


}
