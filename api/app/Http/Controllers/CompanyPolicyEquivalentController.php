<?php
namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use App\Http\Models\CompanyPolicyEquivalentModel;

class CompanyPolicyEquivalentController extends CrudController{
	public $auth = false;

	public $model = 'CompanyPolicyEquivalentModel';

	public $rules = [ 'name' => 'required' ];

	public $table = 'company_policy_equivalent';
}
