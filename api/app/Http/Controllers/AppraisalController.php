<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use App\Http\Models\AppraisalModel;
use \Illuminate\Http\Request;

class AppraisalController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'created_at' ];

	public $model = 'AppraisalModel';

	public $rules = [];

	public $table = 'appraisal';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){

        return $model;
    }

    public function postList($model){
        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function appraisal(Request $request){
      $data = $request->all();
      
      $data = $this->model->appraisal($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }
    
    public function updateObj(Request $request){

        $data=($request->all());
        $duplication=$this->model->updateObj($data);
        return $data;
    }

    public function getEmployeeByAdmin(){
        $data = $this->model->getEmployeeByAdmin();

        $data =[ 'data' => $data ];
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    
    public function getObjNow(Request $request){
      $data = $request->all();
      
      $data = $this->model->getObjNow($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function rateObj(Request $request){
      $data = $request->all();
      
      $data = $this->model->rateObj($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function saveRateObj(Request $request){

      $data = $request->all();
      $return = $this->model->saveRateObj($data);
      $response=[ 'data' => $return ];
      return $response;
    }

    public function viewRate(Request $request){

      $data = $request->all();
      $return = $this->model->viewRate($data);
      $response=[ 'data' => $return ];
      return $response;
    }

    public function pendingAppraisal(Request $request){
      $data = $request->all();
      
      $data = $this->model->pendingAppraisal($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }






}
