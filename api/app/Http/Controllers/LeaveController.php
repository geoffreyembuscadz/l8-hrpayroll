<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use \Illuminate\Http\Request;
use App\Http\Models\LeaveModel;
use App\DB;


class LeaveController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id','created_at' ];

	public $model = 'LeaveModel';

	public $rules = [];

	public $table = 'leave';


    public function addShowData($data, $id = null){

        return $data;
    }

    public function preList($model){

        return $model;
    }


    public function postList($model){

        return $model;
    }

	

	public function preStore($data = []){
        

        return $data;
    }



	public function postStore($id, $data = []){


        return $data;
    }

    public function preUpdate($id, $data = []){
        
        return $data;
    }

    public function updateData(Request $request){

        $data=($request->all());
        $id = [];

        $type = $data["type"];
        $total = $data["total_days"];
        $leave_payment_option = $data['leave_payment_option'];
        array_push($id, $data["employee_id"]);


        $creditVal=$this->leaveCreditValidation($total,$type,$leave_payment_option,$id);

        if($creditVal != []){

            $response['data'] = array(
                            'message'      => 'Unprocessable Entity',
                            'status_code'  => 422,
                            'data'         => $creditVal,
                            'updateData'   => true,
                            'predata'      => $data

                        );
            return $response;
        }
        else{

            $data = $this->model->updateData($data);
        }

           
            
        return $data;
    }

    public function postUpdate($id, $data = []){

        return $data;
    }

    public function getList (){

        $model =  $this->model->getRecords();
        return $model;
    }

    public function show ($id){
        $data = [];
        $data['data'] = $this->model->show($id);
        $data = $this->addShowData($data, $id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function createLeave(Request $request){
        $data=[];
        $array=($request->all());
        $id = [];

        $len = count($array["employee_id"]);

        for ($i=0; $i < $len; $i++) { 

            array_push(
                $data,array(
                    'emp_id'        =>$array["employee_id"][$i], 
                    'type'          =>$array["type"], 
                    'total_days'    =>$array["total_days"], 
                    'total_with_pay'=>$array["total_with_pay"],
                    'leave_payment_option' => $array['leave_payment_option'],
                    'total_hours'   =>$array["total_hours"], 
                    'date_from'     =>$array["date_from"], 
                    'date_to'       =>$array["date_to"], 
                    'remarks'       =>$array["reason"],
                    'details'       =>$array["details"],
                    'attachment'    =>$array["attachment"]
                )
            );
            array_push($id, $array["employee_id"][$i]);
        }

        $type = $data[0]['type'];
        $total = $data[0]['total_days'];

        $creditVal=$this->leaveCreditValidation($total,$type,null,$id);

        if($creditVal != []){

            $response['data'] = array(
                            'message'      => 'Unprocessable Entity',
                            'status_code'  => 422,
                            'data'         => $creditVal,
                            'predata'      => $data
                        );
            return $response;
        }
        else{
            $return=$this->model->createLeave($data); 
        }

        $response=[
          'data' => $return
        ];
        return $response;
    }

    public function leaveCreditValidation($total,$type,$leave_payment = null,$id){

        $credit=$this->model->getCredit($type,$id);
        $len = count($credit);
        $invalid = [];
        $paid = 0;
        $unpaid = 0;
        for ($i=0; $i < $len ; $i++) { 
            $val = $credit[$i]->credit - $total;

            if($val < 0){

                if($credit[$i]->credit < $total){
                    $paid = $credit[$i]->credit;
                    $unpaid = $total - $paid;
                }
                else{
                    $paid = $total;
                }
                $invalid[] = array(
                    'id'       => $credit[$i]->id,
                    'name'     => $credit[$i]->name,
                    'credit'   => $credit[$i]->credit,
                    'paid'     => $paid,
                    'unpaid'   => $unpaid,
                    'total'    => $total
                );
            }
        }
        return $invalid;
    }

    public function getMyLeave(Request $request){
        $data = $request->all();

        $data['status_id'] = explode(",",$data['status_id']); 
        $data['type_id'] = explode(",",$data['type_id']); 

        $data =  $this->model->getMyLeave($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    public function getLeave(Request $request){
        $data = $request->all();

        $data['employee_id'] = explode(",",$data['employee_id']); 
        $data['company_id'] = explode(",",$data['company_id']); 
        $data['status_id'] = explode(",",$data['status_id']); 
        $data['type_id'] = explode(",",$data['type_id']); 

        $data = $this->model->getLeave($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    public function duplicateEntryValidation(Request $request){
        $data=($request->all());

        $duplication=$this->model->duplicateEntryValidation($data);

        if($duplication != []){

            $response['data'] = array(
                        'message'      => 'duplicate',
                        'status_code'  => 400,
                        'data'  => $duplication
                    );
            return $response;
        }
        else{
            $response['data'] = array(
                        'message'      => 'not duplicate'
                    );
            return $response;
        }

    }

    public function leaveCreate(Request $request){
        $data[]=($request->all());

        $return=$this->model->createLeave($data);

        $response=[
          'data' => $return
        ];
        return $response;
    }

    public function updateLeave(Request $request){
        $data=($request->all());

        $return = $this->model->updateData($data);

        $response=[
          'data' => $return
        ];
        return $response;
    }

    public function updateApprovedRequest($id,$created_by){
        $data = $this->model->approvedData($id,$created_by);

        return $data;
    }

    public function getWorkingSchedule(Request $request){

        $d = $request->all();

        $id = $d['emp_id'];
 
        $data  = $this->model->getWorkingSchedule($id);
        
        $data = [ 'data' => $data ];
        
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

}
