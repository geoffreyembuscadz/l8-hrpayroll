<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use App\Http\Models\AttendanceReportModel;
use \Illuminate\Http\Request;

class AttendanceReportController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'created_at' ];

	public $model = 'AttendanceReportModel';

	public $rules = [];

	public $table = 'attendancereport';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    // bar graph
    public function searchReport(Request $request, $first, $last, $step = '+1 day', $output_format = 'd/m/Y' ){
        $department = $request->get('department_id');
        $company = $request->get('company_id');
        $employee = $request->get('employee_id');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $total_employees_per_date = []; 
        // $start_date = '2017-04-01';
        // $end_date = '2017-04-12';
         $dates = array();
         $current = strtotime($first);
         $last = strtotime($last);
         $dataList = array();
        // loop ng date from start to end of selected (date )
         while( $current <= $last ){

            $presentData = AttendanceReportModel::getReport($company, $department, $employee,  $dates, 1);
            $absentData = AttendanceReportModel::getReport($company, $department, $employee,  $dates, 0);

            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
            $dataList[] = ['data' => $presentData, 'label' => 'Present'];
            $dataList[] = ['data' => $absentData, 'label' => 'Absent'];
        }
        
        $retVal = ['data' => $dataList];

        return response()->json($retVal)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }
    public function report(){
        $date = date('Y-m-d');
        $data = $this->model->report($date);

        $data =[ 'data' => $data ];
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }
    
    public function searchActive(){
        $department = 1;
        $employee =1;
        $company = 1;
        $data = AttendanceReportModel::getActive($department, $company, $employee);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

     public function searchTotalPresent(){
        $date = date('Y-m-d');        
        $data = AttendanceReportModel::getPresent($date);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function searchEmp($filter){
        $employee;
        $data = AttendanceReportModel::getTotalEmp($filter);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

     public function selectDate($start_date, $end_date){
        $start_date;
        $end_date;
 
        $data = AttendanceReportModel::selectedDate($start_date, $end_date);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

     public function getInactive(){
        $data = AttendanceReportModel::getInactive();
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function getPendingLeave(){
        $date = date('Y-m-d');
        $data = AttendanceReportModel::pendingLeave();
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }
    public function showEmpLate(){

        $date = date('Y-m-d');
        $data = AttendanceReportModel::showEmpLate($date);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function statusReport(Request $request){   
        $data = $request->all();   
        $data = $this->model->statusReport($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function getAbsnt(Request $request){
      $data = $request->all(); 
      // $data['start_date'] = '2017-04-18';
      // $data['end_date'] = '2017-08-30';

      $data = $this->model->getAbsnt($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function daysSummaryReport(Request $request){
      $data = $request->all();

      // $data['start_date'] = '2017-09-01';
      // $data['end_date'] = '2017-09-30';
      $data = $this->model->daysSummaryReport($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function hoursSummaryReport(Request $request){
      $data = $request->all();
      // $data['start_date'] = '2017-10-01';
      // $data['end_date'] = '2017-10-30';
      $data = $this->model->hoursSummaryReport($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function userReport(Request $request){
      $data = $request->all();
      // $data['start_date'] = '2017-09-01';
      // $data['end_date'] = '2017-09-30';
      // dd($data);
      $data = $this->model->userReport($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function perEmpRprt(Request $request){
     $data = $request->all();   
      // $data['start_date'] = '2017-07-30';
      // $data['end_date'] = '2017-08-30';
      
      $data = $this->model->perEmpRprt($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function lateErlyFltr(Request $request){
      $data = $request->all();  
      $data = $this->model->lateErlyFltr($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function getEmpByComp(Request $request){
      $data = $request->all();
      
      $data = $this->model->getEmpByComp($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }
    
    public function abs(Request $request){
      // $data = $request->all();

      // $data['start_date'] = '2017-09-01';
      // $data['end_date'] = '2017-09-30';
      $data = $this->model->abs($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function perEmpLateEarly(Request $request){
      $data = $request->all();
      $data = $this->model->perEmpLateEarly($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }
    
    public function perEmpDaysSummary(Request $request){
      $data = $request->all();
      
      $data = $this->model->perEmpDaysSummary($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function perEmpHoursSummary(Request $request){
      $data = $request->all();
      
      $data = $this->model->perEmpHoursSummary($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function lateErlyRprt(){
        $date = date('Y-m-d');
        $data = $this->model->lateErlyRprt($date);

        $data =[ 'data' => $data ];
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function working_schedule(Request $request){
      $data = $request->all();
      
      $data = $this->model->working_schedule($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }


    public function filterTotalEmployee(){
      
        $data = $this->model->filterTotalEmployee();

        $data =[ 'data' => $data ];
        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }




    

}
