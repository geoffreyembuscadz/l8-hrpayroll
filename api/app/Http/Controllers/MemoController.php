<?php

namespace App\Http\Controllers;
use App\Http\Models\MemoModel;
use App\Http\Controllers\CrudController;
use \Illuminate\Http\Request;

class MemoController extends CrudController{
    public $auth = false;

    public $list_columns = [ 'id', 'name', 'created_at' ];

    public $model = 'MemoModel';

    public $rules = [ ];

    public $table = 'memo';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        return $model;
    }

    public function postList($model){
        return $model;
    }

    public function preStore($data = []){

        return $data;
    }

    public function postStore($id, $data = []){

        return $data;

    }

    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function getMemoList(){

        $date = date('Y-m-d');
        $data = MemoModel::getMemoList($date);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
            'Access-Control-Allow-Origin' => '*', 
            'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
            ]);

    }

    public function getMemoFilter(Request $request){
        $data = $request->all();
        // $data['company_id'] = explode(",",$data['company_id']);
        $data = $this->model->filterMemo($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    public function getMemoType(){

        $date = date('Y-m-d');
        $data = MemoModel::getMemoType();
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
            'Access-Control-Allow-Origin' => '*', 
            'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
            ]);
    }

    public function getMemo(){
        $date = date('Y-m-d');
        $data = MemoModel::getMemo($date);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
            'Access-Control-Allow-Origin' => '*', 
            'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
            ]);

    }

    public function getEmpMemo(){

        $date = date('Y-m-d');
        $data = MemoModel::getEmpMemo();
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
            'Access-Control-Allow-Origin' => '*', 
            'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
            ]);

    }

    

    public function createMemo(Request $request){
        $data = [];
        $array = ($request->all());
        $j = count($array["comp_id"]); 

        for ($i=0; $i < $j; $i++) { 
            array_push($data, array($array["comp_id"][$i], 
                                    $array["name"], 
                                    $array["memo_type_id"], 
                                    $array["date"],
                                    $array["emp_id"],
                                    $array["created_by"]
                                    )
                      );
        }

        $return = $this->model->createMemo($data);
        $response = [ 'data' => $return ];
        return $response;
    }

    
}
