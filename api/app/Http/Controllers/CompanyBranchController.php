<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use App\Http\Controllers\CrudController;
use App\Http\Models\CompanyBranchModel;

class CompanyBranchController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'company_branch.id', 'company_branch.company_id', 'company_branch.branch_name', 'company_branch.branch_address', 'company_branch.contact_number', 'company_branch.contact_person' ];

	public $model = 'CompanyBranchModel';

	public $rules = [ 'name' => 'required' ];

	public $table = 'company_branch';

    public function addShowData($data, $id = null){
        if(!empty($id)){
            // $data['data']['company'] = $this->model->getCompanyPolicies($id);            
        }
        return $data;
    }

    public function preList($model){
        if(!empty($this->request['company_id'])){
            $model = $model->where('company_branch.company_id', $this->request['company_id']);
        }

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    // get branches with company names
    public function getCompanyBranches() {
        
        $return = [];
        
        $errors = [];

        $return['data'] = $this->model->getCompanyBranches();
        
        return $errors ? $errors : response()->json($return)->withHeaders([
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    // multiple update of company branches
    public function updateMultiple(Request $request, $company_id){
        $input = $request->all();
        $data = [];
        $errors = [];
        $branches = [];

        foreach($input AS $key_index => $input_val){
            $key_index = str_replace(']', '', $key_index);
            $index_pieces = explode('[', $key_index);

            if($index_pieces[0] == 'company_branch'){
                $branches[$index_pieces[1]][$index_pieces[2]] = $input_val;
            }
        }

        $return = [];
        $data['company_branch'] = $branches;
        
        $return['data'] = $this->model->updateCompanyBranch($company_id, $data);

        return $errors ? $errors : response()->json($return)->withHeaders([
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
            'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }
}

//Baeight #faqDisSheet
