<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use App\Http\Models\EvaluationModel;
use App\Http\Controllers\CrudController;

class EvaluationController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'created_at' ];

	public $model = 'EvaluationModel';

	public $rules = [];

	public $table = 'evaluation';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function segment(){

        $data = EvaluationModel::segment();
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function getEmpByStatus(Request $request){
      $data = $request->all();
      $data = $this->model->getEmpByStatus($data);
      $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function employmentStatus(){

        $data = EvaluationModel::employmentStatus();
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function addSegment(){

        $data = EvaluationModel::addSegment();
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function addCategory(){
       $data = EvaluationModel::addCategory();
       $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function addSubCategory(){
       $data = EvaluationModel::addSubCategory();
       $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function getEmployeePosition(){
       $data = EvaluationModel::getEmployeePosition();
       $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function postEvaluationTemplate(Request $request){

      $data = $request->all();
      $return = $this->model->postEvaluationTemplate($data);
      $response=[ 'data' => $return ];
      return $response;
    }

    public function getEvaluation(Request $request){

      $data = $request->all();
      $return = $this->model->getEvaluation($data);
      $response=[ 'data' => $return ];
      return $response;
    }

    public function getEvaluationTitle(Request $request){

      $data = $request->all();
      $return = $this->model->getEvaluationTitle($data);
      $response=[ 'data' => $return ];
      return $response;
    }

    public function getEmployeeByPosition(Request $request){

      $data = $request->all();
      $return = $this->model->getEmployeeByPosition($data);
      $response=[ 'data' => $return ];
      return $response;
    }


    public function postEvaluationForm(Request $request){

      $data = $request->all();
      $return = $this->model->postEvaluationForm($data);
      $response=[ 'data' => $return ];
      return $response;
    }

    public function getEmployeeEvaluation(Request $request){
       
      $data = $request->all();
      $return = $this->model->getEmployeeEvaluation($data);
      $response=[ 'data' => $return ];
      return $response;
    }

    public function postEvaluationAnswer(Request $request){

      $data = $request->all();
      $return = $this->model->postEvaluationAnswer($data);
      $response=[ 'data' => $return ];
      return $response;
    }

    public function showAllEmployee(){
       $data = EvaluationModel::showAllEmployee();
       $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function showEmployeeRate(){
       $data = EvaluationModel::showEmployeeRate();
       $data = [ 'data' => $data ];

      return response()->json($data)->withHeaders([
        'Access-Control-Allow-Origin' => '*', 
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
      ]);
    }

    public function filterEmployee(Request $request){

      $data = $request->all();
      $return = $this->model->filterEmployee($data);
      $response=[ 'data' => $return ];
      return $response;
    }

    public function viewEmployeeEvaluation(Request $request){

      $data = $request->all();
      $return = $this->model->viewEmployeeEvaluation($data);
      $response=[ 'data' => $return ];
      return $response;
    }

     public function getEmployee(Request $request){

      $data = $request->all();
      $return = $this->model->getEmployee($data);
      $response=[ 'data' => $return ];
      return $response;
    }

}


