<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use ThrottleLogins;
    
    //use protected property
    protected $maxLoginAttempts = 3; // number of bad attempts user can make
     
    //OR use protected method
    protected function maxLoginAttempts()
    {
        return 1; //int this sample one bad trial allowed
    }

    //use protected property
    protected $lockoutTime = 2; // period for which user is going to be blocked in minutes
     
    //OR use protected method
    protected function lockoutTime()
    {
        return 3; //in this sample: 3 minutes cool-down
    }

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
}
