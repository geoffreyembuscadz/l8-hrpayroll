<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use \App\Http\Models\PayrollListDataModel as PayrollList;
use App\Http\Models\PayrollListDataModel as PayrollDetails;

use Carbon\Carbon as Carbon;
use \DB as DB;

class StatementOfAccountController extends Controller
{
     public function exportSOA(Request $request) {
      $payroll_id = $request->input('payroll_id');
    
      $payroll_details = DB::table('payroll_list')->select(
        DB::raw('SUM(total_billable) AS total_billable'), 
        DB::raw("(SELECT SUM(total_sss) + SUM(total_pagibig) + SUM(total_philhealth)) AS total_mandatory_ee"), 
        DB::raw("(SELECT SUM(total_sss_employer) + SUM(total_pagibig_employer) + SUM(total_philhealth_employer)) AS total_mandatory_er"),
        DB::raw('SUM(gross_pay) AS gross_pay')

        )->where('payroll_id', $payroll_id)->first();


      $data = array(
        'total_billable'     => $payroll_details->total_billable,
        'total_mandatory_ee' => $payroll_details->total_mandatory_ee,
        'total_mandatory_er' => $payroll_details->total_mandatory_er,      
        'gross_pay'          => $payroll_details->gross_pay,
        'date'               => Carbon::now()
      );


      $payroll_list = new PayrollList;     
      $payroll_list = DB::table('payroll')
                      ->select('users.name as prepared_by', 'branch', 'start_time', 'end_time', 'company.name', 'company.address', 'company.phone_number', 'company.billing_rate', 'company.billing_13th', 'company.billing_vat' )
                      ->leftjoin('company', 'payroll.company', '=', 'company.id')     
                      ->join('users', 'payroll.prepared_by', '=', 'users.id')     
                      ->where('payroll.id', '=', $payroll_id)
                      ->whereNull('payroll.deleted_at')
                      ->get();

      PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif'])->setPaper('a4');

      $pdf = PDF::loadView('export.statement_of_account', ['payroll_list' => $payroll_list],  array('data' => $data));
      return $pdf->stream('statement_of_account.pdf');
    }
}
