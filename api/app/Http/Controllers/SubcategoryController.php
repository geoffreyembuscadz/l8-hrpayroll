<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use App\Http\Models\SubcategoryModel;
use \Illuminate\Http\Request;

class SubcategoryController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'created_at' ];

	public $model = 'SubcategoryModel';

	public $rules = [ ];

	public $table = 'subcategory';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function getSubCat(){

        $data = SubcategoryModel::getSubCat();
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

    public function postSubCategory(Request $request){
      $data = $request->all();
      $return = $this->model->postSubCategory($data);
      $response=['data' => $return];
      return $response;
    }

    public function show ($id){
        $data = [];
        $data['data'] = $this->model->show($id);
        $data = $this->addShowData($data, $id);

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);
    }

}
