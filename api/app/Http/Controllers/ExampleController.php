<?php

namespace App\Http\Controllers;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    public function message(){
      return json_encode([
		'message' => 'Hi This is Lumen HRIS and Payroll API.'
	  ]);
    }

    public function example(){
       $return = [
			'data' => [
				[ 'id' => 1, 'employee_num' => '001', 'firstname' => 'Geoffrey', 'lastname' => 'Embuscado', 'department' => 'IT Department', 'position' => 'Quality Assurance', 'status' => 'Active' ]
			]
		];
		
		return json_encode($return);
    }
}
