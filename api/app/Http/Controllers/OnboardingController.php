<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CrudController;
use \Illuminate\Http\Request;

class OnboardingController extends CrudController{
	public $auth = false;

	public $list_columns = [ 'id', 'name', 'created_at' ];

	public $model = 'OnboardingModel';

	public $rules = [];

	public $table = 'onboarding';

    public function addShowData($data, $id = null){
        return $data;
    }

    public function preList($model){
        // $model = $this->model->functionJoinAndWhere($model);

        return $model;
    }

    public function postList($model){
        // $model = $this->model->functionSelect($model);

        return $model;
    }

	public function preStore($data = []){
        return $data;
    }

	public function postStore($id, $data = []){
        return $data;
    }
    // -- /Store Data --//

    // -- Update Data --//
    public function preUpdate($id, $data = []){
        return $data;
    }

    public function postUpdate($id, $data = []){
        return $data;
    }

    public function getOnboardingList(Request $request){
        $data = $request->all();

        // $data['status_id'] = explode(",",$data['status_id']); 
        // $data['type_id'] = explode(",",$data['type_id']); 

        $data =  $this->model->getOnboardingList($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    public function updateStatus(Request $request){

        $data=($request->all());

        $return =  $this->model->updateStatus($data);

        $response=[
          'data' => $return
        ];
        return $response;

    }

    public function getMyOnboardingList(Request $request){
        $data = $request->all();

        $data =  $this->model->getMyOnboardingList($data);
        $data = [ 'data' => $data ];

        return response()->json($data)->withHeaders([
          'Access-Control-Allow-Origin' => '*', 
          'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
          'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
        ]);

    }

    public function updateMyStatus(Request $request){

        $data=($request->all());

        $return =  $this->model->updateMyStatus($data);

        $response=[
          'data' => $return
        ];
        return $response;

    }
}
