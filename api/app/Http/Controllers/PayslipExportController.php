<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

use Barryvdh\DomPDF\Facade as PDF;

use App\Mail\Payslip;

use Carbon\Carbon as Carbon;
use DB;

use App\Http\Models\PayslipExportModel as PayslipExport;


class PayslipExportController extends Controller
{   
    public $model = 'PayslipExportModel';

    public function exportPayslip(Request $request) {
      
        $id             = $request->input('id');
      	$emp_id         = $request->input('emp_id');
        $branch_input   = $request->input('branch');
        $company_input  = $request->input('company');

        $loans          = new PayslipExport; 
        $payroll_period = new PayslipExport;        
        $adjustment     = new PayslipExport;        
        $payrollList    = new PayslipExport;        

        $loans          = $loans->getLoan($id);
        $payroll_period = $payroll_period->getPayroll($id);
        $adjustment     = $adjustment->getAdjustment($id);
        $payrollList    = $payrollList->getPayrollList($id, $emp_id);

        $data = array(
          'date'    => Carbon::now(),
          'branch'  => $branch_input,
          'company' => $company_input,
        );
               
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif'])->setPaper('a4');

      	$pdf = PDF::loadView('export.payslip', 
          [ 

            'loans'           => $loans,
            'payroll_period'  => $payroll_period, 
            'adjustment'      => $adjustment, 
            'payrollList'     => $payrollList, 

          ],  array( 'data' => $data )
        );

      	return $pdf->stream('payslip.pdf');
    }


    /**
     * Ship the given order.
     *
     * @param  Request  $request
     * @param  int  $orderId
     * @return Response
     */ 
    public function sendPayslip(Request $request)
    {
        $id = $request->input('id');

        $payroll = new PayslipExport;

        $payroll = $payroll->sendPayslip($id);

        foreach ($payroll as $pay) {
            Mail::to($pay->email)->queue(new Payslip($pay));
        }
    }
}
