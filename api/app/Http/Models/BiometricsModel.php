<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\UserController;

class BiometricsModel extends Model
{
    protected $table = "biometrics_device"; use SoftDeletes;

    public function getUser(){
        $contr = new UserController;
        $data = $contr->getListByIdLimited();

        return $data;
    }

    public function getUserId($id){
        $contr = new UserController;
        $data = $contr->getUserId($id);

        return $data;
    }

    public function getList(){

        return self::join('company_branch', 'biometrics_device.branch_id', '=', 'company_branch.id')
        ->select(
            DB::raw('company_branch.branch_name as location'),
            'biometrics_device.id', 
            'biometrics_device.name', 
            'biometrics_device.branch_id', 
            'biometrics_device.ip_address')
        ->orderBy('biometrics_device.created_at', 'desc')
        ->get();

    }

    public function show($id){

        return self::join('company_branch', 'biometrics_device.branch_id', '=', 'company_branch.id')
        ->join('company', 'company_branch.company_id', '=', 'company.id')
        ->select(
            DB::raw('company_branch.branch_name as location'),
            'company.id as company_id', 
            'biometrics_device.id', 
            'biometrics_device.name', 
            'biometrics_device.branch_id', 
            'biometrics_device.ip_address')
        ->where('biometrics_device.id','=',$id)
        ->first();
    }
}
