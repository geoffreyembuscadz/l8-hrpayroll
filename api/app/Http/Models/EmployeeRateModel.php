<?php
namespace App\Http\Models;

use \DB; 
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\UserModel;

use Illuminate\Support\Facades\Hash;

class EmployeeRateModel extends Model
{
    protected $table = "employee_rate"; use SoftDeletes;
}
