<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use \stdClass;

class GoalModel extends Model
{
    protected $table = "goal"; use SoftDeletes;

    public function showGoal($data){
    	$employee_id = $data['employee_id'];

    	$query = DB::table('goal')
    			 ->select('id', 'goal', 'time_frame',
    			 	DB::raw('CAST(date_started As date) AS date_started'))
    			 ->where('employee_id', '=', $employee_id)
    			 ->where('deleted_at', '=', NULL)
    			 ->get();

    	return $query;
    }
}
