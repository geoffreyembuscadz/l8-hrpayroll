<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class BillingSettingsModel extends Model
{
    protected $table = "billingsettings"; use SoftDeletes;
}
