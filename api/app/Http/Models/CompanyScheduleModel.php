<?php
namespace App\Http\Models;

use \DB as DB;
use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyScheduleModel extends Model
{
    protected $table = "working_schedule"; use SoftDeletes;

    public function formatTimeOnSelect($model){
    	return $model->select('working_schedule.id', 'working_schedule.company_id', 'working_schedule.name', 'working_schedule.flexible_time', 'working_schedule.grace_period_mins', DB::raw('date_format(time_in, "%h:%i %p") as time_in, date_format(time_out, "%h:%i %p") as time_out'), DB::raw('date_format(working_schedule.lunch_break_time, "%h:%i %p") AS lunch_break_time'), DB::raw('working_schedule.lunch_mins_break AS lunch_mins_break'), 'working_schedule.days');
    }
}
