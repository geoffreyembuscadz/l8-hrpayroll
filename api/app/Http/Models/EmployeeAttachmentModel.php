<?php
namespace App\Http\Models;

use \DB; use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeAttachmentModel extends Model
{
    protected $table = "employee_attachment"; use SoftDeletes;

    public function whereEmployee($model, $employee_id){
        return $model->where('employee_id', $employee_id);
    }
}
