<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\ItemModel;
use Carbon\Carbon;
use App\Http\Controllers\UserController;

class LeaveModel extends Model
{
    protected $table = "leave"; use SoftDeletes;

    public function getUser(){
        $contr = new UserController;
        $data = $contr->getListByIdLimited();

        return $data;
    }

    public function getUserId($id){
        $contr = new UserController;
        $data = $contr->getUserId($id);

        return $data;
    }


    public static function getRecords(){

        return self::join('status_type', 'leave.status_id', '=', 'status_type.id')
        ->join('employee', 'leave.emp_id', '=', 'employee.id')
        ->join('leave_type', 'leave.leave_type_id', '=', 'leave_type.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('leave_type.name AS type'), 
            DB::raw('leave.remarks AS reason'), 
            'leave.final_remarks', 
            'leave.id', 
            'leave.date_applied',
            'leave.total_days', 
            'leave.total_hours', 
            'leave.emp_id', 
            'leave.date_from', 
            'leave.date_to',
            'leave.remarks',
            'leave.leave_type_id')
        ->orderBy('leave.created_at', 'desc')
        ->get();

    }

    public static function show($id){

        $data = DB::table('leave_request')
                ->join('leave', 'leave_request.leave_id', '=', 'leave.id')
                ->join('status_type', 'leave.status_id', '=', 'status_type.id')
                ->join('employee', 'leave.emp_id', '=', 'employee.id')
                ->join('leave_type', 'leave.leave_type_id', '=', 'leave_type.id')
                ->select(
                    DB::raw('leave_request.id AS request_id'),
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('leave_type.name AS type'), 
                    DB::raw('leave.remarks AS reason'), 
                    DB::raw('status_type.name AS status'),
                    'leave_request.date',
                    'leave_request.time_type',
                    'leave_request.length_hours',
                    'leave_request.length_days',
                    'leave.id',
                    'leave.date_applied',
                    'leave.total_days', 
                    DB::raw('\'1\' as with_pay'),
                    'leave.total_hours', 
                    'leave.total_with_pay', 
                    'leave.emp_id', 
                    'leave.date_from', 
                    'leave.date_to',
                    'leave.leave_type_id',
                    'leave.final_remarks',
                    'leave.attachment')
                ->where('leave_request.leave_id','=' ,$id)
                ->get();
        return $data;

    }

    public function createLeave($data){

        $notif=[];
        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        foreach ($data as $counter => $d) {

            $leave_id = DB::table('leave')->insertGetId(
                array(
                    'status_id'     => 1,
                    'emp_id'        => $d['emp_id'],
                    'leave_type_id' => $d['type'],
                    'total_days'    => $d['total_days'],
                    'total_with_pay'=> ($d['leave_payment_option'] == 1) ? $d['total_with_pay'] : $d['leave_payment_option'],
                    'total_hours'   => $d['total_hours'],
                    'date_from'     => $d['date_from'],
                    'date_to'       => $d['date_to'],
                    'remarks'       => $d['remarks'],
                    'attachment'    => $d['attachment'],
                    'created_by'    => $user->user_id,
                    'created_at'    => $date,
                    'date_applied'  => $date
                    )
                );


            foreach ($d['details'] as $da) {
                DB::table('leave_request')->insert(
                    array(
                        'leave_id'          => $leave_id,
                        'date'              => $da['date'],
                        'time_type'         => $da['time_type'],
                        'length_hours'      => $da['length_hours'],
                        'with_pay'          => ($d['leave_payment_option'] == 1) ? $da['with_pay'] : ($d['leave_payment_option'] == 1),
                        'length_days'       => $da['length_days'],
                        'created_by'        => $user->user_id,
                        'created_at'        => $date
                        )
                    );
            }
            $messenger_id = null;
            $messenger_id = $this->getUserId($d['emp_id']);

            if ($messenger_id != null) {
                $notif_id=DB::table('notification')->insertGetId(
                    array(
                        'title'          => 'Leave Request',
                        'description'    => $d['remarks'],
                        'messenger_id'   => $messenger_id->user_id,
                        'table_from'     => 'leave',
                        'table_from_id'  => $leave_id,
                        'seen'           => 0,
                        'route'          => 'leave-list',
                        'receiver_id'    => $user->supervisor_id,
                        'icon'           => 'fa fa-pencil-square-o text-aqua',
                        'created_by'     => $user->user_id,
                        'created_at'     => $date
                        )
                );
                
                $notif[$counter]= $notif_id;
            }
        }

    return $notif;

    }

    public function getMyLeave($data){

        $user = $this->getUser();

        $employee_id = $user->employee_id;
        $status_id = $data['status_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];
        $type_id = $data['type_id'];

        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('leave')
        ->join('status_type', 'leave.status_id', '=', 'status_type.id')
        ->join('employee', 'leave.emp_id', '=', 'employee.id')
        ->join('leave_type', 'leave.leave_type_id', '=', 'leave_type.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('leave_type.name AS type'), 
            DB::raw('SUBSTRING(leave.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(leave.final_remarks, 1, 15) as final_remarks'), 
            'leave.id', 
            'leave.date_applied',
            'leave.total_days', 
            'leave.total_hours', 
            'leave.emp_id', 
            'leave.date_from', 
            'leave.date_to',
            'leave.leave_type_id')
        ->where('leave.emp_id','=',$employee_id)
        ->where('leave.deleted_at','=',null)
        ->where('leave.deleted_by','=',null)
        ->orderBy('leave.created_at', 'desc');

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('leave.date_applied', [$start_date,$end_date]);
        }

        if($status_id[0] != 'null') {
            $stat = [];
            $len = count($status_id);
            for ($x=0; $x < $len ; $x++) { 
                $stat[$x]= (int)$status_id[$x];
            }
            $filter->whereIn('leave.status_id',$stat);
        }
        
        if($type_id[0] != 'null') {
            $type = [];
            $len = count($type_id);
            for ($x=0; $x < $len ; $x++) { 
                $type[$x]= (int)$type_id[$x];
            }
            $filter->whereIn('leave.leave_type_id',$type);
        }

        $data = $filter->get();
        return $data;

    }

    public function updateSave($id,$d,$user){

        $date = date('Y-m-d H:i:s');

        DB::table('leave')->where('id',$id)->update([
            'leave_type_id' => $d['type'],
            'emp_id'        => $d['employee_id'][0],
            'total_days'    => $d['total_days'],
            'total_hours'   => $d['total_hours'],
            'date_from'     => $d['date_from'],
            'date_to'       => $d['date_to'],
            'attachment'    => $d['attachment'],
            'remarks'       => $d['reason'],
            'updated_by'    => $user->user_id,
            'updated_at'    => $date
        ]);


        foreach ($d['details'] as $da) {
            DB::table('leave_request')->insert(
                array(
                    'leave_id'          => $id,
                    'date'              => $da['date'],
                    'time_type'         => $da['time_type'],
                    'length_hours'      => $da['length_hours'],
                    'length_days'       => $da['length_days'],
                    'created_by'        => $user->user_id,
                    'created_at'        => $date
                    )
            );
        }
    }

    public function updateData($file){
        // return $file;

        $d = $file;
        $user = $this->getUser();
        $date = date('Y-m-d H:i:s');
        $id = $d['id'];
        $data = [];
        return $d['leave_payment_option'];

        if($d['details'][0]['request_id'] == 0){
            DB::table('leave_request')
            ->where('leave_id', $id)->delete();

            $data = $this->updateSave($id,$d,$user);
        }
        elseif($d['details'][0]['request_id'] != 0){
            DB::table('leave')->where('id',$id)->update([
                        'leave_type_id' => $d['type'],
                        'emp_id'        => $d['employee_id'][0],
                        'total_days'    => $d['total_days'],
                        'total_hours'   => $d['total_hours'],
                        'date_from'     => $d['date_from'],
                        'date_to'       => $d['date_to'],
                        'attachment'    => $d['attachment'],
                        'remarks'       => $d['reason'],
                        'updated_by'    => $user->user_id,
                        'updated_at'    => $date
            ]);

            $dat = DB::table('leave_request')
            ->where('leave_id', $id)->get();
            
            if (count($dat) != count($d['details'])) {
                $len = count($d['details']);
                $temp = [];
                for ($x=0; $x < $len ; $x++) { 
                    $temp[$x]= $d['details'][$x]['date'];
                }

                DB::table('leave_request')
                ->where('leave_id', $id)
                ->whereNotIn('date', $temp)
                ->delete();
            }


            foreach ($d['details'] as $key => $da) {
                $request_id = $da['request_id'];
                DB::table('leave_request')
                ->where('id',$request_id)
                ->update([
                    'date'          => $da['date'],
                    'time_type'     => $da['time_type'],
                    'length_hours'  => $da['length_hours'],
                    'length_days'   => $da['length_days'],
                    'updated_by'    => $user->user_id,
                    'updated_at'    => $date
                ]);
            }

            if($d['leave_payment_option'] == 0){
                DB::table('leave')->where('id', $id)->update([
                    'total_with_pay'    => $d['leave_payment_option'],
                    'updated_at'        => date('Y-m-d H:i:s'),
                    'updated_by'        => $user->user_id
                ]);

                DB::table('leave_request')->where('leave_id', $id)->update([
                    'with_pay' => $d['leave_payment_option'],
                    'updated_at'        => date('Y-m-d H:i:s'),
                    'updated_by'        => $user->user_id
                ]);

            }
            
        }
        $table=DB::table('leave')
            ->where('id','=',$id)
            ->select()
            ->get();

        return $table;
    }

    public function getSchedule($emp_id,$days){

         $data = DB::table('employee_working_schedule')
         ->join('working_schedule', 'employee_working_schedule.working_schedule_id', '=', 'working_schedule.id')
         ->select(
            'working_schedule.id', 
            'working_schedule.time_in', 
            'working_schedule.time_out', 
            'working_schedule.days',
            'working_schedule.company_id',
            'working_schedule.grace_period_mins')
        ->where('employee_working_schedule.employee_id','=',$emp_id)
        ->where('working_schedule.days', 'like', '%' . $days . '%')
        ->first();

        if($data == null ){
            $data = $this->getDefaultSchedule($emp_id);
        }
        return $data;
    }

    public function getDefaultSchedule($id){

        $data = DB::table('employee_working_schedule')
         ->join('working_schedule', 'employee_working_schedule.working_schedule_id', '=', 'working_schedule.id')
         ->join('employee_company', 'employee_working_schedule.employee_id', '=', 'employee_company.employee_id')
         ->join('company_branch', 'employee_company.branch_id', '=', 'company_branch.id')
         ->select(
            'working_schedule.id', 
            'working_schedule.time_in', 
            'working_schedule.time_out', 
            'working_schedule.days',
            'working_schedule.company_id',
            'working_schedule.grace_period_mins',
            'working_schedule.lunch_break_time',
            'working_schedule.lunch_mins_break',
            'employee_company.branch_id',
            'company_branch.branch_name')
        ->where('employee_working_schedule.employee_id','=',$id)
        ->first();

        return $data;
    }


    public static function show2($id){

        $data = DB::table('leave_request')
                ->join('leave', 'leave_request.leave_id', '=', 'leave.id')
                ->select(
                    DB::raw('\'\' AS day'),
                    'leave.emp_id',
                    'leave.total_days',
                    'leave.leave_type_id',
                    'leave_request.date',
                    'leave_request.time_type',
                    'leave_request.length_hours',
                    'leave_request.length_days')
                ->where('leave_request.leave_id','=' ,$id)
                ->where('leave_request.with_pay','=' ,1)
                ->get();

        foreach ($data as $key => $d) {
            $dt = strtotime($d->date);
            $e = date("l", $dt);
            $d->day=strtolower($e);
        }

        $len = count($data);

        for ($i=0; $i <$len ; $i++) { 
            $dt = strtotime($data[$i]->date);
            $e = date("l", $dt);
            $data[$i]->day=strtolower($e);
        }

        return $data;

    }

    public function approvedData($id,$created_by){

        //check nito lung ilan ang bayad na leave
        $leaveData = $this->show2($id);

        //if leaveData is 0 means walang bayad na leave kay return na sya
        if (count($leaveData) == 0) {
            return $leaveData;
        }

        $emp_id = $leaveData[0]->emp_id;
        $total_days = $leaveData[0]->total_days;
        $leave_type = $leaveData[0]->leave_type_id;
        $date = date('Y-m-d H:i:s');
        $type = 4;

        $location = $this->getBranch($emp_id);

        //nakadepende sa length nito kung ilan ang bayad na leave
        $len = count($leaveData);

        for ($i=0; $i < $len; $i++) { 
            
            $sched = $this->getSchedule($leaveData[$i]->emp_id,$leaveData[$i]->day);

            $d = $leaveData[$i]->date;

            if($leaveData[$i]->time_type == 'First half'){
                $time_in = $sched->time_in;
                $in = Carbon::parse($time_in);
                $num = $in->addHours(5);
                $time_out = substr($num, 11);
                $halfday = 1;
            }
            else if($leaveData[$i]->time_type == 'Second half'){
                $time_out = $sched->time_out;
                $out = Carbon::parse($time_out);
                $num = $out->subHours(5);
                $time_in = substr($num, 11);
                $halfday = 1;
            }
            else{
                $time_in = $sched->time_in;
                $time_out = $sched->time_out;
                $halfday = 0;

            }

            DB::table('employees_logs')->insert(
                array(
                    'employee_id'        => $emp_id,
                    'attendance_type_id' => $type,
                    'branch_id'          => $location->branch_id,
                    'time_in'            => $d . ' ' . $time_in,
                    'time_out'           => $d . ' ' . $time_out,
                    'undertime'          => 0,
                    'halfday'            => $halfday,
                    'created_by'         => $created_by,
                    'created_at'         => $date
                    )
            );
        }

            //babawasan na yung leave nya sa employee table
            if ($leave_type == 1) {
                $emp=DB::table('employee')
                ->where('id','=',$emp_id)
                ->select('use_sick_leave_credit as credit')
                ->first();
                $total = $emp->credit + $total_days;
                DB::table('employee')->where('id',$emp_id)->update([
                            'use_sick_leave_credit' => $total,
                            'updated_by'            => $created_by,
                            'updated_at'            => $date
                ]); 
            }
            else if ($leave_type == 2) {
                $emp=DB::table('employee')
                ->where('id','=',$emp_id)
                ->select('use_vacation_leave_credit as credit')
                ->first();
                $total = $emp->credit + $total_days;
                DB::table('employee')->where('id',$emp_id)->update([
                            'use_vacation_leave_credit' => $total,
                            'updated_by'                => $created_by,
                            'updated_at'                => $date
                ]); 
            }
            else if ($leave_type == 3) {
                $emp=DB::table('employee')
                ->where('id','=',$emp_id)
                ->select('use_emergency_leave_credit as credit')
                ->first();
                $total = $emp->credit + $total_days;
                DB::table('employee')->where('id',$emp_id)->update([
                            'use_emergency_leave_credit' => $total,
                            'updated_by'                 => $created_by,
                            'updated_at'                 => $date
                ]); 
            }
            else if ($leave_type == 4) {
                $emp=DB::table('employee')
                ->where('id','=',$emp_id)
                ->select('use_maternity_leave_credit as credit')
                ->first();
                $total = $emp->credit + $total_days;
                DB::table('employee')->where('id',$emp_id)->update([
                            'use_maternity_leave_credit' => $total,
                            'updated_by'                 => $created_by,
                            'updated_at'                 => $date
                ]); 
            }
            else{
                $emp=DB::table('employee')
                ->where('id','=',$emp_id)
                ->select('use_others_leave_credit as credit')
                ->first();
                $total = $emp->credit + $total_days;
                DB::table('employee')->where('id',$emp_id)->update([
                            'use_others_leave_credit'   => $total,
                            'updated_by'                => $created_by,
                            'updated_at'                => $date
                ]); 
            }

            $table=DB::table('leave')
            ->where('id','=',$id)
            ->select()
            ->get();
        
        return $table;
    }

    public function getLeave($data){

        $user = $this->getUser();

        $supervisor_id = $user->user_id;
        $role_id = $user->role_id;

        $employee_id = $data['employee_id'];
        $company_id = $data['company_id'];
        $status_id = $data['status_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];
        $type_id = $data['type_id'];

        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('leave')
        ->join('status_type', 'leave.status_id', '=', 'status_type.id')
        ->join('employee', 'leave.emp_id', '=', 'employee.id')
        ->join('leave_type', 'leave.leave_type_id', '=', 'leave_type.id')
        ->join('employee_company', 'leave.emp_id', '=', 'employee_company.employee_id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('leave_type.name AS type'), 
            DB::raw('SUBSTRING(leave.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(leave.final_remarks, 1, 15) as final_remarks'), 
            'leave.id', 
            'leave.date_applied',
            'leave.total_days', 
            'leave.total_hours', 
            'leave.emp_id', 
            'leave.date_from', 
            'leave.date_to',
            'leave.remarks',
            'leave.final_remarks',
            'leave.leave_type_id')
        ->orderBy('leave.created_at', 'desc');

        if($role_id > 2 AND $role_id < 0) {
            $filter->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
        }

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('leave.date_applied', [$start_date,$end_date]);
        }

        if($status_id[0] != 'null') {
            $stat = [];
            $len = count($status_id);
            for ($x=0; $x < $len ; $x++) { 
                $stat[$x]= (int)$status_id[$x];
            }
            $filter->whereIn('leave.status_id',$stat);
        }

        if($type_id[0] != 'null') {
            $type = [];
            $len = count($type_id);
            for ($x=0; $x < $len ; $x++) { 
                $type[$x]= (int)$type_id[$x];
            }
            $filter->whereIn('leave.leave_type_id',$type);
        }

        if ($company_id[0] != 'null') {
            $comp = [];
            $len = count($company_id);
            for ($x=0; $x < $len ; $x++) { 
                $comp[$x]= (int)$company_id[$x];
            }
            $filter->whereIn('employee_company.company_id',$comp);
        }
        if($employee_id[0] != 'null') {
            $filter->whereIn('leave.emp_id',$employee_id);
        }
        $data = $filter->get();
        return $data;

    }

    public function validateDuplication($data){
        
        $len = count($data);
        $already_exist=[];

        for ($x=0; $x < $len; $x++) { 
            $leng = count($data[$x]['details']);
            for ($i=0; $i < $leng ; $i++) { 

                $id = $data[$x]['emp_id'];
                $type = $data[$x]['type'];
                $date = $data[$x]['details'][$i]['date'];

                $validate = DB::table('leave_request')
                ->join('leave', 'leave_request.leave_id', '=', 'leave.id')
                ->join('employee', 'leave.emp_id', '=', 'employee.id')
                ->join('status_type', 'leave.status_id', '=', 'status_type.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('status_type.name AS status'),
                    'leave.id'
                    )
                ->where('leave_request.date', '=' ,$date)
                ->where('leave.emp_id', '=' ,$id)
                ->first();
            }
                if(count($validate)==1){
                    $already_exist[]= array(
                    'name'  =>  $validate->name,
                    'id'    =>  $validate->id,
                    'status' => $validate->status);
                }
        }

        return $already_exist;
    }

    public function duplicateEntryValidation($data){
        
        $already_exist=[];
        $leng = count($data['employee_id']);
        $edit = $data['edit'];
        $validate = [];

        for ($x=0; $x < $leng; $x++) { 
                
            $employee_id = $data['employee_id'][$x];
            $date_from =  $data['date_from'];
            $date_to =  $data['date_to'];

            $filter = DB::table('leave_request')
            ->join('leave', 'leave_request.leave_id', '=', 'leave.id')
            ->join('employee', 'leave.emp_id', '=', 'employee.id')
            ->join('status_type', 'leave.status_id', '=', 'status_type.id')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('status_type.name AS status'),
                'leave.id',
                'leave.emp_id'
                )
            ->whereBetween('leave_request.date', [$date_from,$date_to])
            ->where('leave.emp_id', '=' ,$employee_id);

            if($data['id'] != -1) {
                $reason = $data['reason'];
                $filter->where('leave.remarks', '=' ,$reason);
            }

            if($edit == true) {
                $id = $data['ids'];
                $filter->where('leave.id','!=',$id);
            }

            $validate = $filter->first();

            if(count($validate)==1){
                $already_exist[]= array(
                    'name'   =>  $validate->name,
                    'id'     =>  $validate->id,
                    'emp_id' =>  $validate->emp_id,
                    'status' => $validate->status
                );
            }
        }


        return $already_exist;
    }

    public function getBranch($id){

        $dat = DB::table('employee_company')
         ->join('employee', 'employee_company.employee_id', '=', 'employee.id')
         ->select(
            'employee_company.branch_id',
            'employee.id')
        ->where('employee.id',$id)
        ->first();

        return $dat;
    }

    public function getCredit($type,$id){

        if ($type == 1 || $type == '1') {
            $data=DB::table('employee')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('sick_leave_credit - use_sick_leave_credit as credit'),
                'id as id'
                )
            ->whereIn('id',$id)
            ->get();
        }
        else if ($type == 2|| $type == '2') {
            $data=DB::table('employee')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('vacation_leave_credit - use_vacation_leave_credit as credit'),
                'id as id'
                )
            ->whereIn('id',$id)
            ->get();
        }
        else if ($type == 3|| $type == '3') {
            $data=DB::table('employee')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('emergency_leave_credit - use_emergency_leave_credit as credit'),
                'id as id'
                )
            ->whereIn('id',$id)
            ->get();
        }
        else if ($type == 4|| $type == '4') {
            $data=DB::table('employee')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('maternity_leave_credit - use_maternity_leave_credit as credit'),
                'id as id'
                )
            ->whereIn('id',$id)
            ->get();
        }
        else{
            $data=DB::table('employee')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('others_leave_credit - use_others_leave_credit as credit'),
                'id as id'
                )
            ->whereIn('id',$id)
            ->get();
        }

        foreach ($data as $key => $d) {
            if ($d->credit < 0) {
                $d->credit = 0;
            }
        }

        return $data;
    }

     public static function getWorkingSchedule($id){

        $data = DB::table('employee_working_schedule')
             ->join('working_schedule', 'employee_working_schedule.working_schedule_id', '=', 'working_schedule.id')
             ->join('employee_company', 'employee_working_schedule.employee_id', '=', 'employee_company.employee_id')
             ->select(
                'working_schedule.id', 
                'working_schedule.time_in', 
                'working_schedule.time_out', 
                'working_schedule.days',
                'working_schedule.company_id',
                'working_schedule.grace_period_mins',
                'working_schedule.lunch_break_time',
                'working_schedule.lunch_mins_break',
                'employee_company.branch_id')
            ->where('employee_working_schedule.employee_id','=',$id)
            ->get();

        $s = '2017-01-01';
        $e='2017-10-01';

        $stime = strtotime($s); 
        $etime=strtotime($e);

        $sunday = strtotime('next sunday', $stime); 

        $format = 'Y-m-d'; 

        $sun=array(date($format, $sunday));

        while($sunday<$etime){
            $sunday=strtotime('next sunday', $sunday);
            array_push($sun, date($format, $sunday));
        }


            
      return $sun;
    }
}
