<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\UserController;

class EventTypeModel extends Model
{
    protected $table = "event_type"; use SoftDeletes;
    
    public function getUser(){
        $contr = new UserController;
        $data = $contr->getListByIdLimited();

        return $data;
    }

    public function getUserId($id){
        $contr = new UserController;
        $data = $contr->getUserId($id);

        return $data;
    }

    public function getList(){

        return self::select(
            'id', 
            'name', 
            'description',
        	'color')
        ->orderBy('created_at', 'desc')
        ->get();

    }

    public function show($id){

        return self::select(
            'id', 
            'name', 
            'description',
        	'color')
        ->where('id','=',$id)
        ->first();
    }
	
}


