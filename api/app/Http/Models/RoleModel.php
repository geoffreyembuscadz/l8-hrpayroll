<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\Models\Permission;
use App\Http\Models\RoleModuleModel as RoleModule;
use App\Http\Models\RoleMenuModel as RoleMenu;
use \DB AS DB;

class RoleModel extends Model
{
    protected $table = "roles"; use SoftDeletes;

    public function preList($model) {
    	 // return self::join('')
    }

    public function postList($model) {
    	 // return self::join('')
        return $model->select('roles.id as id', 'roles.name as text', 'roles.display_name', 'roles.description');
    }

    // getList without the first id (superAdmin)
    public function getList() {
        $count = self::count();
        $skip = 1;
        $limit = $count - $skip;
        $query = 
            self::
            select(
               'roles.id', 
               'roles.name as text', 
               'roles.display_name',                     
               'roles.description'
                // ,
                // DB::raw('GROUP_CONCAT( DISTINCT permissions.id) as permission_id'),
                // DB::raw('GROUP_CONCAT( DISTINCT \' \',permissions.display_name) as permission_name'),
                // DB::raw('GROUP_CONCAT( DISTINCT role_module.module_id)as module_id'),
                // DB::raw('GROUP_CONCAT( DISTINCT  role_menu.menu_id) as menu_id') 
            )
            
            // ->leftJoin('role_module',       'roles.id', '=', 'role_module.role_id')
            // ->leftJoin('role_menu',         'roles.id', '=', 'role_menu.role_id')
            // ->leftJoin('permission_role',   'roles.id', '=', 'permission_role.role_id')

            // ->leftJoin('permissions', 'permission_role.permission_id', '=', 'permissions.id'  )


            ->groupBy('roles.id')
            ->groupBy('roles.name')
            ->groupBy('roles.display_name')
            ->groupBy('roles.description')
            ->skip($skip)
            ->take($limit)
            ->get();

        return $query;
    }

    // get the roles with the multiple permissions
    public function getListById($id) {
        $query = 
            self::
            select(
               'roles.id', 
               'roles.name as text', 
               'roles.display_name',                     
               'roles.description',
                DB::raw('GROUP_CONCAT( DISTINCT permissions.id) as permission_id'),
                DB::raw('GROUP_CONCAT( DISTINCT \' \',permissions.display_name) as permission_name'),
                DB::raw('GROUP_CONCAT( DISTINCT role_module.module_id) as module_id'),
                DB::raw('GROUP_CONCAT( DISTINCT  role_menu.menu_id) as menu_id') 
            )
            
            ->leftJoin('role_module',       'roles.id', '=', 'role_module.role_id')
            ->leftJoin('role_menu',         'roles.id', '=', 'role_menu.role_id')
            ->leftJoin('permission_role',   'roles.id', '=', 'permission_role.role_id') 
            ->leftJoin('permissions', 'permission_role.permission_id', '=', 'permissions.id'  )
            ->where('roles.id', '=', $id)
            ->groupBy('roles.id')
            ->groupBy('roles.name')
            ->groupBy('roles.display_name')
            ->groupBy('roles.description')
            ->first();

        return $query;
    }

    public function preStore($id, $data) {
        $role_id = Role::find($id);
        if (isset($data['data']['permission_id'])) {
             $permission_ids = $data['data']['permission_id'];
             if (!empty($permission_ids) && isset($permission_ids[0])) {
                foreach ($permission_ids as $permission_id) {
                     // save the role and permiision to permission_role table
                     $role_id->attachPermission($permission_id);
                }
            } 
        }

        if (isset($data['data']['module_id'])) {
            $module_ids     = $data['data']['module_id'];
            if (!empty($module_ids) && isset($module_ids[0])) {
                // save the role_id and module_id to role_module
                foreach ($module_ids as $module_id) {
                     $RoleModule             = new RoleModule;
                     $RoleModule->role_id    = $id;
                     $RoleModule->created_by = $data['data']['created_by'];
                     $RoleModule->module_id  = $module_id;
                     $RoleModule->save();
                }
            } 
        }

        if (isset($data['data']['menu_id'])) {
            $menu_ids       = $data['data']['menu_id'];
            if (!empty($menu_ids) && isset($menu_ids[0])) {
                // save the role_id and menu_id to role_menu
                foreach ($menu_ids as $menu_id) {
                     $RoleMenu             = new RoleMenu;
                     $RoleMenu->role_id    = $id;
                     $RoleMenu->created_by = $data['data']['created_by'];
                     $RoleMenu->menu_id    = $menu_id;
                     $RoleMenu->save();
                }
            } 
        }
    }

    public function postUpdate($id, $data) {
        $role_id = Role::find($id);
        $RoleModule = new RoleModule;
        $RoleMenu = new RoleMenu;
        
        // get the data     
        $permission_ids = $data['data']['permission_id'];
        $module_ids     = $data['data']['module_id'];
        $menu_ids       = $data['data']['menu_id'];

        // check if there is module_ids
        if (!empty($module_ids) && isset($module_ids[0])) {
            // force delete the existing role_id on the role_module table
            $RoleModule->where('role_id', $id)->forceDelete();
            // save the submitted role_ids and module_ids
            foreach ($module_ids as $module_id) {
                 $RoleModule             = new RoleModule;
                 $RoleModule->role_id    = $id;
                 $RoleModule->created_by = (isset($data['data']['created_by'])) ? $data['data']['created_by'] : 1 ;
                 $RoleModule->module_id  = $module_id;
                 $RoleModule->save();
            }
        } elseif ($module_ids == null) {
            // if null means the data is blank so delete all
            $RoleModule->where('role_id', $id)->forceDelete();   
        }
        
        // check if there is menu_ids
        if (!empty($menu_ids) && isset($menu_ids[0])) {
            // force delete the existing role_id on the role_menu
            $RoleMenu->where('role_id', $id)->forceDelete();
            foreach ($menu_ids as $menu_id) {
                // save the submitted role_ids and menu_ids
                 $RoleMenu             = new RoleMenu;
                 $RoleMenu->role_id    = $id;
                 $RoleMenu->created_by = (isset($data['data']['created_by'])) ? $data['data']['created_by'] : 1 ;
                 $RoleMenu->menu_id    = $menu_id;
                 $RoleMenu->save();
            }
        } elseif ($menu_ids == null) {
            // force delete
            $RoleMenu->where('role_id', $id)->forceDelete();
        }

        if ($permission_ids == null) {
            // delete
            $role_id->perms()->sync([]);

        } else {
            // sync - force delete the existing record and add the newly submitted yaz_record(id, pos, type)
            foreach ($permission_ids as $permission_id) {
                $role_id->perms()->sync($permission_ids);

            }
            return $data;
        } 
    }

    public function module() {
         return $this->belongsToMany('App\Http\Models\RoleModuleModel');
    }

    public function menu() {
         return $this->belongsToMany('App\Http\Models\RoleMenuModel');
    }
}
