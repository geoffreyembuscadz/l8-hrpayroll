<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class CertificateOfAttendanceTypeModel extends Model
{
    protected $table = "certi_of_atten_type"; use SoftDeletes;
}
