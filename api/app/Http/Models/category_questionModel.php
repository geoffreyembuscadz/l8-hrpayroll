<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class category_questionModel extends Model
{
    protected $table = "category_question"; use SoftDeletes;
}
