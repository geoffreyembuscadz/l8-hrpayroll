<?php
namespace App\Http\Models;

use \DB AS DB;
use App\Http\Models\BiometricsModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyBranchModel extends Model
{
    protected $table = "company_branch"; use SoftDeletes;

    public function getCompanyBranches() {
        return static::select('company_branch.branch_name', 'company.name', 'company_branch.id')->leftJoin('company', 'company.id', '=', 'company_branch.company_id')->get();
    }

    public function updateCompanyBranch($id, $data){
        $updated_branches_ids = [];
        $existing_company_branch_ids = CompanyBranchModel::where('company_id', $id)->whereRaw('deleted_at IS NULL')->pluck('id')->toArray();

        if(isset($data['company_branch'])){
            foreach($data['company_branch'] AS $count => $company_branch){
                if(isset($company_branch['id'])){
                    $updated_branches_ids[$count] = $company_branch['id'];

                    CompanyBranchModel::where('id', $company_branch['id'])->where('company_id', $id)->update([
                        'branch_name' => $company_branch['branch_name'],
                        'branch_address' => $company_branch['branch_address'],
                        'contact_number' => $company_branch['contact_number'],
                        'contact_person' => $company_branch['contact_person'],
                        'updated_at' => date('Y-m-d H:i:s'),
                        'updated_by' => 1
                    ]);
                } else {
                    $company_branch_model = new CompanyBranchModel();
                    $company_branch_model->branch_name = $company_branch['branch_name'];
                    $company_branch_model->company_id = $id;
                    $company_branch_model->branch_address = $company_branch['branch_address'];
                    $company_branch_model->contact_number = $company_branch['contact_number'];
                    $company_branch_model->contact_person = $company_branch['contact_person'];
                    $company_branch_model->created_at = date('Y-m-d H:i:s');
                    $company_branch_model->created_by = 1;
                    $company_branch_model->save();

                }
            }
        }

        if(is_array($existing_company_branch_ids) && is_array($updated_branches_ids)){
            $company_branch_ids_to_delete = array_values(array_diff($existing_company_branch_ids, array_values($updated_branches_ids)));

            CompanyBranchModel::whereIn('id', $company_branch_ids_to_delete)->update([
                'deleted_at' => date('Y-m-d H:i:s'),
                'deleted_by' => 1
            ]);
        }

        $data = CompanyBranchModel::where('company_id', $id)->whereRaw('deleted_at IS NOT NULL && deleted_by IS NOT NULL')->get();

        return $data;
    }
}
