<?php
namespace App\Http\Models;
use DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;

class RankTypeModel extends Model
{
    protected $table = "rank_type"; use SoftDeletes;

    public static function getRankType(){
    	$query = DB::table('rank_type')
    			 ->select('id', 'name', 'description')
    			 ->where('deleted_at', '=', NULL)
    			 ->get();
    	return $query;
    }
}
