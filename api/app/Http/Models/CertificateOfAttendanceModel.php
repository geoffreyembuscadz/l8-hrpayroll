<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;
use \DB;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AttendanceController;

class CertificateOfAttendanceModel extends Model
{
    protected $table = "certi_of_atten"; use SoftDeletes;

    public function getUser(){
        $contr = new UserController;
        $data = $contr->getListByIdLimited();

        return $data;
    }

    public function getUserId($id){
        $contr = new UserController;
        $data = $contr->getUserId($id);

        return $data;
    }

    public static function getCOAData(){

        return self::join('status_type', 'certi_of_atten.status_id', '=', 'status_type.id')
        ->join('employee', 'certi_of_atten.emp_id', '=', 'employee.id')
        ->join('certi_of_atten_type', 'certi_of_atten.COA_type_id', '=', 'certi_of_atten_type.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('certi_of_atten_type.name AS type'), 
            DB::raw('SUBSTRING(certi_of_atten.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(certi_of_atten.final_remarks, 1, 15) as final_remarks'),
            'certi_of_atten.date_applied',
            'certi_of_atten.id', 
            'certi_of_atten.created_at', 
            'certi_of_atten.emp_id', 
            'certi_of_atten.COA_type_id',
            'certi_of_atten.time',
            'certi_of_atten.date')
        ->orderBy('certi_of_atten.created_at', 'desc')
        ->get();

    }

    public static function show($id){

       return self::join('status_type', 'certi_of_atten.status_id', '=', 'status_type.id')
        ->join('certi_of_atten_type', 'certi_of_atten.COA_type_id', '=', 'certi_of_atten_type.id')
        ->join('employee', 'certi_of_atten.emp_id', '=', 'employee.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('certi_of_atten_type.name AS type'),
            DB::raw('certi_of_atten.remarks AS reason'),   
            'certi_of_atten.final_remarks',
            'certi_of_atten.date_applied',
            'certi_of_atten.id', 
            'certi_of_atten.created_at', 
            'certi_of_atten.emp_id', 
            'certi_of_atten.COA_type_id',
            'certi_of_atten.time',
            'certi_of_atten.date')
        ->where('certi_of_atten.id', '=' ,$id)
        ->first();

    }

    public function createCOA($data){

        $notif=[];
        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        foreach ($data as $counter => $d) {

            $COA_id =  DB::table('certi_of_atten')->insertGetId(
                    array(
                        'emp_id'        => $d['emp_id'],
                        'date'          => $d['date'],
                        'time'          => $d['time'],
                        'date_applied'  => $date,
                        'remarks'       => $d['remarks'],
                        'status_id'     => 1,
                        'COA_type_id'   => $d['coa_type_id'],
                        'created_by'    => $user->user_id,
                        'created_at'    => $date
                        )
                );

            $messenger_id = null;
            $messenger_id = $this->getUserId($d['emp_id']);

            if ($messenger_id != null) {
                $notif_id=DB::table('notification')->insertGetId(
                    array(
                        'title'          => 'Certificate of Attendance Request',
                        'description'    => $d['remarks'],
                        'messenger_id'   => $messenger_id->user_id,
                        'table_from'     => 'certi_of_atten',
                        'table_from_id'  => $COA_id,
                        'seen'           => 0,
                        'route'          => 'certificate-of-attendance-list',
                        'receiver_id'    => $user->supervisor_id,
                        'icon'           => 'fa fa-pencil-square-o text-aqua',
                        'created_by'     => $user->user_id,
                        'created_at'     => $date
                        )
                );
                $notif[$counter]= $notif_id;
            }

        }

    return $notif;

	}

    public function getMyCOA($data){

        $user = $this->getUser();

        $employee_id = $user->employee_id;
        $type_id = $data['type_id'];
        $status_id = $data['status_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];
        
        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('certi_of_atten')
        ->join('status_type', 'certi_of_atten.status_id', '=', 'status_type.id')
        ->join('employee', 'certi_of_atten.emp_id', '=', 'employee.id')
        ->join('certi_of_atten_type', 'certi_of_atten.COA_type_id', '=', 'certi_of_atten_type.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('certi_of_atten_type.name AS type'), 
            DB::raw('certi_of_atten.remarks AS reason'),   
            'certi_of_atten.final_remarks',
            'certi_of_atten.date_applied',
            'certi_of_atten.id', 
            'certi_of_atten.created_at', 
            'certi_of_atten.emp_id', 
            'certi_of_atten.COA_type_id',
            'certi_of_atten.time',
            'certi_of_atten.date')
        ->where('certi_of_atten.emp_id', '=' ,$employee_id)
        ->orderBy('certi_of_atten.created_at', 'desc');

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('certi_of_atten.date_applied', [$start_date,$end_date]);
        }

        if($status_id[0] != 'null') {
            $stat = [];
            $len = count($status_id);
            for ($x=0; $x < $len ; $x++) { 
                $stat[$x]= (int)$status_id[$x];
            }
            $filter->whereIn('certi_of_atten.status_id',$stat);
        }

        if($type_id[0] != 'null') {
            $type = [];
            $len = count($type_id);
            for ($x=0; $x < $len ; $x++) { 
                $type[$x]= (int)$type_id[$x];
            }
            $filter->whereIn('certi_of_atten.COA_type_id',$type);
        }

        $data = $filter->get();

        return $data;

    }

    public function updateData($data){

        $d = $data;
        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        DB::table('certi_of_atten')->where('id',$d['id'])->update([
                    'emp_id'        => $d['employee_id'][0],
                    'date'          => $d['date'],
                    'time'          => $d['time'],
                    'COA_type_id'   => $d['coa_type_id'],
                    'date_applied'  => $date,
                    'remarks'       => $d['remarks'],
                    'updated_by'    => $user->user_id,
                    'updated_at'    => $date
        ]);

        return $data;
    }

    public function getCOAFilter($data){

        $user = $this->getUser();

        $supervisor_id = $user->user_id;
        $role_id = $user->role_id;

        $employee_id = $data['employee_id'];
        $company_id = $data['company_id'];
        $type_id = $data['type_id'];
        $status_id = $data['status_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];
        
        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('certi_of_atten')
        ->join('status_type', 'certi_of_atten.status_id', '=', 'status_type.id')
        ->join('employee', 'certi_of_atten.emp_id', '=', 'employee.id')
        ->join('certi_of_atten_type', 'certi_of_atten.COA_type_id', '=', 'certi_of_atten_type.id')
        ->join('employee_company', 'certi_of_atten.emp_id', '=', 'employee_company.employee_id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('certi_of_atten_type.name AS type'), 
            DB::raw('SUBSTRING(certi_of_atten.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(certi_of_atten.final_remarks, 1, 15) as final_remarks'), 
            'certi_of_atten.date_applied',
            'certi_of_atten.id', 
            'certi_of_atten.created_at', 
            'certi_of_atten.emp_id', 
            'certi_of_atten.COA_type_id',
            'certi_of_atten.time',
            'certi_of_atten.date')
        ->where('certi_of_atten.deleted_at','=',null)
        ->where('certi_of_atten.deleted_by','=',null)
        ->orderBy('certi_of_atten.created_at', 'desc');
       
        if($role_id > 2 AND $role_id < 0) {
            $filter->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
        }

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('certi_of_atten.date_applied', [$start_date,$end_date]);
        }
        if($status_id[0] != 'null') {
            $stat = [];
            $len = count($status_id);
            for ($x=0; $x < $len ; $x++) { 
                $stat[$x]= (int)$status_id[$x];
            }
            $filter->whereIn('certi_of_atten.status_id',$stat);
        }
        if ($company_id[0] != 'null') {
            $comp = [];
            $len = count($company_id);
            for ($x=0; $x < $len ; $x++) { 
                $comp[$x]= (int)$company_id[$x];
            }
            $filter->whereIn('employee_company.company_id',$comp);
        }
        if($employee_id[0] != 'null') {
            $filter->whereIn('certi_of_atten.emp_id',$employee_id);
        }
        if($type_id[0] != 'null') {
            $type = [];
            $len = count($type_id);
            for ($x=0; $x < $len ; $x++) { 
                $type[$x]= (int)$type_id[$x];
            }
            $filter->whereIn('certi_of_atten.COA_type_id',$type);
        }
        $data = $filter->get();

        
        return $data;

    }

    public function approvedData($id){

        $data = $this->show($id);
        $updated_at = date('Y-m-d H:i:s');

        $date = $data["date"];
        $emp_id = $data["emp_id"];
        $type = $data["type"];
        $time = $data["time"];
        $response = [];

        $contr = new AttendanceController;
        $contr->updateAttendanceFromApproveCOA($data);

        $table=DB::table('employees_logs')
                ->whereRaw("(CAST(employees_logs.time_in AS date) = '$date' or CAST(employees_logs.time_out AS date)= '$date')")
                ->where('employee_id',$emp_id)
                ->get();

        $response['data']=$table;

        return $response;
       
    }

    public function validateDuplication($data){

        
        $len = count($data);
        $already_exist=[];

        for ($x=0; $x < $len; $x++) { 

            $id = $data[$x][0];
            $date = $data[$x][2];

            $validate = self::join('employee', 'certi_of_atten.emp_id', '=', 'employee.id')
            ->join('status_type', 'certi_of_atten.status_id', '=', 'status_type.id')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('status_type.name AS status'),
                'certi_of_atten.id'
                )
            ->where('certi_of_atten.date', '=' ,$date)
            ->where('certi_of_atten.emp_id', '=' ,$id)
            ->first();
            if(count($validate)==1){
                $already_exist[]= array(
                    'name'  =>  $validate["name"],
                    'id'    =>  $validate["id"],
                    'status' => $validate["status"]
                );
            }
        }

        return $already_exist;
    }

    public function getEmployeeName($data){
        $len = count($data);
        $name=[];

        for ($x=0; $x < $len; $x++) { 
            $id = $data[$x][0];
             $info = DB::table('employee')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('id AS id')
                )
            ->where('employee.id', '=' ,$id)
            ->first();

            $name[]= array(
                'id'    =>  $info->id,
                'name'  =>  $info->name
            );
        }

        return $name;
     }

    public function preUpdateDuplication($data){
        $already_exist=[];
        $id = $data['employee_id'];
        $type = $data['coa_type_id'];
        $date = $data['date'];
        $time = $data['time'];
        $remarks = $data['remarks'];

        $validate = self::join('employee', 'certi_of_atten.emp_id', '=', 'employee.id')
        ->join('status_type', 'certi_of_atten.status_id', '=', 'status_type.id')
        ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('status_type.name AS status'),
                'certi_of_atten.id'
                )
        ->where('certi_of_atten.time', '=' ,$time)
        ->where('certi_of_atten.date', '=' ,$date)
        ->where('certi_of_atten.COA_type_id', '=' ,$type)
        ->where('certi_of_atten.emp_id', '=' ,$id)
        ->where('certi_of_atten.remarks', '=' ,$remarks)
        ->first();
        if(count($validate)==1){
            $already_exist[]= array(
                    'name'  =>  $validate["name"],
                    'id'    =>  $validate["id"],
                    'status' => $validate["status"]
                );
        }

        return $already_exist;
    }

    public function checkLogs($data){
        
        $existed=[];

        $id   = $data['employee_id'];
        $date = $data['date'];

        $validate = DB::table('employees_logs')
        ->select(
            'employee_id'
            )
        ->where('employees_logs.employee_id', '=' ,$id)
        ->whereRaw("(CAST(employees_logs.time_in AS date) = '$date' or CAST(employees_logs.time_out AS date)= '$date')")
        ->first();

        if($validate==null){
            $existed[]=[$id];
        }


        return $existed;
    }

    public function duplicateEntryValidation($data){
        
        $leng = count($data['employee_id']);
        $date = $data['date'];
        $edit = $data['edit'];
        $existed=[];

        for ($i=0; $i < $leng ; $i++) { 
            $id = $data['employee_id'][$i];
            $filter = self::join('employee', 'certi_of_atten.emp_id', '=', 'employee.id')
            ->join('status_type', 'certi_of_atten.status_id', '=', 'status_type.id')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('status_type.name AS status'),
                'certi_of_atten.id',
                'certi_of_atten.emp_id'
                )
            ->where('certi_of_atten.date', '=' ,$date)
            ->where('certi_of_atten.emp_id', '=' ,$id);

            if($edit == true) {
                    $id = $data['id'];
                    $filter->where('certi_of_atten.id','!=',$id);
            }

            $validate = $filter->first();
            
            if(count($validate)==1){
                $existed[]= array(
                    'name'   =>  $validate["name"],
                    'id'     =>  $validate["id"],
                    'emp_id' =>  $validate["emp_id"],
                    'status' => $validate["status"]
                );
            }
        }

        return $existed;
    }

    public function checkEmployeeLogs($data){
        $len = count($data);
        $notExist=[];
        $validate = null;

        foreach ($data as $key => $d) {
            $id   = $d['emp_id'];
            $date = $d['date'];

            $validate = DB::table(
                        DB::raw("employees_logs where (CAST(time_in AS DATE) = '". $date ."' or CAST(time_out AS DATE) = '". $date ."' ) and employee_id = ".$id)
                        )
                        ->first();

            if($validate==null){
                $notExist[]=[$id];
            }
        }

        return $notExist;
    }

    public function coaTypeValidation($data){

        $len = count($data);
        $valid=[];

        for ($x=0; $x < $len; $x++) { 
            $id   = $data[$x]['emp_id'];
            $date = $data[$x]['date'];
            $validate = null;

            $filter = DB::table('employees_logs')
            ->join('employee', 'employees_logs.employee_id', '=', 'employee.id')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`')
                )
            ->where('employees_logs.employee_id', '=' ,$id);

            if($data[0]['coa_type_id'] == '1'){
                $filter->whereNull('employees_logs.time_in');
                $filter->whereRaw(" CAST(time_out as date) = '$date'");
            }
            elseif ($data[0]['coa_type_id'] == '2') {
                $filter->whereNull('employees_logs.time_out');
                $filter->whereRaw(" CAST(time_in as date) = '$date'");
            }

            $validate = $filter->first();

            if($validate==null){
                if ($data[$x]['coa_type_id'] == '1') {
                    $data[$x]['coa_type_id'] = 2;
                }
                elseif ($data[$x]['coa_type_id'] == '2') {
                    $data[$x]['coa_type_id'] = 1;
                }
            }
        }

        return $data;
    }

    public function typeValidation($data){

        $valid=[];

        $id   = $data['employee_id'];
        $date = $data['date'];

        $filter = DB::table('employees_logs')
        ->join('employee', 'employees_logs.employee_id', '=', 'employee.id')
        ->select(
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`')
            )
        ->where('employees_logs.employee_id', '=' ,$id);

        if($data['coa_type_id'] == '1'){
            $filter->whereNull('employees_logs.time_in');
        }
        elseif ($data['coa_type_id'] == '2') {
            $filter->whereNull('employees_logs.time_out');
        }

        $validate = $filter->first();

        if($validate==null){
            if ($data['coa_type_id'] == '1') {
                $data['coa_type_id'] = 2;
            }
            elseif ($data['coa_type_id'] == '2') {
                $data['coa_type_id'] = 1;
            }
        }


        return $data;
    }
}
