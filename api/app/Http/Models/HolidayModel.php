<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use \DB;

class HolidayModel extends Model
{
    protected $table = "holiday"; use SoftDeletes;

	public static function getList(){

	    return self::join('holiday_type', 'holiday.holiday_type_id', '=', 'holiday_type.id')
				    ->select(
				        DB::raw('holiday_type.name AS type'), 
				        'holiday.id', 
				        'holiday.name', 
				        'holiday.location', 
				        'holiday.holiday_type_id',
				        'holiday.date')
				    ->get();


				   

	}



}

