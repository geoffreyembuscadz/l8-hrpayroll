<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;
use \DB;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AttendanceController;

class OfficialBusinessModel extends Model
{
    protected $table = "official_business"; use SoftDeletes;

    public function getUser(){
        $contr = new UserController;
        $data = $contr->getListByIdLimited();

        return $data;
    }

    public function getUserId($id){
        $contr = new UserController;
        $data = $contr->getUserId($id);

        return $data;
    }

    public function createOB($data){
        
        $notif=[];
        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();


        foreach ($data as $counter => $d) {
            $OB_id = DB::table('official_business')->insertGetId(
                        array(
                            'status_id'     => 1,
                            'emp_id'        => $d['emp_id'],
                            'start_date'    => $d['start_date'],
                            'end_date'      => $d['end_date'],
                            'attachment'    => $d['attachment'],
                            'remarks'       => $d['remarks'],
                            'created_by'    => $user->user_id,
                            'created_at'    => $date,
                            'date_applied'  => $date
                        )
                    );



            foreach ($d['details'] as $da) {
                DB::table('official_business_request')->insert(
                        array(
                            'OB_id'             => $OB_id,
                            'date'              => $da['date'],
                            'start_time'        => $da['start_time'],
                            'end_time'          => $da['end_time'],
                            'branch_id'         => (int)$da['location'],
                            'created_by'        => $user->user_id,
                            'created_at'        => $date
                            )
                    );
            }

            $messenger_id = null;
            $messenger_id = $this->getUserId($d['emp_id']);

            if ($messenger_id != null) {

                $notif_id=DB::table('notification')->insertGetId(
                    array(
                        'title'          => 'Official Business Request',
                        'description'    => $d['remarks'],
                        'messenger_id'   => $messenger_id->user_id,
                        'table_from'     => 'official_business',
                        'table_from_id'  => $OB_id,
                        'seen'           => 0,
                        'route'          => 'official-business-list',
                        'receiver_id'    => $user->supervisor_id,
                        'icon'           => 'fa fa-pencil-square-o text-aqua',
                        'created_by'     => $user->user_id,
                        'created_at'     => $date
                        )
                );
                $notif[$counter]= $notif_id;
            }
        }

    return $notif;

    }

    public static function getList(){

        return self::join('status_type', 'official_business.status_id', '=', 'status_type.id')
        ->join('employee', 'official_business.emp_id', '=', 'employee.id')
        // ->join('official_business_type', 'official_business.OB_type_id', '=', 'official_business_type.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            // DB::raw('official_business_type.name AS type'), 
            DB::raw('SUBSTRING(official_business.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(official_business.final_remarks, 1, 15) as final_remarks'),
            DB::raw('official_business.date_applied AS date'), 
            'official_business.id', 
            'official_business.created_at', 
            'official_business.emp_id', 
            // 'official_business.OB_type_id',
            'official_business.start_date',
            'official_business.end_date')
        ->orderBy('official_business.created_at', 'desc')
        ->get();

    }

    public static function show($id){

        $data = DB::table('official_business_request')
                ->join('official_business', 'official_business_request.OB_id', '=', 'official_business.id')
                ->join('company_branch', 'official_business_request.branch_id', '=', 'company_branch.id')
                ->join('employee', 'official_business.emp_id', '=', 'employee.id')
                ->join('status_type', 'official_business.status_id', '=', 'status_type.id')
                ->select(
                    DB::raw('official_business_request.id AS request_id'),
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
                    DB::raw('official_business.remarks AS reason'),
                    DB::raw('status_type.name AS status'),
                    'official_business.final_remarks',
                    'official_business.id',  
                    'official_business.date_applied',
                    'official_business.emp_id', 
                    'official_business.start_date',
                    'official_business.end_date',
                    'official_business_request.branch_id',
                    'official_business_request.date',
                    'official_business_request.start_time',
                    'official_business_request.end_time',
                    'official_business.attachment',
                    'company_branch.branch_name as location')
                ->where('official_business.id', '=' ,$id)
                ->get();
        return $data;
    }

    public function getMyOB($data){

        $user = $this->getUser();

        $employee_id = $user->employee_id;
        $status_id = $data['status_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];

        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('official_business')
        ->join('status_type', 'official_business.status_id', '=', 'status_type.id')
        ->join('employee', 'official_business.emp_id', '=', 'employee.id')
        // ->join('official_business_type', 'official_business.OB_type_id', '=', 'official_business_type.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            // DB::raw('official_business_type.name AS type'), 
            DB::raw('official_business.date_applied AS date'), 
            DB::raw('SUBSTRING(official_business.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(official_business.final_remarks, 1, 15) as final_remarks'),
            'official_business.id', 
            'official_business.created_at', 
            'official_business.emp_id', 
            // 'official_business.OB_type_id',
            'official_business.start_date',
            'official_business.end_date')
        ->where('official_business.emp_id','=',$employee_id)
        ->orderBy('official_business.created_at', 'desc');

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('official_business.date_applied', [$start_date,$end_date]);
        }
        if($status_id[0] != 'null') {
            $stat = [];
            $len = count($status_id);
            for ($x=0; $x < $len ; $x++) { 
                $stat[$x]= (int)$status_id[$x];
            }
            $filter->whereIn('official_business.status_id',$stat);
        }

        $data = $filter->get();
        return $data;

    }

    public function updateSave($id,$d,$user){

        $date = date('Y-m-d H:i:s');

        DB::table('official_business')->where('id',$id)->update([
            'emp_id'        => $d['employee_id'][0],
            'remarks'       => $d['remarks'],
            'start_date'    => $d['start_date'],
            'end_date'      => $d['end_date'],
            'remarks'       => $d['remarks'],
            'updated_by'    => $user->user_id,
            'updated_at'    => $date
        ]);

        foreach ($d['details'] as $da) {
            DB::table('official_business_request')->insert(
                array(
                    'OB_id'             => $id,
                    'date'              => $da['date'],
                    'start_time'        => $da['start_time'],
                    'end_time'          => $da['end_time'],
                    'branch_id'         => $da['location'][0],
                    'created_by'        => $user->user_id,
                    'created_at'        => $date
                    )
            );
        }
    }

    public function updateData($file){
        $d = $file;
        $user = $this->getUser();
        $date = date('Y-m-d H:i:s');
        $id = $d['id'];
        $data = [];

        if($d['details'][0]['request_id'] == 0){
            DB::table('official_business_request')
            ->where('OB_id', $id)->delete();

            $data = $this->updateSave($id,$d,$user);
        }
        elseif($d['details'][0]['request_id'] != 0){
            DB::table('official_business')->where('id',$id)->update([
                        'emp_id'        => $d['employee_id'][0],
                        'remarks'       => $d['remarks'],
                        'start_date'    => $d['start_date'],
                        'end_date'      => $d['end_date'],
                        'remarks'       => $d['remarks'],
                        'updated_by'    => $user->user_id,
                        'updated_at'    => $date
            ]);

            $dat = DB::table('official_business_request')
            ->where('OB_id', $id)->get();
            
            if (count($dat) != count($d['details'])) {
                $len = count($d['details']);
                $temp = [];
                for ($x=0; $x < $len ; $x++) { 
                    $temp[$x]= $d['details'][$x]['date'];
                }

                DB::table('official_business_request')
                ->where('OB_id', $id)
                ->whereNotIn('date', $temp)
                ->delete();
            }


            foreach ($d['details'] as $key => $da) {
                $request_id = $da['request_id'];
                DB::table('official_business_request')->where('id',$request_id)->update([
                    'date'          => $da['date'],
                    'start_time'    => $da['start_time'],
                    'end_time'      => $da['end_time'],
                    'branch_id'     => $da['location'],
                    'updated_by'    => $user->user_id,
                    'updated_at'    => $date
                ]);
            }
        }

        $table=DB::table('official_business')
            ->where('id','=',$id)
            ->select()
            ->get();

        return $table;
    }

    public function getSchedule($emp_id){

         $data = DB::table('employee_working_schedule')
         ->join('working_schedule', 'employee_working_schedule.working_schedule_id', '=', 'working_schedule.id')
         ->select(
            'working_schedule.id', 
            'working_schedule.time_in', 
            'working_schedule.time_out', 
            'working_schedule.days',
            'working_schedule.company_id',
            'working_schedule.grace_period_mins')
        ->where('employee_working_schedule.employee_id','=',$emp_id)
        ->get();
        return $data;
    }

    public function approvedData($id){
        $obData = $this->show($id);
        $emp_id = $obData[0]->emp_id;
        $schedData = $this->getSchedule($emp_id);


        $day = [];
        $date = date('Y-m-d H:i:s');
        $type = 5;
        $len = count($schedData);
        $lent = count($obData);

        for ($y=0; $y < $lent ; $y++) { 

            $contr = new AttendanceController;
            $data = $contr->updateAttendanceFromApproveOB($obData[$y]);
        }

        $table=DB::table('official_business')
            ->where('id','=',$id)
            ->select()
            ->get();

        return $table;
    }

    public function getOB($data){

        $user = $this->getUser();

        $supervisor_id = $user->user_id;
        $role_id = $user->role_id;

        $employee_id = $data['employee_id'];
        $company_id = $data['company_id'];
        $status_id = $data['status_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];

        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );
        
        $filter = DB::table('official_business')
        ->join('status_type', 'official_business.status_id', '=', 'status_type.id')
        ->join('employee', 'official_business.emp_id', '=', 'employee.id')
        ->join('employee_company', 'official_business.emp_id', '=', 'employee_company.employee_id')
        // ->join('official_business_type', 'official_business.OB_type_id', '=', 'official_business_type.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            // DB::raw('official_business_type.name AS type'), 
            DB::raw('official_business.date_applied AS date'), 
            DB::raw('SUBSTRING(official_business.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(official_business.final_remarks, 1, 15) as final_remarks'),
            'official_business.id', 
            'official_business.created_at', 
            'official_business.emp_id', 
            // 'official_business.OB_type_id',
            'official_business.attachment',
            'official_business.start_date',
            'official_business.end_date')
        ->where('official_business.deleted_at','=',null)
        ->where('official_business.deleted_by','=',null)
        ->orderBy('official_business.created_at', 'desc');

        if($role_id > 2 AND $role_id < 0) {
            $filter->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
        }

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('official_business.date_applied', [$start_date,$end_date]);
        }
        if($status_id[0] != 'null') {
            $stat = [];
            $len = count($status_id);
            for ($x=0; $x < $len ; $x++) { 
                $stat[$x]= (int)$status_id[$x];
            }
            $filter->whereIn('official_business.status_id',$stat);
        }
        if ($company_id[0] != 'null') {
            $comp = [];
            $len = count($company_id);
            for ($x=0; $x < $len ; $x++) { 
                $comp[$x]= (int)$company_id[$x];
            }
            $filter->whereIn('employee_company.company_id',$comp);
        }
        if($employee_id[0] != 'null') {
            $filter->whereIn('official_business.emp_id',$employee_id);
        }
        $data = $filter->get();
        return $data;

    }

    public function validateDuplication($data){
        
        $len = count($data);
        $already_exist=[];

        for ($x=0; $x < $len; $x++) { 
            $leng = count($data[$x][5]);
            for ($i=0; $i < $leng ; $i++) { 

                $id = $data[$x][0];
                $date = $data[$x][5][$i]['date'];

                $validate = DB::table('official_business_request')
                ->join('official_business', 'official_business_request.OB_id', '=', 'official_business.id')
                ->join('employee', 'official_business.emp_id', '=', 'employee.id')
                ->join('status_type', 'official_business.status_id', '=', 'status_type.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('status_type.name AS status'),
                    'official_business.id'
                    )
                ->where('official_business_request.date', '=' ,$date)
                ->where('official_business.emp_id', '=' ,$id)
                ->first();
            }
                if(count($validate)==1){
                    $already_exist[]= array(
                    'name'  =>  $validate->name,
                    'id'    =>  $validate->id,
                    'status' => $validate->status);
                }
        }

        return $already_exist;
    }

    public function preUpdateDuplication($data){
        $already_exist=[];
        $len = count($data['details']);
        $validate = [];


        for ($i=0; $i <$len ; $i++) { 
            
            $id = $data['employee_id'];
            $reason =  $data['remarks'];
            $date = $data['details'][$i]['date'];
            $location = $data['details'][$i]['location'];
            $start_time =  $data['details'][$i]['start_time'];
            $end_time =  $data['details'][$i]['end_time'];

            $validate = DB::table('official_business_request')
                ->join('official_business', 'official_business_request.OB_id', '=', 'official_business.id')
                ->join('employee', 'official_business.emp_id', '=', 'employee.id')
                ->join('status_type', 'official_business.status_id', '=', 'status_type.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('status_type.name AS status'),
                    'official_business.id'
                    )
                ->where('official_business_request.date', '=' ,$date)
                ->where('official_business.emp_id',$id)
                ->where('official_business.remarks',$reason)
                ->where('official_business_request.start_time',$start_time)
                ->where('official_business_request.end_time',$end_time)
                ->where('official_business_request.branch_id',$location)
                ->first();
        }

        if(count($validate)==1){
            $already_exist[]= array(
                'name'  =>  $validate->name,
                'id'    =>  $validate->id,
                'status' => $validate->status
            );
        }

        return $already_exist;
    }

    public function duplicateEntryValidation($data){
        
        $already_exist=[];
        $leng = count($data['employee_id']);
        $edit = $data['edit'];
        $validate = [];

        for ($i=0; $i < $leng; $i++) { 

            $employee_id = $data['employee_id'][$i];
            $start_date =  $data['start_date'];
            $end_date =  $data['end_date'];

            $filter = DB::table('official_business_request')
                ->join('official_business', 'official_business_request.OB_id', '=', 'official_business.id')
                ->join('employee', 'official_business.emp_id', '=', 'employee.id')
                ->join('status_type', 'official_business.status_id', '=', 'status_type.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('status_type.name AS status'),
                    'official_business.id',
                    'official_business.emp_id'
                    )
                ->whereBetween('official_business_request.date', [$start_date,$end_date])
                ->where('official_business.emp_id',$employee_id);

            if($edit == true) {
                $id = $data['id'];
                $filter->where('official_business.id','!=',$id);
            }

            $validate = $filter->first();

            if(count($validate)==1){
                $already_exist[]= array(
                    'name'  =>  $validate->name,
                    'id'    =>  $validate->id,
                    'emp_id' =>  $validate->emp_id,
                    'status' => $validate->status
                );
            }
        }

        return $already_exist;
    }
}
