<?php
namespace App\Http\Models;

use \DB; use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeDependentModel extends Model
{
    protected $table = "employee_dependent"; use SoftDeletes;
}
