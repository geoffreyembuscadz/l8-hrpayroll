<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use \DB;
use Carbon\Carbon;
use App\Http\Models\CommonModel;
use App\Http\Models\ScheduleAdjustmentsModel;
use App\Http\Controllers\CertificateOfAttendanceController;
use App\Http\Controllers\LeaveController;
use App\Http\Controllers\OfficialBusinessController;
use App\Http\Controllers\OvertimeController;
use App\Http\Controllers\UserController;

class CommonModel extends Model
{

  public static function getUser(){
      $contr = new UserController;
      $data = $contr->getListByIdLimited();

      return $data;
  }

  public static function getUserId($id){
        $contr = new UserController;
        $data = $contr->getUserId($id);

        return $data;
    }

  public static function getEmployee(){

    $user = CommonModel::getUser();

    $supervisor_id= $user->user_id;
    $role_id = $user->role_id;


    $filter = DB::table('employee')
    ->select(
      DB::raw('employee.id AS id'),
      DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `text`')
      )
    ->where('deleted_at','=',null)
    ->where('deleted_by','=',null)
    ->where('employee.status',1)
    ->orderBy('text');

    if($role_id > 2 AND $role_id < 0) {
      $filter->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
    }
    $employee = $filter->get();

    return $employee;
  }


  public static function getDepartment(){

    $data = DB::table('department')
    ->select('id','name as text')
    ->where('deleted_at','=',null)
    ->orderBy('text')
    ->get();

    return $data;
  }

   public static function getCompany(){

    $data = DB::table('company')
    ->select('id','name as text','attendance_default as default')
    ->where('deleted_at','=',null)
    ->orderBy('text')
    ->get();

    return $data;
  }


  public static function getLeaveType(){

    $leavetype = DB::table('leave_type')
    ->select('leave_type.id','leave_type.name as text')
    ->where('deleted_at','=',null)
    ->orderBy('text')
    ->get();

    return $leavetype;
  }

  public static function getOBType(){

    $data = DB::table('official_business_type')
    ->select('official_business_type.id','official_business_type.name as text')
    ->where('deleted_at','=',null)
    ->orderBy('text')
    ->get();

    return $data;
  }

  public static function getStatusType(){

    $statustype = DB::table('status_type')
    ->select('status_type.id','status_type.name as text')
    ->where('deleted_at','=',null)
    ->whereIn('id',[1,2,3,4])
    ->orderBy('text')
    ->get();

    return $statustype;
  }

  public static function getCOAType(){

    $COAType = DB::table('certi_of_atten_type')
    ->select('certi_of_atten_type.id','certi_of_atten_type.name as text')
    ->where('deleted_at','=',null)
    ->orderBy('text')
    ->get();

    return $COAType;
  }

  public static function getAttendanceType(){

    $data = DB::table('attendance_type')
    ->select('attendance_type.id','attendance_type.name as text')
    ->where('deleted_at','=',null)
    ->orderBy('text')
    ->get();

    return $data;
  }

  public static function getEventType(){

    $eventType = DB::table('event_type')
    ->select('event_type.id','event_type.type as text')
    ->where('deleted_at','=',null)
    ->orderBy('text')
    ->get();

    return $eventType;
  }

  public static function getHoliType(){

  return DB::table('holiday_type')
        ->select('holiday_type.id','holiday_type.name as text')
        ->where('deleted_at','=',null)
        ->whereRaw('MOD(id, 2) = 0')
        ->orderBy('text')
        ->get();
  }

  public static function getMySchedule($id){

    $data = DB::table('employee_working_schedule')
         ->join('working_schedule', 'employee_working_schedule.working_schedule_id', '=', 'working_schedule.id')
         ->select(
            'working_schedule.id',
            'working_schedule.time_in',
            'working_schedule.time_out',
            'working_schedule.days',
            'working_schedule.company_id',
            'working_schedule.grace_period_mins')
        ->where('employee_working_schedule.employee_id','=',$id)
        ->get();

    return $data;
  }

  public static function getAllRequest($data){

    $user = CommonModel::getUser();

    $supervisor_id= $user->user_id;
    $role_id = $user->role_id;

    $employee_id = $data['employee_id'];
    $company_id = $data['company_id'];
    $status_id = $data['status_id'];
    $start = $data['start_date'];
    $end = $data['end_date'];
    $type_id = $data['type_id'];

    $stat = [];
    $len = count($status_id);
    for ($x=0; $x < $len ; $x++) {
        $stat[$x]= (int)$status_id[$x];
    }

    $comp = [];
    $len = count($company_id);
    for ($x=0; $x < $len ; $x++) {
        $comp[$x]= (int)$company_id[$x];
    }

    // converter for date to string
    $star = strtotime( $start );
    $start_date = date( 'Y-m-d', $star );

    $end = strtotime( $end );
    $end_date = date( 'Y-m-d', $end );

    $leave = DB::table('leave')
            ->join('status_type', 'leave.status_id', '=', 'status_type.id')
            ->join('employee', 'leave.emp_id', '=', 'employee.id')
            ->join('employee_company', 'leave.emp_id', '=', 'employee_company.employee_id')
            ->select(
              DB::raw('leave.id'),
              DB::raw('status_type.name AS status'),
              DB::raw('leave.date_applied As date'),
              DB::raw('leave.date_applied'),
              DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
              DB::raw('leave.remarks AS `reason`'),
              DB::raw('\'Leave\' as type'),
              DB::raw('\'2\' as i'),
              DB::raw('\'leave\' as url'))
            ->where('leave.deleted_at', '=', NULL);

    if($role_id > 2 AND $role_id < 0) {
      $leave->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
    }
    if($status_id[0] != 'null') {
      $leave->whereIn('leave.status_id',$stat);
    }
    if ($company_id[0] != 'null') {
      $leave->whereIn('employee_company.company_id',$comp);
    }
    if($employee_id[0] != 'null') {
      $leave->whereIn('leave.emp_id',$employee_id);
    }
    if( $start != 'null' && $end != 'null'){
      $leave->whereBetween('leave.date_applied', [$start_date,$end_date]);
    }

    $COA = DB::table('certi_of_atten')
          ->join('status_type', 'certi_of_atten.status_id', '=', 'status_type.id')
          ->join('employee', 'certi_of_atten.emp_id', '=', 'employee.id')
          ->join('employee_company', 'certi_of_atten.emp_id', '=', 'employee_company.employee_id')
          ->select(
            DB::raw('certi_of_atten.id'),
            DB::raw('status_type.name AS status'),
            DB::raw('certi_of_atten.date'),
            DB::raw('certi_of_atten.date_applied'),
            DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
            DB::raw('certi_of_atten.remarks AS reason'),
            DB::raw('\'COA\' as type'),
            DB::raw('\'1\' as i'),
            DB::raw('\'certificate_of_attendance\' as url'))
          ->where('certi_of_atten.deleted_at', '=', NULL);

    if($role_id > 2 AND $role_id < 0) {
      $COA->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
    }
    if($status_id[0] != 'null') {
      $COA->whereIn('certi_of_atten.status_id',$stat);
    }
    if ($company_id[0] != 'null') {
      $COA->whereIn('employee_company.company_id',$comp);
    }
    if($employee_id[0] != 'null') {
      $COA->whereIn('certi_of_atten.emp_id',$employee_id);
    }
    if( $start != 'null' && $end != 'null'){
      $COA->whereBetween('certi_of_atten.date_applied', [$start_date,$end_date]);
    }

    $OB =  DB::table('official_business')
          ->join('status_type', 'official_business.status_id', '=', 'status_type.id')
          ->join('employee', 'official_business.emp_id', '=', 'employee.id')
          ->join('employee_company', 'official_business.emp_id', '=', 'employee_company.employee_id')
          ->select(
            DB::raw('official_business.id'),
            DB::raw('status_type.name AS status'),
            DB::raw('official_business.date_applied AS date'),
            DB::raw('official_business.date_applied'),
            DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
            DB::raw('official_business.remarks AS reason'),
            DB::raw('\'Official Business\' as type'),
            DB::raw('\'3\' as i'),
            DB::raw('\'official_business\' as url'))
          ->where('official_business.deleted_at', '=', NULL);

    if($role_id > 2 AND $role_id < 0) {
      $OB->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
    }
    if($status_id[0] != 'null') {
      $OB->whereIn('official_business.status_id',$stat);
    }
    if ($company_id[0] != 'null') {
      $OB->whereIn('employee_company.company_id',$comp);
    }
    if($employee_id[0] != 'null') {
      $OB->whereIn('official_business.emp_id',$employee_id);
    }
    if( $start != 'null' && $end != 'null'){
      $OB->whereBetween('official_business.date_applied', [$start_date,$end_date]);
    }

    $OT =   DB::table('overtime')
            ->join('status_type', 'overtime.status_id', '=', 'status_type.id')
            ->join('employee', 'overtime.emp_id', '=', 'employee.id')
            ->join('employee_company', 'overtime.emp_id', '=', 'employee_company.employee_id')
            ->select(
              DB::raw('overtime.id'),
              DB::raw('status_type.name AS status'),
              DB::raw('overtime.date'),
              DB::raw('overtime.date_applied'),
              DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
              DB::raw('overtime.remarks AS reason'),
              DB::raw('\'Overtime\' as type'),
              DB::raw('\'4\' as i'),
              DB::raw('\'overtime\' as url'))
            ->where('overtime.deleted_at', '=', NULL);

    if($role_id > 2 AND $role_id < 0) {
      $OT->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
    }
    if($status_id[0] != 'null') {
      $OT->whereIn('overtime.status_id',$stat);
    }
    if ($company_id[0] != 'null') {
      $OT->whereIn('employee_company.company_id',$comp);
    }
    if($employee_id[0] != 'null') {
      $OT->whereIn('overtime.emp_id',$employee_id);
    }
    if( $start != 'null' && $end != 'null'){
      $OT->whereBetween('overtime.date_applied', [$start_date,$end_date]);
    }

    $SA =   DB::table('schedule_adjustments')
            ->join('status_type', 'schedule_adjustments.status_id', '=', 'status_type.id')
            ->join('employee', 'schedule_adjustments.emp_id', '=', 'employee.id')
            ->join('employee_company', 'schedule_adjustments.emp_id', '=', 'employee_company.employee_id')
            ->select(
              DB::raw('schedule_adjustments.id'),
              DB::raw('status_type.name AS status'),
              DB::raw('schedule_adjustments.date_applied AS date'),
              DB::raw('schedule_adjustments.date_applied'),
              DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
              DB::raw('schedule_adjustments.remarks AS `reason`'),
              DB::raw('\'SA\' as type'),
              DB::raw('\'5\' as i'),
              DB::raw('\'schedule_adjustments\' as url'))
            ->where('schedule_adjustments.deleted_at', '=', NULL);

    if($role_id > 2 AND $role_id < 0) {
      $SA->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
    }
    if($status_id[0] != 'null') {
      $SA->whereIn('schedule_adjustments.status_id',$stat);
    }
    if ($company_id[0] != 'null') {
      $SA->whereIn('employee_company.company_id',$comp);
    }
    if($employee_id[0] != 'null') {
      $SA->whereIn('schedule_adjustments.emp_id',$employee_id);
    }
    if( $start != 'null' && $end != 'null'){
      $SA->whereBetween('schedule_adjustments.date_applied', [$start_date,$end_date]);
    }

    $main_query = DB::table(DB::raw('DUAL'))
                    ->select(DB::raw('\'\' AS id'),
                       DB::raw('\'\' status'),
                       DB::raw('\'\' AS date'),
                       DB::raw('\'\' AS date_applied'),
                       DB::raw('\'\' AS name'),
                       DB::raw('\'\' AS reason'),
                       DB::raw('\'\' AS type'),
                       DB::raw('\'\' AS i'),
                       DB::raw('\'\' AS url'));

    $UT =   DB::table('official_undertime')
            ->join('status_type', 'official_undertime.status_id', '=', 'status_type.id')
            ->join('employee', 'official_undertime.emp_id', '=', 'employee.id')
            ->join('employee_company', 'official_undertime.emp_id', '=', 'employee_company.employee_id')
            ->select(
              DB::raw('official_undertime.id'),
              DB::raw('status_type.name AS status'),
              DB::raw('official_undertime.date'),
              DB::raw('official_undertime.date_applied'),
              DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
              DB::raw('official_undertime.remarks AS reason'),
              DB::raw('\'Undertime\' as type'),
              DB::raw('\'6\' as i'),
              DB::raw('\'official_undertime\' as url'))
            ->where('official_undertime.deleted_at', '=', NULL);

    if($role_id > 2 AND $role_id < 0) {
      $UT->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
    }
    if($status_id[0] != 'null') {
        $UT->whereIn('official_undertime.status_id',$stat);
    }
    if ($company_id[0] != 'null') {
      $UT->whereIn('employee_company.company_id',$comp);
    }
    if($employee_id[0] != 'null') {
      $UT->whereIn('official_undertime.emp_id',$employee_id);
    }
    if( $start != 'null' && $end != 'null'){
      $UT->whereBetween('official_undertime.date_applied', [$start_date,$end_date]);
    }

    if (in_array(1, $type_id)) $main_query->unionAll($COA);
    if (in_array(2, $type_id)) $main_query->unionAll($leave);
    if (in_array(3, $type_id)) $main_query->unionAll($OB);
    if (in_array(4, $type_id)) $main_query->unionAll($OT);
    if (in_array(5, $type_id)) $main_query->unionAll($SA);
    if (in_array(6, $type_id)) $main_query->unionAll($UT);

    $main_query->orderBy('date_applied');
    $data = $main_query->get();
    return $data;

  }


  public static function getFilterEmployee($data){
    $company_id = $data['company_id'];
    $comp = [];
    $len = count($company_id);
    for ($x=0; $x < $len ; $x++) {
        $comp[$x]= (int)$company_id[$x];
    }

    $data = DB::table('employee')
    ->join('employee_company', 'employee.id', '=', 'employee_company.employee_id')
    ->select(
      DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
      DB::raw('employee.id As id')
      )
    ->whereIn('employee_company.company_id',$comp)
    ->orderBy('name')
    ->get();

    return $data;
  }

  public static function getBranch(){

    $data = DB::table('company_branch')
    ->select('id','branch_name as text')
    ->where('deleted_at','=',null)
    ->orderBy('text')
    ->get();

    return $data;
  }

  public static function getBranchByCompany($id){

    $data = DB::table('company_branch')
    ->select('id','branch_name as text')
    ->where('company_id','=',$id)
    ->where('deleted_at','=',null)
    ->orderBy('text')
    ->get();

    return $data;
  }

  public static function getEmployees($data){

    // $supervisor_id = $data['supervisor_id'];

    $employee = DB::table('employee')
    ->select(
      DB::raw('employee.id AS id'),
      DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `text`')
      )
    ->join('employee_company', 'employee_company.employee_id', '=', 'employee.id')
    ->join('company', 'company.id', '=', 'employee_company.company_id')
    ->whereRaw('employee.deleted_at IS NULL')
    ->whereRaw('employee.deleted_by IS NULL')
    ->where('employee.status', 1)
    ->orderBy('text');

    if(!empty($data['company_id'])){
      $employee = $employee->where('company.id', $data['company_id']);
    }

    $employee = $employee->get();

    return $employee;
  }

  public static function getBioDevice(){

    $data = DB::table('biometrics_device')
    ->select('ip_address as id','name as text','id as ids')
    ->where('deleted_at','=',null)
    ->orderBy('text')
    ->get();

    return $data;
  }

  public static function showRequest($data){

    $table = $data['table'];
    $id = $data['id'];

    $data = DB::table(
              DB::raw("(select `employee_user`.`user_id`, `$table`.`emp_id` from `$table` inner join `employee` on `$table`.`emp_id` = `employee`.`id` inner join `employee_user` on `employee`.`id` = `employee_user`.`employee_id` where `$table`.`ids` = '$id') As `t`"
              )
            )->first();

    return $data;
  }

  public static function updateRequest($data){

      $user = CommonModel::getUser();

      $created_by = $user->user_id;
      $id = $data['id'];
      $status_id = $data['status_id'];
      $remarks = $data['remarks'];
      $url = $data['url'];
      $status = '';
      $title = '';
      $description = '';
      $notif_id = [];


      $date = date('Y-m-d H:i:s');

      if ($url == 'certificate_of_attendance') {
        $data['table'] = 'certi_of_atten';
        $data['route'] = 'my-coa-list';
        $data['text'] = 'Certificate of Attendance Request';
      }
      elseif ($url == 'leave') {
        $data['table'] = 'leave';
        $data['route'] = 'my-leave-list';
        $data['text'] = 'Leave Request';
      }
      elseif ($url == 'official_business') {
        $data['table'] = 'official_business';
        $data['route'] = 'my-official-business-list';
        $data['text'] = 'Official Business Request';
      }
      elseif ($url == 'overtime') {
        $data['table'] = 'overtime';
        $data['route'] = 'my-overtime-list';
        $data['text'] = 'Overtime Request';
      }
      elseif ($url == 'schedule_adjustments') {
        $data['table'] = 'schedule_adjustments';
        $data['route'] = 'my-schedule-adjustments-list';
        $data['text'] = 'Schedule Adjustments Request';
      }
      elseif ($url == 'official_undertime') {
        $data['table'] = 'official_undertime';
        $data['route'] = 'my-undertime-list';
        $data['text'] = 'Undertime Request';
      }

      $table = $data['table'];
      $route = $data['route'];
      $text = $data['text'];

      if ($status_id == 1) {
          $status = '';
      }
      elseif ($status_id == 2) {
          $status = 'Rejected';
      }
      elseif ($status_id == 3) {
          $status = 'Cancelled';
      }
      //update attendance when status is approved
      elseif ($status_id == 4) {
          $status = 'Approved';
          if ($url == 'certificate_of_attendance') {
            $contr = new CertificateOfAttendanceController;
            $contr->updateApprovedRequest($id);
          }
          elseif ($url == 'leave') {
            $contr = new LeaveController;
            $contr->updateApprovedRequest($id,$created_by);
          }
          elseif ($url == 'official_business') {
            $contr = new OfficialBusinessController;
            $contr->updateApprovedRequest($id);
          }
          elseif ($url == 'overtime') {
            $contr = new OvertimeController;
            $contr->updateApprovedRequest($id);
          }
      }

      if($remarks == null) {
        $title = $status .' ' . $text;
        $description = $status .' ' . $text;

         DB::table($table)->where('id',$id)->update([
          'status_id'         => $status_id,
          'updated_by'        => $created_by,
          'updated_at'        => $date
          ]);
      }
      elseif ($remarks != null) {
        $title = $status .' ' . $text;
        $description = $remarks;

          DB::table($table)->where('id',$id)->update([
              'status_id'         => $status_id,
              'final_remarks'     => $remarks,
              'updated_by'        => $created_by,
              'updated_at'        => $date
          ]);
      }

      $receiver_id = null;
      $temp=DB::table($table)->where('id',$id)->select('emp_id')->first();
      $receiver_id = CommonModel::getUserId($temp->emp_id);

      if ($receiver_id != null) {
        $notif_id[]=DB::table('notification')->insertGetId(
                array(
                    'title'          => $title,
                    'description'    => $description,
                    'messenger_id'   => $created_by,
                    'table_from'     => $table,
                    'table_from_id'  => $id,
                    'seen'           => 0,
                    'route'          => $route,
                    'receiver_id'    => $receiver_id->user_id,
                    'icon'           => 'fa fa-pencil-square text-aqua',
                    'created_by'     => $created_by,
                    'created_at'     => $date
                    )
        );
      }

      return $notif_id;
    }

    public static function getPosition(){

      $data = DB::table('position')
      ->select('id','name as text')
      ->where('deleted_at','=',null)
      ->orderBy('text')
      ->get();

      return $data;
    }

    public static function getWorkingSchedule($id){
      $filter = DB::table('working_schedule')->whereRaw('deleted_at IS NULL AND deleted_by IS NULL')->orderBy('time_in')->orderBy('time_out');

      $filter->select('id', 'time_in', 'time_out', 'lunch_mins_break', DB::raw('lunch_break_time as break_start'), DB::raw('\'\' AS break_end'), DB::raw('CONCAT(time_in, " - ",time_out) AS `text`'));

      if($id != 0) {
        $filter->where('company_id','=',$id);
      }

      $data = $filter->get();

      for ($i=0; $i < count($data); $i++) {

        if ($data[$i]->break_start == null) {
          $data[$i]->break_start = '12:00:00';
        }
        if ($data[$i]->lunch_mins_break == null) {
          $data[$i]->lunch_mins_break = '60';
        }

        $data[$i]->break_start =  $data[$i]->break_start;
        $hour  = Carbon::parse( $data[$i]->break_start)->addMinutes( $data[$i]->lunch_mins_break);
        $convert = $hour->toDateTimeString();
        $data[$i]->break_end  = substr($convert, 11);
        $data[$i]->text = $data[$i]->text .' / '. $data[$i]->break_start .' - '. $data[$i]->break_end;
      }

      return $data;
    }

    public static function getWorkingScheduleWithoutBreak(){

      $data = DB::table('working_schedule')
      ->select(
        'id',
        'time_in',
        'time_out',
        'lunch_mins_break',
        DB::raw('lunch_break_time as break_start'),
        DB::raw('\'\' AS break_end'),
        DB::raw('CONCAT(time_in, " - ",time_out) AS `text`')
      )
      ->where('deleted_at','=',null)
      ->orderBy('id')
      ->get();

      return $data;
    }

    public static function getOnboarding(){

    $data = DB::table('onboarding')
    ->select('id','name',DB::raw('\'\' AS val'))
    ->where('deleted_at','=',null)
    ->orderBy('name')
    ->get();

    for ($i=0; $i < count($data); $i++) {
        $data[$i]->val =  false;
    }

    return $data;
  }

  public static function getClearance(){

    $data = DB::table('clearance')
    ->select('id','name',DB::raw('\'\' AS val'),DB::raw('\'\' AS checker_id'))
    ->where('deleted_at','=',null)
    ->orderBy('name')
    ->get();

    for ($i=0; $i < count($data); $i++) {
        $data[$i]->val =  false;
        $data[$i]->checker_id =  0;
    }

    return $data;
  }

  public static function getSuggestedSchedulesAdjustment(){
    return ScheduleAdjustmentsModel::getSuggestedSchedulesAdjustment();
  }

  public static function checkHoliday($data){

    $date = $data["date"];
    $return = [];

    $return = DB::table('holiday')
        ->join('holiday_type', 'holiday.holiday_type_id', '=', 'holiday_type.id')
        ->select(
          'holiday.date',
          'holiday.holiday_type_id as type',
          DB::raw('\'\' AS val'),
          'holiday_type.name as holiday',
          'holiday.name',
          'holiday.id'
        )
        ->whereIn('holiday.date',$date)
        ->where('holiday.deleted_at','=',null)
        ->get();

    for ($i=0; $i < count($return); $i++) {
        $return[$i]->val = false;
    }

    return $return;
  }
}
