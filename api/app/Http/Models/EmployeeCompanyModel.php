<?php
namespace App\Http\Models;

use \DB; 
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\UserModel;

use Illuminate\Support\Facades\Hash;

class EmployeeCompanyModel extends Model
{
    protected $table = "employee_company"; use SoftDeletes;
}
