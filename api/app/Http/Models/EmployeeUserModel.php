<?php
namespace App\Http\Models;

use \DB; 
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\UserModel;

use Illuminate\Support\Facades\Hash;

class EmployeeUserModel extends Model
{
    protected $table = "employee_user";
    use SoftDeletes;
    protected $hidden = ['updated_at'];

}
