<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Http\Request;
use App\Http\Models\Arrays;
use \stdClass;

class DevelopmentModel extends Model
{
    protected $table = "development"; use SoftDeletes;

    public function showDevelopment($data){
    	$employee_id = $data['employee_id'];

    	return $query = DB::table('development')
    				->select('id', 'development', 'achieve_role', 'development_role', 'due')
    				->where('employee_id', '=', $employee_id)
    				->where('deleted_by', '=', NULL)
    				->get();
    }
}
