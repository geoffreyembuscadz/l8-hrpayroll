<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use \DB;


class NotificationModel extends Model
{

    public static function getNotification(){

    	$user = CommonModel::getUser();
    	$id = $user->user_id;

	    $data = DB::table('notification')
	    ->leftJoin('users', 'notification.messenger_id', '=', 'users.id')
	    ->leftJoin('employee_user', 'users.id', '=', 'employee_user.user_id')
        ->leftJoin('employee', 'employee_user.employee_id', '=', 'employee.id')
	    ->select(
	    	DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
	    	DB::raw('CAST(notification.created_at as date) AS `date_create`'),
	    	DB::raw('null AS remarks'),
	    	'users.name as user_name',
	    	'notification.id',
	    	'notification.description',
	    	'notification.messenger_id',
	    	'notification.table_from',
	    	'notification.table_from_id',
	    	'notification.seen',
	    	'notification.route',
	    	'notification.icon',
	    	'notification.title'
		)
	    ->where('notification.deleted_at','=',null)
	    ->where('notification.receiver_id', 'LIKE', '%' . $id . '%')
	    ->orderBy('notification.created_at', 'desc')
	    ->limit(9)
	    ->get();

	    return $data;
  	}

  	public static function updateNotification($data){

  		$user = CommonModel::getUser();
	    $date = date('Y-m-d H:i:s');
	    $id = $data['id'];

	    $icon = trim($data['icon'], 'text-aqua');

        $ids=DB::table('notification')->where('id',$id)->update([
            'seen'       		=> 1,
            'icon'       		=> $icon,
            'updated_by'        => $user->user_id,
            'updated_at'        => $date
        ]);

        $notif[]= $id;

	    return $notif;
  	}

  	public static function getNotif($data){
  		$id = $data['id'];
  		$user_id = $data['user_id'];

  		$d = DB::table('notification')
	    ->leftJoin('users', 'notification.messenger_id', '=', 'users.id')
	    ->leftJoin('employee_user', 'users.id', '=', 'employee_user.user_id')
        ->leftJoin('employee', 'employee_user.employee_id', '=', 'employee.id')
	    ->select(
	    	DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
	    	DB::raw('CAST(notification.created_at as date) AS `date_create`'),
	    	'users.name as user_name',
	    	'notification.description',
	    	'notification.messenger_id',
	    	'notification.table_from',
	    	'notification.table_from_id',
	    	'notification.seen',
	    	'notification.route',
	    	'notification.icon',
	    	'notification.title'
		)
	    ->where('notification.deleted_at','=',null)
	    ->where('notification.receiver_id', 'LIKE', '%' . $user_id . '%');

    	$i = [];
        $len = count($id);
        for ($x=0; $x < $len ; $x++) { 
            $i[$x]= (int)$id[$x];
        }
        
        $d->whereIn('notification.id',$i);

	    $data = $d->get();
        return $data;
  	}


  	public static function getAllNotification($data){

  		$user = CommonModel::getUser();
    	$id = $user->user_id;

  		$start = $data['start_date'];
        $end = $data['end_date'];
 
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

	    $filter = DB::table('notification')
	    ->leftJoin('users', 'notification.messenger_id', '=', 'users.id')
	    ->leftJoin('employee_user', 'users.id', '=', 'employee_user.user_id')
        ->leftJoin('employee', 'employee_user.employee_id', '=', 'employee.id')
	    ->select(
	    	DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
	    	DB::raw('CAST(notification.created_at as date) AS `date_create`'),
	    	DB::raw('null AS remarks'),
	    	'users.name as user_name',
	    	'notification.id',
	    	'notification.description',
	    	'notification.messenger_id',
	    	'notification.table_from',
	    	'notification.table_from_id',
	    	'notification.created_at',
	    	'notification.seen',
	    	'notification.route',
	    	'notification.icon',
	    	'notification.title',
	    	'notification.created_at'
		)
	    ->where('notification.deleted_at','=',null)
	    ->where('notification.receiver_id', 'LIKE', '%' . $id . '%')
	    ->orderBy('notification.created_at', 'desc');

	    if( $start != 'null' && $end != 'null'){
            $filter->whereRaw(" CAST(notification.created_at as date) between '$start_date' and '$end_date'");
        }

        $data = $filter->get();
	    return $data;
  	}

  	public static function updateAllNotif($data){

  		$user = CommonModel::getUser();
    	$id = $user->user_id;

  		$date = date('Y-m-d H:i:s');
  		$ids = [];

  		$da=DB::table('notification')
	    ->select(
	    	'notification.id',
	    	'notification.icon'
		)
	    ->where('notification.seen','=',0)
	    ->where('notification.receiver_id', 'LIKE', '%' . $id . '%')
	    ->get();

	    for ($i=0; $i <count($da) ; $i++) { 

  			$icon = trim($da[$i]->icon, 'text-aqua');

	  		$ids[]=DB::table('notification')->where('id',$da[$i]->id)->update([
	            'seen'       		=> 1,
	            'icon'       		=> $icon,
	            'updated_by'        => $id,
	            'updated_at'        => $date
	        ]);
	    	
	    }

	    return $ids;
  	}
}
