<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Http\Request;

class RatingModel extends Model
{
    protected $table = "rating"; use SoftDeletes;

    public static function showRating(){
    	$query = DB::table('rating')
    			->select('id', 'rating', 'description')
    			->where('deleted_by', '=', NULL)
    			->get();
    	return $query;
    }
}
