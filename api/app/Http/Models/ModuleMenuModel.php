<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\ModuleMainModel as ModuleMain;

class ModuleMenuModel extends Model
{
    public $table = "module_menus"; use SoftDeletes;

    public function menu() {
    	return $this->belongsTo('App\Http\Models\ModuleMainModel', 'id');
    }

    public function permission() {
    	return $this->hasMany('App\Http\Models\PermissionModel', 'parent_id');
    }
    
    public function preListJoin($model) {
    	return $model->join('modules', 'module_menus.parent_id', '=', 'modules.id');
    }

    public function postListJoin($model) {
    	return $model->select(
            'module_menus.id', 
            'module_menus.name',
            'module_menus.display_name', 
            'module_menus.description', 
            'module_menus.action_url', 
            'modules.display_name as module_name'
        );
    }


   


}
