<?php
namespace App\Http\Models;

use \DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model; 
use \Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTime;


class AttendanceReportModel extends Model
{
	protected $table = "attendancereport"; use SoftDeletes;

	public static function attendReport(){
		return self::select('id',
				 DB::raw('company_id as company'), 
				 DB::raw('department_id as department'), 
				 DB::raw('employee_id as employee'))
			   ->get();

	}

	public static function getActive($company, $department, $employee)/*, $date, $is_present*/{
		$active = DB::table('employees_logs AS e_l')
				->join('employee AS e', 'e_l.employee_id', '=', 'e.id')
				->join('department', 'e.department', '=', 'department.id')
				->join('employee_company AS e_c','e.id','=','e_c.employee_id')
				->join('company AS c','e_c.company_id','=','c.id')
				->select(
		// DB::raw('cast(time_in as date) as date_given'),
			DB::raw('COUNT(DISTINCT employee_id) AS attendance_count'))
		->where('c.id','=','1');

		$active = $active->get()->toArray();

		$actives = array_map(function ($e) {
			return is_object($e) ? $e->attendance_count : $e['attendance_count'];
		}, $active);

		return $actives;

	}

	public static function selectedDate($start_date,$end_date) {
		$dateSelected = DB::table('employees_logs')
						->select( DB::raw('DISTINCT Cast(time_in AS date) as selected_date'))
						->whereBetween(DB::raw('CAST(time_in AS date)'), [$start_date, $end_date])
						->get()->toArray();

		$dates = array_map(function ($e) {
			return is_object($e) ? $e->selected_date : $e['selected_date'];
		}, $dateSelected);

		return $dateSelected;

	}    

	public static function getPresent($date){
		$query = DB::table(
			DB::raw("(
				SELECT COUNT(e_l.employee_id) AS present
				FROM employees_logs e_l
				JOIN employee e ON e_l.employee_id = e.id
				WHERE DATE(time_in) = CURDATE() AND e.status = 1

			) As `a`
			")
		)->get();
		return $query;
	}

	public static function getTotalEmp($filter){
		$totalEmp = DB::select('Select count(id) as total_employee FROM employee where employment_status = "'.$filter.'" AND deleted_at IS NOT NULL OR deleted_by IS NOT NULL');
		return $totalEmp;
	}

	public static function pendingLeave(){
		$pendingLeave = DB::table('leave')
					->join('status_type', 'leave.status_id', '=', 'status_type.id')
					->select(
						DB::raw('Count(status_id) As pending_leave'))
					->where('status_id', '=', '1')->get();
		return $pendingLeave;

	}   

	public function report($date){
		$empList = $this->employee_list();
		$emp_id = [];
		for ($a=0, $b = count($empList); $a < $b; $a++) { 
			 $emp_id[$a] = $empList[$a]->id;
		}
		$present_count = DB::table('employees_logs')
						->select(DB::raw('Count(DISTINCT employee_id) As present_count'))
						->whereDate('employees_logs.time_in', '=', date('Y-m-d'))
						->get();

		$absent_count =	DB::table(
							DB::raw("(
								SELECT COUNT(e.id) As absent_count
								FROM employee e
								WHERE e.id NOT IN (
									SELECT e_l.employee_id FROM employees_logs e_l
								)
							)As `t` "))->get();						

		$paid_leave_count = DB::table('leave')
						->join('leave_request', 'leave.id', '=', 'leave_request.leave_id')
						->select(DB::raw('Count(leave.emp_id) As paid_leave_count'))
						->where('leave_request.with_pay', '=', '1')
						->where('leave.status_id', '=', '4')
						->where(DB::raw('Cast(leave.date_from As date)', '=', $date))
						->get();

		$unpaid_leave_count = DB::table('leave')
						->join('leave_request', 'leave.id', '=', 'leave_request.leave_id')
						->select(DB::raw('Count(leave.emp_id) As unpaid_leave_count'))
						->where('leave_request.with_pay', '=', '0')
						->where('leave.status_id', '=', '4')
						->where(DB::raw('Cast(leave.date_from As date)', '=', $date))
						->get();

		$final_data = array(0 => array($present_count[0]->present_count),
				            1 => array($absent_count[0]->absent_count),
				            2 => array($paid_leave_count[0]->paid_leave_count),
				            3 => array($unpaid_leave_count[0]->unpaid_leave_count));
		$single = array_reduce($final_data, 'array_merge', array());

		return $single;

	}

	public static function lateErlyRprt($data){	
		$date = date('Y-m-d');

		$query = DB::table(
				DB::raw("(
					SELECT
					CONCAT(e.firstname,' ',e.lastname) AS employee_name,
					w_s.time_in As start_shift,
					w_s.time_out As end_shift,
					CAST(e_l.time_in AS time) As emp_in,
					CAST(e_l.time_out AS time) As emp_out
					FROM working_schedule w_s
					JOIN employee_working_schedule ews ON w_s.id = ews.working_schedule_id
					JOIN employee e ON ews.employee_id = e.id
					JOIN employees_logs e_l ON e.id = e_l.employee_id
					WHERE DATE(e_l.time_in) = CURDATE()

					) As `t`"))->get();
		return $query;	
	}


	public static function lateErlyFltr($data){	
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];
		$employee_id = $data['employee_id'];
		$date = date('Y-m-d');

		$query = DB::table('working_schedule')
				->join('employee_working_schedule AS ews', 'working_schedule.id', '=', 'ews.working_schedule_id')
				->join('employee', 'ews.employee_id', '=', 'employee.id')
				->join('employees_logs','employee.id','=','employees_logs.employee_id')
				->select(
					DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `employee_name`'),
					DB::raw('working_schedule.time_in As start_shift'),
					DB::raw('working_schedule.time_out As end_shift'),
					DB::raw('Cast(employees_logs.time_in As time) As emp_in'),
					DB::raw('Cast(employees_logs.time_out As time) As emp_out'));
				// ->whereDate('employees_logs.time_in', '=', date('Y-m-d'))	


		if( $start_date != 'null' && $end_date != 'null'){
            $query->whereBetween('employees_logs.time_in', [$start_date, $end_date]);   
        }
        if($employee_id != 'null') {
        	$query->whereIn('employee.id',$employee_id);
        }
        $data = $query->get();
        return $data;
	}

	public static function getHoliday($model){
		return $model = DB::table('holiday')
						->select(['holiday.holiday_type_id'])
						->whereRaw("date_format(holiday.date, '%m-%d') = date_format('$date', '%m-%d')")
						->first();
	}

	public function statusReport($data){
		$id = $data['emp_id'];
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];	
		$date_array = self::getDatesFromRange($start_date, $end_date);

		$date = [];
		for ($x=0, $y=count($date_array); $x < $y; $x++) { 
			$date[$x] = $date_array[$x];
		}
		$query = DB::table(
				DB::raw("(
					SELECT 
					`leave`.emp_id AS id,
					leave_request.date As date,
					CASE WHEN leave.status_id = 4 AND leave_request.with_pay = 1 THEN 
					(
						SELECT TIMESTAMPDIFF(HOUR, w_s.time_in, w_s.time_out)
						FROM   working_schedule w_s
						JOIN employee_working_schedule ews ON  w_s.id = ews.working_schedule_id
						WHERE ews.employee_id = '$id'
					) 

					WHEN leave.status_id = 4 AND leave_request.with_pay = 0 THEN '0'
					END As hours_count,
					CASE WHEN leave.status_id = 4 AND leave_request.with_pay = 1 THEN 'PL'
					WHEN leave.status_id = 4 AND leave_request.with_pay = 0 THEN 'LOP'
					END As status
					FROM `leave`
					JOIN leave_request ON `leave`.id = leave_request.leave_id
					WHERE   `leave`.emp_id = '$id' 
					AND leave_request.date BETWEEN '$start_date' AND '$end_date'

					UNION

					SELECT e.id AS id,
					CAST(e_l.time_in AS DATE) AS date,
					CASE WHEN e_l.time_in IS NOT NULL THEN TIMESTAMPDIFF(HOUR, e_l.time_in, e_l.time_out)
					END AS hours_count,
					'P' AS status
					FROM   employee e
					LEFT JOIN employees_logs e_l
					ON  e.id = e_l.employee_id
					LEFT JOIN employee_company e_c
					ON  e.id = e_c.employee_id
					LEFT JOIN company c
					ON  e_c.company_id = c.id
					JOIN employee_working_schedule ews
					ON  e.id = ews.employee_id
					JOIN working_schedule w_s
					ON  ews.working_schedule_id = w_s.id
					WHERE  e.id = '$id'

					UNION

					SELECT '$id',
					CASE WHEN holiday.date BETWEEN '$start_date' AND '$end_date' THEN (
						SELECT holiday.date
					)
					END  AS DATE,
					CASE WHEN holiday.date BETWEEN '$start_date' AND '$end_date' THEN 
					(
						SELECT TIMESTAMPDIFF(HOUR, w_s.time_in, w_s.time_out)
						FROM   working_schedule w_s
						JOIN employee_working_schedule ews
						ON  w_s.id = ews.working_schedule_id

					)
					END  AS hours_count,
					'H' AS status
					FROM   holiday

					) As `t`"))->get();
		return $query;			
	}
	public function getAbsnt($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];
		$date_array = self::getDatesFromRange($start_date, $end_date);

		$day = ['0']; //pag empty di mag eeror
		foreach ($date_array as $date) {
			$day[] = 'name LIKE %'.date('l', strtotime($date)).'%';
		}

		$query = DB::table(
				DB::raw(
					"
					(
						SELECT e.id,
						CONCAT(employee.firstname,' ',employee.lastname) AS employee_name
						FROM
						employee e
						LEFT JOIN employee_working_schedule ews ON e.id = ews.employee_id
						JOIN employees_logs e_l ON e.id = e_l.employee_id
						JOIN working_schedule w_s ON ews.working_schedule_id = w_s.id
						WHERE 
					)
					AS `a`
					"
				)
		);
		$arr = $query->get();
		return $arr;
	}

	/**
	 * Get an array of dates given two dates
	 * @param date string
	 * @param date string
	 * @return array
	 */
	private static function getDatesFromRange($date_time_from, $date_time_to) {
		$start = Carbon::createFromFormat('Y-m-d', substr($date_time_from, 0, 10));
		$end = Carbon::createFromFormat('Y-m-d', substr($date_time_to, 0, 10));
		$dates = [];
		while ($start->lte($end)) {
			$dates[] = $start->copy()->format('Y-m-d');
			$start->addDay();
		}
		return $dates;
	}

	public function daysSummaryReport($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$temp = [];
		$work = $this->workedDays($data);
		$logs = $this->logs($data);
		$employee = $this->employee_list();
		$expctdPybleDays = $this->expectedPayableDays($employee,$data,$logs);
		$active = $this->present($employee, $data);
		$holi = $this->holiday($employee, $data);
		$leave = $this->leave($data);
		$weekend = $this->weekend($data);
		$unpaidLeave = $this->unpaidLeave($data);
		$absntCnt = $this->absntCnt($data, $employee);
		$countDays = $this->countDays($data);

		for ($i=0, $j = count($employee); $i < $j; $i++) { 
			for ($x=0, $y = count($work); $x < $y; $x++) { 

				$employee[$i]->worked_days = 0;
				if(isset($work[$x]) && $employee[$i]->id == $work[$x]->code){						
					$employee[$i]->worked_days = $work[$x]->worked_days;					
				}
				$employee[$i]->absent_count = 0;
				if (isset($absntCnt[$x]) && $employee[$i]->id == $absntCnt[$x]->code) {
					$employee[$i]->absent_count = $absntCnt[$x]->absent_count;
				}
				// $employee[$i]->days = 0;
				// if (isset($expctdPybleDays[$x]) && $employee[$i]->id == $expctdPybleDays[$x]->code) {
				// 	$employee[$i]->days = $expctdPybleDays[$x]->days;
				// }
				$employee[$i]->no_of_present = 0;
				if (isset($active[$x]) && $employee[$i]->id == $active[$x]->code) {
					$employee[$i]->no_of_present = $active[$x]->no_of_present;
				}
				$employee[$i]->leave_count = 0;
				if (isset($leave[$x]) && $employee[$i]->id == $leave[$x]->code) {
					$employee[$i]->leave_count = $leave[$x]->leave_count;
				}
				$employee[$i]->unpaid_leave = 0;
				if (isset($unpaidLeave[$x]) && $employee[$i]->id == $unpaidLeave[$x]->code) {
					$employee[$i]->unpaid_leave = $unpaidLeave[$x]->unpaid_leave;
				}	
				$holiday = count($holi);
				$employee[$i]->holi_count = 0;
				for ($a=0; $a < $holiday; $a++) { 
					$employee[$i]->holi_count = $holi[$a]->holi_count;
				}
				$employee[$i]->weekend_count = 0;
				for ($x=0; $x < $weekend; $x++) { 
					$employee[$i]->weekend_count = $weekend;
				}
				$employee[$i]->days = 0;
				for ($x=0, $y = count($expctdPybleDays); $x < $y; $x++) { 
					$employee[$i]->days = $expctdPybleDays;
				}
				$employee[$i]->total_hours = 0;
				for ($x=0, $y = count($countDays); $x < $y; $x++) { 
					$employee[$i]->total_hours = $countDays;
				}		
			}
		}
		return $employee;
	}
	public function employee_list(){
		$data = DB::table('employee')
         ->select('id',
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`')
            )
        ->get();
        return $data;
	}

	public function countDays($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];
		$date_array = self::getDatesFromRange($start_date, $end_date);
		$count = count($date_array);
		return $count;
	}

	public function logs($data){

		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$logs = DB::table('employee_working_schedule As ews')
				->join('employee', 'ews.employee_id', '=', 'employee.id')
			    ->join('working_schedule As ws', 'ews.working_schedule_id', '=', 'ws.id')
				->join('employees_logs As e_l', 'ews.employee_id', '=', 'e_l.employee_id')
				->select('employee.id As code',
					DB::raw('DAYNAME((e_l.time_in)) As dayName'),
					DB::raw('Cast(e_l.time_in as Date) date'))
				->whereBetween('e_l.time_in', [$start_date, $end_date])
				->get();

		return $logs;
	}

	public function pay_off($logs){
		$id = $logs->code;
		$dayName = $logs->dayName;
		$num = 0;		
		$data[] = DB::table('employee_working_schedule')
			         ->join('working_schedule', 'employee_working_schedule.working_schedule_id', '=', 'working_schedule.id')
			         ->select('working_schedule.id', 'working_schedule.days')
			         ->where('employee_working_schedule.employee_id','=',$id)
			         ->where('working_schedule.days', 'like', '%' . $dayName . '%')
			         ->get();

        if(!empty($data) && isset($data[0])){
        	$num = 1;
        }
	    return $num;
	}

	public function workedDays($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$b = DB::table('employees_logs')
	 			->join('employee', 'employees_logs.employee_id', '=', 'employee.id')
	 			->select('employee.id As code',
	 				DB::raw('Count(DISTINCT employees_logs.time_in) As worked_days'))
	 			->whereBetween('employees_logs.time_in', [$start_date, $end_date])
	 			->groupBy('code')
	 			->get();
	 	return $b;
	}

	public function expectedPayableDays($employee,$data,$logs) {	
	 	$start_date = $data['start_date'];
		$end_date = $data['end_date'];
		$date_array = self::getDatesFromRange($start_date, $end_date);
		
		// exclude weekend
			$end = [];
			foreach ($date_array as $day) {
				$end[] = date('l', strtotime($day));
			}
			$arr = ['Saturday', 'Sunday'];
			$array = array_intersect($end, $arr);
			$dates = count(array_diff($end, $array));

			return $dates;
	}
	public function present($employee, $data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$query = DB::table('employees_logs')
	 			->join('employee', 'employees_logs.employee_id', '=', 'employee.id')
	 			->join('employee_working_schedule AS ews', 'employees_logs.employee_id', '=', 'ews.employee_id')
	 			->join('working_schedule As w_s', 'ews.working_schedule_id', '=', 'w_s.id')
	 			->select('employee.id As code',
	 				DB::raw('Count(DISTINCT employees_logs.time_in) As no_of_present'))
	 			->whereBetween('employees_logs.time_in', [$start_date, $end_date])
	 			->groupBy('code')
	 			->get();
	 
	 	return $query;	 	
	}	
	public function holiday($employee, $data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		return $holiday = DB::table('holiday')
							->select(DB::raw('Count(holiday_type_id) As holi_count'))
							->whereBetween('date', [$start_date, $end_date])
							->get();		
	}

	public function leave($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$leave = DB::table('leave')
					->join('leave_request', 'leave.id', '=', 'leave_request.leave_id')
					->select('leave.emp_id As code',
						DB::raw('Count(leave.leave_type_id) As leave_count'))
					->where('leave_request.with_pay', '=', 1)
					->whereBetween('leave.date_applied', [$start_date, $end_date])
					->groupBy('code')
					->get();
		return $leave;
	}

	public function unpaidLeave($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$leave = DB::table('leave')
					->join('leave_request', 'leave.id', '=', 'leave_request.leave_id')
					->select('leave.emp_id As code',
						DB::raw('Count(leave.leave_type_id) As unpaid_leave'))
					->where('leave_request.with_pay', '=', 0)
					->whereBetween('leave.date_applied', [$start_date, $end_date])
					->groupBy('code')
					->get();
		return $leave;		
	}

	public function weekend($data) {
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];
		$from = strtotime($start_date);
	    $to = strtotime($end_date);

	    $diff = floor(abs($to-$from)/(60*60*24));   

	    $num  = floor($diff/7) * 2;      
	    $fromNum = date("N", $from);
	    $toNum = date("N", $to);

	    if ($toNum < $fromNum)
	       $toNum += 7;

	    $dayarr = range($fromNum, $toNum); 
	    $num += count(array_intersect($dayarr, array(6, 7, 13)));
	    return $num;   
	}

	public function absntCnt($data, $employee){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$id =[];
		$b = count($employee);
		for ($a=0; $a < $b; $a++) { 
			$id[$a] = $employee[$a]->id;
		}

		$inctv = DB::table('employee As e')
	            ->join('employees_logs As e_l', 'e.id', '=', 'e_l.employee_id', 'left')
	            ->select('e.id AS emp',
	            	DB::raw('COUNT(e.id) As absent_count'))
	            ->whereNotIn('e_l.employee_id', $id)
	            ->whereBetween('e_l.time_in', [$start_date, $end_date])
	            ->groupBy('emp')
	            ->get(); 
		return $inctv;
	}

	public function hoursSummaryReport($data){
		$employee = $this->employee_list();
		$wrkHrs = $this->totalWorkedHours($data);
		$expctdPybleHrs = $this->expectedPayableHours($data);
		$lvCnt = $this->empLeaveCount($data);
		$holiCnt = $this->holidayCount($data);
		// $pdOffHrs = $this->paidOffHours($data, $lvCnt, $holiCnt);
		$tmp = $this->temp($data);
		$unpdLv = $this->unpaidLeaveHours($data);
		$weekendHrs = $this->weekendHrs($data);
		$ot = $this->overtime($data);
		$xpctdWrkngHrs = $this->expectedWorkingHours($expctdPybleHrs, $weekendHrs);
		// dd($wrkHrs);exit;

		for ($i=0, $j = count($employee); $i < $j; $i++) { 
			for ($x=0, $y = count($wrkHrs); $x < $y; $x++) { 				
				if($employee[$i]->id == $wrkHrs[$x]->id){						
					$employee[$i]->worked_hours = $wrkHrs[$x]->hrs;					
				}
			}
			for ($x=0, $y = count($expctdPybleHrs); $x < $y; $x++) { 
				// $employee[$i]->expected_payable_hours = 0;
				if($employee[$i]->id == $expctdPybleHrs[$x]->employee_id){						
					$employee[$i]->expected_payable_hours = $expctdPybleHrs[$x]->total_hours;					
				}
			}
			for ($x=0, $y = count($lvCnt); $x < $y; $x++) { 
				// $employee[$i]->paid_leave = 0;
				if($employee[$i]->id == $lvCnt[$x]->emp_id){						
					$employee[$i]->paid_leave = $lvCnt[$x]->totalHrs;					
				}
			}
			for ($x=0, $y = count($unpdLv); $x < $y; $x++) { 
				// $employee[$i]->unpaid_leave = 0;
				if($employee[$i]->id == $unpdLv[$x]->emp_id){						
					$employee[$i]->unpaid_leave = $unpdLv[$x]->totalHrs;					
				}
			}
			for ($x=0, $y = count($weekendHrs); $x < $y; $x++) { 
				// $employee[$i]->weekend_hours = 0;
				if($employee[$i]->id == $weekendHrs[$x]->employee_id){						
					$employee[$i]->weekend_hours = $weekendHrs[$x]->total_hours;					
				}
			}
			for ($x=0, $y = count($ot); $x < $y; $x++) { 
				// $employee[$i]->ot_hours = 0;
				if($employee[$i]->id == $ot[$x]->emp_id){						
					$employee[$i]->ot_hours = $ot[$x]->ot_count;					
				}
			}
			for ($x=0, $y = count($xpctdWrkngHrs); $x < $y; $x++) { 
				// $employee[$i]->expected_working_hours = 0;
				if($employee[$i]->id == $xpctdWrkngHrs[$x]->employee_id){						
					$employee[$i]->expected_working_hours = $xpctdWrkngHrs[$x]->total_hours;					
				}
			}
			for ($x=0, $y = count($holiCnt); $x < $y; $x++) { 
				// $employee[$i]->holiday_hours = 0;
				if($employee[$i]->id == $holiCnt[$x]->employee_id){						
					$employee[$i]->holiday_hours = $holiCnt[$x]->total_hours;					
				}
			}
		}		
		return $employee;
		
	}

	public function totalWorkedHours($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$query = DB::table(
			DB::raw("
				(
					SELECT e.id, 
					SUM(TIMESTAMPDIFF(HOUR, e_l.time_in, e_l.time_out)) as hrs
					FROM
					employee e
					LEFT JOIN employees_logs e_l ON e.id = e_l.employee_id
					WHERE e_l.time_in BETWEEN '$start_date' AND '$end_date'
					GROUP BY e_l.employee_id	 
				) 
				As `t`
				")
		);
		$data = $query->get();
		return $data;
	}

	public function expectedPayableHours($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];
		$date_array = self::getDatesFromRange($start_date, $end_date);
		$num = count($date_array);
		$data = $this->temp($data);

		foreach ($data as $key => $value) {
			$data[$key]->total_hours *= $num;
		}
		return $data;				
	}

	public function empLeaveCount($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$query = DB::table(
			DB::raw("
				(
					SELECT l.emp_id,
					l.total_days * TIMESTAMPDIFF(HOUR, w_s.time_in, w_s.time_out) AS totalHrs
					FROM
					`leave` l
					JOIN leave_request l_r On l.id = l_r.leave_id
					JOIN employee_working_schedule On l.emp_id = employee_working_schedule.employee_id
					JOIN working_schedule w_s On employee_working_schedule.working_schedule_id = w_s.id
					WHERE l.status_id = 4 
					AND l_r.with_pay = 1
					AND l.date_from BETWEEN '$start_date' 
					AND '$end_date'	 
				) 
				As `t`
				")
		);
		$data = $query->get();
		
		return $data;
	}

	public function holidayCount($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$query = DB::table(
			DB::raw("
				(
					SELECT ews.employee_id,
					TIMESTAMPDIFF(HOUR, w_s.time_in, w_s.time_out) *
					(
						SELECT COUNT(h.date) 
						FROM
						holiday h
						WHERE h.date = '$start_date' OR h.date = '$end_date'
					)
					AS total_hours
					FROM working_schedule w_s
					JOIN employee_working_schedule ews ON w_s.id = ews.working_schedule_id 
	 
				) 
				As `t`
				")
		);
		$data = $query->get();
		return $data;

	}

	public function temp($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$query = DB::table('employee_working_schedule As ews')
					->join('working_schedule As w_s', 'ews.working_schedule_id', '=', 'w_s.id')
					->select('ews.employee_id',
							 DB::raw('TIMESTAMPDIFF(HOUR, w_s.time_in, w_s.time_out) As total_hours'))
					->get();
		return $query;

	}
	
	public function unpaidLeaveHours($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$query = DB::table(
			DB::raw("
				(
					SELECT l.emp_id,
					l.total_days * TIMESTAMPDIFF(HOUR, w_s.time_in, w_s.time_out) AS totalHrs
					FROM
					`leave` l
					JOIN leave_request l_r On l.id = l_r.leave_id
					JOIN employee_working_schedule On l.emp_id = employee_working_schedule.employee_id
					JOIN working_schedule w_s On employee_working_schedule.working_schedule_id = w_s.id
					WHERE l.status_id = 4 
					AND l_r.with_pay = 0
					AND l.date_from BETWEEN '$start_date' 
					AND '$end_date'
	 
				) 
				As `t`
				")
		);
		$data = $query->get();
		return $data;
	}

	public function weekendHrs($data){
		$weekend = $this->weekend($data);
		$data = $this->temp($data);

		foreach ($data as $key => $value) {
			$data[$key]->total_hours *= $weekend;
		}
		return $data;
	}

	public function overtime($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$query = DB::table('overtime')
				->select(
					DB::raw('overtime.emp_id'),
					DB::raw('Count(emp_id) As ot_count'))
				->where('overtime.status_id', '=', 4)
				->whereBetween('date', [$start_date, $end_date])
				->groupBy('overtime.emp_id')
				->get();

		return $query;
	}	

	public function expectedWorkingHours($expctdPybleHrs, $weekendHrs){
		foreach ($expctdPybleHrs as $key => $value) {
			$expctdPybleHrs[$key]->total_hours += $weekendHrs[$key]->total_hours * -1;
		}
		return $expctdPybleHrs;
	}

	public function userReport($data){
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];
		$emp_id = $data['emp_id'];
		$company_id = $data['company_id'];
		$date_array = self::getDatesFromRange($start_date, $end_date);
		$day = [];
		foreach ($date_array as $date) {
			$day[] = $date;
		}
		
		if (!empty($company_id)) {
			$query = DB::table(
				DB::raw("(
							SELECT employee_id
							FROM employee_company
							WHERE company_id = '$company_id'
						  ) As `a`
						")
			);
		}

		if ($emp_id != 'NULL') {
			$query = DB::table(
			DB::raw("
				(
					Select DISTINCT `leave`.emp_id,
					leave_request.date,
					CASE  WHEN  leave.status_id = 4 AND leave_request.with_pay = 1  THEN 'Paid Leave'
					WHEN leave.status_id = 4 AND leave_request.with_pay = 0  THEN 'Unpaid Leave'
					END AS status,
					NULL AS time_in,
					NULL AS time_out,
					NULL AS total_hours,					
					(
						SELECT CASE WHEN status = 'Paid Leave' THEN 
						(
							SELECT TIMESTAMPDIFF(HOUR, w_s.time_in, w_s.time_out) As pay_hrs
							FROM working_schedule w_s
							LEFT JOIN employee_working_schedule ews ON w_s.id = ews.working_schedule_id
							WHERE ews.employee_id = '$emp_id' 
						) 
						END 
					) As pay_hrs
					FROM `leave`
					JOIN leave_request On `leave`.id = leave_request.`leave_id`
					JOIN employees_logs AS e_l ON leave.emp_id = e_l.employee_id
					WHERE `leave`.emp_id = '$emp_id'	

					UNION

					SELECT e.id AS id, 
					CAST(e_l.time_in As date) date,
					CASE WHEN e_l.time_in IS NOT NULL THEN 'Present'
						
					ELSE 'Absent'
					END AS status,
					CAST(e_l.time_in As time) As time_in,
					CAST(e_l.time_out As time) As time_out,
					TIMESTAMPDIFF(HOUR, e_l.time_in, e_l.time_out) As total_hours,
					TIMESTAMPDIFF(HOUR, w_s.time_in, w_s.time_out) As pay_hrs
					FROM employee e
					LEFT JOIN employees_logs e_l ON e.id = e_l.employee_id
					LEFT JOIN employee_company e_c ON e.id = e_c.employee_id
					LEFT JOIN company c ON e_c.company_id = c.id
					JOIN employee_working_schedule ews On e.id = ews.employee_id
					JOIN working_schedule w_s On ews.working_schedule_id = w_s.id
					WHERE e.id = '$emp_id'

					UNION

					SELECT '$emp_id',
					CASE WHEN holiday.date BETWEEN '$start_date' AND '$end_date' THEN
					(SELECT holiday.date) 
					END AS date,
					CASE WHEN holiday.date BETWEEN '$start_date' AND '$end_date' THEN 'Holiday' 
					END AS status,
					NULL as time_in,
					NULL as time_out,
					NULL AS total_hours,
					(
						SELECT TIMESTAMPDIFF(HOUR, w_s.time_in, w_s.time_out) As pay_hrs
						FROM working_schedule w_s
						LEFT JOIN employee_working_schedule ews ON w_s.id = ews.working_schedule_id
						WHERE ews.employee_id = '$emp_id'
					)
					FROM holiday				 
				) 
				As `t`
				")
		);
		}
        $array = $query->get();        
		return $array;
	}

	public function getEmpByComp($data){
		$company_id = $data['company_id'];

		$query = DB::table(
			DB::raw("(
				SELECT e.id AS id,
				position.name As pos_name,
				e.email,
				CONCAT(e.firstname,' ',e.lastname) AS text, 
				d.name as dept, 
				c.name as comp,
				CASE WHEN e.supervisor_id != 0 THEN (SELECT 	CONCAT(e.firstname,' ',e.lastname)
				 from employee e
				 WHERE supervisor_id = e.id)
				end 
				FROM employee e
				JOIN employee_company e_c ON e.id = e_c.employee_id
				JOIN company c ON e_c.company_id = c.id
				JOIN position on e.position = position.id
				JOIN department d ON e.department = d.id
				WHERE e_c.company_id = '$company_id'
			) As `a`
			")
		);
		$data = $query->get();
		return $data;
	}

	 public function abs($data){
	 	$emp_id = $data['emp_id'];
	 	$start_date = $data['start_date'];
		$end_date = $data['end_date'];

	 	$logs = DB::table('employees_logs As e_l')
	 			 ->where('employee_id', '=', '$emp_id')
	 			 ->whereBetween('time_in', [$start_date, $end_date])
	 			 ->pluck(DB::raw('DAYNAME(Cast(time_in As date)) As date'),
	 			  		 DB::raw('CAST(time_in As date) AS main_key'));

	 	$sched = DB::table('working_schedule')
			 	->join('employee_working_schedule As ews', 'working_schedule.id', '=', 'ews.working_schedule_id')
			 	->select('ews.employee_id',
			 		DB::raw('working_schedule.days As dayName'))
			 	->where('employee_id', '=', '$emp_id')
			 	->get();

	 	$logs_to_array =  json_decode(json_encode($logs), true);
	 	$temp =  json_decode(json_encode($sched), true);
	 	$sched_transform = [];
	 	foreach ($temp as $key => $val) {
	 		$sched_transform[$key]['days'] = json_decode($val['dayName']);
	 	}

	 	// ITO AY PARA GAWING SINGLE ARRAY ANG NESTED ARRAY
	 	$sched_single_array = []; 
	 	foreach ($sched_transform as $key => $value) { 
	 		if (is_array($value)) { 
	 			//array_flatten nagcoconvert sa single 
	 			$sched_single_array = array_merge($sched_single_array, array_flatten($value));
	 		} else { 
	 			$sched_single_array[$key] = $value; 
	 		} 
	 	} 
	 	$count_absent = count(array_diff($sched_single_array, $logs_to_array));
	 	return $count_absent;
	}

	public function perEmpLateEarly($data){
		$id = $data['emp_id'];
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$query = DB::table(
			DB::raw("(
				SELECT CONCAT(employee.firstname,' ',employee.lastname) AS `employee_name`,
				CAST(employees_logs.time_in AS date) AS date_given, 
				working_schedule.time_in AS start_shift, 
				working_schedule.time_out AS end_shift, 
				CAST(employees_logs.time_in AS time) AS emp_in, 
				CAST(employees_logs.time_out As time) AS emp_out 
				FROM `working_schedule` 
				INNER JOIN `employee_working_schedule` AS `ews` ON `working_schedule`.`id` = `ews`.`working_schedule_id`
				INNER JOIN `employee` ON `ews`.`employee_id` = `employee`.`id` 
				INNER JOIN `employees_logs` ON `employee`.`id` = `employees_logs`.`employee_id`
				WHERE employee.id = '$id'
				AND employees_logs.time_in BETWEEN '$start_date' AND '$end_date'
			) As `a`
			")
		)->get();
		return $query;
	}

	public function perEmpDaysSummary($data){
		$id = $data['emp_id'];
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$array = (array) $this->daysSummaryReport($data);

		foreach ($array as $item) {   // iterate over all items
		    foreach($item AS $collection){ // multidimensional array sya!
		    	$collection = (array) $collection;
			    if ($collection["id"] == $id) {   // check if id matches
			    	$result = $collection;     // pag match, print this collection
			   	}
		    }
		}
		return $result;
	}

	public function perEmpHoursSummary($data){
		$id = $data['emp_id'];
		$start_date = $data['start_date'];
		$end_date = $data['end_date'];

		$array = (array) $this->hoursSummaryReport($data);

		foreach ($array as $item) {   // iterate over all items
		    foreach($item AS $collection){ // multidimensional array sya!
		    	$collection = (array) $collection;
			    if ($collection["id"] == $id) {   // check if id matches
			    	$result = $collection;     // pag match, print this collection
			   	}
		    }
		}
		return $result;
	}

	public function working_schedule($data){
		$id = $data['emp_id'];

		$query = DB::table(
			DB::raw("(
				SELECT w_s.time_in, w_s.time_out, w_s.days AS dayName
				FROM working_schedule w_s
				JOIN employee_working_schedule ews ON w_s.id = ews.working_schedule_id
				WHERE ews.employee_id = '$id'
			) As `a`
			")
		)->get();
	 	$temp =  json_decode(json_encode($query), true);
	 	$sched_transform = [];
	 	foreach ($temp as $key => $val) {
	 		$sched_transform[$key]['time_in'] = $val['time_in'];
	 		$sched_transform[$key]['time_out'] = $val['time_out'];
	 		$sched_transform[$key]['days'] = json_decode($val['dayName']);
	 	}
	 	return $sched_transform;
	}


	public static function filterTotalEmployee(){
		$query =  DB::table(
			DB::raw("(
				SELECT count(*) total_employee FROM (
				SELECT DISTINCT employee.id, days, LOWER(DAYNAME(NOW())) as today from employee 
				LEFT JOIN employee_working_schedule as ews ON employee.id = ews.employee_id
				LEFT JOIN working_schedule as ws ON ews.working_schedule_id = ws.id
				GROUP BY employee.id
				) as sched
				WHERE INSTR(sched.days, sched.today)
				) As `a`
				")
			)->get();

		return $query;
	}

}



