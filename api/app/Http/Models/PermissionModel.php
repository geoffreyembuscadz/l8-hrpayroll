<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class PermissionModel extends Model
{
    protected $table = "permissions"; use SoftDeletes;

    public function preList($model) {
    	 // return self::join('')
        return $model->join('module_menus', 'permissions.parent_id', '=', 'module_menus.id');
        
    }

    public function postList($model) {
    	 // return self::join('')
        return $model->select('permissions.id as id', 'permissions.name as text', 'permissions.display_name', 'permissions.description', 'permissions.parent_id', 'module_menus.display_name as menu_name');
    }

    public function permission() {
    	return $this->belongsTo('App\Http\Models\ModuleMenuModel', 'id');
    }
}
