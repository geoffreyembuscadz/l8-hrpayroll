<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;

class ModuleMainModel extends Model
{
    protected $table = "modules"; use SoftDeletes;

    public function menu() {
    	return $this->hasMany('App\Http\Models\ModuleMenuModel', 'parent_id');
    }


}
