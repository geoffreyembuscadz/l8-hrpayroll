<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class DepartmentModel extends Model
{
    protected $table = "department"; use SoftDeletes;
}
