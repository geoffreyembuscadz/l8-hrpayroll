<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class Event_DetailsModel extends Model
{
    protected $table = "event_details"; use SoftDeletes;
}
