<?php
namespace App\Http\Models;

use \DB; 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\UserModel;
use App\Http\Models\EmployeeUserModel;
use App\Http\Models\EmployeeRateModel;
use App\Http\Models\EmployeeCompanyModel;
use App\Http\Models\EmployeePositionModel;
use App\Http\Models\EmployeeDependentModel;
use App\Http\Models\EmployeeWorkingScheduleModel;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\UserController;

class EmployeeModel extends Model
{
    protected $table = "employee"; use SoftDeletes;

    public function preListJoin($model){
        return $model->whereRaw('employee.deleted_at IS NULL')
            ->join('employee_company', function($join){
                $join->on('employee.id', '=', 'employee_company.employee_id');
            })
            ->join('company', 'employee_company.company_id', '=', 'company.id')
            ->orderBy('employee.lastname');
    }

    public function postListJoin($model){
        return $model->select(DB::raw('DISTINCT employee.id'), 'employee.employee_code', 'employee.firstname', 'employee.middlename', 'employee.lastname', /*'department.name AS department', 'position.name AS position',*/ 'employee.status',  DB::raw("CONCAT( employee.firstname , ' ' , employee.lastname) as text"), 'company.name AS company', DB::raw('IF(employee.status = 1, "Active", "Inactive") AS real_status'));
    }

    public function setEmployeeCompanies($employee_id, $employee_companies = []){
        if(!empty($employee_companies['company_id'])){
            DB::table('employee')->where('id', $employee_id)->update(['company_id' => $employee_companies['company_id']]);
        }
        DB::table('employee_company')->insert($employee_companies);
    }

    public function setEmployeePosition($employee_id){
        $employee = self::where('id', $employee_id)->select('department', 'position')->first();

        DB::table('employee_position')->insert([
            'employee_id' => $employee_id,
            'department' => $employee['department'],
            'position'  => $employee['position'],
            'created_at'    => DB::raw('now()'),
            'created_by'    => 1
        ]);
    }

    public function tagEmployeeAttachments($data){
        DB::table('employee_attachment')->insert($data);
    }

    public function appendEmployeeLog($employee_id, $data){
        $insert_data = [
            'employee_id' => $employee_id,
            'latest_data' => json_encode($data),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => 1
        ];

        $existing = DB::table('employee_audit')->where('employee_id', $employee_id)->count();

        if($existing){
            // $insert_data['previous_data'] = json_encode(self::where('id', $employee_id)->first());
        } else {
            DB::table('employee_audit')->insert($insert_data);
        }
    }

    public function addUser($employee_id, $data = []){
        $username = (!empty(trim($data['email']))) ? strtolower($data['email']) : strtolower($data['firstname'] . '.' . $data['lastname']);
        $name = ucfirst($data['firstname']) . ' ' . ucfirst($data['lastname']);
        $password = Hash::make(strtolower($data['firstname'] . $data['lastname']));

        if(!empty($data['email'])){
            $user = new UserModel();
            $user->email = $data['email'];
            $user->name = $name;
            $user->password = $password;
            $user->created_at = date('Y-m-d H:i:s');
            $user->created_by = 1;
            $user->save();

            $user_id = $user->id;

            DB::table('employee_user')->insert([
                'employee_id'   => $employee_id,
                'user_id'       => $user_id
            ]);

            $role_id = DB::table('roles')->whereRaw("lcase(name) LIKE '%user%'")->first()->id;

            DB::table('role_user')->insert([
                'user_id' => $user_id,
                'role_id' => $role_id
            ]);
        }
    }

    public function setEmployeeDependencies($employee_id, $datas){
        if(isset($datas)){
            foreach($datas AS $data){
                if(!empty($data['employee_dependent_name']) && !empty($data['employee_dependent_age'])){
                    DB::table('employee_dependent')->insert([
                        'employee_id' => $employee_id,
                        'name'  => $data['employee_dependent_name'],
                        'relationship' => $data['employee_dependent_relationship'],
                        'age' => (!empty($data['employee_dependent_age'])) ? $data['employee_dependent_age'] : 0,
                        'pwd' => (!empty($data['employee_dependent_pwd'])) ? 1 : 0,
                        'created_by' => 1,
                        'created_at' => date('Y-m-d')
                    ]);
                }
            }
        }
    }

    public function setEmployeeRate($employee_id, $data = []){
        $update_rate = [
            'employee_id' => $employee_id,
            'company_id' => $data['primary_company'],
            'daily_rate' => 0,
            'weekly_rate' => 0,
            'monthly_rate' => 0,
            'effectivity_date_start' => date('Y-m-d H:i:s'),
            'effectivity_date_end' => null,
            'allowance_amount' => (isset($data['allowance_amount'])) ? (float)$data['allowance_amount'] : 0,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ];

        if(strtolower($data['salary_receiving_type']) == 'daily'){
            $update_rate['daily_rate'] = (float)$data['income_rate'];
        } else if(strtolower($data['salary_receiving_type']) == 'week'){
            $update_rate['weekly_rate'] = (float)$data['income_rate'];
        } else if(strtolower($data['salary_receiving_type']) == 'month'){
            $update_rate['monthly_rate'] = (float)$data['income_rate'];
        } else {
            $update_rate['daily_rate'] = (float)$data['income_rate'];
        }

        DB::table('employee_rate')->insert($update_rate);
    }

    public function updateEmployeeRate($employee_id, $data = []){
        $time_of_update = date('Y-m-d H:i:s');

        $update_employee_rate = [
            'employee_id' => $employee_id,
            'company_id' => $data['data']['primary_company'],
            'allowance_amount' => $data['data']['allowance_amount'],
            'effectivity_date_start' => date('Y-m-d H:i:s'),
            'updated_by' => 1,
            'updated_at' => $time_of_update,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => 1
        ];

        $if_previous = DB::table('employee_rate')->where('employee_id', $employee_id);

        if(strtolower($data['data']['salary_receiving_type']) == 'daily'){
            $update_employee_rate['daily_rate'] = $data['data']['income_rate'];
            $update_employee_rate['weekly_rate'] = 0;
            $update_employee_rate['monthly_rate'] = 0;
        } else if(strtolower($data['data']['salary_receiving_type']) == 'weekly'){
            $update_employee_rate['weekly_rate'] = $data['data']['income_rate'];
            $update_employee_rate['daily_rate'] = 0;
            $update_employee_rate['monthly_rate'] = 0;
        } else if(strtolower($data['data']['salary_receiving_type']) == 'monthly'){
            $update_employee_rate['monthly_rate'] = $data['data']['income_rate'];
            $update_employee_rate['weekly_rate'] = 0;
            $update_employee_rate['daily_rate'] = 0;
        } else {
            $update_employee_rate['daily_rate'] = $data['data']['income_rate'];
        }

        if($if_previous->count() > 0){
            DB::table('employee_rate')->where('employee_id', $employee_id)->update($update_employee_rate);
        } else {
            DB::table('employee_rate')->insert($update_employee_rate);
        }
        
        DB::table('employee_rate_log')->insert([
            'employee_id' => $employee_id,
            'company_id' => $data['data']['primary_company'],
            'daily_rate' => $data['data']['income_rate'],
            'created_by' => 1,
            'created_at' => $time_of_update
        ]);

        DB::table('employee')->where('id', $employee_id)->update([ 'salary_receiving_type' => $data['data']['salary_receiving_type'] ]);
    }

    public function setEmployeeEmergencyContacts($employee_id, $datas){
        if(isset($datas)){
            foreach($datas AS $data){
                if(!empty($data['emergency_contact_number']) && !empty($data['emergency_contact_number'])){
                    DB::table('employee_emergency_contact')->insert([
                        'employee_id' => $employee_id,
                        'person_name' => $data['emergency_person_name'],
                        'relationship' => $data['emergency_relationship'],
                        'contact_number' => $data['emergency_contact_number'],
                        'created_at'    => DB::raw('NOW()'),
                        'created_by'    => 1
                    ]);
                }
            }
        }
    }

    public function getEmployeeWorkingSchedule($employee_id){
        $data = [];
        $employee_working_schedule = EmployeeWorkingScheduleModel::where('employee_id', $employee_id)->pluck('working_schedule_id');
        
        if(count($employee_working_schedule) > 0){
            foreach($employee_working_schedule AS $index => $sched_id){
                $data[$index] = $sched_id;
            }
        }

        return $data;
    }

    public function getEmployeeDependencies($employee_id){
        $employee_dependencies = EmployeeDependentModel::where('employee_id', $employee_id)->select('id', 'name', 'relationship', 'age', 'birthdate', 'pwd')->get();

        return $employee_dependencies;
    }

    public function getEmployeeRate($id){
        $rate = 0;

        $employee = DB::table('employee')->where('id', $id)->get(); // getting employee_code

        if(isset($employee) && count($employee)){
            $employee_rate = DB::table('employee_rate')->where('employee_id', $id)->first();

            if(isset($employee_rate)){
                if(strtolower($employee[0]->salary_receiving_type) == 'daily'){
                    $rate = (float)$employee_rate->daily_rate;
                } else if(strtolower($employee[0]->salary_receiving_type) == 'weekly'){
                    $rate = (float)$employee_rate->weekly_rate;
                } else if(strtolower($employee[0]->salary_receiving_type) == 'monthly'){
                    $rate = (float)$employee_rate->monthly_rate;
                } else {
                    $rate = (float)$employee_rate->daily_rate;
                }
            }
        }

        return $rate;
    }

    public function getEmployeeAllowance($id){
        $allowance = 0;

        $employee = DB::table('employee')->where('id', $id)->get();

        if(isset($employee)){
            $allowance = DB::table('employee_rate')->where('employee_id', $id)->value('allowance_amount');
        }

        return $allowance;
    }

    public function appendEmployeeWorkingSchedule($employee_id, $working_schedule_ids){
        $employee_code = EmployeeModel::where('id', $employee_id)->first()->employee_code;
        $count_employee_schedule = EmployeeWorkingScheduleModel::where('employee_id', $employee_id)->count();

        if(!empty($working_schedule_ids)){
            foreach($working_schedule_ids AS $working_schedule_id){
                $employee_working_schedule = new EmployeeWorkingScheduleModel();
                $employee_working_schedule->employee_id = $employee_id;
                $employee_working_schedule->working_schedule_id = $working_schedule_id;
                $employee_working_schedule->approved = ($count_employee_schedule == 0) ? 1 : 0;
                $employee_working_schedule->created_by = 1;
                $employee_working_schedule->created_at = date('Y-m-d H:i:s');
                $employee_working_schedule->save();
            }
        } else {
            $company_id_from_employee_company = EmployeeCompanyModel::where('employee_id', $employee_id)->first()->company_id;
            $company_working_schedule = DB::table('working_schedule')->where('company_id', $company_id_from_employee_company)->first()->id;

            $employee_working_schedule = new EmployeeWorkingScheduleModel();
            $employee_working_schedule->employee_id = $employee_id;
            $employee_working_schedule->working_schedule_id = $company_working_schedule;
            $employee_working_schedule->approved = ($count_employee_schedule == 0) ? 1 : 0;
            $employee_working_schedule->created_by = 1;
            $employee_working_schedule->created_at = date('Y-m-d H:i:s');
            $employee_working_schedule->save();
        }
    }

    public function updateEmployeeWorkingSchedule($employee_id, $working_schedule_ids){
        $employee_code = EmployeeModel::where('id', $employee_id)->value('employee_code');
        $employee_current_working_schedule = $this->getLatestEmployeeWorkingSchedule($employee_id);

        if(is_array($working_schedule_ids)){
            // Instead of soft delete then update just update
            // EmployeeWorkingScheduleModel::where('employee_id', $employee_id)->where('approved', 0)->delete();

            // foreach($working_schedule_ids AS $working_schedule_id){
            //     $employee_working_schedule = new EmployeeWorkingScheduleModel();
            //     $employee_working_schedule->employee_id = $employee_id;
            //     $employee_working_schedule->working_schedule_id = $working_schedule_id;
            //     $employee_working_schedule->created_by = 1;
            //     $employee_working_schedule->created_at = date('Y-m-d H:i:s');
            //     $employee_working_schedule->save();
            // }
            DB::table('employee_working_schedule')->where('employee_id', $employee_id)->delete();

            foreach($working_schedule_ids AS $working_schedule_id){
                $employee_working_schedule = new EmployeeWorkingScheduleModel();
                $employee_working_schedule->employee_id = $employee_id;
                $employee_working_schedule->working_schedule_id = $working_schedule_id;
                $employee_working_schedule->approved = 1;
                $employee_working_schedule->approved_by = 1;
                $employee_working_schedule->created_by = 1;
                $employee_working_schedule->created_at = date('Y-m-d H:i:s');
                $employee_working_schedule->save();
            }
        }
    }

    public function getLatestEmployeeWorkingSchedule($employee_id){
        $employee_code = EmployeeModel::where('id', $employee_id)->pluck('employee_code');
        return EmployeeWorkingScheduleModel::where('employee_id', $employee_id)->orderBy('id', 'DESC')->first();
    }

    public function getEmployeeAttachments($employee_id){
        $employee_code = EmployeeModel::where('id', $employee_id)->pluck('employee_code');

        return DB::table('employee_attachment')->where('employee_id', $employee_id)->select('employee_id', 'filename', 'file_url')->get();
    }

    public function archiveAttachment($datas = []){
        $attachment_employee_id = $datas['employee_id'];
        $attachment_file_url = $datas['file_url'];

        $attachment = DB::table('employee_attachment')
            ->where('employee_id', $attachment_employee_id)
            ->where('file_url', $attachment_file_url)
            ->first();

        if(isset($attachment->file_dir)){
            unlink($attachment->file_dir);
        }
    }

    public function getEmergencyContacts($id){
        $emergency_contacts = DB::table('employee_emergency_contact')->where('employee_id', $id)->select('person_name', 'relationship', 'contact_number')->get();

        return $emergency_contacts;
    }

    public function getEmployeeCompany($id){
        $employee = DB::table('employee')->where('id', $id)->first();

        $employee_companies = DB::table('employee_company')->where('employee_id', $id)->where('company_level', 'primary')->orderBy('created_at', 'DESC')->select('company_id')->first();

        return (isset($employee_companies->company_id)) ? $employee_companies->company_id : null;
    }

    public function getEmployeeCompanyBranch($id){
        $employee = DB::table('employee')->where('id', $id)->value('employee_code');

        $employee_companies = DB::table('employee_company')->where('employee_id', $id)->select('branch_id')->first();

        // $employee_companies = DB::table('employee_company')->where('employee_id', $id)->select('branch_id')->first();

        return (isset($employee_companies->branch_id)) ? $employee_companies->branch_id : null;
    }

    public function getEmployeeRemarks($id){
        $employee_remarks = DB::table('employee_remarks')->join('company', 'company.id', '=', 'employee_remarks.company_id')->where('employee_remarks.employee_id', $id)->select('employee_remarks.id', 'company.name as company', 'employee_remarks.remarks', 'employee_remarks.created_at', 'employee_remarks.created_by')->get();

        return (isset($employee_remarks)) ? $employee_remarks : null;
    }

    public function updateEmployeeCompany($id, $data = []){
        $time_of_update = date('Y-m-d H:i:s');

        if(!empty($data['data']['company_branch'])){
            $update_data = [
                'branch_id' => $data['data']['company_branch'],
                'company_id' => $data['data']['primary_company'],
                'company_level' => 'primary',
                'updated_by' => 1,
                'updated_at' => $time_of_update,
                'deleted_at' => null,
                'deleted_by' => null
            ];

            // $is_existing = DB::table('employee_company')->where('employee_id', $id)->where('company_id', $data['data']['primary_company'])->count();

            $is_existing = DB::table('employee_company')->where('employee_id', $id)->where('company_id', $data['data']['primary_company'])->count();

            if($is_existing == 0){
                $insert_data = [
                    'employee_id' => $id,
                    'company_id' => $data['data']['primary_company'],
                    'branch_id' => $data['data']['company_branch'],
                    'updated_by' => 1,
                    'company_level' => 'primary',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => 1
                ];

                DB::table('employee')->where('id', $id)->update(['branch' => $data['data']['company_branch']]);

                DB::table('employee_company')->where('employee_id', $id)->delete();

                DB::table('employee_company')->insert($insert_data);
            } else {

                DB::table('employee')->where('id', $id)->update(['branch' => $data['data']['company_branch']]);

                DB::table('employee_company')->where('employee_id', $id)->update(['deleted_at'=>date('Y-m-d H:i:s'), 'deleted_by' => 1]);

                DB::table('employee_company')->where('employee_id', $id)->where('company_id', $data['data']['primary_company'])->update($update_data);


                // DB::table('employee_company')->where('employee_id', $id)->update(['deleted_at'=>date('Y-m-d H:i:s'), 'deleted_by' => 1]);

                // DB::table('employee_company')->where('employee_id', $id)->where('company_id', $data['data']['primary_company'])->update($update_data);
            }
            
        }
    }

    public function selectExportBasicEmployeeListQuery($datas = []){
        $data = DB::table('employee')
            ->join('employee_company', 'employee_company.employee_id', '=', 'employee.id')
            ->join('company', 'company.id', '=', 'employee_company.company_id')
            ->join('company_branch', 'company_branch.id', '=', 'employee_company.branch_id')
            ->join('employee_rate', 'employee_rate.employee_id', '=', 'employee.id')
            ->orderBy('employee.lastname');


        if(!empty($datas['company_id'])){
            $data->where('company.id', $datas['company_id']);
        }

        if(!empty($datas['company_branch'])){
            $data->where('company_branch.id', $datas['company_branch']);
        }

        /*
        employee_code,firstname,middlename,lastname,company,branch,gender,birthdate,nationality,sss,philhealth_no,pagibig_no,tin_no,e_cola,salary_receiving_type,rate,allowance_amount,status,current_address
        */
        $data = $data->select(DB::raw("employee.employee_code, employee.firstname, employee.middlename, employee.lastname,company.name as company, company_branch.branch_name as branch,employee.gender,employee.birthdate,employee.nationality,employee.sss, employee.philhealth_no,employee.pagibig_no,employee.tin_no,employee.e_cola,employee.salary_receiving_type,IF(employee_rate.daily_rate > 0, employee_rate.daily_rate, IF(employee_rate.weekly_rate > 0, employee_rate.weekly_rate, if(employee_rate.monthly_rate > 0, employee_rate.monthly_rate, 0))) as rate,employee_rate.allowance_amount,employee.status,employee.current_address"))->get();

        return $data;
    }

    public function setEmployeeBankAccount($id, $data = []){
        DB::table('employee_bank_account')->insert($data);
    }

    public function updateEmployeeBankAccount($id, $data = []){
        $employee_bank_existing = DB::table('employee_bank_account')->where('employee_id', $id)->count();

        $insert_data = [
                'employee_id' => $id,
                'employee_code' => $data['data']['employee_code'],
                'bank_name' => $data['data']['bank_name'],
                'bank_code' => $data['data']['bank_code'],
                'bank_account_number' => $data['data']['bank_account_number'],
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 1
            ];

        if($employee_bank_existing == 0){
            if(!empty($data['data']['bank_name']) && !empty($data['data']['bank_code']) && !empty($data['data']['bank_account_number'])){
                DB::table('employee_bank_account')->insert($data);
            }
        } else {
            if(!empty($data['data']['bank_name']) && !empty($data['data']['bank_code']) && !empty($data['data']['bank_account_number'])){
                DB::table('employee_bank_account')->where('employee_id', $id)->update(['deleted_at' => date('Y-m-d H:i:s'), 'deleted_by' => 1]);

                DB::table('employee_bank_account')->insert($insert_data);
            }
        }
    }

    public function insertDirect($datas = []){
        ini_set('maximum_execution_time', 180);
        $error_counter = 0;
        $errors = [ 'count' => 0, 'error_msg' => null ];
        $columns = DB::getSchemaBuilder()->getColumnListing('employee');

        if(is_array($datas)){
            $employee_class = get_class($this);
            //$new_employee = new $employee_class();
            $temp = $datas;

            // This part is for fresh Database with no Employee Records.
            $employee_count_checking = DB::table('employee')->count();
            if($employee_count_checking == 0){
                $this->truncateEmployeeRelatedTables();
            } else {
                $employee_count_checking_deleted = DB::table('employee')->whereRaw('deleted_at IS NOT NULL')->count();
                $employee_count_checking_not_deleted = DB::table('employee')->whereRaw('deleted_at IS NULL')->count();

                if($employee_count_checking_not_deleted == 0 && $employee_count_checking_deleted > 0){
                    $this->truncateEmployeeRelatedTables();
                }
            }

            foreach($datas AS $row_index => $row_data)
            {
                // Pre-requsites variable
                $employee_firstname = (isset($row_data['firstname'])) ? trim($row_data['firstname']) : 'N/A';
                $employee_lastname = (isset($row_data['lastname'])) ? trim($row_data['lastname']) : 'N/A';

                $employee_rate = (isset($row_data['rate'])) ? str_replace(',', '', trim($row_data['rate'])) : 99999;
                $employee_sss = (isset($row_data['sss'])) ? $row_data['sss'] : 99999;
                
                $employee_position_type = (isset($row_data['position_type'])) ? $row_data['position_type'] : 99999;
                $employee_skills = (isset($row_data['skills'])) ? $row_data['skills'] : 99999;
                $employee_blood_type = (isset($row_data['blood_type'])) ? $row_data['blood_type'] : 99999;
                $employee_nationality = (isset($row_data['nationality'])) ? $row_data['nationality'] : 99999;
                $employee_citizenship = (isset($row_data['citizenship'])) ? $row_data['citizenship'] : 99999;
                $employee_driving_license = (isset($row_data['driving_license'])) ? $row_data['driving_license'] : 99999;

                $employee_philhealth = (isset($row_data['philhealth_no'])) ? $row_data['philhealth_no'] : 99999;
                $employee_company = (isset($row_data['company'])) ? preg_replace('/\s+/', ' ', $row_data['company']) : 99999;
                $employee_company_b = (isset($row_data['branch'])) ? preg_replace('/\s+/', ' ', $row_data['branch']) : 99999;
                $employee_position = (isset($row_data['position'])) ? preg_replace('/\s+/', ' ', $row_data['position']) : 99999;
                $employee_salary_type = (isset($row_data['salary_receiving_type'])) ? $row_data['salary_receiving_type'] : 99999;

                // Check for if employee existing
                $count_existing_employee = 0;
                $where_raw_existing_information_employee = [];

                if( !empty($row_data['employee_code']) ){
                    $count_existing_employee_query = DB::table('employee')
                        ->join('employee_company', 'employee.id', '=', 'employee_company.employee_id')
                        ->join('company', 'company.id', '=', 'employee_company.company_id');

                    $count_existing_employee = 0;

                    // VALIDATION by Employee Code
                    if(!empty($row_data['employee_code'])){
                        $where_raw_existing_information_employee = [];

                        $count_existing_employee = DB::table('employee')
                            ->join('employee_company', 'employee.id', '=', 'employee_company.employee_id')
                            ->join('company', 'company.id', '=', 'employee_company.company_id');

                        $where_raw_existing_information_employee[0] = " employee.employee_code = '" . $row_data['employee_code'] . "' ";

                        if(count($where_raw_existing_information_employee)){
                            $count_existing_employee = $count_existing_employee_query
                            ->whereRaw(implode(" OR ", $where_raw_existing_information_employee))
                            ->count();
                        } else {
                            $count_existing_employee = 0;
                        }
                    }
                }

                // IF EMPLOYEE IS EXISTING WILL UPDATE IN MASS UPLOAD
                if($count_existing_employee > 0) {
                    /*
                    UPDATE EMPLOYEE
                    Update Employee if record is already existing.
                    */
                    $employee_searched = DB::table('employee')
                        ->join('employee_company', 'employee.id', '=', 'employee_company.employee_id')
                        ->join('company', 'company.id', '=', 'employee_company.company_id')
                        ->whereRaw(implode(" OR ", $where_raw_existing_information_employee))
                        ->select('employee.id')
                        ->first();

                    if(!empty($employee_searched->id)){
                        $update_searched_employee = [];
                        $employee_searched_obj_id = null;

                        // Employee Company Before Update
                        $employee_company_update = [];
                        $employee_company_update['company_id'] = null;
                        $employee_company_update['branch_id'] = null;
                        $employee_company_update['updated_by'] = 1;
                        $employee_company_update['updated_at'] = date('Y-m-d H:i:s');
                        $employee_company_update['created_by'] = 1;
                        $employee_company_update['created_at'] = date('Y-m-d H:i:s');
                        
                        // Employee Rate Before Update
                        $employee_rate_new = null;
                        $employee_salary_update = [];
                        $employee_salary_update['daily_rate'] = 0;
                        $employee_salary_update['hourly_rate'] = 0;
                        $employee_salary_update['weekly_rate'] = 0;
                        $employee_salary_update['monthly_rate'] = 0;
                        $employee_salary_update['allowance_amount'] = 0;
                        $employee_salary_update['method'] = null;
                        $employee_salary_update['created_by'] = 1;
                        $employee_salary_update['updated_by'] = 1;
                        $employee_salary_update['created_at'] = date('Y-m-d H:i:s');
                        $employee_salary_update['updated_at'] = date('Y-m-d H:i:s');

                        $employee_searched_obj_id = $employee_searched->id;
                        // DB::table('employee')->where('id', $employee_searched_obj_id)->update(['deleted_at' => null, 'deleted_by' => null]);

                        // $update_searched_employee = EmployeeModel::where('id', $employee_searched_obj_id)->first()->toArray();
                        $update_searched_employee = DB::table('employee')->where('id', $employee_searched_obj_id)->first();

                        $employee_rate_current = [];
                        if(!empty($employee_searched->id)){
                            $employee_rate_current = EmployeeRateModel::where('employee_id', $employee_searched_obj_id)->orderBy('id', 'DESC')->first()->toArray();
                        }

                        foreach($row_data AS $row_data_index => $row_data_value){
                            if(count($update_searched_employee)){
                                foreach($update_searched_employee AS $update_searched_employee_index => $update_searched_employee_value){
                                    if($row_data_index == 'supervisor_id' || $row_data_index == 'position' || $row_data_index == 'department' ) continue;

                                    if(strpos($row_data_index, 'date') !== false){
                                        continue;
                                        // $update_datas[$row_data_index] = (DateTime::createFromFormat('Y-m-d G:i:s', $myString) !== FALSE) ? date_format(date_create($row_data_value), 'Y-m-d') : date('Y-m-d');
                                    } else if(strpos($row_data_index, 'marital_status') !== false){
                                        $update_datas[$row_data_index] = (!empty(trim($row_data_value))) ? utf8_decode(trim($row_data_value)) : 'single';
                                    }else if(strtolower($row_data_index) == 'company' && !empty(trim($row_data_value))){
                                        // For Update Company
                                        $employee_company_update['company_id'] = null ;
                                        $count_existing_company = DB::table('company')->whereRaw('LCASE(`name`) LIKE "%' . trim(strtolower($row_data_value)) . '%"')->count();

                                        if($count_existing_company > 0){
                                            $employee_company_update['company_id'] = DB::table('company')->whereRaw('LCASE(`name`) LIKE "%' . strtolower(trim($row_data_value)) . '%"')->first()->id;
                                            $employee_salary_update['company_id'] = $employee_company_update['company_id'];
                                        }
                                    } else if(strtolower($row_data_index) == 'branch' && !empty(trim($row_data_value))){
                                        // For Update Company Branch
                                        $company_branch_existing = 0;
                                        if(!empty($employee_company_update['company_id'])){
                                            $company_branch_existing = DB::table('company_branch')->where('company_id', $employee_company_update['company_id'])->whereRaw('LCASE(`branch_name`) LIKE "%' . strtolower(trim($row_data_value)) . '%" ')->count();

                                            if($company_branch_existing > 0){
                                                $employee_company_update['branch_id'] = DB::table('company_branch')->where('company_id', $employee_company_update['company_id'])->whereRaw('LCASE(`branch_name`) LIKE "%' . strtolower(trim($row_data_value)) . '%" ')->first()->id;
                                            } else {
                                                $employee_company_update['branch_id'] = DB::table('company_branch')->where('company_id', $employee_company_update['company_id'])->whereRaw("deleted_at IS NOT NULL AND deleted_by IS NOT NULL")->first()->id;
                                            }
                                        }
                                    } else if(strtolower($row_data_index) == 'rate' && !empty(trim($row_data_value))){
                                        $employee_rate = (float)$row_data_value;
                                        $employee_salary_update['daily_rate'] = $employee_rate;
                                        $employee_salary_update['hourly_rate'] = $employee_rate;
                                        $employee_salary_update['monthly_rate'] = $employee_rate;
                                        $employee_salary_update['weekly_rate'] = $employee_rate;
                                    } else if(strtolower($row_data_index) == 'allowance_amount' && !empty((double)$row_data_value)){
                                        $employee_salary_update['allowance_amount'] = (double)$row_data_value;
                                        // $update_datas[$row_data_index] = (double)$row_data_value;
                                    } else if(strtolower($row_data_index) == 'salary_receiving_type' && !empty(trim($row_data_value))){
                                        $employee_salary_update['method'] = trim(strtolower($row_data_value));
                                        $update_datas[$row_data_index] = trim(utf8_decode(strtolower($row_data_value)));
                                    }

                                    // For updating Employee_rate as the employee record is being modified
                                    if( $row_data_index == $update_searched_employee_index && $update_searched_employee_value != $row_data_value && !empty(trim($row_data_value))){
                                        $update_datas[$row_data_index] = trim(utf8_decode($row_data_value));
                                    }
                                }
                            }
                        }

                        $update_datas['deleted_by'] = null;
                        $update_datas['deleted_at'] = null;
                        $update_datas['status'] = 1;

                        $update_datas['updated_at'] = date('Y-m-d H:i:s');
                        $update_datas['updated_by'] = 1;

                        if(count($update_datas)){
                            DB::table('employee')->where('id', $employee_searched->id)->update($update_datas);
                        }

                        // Employee Update Company
                        if(count($employee_company_update) > 0){
                            $check_employee_company_existing = DB::table('employee_company')->where('employee_id', $employee_searched->id)->count();
                            if($check_employee_company_existing > 0){
                                DB::table('employee_company')->where('employee_id', $employee_searched->id)->update($employee_company_update);
                            } else {
                                DB::table('employee_company')->where('employee_id', $employee_searched->id)->insert($employee_company_update);
                            }
                        }

                        // Employee Update Rate
                        if(count($employee_salary_update) > 0){
                            if($employee_salary_update['method'] == 'daily'){
                                $employee_salary_update['hourly_rate'] =0;
                                $employee_salary_update['monthly_rate'] =0;
                                $employee_salary_update['weekly_rate'] =0;
                            } else if($employee_salary_update['method'] == 'weekly'){
                                $employee_salary_update['daily_rate'] = 0;
                                $employee_salary_update['hourly_rate'] =0;
                                $employee_salary_update['monthly_rate'] =0;
                            } else if($employee_salary_update['method'] == 'monthly'){
                                $employee_salary_update['daily_rate'] = 0;
                                $employee_salary_update['hourly_rate'] =0;
                                $employee_salary_update['weekly_rate'] =0;
                            } else if($employee_salary_update['method'] == 'hourly'){
                                $employee_salary_update['daily_rate'] = 0;
                                $employee_salary_update['monthly_rate'] = 0;
                                $employee_salary_update['weekly_rate'] = 0;
                            } else {
                                $employee_salary_update['hourly_rate'] = 0;
                                $employee_salary_update['monthly_rate'] = 0;
                                $employee_salary_update['weekly_rate'] = 0;
                            }
                            unset($employee_salary_update['method']);

                            $check_employee_rate_existing_to_update = DB::table('employee_rate')->where('employee_id', $employee_searched->id)->count();

                            if($check_employee_rate_existing_to_update > 0){
                                DB::table('employee_rate')->where('employee_id', $employee_searched->id)->update($employee_salary_update);
                            } else {
                                $employee_salary_update['employee_id'] = $employee_searched->id;
                                DB::table('employee_rate')->where('employee_id', $employee_searched->id)->update($employee_salary_update);
                            }
                        }
                    }
                } else if($count_existing_employee == 0){ /*// IF EMPLOYEE IS NOT YET EXISTING WILL INSERT IN MASS UPLOAD */
                    /*
                    ADD EMPLOYEE
                    Store Employee if not existing
                    */
                    $employee_new_allowance_amount = 0;

                    if(empty($row_data['employee_code'])){
                        continue;
                    } else {
                        // will insert a employee if employee has correct company and company's branch
                        if(!empty(trim($employee_company)) && !empty(trim($employee_company_b))){
                            // START - SAVING EMPLOYEE IN EMPLOYEE TABLE
                            $new_employee = new $employee_class();

                            foreach($row_data AS $col_index => $col_value){
                                $col_value = trim($col_value);

                                if(in_array($col_index, $columns) ){
                                    if(strpos($col_index, 'suffix') !== false){
                                        $new_employee->{$col_index} = (!empty($col_value)) ? substr(trim(utf8_decode($col_value)), 0, 3) : 'none';
                                        $temp[$row_index][$col_index] = (!empty($col_value)) ? substr(trim(utf8_decode($col_value)), 0, 3) : 'none';
                                    } else if(strpos($col_index, 'date') !== false){
                                        $new_employee->{$col_index} = date('Y-m-d', strtotime($col_value));
                                    } else if(strpos($col_index, 'department') !== false){
                                        $new_employee->department = 1;
                                    } else if(strpos($col_index, 'supervisor_id') !== false){
                                        $new_employee->supervisor_id = (!empty($col_value)) ? $col_value : 1;
                                    } else if(strpos($col_index, 'marital_status') !== false){
                                        $new_employee->marital_status = (!empty($col_value)) ? $col_value : 'single';
                                    } else if(strpos($col_index, 'citizenship') !== false || strpos($col_index, 'nationality') !== false){
                                        $new_employee->marital_status = (!empty($col_value)) ? $col_value : 'Filipino';
                                    } else if(strpos($col_index, 'blood_type') !== false){
                                        $new_employee->marital_status = (!empty($col_value)) ? $col_value : 'O';
                                    } else if(strpos($col_index, 'allowance_amount') !== false){
                                        $employee_new_allowance_amount = (double)$col_value;
                                    } else if(strpos($col_index, 'employment_status') !== false){
                                        $new_employee->employment_status = (!empty($col_value)) ? trim(utf8_decode($col_value)) : 'trainee';

                                    } else if(strpos($col_index, 'position') !== false && $col_index != 'position_type'){
                                        $position = DB::table('position')->whereRaw('lcase(`name`) LIKE "%' . strtolower($col_value) . '%"')->first();

                                        $new_employee->position = (!empty($position->id)) ? $position->id : 1;
                                    } else if(strpos($col_index, 'cel_no') !== false){
                                        $new_employee->{$col_index} = str_replace('-', '', trim(utf8_decode($col_value)));
                                        $temp[$row_index][$col_index] = str_replace('-', '', trim(utf8_decode($col_value)));
                                    } else if(strpos($col_index, 'status') !== false){
                                        $new_employee->{$col_index} = (int)$col_value;
                                        $temp[$row_index][$col_index] = (int)$col_value;
                                    } else if(strpos($col_index, '_credit') !== false || strpos($col_index, 'e_cola') !== false){
                                        $new_employee->{$col_index} = (!empty($col_value)) ? (float)$col_value : 0;
                                        $temp[$row_index][$col_index] = (!empty($col_value)) ? (float)$col_value : 0;
                                    } else if(strpos($col_index, 'employee_code') !== false){
                                        if(!empty($col_value)){
                                            $new_employee->{$col_index} = $col_value;
                                            $temp[$row_index][$col_index] = $col_value;
                                        } else {
                                            if(!empty($employee_sss)){
                                                $new_employee->{$col_index} = trim(utf8_decode($employee_sss));
                                                $temp[$row_index][$col_index] = trim(utf8_decode($employee_sss));
                                            } else if(!empty($employee_philhealth)){
                                                $new_employee->{$col_index} = trim(utf8_decode($employee_philhealth));
                                                $temp[$row_index][$col_index] = trim(utf8_decode($employee_philhealth));
                                            }
                                        }

                                    } else {
                                        if(strpos($col_index, 'sss') !== false){
                                            $employee_sss = (!in_array(trim($col_value), ['N/A', 'n/a'])) ? $col_value : '';
                                        } else if(strpos($col_index, 'position_type') !== false){
                                            $employee_position_type = (!empty(trim($col_value))) ? $col_value : 'subordinate';
                                        } else if(strpos($col_index, 'skills') !== false){
                                            $employee_position_type = (!empty(trim($col_value))) ? $col_value : 'customer caring, ms office';
                                        } else if(strpos($col_index, 'blood_type') !== false){
                                            $employee_blood_type = (!empty(trim($col_value))) ? $col_value : 'O';
                                        } else if(strpos($col_index, 'nationality') !== false){
                                            $employee_nationality = (!empty(trim($col_value))) ? $col_value : 'Filipino';
                                        } else if(strpos($col_index, 'driving_license') !== false){
                                            $employee_driving_license = (!empty(trim($col_value))) ? $col_value : 'N/A';
                                        }

                                        $new_employee->{$col_index} = utf8_decode(str_replace([" ", ","], "", (trim($col_value))));
                                        $temp[$row_index][$col_index] = utf8_decode(str_replace([" ", ","], "", (trim($col_value))));
                                    }
                                }                                
                                $new_employee->created_at = date('Y-m-d H:i:s');
                                $new_employee->created_by = 1;//date('Y-m-d H:i:s');
                                $new_employee->updated_at = date('Y-m-d H:i:s');
                                $new_employee->updated_by = 1;//date('Y-m-d H:i:s');
                            }
                            $new_employee->save();
                            // END - SAVING EMPLOYEE IN EMPLOYEE TABLE

                            // Employee Repolish Employee Code Value, if no employee_code use sss number
                            if(in_array($new_employee->employee_code, ['N/A', 'n/a', '-N/A-', '-n/a-'] ) ){
                                $ramdom_emp_code = substr(md5($new_employee->firstname . $new_employee->lastname . $new_employee->sss . $new_employee->philhealth_no) , 0, 14);
                                DB::table('employee')->where('id', $new_employee->id)->update(['employee_code' => $ramdom_emp_code]);

                                if( !empty($new_employee->sss) ){
                                    DB::table('employee')->where('id', $new_employee->id)->update([ 'employee_code' => $new_employee->sss ]);
                                }
                            }

                            // If employee code is empty will generate a new employee_code of if has sss but not employee code, uses sss as employee code
                            if(empty($new_employee->employee_code) ){
                                $ramdom_emp_code = substr(md5($new_employee->firstname . $new_employee->lastname . date('YmdHis')) , 0, 14);
                                DB::table('employee')->where('id', $new_employee->id)->update(['employee_code' => $ramdom_emp_code]);

                                if( !empty($new_employee->sss) ){
                                    DB::table('employee')->where('id', $new_employee->id)->update([ 'employee_code' => $new_employee->sss ]);
                                }
                            }

                            // New Employee
                            $new_employee_instance = DB::table('employee')->where('id', $new_employee->id)->first();
                            
                            // Employee Company
                            $fetch_company_id = DB::table('company')->whereRaw('LCASE(name) LIKE "%' . trim(strtolower($employee_company)) . '%"')->value('id');
                            $fetch_company_branch_id = DB::table('company_branch')->whereRaw('LCASE(TRIM(branch_name)) LIKE "%'.trim(strtolower($employee_company_b)).'%" AND company_id = ' . $fetch_company_id . ' AND deleted_at IS NULL')->value('id');

                            if(empty($fetch_company_branch_id)){
                                $fetch_company_branch_id = DB::table('company_branch')->whereRaw('company_id = ' . $fetch_company_id . ' AND deleted_at IS NULL')->orderBy('created_at', 'DESC')->value('id');
                            }

                            // Workaround for Company: Pares due to wrong inputs in Employee 201 files of Pares
                            // if($fetch_company_id == 9){
                            //     $fetch_company_branch_id = DB::table('company_branch')->whereRaw('LCASE(REPLACE(branch_name, " ", "")) LIKE "%'.str_replace(' ', '', trim(strtolower($employee_company_b))).'%" AND company_id = ' . $fetch_company_id . ' AND deleted_at IS NULL')->value('id');
                            // }

                            // IF Fetched Company ID & Fetched Company Branch ID is not empty, will add Employee Company record to table employee_company
                            if(!empty($fetch_company_id) && !empty($fetch_company_branch_id) && !empty($new_employee_instance->id)){
                                $count_emp_comp = EmployeeCompanyModel::where('employee_id', $new_employee_instance->id)
                                    ->where('company_id', $fetch_company_id)
                                    ->where('branch_id', $fetch_company_branch_id)->count();

                                if($count_emp_comp == 0){
                                    // Insert Employee Company
                                    $new_employee_company = new EmployeeCompanyModel();
                                    $new_employee_company->employee_id = $new_employee_instance->id;

                                    $new_employee_company->company_id = $fetch_company_id;
                                    $new_employee_company->branch_id = $fetch_company_branch_id;

                                    $new_employee_company->company_level  = 'primary';

                                    $new_employee_company->created_at = date('Y-m-d H:i:s');
                                    $new_employee_company->created_by = 1;
                                    $new_employee_company->updated_by = 1;
                                    $new_employee_company->updated_at = date('Y-m-d H:i:s');
                                    $new_employee_company->save();

                                    DB::table('employee')->where('id', $new_employee_instance->id)->update(['branch' => $fetch_company_branch_id]);
                                }
                            }

                            // Employee Position
                            $fetch_position = (array)DB::table('position')->whereRaw('LCASE(`name`) LIKE "%' . strtolower(trim($employee_position) ). '%"')->orderBy('id', 'DESC')->first();
                            // IF Fetched Position is existing will insert EmployeePosition table
                            if(count($fetch_position) > 0){                                
                                $new_employee_position = new EmployeePositionModel();
                                $new_employee_position->department = 1;
                                $new_employee_position->employee_id = $new_employee_instance->id;
                                $new_employee_position->position = $fetch_position['id'];
                                $new_employee_position->created_by = 1;
                                $new_employee_position->created_at = date('Y-m-d H:i:s');
                                $new_employee_position->save();
                            } elseif(count($fetch_position) == 0) {
                                // IF Fetched Position is NOT existing will insert EmployeePosition table & Position table
                                DB::table('position')->insert([
                                    'name' => trim(ucfirst($employee_position)),
                                    'description' => trim(ucfirst($employee_position)),
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'created_by' => 1
                                ]);

                                $fetch_position = DB::table('position')->whereRaw('LCASE(name) LIKE "%' . trim(strtolower($employee_position)) . '%"')->first();

                                $new_employee_position = new EmployeePositionModel();
                                $new_employee_position->department = 1;
                                $new_employee_position->employee_id = $new_employee_instance->id;
                                $new_employee_position->position = $fetch_position->id;
                                $new_employee_position->created_by = 1;
                                $new_employee_position->created_at = date('Y-m-d H:i:s');
                                $new_employee_position->save();
                            }

                            // Employee Rate
                            if(!empty($employee_rate) && !empty($employee_salary_type) && !empty($fetch_company_id)){
                                DB::table('employee_rate')->where('employee_id', $new_employee_instance->id)->delete();

                                $new_employee_rate = new EmployeeRateModel();
                                $new_employee_rate->employee_id = $new_employee_instance->id;
                                $new_employee_rate->company_id = $fetch_company_id;
                                
                                if(strtolower(trim($employee_salary_type)) == 'daily'){
                                    $new_employee_rate->daily_rate = $employee_rate;
                                } else if(strtolower(trim($employee_salary_type)) == 'month'){
                                    $new_employee_rate->monthly = $employee_rate;
                                } else if(strtolower(trim($employee_salary_type)) == 'week'){
                                    $new_employee_rate->weekly = $employee_rate;
                                } else {
                                    $new_employee_rate->weekly = 0;
                                    $new_employee_rate->monthly = 0;
                                    $new_employee_rate->daily_rate = $employee_rate;
                                }

                                $new_employee_rate->allowance_amount = (double)$employee_new_allowance_amount;

                                $new_employee_rate->created_at = date('Y-m-d H:i:s');
                                $new_employee_rate->created_by = 1;
                                $new_employee_rate->save();
                            }

                            // Employee Working Schedule
                            $count_available_schedules = DB::table('working_schedule')->where('company_id', $fetch_company_id)->count();

                            if($count_available_schedules == 0){
                                if(!empty($fetch_company_id)){
                                    DB::table('working_schedule')->insert([
                                        'company_id' => $fetch_company_id,
                                        'flexible_time' => 0,
                                        'grace_period_mins' => 0,
                                        'name' => 'main working schedule',
                                        'time_in' => '08:00:00',
                                        'time_out' => '18:00:00',
                                        'days' => json_encode([ 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ]),
                                        'created_by' => 1,
                                        'created_at' => date('Y-m-d H:i:s')
                                    ]);
                                }
                            }

                            // Employee Working Schedule
                            $available_schedules = DB::table('working_schedule')->where('company_id', $fetch_company_id)->whereRaw('deleted_at IS NOT NULL')->oldest()->limit(1)->get();
                            if(!empty($available_schedules)){
                                foreach($available_schedules AS $schedule){
                                    $employee_working_schedule =  new EmployeeWorkingScheduleModel();
                                    $employee_working_schedule->employee_id = $new_employee_instance->id;
                                    $employee_working_schedule->working_schedule_id = $schedule->id;
                                    $employee_working_schedule->approved = 1;
                                    $employee_working_schedule->approved_by = 1;
                                    $employee_working_schedule->created_by = 1;
                                    $employee_working_schedule->created_at = date('Y-m-d H:i:s');
                                    $employee_working_schedule->save();
                                }
                            }

                            // Employee USER Insert & adding new User
                            if(!empty($new_employee_instance->firstname) && !empty($new_employee_instance->lastname) && !empty($fetch_company_id) && !empty($new_employee_instance->email)){
                                $password = strtolower($new_employee_instance->firstname . $new_employee_instance->lastname);

                                $count_existing_user = UserModel::whereRaw('LCASE(`email`) = "' .strtolower($new_employee_instance->email) . '"')->count();

                                if($count_existing_user == 0){
                                    $new_user = new UserModel();
                                    $new_user->name = $new_employee_instance->firstname . ' ' . $new_employee_instance->lastname;
                                    
                                    $new_user->email = str_replace( [ ' ', ',' ], "", strtolower($new_employee_instance->email));

                                    $new_user->password = Hash::make($password);

                                    $new_user->created_by = 1;
                                    $new_user->created_at = date('Y-m-d H:i:s');
                                    $new_user->save();

                                    DB::table('employee_user')->insert([
                                        'employee_id' => $new_employee_instance->id,
                                        'user_id' => $new_user->id
                                    ]);

                                    DB::table('role_user')->insert([
                                        'user_id' => $new_user->id,
                                        'role_id' => 3
                                    ]);
                                }
                            }

                            // Insert of Employee Working Schedule
                            if(!empty($new_employee_instance->id)){

                            }
                        } else {
                            $index_row_human = $row_index + 1;
                            $error_counter++;
                            $errors['errors'][$error_counter] = $error_counter;
                            $errors['error_msg'][$error_counter] = "Row No. " . ($index_row_human) . "";
                        }
                    }
                }
            }

            $users = [];
            $user_ids = DB::table('users')->pluck('id');
            foreach($user_ids AS $counter_user_id => $user_id){
                $users[$counter_user_id] = $user_id;
            }
            
            DB::table('employee')->update(['supervisor_id' => json_encode($users)]);

            if(count($errors['error_msg']) > 0){
                $errors['error_msg'] = "Some of the records were added or updated while some of the records were not. Please check these following rows in your CSV file if it has accurate datas. (Refer to " . implode(', ', $errors['error_msg']) . ")";
            }

            return [ 'data' => ['data' => $temp, 'error' => $errors ]];
            // return [ 'data' => $temp, 'error' => $errors ]; // important
        }
    }

    public function createUserTimeSetting($id,$data){
        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        DB::table('user_time_setting')->insert(
            array(
                'employee_id'       => $id,
                'ip_address'        => $data['data']['ip_address'],
                'mac_address'       => $data['data']['mac_address'],
                'ddns'              => $data['data']['ddns'],
                'created_by'        => $user->user_id,
                'created_at'        => $date
                )
            );
    }

    public function createOnboarding($id,$data){
        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        if(!empty($user) && !empty($user->supervisor_id) && !empty($user->user_id)){
            $ids = DB::table('employee_onboarding')->insertGetId(
                array(
                    'employee_id'              => $id,
                    'checker_id'               => $user->supervisor_id,
                    'created_by'               => $user->user_id,
                    'created_at'              => $date
                    )
                );
            
            foreach ($data['data']['onboarding'] as $d) {
                DB::table('onboarding_list')->insert(
                array(
                    'employee_onboarding_id'    => $ids,
                    'onboarding_id'             => $d,
                    'status_id'                 => 1,
                    'created_by'                => $user->user_id,
                    'created_at'                => $date
                    )
                );
            }
        }

        return $data;
    }

    public function getOnboarding($id){

        $data = DB::table('employee_onboarding')
                ->join('onboarding_list', 'employee_onboarding.id', '=', 'onboarding_list.employee_onboarding_id')
                ->join('onboarding', 'onboarding_list.onboarding_id', '=', 'onboarding.id')
                ->select(
                    'onboarding_list.id'
                )
                ->where('employee_onboarding.employee_id','=' ,$id)
                ->get();

        for ($i=0; $i < count($data); $i++) { 
            $data[$i]->val =  false;
        }

        return $data;
    }

    public function getUserTimeSetting($id){
        $data = [
            'ip_address' => null,
            'ddns' => null,
            'mac_address' => null
        ];

        $user_time_setting_count = DB::table('user_time_setting')->where('employee_id','=' ,$id)->count();

        if($user_time_setting_count > 0){
            $data = DB::table('user_time_setting')->select(
                    'ip_address',
                    'ddns',
                    'mac_address'
                )
                ->where('employee_id','=' ,$id)
                ->first();
        }

        return $data;
    }

    public function updateUserTimeSetting($id,$data){

        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        $ip_address = $data['data']['ip_address'];
        $ddns = $data['data']['ddns'];
        $mac_address = $data['data']['mac_address'];

        DB::table('user_time_setting')->where('employee_id',$id)->update([
            'ip_address'    => $ip_address,
            'ddns'          => $ddns,
            'mac_address'   => $mac_address,
            'updated_by'    => $user->user_id,
            'updated_at'    => $date
        ]);


        return $data;
    }

    public function updateOnboarding($emp_id,$data){
        $date = date('Y-m-d H:i:s');
        $onboard = $data['data']['onboarding'];
        $id = null;

        $id = DB::table('employee_onboarding')->where('employee_id', $emp_id)->select('id')->first();

        if ($id != null) {
            $existval = [];
            $notexistval = [];
            $exist =[];

            foreach ($onboard as $key => $d) {

                $exist = DB::table('onboarding_list')
                ->where('employee_onboarding_id', $id->id)
                ->where('onboarding_id', $d)
                ->select('id')->first();

                if ($exist == []) {
                    array_push($notexistval, $d);
                }else{
                    array_push($existval, $exist->id);
                }
            }

            DB::table('onboarding_list')
            ->where('employee_onboarding_id', $id->id)
            ->whereNotIn('id', $existval)
            ->delete();

            if ($notexistval != []) {
                foreach ($notexistval as $d) {
                    DB::table('onboarding_list')->insert(
                    array(
                        'employee_onboarding_id'    => $id->id,
                        'onboarding_id'             => $d,
                        'status_id'                 => 1,
                        'created_by'                => 1,
                        'created_at'                => $date
                        )
                    );
                }
            }
        }else{
            $this->createOnboarding($emp_id,$data);
        }
        return $data;
    }

    public function updateEmployeeStatus($data){

        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        $created_by= $user->user_id;
        $employee_id = $data['employee_id'];
        $status = $data['status'];

        $dat = $this->getUserId($employee_id);

        DB::table('employee')->where('id',$employee_id)->update([
            'employment_status'    => $status,
            'updated_by'           => $created_by,
            'updated_at'           => $date
        ]);

        $prompt = strtoupper($data['status']);

        $notif_id=DB::table('notification')->insertGetId(
                array(
                    'title'          => 'Employee Status',
                    'description'    => 'Your status are been updated to ' . $prompt,
                    'messenger_id'   => $created_by,
                    'table_from'     => 'employee',
                    'table_from_id'  => $employee_id,
                    'seen'           => 0,
                    'route'          => 'employee-dashboard',
                    'receiver_id'    => $dat->user_id,
                    'icon'           => 'fa fa-heart text-aqua',
                    'created_by'     => $created_by,
                    'created_at'     => $date
                    )
            );
            
        $notif[]= $notif_id;

        return $notif;
    }

    public function getUser(){
        $contr = new UserController;
        $data = $contr->getListByIdLimited();

        return $data;
    }

    public function getUserId($id){
        $contr = new UserController;
        $data = $contr->getUserId($id);

        return $data;
    }

    public function truncateEmployeeRelatedTables(){
                DB::table('employee')->truncate();
                DB::table('employee_attachment')->truncate();
                DB::table('employee_audit')->truncate();
                DB::table('employee_bank')->truncate();
                DB::table( 'employee_bank_account')->truncate();
                DB::table('employee_basic_pay')->truncate();
                DB::table('employee_biometrics_id')->truncate();
                DB::table('employee_clearance')->truncate();
                DB::table( 'employee_company')->truncate();
                DB::table( 'employee_dependent')->truncate();
                DB::table( 'employee_dynasty_loan')->truncate();
                DB::table( 'employee_emergency_contact')->truncate();
                DB::table( 'employee_evaluation_rating')->truncate();
                DB::table( 'employee_evaluation_rating_details')->truncate();
                DB::table( 'employee_logs2')->truncate();
                DB::table( 'employee_onboarding')->truncate();
                DB::table( 'employee_position')->truncate();DB::table('employee_rate')->truncate();
                DB::table( 'employee_rate_log')->truncate();
                DB::table( 'employee_remarks')->truncate();
                DB::table( 'employee_schedule_master')->truncate();
                DB::table( 'employee_user')->truncate();
                DB::table( 'employee_working_schedule')->truncate();
                DB::table('employees_logs')->truncate();
    }

}
