<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class OfficialBusinessTypeModel extends Model
{
    protected $table = "official_business_type"; use SoftDeletes;
}
