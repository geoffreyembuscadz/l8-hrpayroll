<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeWorkingScheduleModel extends Model
{
    protected $table = "employee_working_schedule"; use SoftDeletes;
}
