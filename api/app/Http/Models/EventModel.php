<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;

class EventModel extends Model
{
    protected $table = "event"; use SoftDeletes;

    public static function eventLists($start_date, $end_date){
        return DB::table('event_type')
                ->join('event', 'event_type.id', '=', 'event.event_type_id')
                ->select(DB::raw('event.id As id'),
                         DB::raw('event.name as title'), 
                         DB::raw('DATE(event.start_time) as start'), 
                         DB::raw('DATE(event.end_time) as end'),
                         DB::raw('event_type.color'))
                ->where('event.deleted_by', '=', NULL)
                ->whereBetween('event.start_time', [$start_date, $end_date])
                ->get();
    }

    public function postEventDetails($data){
        dd($data);exit;
        $event_ids=[];
        $created_by = 1;
        $date = date('Y-m-d H:i:s');
        $event_id = 
        DB::table('event')->insertGetId
        ( array
            (   
                'name' 	          => $data['name'],
                'status_id'       => 1,
                'event_type_id'   => $data['type'],
                'start_time'      => $data['start_time'],
                'end_time'        => $data['end_time'],
                'employee_id'     => $data['employee_id'],
                'created_by'      => $created_by,
                'created_at'      => $date
            )
        );

        DB::table('event_details')->insert
        ( array
            (   
                'event_id'  	=> $event_id,
                'location'      => $data['location'],
                'created_by'    => $created_by,
                'created_at'    => $date
            )
        );

        $table=DB::table('event')->whereIn('event_id',$event_ids)->select()->get();
        return $table;
    }
    public static function getRecords(){

        return self::join('status_type', 'event.status_id', '=', 'status_type.id')
                    ->join('employee', 'event.created_by', '=', 'employee.id')
                    ->join('event_details', 'event.id', '=', 'event_details.event_id')
                    ->join('event_type', 'event.event_type_id', '=', 'event_type.id')
                    ->select(DB::raw('status_type.name AS status'), 
                        DB::raw('event_details.location As location'),
                        DB::raw('event.name As name'), 
                        DB::raw('DATE(event.start_time) as start'), 
                        DB::raw('DATE(event.end_time) as end'), 
                        DB::raw('event_type.type as type'),
                        DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `created_by`')
                        ,'event.id')
                    ->where('deleted_at', '!=', Null)


                    ->get(); 

    }

    public static function getEventName($data){
        $event_id = $data['event_id'];

        $query = DB::table('event As e')
                  ->join('event_type As e_t', 'e.event_type_id', 'e_t.id')
                  ->select( 'e_t.id', 'e.name', 'e_t.type As type', 
                            DB::raw('DATE(e.start_time) As start'),
                            DB::raw('DATE(e.end_time) AS end')
                  );
        if ( isset($event_id) || !empty($event_id) ) {
            $query->where('e_t.id', '=', $event_id);
        }
           
        $data = $query->get();    
        return $data;

    } 

    public static function getET(){
        $event = DB::table('event_type')
                    ->select('event_type.id As id',
                             'event_type.color As color',
                        DB::raw('event_type.type As text'))
                    ->get();
        return $event;

    }

    public static function getEventStat(){
        return self::join('status_type', 'event.status_id', '=', 'status_type.id')
                    ->select(DB::raw('status_type.name AS status'))->get();

    }

    public static function show($id){


        return self::join('status_type', 'event.status_id', '=', 'status_type.id')
                    ->join('event_details', 'event.id', '=', 'event_details.event_id')
                    ->join('event_type', 'event.event_type_id', '=', 'event_type.id')
                    ->select(
                            'event.id',
                            'event.event_type_id',
                            'event.event_type_id',
                            DB::raw('status_type.name AS status'), 
                            DB::raw('event_details.location As location'),
                            DB::raw('event.name As name'), 
                            DB::raw('DATE(event.start_time) as start'), 
                            DB::raw('DATE(event.end_time) as end'), 
                            DB::raw('event_type.type as type'),
                            DB::raw('event_type.color as color'))
                    ->where('event.id','=' ,$id)
                    ->first();
     
    }
    public static function eventList(){
        return self::join('event_details', 'event.id', '=', 'event_details.event_id')
                   ->join('event_type', 'event.event_type_id', '=', 'event_type.id')
                   ->join('status_type', 'event.status_id', '=', 'status_type.id')
                   ->select(
                        DB::raw('event.name As event'),
                        DB::raw('event_type.type As type'),
                        DB::raw('Cast(event.start_time as date) As start'),
                        DB::raw('Cast(event.end_time as date) As end'),
                        DB::raw('event_details.location As location'),
                        DB::raw('status_type.name As status'))
                   ->get();
    }

}



