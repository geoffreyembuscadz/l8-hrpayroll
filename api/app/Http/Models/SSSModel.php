<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class SSSModel extends Model
{
    protected $table = "sss"; use SoftDeletes;
}
