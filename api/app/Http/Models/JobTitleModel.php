<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class JobTitleModel extends Model
{
    protected $table = "jobtitle"; use SoftDeletes;

    public function joins($model){
    	$model = $model->join('department', 'department.id', '=', 'jobtitle.department_id');

    	return $model;
    }

    public function moderateSelection($model){
    	$model->select('jobtitle.id', 'jobtitle.name', 'jobtitle.description', DB::raw('department.name AS department_name'));
    	return $model;
    }
}
