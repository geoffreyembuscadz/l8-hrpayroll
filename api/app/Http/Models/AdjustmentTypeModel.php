<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\AdjustmentTypeModel as AdjustmentType;
use Carbon\Carbon;

use \DB AS DB;

class AdjustmentTypeModel extends Model
{
    protected $table = "adjustment_type"; use SoftDeletes;

    public function postListJoin($model) {
    	return $model->select('adjustment_type.id', 'adjustment_type.name', 'adjustment_type.description', 'adjustment_type.display_name', 'adjustment_type.type',    DB::raw("if(adjustment_type.include_payslip = 1, 'Yes', 'No' ) as include_payslip"),     
 				DB::raw("(select if(adjustment_type.taxable = 1, 'Yes', 'No' )) AS taxable"),
                DB::raw(" CASE adjustment_type.frequency WHEN 1 THEN 'Daily' ELSE 'Monthly' END as frequency ")
    		    
			);
    }

                        


    public function preUpdate($id, $data) {
    	$AdjustmentType = AdjustmentType::find($id);

    	$AdjustmentType->name         = $data['data']['name'];
    	$AdjustmentType->display_name = $data['data']['display_name'];
    	$AdjustmentType->description  = $data['data']['description'];
    	$AdjustmentType->type         = $data['data']['type'];
        $AdjustmentType->taxable      = $data['data']['taxable'];
        $AdjustmentType->frequency    = $data['data']['frequency'];
    	$AdjustmentType->updated_by   = 1;
    	$AdjustmentType->updated_at   = Carbon::now()->toDateTimeString();
        $AdjustmentType->save();

        return $AdjustmentType;

    }
}
