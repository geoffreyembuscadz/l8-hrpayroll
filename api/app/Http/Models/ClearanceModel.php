<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;
use \DB;
use App\Http\Controllers\UserController;

class ClearanceModel extends Model
{
    protected $table = "clearance"; use SoftDeletes;

    public static function getUser(){
      $contr = new UserController;
      $data = $contr->getListByIdLimited();

      return $data;
    }

    public function getClearanceList($data){

        $user = $this->getUser();

        $id= $user->user_id;

    	$data = DB::table('employee_clearance')
                ->join('employee', 'employee_clearance.employee_id', '=', 'employee.id')
                ->join('clearance_list', 'employee_clearance.id', '=', 'clearance_list.employee_clearance_id')
                ->join('clearance', 'clearance_list.clearance_id', '=', 'clearance.id')
                ->join('status_type', 'clearance_list.status_id', '=', 'status_type.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('status_type.name AS status'), 
                    DB::raw('\'\' AS val'),
                    'employee_clearance.id',
                    'clearance_list.status_id as status_id',
                    'clearance.name as type',
                    'clearance.id as clearance_id',
                    'clearance_list.id as clearance_list_id',
                    'clearance_list.remarks',
                    'employee_clearance.id as employee_clearance_id',
                    'employee_clearance.employee_id as employee_id',
                    'clearance_list.checker_id'
                )
                ->where('employee_clearance.created_by','=' ,$id)
                ->get();

        for ($i=0; $i < count($data); $i++) { 
        	$data[$i]->val =  false;
    	}

        return $data;
    }

    public function updateStatus($data){

    	$date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        $created_by = $user->user_id;

        $receiver_id = $data['receiver_id'];
        $table_id = $data['table_id'];
        $clearance = $data['clearance'];

        $da = $this->getEmployeeUserId($receiver_id);

    	foreach ($clearance as $key => $d) {

    		DB::table('clearance_list')->where('id',$d['id'])->update([
                'status_id'     => 8,
	            'remarks' 	    => $d['remarks'],
	            'updated_by'    => $created_by,
	            'updated_at'    => $date
	       	]);
    	}

    	$notif_id=DB::table('notification')->insertGetId(
                array(
                    'title'          => 'Clearance',
                    'description'    => 'Done Clearance',
                    'messenger_id'   => $created_by,
                    'table_from'     => 'employee_clearance',
                    'table_from_id'  => $table_id,
                    'seen'           => 0,
                    'route'          => 'employee-dashboard',
                    'receiver_id'    => $da->id,
                    'icon'           => 'fa fa-list-ol text-aqua',
                    'created_by'     => $created_by,
                    'created_at'     => $date
                    )
            );
            
        $notif[]= $notif_id;

        return $notif;
    }

    public function createClearance($data){
        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();


        foreach ($data as $key => $d) {
            $ids = DB::table('employee_clearance')->insertGetId(
                array(
                    'employee_id'               => $d['emp_id'],
                    'created_by'                => $user->user_id,
                    'created_at'                => $date
                    )
                );

            foreach ($d['clearance'] as $da) {
                DB::table('clearance_list')->insert(
                array(
                    'employee_clearance_id'    => $ids,
                    'checker_id'               => $da['checker_id'],
                    'clearance_id'             => $da['id'],
                    'status_id'                => 1,
                    'created_by'               => $user->user_id,
                    'created_at'               => $date
                    )
                );
            }
        }

        return $data;
    }

    public function duplicateEntryValidation($data){
        
        $already_exist=[];
        $leng = count($data['employee_id']);
        $edit = $data['edit'];
        $validate = [];

        for ($i=0; $i < $leng; $i++) { 

            $employee_id = $data['employee_id'][$i];

            $filter = DB::table('employee_clearance')
                ->join('employee', 'employee_clearance.employee_id', '=', 'employee.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    'employee_clearance.id',
                    'employee_clearance.employee_id'
                    )
                ->where('employee_clearance.employee_id',$employee_id);

            if($edit == true) {
                $id = $data['id'];
                $filter->where('employee_clearance.id','!=',$id);
            }

            $validate = $filter->first();

            if(count($validate)==1){
                $already_exist[]= array(
                    'name'   =>  $validate->name,
                    'id'     =>  $validate->id,
                    'emp_id' =>  $validate->employee_id
                );
            }
        }

        return $already_exist;
    }

    public function getMyClearanceList(){

        $user = $this->getUser();

        $id = $user->employee_id;

        $data = DB::table('employee_clearance')
                ->join('clearance_list', 'employee_clearance.id', '=', 'clearance_list.employee_clearance_id')
                ->join('clearance', 'clearance_list.clearance_id', '=', 'clearance.id')
                ->join('status_type', 'clearance_list.status_id', '=', 'status_type.id')
                ->join('employee', 'clearance_list.checker_id', '=', 'employee.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('status_type.name AS status'), 
                    DB::raw('\'\' AS val'),
                    'clearance_list.id',
                    'employee_clearance.id as employee_clearance_id',
                    'clearance.name as type',
                    'employee_clearance.employee_id',
                    'clearance_list.remarks'
                )
                ->where('employee_clearance.employee_id','=' ,$id)
                ->get();

        for ($i=0; $i < count($data); $i++) { 
            $data[$i]->val =  false;
        }

        return $data;
    }

    public function updateMyStatus($dat){

        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        $created_by = $user->user_id;
        $receiver_id = $user->supervisor_id;

        // $created_by = $dat['created_by'];
        // $receiver_id = $dat['receiver_id'];
        // $messenger_id = $dat['messenger_id'];
        $data = $dat['data'];
        $table_id = $dat['table_id'];

        foreach ($data as $key => $d) {

            DB::table('clearance_list')->where('id',$d)->update([
                'status_id'     => 7,
                'updated_by'    => $created_by,
                'updated_at'    => $date
            ]);
        }

        $notif_id=DB::table('notification')->insertGetId(
                array(
                    'title'          => 'Clearance',
                    'description'    => 'Check Clearance',
                    'messenger_id'   => $created_by,
                    'table_from'     => 'employee_clearance',
                    'table_from_id'  => $table_id,
                    'seen'           => 0,
                    'route'          => 'clearance-checklist',
                    'receiver_id'    => $receiver_id,
                    'icon'           => 'fa fa-list-ol text-aqua',
                    'created_by'     => $created_by,
                    'created_at'     => $date
                    )
            );
            
        $notif[]= $notif_id;

        return $notif;
    }

    public function getEmployeeUserId($id){

        $data = DB::table('users')
                ->leftJoin('employee_user', 'users.id', '=', 'employee_user.user_id')
                 ->select(
                    'users.id'
                    )
                ->where('employee_user.employee_id',$id)
                ->first();

        return $data;
    }

    public function employeeInfo($id){
        
        $data = DB::table('employee')
                ->join('employee_company', 'employee.id', '=', 'employee_company.employee_id')
                ->join('employee_position', 'employee.id', '=', 'employee_position.employee_id')
                ->join('company', 'employee_company.company_id', '=', 'company.id')
                ->join('position', 'employee_position.position', '=', 'position.id')
                ->select(
                     DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
                     'employee.employee_code',
                     'employee.date_hired',
                     'employee.starting_date',
                     'employee.end_date',
                     'employee.date_terminated',
                     'company.name as company_name',
                     'position.name as position_name'
                )
                ->where('employee.id','=' ,$id)
                ->first();

        return $data;
    }

    public function show2($id){

        $data = DB::table('employee_clearance')
                ->join('clearance_list', 'employee_clearance.id', '=', 'clearance_list.employee_clearance_id')
                ->join('clearance', 'clearance_list.clearance_id', '=', 'clearance.id')
                ->join('status_type', 'clearance_list.status_id', '=', 'status_type.id')
                ->select(
                    DB::raw('status_type.name AS status'), 
                    'clearance_list.clearance_id as id',
                    'clearance_list.checker_id'
                )
                ->where('employee_clearance.id','=' ,$id)
                ->get();

        for ($i=0; $i < count($data); $i++) { 
            $data[$i]->val =  false;
        }

        return $data;
    }

    public function updateClearance($data){

        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        $created_by= $user->user_id;
        $clear = $data['clearance'];
        $emp_id = $data['employee_id'][0];
        $id = $data['id'];

        $existval = [];
        $notexistval = [];
        $exist =[];

        DB::table('employee_clearance')->where('id',$id)->update([
            'employee_id'   => $emp_id,
            'updated_by'    => $user->user_id,
            'updated_at'    => $date
        ]);

        foreach ($clear as $key => $d) {

            $exist = DB::table('clearance_list')
            ->where('employee_clearance_id', $id)
            ->where('clearance_id', $d['id'])
            ->select('id')->first();

            if ($exist == []) {
                array_push(
                    $notexistval,array(
                        'id'            =>$d['id'], 
                        'checker_id'    =>$d['checker_id']
                    )
                );
            }else{
                array_push($existval, $exist->id);
            }
        }

        DB::table('clearance_list')
        ->where('employee_clearance_id', $id)
        ->whereNotIn('id', $existval)
        ->delete();

        if ($notexistval != []) {
            foreach ($notexistval as $d) {
                DB::table('clearance_list')->insert(
                array(
                    'employee_clearance_id'    => $id,
                    'clearance_id'             => $d['id'],
                    'checker_id'               => $d['checker_id'],
                    'status_id'                => 1,
                    'created_by'               => $created_by,
                    'created_at'               => $date
                    )
                );
            }
        }
        return $data;
    }

    public function getClear($id){

        $data = DB::table('employee_clearance')
                ->join('clearance_list', 'employee_clearance.id', '=', 'clearance_list.employee_clearance_id')
                ->join('clearance', 'clearance_list.clearance_id', '=', 'clearance.id')
                ->join('status_type', 'clearance_list.status_id', '=', 'status_type.id')
                ->join('employee', 'clearance_list.checker_id', '=', 'employee.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('status_type.name AS status'), 
                    DB::raw('\'\' AS val'),
                    'clearance_list.id',
                    'employee_clearance.id as employee_clearance_id',
                    'clearance.name as type',
                    'employee_clearance.employee_id',
                    'clearance_list.remarks'
                )
                ->where('employee_clearance.employee_id','=' ,$id)
                ->get();

        for ($i=0; $i < count($data); $i++) { 
            $data[$i]->val =  false;
        }

        return $data;
    }


}
