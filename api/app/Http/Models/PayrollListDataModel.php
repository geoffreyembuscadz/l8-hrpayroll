<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use \DB;

class PayrollListDataModel extends Model
{
    protected $table = "payroll_list"; use SoftDeletes;
}
