<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;

class MemoModel extends Model
{
    protected $table = "memo"; 
    use SoftDeletes;

    public static function getMemoList($date){
     $memo = DB::table('memo')
             ->join('employee', 'memo.emp_id', '=', 'employee.employee_code', 'left')
             ->select(
                      DB::raw('memo.date'),
                      DB::raw('memo.name'),
                      DB::raw('employee.employee_image As emp_pic')
                    )
             ->where('memo_type_id', '=', '1')
             ->where('date', '=', $date)
             ->distinct()
             ->limit(2)
             ->get();
     return $memo;
   }

   public static function getMemo($date){

       $query = DB::table('memo')
                ->join('company', 'memo.company_id', '=', 'company.id','left')
                ->join('employee', 'memo.emp_id', '=', 'employee.employee_code', 'left')
                ->join('memo_type', 'memo.memo_type_id', '=', 'memo_type.id', 'left')
                ->select(DB::raw('memo.date'), 
                 DB::raw('memo.name AS memo_name'), 
                 DB::raw('company.name as com_name'),
                 DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `emp_memo`'))
                ->where('memo.memo_type_id', '=', '1')
                ->where('date', '=', $date)
                ->get();
       return $query;

   }

   public function filterMemo($data){
    // dd($data);exit;
    $start_date = $data['start_date'];
    $end_date = $data['end_date'];
    $emp_id = $data['emp_id'];
    $company_id = $data['company_id'];

    $filter = DB::table('memo')
            ->join('company', 'memo.company_id', '=', 'company.id','left')
            ->join('employee', 'memo.emp_id', '=', 'employee.employee_code', 'left')
            ->join('memo_type', 'memo.memo_type_id', '=', 'memo_type.id', 'left')
            ->select(DB::raw('memo.date'), 
             DB::raw('memo.name As memo_name'), 
             DB::raw('company.name as com_name'),
             DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `emp_memo`'));

    if( $start_date != 'null' && $end_date != 'null'){
      $filter->whereBetween('memo.date', [$start_date, $end_date]);
    }

    if ( isset($emp_id) && !empty($emp_id) ) {
      $filter->where('memo.memo_type_id', '=', 2)
             ->where('memo.emp_id', '=', $emp_id);
    }    

    if (isset($company_id) && !empty($company_id)) {
        $filter->where('memo.memo_type_id', '=', 1)
               ->where('memo.company_id', '=', $company_id);
    }  

    $data = $filter->get();
    return $data;
  }

  public static function getMemoType(){
    return $memo_type = DB::table('memo_type')
          ->select('memo_type.id', 'memo_type.name as text')
          ->get();
  }

  public static function getEmpMemo(){
   return $query = DB::table('memo')
         ->join('employee', 'memo.emp_id', '=', 'employee.id')
         ->select(DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `emp_memo`'),
          DB::raw('memo.name'))
         ->where('memo_type_id', '=', '2')
         ->get();
  }

  // public function createMemo($data){
  //   $date = date('Y-m-d H:i:s');

  //   foreach ($data as $da) {
  //     DB::table('memo')->insert(
  //       array(
  //         'company_id'        => $da[0],
  //         'name'              => $da[1],
  //         'memo_type_id'      => $da[2],
  //         'emp_id'            => $da[4],
  //         'date'              => $da[3],
  //         'created_by'        => $da[5],
  //         'created_at'        => $date
  //       )
  //     );
  //   }

  //   return $data;
  // }

}
