<?php
namespace App\Http\Models;
use \DB;
use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\UserController;

class ScheduleAdjustmentsModel extends Model
{
    protected $table = "schedule_adjustments"; use SoftDeletes;

    static $static_suggested_schedules = [
        ['break_end'=>'4:00:00','id'=>1,'break_start' =>'3:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 12:00AM - 09:00AM / Break: 03:00AM - 04:00AM','time_in'=>'00:00:00','time_out'=>'9:00:00'],
        ['break_end'=>'5:00:00','id'=>2,'break_start' =>'4:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 01:00AM - 10:00AM / Break: 04:00AM - 05:00AM','time_in'=>'1:00:00','time_out'=>'10:00:00'],
        ['break_end'=>'6:00:00','id'=>3,'break_start' =>'5:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 02:00AM - 11:00AM / Break: 05:00AM - 06:00AM','time_in'=>'2:00:00','time_out'=>'11:00:00'],
        ['break_end'=>'7:00:00','id'=>4,'break_start' =>'6:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 03:00AM - 12:00PM / Break: 06:00AM - 07:00AM','time_in'=>'3:00:00','time_out'=>'12:00:00'],
        ['break_end'=>'8:00:00','id'=>5,'break_start' =>'7:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 04:00AM - 01:00PM / Break: 07:00AM - 08:00AM','time_in'=>'4:00:00','time_out'=>'13:00:00'],
        ['break_end'=>'9:00:00','id'=>6,'break_start' =>'8:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 05:00AM - 02:00PM / Break: 08:00AM - 09:00AM','time_in'=>'5:00:00','time_out'=>'14:00:00'],
        ['break_end'=>'10:00:00','id'=>7,'break_start' =>'9:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 06:00AM - 03:00PM / Break: 09:00AM - 10:00AM','time_in'=>'6:00:00','time_out'=>'15:00:00'],
        ['break_end'=>'11:00:00','id'=>8,'break_start' =>'10:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 07:00AM - 04:00PM / Break: 10:00AM - 11:00AM','time_in'=>'7:00:00','time_out'=>'16:00:00'],
        ['break_end'=>'12:00:00','id'=>9,'break_start' =>'11:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 08:00AM - 05:00PM / Break: 11:00AM - 12:00PM','time_in'=>'8:00:00','time_out'=>'17:00:00'],
        ['break_end'=>'13:00:00','id'=>10,'break_start' =>'12:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 09:00AM - 06:00PM / Break: 12:00PM - 01:00PM','time_in'=>'9:00:00','time_out'=>'18:00:00'],
        ['break_end'=>'14:00:00','id'=>11,'break_start' =>'13:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 10:00AM - 07:00PM / Break: 01:00PM - 02:00PM','time_in'=>'10:00:00','time_out'=>'19:00:00'],
        ['break_end'=>'15:00:00','id'=>12,'break_start' =>'14:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 11:00AM - 08:00PM / Break: 02:00PM - 03:00PM','time_in'=>'11:00:00','time_out'=>'20:00:00'],
        ['break_end'=>'16:00:00','id'=>13,'break_start' =>'15:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 12:00PM - 09:00PM / Break: 03:00PM - 04:00PM','time_in'=>'12:00:00','time_out'=>'21:00:00'],
        ['break_end'=>'17:00:00','id'=>14,'break_start' =>'16:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 01:00PM - 10:00PM / Break: 04:00PM - 05:00PM','time_in'=>'13:00:00','time_out'=>'22:00:00'],
        ['break_end'=>'18:00:00','id'=>15,'break_start' =>'17:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 02:00PM - 11:00PM / Break: 05:00PM - 06:00PM','time_in'=>'14:00:00','time_out'=>'23:00:00'],
        ['break_end'=>'19:00:00','id'=>16,'break_start' =>'18:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 03:00PM - 12:00AM / Break: 06:00PM - 07:00PM','time_in'=>'15:00:00','time_out'=>'0:00:00'],
        ['break_end'=>'20:00:00','id'=>17,'break_start' =>'19:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 04:00PM - 01:00AM / Break: 07:00PM - 08:00PM','time_in'=>'16:00:00','time_out'=>'1:00:00'],
        ['break_end'=>'21:00:00','id'=>18,'break_start' =>'20:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 05:00PM - 02:00AM / Break: 08:00PM - 09:00PM','time_in'=>'17:00:00','time_out'=>'2:00:00'],
        ['break_end'=>'22:00:00','id'=>19,'break_start' =>'21:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 06:00PM - 03:00AM / Break: 09:00PM - 10:00PM','time_in'=>'18:00:00','time_out'=>'3:00:00'],
        ['break_end'=>'23:00:00','id'=>20,'break_start' =>'22:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 07:00PM - 04:00AM / Break: 10:00PM - 11:00PM','time_in'=>'19:00:00','time_out'=>'4:00:00'],
        ['break_end'=>'24:00:00','id'=>21,'break_start' =>'23:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 08:00PM - 05:00AM / Break: 11:00PM - 12:00AM','time_in'=>'20:00:00','time_out'=>'5:00:00'],
        ['break_end'=>'1:00:00','id'=>22,'break_start' =>'0:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 09:00PM - 06:00AM / Break: 12:00AM - 01:00AM','time_in'=>'21:00:00','time_out'=>'6:00:00'],
        ['break_end'=>'2:00:00','id'=>23,'break_start' =>'1:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 10:00PM - 07:00AM / Break: 01:00AM - 02:00AM','time_in'=>'22:00:00','time_out'=>'7:00:00'],
        ['break_end'=>'3:00:00','id'=>24,'break_start' =>'2:00:00','lunch_mins_break'=>60,'text'=>'Schedule: 11:00PM - 08:00AM / Break: 02:00AM - 03:00AM','time_in'=>'23:00:00','time_out'=>'8:00:00']
    ];

    public function getUser(){
        $contr = new UserController;
        $data = $contr->getListByIdLimited();

        return $data;
    }
    public function getUserId($id){
        $contr = new UserController;
        $data = $contr->getUserId($id);

        return $data;
    }

    public static function getSuggestedSchedulesAdjustment(){
        return self::$static_suggested_schedules;
    }

    public static function getList(){

        return self::join('status_type', 'schedule_adjustments.status_id', '=', 'status_type.id')
        ->join('employee', 'schedule_adjustments.emp_id', '=', 'employee.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('SUBSTRING(schedule_adjustments.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(schedule_adjustments.final_remarks, 1, 15) as final_remarks'), 
            'schedule_adjustments.id', 
            'schedule_adjustments.created_at', 
            'schedule_adjustments.emp_id',
            'schedule_adjustments.start_date',
            'schedule_adjustments.end_date',
            'schedule_adjustments.date_applied')
        ->orderBy('schedule_adjustments.created_at', 'desc')
        ->get();

    }

    public function getScheduleAdjustmentRequestBySA($id){
        $data = [];

        if($id){
            $data = DB::table('schedule_adjustments_request')->where('schedule_adjustments_request.SA_id', $id)->get();
        }

        return $data;
    }

    public function getSArequest($id){
        $data = [];

        if($id){
            $data = DB::table('schedule_adjustments_request')
                ->join('employee', 'employee.id', '=', 'schedule_adjustments_request.employee_id')
                ->join('employee_company', 'employee.id', '=', 'employee_company.employee_id')
                ->join('company', 'company.id', '=', 'employee_company.company_id')
                ->whereRaw('schedule_adjustments_request.deleted_at IS NULL AND schedule_adjustments_request.deleted_by IS NULL')
                ->where('schedule_adjustments_request.id', $id)
                ->select('schedule_adjustments_request.id', 'schedule_adjustments_request.date', 'schedule_adjustments_request.start_time', 'schedule_adjustments_request.end_time', 'schedule_adjustments_request.break_start', 'schedule_adjustments_request.break_end', DB::raw('TIMESTAMPDIFF(MINUTE, schedule_adjustments_request.break_start, schedule_adjustments_request.break_end) AS lunch_mins_break'), DB::raw("concat(employee.firstname, ' ', employee.lastname) as employee_name, employee.id as employee_id, company.id as company_id, company.name as company_name"))
                ->first();

        }
        
        return $data;
    }

    public static function show($id){
        
       $data = DB::table('schedule_adjustments_request')
        ->join('schedule_adjustments', 'schedule_adjustments_request.SA_id', '=', 'schedule_adjustments.id')
        ->join('status_type', 'schedule_adjustments.status_id', '=', 'status_type.id')
        ->join('employee', 'schedule_adjustments.emp_id', '=', 'employee.id')
        ->select(
            DB::raw('schedule_adjustments_request.SA_id AS request_id'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('schedule_adjustments.remarks AS reason'),
            DB::raw('status_type.name AS status'), 
            'schedule_adjustments.final_remarks',
            'schedule_adjustments.id',
            'schedule_adjustments.emp_id', 
            'schedule_adjustments.start_date', 
            'schedule_adjustments.end_date',
            'schedule_adjustments.remarks',
            'schedule_adjustments_request.date',
            'schedule_adjustments_request.start_time',
            'schedule_adjustments_request.end_time',
            'schedule_adjustments_request.break_start',
            'schedule_adjustments_request.break_end')
        ->where('schedule_adjustments_request.SA_id','=' ,$id)
        ->get();

        return $data;

    }

    public function createSA($data){
        
        $notif=[];
        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        foreach ($data as $counter => $d) {
           
            $SA_id = DB::table('schedule_adjustments')->insertGetId(
                        array(
                            'status_id'     => 1,
                            'emp_id'        => $d['emp_id'],
                            'start_date'    => $d['start_date'],
                            'end_date'      => $d['end_date'],
                            'date_applied'  => $date,
                            'remarks'       => $d['remarks'],
                            'created_by'    => $user->user_id,
                            'created_at'    => $date
                        )
                    );


            foreach ($d['details'] as $da) {
                DB::table('schedule_adjustments_request')->insert(
                        array(
                            'SA_id'             => $SA_id,
                            'date'              => $da['date'],
                            'start_time'        => $da['start_time'],
                            'end_time'          => $da['end_time'],
                            'break_start'       => $da['break_start'],
                            'break_end'         => $da['break_end'],
                            'created_by'        => $user->user_id,
                            'created_at'        => $date,
                            'employee_id'       => $d['emp_id']
                            )
                    );
            }

            $messenger_id = null;
            $messenger_id = $this->getUserId($d['emp_id']);

            if ($messenger_id != null) {
                $notif_id=DB::table('notification')->insertGetId(
                    array(
                        'title'          => 'Schedule Adjustment Request',
                        'description'    => $d['remarks'],
                        'messenger_id'   => $messenger_id->user_id,
                        'table_from'     => 'schedule_adjustments',
                        'table_from_id'  => $SA_id,
                        'seen'           => 0,
                        'route'          => 'schedule-adjustments-list',
                        'receiver_id'    => $user->supervisor_id,
                        'icon'           => 'fa fa-pencil-square-o text-aqua',
                        'created_by'     => $user->user_id,
                        'created_at'     => $date
                        )
                );
                $notif[$counter]= $notif_id;
            }
        }

    return $notif;
    }

    public function getMySA($data){

        $user = $this->getUser();

        $employee_id = $user->employee_id;
        $status_id = $data['status_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];
        
        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

       $filter = DB::table('schedule_adjustments')
        ->join('status_type', 'schedule_adjustments.status_id', '=', 'status_type.id')
        ->join('employee', 'schedule_adjustments.emp_id', '=', 'employee.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('SUBSTRING(schedule_adjustments.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(schedule_adjustments.final_remarks, 1, 15) as final_remarks'), 
            'schedule_adjustments.id', 
            'schedule_adjustments.created_at', 
            'schedule_adjustments.emp_id',
            'schedule_adjustments.start_date',
            'schedule_adjustments.end_date',
            'schedule_adjustments.date_applied')
        ->where('schedule_adjustments.emp_id','=',$employee_id)
        ->orderBy('schedule_adjustments.created_at', 'desc');

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('schedule_adjustments.date_applied', [$start_date,$end_date]);
        }
        if($status_id[0] != 'null') {
            $stat = [];
            $len = count($status_id);
            for ($x=0; $x < $len ; $x++) { 
                $stat[$x]= (int)$status_id[$x];
            }
            $filter->whereIn('schedule_adjustments.status_id',$stat);
        }

        $data = $filter->get();

        return $data;

    }

    public function updateSave($id,$d,$user){

        $date = date('Y-m-d H:i:s');

        DB::table('schedule_adjustments')->where('id',$id)->update([
            'emp_id'        => $d['employee_id'][0],
            'start_date'    => $d['start_date'],
            'end_date'      => $d['end_date'],
            'remarks'       => $d['remarks'],   
            'created_by'    => $user->user_id,
            'created_at'    => $date

        ]);

        foreach ($d['details'] as $da) {
            DB::table('schedule_adjustments_request')->insert(
                array(
                    'SA_id'             => $id,
                    'date'              => $da['date'],
                    'start_time'        => $da['start_time'],
                    'end_time'          => $da['end_time'],
                    'break_start'       => $da['break_start'],
                    'break_end'         => $da['break_end'],
                    'created_by'        => $user->user_id,
                    'created_at'        => $date,
                    'employee_id'       => $d['emp_id']
                    )
            );
        }
    }

    public function updateData($file){
        $d = $file;
        $user = $this->getUser();
        $date = date('Y-m-d H:i:s');
        $id = $d['id'];
        $data = [];


        if($d['details'][0]['request_id'] == 0){

            DB::table('schedule_adjustments_request')
            ->where('SA_id', $id)->delete();

            $return = $this->updateSave($id,$d,$user);
        }
        elseif($d['details'][0]['request_id'] != 0){

            DB::table('schedule_adjustments')->where('id',$id)->update([
                        'emp_id'        => $d['employee_id'][0],
                        'start_date'    => $d['start_date'],
                        'end_date'      => $d['end_date'],
                        'remarks'       => $d['remarks'],
                        'created_by'    => $user->user_id,
                        'created_at'    => $date

            ]);

            $dat = DB::table('schedule_adjustments_request')
            ->where('SA_id', $id)->get();
            
            if (count($dat) != count($d['details'])) {
                $len = count($d['details']);
                $temp = [];
                for ($x=0; $x < $len ; $x++) { 
                    $temp[$x]= $d['details'][$x]['date'];
                }

                DB::table('schedule_adjustments_request')
                ->where('SA_id', $id)
                ->whereNotIn('date', $temp)
                ->delete();
            }


            foreach ($d['details'] as $key => $da) {
                $request_id = $da['request_id'];

                DB::table('schedule_adjustments_request')->where('id',$request_id)->update([
                    'date'           => $da['date'],
                    'start_time'     => $da['start_time'],
                    'end_time'       => $da['end_time'],
                    'break_start'    => $da['break_start'],
                    'break_end'      => $da['break_end'],
                    'updated_by'     => $user->user_id,
                    'updated_at'     => $date
                ]);
            }
        }

        $table=DB::table('schedule_adjustments')
            ->where('id','=',$id)
            ->select()
            ->get();

        return $table;
    }

    public function getSA($data){

        $user = $this->getUser();

        $supervisor_id = $user->user_id;
        $role_id = $user->role_id;

        $employee_id = $data['employee_id'];
        $company_id = $data['company_id'];
        $status_id = $data['status_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];
        
        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('schedule_adjustments')
        ->join('status_type', 'schedule_adjustments.status_id', '=', 'status_type.id')
        ->join('employee', 'schedule_adjustments.emp_id', '=', 'employee.id')
        ->join('employee_company', 'schedule_adjustments.emp_id', '=', 'employee_company.employee_id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('SUBSTRING(schedule_adjustments.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(schedule_adjustments.final_remarks, 1, 15) as final_remarks'), 
            'schedule_adjustments.id', 
            'schedule_adjustments.created_at', 
            'schedule_adjustments.emp_id',
            'schedule_adjustments.start_date',
            'schedule_adjustments.end_date',
            'schedule_adjustments.date_applied')
        ->whereRaw('schedule_adjustments.deleted_at IS NULL')
        ->whereRaw('schedule_adjustments.deleted_by IS NULL')
        ->orderBy('schedule_adjustments.created_at', 'desc');

        if($role_id > 2 && $role_id < 0) {
            $filter->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
        }

        if( !empty($start)  && !empty($end) ){
            $filter->whereBetween('schedule_adjustments.date_applied', [$start_date,$end_date]);
        }

        if(!empty($status_id[0])) {
            $stat = [];
            $len = count($status_id);
            for ($x=0; $x < $len ; $x++) { 
                $stat[$x]= (int)$status_id[$x];
            }
            $filter->whereIn('schedule_adjustments.status_id',$stat);
        }

        if (!empty($company_id[0])) {
            $comp = [];
            $len = count($company_id);
            for ($x=0; $x < $len ; $x++) { 
                $comp[$x]= (int)$company_id[$x];
            }
            $filter->whereIn('employee_company.company_id',$comp);
        }

        if(!empty($employee_id[0])) {
            $filter->whereIn('schedule_adjustments.emp_id',$employee_id);
        }

        $data = $filter->get();

        return $data;

    }

    public function validateDuplication($data){
        
        $len = count($data);
        $already_exist=[];

        for ($x=0; $x < $len; $x++) { 
            $leng = count($data[$x][5]);
            for ($i=0; $i < $leng ; $i++) { 

                $id = $data[$x][0];
                $date = $data[$x][5][$i]['date'];

                $validate =  DB::table('schedule_adjustments_request')
                ->join('schedule_adjustments', 'schedule_adjustments_request.SA_id', '=', 'schedule_adjustments.id')
                ->join('status_type', 'schedule_adjustments.status_id', '=', 'status_type.id')
                ->join('employee', 'schedule_adjustments.emp_id', '=', 'employee.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('status_type.name AS status'),
                    'schedule_adjustments.id'
                    )
                ->where('schedule_adjustments_request.date', '=' ,$date)
                ->where('schedule_adjustments.emp_id', '=' ,$id)
                ->first();
            }
                if(count($validate)==1){
                    $already_exist[]= array(
                    'name'  =>  $validate->name,
                    'id'    =>  $validate->id,
                    'status' => $validate->status);
                }
        }

        return $already_exist;
    }

    public function preUpdateDuplication($data){
        
        $already_exist=[];
        $len = count($data['details']);
        $validate = [];


        for ($i=0; $i <$len ; $i++) { 
            
            $id = $data['employee_id'][0];
            $date = $data['details'][$i]['date'];
            $start_time = $data['details'][$i]['start_time'];
            $end_time = $data['details'][$i]['end_time'];
            $break_start = $data['details'][$i]['break_start'];
            $break_end = $data['details'][$i]['break_end'];
            $reason =  $data['remarks'];

            $validate = DB::table('schedule_adjustments_request')
                ->join('schedule_adjustments', 'schedule_adjustments_request.SA_id', '=', 'schedule_adjustments.id')
                ->join('status_type', 'schedule_adjustments.status_id', '=', 'status_type.id')
                ->join('employee', 'schedule_adjustments.emp_id', '=', 'employee.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('status_type.name AS status'),
                    'schedule_adjustments.id'
                    )
                ->where('schedule_adjustments_request.date', '=' ,$date)
                ->where('schedule_adjustments_request.start_time', '=' ,$start_time)
                ->where('schedule_adjustments_request.end_time', '=' ,$end_time)
                ->where('schedule_adjustments_request.break_start', '=' ,$break_start)
                ->where('schedule_adjustments_request.break_end', '=' ,$break_end)
                ->where('schedule_adjustments.emp_id', '=' ,$id)
                ->where('schedule_adjustments.remarks', '=' ,$reason)
                ->first();
        }

        if(count($validate)==1){
            $already_exist[]= array(
                'name'  =>  $validate->name,
                'id'    =>  $validate->id,
                'status' => $validate->status
            );
        }

        return $already_exist;
    }
    public function duplicateEntryValidation($data){
        
        $already_exist=[];
        $len = count($data['details']);
        $leng = count($data['employee_id']);
        $edit = $data['edit'];
        $validate = [];

        for ($x=0; $x <$leng ; $x++) { 
            
            $id = $data['employee_id'][$x];
            $start_date =  $data['start_date'];
            $end_date =  $data['end_date'];

            $filter = DB::table('schedule_adjustments_request')
                ->join('schedule_adjustments', 'schedule_adjustments_request.SA_id', '=', 'schedule_adjustments.id')
                ->join('status_type', 'schedule_adjustments.status_id', '=', 'status_type.id')
                ->join('employee', 'schedule_adjustments.emp_id', '=', 'employee.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('status_type.name AS status'),
                    'schedule_adjustments.id',
                    'schedule_adjustments.emp_id'
                    )
                ->whereBetween('schedule_adjustments_request.date', [$start_date,$end_date])
                ->where('schedule_adjustments.emp_id', '=' ,$id);

                if($edit == true) {
                    $id = $data['id'];
                    $filter->where('schedule_adjustments.id','!=',$id);
                }

                $validate = $filter->first();

            if(count($validate)==1){
                $already_exist[]= array(
                    'name'  =>  $validate->name,
                    'id'    =>  $validate->id,
                    'emp_id' =>  $validate->emp_id,
                    'status' => $validate->status
                );
            }
        }


        return $already_exist;
    }

}
