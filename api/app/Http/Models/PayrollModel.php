<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use \DB AS DB;


class PayrollModel extends Model
{
    protected $table = "payroll"; use SoftDeletes;	
    
    public function preList($model) {
    	return $model
               ->leftJoin('users', 'payroll.prepared_by', '=', 'users.id')
               ->leftJoin(DB::raw("(SELECT id, name FROM users) as approver"), 'payroll.approved_by', '=', 'approver.id')
               ->leftJoin(DB::raw("(
                 SELECT
                    DISTINCT id,
                    CASE type_of_run WHEN 1 THEN 'Company'
                                         WHEN 2 THEN 'Department'
                                         WHEN 3 THEN 'Position'
                                         ELSE 'All' END as `type_of_run`,
                    CONCAT(CAST(start_time AS DATE), ' - ',CAST(end_time AS DATE)) as `payroll_period`
                    FROM payroll WHERE deleted_at is NULL) as payroll_sort"), 'payroll.id', '=', 'payroll_sort.id');
    }
    
    public function postList($model) {
    	return $model->select(
            DB::raw('DISTINCT payroll.id'),
            'payroll.name', 
    		'payroll_sort.type_of_run as type_of_run', 
            'payroll.approved', 
            'users.name as prepared_by',
            'approver.name as approved_by',
            'payroll_sort.payroll_period as payroll_period',
            'payroll.created_at'
    	);
    }

}
