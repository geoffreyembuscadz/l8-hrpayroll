<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\Models\LoanTypeModel as Loans;


class LoanTypeModel extends Model
{
    protected $table = "loan_type"; use SoftDeletes;



    public function preUpdate($id, $data = []){

        $loan = new Loans;
        $loan = Loans::find($id);
        $loan->name            = $data['data']['name'];
        $loan->display_name    = $data['data']['display_name'];
        $loan->description     = $data['data']['description'];
        $loan->type            = $data['data']['type'];
        $loan->include_payslip = $data['data']['include_payslip'];
        $loan->save();

        return $data;
    }
}
