<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\AttendanceReportModel;

class DashboardModel extends Model
{

    protected $table = "employees_logs";
    use SoftDeletes;
    public  $temp;

    public function __constructor(){
        $this->temp = AttendanceReportModel();
    }

    public static function getLate($date){


        $query = DB::table(
            DB::raw("(
                SELECT 
                DISTINCT COUNT(employee_id) emp_id
                FROM employees_logs
                WHERE late != 0 AND
                DATE(time_in) = CURDATE()
            ) As `a`
            ")
        )->get();
      
        return $query;
    }

    public static function showLate($date){

        return DB::table('employees_logs As e_l')
                    ->join('employee As e', 'e_l.employee_id', '=', 'e.employee_code')
                    ->join('employee_working_schedule As ews', 'e_l.employee_id', '=', 'ews.employee_id') 
                    ->join('working_schedule As w_s', 'ews.working_schedule_id', '=', 'w_s.id')
                    ->select(
                             DB::raw('Cast(e_l.time_in As date) As date_given'),
                             DB::raw('CONCAT(e.firstname, " ",e.lastname) AS `employee_name`'),
                             DB::raw('w_s.time_in'),
                             DB::raw('w_s.time_out'),
                             DB::raw('e_l.late')
                             )
                    // ->whereDate('e_l.time_in', '=', date('Y-m-d'))
                    ->get();     
        }

   public static function getLateFilter($data){
        $start = $data['start_date'];
        $end = $data['end_date'];
        $company_id = $data['company_id'];
        $employee_id = $data['employee_id'];

        $query = DB::table('employee As e')
                    ->join('employees_logs As e_l', 'e.employee_code', '=', 'e_l.employee_id')
                    ->join('employee_working_schedule As ews', 'e_l.employee_id', '=', 'ews.employee_id') 
                    ->join('working_schedule As w_s', 'ews.working_schedule_id', '=', 'w_s.id')
                    ->join('employee_company As e_c', 'e.employee_code', '=', 'e_c.employee_id', 'left')
                    ->join('company As c', 'e_c.company_id', '=', 'c.id', 'left')
                    ->select(
                             DB::raw('Cast(e_l.time_in As date) As date_given'),
                             DB::raw('CONCAT(e.firstname, " ",e.lastname) AS `employee_name`'),
                             DB::raw('w_s.time_in'),
                             DB::raw('w_s.time_out'),
                             DB::raw('e_l.late')
                             );
                 

        if (!empty($start && $end)){
            $query->whereBetween('e_l.time_in', [$start, $end]);   
        }

        if (!empty($company_id) && isset($company_id[0])) {
            $comp = [];
            $arr = count($company_id);            
            for ($a=0; $a < $arr ; $a++) { 
                $comp[$a]= (int) $company_id[$a];
            }
            $query->select('c.name')
                  ->whereNotIn('e_c.company_id', $comp);
        }
        // $emp = $this->temp->employee_list();

        if(!empty($employee_id) && isset($employee_id)) {
            $emp = [];
            for ($x=0, $y = Count($employee_id); $x < $y; $x++) { 
                $emp[$x] = (int) $employee_id[$x];
            }
            $query->whereIn('e.employee_code',$emp);
        }
        $data = $query->get();
        return $data;

    }

    public static function lateDetails(){
        return $lateDetails = DB::raw('employees_logs')
                                ->join('employee', 'employees_logs.employee_id', '=', 'employee.id')
                                ->select(
                                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `created_by`'),
                                    DB::raw('employees_logs.time_in'))
                                ->get();
    } 

    

    public static function getAllRequest(){

        $pendingLeave = DB::table('leave')
                        ->join('status_type', 'leave.status_id', '=', 'status_type.id')
                        ->join('employee', 'leave.emp_id', '=', 'employee.employee_code')
                        ->select(
                            DB::raw('leave.id'),
                            DB::raw('leave.created_at AS date'),
                            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `created_by`'),
                            DB::raw('\'Leave\' as table_name'),
                            DB::raw('\'Leave\' as type'),
                            DB::raw('\'leave\' as url')
                            )
                        ->where('leave.status_id', '=', '1')
                        ->where('leave.deleted_at', '=', NULL);
        
        $pendingCOA = DB::table('certi_of_atten')
                        ->join('status_type', 'certi_of_atten.status_id', '=', 'status_type.id')
                        ->join('employee', 'certi_of_atten.emp_id', '=', 'employee.employee_code')
                        ->select(
                            DB::raw('certi_of_atten.id'),
                            DB::raw('certi_of_atten.created_at AS date'),
                            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `created_by`'),
                            DB::raw('\'COA\' as table_name'),
                            DB::raw('\'COA\' as type'),
                            DB::raw('\'certificate_of_attendance\' as url')
                            )
                        ->where('status_id', '=', '1')
                        ->where('certi_of_atten.deleted_at', '=', NULL);

        $OB =  DB::table('official_business')
                        ->join('status_type', 'official_business.status_id', '=', 'status_type.id')
                        ->join('employee', 'official_business.emp_id', '=', 'employee.employee_code')
                        ->select(
                            DB::raw('official_business.id'),
                            DB::raw('official_business.created_at AS date'),
                            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `created_by`'),
                            DB::raw('\'OB\' as table_name'),
                            DB::raw('\'Official Business\' as type'),
                            DB::raw('\'official_business\' as url')
                            )
                        ->where('status_id', '=', '1') 
                        ->where('official_business.deleted_at', '=', NULL);

        $OT =   DB::table('overtime')
                        ->join('status_type', 'overtime.status_id', '=', 'status_type.id')
                        ->join('employee', 'overtime.emp_id', '=', 'employee.employee_code')
                        ->select(
                            DB::raw('overtime.id'),
                            DB::raw('overtime.created_at AS date'),
                            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `created_by`'),
                            DB::raw('\'Overtime\' as table_name'),
                            DB::raw('\'Overtime\' as type'),
                            DB::raw('\'overtime\' as url')
                            )
                        ->where('status_id', '=', '1')
                        ->where('overtime.deleted_at', '=', NULL); 

        $SA =   DB::table('schedule_adjustments')
                        ->join('status_type', 'schedule_adjustments.status_id', '=', 'status_type.id')
                        ->join('employee', 'schedule_adjustments.emp_id', '=', 'employee.employee_code')
                        ->select(
                            DB::raw('schedule_adjustments.id'),
                            DB::raw('schedule_adjustments.created_at AS date'),
                            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `created_by`'),
                            DB::raw('\'Schedule Adjustment\' as table_name'),
                            DB::raw('\'SA\' as type'),
                            DB::raw('\'schedule_adjustments\' as url')
                            )
                        ->where('status_id', '=', '1')
                        ->where('schedule_adjustments.deleted_at', '=', NULL); 

        $UT =   DB::table('official_undertime')
                        ->join('status_type', 'official_undertime.status_id', '=', 'status_type.id')
                        ->join('employee', 'official_undertime.emp_id', '=', 'employee.employee_code')
                        ->select(
                            DB::raw('official_undertime.id'),
                            DB::raw('official_undertime.created_at AS date'),
                            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `created_by`'),
                            DB::raw('\'Undertime\' as table_name'),
                            DB::raw('\'Undertime\' as type'),
                            DB::raw('\'official_undertime\' as url')
                            )
                        ->where('status_id', '=', '1')
                        ->where('official_undertime.deleted_at', '=', NULL)
                        ->unionAll($pendingLeave)
                        ->unionAll($pendingCOA)
                        ->unionAll($OB)
                        ->unionAll($OT)
                        ->unionAll($SA)
                        ->orderBy('date')
                        ->limit(5)
                        ->distinct()
                        ->get();

        return $UT; 

        }

        public static function employeeCount(){

        $query = DB::table(
            DB::raw("(
              SELECT COUNT(id) As late_count
              FROM employee
            ) As `a`
            ")
        )->get()->toArray();

        return $query;
        }


        public static function getEmployeeActive(){

            $query =  DB::table(
                DB::raw("(
                SELECT e_l.employee_id, e_c.company_id, c.name, 
                COUNT(e_l.employee_id) AS employee_count, c.id As c_id
                FROM employees_logs AS e_l
                JOIN employee_company e_c ON e_l.employee_id = e_c.employee_id
                JOIN company AS c ON e_c.company_id = c.id
                WHERE CAST(e_l.time_in AS date) = CURDATE()
                GROUP BY e_l.employee_id
                ORDER BY c_id
                ) As `a`
                ")
            )->get();

            $array = [];
            foreach ($query as $key => $value) {
                $array[] = $value->employee_count;
            }

            return $array;
     
        }

         public static function getCompanyActive(){

            $query =  DB::table(
                DB::raw("(
                SELECT id, name company 
                FROM company
                ORDER BY id
                ) As `a`
                ")

            )->get();

            $array = [];
            foreach ($query as $key => $value) {
                $array[] = $value->company;
            }

            return $array;
     
        }

        public static function getPendingRequestCount() {
            $query = DB::table(
                DB::raw("(
                        SELECT 
                        (select count(*) from `leave` join employee on employee.id = leave.emp_id where status_id = 1 and `leave`.deleted_at is null ) +  
                        (select count(*) from certi_of_atten join employee on employee.id = certi_of_atten.emp_id where status_id = 1 and certi_of_atten.deleted_at is null ) +
                        (select count(*) from official_business join employee on employee.id = official_business.emp_id where status_id = 1 and official_business.deleted_at is null ) +
                        (select count(*) from overtime join employee on employee.id = overtime.emp_id where status_id = 1 and overtime.deleted_at is null ) +
                        (select count(*) from schedule_adjustments join employee on employee.id = schedule_adjustments.emp_id where status_id = 1 and schedule_adjustments.deleted_at is null )  +
                        (select count(*) from official_undertime join employee on employee.id = official_undertime.emp_id where status_id = 1 and official_undertime.deleted_at is null ) as requests
                    ) as allRequest")
                )->get();


            $array = [];
            foreach ($query as $key => $value) {
                $array[] = $value->requests;
            }

            return $array;
            return $array;
        }

  

}