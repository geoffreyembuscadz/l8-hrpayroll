<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\UserController;

class CompanyPolicyTypeModel extends Model
{
    protected $table = "company_policy_type"; use SoftDeletes;

    public function getUser(){
        $contr = new UserController;
        $data = $contr->getListByIdLimited();

        return $data;
    }

    public function getUserId($id){
        $contr = new UserController;
        $data = $contr->getUserId($id);

        return $data;
    }

    public function getList(){

        return self::join('company', 'company_policy_type.company_id', '=', 'company.id')
        ->select(
            DB::raw('company.name as company'),
            'company_policy_type.id', 
            'company_policy_type.name', 
            'company_policy_type.company_id')
        ->orderBy('company_policy_type.created_at', 'desc')
        ->get();

    }

    public function show($id){

        return self::join('company', 'company_policy_type.company_id', '=', 'company.id')
        ->select(
            DB::raw('company.name as company'),
            'company_policy_type.id', 
            'company_policy_type.name', 
            'company_policy_type.company_id')
        ->where('company_policy_type.id','=',$id)
        ->first();
    }
}
