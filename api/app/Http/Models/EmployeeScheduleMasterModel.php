<?php
namespace App\Http\Models;
use \DB;
use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeScheduleMasterModel extends Model
{
    protected $table = "employee_schedule_master"; use SoftDeletes;

    public function preListJoin($model){
        return $model->whereRaw('employee.deleted_at IS NULL')
        	->join('employee', 'employee.id', '=', 'employee_schedule_master.employee_id')
        	->join('employee_company', 'employee_company.employee_id', '=', 'employee.id')
        	->join('company', 'company.id', '=', 'employee_company.company_id');
    }

    public function postListJoin($model){
        return $model->select(DB::raw("DISTINCT employee_schedule_master.id,employee.id as emp_id, employee.employee_code,employee.firstname,employee.middlename,employee.lastname,employee.status,  CONCAT( employee.firstname , ' ' , employee.lastname) as employee_fullname,company.name as company_name,DATE_FORMAT(employee_schedule_master.schedule_date, '%M %e, %Y') as schedule_date,DATE_FORMAT(employee_schedule_master.timein, '%h:%i %p') as timein,date_format(employee_schedule_master.breakin,'%h:%i %p') as breakin,date_format(employee_schedule_master.breakout,'%h:%i %p') as breakout,date_format(employee_schedule_master.timeout,'%h:%i %p') as timeout"));
    }
}
