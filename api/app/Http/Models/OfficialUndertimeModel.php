<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\UserController;
use \DB;

class OfficialUndertimeModel extends Model
{
    protected $table = "official_undertime"; use SoftDeletes;


    public function getUser(){
        $contr = new UserController;
        $data = $contr->getListByIdLimited();

        return $data;
    }
    public function getUserId($id){
        $contr = new UserController;
        $data = $contr->getUserId($id);

        return $data;
    }

    public static function getList(){

        return self::join('status_type', 'official_undertime.status_id', '=', 'status_type.id')
        ->join('employee', 'official_undertime.emp_id', '=', 'employee.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('SUBSTRING(official_undertime.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(official_undertime.final_remarks, 1, 15) as final_remarks'), 
            'official_undertime.date_applied', 
            'official_undertime.date', 
            'official_undertime.id', 
            'official_undertime.created_at', 
            'official_undertime.emp_id',
            'official_undertime.start_time',
            'official_undertime.end_time',
            'official_undertime.total_hours')
        ->orderBy('official_undertime.created_at', 'desc')
        ->get();

    }

    public static function show($id){

        return self::join('status_type', 'official_undertime.status_id', '=', 'status_type.id')
        ->join('employee', 'official_undertime.emp_id', '=', 'employee.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('official_undertime.remarks AS reason'), 
            'official_undertime.date_applied', 
            'official_undertime.date', 
            'official_undertime.id', 
            'official_undertime.created_at', 
            'official_undertime.emp_id',
            'official_undertime.start_time',
            'official_undertime.end_time',
            'official_undertime.total_hours',
            'official_undertime.final_remarks')
        ->where('official_undertime.id', '=' ,$id)
        ->first();

    }

    public function createOU($data){
        
        $notif=[];
        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        foreach ($data as $counter => $d) {
            $OU_id = DB::table('official_undertime')->insertGetId(
                    array(
                        'emp_id'         => $d['emp_id'],
                        'date'           => $d['date'],
                        'start_time'     => $d['start_time'],
                        'end_time'       => $d['end_time'],
                        'total_hours'    => $d['total_hours'],
                        'remarks'        => $d['remarks'],
                        'status_id'      => 1,
                        'date_applied'   => $date,
                        'created_by'     => $user->user_id,
                        'created_at'     => $date
                        )
                );

            $messenger_id = null;
            $messenger_id = $this->getUserId($d['emp_id']);

            if ($messenger_id != null) {

                $notif_id=DB::table('notification')->insertGetId(
                    array(
                        'title'          => 'Undertime Request',
                        'description'    => $d['remarks'],
                        'messenger_id'   => $messenger_id->user_id,
                        'table_from'     => 'official_undertime',
                        'table_from_id'  => $OU_id,
                        'seen'           => 0,
                        'route'          => 'undertime-list',
                        'receiver_id'    => $user->supervisor_id,
                        'icon'           => 'fa fa-pencil-square-o text-aqua',
                        'created_by'     => $user->user_id,
                        'created_at'     => $date
                        )
                );
                $notif[$counter]= $notif_id;
            }
        }

    return $notif;

	}

    public function getMyOU($data){

        $user = $this->getUser();

        $employee_id = $user->employee_id;
        $status_id = $data['status_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];

        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('official_undertime')
        ->join('status_type', 'official_undertime.status_id', '=', 'status_type.id')
        ->join('employee', 'official_undertime.emp_id', '=', 'employee.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('SUBSTRING(official_undertime.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(official_undertime.final_remarks, 1, 15) as final_remarks'),
            'official_undertime.date_applied', 
            'official_undertime.date', 
            'official_undertime.id', 
            'official_undertime.created_at', 
            'official_undertime.emp_id',
            'official_undertime.start_time',
            'official_undertime.end_time',
            'official_undertime.total_hours')
        ->where('official_undertime.emp_id','=',$employee_id)
        ->orderBy('official_undertime.created_at', 'desc');

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('official_undertime.date_applied', [$start_date,$end_date]);
        }

        if($status_id[0] != 'null') {
            $stat = [];
            $len = count($status_id);
            for ($x=0; $x < $len ; $x++) { 
                $stat[$x]= (int)$status_id[$x];
            }
            $filter->whereIn('official_undertime.status_id',$stat);
        }

        $data = $filter->get();
        return $data;

    }

     public function updateData($file){
        $d = $file;
        $user = $this->getUser();
        $date = date('Y-m-d H:i:s');
        $id = $d['id'];
        $data = [];

        DB::table('official_undertime')->where('id',$id)->update([
                    'emp_id'        => $d['employee_id'][0],
                    'date'          => $d['date'],
                    'start_time'    => $d['start_time'],
                    'end_time'      => $d['end_time'],
                    'total_hours'   => $d['total_hours'],
                    'remarks'       => $d['remarks'],
                    'updated_by'    => $user->user_id,
                    'updated_at'    => $date
        ]);

        return $data;
    }

    public function getOU($data){

        $dat = $this->getUser();

        $supervisor_id = $dat->user_id;
        $role_id = $dat->role_id;

        $employee_id = $data['employee_id'];
        $company_id = $data['company_id'];
        $status_id = $data['status_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];

        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('official_undertime')
        ->join('status_type', 'official_undertime.status_id', '=', 'status_type.id')
        ->join('employee', 'official_undertime.emp_id', '=', 'employee.id')
        ->join('employee_company', 'official_undertime.emp_id', '=', 'employee_company.employee_id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
            DB::raw('SUBSTRING(official_undertime.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(official_undertime.final_remarks, 1, 15) as final_remarks'),
            'official_undertime.date_applied', 
            'official_undertime.date', 
            'official_undertime.id', 
            'official_undertime.created_at', 
            'official_undertime.emp_id',
            'official_undertime.start_time',
            'official_undertime.end_time',
            'official_undertime.status_id',
            'official_undertime.total_hours')
        ->where('official_undertime.deleted_at','=',null)
        ->where('official_undertime.deleted_by','=',null)
        ->orderBy('official_undertime.created_at', 'desc');
        
        if($role_id > 2 AND $role_id < 0) {
            $filter->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
        }

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('official_undertime.date_applied', [$start_date,$end_date]);
        }
        if($status_id[0] != 'null') {
            $stat = [];
            $len = count($status_id);
            for ($x=0; $x < $len ; $x++) { 
                $stat[$x]= (int)$status_id[$x];
            }
            $filter->whereIn('official_undertime.status_id',$stat);
        }
        if ($company_id[0] != 'null') {
            $comp = [];
            $len = count($company_id);
            for ($x=0; $x < $len ; $x++) { 
                $comp[$x]= (int)$company_id[$x];
            }
            $filter->whereIn('employee_company.company_id',$comp);
        }
        if($employee_id[0] != 'null') {
            $filter->whereIn('official_undertime.emp_id',$employee_id);
        }
        $data = $filter->get();
        return $data;
    }

    public function validateDuplication($data){
        $len = count($data);
        $already_exist=[];
        

        for ($x=0; $x < $len; $x++) { 

            $id = $data[$x][0];
            $date = $data[$x][5];

            $validate = self::join('status_type', 'official_undertime.status_id', '=', 'status_type.id')
            ->join('employee', 'official_undertime.emp_id', '=', 'employee.id')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('status_type.name AS status'),
                'official_undertime.id'
                )
            ->where('official_undertime.date', '=' ,$date)
            ->where('official_undertime.emp_id', '=' ,$id)
            ->first();
            if(count($validate)==1){
                $already_exist[]= array(
                    'name'  =>  $validate["name"],
                    'id'    =>  $validate["id"],
                    'status' => $validate["status"]);
            }
        }

        return $already_exist;
    }

    public function preUpdateDuplication($data){
        
        $already_exist=[];
        $id = $data['employee_id'];
        $date = $data['date'];
        $start_time = $data['start_time'];
        $end_time = $data['end_time'];
        $remarks = $data['remarks'];

        $validate = self::join('status_type', 'official_undertime.status_id', '=', 'status_type.id')
            ->join('employee', 'official_undertime.emp_id', '=', 'employee.id')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('status_type.name AS status'),
                'official_undertime.id'
                )
            ->where('official_undertime.date', '=' ,$date)
            ->where('official_undertime.emp_id', '=' ,$id)
            ->where('official_undertime.start_time', '=' ,$start_time)
            ->where('official_undertime.end_time', '=' ,$end_time)
            ->where('official_undertime.remarks', '=' ,$remarks)
            ->first();

        if(count($validate)==1){
            $already_exist[]= array(
                    'name'  =>  $validate["name"],
                    'id'    =>  $validate["id"],
                    'status' => $validate["status"]
                );
        }

        return $already_exist;
    }
    public function duplicateEntryValidation($data){
        
        $already_exist=[];
        $date = $data['date'];
        $edit = $data['edit'];
        $leng = count($data['employee_id']);

        for ($i=0; $i < $leng ; $i++) { 
            
            $id = $data['employee_id'][$i];
            $filter = self::join('status_type', 'official_undertime.status_id', '=', 'status_type.id')
                ->join('employee', 'official_undertime.emp_id', '=', 'employee.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('status_type.name AS status'),
                    'official_undertime.id',
                    'official_undertime.emp_id'
                    )
                ->where('official_undertime.date', '=' ,$date)
                ->where('official_undertime.emp_id', '=' ,$id);
                
                if($edit == true) {
                    $id = $data['id'];
                    $filter->where('official_undertime.id','!=',$id);
                }

                $validate = $filter->first();

            if(count($validate)==1){
                $already_exist[]= array(
                        'name'        =>  $validate["name"],
                        'id'          =>  $validate["id"],
                        'emp_id'      =>  $validate["emp_id"],
                        'status'      =>  $validate["status"]
                    );
            }
        }

        return $already_exist;
    }
}
