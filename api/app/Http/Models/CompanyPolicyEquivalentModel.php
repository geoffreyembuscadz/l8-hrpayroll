<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyPolicyEquivalentModel extends Model
{
    protected $table = "company_policy_equivalent"; use SoftDeletes;
}
