<?php
namespace App\Http\Models;

use DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;

class Holiday_TypeModel extends Model
{
    protected $table = "holiday_type"; use SoftDeletes;

    public static function getHolidayType(){
    	$query = DB::table('holiday_type')
    			 ->select('id', 'name As text', 'description', 'computation')
    			 ->get();
    	return $query;
    }

   	
}
