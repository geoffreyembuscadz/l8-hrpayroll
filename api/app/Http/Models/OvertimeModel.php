<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\UserController;
use App\Http\Models\AttendanceModel;
use \DB;

class OvertimeModel extends Model
{
    protected $table = "overtime"; use SoftDeletes;

    public function getUser(){
        $contr = new UserController;
        $data = $contr->getListByIdLimited();

        return $data;
    }

    public function getUserId($id){
        $contr = new UserController;
        $data = $contr->getUserId($id);

        return $data;
    }

    public function getScheduleAdjustment($id, $days, $date) {
        $attendance = new AttendanceModel;
        $schedule_adjustments = $attendance->checkSA($id, $days, $date);

        return $schedule_adjustments;
    }

    public static function getList(){

        return self::join('status_type', 'overtime.status_id', '=', 'status_type.id')
        ->join('employee', 'overtime.emp_id', '=', 'employee.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('SUBSTRING(overtime.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(overtime.final_remarks, 1, 15) as final_remarks'),
            'overtime.date',
            'overtime.date_applied',
            'overtime.id', 
            'overtime.created_at', 
            'overtime.emp_id',
            'overtime.start_time',
            'overtime.end_time',
            'overtime.total_hours')
        ->orderBy('overtime.created_at', 'desc')
        ->get();

    }

    public function show($id){

      return self::join('status_type', 'overtime.status_id', '=', 'status_type.id')
        ->join('employee', 'overtime.emp_id', '=', 'employee.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('overtime.remarks AS reason'), 
            'overtime.date',
            'overtime.final_remarks',
            'overtime.date_applied',
            'overtime.id', 
            'overtime.created_at', 
            'overtime.emp_id',
            'overtime.start_time',
            'overtime.end_time',
            'overtime.total_hours')
        ->where('overtime.id', '=' ,$id)
        ->first();

    }

    public function createOvertime($data){

        $notif=[];
        $current_date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        foreach ($data as $counter => $d) {
            $date = strtotime($d['date']);
            $day_of_the_week = date("l", $date);
            $days = strtolower($day_of_the_week);

            // check restday
            $schedule = $this->getScheduleAdjustment($d['emp_id'], $days, $date);
            // $restday = (strpos($schedule->days, $day_of_the_week) === false ? 1 : 0);
            $restday = ($schedule->restday == 1 ? 1 : 0);

            $OT_id =  DB::table('overtime')->insertGetId(
                    array(
                        'date'           => $d['date'],
                        'emp_id'         => $d['emp_id'],
                        'start_time'     => $d['start_time'],
                        'end_time'       => $d['end_time'],
                        'total_hours'    => $d['total_hours'],
                        'remarks'        => $d['remarks'],
                        'status_id'      => 1,
                        'restday'        => $restday,
                        'date_applied'   => $current_date,
                        'created_by'     => $user->user_id,
                        'created_at'     => $current_date
                        )
                );

            $messenger_id = null;
            $messenger_id = $this->getUserId($d['emp_id']);

            if ($messenger_id != null) {
                $notif_id=DB::table('notification')->insertGetId(
                    array(
                        'title'          => 'Overtime Request',
                        'description'    => $d['remarks'],
                        'messenger_id'   => $messenger_id->user_id,
                        'table_from'     => 'overtime',
                        'table_from_id'  => $OT_id,
                        'seen'           => 0,
                        'route'          => 'overtime-list',
                        'receiver_id'    => $user->supervisor_id,
                        'icon'           => 'fa fa-pencil-square-o text-aqua',
                        'created_by'     => $user->user_id,
                        'created_at'     => $date
                        )
                );
                $notif[$counter]= $notif_id;
            }
        }

    return $notif;
	}

    public function getMyOT($data){

        $user = $this->getUser();

        $employee_id = $user->employee_id;
        $status_id = $data['status_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];
        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('overtime')
        ->join('status_type', 'overtime.status_id', '=', 'status_type.id')
        ->join('employee', 'overtime.emp_id', '=', 'employee.id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('SUBSTRING(overtime.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(overtime.final_remarks, 1, 15) as final_remarks'),
            'overtime.date',
            'overtime.date_applied',
            'overtime.id', 
            'overtime.created_at', 
            'overtime.emp_id',
            'overtime.start_time',
            'overtime.end_time',
            'overtime.total_hours')
        ->where('overtime.emp_id','=',$employee_id)
        ->orderBy('overtime.created_at', 'desc');

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('overtime.date_applied', [$start_date,$end_date]);
        }

        if($status_id[0] != 'null') {
            $stat = [];
            $len = count($status_id);
            for ($x=0; $x < $len ; $x++) { 
                $stat[$x]= (int)$status_id[$x];
            }
            $filter->whereIn('overtime.status_id',$stat);
        }

        $data = $filter->get();
        return $data;

    }

    public function updateData($file){
        $d = $file;
        $user = $this->getUser();
        $current_date = date('Y-m-d H:i:s');
        $id = $d['id'];
        $data = [];

        $date = strtotime($d['date']);

        $day_of_the_week = date("l", $date);
        $days = strtolower($day_of_the_week);
        // check restday
        $schedule = $this->getScheduleAdjustment($d['employee_id'], $days, $date);
        $restday = ($schedule->restday == 1 ? 1 : 0);

        DB::table('overtime')->where('id',$id)->update([
                    'emp_id'        => $d['employee_id'][0],
                    'date'          => $d['date'],
                    'start_time'    => $d['start_time'],
                    'end_time'      => $d['end_time'],
                    'total_hours'   => $d['total_hours'],
                    'remarks'       => $d['remarks'],
                    'restday'       => $restday,
                    'updated_by'    => $user->user_id,
                    'updated_at'    => $current_date
        ]);

        return $data;
    }

    public function getOvertime($data){

        $user = $this->getUser();

        $supervisor_id = $user->user_id;
        $role_id = $user->role_id;

        $employee_id = $data['employee_id'];
        $company_id = $data['company_id'];
        $status_id = $data['status_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];

        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('overtime')
        ->join('status_type', 'overtime.status_id', '=', 'status_type.id')
        ->join('employee', 'overtime.emp_id', '=', 'employee.id')
        ->join('employee_company', 'overtime.emp_id', '=', 'employee_company.employee_id')
        ->select(
            DB::raw('status_type.name AS status'), 
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('SUBSTRING(overtime.remarks, 1, 15) AS reason'), 
            DB::raw('SUBSTRING(overtime.final_remarks, 1, 15) as final_remarks'), 
            'overtime.date',
            'overtime.date_applied', 
            'overtime.id', 
            'overtime.created_at', 
            'overtime.emp_id',
            'overtime.start_time',
            'overtime.end_time',
            'overtime.total_hours')
        ->where('overtime.deleted_at','=',null)
        ->where('overtime.deleted_by','=',null)
        ->orderBy('overtime.created_at', 'desc');

        if($role_id > 2 AND $role_id < 0) {
            $filter->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
        }

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('overtime.date_applied', [$start_date,$end_date]);
        }
        if($status_id[0] != 'null') {
            $stat = [];
            $len = count($status_id);
            for ($x=0; $x < $len ; $x++) { 
                $stat[$x]= (int)$status_id[$x];
            }
            $filter->whereIn('overtime.status_id',$stat);
        }
        if ($company_id[0] != 'null') {
            $comp = [];
            $len = count($company_id);
            for ($x=0; $x < $len ; $x++) { 
                $comp[$x]= (int)$company_id[$x];
            }
            $filter->whereIn('employee_company.company_id',$comp);
        }
        if($employee_id[0] != 'null') {
            $filter->whereIn('overtime.emp_id',$employee_id);
        }
        $data = $filter->get();

        return $data;
    }

    public function validateDuplication($data){
        $len = count($data);
        $already_exist=[];

        for ($x=0; $x < $len; $x++) { 

            $id = $data[$x][0];
            $date = $data[$x][6];

            $validate = self::join('employee', 'overtime.emp_id', '=', 'employee.id')
            ->join('status_type', 'overtime.status_id', '=', 'status_type.id')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('status_type.name AS status'),
                'overtime.id'
                )
            ->where('overtime.date', '=' ,$date)
            ->where('overtime.emp_id', '=' ,$id)
            ->first();
            if(count($validate)==1){
                $already_exist[]= array(
                    'name'  =>  $validate["name"],
                    'id'    =>  $validate["id"],
                    'status' => $validate["status"]);
            }
        }
        return $already_exist;
    }

    public function preUpdateDuplication($data){
        
        $already_exist=[];
        $id = $data['employee_id'];
        $date = $data['date'];
        $start_time = $data['start_time'];
        $end_time = $data['end_time'];
        $remarks = $data['remarks'];

        $validate = self::join('employee', 'overtime.emp_id', '=', 'employee.id')
            ->join('status_type', 'overtime.status_id', '=', 'status_type.id')
            ->select(
                DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                DB::raw('status_type.name AS status'),
                'overtime.id'
                )
            ->where('overtime.date', '=' ,$date)
            ->where('overtime.emp_id', '=' ,$id)
            ->where('overtime.start_time', '=' ,$start_time)
            ->where('overtime.end_time', '=' ,$end_time)
            ->where('overtime.remarks', '=' ,$remarks)
            ->first();

        if(count($validate)==1){
            $already_exist[]= array(
                    'name'  =>  $validate["name"],
                    'id'    =>  $validate["id"],
                    'status' => $validate["status"]
                );
        }

        return $already_exist;
    }

    public function duplicateEntryValidation($data){
        $already_exist=[];
        $date = $data['date'];
        $edit = $data['edit'];
        $leng = count($data['employee_id']);

        for ($i=0; $i < $leng ; $i++) { 
            
            $employee_id = $data['employee_id'][$i];
            $filter = self::join('employee', 'overtime.emp_id', '=', 'employee.id')
                ->join('status_type', 'overtime.status_id', '=', 'status_type.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('status_type.name AS status'),
                    'overtime.id',
                    'overtime.emp_id'
                    )
                ->where('overtime.date', '=' ,$date)
                ->where('overtime.emp_id', '=' ,$employee_id);

                if($edit == true) {
                    $id = $data['id'];
                    $filter->where('overtime.id','!=',$id);
                }

                $validate = $filter->first();

            if(count($validate)==1){
                $already_exist[]= array(
                        'name'      =>  $validate["name"],
                        'id'        =>  $validate["id"],
                        'emp_id'    =>  $validate["emp_id"],
                        'status'    => $validate["status"]
                    );
            }
        }

        return $already_exist;
    }

    public function approvedData($id){
        $data = $this->show($id);

        $date = $data->date;
        $emp_id = $data->emp_id;
        $table = null;

        $table=DB::table('employees_logs')
                ->whereRaw("(CAST(employees_logs.time_in AS date) = '$date' or CAST(employees_logs.time_out AS date)= '$date')")
                ->where('employee_id',$emp_id)
                ->select('id')
                ->first();

        if ($table != null) {
            $date = date('Y-m-d H:i:s');
            $user = $this->getUser();

            DB::table('employees_logs')->where('id',$table->id)->update([
                'overtime'          => 1,
                'updated_by'        => $user->user_id,
                'updated_at'        => $date
            ]);
        }

        return $data;
    }

}
