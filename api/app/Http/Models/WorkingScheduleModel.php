<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkingScheduleModel extends Model
{
    protected $table = "working_schedule"; use SoftDeletes;
    // alter table working_schedule modify column days varchar(80);
}
