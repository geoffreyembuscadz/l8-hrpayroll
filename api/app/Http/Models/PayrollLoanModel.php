<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;

class PayrollLoanModel extends Model
{
    protected $table = "payroll_loan"; 
    use SoftDeletes;
}
