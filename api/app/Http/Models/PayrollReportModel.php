<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DB as DB;

class PayrollReportModel extends Model
{
    protected $table = "payroll"; use SoftDeletes;

    /**
     * get employee list with total number reports
     * 
     * @return $employee 
     */
    public function getEmployee() {
        $employee = DB::table('employee')
                    ->whereRaw('employee.deleted_at IS NULL')
                    ->join('employee_company', function($join){
                        $join->on('employee.id', '=', 'employee_company.employee_id');
                        $join->on('employee.branch', '=', 'employee_company.branch_id');
                    })
                    ->join('company', 'employee_company.company_id', '=', 'company.id')
                    ->join('position', 'employee.position', '=', 'position.id')
                    ->join('department', 'employee.department', '=', 'department.id')
                    ->leftJoin('payroll_list as pl', 'employee.id', '=', 'pl.emp_id')
                    ->whereNull('pl.deleted_at')
                    ->whereRaw('employee_company.deleted_at IS NULL')
                    ->orderBy('employee.lastname')
                    ->groupBy('employee.id')
                    ->select(DB::raw('DISTINCT employee.id'), 
                        'employee.employee_code', 
                        DB::raw("CONCAT( employee.firstname , ' ' , employee.lastname) as name"),
                        DB::raw('IFNULL(SUM(pl.total_sss), 0) as total_sss'),
                        DB::raw('IFNULL(SUM(pl.total_sss_employer), 0) as total_sss_employer'),
                        DB::raw('IFNULL(SUM(pl.total_philhealth), 0) as total_philhealth'),
                        DB::raw('IFNULL(SUM(pl.total_philhealth_employer), 0) as total_philhealth_employer'),
                        DB::raw('IFNULL(SUM(pl.total_pagibig), 0) as total_pagibig'),
                        DB::raw('IFNULL(SUM(pl.total_pagibig_employer), 0) as total_pagibig_employer'),
                        DB::raw('IFNULL(SUM(pl.total_adjustment), 0) as total_adjustment'),
                        DB::raw('IFNULL(SUM(pl.total_deduction), 0) as total_deduction')
                     )->get();

        return $employee;
    }


    /**
     * select employee details and payrolllust details
     * 
     * @param  $emp_id get the employee.id to compare for the query
     * @return $payroll_report
     */
    public function getPayrollReport($emp_id) {
    	
    	$payroll_report = DB::table('payroll')
                            ->leftJoin('payroll_list as pl', 'payroll.id', '=', 'pl.payroll_id')
                            ->select('pl.id as payroll_id', 'pl.emp_id', 'pl.total_adjustment', 'pl.total_deduction', 'pl.total_sss', 'pl.total_philhealth', 'pl.total_pagibig', 'pl.total_loan', 'payroll.start_time', 'payroll.end_time', 'pl.total_sss_employer', 'pl.total_philhealth_employer', 'pl.total_pagibig_employer' )
                            ->where('pl.emp_id', '=', $emp_id)
                            ->whereNull('pl.deleted_at')
                            ->get();

    	return $payroll_report;

    }

    /**
     * get data on payroll_adjustment table where it is an ADJUSTMENT
     * 
     * @param  obj $model
     * @param  int $payroll_id
     * @param  int $emp_id
     * @return $payroll_adjustment
     */
    public function getPayrollAdjustment($emp_id) {

    	$payroll_adjustment = DB::table('payroll_adjustment as pa')
                                ->leftJoin('payroll as p', 'pa.payroll_id', '=', 'p.id')
                                ->leftJoin('adjustment_type as at', 'pa.adjustment_type', '=', 'at.id')
                                ->select( 'p.start_time', 'p.end_time', 'pa.payroll_id', 'pa.adjustment_id', 'pa.employee_id', 'pa.adjustment_type', 'pa.adjustment_amount', 'pa.type', 'at.display_name' )
                                ->where('at.type', '=', 'ADJUSTMENT')
                                ->where('pa.employee_id', '=', $emp_id)
                                ->whereNull('pa.deleted_at')
                                ->get();

    	return $payroll_adjustment;

    }

    /**
     * get data on payroll_adjustment table where it is a DEDUCTION
     * 
     * @param  int $payroll_id 
     * @param  int $emp_id     
     * @return $payroll_deduction
     */
    public function getPayrollDeduction($emp_id) {

        $payroll_deduction = DB::table('payroll_adjustment as pa')
                                ->leftJoin('payroll as p', 'pa.payroll_id', '=', 'p.id')
                                ->leftJoin('adjustment_type as at', 'pa.adjustment_type', '=', 'at.id')
                                ->select( 'p.start_time', 'p.end_time', 'pa.payroll_id', 'pa.adjustment_id', 'pa.employee_id', 'pa.adjustment_type', 'pa.adjustment_amount', 'pa.type', 'at.display_name' )
                                ->where('at.type', '=', 'DEDUCTION')
                                ->where('pa.employee_id', '=', $emp_id)
                                ->whereNull('pa.deleted_at')
                                ->get();

        return $payroll_deduction;

    }

    /**
     * get data on payroll_loan 
     * 
     * @param  int $payroll_id
     * @param  int $emp_id
     * @return $payroll_loan
     */
    public function getPayrollLoan($emp_id) {

    	$payroll_loan = DB::table('payroll_loan as pl')
                          ->leftJoin('payroll as p', 'pl.payroll_id', '=', 'p.id')
                          ->select( 'start_time', 'end_time', 'payroll_id', 'loan_id', 'employee_id', 'loan_type', 'loan_amount', 'type')
    	                  ->where('employee_id', '=', $emp_id)
                          ->whereNull('pl.deleted_at')
                          ->get();

    	return $payroll_loan;

    }

}
