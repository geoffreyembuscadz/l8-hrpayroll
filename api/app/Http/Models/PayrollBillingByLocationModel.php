<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\AdjustmentTypeModel as AdjustmentType;
use App\Http\Models\LoanTypeModel as LoanType;
use App\Http\Models\AttendanceModel as AttendanceModel;
use \App\Http\Models\PayrollListDataModel as PayrollModel;
use App\Http\Models\AdjustmentModel as Adjustment;
use App\Http\Models\LoanModel as Loan;
use \DB AS DB;

class PayrollBillingByLocationModel extends Model
{
    protected $table = "employees_logs"; use SoftDeletes; 
    
     /**
     * get payroll pre list
     * 
     * @param  object $model 
     * @param  datetime $start     
     * @param  datetime $end      
     * @param  integer $run_type    via employee/company/department/position
     * @param  integer $company  
     * @param  integer $branch     
     * @param  integer $department 
     * @param  integer $position   
     * @param  integer $cutoff      1 or 0
     * @return $payroll             
     */
    public function preList($model, $start, $end, $run_type, $company, $branch, $department, $position, $cutoff, $removed_employee) {
        return $model
            ->join('employee_company', 'employees_logs.employee_id', '=', 'employee_company.employee_id')
            ->join('company', 'employee_company.company_id', '=', 'company.id')
            // select fields from employee table
            ->leftJoin(DB::raw("( SELECT id, firstname, lastname, e_cola, status, department, position, branch FROM employee ) AS employee"), 'employees_logs.employee_id' , '=', 'employee.id')
            // select fields from employee_rate
            ->leftJoin(DB::raw("( SELECT employee_id, IFNULL(hourly_rate, 0) AS hourly_rate, IFNULL(daily_rate, 0) AS daily_rate, IFNULL(weekly_rate, 0) AS weekly_rate, IFNULL(monthly_rate, 0) AS monthly_rate FROM employee_rate WHERE deleted_at is NULL
            ) AS employee_rate"), 'employees_logs.employee_id', '=', 'employee_rate.employee_id' )
            ->join(DB::raw("(
                SELECT DISTINCT 
                    ROUND(IF(company.attendance_default = 0, attendance_default.number_of_days,
                        IF(company.undertime_rule = '1', ROUND(SUM(employees_logs.no_of_days_undertime) / 8, 2), IF(company.late_convertion = 1, SUM(employees_logs.no_of_days_converted), SUM(employees_logs.no_of_days)) / 8)
                    ), 2) AS no_of_days,

                    ROUND(employee_rate.allowance_amount * IF(company.attendance_default = 0, attendance_default.number_of_days,
                        IF(company.undertime_rule = '1', ROUND(SUM(employees_logs.no_of_days_undertime) / 8, 2), IF(company.late_convertion = 1, SUM(employees_logs.no_of_days_converted), SUM(employees_logs.no_of_days)) / 8)
                    ) ,2) AS total_allowance,

                    employee_rate.allowance_amount AS allowance,

                    ROUND(IF(company.attendance_default = 0, attendance_default.late, SUM(IF(company.undertime_rule = '0', IF(company.late_convertion = '0', IFNULL(employees_logs.late ,0) , IFNULL(employees_logs.late_converted, 0)) , 0 ) )), 2) AS emp_late,

                    ROUND((((employee_rate.daily_rate + employee_rate.allowance_amount) / 8 * IF(company.attendance_default = 0, attendance_default.late, SUM(IF(company.undertime_rule = '0', IF(company.late_convertion = '0', IFNULL(employees_logs.late ,0) , IFNULL(employees_logs.late_converted, 0)) , 0) ))  ) / 60), 2) AS total_emp_late,

                    ROUND(IF(company.attendance_default = 0, attendance_default.undertime, IF(company.undertime_rule = '1', IF(company.late_convertion = '0', IFNULL((100 - (100 / (480 / employees_logs.undertime))) * 0.01 , 0) ,IFNULL((100 - (100 / (480 / employees_logs.undertime))) * 0.01 , 0)), IF(company.late_convertion = '0', SUM(employees_logs.undertime) , SUM(employees_logs.undertime_converted)) )), 2) AS emp_undertime,

                    ROUND(IF(company.undertime_rule = '1', (employee_rate.daily_rate + employee_rate.allowance_amount) * IF(company.attendance_default = 0, attendance_default.undertime,
                             IF(company.late_convertion = '0', IFNULL((100 - (100 / (480 / employees_logs.undertime))) * 0.01 , 0) , IFNULL((100 - (100 / (480 / employees_logs.undertime))) * 0.01 , 0))), (((employee_rate.daily_rate + employee_rate.allowance_amount) / 8) * IF(company.attendance_default = 0, attendance_default.undertime, IF(company.late_convertion = '0', SUM(employees_logs.undertime) , SUM(employees_logs.undertime_converted)))) / 60) ,2) AS total_undertime,
                    emp.cola AS e_cola,

                    ROUND(IF(company.attendance_default = 0, attendance_default.number_of_days,
                        IF(company.undertime_rule = '1', ROUND(SUM(employees_logs.no_of_days_undertime) / 8, 2), IF(company.late_convertion = 1, SUM(employees_logs.no_of_days_converted), SUM(employees_logs.no_of_days)) / 8)
                    ) * emp.cola, 2) AS cola,

                    ROUND((((employee_rate.daily_rate) / 12) * IF(company.attendance_default = 0, ROUND(attendance_default.number_of_days, 2), IF(company.undertime_rule = '1', ROUND(SUM(employees_logs.no_of_days_undertime) / 8, 2), IF(company.late_convertion = 1, SUM(employees_logs.no_of_days_converted), SUM(employees_logs.no_of_days)) / 8))), 2) AS thirteen_month,

                    ROUND( (employee_rate.daily_rate) * IF(company.attendance_default = 0, attendance_default.number_of_days,
                        IF(company.undertime_rule = '1', ROUND(ROUND(SUM(employees_logs.no_of_days_undertime) / 8, 2), 2), IF(company.late_convertion = 1, SUM(employees_logs.no_of_days_converted), SUM(employees_logs.no_of_days)) / 8)
                    ), 2) AS ttl_basic_pay,
                    employees_logs.branch_id AS branch,

                    employees_logs.employee_id AS emp_code

                FROM (SELECT employee_id AS employee_id, time_in AS time_in, halfday AS halfday, deleted_at AS deleted_at, 
                        SUM(CASE WHEN employees_logs.halfday = '0' AND employees_logs.time_out IS NOT NULL AND employees_logs.late < 60 THEN (480 - employees_logs.late) / 60
                                WHEN employees_logs.halfday = '1' AND employees_logs.time_out IS NOT NULL THEN 1 
                                WHEN employees_logs.halfday = '2' AND employees_logs.time_out IS NOT NULL THEN 2 
                                WHEN employees_logs.halfday = '3' AND employees_logs.time_out IS NOT NULL THEN 3 
                                WHEN employees_logs.halfday = '4' AND employees_logs.time_out IS NOT NULL THEN 4 
                                WHEN employees_logs.halfday = '5' AND employees_logs.time_out IS NOT NULL THEN 5 
                                WHEN employees_logs.halfday = '6' AND employees_logs.time_out IS NOT NULL THEN 6 
                                WHEN employees_logs.halfday = '7' AND employees_logs.time_out IS NOT NULL THEN 7 
                                ELSE 0 END) 
                        AS no_of_days_undertime,              
                        SUM(CASE WHEN employees_logs.halfday = '0' AND employees_logs.time_out IS NOT NULL AND employees_logs.late < 60 THEN 8
                                WHEN employees_logs.halfday = '0' AND employees_logs.late >= 60 THEN (480 - employees_logs.late) / 60
                                WHEN employees_logs.halfday = '1' AND employees_logs.time_out IS NOT NULL THEN 1 
                                WHEN employees_logs.halfday = '2' AND employees_logs.time_out IS NOT NULL THEN 2 
                                WHEN employees_logs.halfday = '3' AND employees_logs.time_out IS NOT NULL THEN 3 
                                WHEN employees_logs.halfday = '4' AND employees_logs.time_out IS NOT NULL THEN 4 
                                WHEN employees_logs.halfday = '5' AND employees_logs.time_out IS NOT NULL THEN 5 
                                WHEN employees_logs.halfday = '6' AND employees_logs.time_out IS NOT NULL THEN 6 
                                WHEN employees_logs.halfday = '7' AND employees_logs.time_out IS NOT NULL THEN 7 
                                ELSE 0 END) 
                        AS no_of_days_converted,
                        SUM(CASE WHEN employees_logs.deleted_at IS NULL AND employees_logs.time_out IS NOT NULL THEN  8
                                ELSE 0 END) 
                        AS no_of_days,
                        SUM(CASE WHEN employees_logs.halfday = '0' THEN employees_logs.undertime
                                 WHEN employees_logs.halfday != '0' THEN 0 
                        ELSE 0 END) AS undertime_converted,
                        undertime AS undertime,
                        late AS late, 
                        SUM(CASE WHEN employees_logs.halfday = '0' AND employees_logs.late >= 60 THEN 0
                                 WHEN employees_logs.deleted_at IS NULL AND employees_logs.late BETWEEN 0 AND 59 THEN employees_logs.late 
                                 WHEN employees_logs.halfday = '1' AND employees_logs.time_out IS NOT NULL THEN employees_logs.late - 420 
                                 WHEN employees_logs.halfday = '2' AND employees_logs.time_out IS NOT NULL THEN employees_logs.late - 360 
                                 WHEN employees_logs.halfday = '3' AND employees_logs.time_out IS NOT NULL THEN employees_logs.late - 300
                                 WHEN employees_logs.halfday = '4' AND employees_logs.time_out IS NOT NULL THEN employees_logs.late - 240 
                                 WHEN employees_logs.halfday = '5' AND employees_logs.time_out IS NOT NULL THEN employees_logs.late - 180 
                                 WHEN employees_logs.halfday = '6' AND employees_logs.time_out IS NOT NULL THEN employees_logs.late - 120
                                 WHEN employees_logs.halfday = '7' AND employees_logs.time_out IS NOT NULL THEN employees_logs.late - 60 
                        ELSE 0 END) 
                        AS late_converted,
                        branch_id
                    FROM (SELECT *, MIN(ee.time_in) FROM employees_logs AS ee 
                        WHERE ee.deleted_at IS NULL AND time_in BETWEEN '$start' AND '$end' GROUP BY employee_id , CAST(time_in AS DATE)
                    ) AS employees_logs GROUP BY id , employee_id) AS employees_logs
                        JOIN employee ON employees_logs.employee_id = employee.id
                        JOIN employee_company ON employees_logs.employee_id = employee_company.employee_id
                        JOIN company ON employee_company.company_id = company.id
                        JOIN employee_rate ON employees_logs.employee_id = employee_rate.employee_id
                        LEFT JOIN
                    (SELECT * FROM employee_rate_log 
                        INNER JOIN (SELECT MAX(created_at) AS date FROM employee_rate_log) AS maxDate ON employee_rate_log.created_at = maxDate.date
                     WHERE created_at BETWEEN '$start' AND '$end') AS erl ON erl.employee_id = employees_logs.employee_id LEFT JOIN
                    (SELECT DISTINCT IF(employee.e_cola IS NULL, company.cola_amount, employee.e_cola) AS cola, employee.id, employee.salary_receiving_type AS slr
                    FROM employees_logs
                    JOIN employee_company ON employees_logs.employee_id = employee_company.employee_id
                    JOIN company ON employee_company.company_id = company.id
                    JOIN employee ON employees_logs.employee_id = employee.id) AS emp ON emp.id = employees_logs.employee_id
                    LEFT JOIN (SELECT * FROM  employee_logs2  WHERE start_date = '$start' AND end_date = '$end') AS attendance_default ON employees_logs.employee_id = attendance_default.employee_id
                GROUP BY employees_logs.employee_id , employee.e_cola , company.cola_amount     
            ) AS payroll"), 'employees_logs.employee_id', '=', 'payroll.emp_code')

            // GET THE OVERTIME ATTENDANCE
            // SAMPLE DATA
            //  ID       ordinary_ot     restday_ot      special_ot
            //  1             5               0               2
            //  2             0               5               1
            ->leftJoin(DB::raw("(   
                    SELECT DISTINCT logs.employee_id AS id,
                        ROUND(IF(company.attendance_default = 0, attendance_default.overtime , IF(company.overtime_rule != '0', SUM(CASE  WHEN logs.overtime = '1' AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 0 THEN  (total_hours) ELSE 0 END),  0)) ,2) 
                    AS ordinary_ot,

                    IF(company.overtime_rule != '0',
                        ROUND(SUM(CASE WHEN logs.overtime = '1'  AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 1
                            THEN (total_hours) ELSE 0 END), 0) ,2) 
                    AS restday_ot,

                    IF(company.overtime_rule != '0',
                        ROUND(SUM(CASE WHEN logs.overtime = '1'  AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 2 THEN (total_hours) ELSE 0 END), 0) ,2)
                    AS special_ot,

                    IF(company.overtime_rule != '0',
                        ROUND(SUM(CASE WHEN logs.overtime = '1' AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 3 THEN (total_hours) ELSE 0  END), 0) ,2) 
                    AS special_rest_ot,

                    IF(company.overtime_rule != '0',
                        SUM(CASE WHEN logs.overtime = '1' AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 4 THEN (total_hours) ELSE 0 END), 0) 
                    AS legal_ot,

                    IF(company.overtime_rule != '0', SUM(CASE WHEN logs.overtime = '1' AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 5 THEN (total_hours) ELSE 0 END), 0) 
                    AS legal_rest_ot,

                    IF(company.overtime_rule != '0', SUM(CASE WHEN logs.overtime = '1' AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 6 THEN (total_hours) ELSE 0 END), 0) 
                    AS double_ot,

                    IF(company.overtime_rule != '0', SUM(CASE WHEN logs.overtime = '1' AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 7 THEN (total_hours) ELSE 0 END), 0) 
                    AS double_rest_ot,

                    ROUND((((((employee_rate.daily_rate + IF(company.overtime_allowance = 1, employee_rate.allowance_amount + IF(employee.e_cola IS NULL, company.cola_amount, employee.e_cola), 0)) / 8) * IF(company.overtime_rule = '2', ot_type.ot1, 1)) * IF(company.attendance_default = 0, attendance_default.overtime, IF(company.overtime_rule != '0', SUM(CASE WHEN logs.overtime = '1'  AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 0 THEN (total_hours) ELSE 0 END), 0)))), 2) 
                    AS pay_ordinary_ot,

                    ROUND((((((employee_rate.daily_rate + IF(company.overtime_allowance = 1, employee_rate.allowance_amount, 0) + IF(employee.e_cola IS NULL, company.cola_amount, employee.e_cola)) / 8) * IF(company.overtime_rule = 2, ot_type.ot2, 1)) * IF(company.overtime_rule != '0', SUM(CASE WHEN logs.overtime = '1' AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 1 THEN (total_hours) ELSE 0 END), 0))), 2) 
                    AS pay_restday_ot,

                    ROUND((((((employee_rate.daily_rate + IF(company.overtime_allowance = 1, employee_rate.allowance_amount, 0) + IF(employee.e_cola IS NULL, company.cola_amount, employee.e_cola)) / 8) * IF(company.overtime_rule = '2', ot_type.ot3, 1)) * IF(company.overtime_rule != '0', SUM(CASE WHEN logs.overtime = '1' AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 2 THEN (total_hours) ELSE 0  END), 0))), 2) 
                    AS pay_special_ot,

                    ROUND((((((employee_rate.daily_rate + IF(company.overtime_allowance = 1,  employee_rate.allowance_amount, 0) + IF(employee.e_cola IS NULL, company.cola_amount,  employee.e_cola)) / 8) * IF(company.overtime_rule = '2', ot_type.ot4, 1)) * IF(company.overtime_rule != '0', SUM(CASE WHEN logs.overtime = '1' AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 3 THEN (total_hours) ELSE 0 END), 0))), 2) 
                    AS pay_special_rest_ot,

                    ROUND((((((employee_rate.daily_rate + IF(company.overtime_allowance = 1, employee_rate.allowance_amount, 0) + IF(employee.e_cola IS NULL, company.cola_amount, employee.e_cola)) / 8) * IF(company.overtime_rule = '2', ot_type.ot5, 1)) * IF(company.overtime_rule != '0', SUM(CASE WHEN logs.overtime = '1' AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 4 THEN (total_hours) ELSE 0 END), 0))), 2) 
                    AS pay_legal_ot,

                    ROUND((((((employee_rate.daily_rate + IF(company.overtime_allowance = 1, employee_rate.allowance_amount, 0) + IF(employee.e_cola IS NULL, company.cola_amount, employee.e_cola)) / 8) * IF(company.overtime_rule = '2', ot_type.ot6, 1)) * IF(company.overtime_rule != '0', SUM(CASE WHEN logs.overtime = '1' AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 5 THEN (total_hours) ELSE 0 END), 0))), 2) 
                    AS pay_legal_rest_ot,

                    ROUND((((((employee_rate.daily_rate + IF(company.overtime_allowance = 1, employee_rate.allowance_amount, 0) + IF(employee.e_cola IS NULL, company.cola_amount, employee.e_cola)) / 8) * IF(company.overtime_rule = '2', ot_type.ot7, 1)) * IF(company.overtime_rule != '0', SUM(CASE WHEN logs.overtime = '1' AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 6 THEN (total_hours) ELSE 0 END), 0))), 2) 
                    AS pay_double_ot,

                    ROUND((((((employee_rate.daily_rate + IF(company.overtime_allowance = 1, employee_rate.allowance_amount, 0) + IF(employee.e_cola IS NULL, company.cola_amount, employee.e_cola)) / 8) * IF(company.overtime_rule = '2', ot_type.ot8, 1)) * IF(company.overtime_rule != '0', SUM(CASE WHEN logs.overtime = '1' AND overtime.date = CAST(logs.time_in AS DATE) AND overtime.status_id = 4 AND logs.holiday_id = 7 THEN (total_hours) ELSE 0 END), 0))), 2) 
                    AS pay_double_rest_ot

                FROM (SELECT *, MIN(ee.time_in) FROM employees_logs AS ee WHERE ee.deleted_at IS NULL  AND time_in BETWEEN '$start' AND '$end'
                        GROUP BY employee_id , CAST(time_in AS DATE)) AS logs
                            LEFT JOIN overtime ON logs.employee_id = overtime.emp_id
                            JOIN employee_rate ON logs.employee_id = employee_rate.employee_id
                            JOIN employee_company ON logs.employee_id = employee_company.employee_id
                            JOIN company ON employee_company.company_id = company.id
                            JOIN employee ON logs.employee_id = employee.id
                            LEFT JOIN (SELECT * FROM  employee_logs2  WHERE start_date = '$start' AND end_date = '$end') AS attendance_default ON logs.employee_id = attendance_default.employee_id
                            CROSS JOIN 
                            (SELECT SUM(CASE WHEN ot.id = '1' THEN ot.computation ELSE 0 END) AS ot1,
                                    SUM(CASE WHEN ot.id = '2' THEN ot.computation ELSE 0 END) AS ot2,
                                    SUM(CASE WHEN ot.id = '3' THEN ot.computation ELSE 0 END) AS ot3,
                                    SUM(CASE WHEN ot.id = '4' THEN ot.computation ELSE 0 END) AS ot4,
                                    SUM(CASE WHEN ot.id = '5' THEN ot.computation ELSE 0 END) AS ot5,
                                    SUM(CASE WHEN ot.id = '6' THEN ot.computation ELSE 0 END) AS ot6,
                                    SUM(CASE WHEN ot.id = '7' THEN ot.computation ELSE 0 END) AS ot7,
                                    SUM(CASE WHEN ot.id = '8' THEN ot.computation ELSE 0 END) AS ot8
                            FROM overtime_type AS ot) AS ot_type GROUP BY id
            ) AS ot"), 'employees_logs.employee_id', '=', 'ot.id')
            // GET THE HOLIDAYS ATTENDANCE
            // SAMPLE DATA
            //  ID       restday     special_hol      special_rest_hol
            //  1           1            0                   2
            //  2           0            5                   1
            ->leftJoin(DB::raw("(
               SELECT logs.employee_id AS emp_id, 
               IF(company.attendance_default = 0, emp_log2.restday, IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '1' THEN 1 ELSE 0  END), 0)) AS restday,
                IF(company.attendance_default = 0, emp_log2.special ,IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '2' THEN 1 ELSE 0 END), 0)) AS special_hol,
                IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '3' THEN 1 ELSE 0 END), 0) AS special_rest_hol,
                IF(company.attendance_default = 0, emp_log2.regular, IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '4' THEN 1 ELSE 0 END), 0)) AS legal_hol,
                IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '5' THEN 1 ELSE 0 END), 0) AS legal_rest_hol,
                IF(company.attendance_default = 0, emp_log2.double_hol, IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '6' THEN 1 ELSE 0 END), 0)) AS double_hol,
                IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '7' THEN 1 ELSE 0 END), 0) AS double_rest_hol,
                ROUND((daily_rate + IF(company.holiday_allowance = 1, allowance_amount, 0)) * hol_type.ht1 * IF(company.attendance_default = 0, (emp_log2.restday / 8) / 60 ,IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '1' THEN 1 ELSE 0 END), 0)), 2) AS pay_restday,
                ROUND((daily_rate) * hol_type.ht2 * IF(company.attendance_default = 0, emp_log2.special, IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '2' THEN 1 ELSE 0 END), 0)), 2) 
                AS pay_special_hol,
                ROUND((daily_rate) * hol_type.ht3 * IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '3' THEN 1 ELSE 0 END), 0), 2) AS pay_special_rest_hol,
                ROUND((daily_rate + IF(company.holiday_allowance = 1, allowance_amount, 0) + IF(employee.e_cola IS NULL, company.cola_amount, employee.e_cola)) * hol_type.ht4 * IF(company.attendance_default = 0, emp_log2.regular, IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '4' THEN 1 ELSE 0 END), 0)), 2) 
                AS pay_legal_hol,
                ROUND((daily_rate + IF(company.holiday_allowance = 1, allowance_amount, 0) + IF(employee.e_cola IS NULL, company.cola_amount, employee.e_cola)) * hol_type.ht5 * IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '5' THEN 1 ELSE 0 END), 0), 2) 
                AS pay_legal_rest_hol,
                ROUND((daily_rate + IF(company.holiday_allowance = 1, allowance_amount, 0) + IF(employee.e_cola IS NULL, company.cola_amount, employee.e_cola)) * hol_type.ht6 * IF(company.attendance_default = 0, emp_log2.double_hol, IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '6' THEN 1 ELSE 0 END), 0)), 2) 
                AS pay_double_hol,
                ROUND((daily_rate + IF(company.holiday_allowance = 1, allowance_amount, 0) + IF(employee.e_cola IS NULL, company.cola_amount, employee.e_cola)) * hol_type.ht7 * IF(company.holiday_rule != '0', SUM(CASE WHEN logs.holiday_id = '7' THEN 1 ELSE 0 END), 0), 2) 
                AS pay_double_rest_hol

            FROM (SELECT  *, MIN(ee.time_in) FROM employees_logs AS ee
                WHERE ee.deleted_at IS NULL AND time_in BETWEEN '$start' AND '$end'
                GROUP BY employee_id , CAST(time_in AS DATE)) AS logs
                    JOIN employee_rate ON logs.employee_id = employee_rate.employee_id
                    JOIN employee_company ON logs.employee_id = employee_company.employee_id
                    JOIN company ON employee_company.company_id = company.id
                    JOIN employee ON logs.employee_id = employee.id
                    LEFT JOIN (SELECT employee_id, restday,
                        LENGTH(holiday_id) - LENGTH(REPLACE(holiday_id, '2', '')) AS special,
                        LENGTH(holiday_id) - LENGTH(REPLACE(holiday_id, '4', '')) AS regular,
                        LENGTH(holiday_id) - LENGTH(REPLACE(holiday_id, '6', '')) AS double_hol 
                FROM employee_logs2 WHERE start_date = '$start' AND end_date = '$end') AS emp_log2 ON logs.employee_id = emp_log2.employee_id
                    CROSS JOIN (SELECT SUM(CASE WHEN ht.id = '1' THEN ht.computation ELSE 0 END) AS ht1,
                        SUM(CASE WHEN ht.id = '2' THEN ht.computation ELSE 0 END) AS ht2,
                        SUM(CASE WHEN ht.id = '3' THEN ht.computation ELSE 0 END) AS ht3,
                        SUM(CASE WHEN ht.id = '4' THEN ht.computation ELSE 0 END) AS ht4,
                        SUM(CASE WHEN ht.id = '5' THEN ht.computation ELSE 0 END) AS ht5,
                        SUM(CASE WHEN ht.id = '6' THEN ht.computation ELSE 0 END) AS ht6,
                        SUM(CASE WHEN ht.id = '7' THEN ht.computation ELSE 0 END) AS ht7
                FROM holiday_type AS ht) AS hol_type
            GROUP BY logs.employee_id
            ) AS holidays") ,'employees_logs.employee_id', '=', 'holidays.emp_id')

            // GET THE NIGHT DIFF TOTAL
            // SAMPLE DATA
            // ID       nd_ordinary_ot  nd_restday_ot   nd_special_ot
            // 1               1             0                2
            // 2               0             5                1
            ->leftJoin(DB::raw("(
                SELECT DISTINCT logs.employee_id AS id, 
                    IF(c.attendance_default = 0, ad.night_differential, IF(c.night_shift != 0, SUM(CASE WHEN logs.holiday_id = 0 THEN (logs.night_differential) ELSE 0 END), 0)) AS nd_ordinary,
                    IF(c.attendance_default = 0, 0, IF(c.night_shift != 0, SUM(CASE WHEN logs.holiday_id = 1 THEN (logs.night_differential) ELSE 0 END), 0)) AS nd_restday,
                    IF(c.attendance_default = 0, 0, IF(c.night_shift != 0, SUM(CASE WHEN logs.holiday_id = 2 THEN (logs.night_differential) ELSE 0 END), 0)) AS nd_special,
                    IF(c.attendance_default = 0, 0, IF(c.night_shift != 0, SUM(CASE WHEN logs.holiday_id = 3 THEN (logs.night_differential) ELSE 0 END), 0)) AS nd_special_rest,
                    IF(c.attendance_default = 0, 0, IF(c.night_shift != 0, SUM(CASE WHEN logs.holiday_id = 4 THEN (logs.night_differential) ELSE 0 END), 0)) AS nd_legal,
                    IF(c.attendance_default = 0, 0, IF(c.night_shift != 0, SUM(CASE WHEN logs.holiday_id = 5 THEN (logs.night_differential) ELSE 0 END), 0)) AS nd_legal_rest,
                    IF(c.attendance_default = 0, 0, IF(c.night_shift != 0, SUM(CASE WHEN logs.holiday_id = 6 THEN (logs.night_differential) ELSE 0 END), 0)) AS nd_double,
                    IF(c.attendance_default = 0, 0, IF(c.night_shift != 0, SUM(CASE WHEN logs.holiday_id = 7 THEN (logs.night_differential) ELSE 0  END), 0)) AS nd_double_rest,
                    ROUND((((er.daily_rate) / 8) * 0.10) * IF(c.attendance_default = 0, ad.night_differential, IF(c.night_shift != '0', SUM(CASE WHEN logs.holiday_id = 0 THEN (logs.night_differential)  ELSE 0 END), 0)), 2) AS pay_reg_nd,
                    ROUND((((er.daily_rate) / 8) * 0.10 * IF(c.night_shift = '2', 1.3, 1)) * IF(c.attendance_default = 0, 0, IF(c.night_shift != '0', SUM(CASE WHEN logs.holiday_id = 1 THEN (logs.night_differential) ELSE 0 END), 0)), 2) AS pay_rd_nd,
                    ROUND((((er.daily_rate) / 8) * 0.10 * IF(c.night_shift = '2', 1.3, 1)) * IF(c.attendance_default = 0, 0, IF(c.night_shift != '0', SUM(CASE WHEN logs.holiday_id = 2 THEN (logs.night_differential) ELSE 0 END), 0)), 2) AS pay_special_hol_nd,
                    ROUND((((er.daily_rate) / 8) * 0.10 * IF(c.night_shift = '2', 1.5, 1)) * IF(c.attendance_default = 0, 0, IF(c.night_shift != '0', SUM(CASE WHEN logs.holiday_id = 3 THEN (logs.night_differential) ELSE 0 END), 0)), 2) AS pay_special_hol_rd_nd,
                    ROUND((((er.daily_rate) / 8) * 0.10 * IF(c.night_shift = '2', 2.0, 1)) * IF(c.attendance_default = 0, 0, IF(c.night_shift != '0', SUM(CASE WHEN logs.holiday_id = 4 THEN (logs.night_differential) ELSE 0 END), 0)), 2) AS pay_legal_hol_nd,
                    ROUND((((er.daily_rate) / 8) * 0.10 * IF(c.night_shift = '2', 2.6, 1)) * IF(c.attendance_default = 0, 0, IF(c.night_shift != '0', SUM(CASE WHEN logs.holiday_id = 5 THEN (logs.night_differential) ELSE 0 END), 0)), 2) AS pay_legal_hol_rd_nd,
                    ROUND((((er.daily_rate) / 8) * 0.10 * IF(c.night_shift = '2', 3.3, 1)) * IF(c.attendance_default = 0, 0, IF(c.night_shift != '0', SUM(CASE WHEN logs.holiday_id = 6 THEN (logs.night_differential) ELSE 0 END), 0)), 2) AS pay_dbl_hol_nd,
                    ROUND((((er.daily_rate) / 8) * 0.10 * IF(c.night_shift = '2', 3.9, 1)) * IF(c.attendance_default = 0, 0, IF(c.night_shift != '0', SUM(CASE WHEN logs.holiday_id = 7 THEN (logs.night_differential) ELSE 0 END), 0)), 2) AS pay_dbl_hol_rd_nd
                FROM (SELECT *, MIN(ee.time_in) FROM employees_logs AS ee
                    WHERE ee.deleted_at IS NULL AND time_in BETWEEN '$start' AND '$end'
                    GROUP BY employee_id , CAST(time_in AS DATE)) AS logs
                        JOIN employee ON logs.employee_id = employee.id
                        JOIN employee_rate AS er ON logs.employee_id = er.employee_id
                        JOIN employee_company ON logs.employee_id = employee_company.employee_id
                        JOIN company ON employee_company.company_id = company.id
                        JOIN company AS c ON employee_company.company_id = c.id
                        LEFT JOIN (SELECT * FROM employee_logs2
                    WHERE start_date = '$start' AND end_date = '$end') AS ad ON logs.employee_id = ad.employee_id
                GROUP BY id
            ) AS nd"), 'employees_logs.employee_id', '=', 'nd.id')

            // ADJUSTMENTS AND DEDUCTION
            ->leftJoin(DB::raw("(
                SELECT adjustment.employee_id AS id,
                    SUM(IF((adjustment_amount) IS NOT NULL AND adjustment_type.type = 'ADJUSTMENT', (CASE WHEN adjustment_type.frequency = 1 THEN adjustment.adjustment_amount * employees_logs.no_of_days ELSE (adjustment.adjustment_amount) END), 0)) AS adjustment,
                    SUM(IF((adjustment_amount) IS NOT NULL AND adjustment_type.type = 'DEDUCTION', (CASE WHEN adjustment_type.frequency = 1 THEN adjustment.adjustment_amount * employees_logs.no_of_days  ELSE (adjustment.adjustment_amount) END), 0)) AS deduction,
                    SUM(IF((adjustment_amount) IS NOT NULL AND adjustment_type.type = 'ADJUSTMENT' AND adjustment.billable = 1, (CASE WHEN adjustment_type.frequency = 1 THEN adjustment.adjustment_amount * employees_logs.no_of_days ELSE (adjustment.adjustment_amount) END), 0)) AS bill_adjustment,
                    SUM(IF((adjustment_amount) IS NOT NULL AND adjustment_type.type = 'DEDUCTION' AND adjustment.billable = 1, (CASE WHEN adjustment_type.frequency = 1 THEN adjustment.adjustment_amount * employees_logs.no_of_days  ELSE (adjustment.adjustment_amount) END), 0)) AS bill_deduction
                FROM
                    (SELECT DISTINCT ROUND(IF(company.attendance_default = 0, attendance_default.number_of_days,
                        IF(company.undertime_rule = '1', ROUND(SUM(employees_logs.no_of_days_undertime) / 8, 2), IF(company.late_convertion = 1, SUM(employees_logs.no_of_days_converted), SUM(employees_logs.no_of_days)) / 8)
                    ), 2) AS no_of_days, employees_logs.employee_id FROM (SELECT employee_id AS employee_id, time_in AS time_in, halfday AS halfday, deleted_at AS deleted_at,  
                        SUM(CASE WHEN employees_logs.halfday = '0' AND employees_logs.time_out IS NOT NULL AND employees_logs.late < 60 THEN (480 - employees_logs.late) / 60
                                WHEN employees_logs.halfday = '1' AND employees_logs.time_out IS NOT NULL THEN 1 
                                WHEN employees_logs.halfday = '2' AND employees_logs.time_out IS NOT NULL THEN 2 
                                WHEN employees_logs.halfday = '3' AND employees_logs.time_out IS NOT NULL THEN 3 
                                WHEN employees_logs.halfday = '4' AND employees_logs.time_out IS NOT NULL THEN 4 
                                WHEN employees_logs.halfday = '5' AND employees_logs.time_out IS NOT NULL THEN 5 
                                WHEN employees_logs.halfday = '6' AND employees_logs.time_out IS NOT NULL THEN 6 
                                WHEN employees_logs.halfday = '7' AND employees_logs.time_out IS NOT NULL THEN 7 
                                ELSE 0 END) 
                        AS no_of_days_undertime,  
                        SUM(CASE WHEN employees_logs.halfday = '0' AND employees_logs.time_out IS NOT NULL AND employees_logs.late < 60 THEN 8
                                WHEN employees_logs.halfday = '0' AND employees_logs.late >= 60 THEN (480 - employees_logs.late) / 60
                                WHEN employees_logs.halfday = '1' AND employees_logs.time_out IS NOT NULL THEN 1 
                                WHEN employees_logs.halfday = '2' AND employees_logs.time_out IS NOT NULL THEN 2 
                                WHEN employees_logs.halfday = '3' AND employees_logs.time_out IS NOT NULL THEN 3 
                                WHEN employees_logs.halfday = '4' AND employees_logs.time_out IS NOT NULL THEN 4 
                                WHEN employees_logs.halfday = '5' AND employees_logs.time_out IS NOT NULL THEN 5 
                                WHEN employees_logs.halfday = '6' AND employees_logs.time_out IS NOT NULL THEN 6 
                                WHEN employees_logs.halfday = '7' AND employees_logs.time_out IS NOT NULL THEN 7 
                                ELSE 0 END) 
                        AS no_of_days_converted,
                        SUM(CASE WHEN employees_logs.deleted_at IS NULL AND employees_logs.time_out IS NOT NULL THEN 8 ELSE 0 END) AS no_of_days
                    FROM (SELECT *, MIN(ee.time_in) FROM employees_logs AS ee WHERE ee.deleted_at is NULL AND time_in BETWEEN '$start' AND '$end' GROUP BY employee_id , CAST(time_in AS DATE)) AS employees_logs
                    GROUP BY id , employee_id) AS employees_logs
                    LEFT JOIN (SELECT * FROM  employee_logs2  WHERE start_date = '$start' AND end_date = '$end') AS attendance_default ON employees_logs.employee_id = attendance_default.employee_id
                    JOIN employee ON employees_logs.employee_id = employee.id
                    JOIN employee_company ON employees_logs.employee_id = employee_company.employee_id
                    JOIN company ON employee_company.company_id = company.id
                    JOIN employee_rate ON employees_logs.employee_id = employee_rate.employee_id
                        GROUP BY employees_logs.employee_id , employee.e_cola , company.cola_amount) AS employees_logs
                    JOIN adjustment ON employees_logs.employee_id = adjustment.employee_id
                    JOIN adjustment_type ON adjustment.adjustment_type = adjustment_type.id

                WHERE adjustment.deleted_at IS NULL
                        AND IF(adjustment.type = 'ONCE',
                        adjustment.date_file BETWEEN '$start' AND '$end',
                        adjustment.date_file IS NOT NULL)
                GROUP BY id
            ) AS adjustment"),'employees_logs.employee_id', '=', 'adjustment.id')
            
            // LOANS
            ->leftJoin(DB::raw("(
                SELECT loan.id, SUM(loan.loan) as loan
                FROM  (SELECT DISTINCT loan.employee_id AS id, SUM(loan_amount) AS loan, loan.type FROM (SELECT DISTINCT employee_id FROM employees_logs
                    WHERE time_in BETWEEN '$start' AND '$end') AS employee_logs LEFT JOIN loan ON employee_logs.employee_id = loan.employee_id
                    WHERE loan.deleted_at IS NULL AND IF(loan.type = 'ONCE' OR loan.current_amount = '0', loan.date_file BETWEEN '$start' AND '$end', loan.date_file IS NOT NULL)
                    GROUP BY id) AS loan
                GROUP BY id
            ) AS loan"),'employees_logs.employee_id', '=', 'loan.id')

            // LAST PAYROLL NUMBER OF DAYS
            ->leftJoin(DB::raw("(
                SELECT DISTINCT employee_logs.employee_id AS id,
                    employee_company.company_id AS company,
                    SUM(CASE WHEN cfg.tax_term = 'MONTHLY' AND cfg.code = 'SSS' THEN pl.no_of_days ELSE 0 END) AS sss_days,
                    SUM(CASE WHEN cfg.tax_term = 'MONTHLY' AND cfg.code = 'PAGIBIG' THEN pl.no_of_days ELSE 0 END) AS pag_days,
                    SUM(CASE WHEN cfg.tax_term = 'MONTHLY' AND cfg.code = 'PHILHEALT' THEN pl.no_of_days ELSE 0 END) AS phi_days,
                    (CASE WHEN cfg.tax_term = 'MONTHLY' THEN pl.ttl_basic_pay ELSE 0  END) AS last_basic_pay
                FROM (SELECT DISTINCT employee_id FROM employees_logs) AS employee_logs
                        JOIN employee_company ON employee_company.employee_id = employee_logs.employee_id
                        JOIN payroll_list AS pl ON employee_logs.employee_id = pl.emp_id
                        JOIN company_government_field AS cfg
                        JOIN company ON employee_company.company_id = company.id
                        INNER JOIN (SELECT emp_id AS eid, payroll_id AS pid, MAX(created_at) AS MaxDate FROM payroll_list GROUP BY eid) AS tm ON pl.created_at = tm.MaxDate
                        AND pl.emp_id = tm.eid
                WHERE pl.deleted_at IS NULL
                GROUP BY employee_logs.employee_id
            ) AS past"), function ($join) {
                $join->on('employees_logs.employee_id', '=', 'past.id');
                $join->on('employee_company.company_id', '=', 'past.company');
            })
            ->leftJoin(DB::raw("(
                SELECT * FROM company_government_field WHERE company_government_field.code = 'SSS' 
            ) AS g_sss") , 'company.id', '=', 'g_sss.company_id')
            ->leftJoin(DB::raw("(
                SELECT * FROM company_government_field WHERE company_government_field.code = 'PAGIBIG'
            ) AS g_pag") , 'company.id', '=', 'g_pag.company_id')
            ->leftJoin(DB::raw("(
                SELECT * FROM company_government_field WHERE company_government_field.code = 'PHILHEALTH'
            ) AS g_phi") , 'company.id', '=', 'g_phi.company_id')
            ->leftJoin('billing_rate', 'employee_rate.daily_rate', '=', 'billing_rate.salary');
        
    }

     public function postList($model, $start, $end, $run_type, $company, $branch, $department, $position, $cutoff, $removed_employee, $branch_location) {
            $payroll = $model->select(
            DB::raw('DISTINCT employees_logs.employee_id AS emp_id'),
            'employee.firstname',
            'employee.lastname',
            'employee_rate.daily_rate AS daily_rate',
            'payroll.branch',
            'payroll.e_cola AS cola',
            'payroll.no_of_days AS no_of_days',
            'payroll.ttl_basic_pay AS ttl_basic_pay',
            'payroll.allowance AS allowance',
            'payroll.total_allowance AS total_allowance',
            
            // total_ecola = cola * no_of_days
            'payroll.cola AS ecola',
            // 13th month = daily_rate / 12months * no_of_days
            'payroll.thirteen_month AS thirteen_month',
            // late 
            'payroll.emp_late AS total_late',
            // (daily_rate / 8 * total_late) / 60 
            'payroll.total_emp_late AS late_amount',
             // undertime
            'payroll.emp_undertime AS total_undertime',
            // (daily_rate / 8 * total_undertime) / 60 
            'payroll.total_undertime AS undertime_amount',  

            'company.undertime_rule',

            // regular OT                   
            'ot.ordinary_ot AS reg_ot',
            // regulart OT = ((daily_rate / 8) * ot_computation ) * reg_ot
            'ot.pay_ordinary_ot AS pay_reg_ot',

            // regular restday
            'holidays.restday AS restday',
            // regulart resday = (daily_rate * holiday.computation * restday)
            'holidays.pay_restday AS pay_restday',

            // restday OT       
            'ot.restday_ot AS rd_ot',
            // restdayt OT = ((daily_rate / 8) * ot_computation ) * reg_ot
            'ot.pay_restday_ot AS pay_rd_ot',

            // special_hol
            'holidays.special_hol AS special_hol',
            // special_hol = (daily_rate * holiday.computation * special_hol)
            'holidays.pay_special_hol AS pay_special_hol',

            // special holiday OT       
            'ot.special_ot AS special_hol_ot',
            // special holiday  OT = ((daily_rate / 8) * ot_computation ) * special_hol_ot
            'ot.pay_special_ot AS pay_special_hol_ot',

            // special_hol_rd
            'holidays.special_rest_hol AS special_hol_rd',
            // special_hol_rd = (daily_rate * holiday.computation * special_rest_hol)
            'holidays.pay_special_rest_hol AS pay_special_hol_rd',

            // special holiday rd OT       
            'ot.special_rest_ot AS special_hol_rd_ot',
            // special holiday rd OT  = ((daily_rate / 8) * ot_computation ) * special_rest_ot
            'ot.pay_special_rest_ot AS pay_special_hol_rd_ot',

            // legal/regular holiday
            'holidays.legal_hol AS legal_hol',
            //  legal/regular holiday = (daily_rate * holiday.computation * legal_hol)
            'holidays.pay_legal_hol AS pay_legal_hol',

            // legal/regular holiday OT       
            'ot.legal_ot AS legal_hol_ot',
            // legal/regular holiday OT  = ((daily_rate / 8) * ot_computation ) * legal_ot
            'ot.pay_legal_ot AS pay_legal_hol_ot',

            // legal/regular rd holiday
            'holidays.legal_rest_hol AS legal_hol_restday',
            //  legal/regular rd holiday = (daily_rate * holiday.computation * legal_rest_hol)
            'holidays.pay_legal_rest_hol AS pay_legal_hol_restday',

            // legal/regular holiday rd OT       
            'ot.legal_rest_ot AS legal_hol_rd_ot',
            // legal/regular holiday rd OT  = ((daily_rate / 8) * ot_computation ) * legal_rest_ot
            'ot.pay_legal_rest_ot AS pay_legal_hol_rd_ot',

            // double holiday 
            'holidays.double_hol AS double_hol',
            // double holiday  = (daily_rate * holiday.computation * double_hol)
            'holidays.pay_double_hol AS pay_double_hol',

            // on double holiday overtime      
            'ot.double_ot AS dbl_hol_ot',
            // on double holiday overtime  = ((daily_rate / 8) * ot_computation ) * double_ot
            'ot.pay_double_ot AS pay_dbl_hol_ot',

            // double holiday + restday
            'holidays.double_rest_hol AS double_hol_rd',
            // double holiday + restday  = (daily_rate * holiday.computation * double_rest_ot)
            'holidays.pay_double_rest_hol AS pay_double_hol_rd',

            // on double holiday rd overtime      
            'ot.double_rest_ot AS dbl_hol_rd_ot',
            // on double holiday rd overtime  = ((daily_rate / 8) * ot_computation ) * double_rest_ot
            'ot.pay_double_rest_ot AS pay_dbl_hol_rd_ot',

            // nd_ordinary
            'nd.nd_ordinary AS reg_nd',
            'nd.pay_reg_nd AS pay_reg_nd',
            
            // nd_restday
            'nd.nd_restday AS rd_nd',
            'nd.pay_rd_nd AS pay_rd_nd',

            // nd_special
            'nd.nd_special AS special_hol_nd',
            'nd.pay_special_hol_nd AS pay_special_hol_nd',

            // nd_special_rest
            'nd.nd_special_rest AS special_hol_rd_nd',
            'nd.pay_special_hol_rd_nd AS pay_special_hol_rd_nd',

            // nd_legal
            'nd.nd_legal AS legal_hol_nd',
            'nd.pay_legal_hol_nd AS pay_legal_hol_nd',

            // nd_legal_rest
            'nd.nd_legal_rest AS legal_hol_rd_nd',
            'nd.pay_legal_hol_rd_nd AS pay_legal_hol_rd_nd',

            // nd.nd_double
            'nd.nd_double AS dbl_hol_nd',
            'nd.pay_dbl_hol_nd AS pay_dbl_hol_nd',

            // nd_double_rest
            'nd.nd_double_rest AS dbl_hol_rd_nd',
            'nd.pay_dbl_hol_rd_nd AS pay_dbl_hol_rd_nd',

            // adjustment
            DB::raw("(IFNULL(adjustment.adjustment, 0)) AS total_adjustment"),
            // deduction
            DB::raw("(IFNULL(adjustment.deduction, 0)) AS total_deduction"),
            // loan
            DB::raw("(IFNULL(loan.loan,0)) AS total_loan"),

            // sss
            DB::raw("( SELECT IF(g_sss.default_value IS NULL OR g_sss.default_value = 0, ROUND((sss.ee) / 26 * payroll.no_of_days, 2), IF(g_sss.is_default = '1', g_sss.default_value, ROUND((g_sss.default_value / 26) * (no_of_days), 2)) ) FROM  sss  WHERE (daily_rate * 26) BETWEEN sss.salary_base AND sss.salary_ceiling
            ) AS zsss"),
            DB::raw("( SELECT IF(g_sss.daily_min_days < no_of_days, (IF(g_sss.tax_term = 'MONTHLY', IF('$cutoff' = 0, IF(g_sss.first_cutoff = 1, zsss, 0), IF(g_sss.second_cutoff = 1, zsss, 0)),  zsss)),0)) as total_sss"),

            // philhealth
            DB::raw("( SELECT IF(g_phi.default_value IS NULL OR g_phi.default_value = 0, ROUND(daily_rate * 26 * 0.0275 / 2 / 26 * payroll.no_of_days, 2), IF(g_phi.is_default = '1', g_phi.default_value, ROUND((g_phi.default_value / (IF(g_phi.is_default = '0', 26, 1))) * (payroll.no_of_days), 2)))
            ) AS zphil"),
            DB::raw("(  SELECT IF(g_phi.daily_min_days < no_of_days, (IF(g_phi.tax_term = 'MONTHLY', IF('$cutoff' = 0, IF(g_phi.first_cutoff = 1, zphil, 0),  IF(g_phi.second_cutoff = 1, zphil, 0)), zphil)), 0)
            ) as total_philhealth"),

            // pagibig
            DB::raw("( SELECT IF(g_pag.default_value IS NULL OR g_pag.is_default = 0, IF(ttl_basic_pay < 1500, ROUND((IF(ttl_basic_pay > 5000, 5000, ttl_basic_pay) * .01), 2), ROUND((IF(ttl_basic_pay > 5000, 5000, ttl_basic_pay) * .02), 2)), IF(g_pag.is_default = 1, g_pag.default_value, ROUND((g_pag.default_value / (IF(g_pag.is_default = '0', 26, 1))) * (no_of_days),  2)))
            ) AS zpag"),
            DB::raw("( SELECT  IF(g_phi.daily_min_days < no_of_days, (IF(g_phi.tax_term = 'MONTHLY', IF('$cutoff' = 0, IF(g_phi.first_cutoff = 1, zpag, 0), IF(g_phi.second_cutoff = 1, zpag, 0)), zpag)), 0)
            ) as total_pagibig"),
            // gross pay
            DB::raw("( SELECT ROUND((ttl_basic_pay + total_allowance + IF(company.undertime_rule = '1', undertime_amount, undertime_amount * - 1) - late_amount + ecola + pay_reg_ot + pay_restday + pay_rd_ot + pay_special_hol + pay_special_hol_ot + pay_special_hol_rd + pay_special_hol_rd_ot + pay_legal_hol + pay_legal_hol_ot + pay_legal_hol_restday + pay_legal_hol_rd_ot + pay_double_hol + pay_dbl_hol_ot + pay_double_hol_rd + pay_dbl_hol_rd_ot + IF(company.holiday_rule = 3, 0, pay_reg_nd) + IF(company.holiday_rule = 3, 0, pay_rd_nd) + IF(company.holiday_rule = 3, 0, pay_special_hol_nd) + IF(company.holiday_rule = 3, 0, pay_special_hol_rd_nd) + IF(company.holiday_rule = 3, 0, pay_legal_hol_nd) + IF(company.holiday_rule = 3, 0, pay_legal_hol_rd_nd) + IF(company.holiday_rule = 3, 0, pay_dbl_hol_nd) + IF(company.holiday_rule = 3, 0, pay_dbl_hol_rd_nd) + total_adjustment + total_deduction + total_loan), 2)
            )  AS gross_pay" ),
            // net pay
            DB::raw("(SELECT (gross_pay - total_sss - total_pagibig - total_philhealth )) AS net_pay"), 
            // sss employer
            DB::raw("( SELECT  ROUND((sss.er / 26) * no_of_days, 2) FROM sss WHERE (daily_rate * 26) BETWEEN sss.salary_base AND sss.salary_ceiling ) AS total_sss_employer"),
            // pagibig employer
            DB::raw("(SELECT  total_pagibig) AS total_pagibig_employer"),
            // philhealth employer 
            DB::raw("(SELECT  total_philhealth) AS total_philhealth_employer"),
            DB::raw("(SELECT (total_sss + total_pagibig + total_philhealth + total_sss_employer + total_pagibig_employer + total_philhealth_employer) ) AS total_mandatory"),

            DB::raw("(SELECT gross_pay + total_sss_employer + total_sss + total_philhealth_employer + total_philhealth + total_pagibig_employer + total_pagibig ) AS total_billable")
           
            
        )
        ->whereBetween('employees_logs.time_in',  [$start, $end])
        ->where('employee.status', 1) 
        ->where('employees_logs.branch_id', $branch_location)
        ->whereNull('employees_logs.deleted_at');

        
        // run_type = company
        if ($run_type == 1) {
            $payroll->where('company.id', '=' , $company);       
            if (is_numeric($branch)) {
                $payroll->where('employee.branch', '=' , $branch); 
            }      
        // run_type = department
        } elseif ($run_type == 2) {
            $payroll->where('company.id', '=' , $company);       
            if (is_numeric($branch)) {
                $payroll->where('employee.branch', '=' , $branch); 
            }   
            $payroll->where('employee.department', '=' , $department);       
        // run_type = position                   
        } elseif ($run_type == 3) {
            $payroll->where('company.id', '=' , $company);   
            if (is_numeric($branch)) {
                $payroll->where('employee.branch', '=' , $branch); 
            }   
            
            $payroll->where('employee.position', '=' , $position);      
        }
        // check if the removed_employee is undefined
        if ($removed_employee != 'undefined' || $removed_employee != null) {
            if ( strpos($removed_employee, ',') !== false ) {
                $employee_ids = explode(',', $removed_employee);
            } else {
                $employee_ids[] = $removed_employee;
            }
            // search if with comma eg. 2,1,3,5
            $payroll->whereNotIn('employees_logs.employee_id', $employee_ids);
        } 
        return $payroll;
    }
}




                