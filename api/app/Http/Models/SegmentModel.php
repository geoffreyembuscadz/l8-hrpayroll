<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Http\Request;

class SegmentModel extends Model
{
    protected $table = "segment"; use SoftDeletes;

    public static function showSegment(){
    	$query = DB::table('segment')
    			->select('id', 'name', 'description')
    			->where('deleted_by', '=', NULL)
    			->get();
    	return $query;
    } 

    public static function getSegment(){
    	$query = DB::table('segment')
    			->select('id', 'name As text')
    			->where('deleted_by', '=', NULL)
    			->get();
    	return $query;
    } 
}
