<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\ItemModel;
use Carbon\Carbon;
use App\Http\Controllers\UserController;

class TaskModel extends Model
{
    protected $table = "task_list"; use SoftDeletes;

    public static function getUser(){
      $contr = new UserController;
      $data = $contr->getListByIdLimited();

      return $data;
    }

    public static function getUserId($id){
      $contr = new UserController;
      $data = $contr->getUserId($id);

      return $data;
    }

    public function getTask($data){

        $user = $this->getUser();

        $supervisor_id= $user->user_id;
        $role_id = $user->role_id;

    	$employee_id = $data['employee_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];

        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('task_list')
        ->join('employee', 'task_list.employee_id', '=', 'employee.id')
        ->join('status_type', 'task_list.status_id', '=', 'status_type.id')
        ->select(
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('SUBSTRING(task_list.task, 1, 15) as task'),
            'task_list.id', 
            'task_list.task as comp_task', 
            'task_list.deadline', 
            'task_list.employee_id', 
            'task_list.status_id', 
            'task_list.start_task', 
            'task_list.end_task',
            'status_type.name as status')
        ->orderBy('task_list.created_at', 'desc');

        if($role_id != 1 || $role_id != 2) {
            $filter->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
        }

        if( $start != 'null' && $end != 'null'){
            $filter->whereRaw(" CAST(task_list.created_at as date) between '$start_date' and '$end_date'");
        }

        if($employee_id[0] != 'null') {
            $filter->whereIn('task_list.employee_id',$employee_id);
        }
        $data = $filter->get();

        return $data;
    }

    public function duplicateEntryValidation($data){
        $already_exist=[];
        $task = $data['task'];
        $edit = $data['edit'];
        $leng = count($data['employee_id']);

        for ($i=0; $i < $leng ; $i++) { 
            
            $employee_id = $data['employee_id'][$i];
            $filter = self::join('employee', 'task_list.employee_id', '=', 'employee.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    'task_list.id',
                    'task_list.employee_id'
                    )
                ->where('task_list.task', '=' ,$task)
                ->where('task_list.employee_id', '=' ,$employee_id);

                if($edit == true) {
                    $id = $data['id'];
                    $filter->where('task_list.id','!=',$id);
                }

                $validate = $filter->first();

            if(count($validate)==1){
                $already_exist[]= array(
                    'name'   =>  $validate["name"],
                    'id'     =>  $validate["id"],
                    'emp_id' =>  $validate["employee_id"]
                );
            }
        }

        return $already_exist;
    }

    public function createTask($data){

        $notif=[];
        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        foreach ($data as $counter => $d) {

            $OT_id =  DB::table('task_list')->insertGetId(
                    array(
                        'employee_id'    => $d['employee_id'],
                        'task'           => $d['task'],
                        'status_id'      => 5,
                        'deadline'       => $d['deadline'],
                        'created_by'     => $user->user_id,
                        'created_at'     => $date
                        )
                );


           //  $notif_id=DB::table('notification')->insertGetId(
           //      array(
           //          'title'          => 'New Task',
           //          'description'    => $d['task'],
           //          'messenger_id'   => $d[0],
           //          'table_from'     => 'overtime',
           //          'table_from_id'  => $OT_id,
           //          'seen'           => 0,
           //          'route'          => 'overtime-list',
           //          'receiver_id'    => $d[7],
           //          'icon'           => 'fa fa-pencil-square-o text-aqua',
           //          'created_by'     => $d[4],
           //          'created_at'     => $date
           //          )
           //  );
           // $notif[$counter]= $notif_id;
        }

        return $data;
    }

    public function show($id){

        return self::join('employee', 'task_list.employee_id', '=', 'employee.id')
        ->join('status_type', 'task_list.status_id', '=', 'status_type.id')
        ->select(
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            'task_list.id', 
            'task_list.task',
            'task_list.deadline', 
            'task_list.employee_id', 
            'task_list.status_id', 
            'task_list.start_task', 
            'task_list.end_task',
            'status_type.name as status')
        ->where('task_list.id', '=' ,$id)
        ->first();

    }

    public function updateTask($file){
        $d = $file;
        $user = $this->getUser();
        $date = date('Y-m-d H:i:s');
        $id = $d['id'];
        $data = [];

        DB::table('task_list')->where('id',$id)->update([
                    'employee_id'   => $d['employee_id'][0],
                    'deadline'      => $d['deadline'],
                    'task'          => $d['task'],
                    'updated_by'    => $user->user_id,
                    'updated_at'    => $date
        ]);

        return $data;
    }

    public function getMyTask($data){

        $user = $this->getUser();

        $employee_id = $user->employee_id;
        $start = $data['start_date'];
        $end = $data['end_date'];

        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('task_list')
        ->join('employee', 'task_list.employee_id', '=', 'employee.id')
        ->join('status_type', 'task_list.status_id', '=', 'status_type.id')
        ->select(
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('SUBSTRING(task_list.task, 1, 15) as task'),
            'task_list.id', 
            'task_list.task as comp_task',
            'task_list.deadline', 
            'task_list.employee_id', 
            'task_list.status_id', 
            'task_list.start_task', 
            'task_list.end_task',
            'status_type.name as status')
        ->where('task_list.employee_id','=',$employee_id)
        ->orderBy('task_list.created_at', 'desc');

        if( $start != 'null' && $end != 'null'){
            $filter->whereRaw(" CAST(task_list.created_at as date) between '$start_date' and '$end_date'");
        }

        $data = $filter->get();
        return $data;

    }

    public function updateTaskTime($d){

        $user = null;
        $user = $this->getUser();

        $date = date('Y-m-d H:i:s');
        $id = $d['id'];
        $employee_id = $d['employee_id'];
        $data = [];
        $notif_id = [];

        if ($d['type'] == 'start') {
            DB::table('task_list')->where('id',$id)->update([
                    'start_task'    => $d['datetime'],
                    'status_id'     => 6,
                    'updated_by'    => $user->user_id,
                    'updated_at'    => $date
            ]);
        }
        elseif ($d['type'] == 'end') {
            DB::table('task_list')->where('id',$id)->update([
                    'end_task'      => $d['datetime'],
                    'status_id'     => 7,
                    'updated_by'    => $user->user_id,
                    'updated_at'    => $date
            ]);


            if ($user != null) {
                $notif_id[]=DB::table('notification')->insertGetId(
                    array(
                        'title'          => 'Task',
                        'description'    => 'Checking of Task',
                        'messenger_id'   => $user->user_id,
                        'table_from'     => 'task_list',
                        'table_from_id'  => $id,
                        'seen'           => 0,
                        'route'          => 'task-list',
                        'receiver_id'    => $user->supervisor_id,
                        'icon'           => 'fa fa-tasks text-aqua',
                        'created_by'     => $user->user_id,
                        'created_at'     => $date
                        )
                );
            }
        }

        return $notif_id;
    }

    public function updateTaskStatus($d){
        $user = $this->getUser();
        $date = date('Y-m-d H:i:s');

        $id = $d['id'];
        $check_status = $d['status'];
        $status = $d['status'];
        $remarks = $d['remarks'];

        if ($check_status == '1') {
            $status = 6;

            DB::table('task_list')->where('id',$id)->update([
                'end_task'      => null,
                'check_status'  => $check_status,
                'remarks'       => $remarks
            ]);
        }
        else if ($check_status == '2') {
            $status = 8;
            DB::table('task_list')->where('id',$id)->update([
                'check_status'  => $check_status
            ]);
        }

        DB::table('task_list')->where('id',$id)->update([
            'status_id'         => $status,
            'check_by'          => $user->user_id,
            'updated_by'        => $user->user_id,
            'updated_at'        => $date
        ]);

        return $status;
    }

    public function notifyTaskDeadline(){ 

        $date = date('Y-m-d');
        $notif_id = [];

        $data = DB::table('task_list')
        ->join('employee', 'task_list.employee_id', '=', 'employee.id')
        ->join('status_type', 'task_list.status_id', '=', 'status_type.id')
        ->select(
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('SUBSTRING(task_list.task, 1, 15) as task'),
            'task_list.id', 
            'task_list.deadline', 
            'task_list.employee_id', 
            'task_list.created_by', 
            'task_list.status_id', 
            'task_list.start_task', 
            'task_list.end_task',
            'status_type.name as status')
        ->whereRaw("CAST(task_list.deadline as date) = '$date'")
        ->get();


        foreach ($data as $d) {

            $messenger_id = null;
            $messenger_id = $this->getUserId($d->employee_id);

            if ($messenger_id != null) {
                $notif_id[]=DB::table('notification')->insertGetId(
                        array(
                            'title'          => 'Deadline of Task today',
                            'description'    => $d->task,
                            'messenger_id'   => $d->created_by,
                            'table_from'     => 'task_list',
                            'table_from_id'  => $d->id,
                            'seen'           => 0,
                            'route'          => 'my-task-list',
                            'receiver_id'    => $messenger_id->user_id,
                            'icon'           => 'fa fa-tasks text-aqua',
                            'created_by'     => $d->created_by,
                            'created_at'     => $date
                            )
                );
            }
        }


        return $notif_id;
        
    }

    public function getMyTaskForDashboard(){

        $user = $this->getUser();
        $employee_id = $user->employee_id;

        $data = DB::table('task_list')
        ->join('employee', 'task_list.employee_id', '=', 'employee.id')
        ->join('status_type', 'task_list.status_id', '=', 'status_type.id')
        ->select(
            DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'), 
            DB::raw('SUBSTRING(task_list.task, 1, 15) as task'),
            'task_list.id', 
            'task_list.task as comp_task',
            'task_list.deadline', 
            'task_list.employee_id', 
            'task_list.status_id', 
            'task_list.start_task', 
            'task_list.end_task',
            'status_type.name as status')
        ->where('task_list.employee_id','=',$employee_id)
        ->orderBy('task_list.created_at', 'desc')
        ->limit(3)
        ->get();

        return $data;
    }

    public function approvedTask($d){

        $user = $this->getUser();
        $date = date('Y-m-d H:i:s');
        $id = $d['id'];
        $notif_id = [];

        DB::table('task_list')->where('id',$id)->update([
            'status_id'         => 1,
            'updated_by'        => $user->user_id,
            'updated_at'        => $date
        ]);

        $messenger_id = null;
        $messenger_id = $this->getUserId($d['employee_id']);

        if ($messenger_id != null) {
            $notif_id[]=DB::table('notification')->insertGetId(
                    array(
                        'title'          => 'New Task',
                        'description'    => $d['task'],
                        'messenger_id'   => $user->user_id,
                        'table_from'     => 'my-task_list',
                        'table_from_id'  => $id,
                        'seen'           => 0,
                        'route'          => 'my-task-list',
                        'receiver_id'    => $messenger_id->user_id,
                        'icon'           => 'fa fa-tasks text-aqua',
                        'created_by'     => $user->user_id,
                        'created_at'     => $date
                        )
            );
        }

        return $notif_id;
    }
}
