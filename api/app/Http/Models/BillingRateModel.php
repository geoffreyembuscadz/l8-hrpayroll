<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class BillingRateModel extends Model
{
    protected $table = "billing_rate"; use SoftDeletes;
}
