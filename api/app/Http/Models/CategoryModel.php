<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use \Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryModel extends Model
{
    protected $table = "category"; use SoftDeletes;

    public static function categoryList(){
    	$query = DB::table('category As c')
    			->select(
    				DB::raw('c.id'),
                    DB::raw('c.name As name'), 
    				DB::raw('c.is_cat As is_cat'), 
    				DB::raw('c.description'))
    			->where('c.deleted_by', '=', NULL)
    			->get();
    	return $query;
    }

    public static function catName(){
       $query = DB::table(
            DB::raw("(
                SELECT id, 
                name AS text
                FROM category c
            ) As `a`
            ")
        )->get();
        return $query;
    }


}
