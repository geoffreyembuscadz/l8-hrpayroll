<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\AdjustmentModel as Adjustment;
use Carbon\Carbon;
use \DB as DB;

class AdjustmentModel extends Model
{
    protected $table = "adjustment"; use SoftDeletes;

    public function preListJoin($model) {
    	return $model
    		   ->join('adjustment_type', 'adjustment.adjustment_type', '=', 'adjustment_type.id')
    		   ->join('employee', 'adjustment.employee_id', '=', 'employee.id')
    		   ->join('employee_company', 'employee.id', '=', 'employee_company.employee_id')
               ->join('company', 'employee_company.company_id', '=', 'company.id')
    		   ->leftJoin('department', 'employee.department', '=', 'department.id')
    		   ->leftJoin('position', 'employee.position', '=', 'position.id')
               ->join(DB::raw("(SELECT 
                        adjustment_type.id,
                        if(adjustment_type.taxable = 1, 'Yes', 'No' ) as taxable,
                        if(adjustment_type.billable = 1, 'Yes', 'No' ) as billable,
                        if(adjustment_type.frequency = 1, 'Daily', 'Monthly' ) as frequency
                        from adjustment_type) AS adjust_type")

            ,'adjustment.adjustment_type', '=', 'adjust_type.id');
    }
    /**
     * change query for adjustment
     * @param  [type] $model      
     * @param  int $emp_id     
     * @param  datetime $start      
     * @param  datetime $end        
     * @param  int $type       
     * @param  int $no_of_days 
     * @return $adjustment             
     */
    public function postListJoin($model, $emp_id, $start, $end, $type, $no_of_days) {
    	
        if (empty($no_of_days) || $no_of_days == '' || $no_of_days == 'undefined') {
            $no_of_days = 1;
        } 

        $adjustment = $model->select(
            'adjustment.id', 
            'adjustment.adjustment_type', 
            'adjustment.employee_id', 
            'adjustment_type.display_name', 
            'adjust_type.taxable',
            DB::raw("(SELECT if(adjustment.billable = 1, 'Yes', 'No' )) as billable"),
            'adjust_type.frequency',
            'adjustment_type.type as adjustment_type', 
            'employee.lastname', 
            'employee.firstname', 
            DB::raw("(select SUM(CASE WHEN adjustment_type.frequency = 1 THEN adjustment.adjustment_amount * '$no_of_days' ELSE adjustment.adjustment_amount END)) as adjustment_amount"), 
            'company.name AS company', 
            'department.name as department', 
            'position.name as position', 
            'adjustment.type', 
            'adjustment.created_at',
            'adjustment.date_file'

        )
        ->groupBy('adjustment_amount')
        ->groupBy('adjustment.id')
        ->groupBy('adjustment_type.display_name')
        ->groupBy('adjustment.employee_id')
        ->groupBy('adjustment_type.type')
        ->groupBy('adjustment.adjustment_type')
        ->groupBy('adjustment_type.frequency')
        ->groupBy('taxable')
        ->groupBy('billable')
        ->groupBy('lastname')
        ->groupBy('firstname')
        ->groupBy('company.name')
        ->groupBy('department.name')
        ->groupBy('position.name')
        ->groupBy('adjustment.type')
        ->groupBy('adjustment.created_at');
        
        if (!empty($emp_id) || $emp_id != '') {
            $adjustment->where('adjustment.employee_id', '=', $emp_id);
            $adjustment->where('adjustment_type.type', '=', $type);
            $adjustment->whereNull('adjustment.deleted_at');
            $adjustment->whereRaw("if(adjustment.type = 'ONCE', adjustment.date_file BETWEEN '$start' AND '$end', adjustment.date_file is NOT NULL)");
        }

        return $adjustment;
    }
    /**
     * store adjustment
     * @param  obj $data 
     * @return $adjustment   
     */
    public function preStore($data) {
        $employee_ids = $data['data']['employee_id'];
        if (is_array($employee_ids)) {
            for ($i=0; $i < count($employee_ids); $i++) { 
                $adjustment = new Adjustment;
                $adjustment->employee_id = $data['data']['employee_id'][$i];
                $adjustment->adjustment_type = $data['data']['adjustment_type'];
                $adjustment->adjustment_amount = $data['data']['adjustment_amount'];
                $adjustment->type = $data['data']['type'];
                $adjustment->billable = $data['data']['billable'];
                $adjustment->date_file = $data['data']['date_file'];
                $adjustment->created_by = $data['data']['created_by'];
                $adjustment->created_at = Carbon::now()->toDateTimeString();
                Adjustment::insert($adjustment->toArray());
            }
        }

    } 

    public function payrollListModel() {
        return $this->belongsTo('App\Http\Models\PayrollListModel', 'id');
    }
}
