<?php
namespace App\Http\Models;
use \DB;
use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class PositionModel extends Model
{
    protected $table = "position"; use SoftDeletes;

    public static function position(){
    	return self::select('position.id',
    						DB::raw('position.name As pos'),
    						DB::raw('position.description As des'))
    			->get();
    }
}
