<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class PhilhealthModel extends Model
{
    protected $table = "philhealth"; use SoftDeletes;
}
