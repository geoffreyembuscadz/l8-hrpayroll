<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class TaskStatusModel extends Model
{
    protected $table = "taskstatus"; use SoftDeletes;
}
