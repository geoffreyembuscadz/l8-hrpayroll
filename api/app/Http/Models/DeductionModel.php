<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class DeductionModel extends Model
{
    protected $table = "deduction"; use SoftDeletes;
}
