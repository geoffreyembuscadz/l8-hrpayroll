<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class RoleMenuModel extends Model
{
    protected $table = "role_menu"; use SoftDeletes;
}
