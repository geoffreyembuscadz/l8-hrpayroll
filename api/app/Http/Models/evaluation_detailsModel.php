<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class evaluation_detailsModel extends Model
{
    protected $table = "evaluation_details"; use SoftDeletes;
}
