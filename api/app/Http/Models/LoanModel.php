<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\Http\Models\LoanModel as Loans;


class LoanModel extends Model
{
    protected $table = "loan"; use SoftDeletes;

    public function preListJoin($model) {
    	return $model
    		   ->join('loan_type', 'loan.loan_type', '=', 'loan_type.id')
    		   ->join('employee', 'loan.employee_id', '=', 'employee.id')
               ->join('employee_company', 'employee.id', '=', 'employee_company.employee_id')
               ->join('company', 'employee_company.company_id', '=', 'company.id')
    		   ->join('department', 'employee.department', '=', 'department.id')
    		   ->join('position', 'employee.position', '=', 'position.id');
    }


    /**
     * change query for loan model
     * @param  object $model  
     * @param  int $emp_id 
     * @param  datetime $start  
     * @param  datetime $end    
     * @return $loan         
     */
    public function postListJoin($model, $emp_id, $start, $end) {
    	$loan = $model->select(
            'loan.id', 
            'loan.loan_type', 
            'loan.employee_id', 
            'loan_type.display_name', 
            'loan_type.type as loan_type', 
            'employee.lastname', 
            'employee.firstname', 
            'loan.loan_amount', 
            'loan.max_amount', 
            'loan.current_amount', 
            'company.name as company', 
            'department.name as department', 
            'position.name as position', 
            'loan.type', 
            'loan.created_at',
            'loan.date_file'
        );


        if (!empty($emp_id) || $emp_id != '') {
            $loan->where('loan.employee_id', '=', $emp_id);
            $loan->whereNull('loan.deleted_at');
            $loan->whereRaw("if(loan.type = 'ONCE' OR loan.current_amount = '0', loan.date_file BETWEEN '$start' AND '$end', loan.date_file is NOT NULL)");
        }

        return $loan;
    }

    /**
     * store loan
     * @param  object $data 
     * @return $loan       
     */
    public function preStore($data) {
        $employee_ids = $data['data']['employee_id'];
        if (is_array($employee_ids)) {
            for ($i=0; $i < count($employee_ids); $i++) { 
                $loan                 = new Loans;
                $loan->employee_id    = $data['data']['employee_id'][$i];
                $loan->loan_type      = $data['data']['loan_type'];
                $loan->loan_amount    = $data['data']['loan_amount'];
                $loan->current_amount = $data['data']['current_amount'];
                $loan->max_amount     = $data['data']['max_amount'];
                $loan->type           = $data['data']['type'];
                $loan->date_file      = $data['data']['date_file'];
                $loan->created_by     = $data['data']['created_by'];
                $loan->created_at     = Carbon::now()->toDateTimeString();
                Loans::insert($loan->toArray());
            }
        }

    }

    public function payrollListModel() {
        return $this->belongsTo('App\Http\Models\PayrollListModel', 'id');
    }

}
