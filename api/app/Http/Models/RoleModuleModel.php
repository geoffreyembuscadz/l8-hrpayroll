<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class RoleModuleModel extends Model
{
    protected $table = "role_module"; use SoftDeletes;
}
