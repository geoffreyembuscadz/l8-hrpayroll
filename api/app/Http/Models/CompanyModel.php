<?php
namespace App\Http\Models;

use \DB AS DB;
use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\CompanyPolicyModel;
use App\Http\Models\CompanyBranchModel;
use App\Http\Models\WorkingScheduleModel;
use App\Http\Models\CompanyPolicyTypeModel;
use App\Http\Models\CompanyGovernmentFieldModel;

class CompanyModel extends Model
{
    protected $table = "company"; use SoftDeletes;

    public function preList($model){

        return $model->join('company_cutoff_date', 'company.id', '=', 'company_cutoff_date.company_id')->orderBy('company.name', 'ASC');
    }

    public function postListJoin($model) {
        
        $company = $model->select(
            'company.id',
            'company.name',
            'company.address',
            'company.contact_person',
            'company.email',
            'company.phone_number',
            'company_cutoff_date.first_cutoff_date',
            'company_cutoff_date.second_cutoff_date',
            'company_cutoff_date.days_before_notif',
            'company.thirteen_month_rule'
        );
        return $company;
    }

    public function getBranchesByCompany($company_id){
        $branches = CompanyBranchModel::where('company_id', $company_id)
            ->select('company_branch.id', 'branch_name', 'branch_address', 'contact_number', 'contact_person')
            ->get();

        return $branches;
    }

    public function getCompanyPolicies($company_id = null){
    	$data = self::join('company_policy', 'company_policy.company_id', '=', 'company.id')
    		->join('company_policy_type', 'company_policy_type.id', '=', 'company_policy.company_policy_type_id')
    		->where('company.id', $company_id)
            ->whereRaw("company_policy.deleted_at IS NULL")
    		->select('company_policy.id', 'company_policy.name', 'company_policy_type.id AS company_policy_type_id', 'company_policy.default_value', 'company_policy.start_value', 'company_policy.end_value', 'company_policy_type.name AS company_policy_type', 'company_policy.start_value', 'company_policy.end_value', 'company_policy.default_value', 'company_policy.format_value')
    		->get();

    	return $data;
    }

    public function getCompanyPolicyEquations($id){
        $data = DB::table('company_policy_equation')
            ->where('company_id', $id)
            ->select('name', 'description', 'id', 'value', 'value_unit', 'equivalent_value', 'equivalent_unit')
            ->get();

        return $data;
    }

    public function updateCompanyPolicy($company_id, $data = []){
        $policy_type_schedule = CompanyPolicyTypeModel::whereRaw("lower(`name`) LIKE '%work%' AND lower(`name`) LIKE '%schedule%'")->select('id')->first();
        $policy_type_schedule_id = (isset($policy_type_schedule['id'])) ? $policy_type_schedule['id'] : null;
        $is_working_schedule = true;

        if(isset($data['policies'])){
        	foreach($data['policies'] AS $policy){
        		if(isset($policy['id'])){
                    $company_policy = CompanyPolicyModel::find($policy['id']);
    	    		$company_policy->name = $policy['company_policy'];
    	    		$company_policy->company_id = $company_id;
    	    		$company_policy->company_policy_type_id = $policy['company_policy_type_id'];
    	    		$company_policy->format_value = $policy['format_value'];
    	    		$company_policy->default_value = $policy['default_value'];
    	    		$company_policy->start_value = $policy['start_value'];
    	    		$company_policy->end_value = $policy['end_value'];
    	    		$company_policy->updated_by = 1;
    	    		$company_policy->updated_at = date('Y-m-d H:i:s');

        			$company_policy->save();

                    if($policy['company_policy_type_id'] == $policy_type_schedule_id){

                        WorkingScheduleModel::where('policy_id', $policy['id'])->update([
                            'time_in'   => $policy['start_value'],
                            'time_out'  => $policy['end_value'],
                            'updated_by'    => 1,
                            'updated_at'    => date('Y-m-d H:i:s')
                        ]);
                    }
        		} else {
        			$company_policy = new CompanyPolicyModel();

        			$company_policy->name = $policy['company_policy'];
        			$company_policy->company_id = $company_id;
        			$company_policy->company_policy_type_id = (isset($policy['company_policy_type_id'])) ? $policy['company_policy_type_id'] : 0;
        			$company_policy->format_value = $policy['format_value'];
        			$company_policy->default_value = (isset($policy['default_value'])) ? $policy['default_value'] : null;
        			$company_policy->start_value = (isset($policy['start_value'])) ? $policy['start_value'] : null;
        			$company_policy->end_value = (isset($policy['end_value'])) ? $policy['end_value'] : null;
        			$company_policy->created_by = 1;
        			$company_policy->created_at = date('Y-m-d H:i:s');

        			$company_policy->save();

                    if($policy['company_policy_type_id'] == $policy_type_schedule_id){
                        $new_working_schedule = new WorkingScheduleModel();
                        $new_working_schedule->name = $policy['company_policy'];
                        $new_working_schedule->time_in = $policy['start_value'];
                        $new_working_schedule->time_out = $policy['end_value'];
                        $new_working_schedule->policy_id = $company_policy->id;
                        $new_working_schedule->created_by = 1;
                        $new_working_schedule->created_at = date('Y-m-d H:i:s');
                        $new_working_schedule->save();
                    }
        		}
        	}
        }

        if(isset($data['company_cutoff_date'])){
            $check_existing = DB::table('company_cutoff_date')->where('company_id', $company_id)->count();

            if($check_existing > 0){
                $first_cutoff_first_day = (isset($data['company_cutoff_date']['first_cutoff_first_day'])) ? $data['company_cutoff_date']['first_cutoff_first_day'] : null;
                $first_cutoff_sec_day = (isset($data['company_cutoff_date']['first_cutoff_second_day'])) ? $data['company_cutoff_date']['first_cutoff_second_day'] : null;
                $second_cutoff_first_day = (isset($data['company_cutoff_date']['second_cutoff_first_day'])) ? $data['company_cutoff_date']['second_cutoff_first_day'] : null;
                $second_cutoff_sec_day = (isset($data['company_cutoff_date']['second_cutoff_second_day'])) ? $data['company_cutoff_date']['second_cutoff_second_day'] : null;

                
                $first_cutoff_dates = [ $first_cutoff_first_day, $first_cutoff_sec_day ];
                $second_cutoff_dates = [ $second_cutoff_first_day, $second_cutoff_sec_day ];

                if(count($first_cutoff_dates) > 0 && count($second_cutoff_dates) > 0 && (isset($data['company_cutoff_date']['days_before_notif']) || !empty($data['company_cutoff_date']['days_before_notif']))){
                    DB::table('company_cutoff_date')->where('company_id', $company_id)->update([
                        'first_cutoff_date'     => json_encode($first_cutoff_dates),
                        'second_cutoff_date'    => json_encode($second_cutoff_dates),
                        'days_before_notif'     => $data['company_cutoff_date']['days_before_notif'],
                        'created_at'            => date('Y-m-d H:i:s'),
                        'created_by'            => 1
                    ]);
                }
            } else if($check_existing == 0){
                $first_cutoff_dates = (isset($data['company_cutoff_date']['first_cutoff_first_day']) && isset($data['company_cutoff_date']['first_cutoff_second_day'])) ? [ $data['company_cutoff_date']['first_cutoff_first_day'], $data['company_cutoff_date']['first_cutoff_second_day'] ] : [];

                $second_cutoff_dates = (isset($data['company_cutoff_date']['second_cutoff_first_day']) && isset($data['company_cutoff_date']['second_cutoff_second_day'])) ? [ $data['company_cutoff_date']['second_cutoff_first_day'], $data['company_cutoff_date']['second_cutoff_second_day'] ] : [];

                if(count($first_cutoff_dates) > 0 && count($second_cutoff_dates) > 0){
                    DB::table('company_cutoff_date')->insert([
                        'company_id'            => $company_id,
                        'first_cutoff_date'     => json_encode($first_cutoff_dates),
                        'second_cutoff_date'    => json_encode($second_cutoff_dates),
                        'days_before_notif'     => $data['company_cutoff_date']['days_before_notif'],
                        'created_at'            => date('Y-m-d H:i:s'),
                        'created_by'            => 1
                    ]);
                }
            }
        }

        if(isset($data['company_government_field'])){
            CompanyGovernmentFieldModel::where('company_id', $company_id)->update(['first_cutoff' => null, 'second_cutoff' => null ]);
            
            foreach($data['company_government_field'] AS $government_input_field){

                if(isset($government_input_field['code'])){
                    $check = DB::table('company_government_field')->where('company_id', $company_id)->where('code', $government_input_field['code'])->count();

                    if($check > 0){
                        $update = [];

                        if(isset($government_input_field['tax_term']) ){
                            $update['tax_term'] = $government_input_field['tax_term'];
                        }

                        if(isset($government_input_field['first_cutoff']) ){
                            $update['first_cutoff'] = (int)$government_input_field['first_cutoff'];
                        }

                        if(isset($government_input_field['second_cutoff']) ){
                            $update['second_cutoff'] = (int)$government_input_field['second_cutoff'];
                        }

                        if(isset($government_input_field['default_value']) ){
                            $update['default_value'] = (int)$government_input_field['default_value'];
                        }

                        if(isset($government_input_field['is_default']) ){
                            $update['is_default'] = (int)$government_input_field['is_default'];
                        }

                        if(isset($government_input_field['min_days']) ){
                            $update['daily_min_days'] = $government_input_field['min_days'];
                        }

                        if($government_input_field['active']){
                            $update['active'] = (int)$government_input_field['active'];
                        }

                        $update['updated_by'] = 1;
                        $update['updated_at'] = date('Y-m-d H:i:s');

                        CompanyGovernmentFieldModel::where('company_id', $company_id)->where('id', $government_input_field['id'])->update($update);
                    } else if($check == 0){

                        CompanyGovernmentFieldModel::insert([
                            'company_id' => $company_id,
                            'code' => $government_input_field['code'],
                            'tax_term' => (!empty($government_input_field['tax_term'])) ? $government_input_field['tax_term'] : null,
                            'default_value' => (!empty($government_input_field['default_value'])) ? (int)$government_input_field['default_value'] : 0,
                            'is_default' => (!empty($government_input_field['is_default'])) ? (int)$government_input_field['is_default'] : 0,
                            'first_cutoff' => (!empty($government_input_field['first_cutoff'])) ? (int)$government_input_field['first_cutoff'] : 0,
                            'second_cutoff' => (!empty($government_input_field['second_cutoff'])) ? (int)$government_input_field['second_cutoff'] : 0,
                            'daily_min_days' => (!empty($government_input_field['min_days'])) ? (int)$government_input_field['min_days'] : 0,
                            'active' => ($government_input_field['active']) ? 1 : 0,
                            'created_at' => date('Y-m-d H:i:s'),
                            'created_by' => 1
                        ]);
                    }
                }
            }
        }

        $misc_updates = [ 'billing_rate', 'billing_asf', 'billing_vat', 'billing_13th', 'billing_breakdown', 'default_payroll_period', 'undertime_rule', 'late_convertion', 'fixed_halfday', 'night_shift', 'overtime_rule', 'holiday_rule', 'holiday_allowance', 'attendance_default', 'is_admin_fee', 'admin_fee', 'thirteen_month_rule', 'default_trainee_rate', 'overtime_allowance', 'approver_name', 'flexi_late', 'late_per_hour' ];
        $misc_data_update = [];

        foreach($misc_updates AS $misc_index){
            if(isset($data[$misc_index])){
                $misc_data_update[$misc_index] = $data[$misc_index];
            }
        }

        $misc_data_update['updated_at'] = date('Y-m-d');
        $misc_data_update['updated_by'] = 1;

        self::where('id', $company_id)->update($misc_data_update);

    	$data = CompanyPolicyModel::where('company_id', $company_id)->get();

    	return $data;
    }

    public function updateCompanyBranch($id, $data){
        if(isset($data['company_branch'])){
            foreach($data['company_branch'] AS $company_branch){
                if(!empty($company_branch['id'])){
                    CompanyBranchModel::where('id', $company_branch['id'])->where('company_id', $id)->update([
                        'branch_name' => $company_branch['branch_name'],
                        'branch_address' => $company_branch['branch_address'],
                        'contact_number' => $company_branch['contact_number'],
                        'contact_person' => $company_branch['contact_person']
                    ]);
                } else {
                    CompanyBranchModel::insert([
                        'branch_name' => preg_replace(['/\s{2,}/', '/[\t\n]/'], ' ', trim($company_branch['branch_name'])),
                        'branch_address' => preg_replace(['/\s{2,}/', '/[\t\n]/'], ' ', trim($company_branch['branch_address'])),
                        'contact_number' => preg_replace(['/\s{2,}/', '/[\t\n]/'], ' ', trim($company_branch['contact_number'])),
                        'contact_person' => preg_replace(['/\s{2,}/', '/[\t\n]/'], ' ', trim($company_branch['contact_person'])),
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => 1
                    ]);
                }
            }
        }

        $data = CompanyBranchModel::where('company_id', $id)->get();

        return $data;
    }

    public function getCompanyWorkingSchedules($company_id, $data = []){
        $data = [];

        if(!empty($company_id))
            $data = WorkingScheduleModel::where('company_id', $company_id)->select('working_schedule.id', 'working_schedule.name', 'working_schedule.flexible_time', 'working_schedule.grace_period_mins', DB::raw('date_format(time_in, "%h:%i %p") as time_in, date_format(time_out, "%h:%i %p") as time_out'), 'working_schedule.lunch_break_time', 'working_schedule.lunch_mins_break', 'working_schedule.days')->get();

        return $data;
    }

    public function updateCompanyWorkingSchedules($company_id, $data = []){
        $company_id = $company_id;
        
        if(count($data)){
            foreach($data AS $schedule){
                if(isset($schedule['id']) && !empty($company_id)){
                    $working_schedule = WorkingScheduleModel::find($schedule['id']);
                    $working_schedule->company_id = $company_id;
                    $working_schedule->name = $schedule['name'];
                    $working_schedule->days = json_encode($schedule['days']);
                    $working_schedule->flexible_time = (isset($schedule['flexible_time'])) ? 1 : 0;
                    $working_schedule->grace_period_mins = $schedule['grace_period_mins'];
                    $working_schedule->time_in = date_format(date_create($schedule['time_in']), "H:i:s") ;
                    
                    // $working_schedule->lunch_break_time = (isset($schedule['lunch_break_time'])) ? date_format(date_create($schedule['lunch_break_time']), "H:i:s") : date('h:i:s A', strtotime($schedule['time_in'] . ' + 10 hours'));
                    if(!empty($schedule['lunch_break_time'])){
                        $working_schedule->lunch_break_time = date_format(date_create($schedule['lunch_break_time']), "H:i:s");
                    } else {
                        $start_time_ng_schedule = date_format(date_create($schedule['time_in']), "H:i:s");
                        $start_time_ng_schedule_pieces = explode(':', $start_time_ng_schedule);
                        $lunch_time_added_in_three_hours = ((int)$start_time_ng_schedule_pieces[0] + 3) . ':' . $start_time_ng_schedule_pieces[1] . ':' . $start_time_ng_schedule_pieces[2];
                        $working_schedule->lunch_break_time = $lunch_time_added_in_three_hours;
                    }

                    $working_schedule->lunch_mins_break = (isset($schedule['lunch_mins_break']) && !empty($schedule['lunch_mins_break'])) ? (int)$schedule['lunch_mins_break'] : 60;
                    // date("H:i:s", strtotime('+3 hours', $schedule['time_in']))

                    $working_schedule->time_out = date_format(date_create($schedule['time_out']), "H:i:s");
                    $working_schedule->updated_at = date('Y-m-d H:i:s');
                    $working_schedule->updated_by = 1;

                    $working_schedule->save();

                } else {
                    $working_schedule = new WorkingScheduleModel();
                    
                    $working_schedule->company_id = $company_id;
                    
                    $working_schedule->name = $schedule['name'];
                    $working_schedule->days = json_encode($schedule['days']);
                    $working_schedule->flexible_time = (isset($schedule['flexible_time'])) ? 1 : 0;
                    $working_schedule->grace_period_mins = $schedule['grace_period_mins'];
                    $working_schedule->time_in = date_format(date_create($schedule['time_in']), "H:i:s");
                    $working_schedule->lunch_break_time = (isset($schedule['lunch_break_time'])) ? date_format(date_create($schedule['lunch_break_time']), "H:i:s") :  date("H:i:s", strtotime('+3 hours', date_format(date_create($schedule['time_in']),"H:i:s"))); // 3600 is 1Hr
                    $working_schedule->lunch_mins_break = (isset($schedule['lunch_mins_break'])) ? (int)$schedule['lunch_mins_break'] : 60;
                    $working_schedule->time_out = date_format(date_create($schedule['time_out']), "H:i:s");
                    $working_schedule->created_at = date('Y-m-d H:i:s');
                    $working_schedule->created_by = 1;

                    $working_schedule->deleted_at = null;
                    $working_schedule->deleted_by = null;

                    $working_schedule->save();
                }
            }
        }

        $data = WorkingScheduleModel::where('company_id', $company_id)->get();

        return $data;
    }

    public function getCompanyCutoffDates($company_id){
        $catch = [ 'days_before_notif' => null, 'first_cutoff_date' => '[]', 'second_cutoff_date' => '[]' ];
        $data = DB::table('company_cutoff_date')->where('company_id', $company_id)->select('id', 'days_before_notif', 'first_cutoff_date', 'second_cutoff_date')->first();

        return count($data) ? $data : $catch;
    }

    public function getGovernmentFields($company_id){
        $government_fields = [ 'SSS', 'PAGIBIG', 'PHILHEALTH', 'TIN' ];

        foreach($government_fields AS $government_field){
            $check = CompanyGovernmentFieldModel::where('company_id', $company_id)->where('code', $government_field)->count();

            if($check == 0){
                CompanyGovernmentFieldModel::insert([
                    'company_id' => $company_id,
                    'code' => $government_field,
                    'active' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => 1
                ]);
            }
        }

        $data = CompanyGovernmentFieldModel::where('company_id', $company_id)->select('id', 'company_id', 'code', 'default_value', 'active', 'tax_term', 'is_default', 'first_cutoff', 'second_cutoff', 'daily_min_days')->get();
        
        return $data;
    }

    public function addDefaultMainSchedule($company_id ){
        // This function is only executed when a company is successfully added
        if(!empty($company_id)){
            $new_company_working_schedule = new WorkingScheduleModel();
            $new_company_working_schedule->company_id = $company_id;
            $new_company_working_schedule->name = 'main / regular company schedule';
            $new_company_working_schedule->flexible_time = 0;
            $new_company_working_schedule->grace_period_mins = 0;

            $new_company_working_schedule->time_in = '09:00:00';
            $new_company_working_schedule->time_out = '17:00:00';
            $new_company_working_schedule->days = json_encode([ 'monday', 'tuesday', 'wednesday', 'thursday', 'friday' ]);

            $new_company_working_schedule->created_at = date('Y-m-d H:i:s');
            $new_company_working_schedule->created_by = 1;
            $new_company_working_schedule->save();
        }
    }

    public function addDefaultGovernmentField($company_id){
        $government_fields = [ 'SSS', 'PAGIBIG', 'PHILHEALTH', 'TIN' ];

        foreach($government_fields AS $govrn_field){
            $is_default = 0;
            $default_value = 0;

            if($govrn_field == 'PAGIBIG'){
                $is_default = 1;
                $default_value = 50;
            }

            $new_company_gov_field = new CompanyGovernmentFieldModel();
            $new_company_gov_field->code = $govrn_field;
            $new_company_gov_field->company_id = $company_id;
            $new_company_gov_field->tax_term = 'DAILY';
            $new_company_gov_field->default_value = $default_value;
            $new_company_gov_field->is_default = $is_default;
            $new_company_gov_field->first_cutoff = 1;
            $new_company_gov_field->second_cutoff = 0;
            $new_company_gov_field->daily_min_days = 1;
            $new_company_gov_field->active = 1;
            $new_company_gov_field->created_by = 1;
            $new_company_gov_field->created_at = date('Y-m-d H:i:s');
            $new_company_gov_field->save();
        }
    }



    public function setDefaultPolicies($company_id){
        $data = [
            'default_payroll_period' => 'semi-monthly',
            'is_admin_fee' => 0,
            'billing_13th' => 0,
            'billing_rate' => 0,
            'billing_vat' => 0,
            'is_admin_p' => 0,
            'admin_fee' => 0,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => 1
        ];

        $data['is_admin_p'] = 0;
        $data['billing_13th'] = 0;
        $data['billing_asf'] = 0;
        $data['billing_vat'] = 0;
        $data['night_shift'] = 2;
        $data['is_admin_fee'] = 0;
        $data['billing_rate'] = 670.78;
        $data['holiday_rule'] = 2;
        $data['overtime_rule'] = 2;
        $data['fixed_halfday'] = 1;
        $data['undertime_rule'] = 0;
        $data['late_convertion'] = 1;
        $data['billing_breakdown'] = 0;
        $data['holiday_allowance'] = 1;
        $data['attendance_default'] = 1;
        $data['overtime_allowance'] = 0;
        $data['thirteen_month_rule'] = 0;
        $data['default_trainee_rate'] = 0;
        $data['default_payroll_period'] = 'semi-monthly';

        DB::table('company')->where('id', $company_id)->update($data);

    }

    public function setDefaultCutOffDates($company_id){
        DB::table('company_cutoff_date')->insert([
            'company_id' => $company_id,
            'first_cutoff_date' => '["1", "15"]',
            'second_cutoff_date' => '["16", "last_day_of_month"]',
            'days_before_notif' => 3,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }

    public function addDefaultMainBranch($company_id){
        // This function is only executed when a company is successfully added
        $new_company_branch = new CompanyBranchModel();
        $new_company_branch->company_id = $company_id;
        $new_company_branch->branch_name = 'Main Branch';
        $new_company_branch->branch_address = 'Main Branch Address';
        $new_company_branch->contact_number = 'Main Branch Contact Number';
        $new_company_branch->contact_person = 'Main Branch Contact Person';
        $new_company_branch->created_at = date('Y-m-d H:i:s');
        $new_company_branch->created_by = 1;
        $new_company_branch->save();
    }

    public function showComInfo(){
        return DB::table('company')
                ->select('id',
                         'name',
                         'description',
                         'address',
                         'email',
                         'phone_number',
                         'contact_person')
                ->whereRaw("company.deleted_at IS NULL")->get();
    }
}
