<?php
namespace App\Http\Models;

use \DB; 
use Illuminate\Database\Eloquent\Model; 
use \Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class questionModel extends Model
{
    protected $table = "question"; use SoftDeletes;

    public static function question(){
    	$query = DB::table('question')
    			->select('id','question As text')
    			->where('deleted_by', '=', NULL)
    			->get();
    	return $query;
    }
}
