<?php
namespace App\Http\Models;

use \DB AS DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyGovernmentFieldModel extends Model
{
    protected $table = "company_government_field"; use SoftDeletes;
}