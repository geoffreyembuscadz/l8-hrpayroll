<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Http\Controllers\UserController;

class AttendanceModel extends Model
{
    protected $table = "employees_logs"; use SoftDeletes;

    public static function getUser(){
      $contr = new UserController;
      $data = $contr->getListByIdLimited();

      return $data;
    }

    public function getUserId($id){
        $contr = new UserController;
        $data = $contr->getUserId($id);

        return $data;
    }

    public function getEmployeeBranch($id){
        $contr = new UserController;
        $data = $contr->getEmployeeBranch($id);

        return $data;
    }

    public function storeCSVFile($data){

        $user = $this->getUser();
        $len = count($data);
        $date = date('Y-m-d H:i:s');
        $type = 2;

        foreach ($data as $d) {
 
            DB::table('employees_logs')->insert(
                array(
                    'employee_id'        => $d['emp_id'],
                    'attendance_type_id' => $type,
                    'time_in'            => $d['time_in'],
                    'time_out'           => $d['time_out'],
                    'branch_id'          => $d['branch_id'],
                    'late'               => $d['late'],
                    'undertime'          => $d['undertime'],
                    'holiday_id'         => $d['holiday_id'],
                    'overtime'           => $d['overtime'],
                    'night_differential' => $d['night_differential'],
                    'halfday'            => $d['halfday'],
                    'created_by'         => $user->user_id,
                    'created_at'         => $date
                    )
                );
        }  
        return $data;   
    }

    public function searchEmployee($data){

        $user = $this->getUser();
        $supervisor_id = $user->user_id;
        $role_id = $user->role_id;

        $department = $data['department'];
        $company = $data['company'];
        $branch_id = $data['branch_id'];
        $position_id = $data['position_id'];

        $filter = DB::table('employee')
            ->join('employee_company', function($join){
                $join->on('employee.id', '=', 'employee_company.employee_id');
                $join->on('employee.branch', '=', 'employee_company.branch_id');
            })
            ->join('company', 'employee_company.company_id', '=', 'company.id')
            ->join('position', 'employee.position', '=', 'position.id')
            ->join('department', 'employee.department', '=', 'department.id')
            ->select(
              DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
              DB::raw('employee.id As id'),
              'employee_company.branch_id'
              )
            ->where('employee.status',1)
            ->whereRaw('employee.deleted_at IS NOT NULL AND employee.deleted_by IS NOT NULL')
            ->orderBy('name');

        if($role_id > 2 AND $role_id < 0) {
            $filter->where('employee.supervisor_id', 'LIKE', '%' . $supervisor_id . '%');
        }

        if($company != 0){
            $filter->where('employee_company.company_id',$company);
        }
        
        if($department != 0){
            $filter->where('employee.department',$department);
        }

        if(count($branch_id) != 0 && $branch_id != 0){
            $filter->where('employee.branch',$branch_id);
        }

        if($position_id != 0){
            $filter->where('employee.position',$position_id);
        }

        $data = $filter->get();
        return $data;
    }

    public function createTimeAttendance($data){
        
        $user = $this->getUser();
        $ids= [];
        $type = 1;
        $date = date('Y-m-d H:i:s');
        foreach ($data as $counter => $d) {
            $id = DB::table('employees_logs')->insertGetId(
                    array(
                        'employee_id'         => $d['id'],
                        'attendance_type_id'  => $type,
                        'time_in'             => $d['time_in'],
                        'time_out'            => $d['time_out'],
                        'lunch_in'            => $d['lunch_in'],
                        'lunch_out'           => $d['lunch_out'],
                        'late'                => $d['late'],
                        'undertime'           => $d['undertime'],
                        'overtime'            => $d['overtime'],
                        'halfday'             => $d['halfday'],
                        'holiday_id'          => $d['holiday_id'],
                        'night_differential'  => $d['night_differential'],
                        'branch_id'           => $d['branch_id'],
                        'created_by'          => $user->user_id,
                        'created_at'          => $date
                        )
                );
            $ids[$counter]= $id;
        }

        $table = DB::table('employees_logs')->whereIn('id',$ids)->select()->get();
        return $table;
        //bulk insert
        // return self::insert($data);
    }

    public static function getList(){

        return self::join('employee', 'employees_logs.employee_id', '=', 'employee.id')
        ->join('attendance_type', 'employees_logs.attendance_type_id', '=', 'attendance_type.id')
        ->join('employee_company', 'employee.id', '=', 'employee_company.employee_id')
        ->join('company_branch', 'employees_logs.branch_id', '=', 'company_branch.id')
        ->select(
            DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'), 
            DB::raw('attendance_type.name AS types'),
            DB::raw('company_branch.branch_name as location'),
            'employees_logs.id', 
            'employees_logs.employee_id', 
            'employees_logs.time_in', 
            'employees_logs.time_out', 
            'employees_logs.attendance_type_id',
            'employees_logs.late',
            'employees_logs.undertime')
        ->orderBy('employees_logs.created_at', 'desc')
        ->whereRaw('employee.deleted_at IS NOT NULL AND employee.deleted_by IS NOT NULL')
        ->get();
    }

    public function getMyAttendance($data){

        $user = $this->getUser();

        $employee_id = $user->employee_id;
        $type_id = $data['type_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];

        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('employees_logs')
        ->join('employee', 'employees_logs.employee_id', '=', 'employee.id')
        ->join('attendance_type', 'employees_logs.attendance_type_id', '=', 'attendance_type.id')
        ->join('employee_company', 'employees_logs.employee_id', '=', 'employee_company.employee_id')
        ->leftJoin('company_branch', 'employees_logs.branch_id', '=', 'company_branch.id')
        ->select(
            DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
            DB::raw('attendance_type.name AS types'),
            DB::raw("(SELECT IFNULL(company_branch.branch_name, 'From Biometrics')) as location"),
            'employees_logs.id', 
            'employees_logs.employee_id', 
            'employees_logs.time_in', 
            'employees_logs.time_out', 
            'employees_logs.attendance_type_id',
            'employees_logs.late',
            'employees_logs.undertime',
            'employees_logs.branch_id')
        ->whereRaw('employees_logs.deleted_at IS NOT NULL')
        ->where('employees_logs.employee_id','=',$employee_id)
        ->orderBy('employees_logs.created_at', 'desc');

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('employees_logs.time_in', [$start_date,$end_date]);
        }

        if ($type_id[0] != 'null') {
            $type = [];
            $len = count($type_id);
            for ($x=0; $x < $len ; $x++) { 
                $type[$x]= (int)$type_id[$x];
            }
            $filter->whereIn('employees_logs.attendance_type_id',$type);
        }

        $data = $filter->get();
        return $data;

    }

    public function getAttendanceDefault($data){

        $user = $this->getUser();

        $supervisor_id = $user->user_id;
        $role_id = $user->role_id;

        $employee_id = $data['employee_id'];
        $company_id = $data['company_id'];
        $type_id = $data['type_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];

        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('employees_logs')
        ->join('employee', 'employees_logs.employee_id', '=', 'employee.id')
        ->join('attendance_type', 'employees_logs.attendance_type_id', '=', 'attendance_type.id')
        ->join('employee_company', 'employees_logs.employee_id', '=', 'employee_company.employee_id')
        ->leftJoin('company_branch', 'employees_logs.branch_id', '=', 'company_branch.id')
        ->select(
            DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
            DB::raw('attendance_type.name AS types'),
            DB::raw("(SELECT IFNULL(company_branch.branch_name, 'From Biometrics')) as location"),
            'employees_logs.id', 
            'employees_logs.employee_id', 
            'employees_logs.time_in', 
            'employees_logs.time_out', 
            DB::raw("CAST(employees_logs.time_in AS date) as date_in"),
            DB::raw("CAST(employees_logs.time_out AS date) as date_out"),
            'employees_logs.lunch_in', 
            'employees_logs.lunch_out', 
            'employees_logs.attendance_type_id',
            'employees_logs.late',
            'employees_logs.undertime')
        ->where('employees_logs.deleted_at','=',null)
        ->orderBy('name', 'desc')
        ->orderBy('employees_logs.time_in', 'asc')
        ->orderBy('employees_logs.created_at', 'desc');

        if($role_id > 2 AND $role_id < 0) {
            $filter->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
        }

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('employees_logs.time_in', [$start_date,$end_date]);
        }
        if ($type_id[0] != 'null') {
            $type = [];
            $len = count($type_id);
            for ($x=0; $x < $len ; $x++) { 
                $type[$x]= (int)$type_id[$x];
            }
            $filter->whereIn('employees_logs.attendance_type_id',$type);
        }
        if ($company_id[0] != 'null') {
            $comp = [];
            $len = count($company_id);
            for ($x=0; $x < $len ; $x++) { 
                $comp[$x]= (int)$company_id[$x];
            }
            $filter->whereIn('employee_company.company_id',$comp);
        }
        if($employee_id[0] != 'null') {
            $filter->whereIn('employees_logs.employee_id',$employee_id);
        }

        $data = $filter->get();
        return $data;


    }

    public function getAttendance($data){

        $user = $this->getUser();

        $supervisor_id= $user->user_id;
        $role_id = $user->role_id;

        $employee_id = $data['employee_id'];
        $company_id = $data['company_id'];
        $type_id = $data['type_id'];
        $start = $data['start_date'];
        $end = $data['end_date'];

        // converter for date to string
        $star = strtotime( $start );
        $start_date = date( 'Y-m-d', $star );

        $end = strtotime( $end );
        $end_date = date( 'Y-m-d', $end );

        $filter = DB::table('employee_logs2')
        ->join('employee', 'employee_logs2.employee_id', '=', 'employee.id')
        ->join('employee_company', 'employee_logs2.employee_id', '=', 'employee_company.employee_id')
        ->join('company_branch', 'employee_logs2.branch_id', '=', 'company_branch.id')
        ->join('company', 'company_branch.company_id', '=', 'company.id')
        ->select(
            DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
            DB::raw('company_branch.branch_name as branch'),
            DB::raw('company.name as company'),
            'employee_logs2.id', 
            'employee_logs2.employee_id', 
            'employee_logs2.start_date', 
            'employee_logs2.end_date', 
            'employee_logs2.late',
            'employee_logs2.overtime')
        ->where('employee_logs2.deleted_at','=',null)
        ->orderBy('employee_logs2.created_at', 'desc');

        if($role_id > 2 AND $role_id < 0) {
            $filter->where('employee.supervisor_id', 'like', '%' . $supervisor_id . '%');
        }

        if( $start != 'null' && $end != 'null'){
            $filter->whereBetween('employee_logs2.start_date', [$start_date,$end_date]);
        }
        if ($type_id[0] != 'null') {
            $type = [];
            $len = count($type_id);
            for ($x=0; $x < $len ; $x++) { 
                $type[$x]= (int)$type_id[$x];
            }
            $filter->whereIn('employee_logs2.attendance_type_id',$type);
        }
        if ($company_id[0] != 'null') {
            $comp = [];
            $len = count($company_id);
            for ($x=0; $x < $len ; $x++) { 
                $comp[$x]= (int)$company_id[$x];
            }
            $filter->whereIn('employee_company.company_id',$comp);
        }
        if($employee_id[0] != 'null') {
            $filter->whereIn('employee_logs2.employee_id',$employee_id);
        }

        $data = $filter->get();
        return $data;
    }

    //arrange schedule in an array
    /**
     * [arrangeDateData description]
     * @param  [type] $id    string employee_id
     * @param  [type] $start_time     datetime time_in or start_time
     * @param  [type] $date date
     * @return [type] obj   schedule of time_in and time_out for that date
     */
    public function arrangeDateData($id,$time_in,$date){

        $dt = strtotime($date);
        $e = date("l", $dt);
        $days=strtolower($e);
        $data = null;

        $sched = $this->checkSA($id,$days,$date);

        
        if ($sched != null) {
        //id = employee_id string
        //date date
        //time_in time
        //time_out time
        //grace_period_mins int
        //restday 1 or 0 int, if 1 means restday 0 is not
            $data[] = array(
                'id'                => $id,
                'date'              => $date,
                'time_in'           => $sched->time_in,
                'lunch_in'          => $sched->lunch_break_time,
                'lunch_out'         => $sched->lunch_mins_break,
                'time_out'          => $sched->time_out,
                'grace_period_mins' => 0,
                'restday'           => $sched->restday,
                'sched_id'          => (isset($sched->working_schedule_id) ? $sched->working_schedule_id : null)
            );

        }else{
            $data[] = array(
                'id'                => $id,
                'date'              => $date,
                'time_in'           => "08:00:00",
                'lunch_in'          => "12:00:00",
                'lunch_out'         => "13:00:00",
                'time_out'          => "17:00:00",
                'grace_period_mins' => 0,
                'restday'           => 1
            );
        }

        //check if he had a overtime
        $overtime = $this->getOvertimeApproved($data,$time_in,$id);
        $data[0]["overtime"] = $overtime;

        //grace period
        $time = Carbon::parse($data[0]["time_in"]);
        $minus = $time->addMinutes($data[0]["grace_period_mins"]);
        $convert = $minus->toDateTimeString();
        $data[0]["time_in"] = substr($convert, 11);
       
        return $data;
    }
    public function getWorkingScheduleById($dat){

        $sa = $dat['sa'];
        $id = $dat['id'];
        $date = $dat['date'];
        $d = $dat['start_time'];
        $restday = null;

        $sched = DB::table('working_schedule')
                ->select(
                    'working_schedule.id', 
                    'working_schedule.time_in', 
                    'working_schedule.time_out', 
                    'working_schedule.days',
                    'working_schedule.company_id',
                    'working_schedule.grace_period_mins',
                    'working_schedule.lunch_break_time',
                    'working_schedule.lunch_mins_break')
                ->where('working_schedule.id','=',$sa)
                ->whereNull('working_schedule.deleted_at')
                ->first();

        if ($sched != null) {
            if ($sched->lunch_break_time == null) {
                $sched->lunch_break_time = '12:00:00';
            }
            if ($sched->lunch_mins_break == null) {
                $sched->lunch_mins_break = 60;
            }

            $sched->break_start = $sched->lunch_break_time;
            $hour  = Carbon::parse($sched->lunch_break_time)->addMinutes($sched->lunch_mins_break);
            $convert = $hour->toDateTimeString();
            $sched->break_end  = substr($convert, 11);
        }

        $data[] = array(
            'id'                => $id,
            'date'              => $date,
            'time_in'           => $sched->time_in,
            'time_out'          => $sched->time_out,
            'grace_period_mins' => 0,
            'restday'           => $restday
        );

        //check if he had a overtime
        $overtime = $this->getOvertimeApproved($data,$d,$id);
        $data[0]["overtime"] = $overtime;

        //grace period
        $time = Carbon::parse($data[0]["time_in"]);
        $minus = $time->addMinutes($data[0]["grace_period_mins"]);
        $convert = $minus->toDateTimeString();
        $data[0]["time_in"] = substr($convert, 11);
       
        return $data;
    }

    public function getOvertimeApproved($data,$d,$id){
        $date = substr($d, 0,-9);

        $data = DB::table('overtime')
        ->select( 
            'overtime.status_id'
        )
        ->where('overtime.date','=',$date)
        ->where('overtime.emp_id','=',$id)
        ->where('overtime.status_id','=',4)
        ->first();


        $status_id = 0;

        if($data != null){
            $status_id = 1;
        }
        
        return $status_id;

     }

    

    public function getRestDay($days){

        $restday = ["monday","tuesday","wednesday","thursday","friday","saturday","sunday"];

        $len = count($days);
        for ($i=0; $i < $len ; $i++) { 

            if (($key = array_search($days[$i], $restday)) !== false) {
                // unset($restday[$key]);
                array_splice($restday, $key, 1);
            }

        }

        return $restday;

    }

    public function preUpdate($id, $data){
        $d = $data;
        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        DB::table('employees_logs')->where('id',$id)->update([
            'employee_id'       => $d['data']['employee_id'],
            'time_in'           => $d['data']['time_in'],
            'time_out'          => $d['data']['time_out'],
            'late'              => $d['data']['late'],
            'undertime'         => $d['data']['undertime'],
            'holiday_id'        => $d['data']['holiday_id'],
            'overtime'          => $d['data']['overtime'],
            'halfday'           => $d['data']['halfday'],
            'night_differential'=> $d['data']['night_differential'],
            'branch_id'         => $d['data']['branch_id'],
            'updated_by'        => $user->user_id,
            'updated_at'        => $date
        ]);
        
        return $data;
    }

    public function getHoliday($date,$restday){
   
        $data = DB::table('holiday')
            ->select([
            'holiday.holiday_type_id',
            ])
            ->whereRaw("date_format(holiday.date, '%m-%d') = date_format('$date', '%m-%d')")
            ->where('deleted_at','=',null)
            ->where('deleted_by','=',null)
            ->first();


        $holiday_type_id = null;

        //if restday sya see holiday_type table in database
        if ($restday != 0 || $restday != '0') {
            $holiday_type_id = 1;
        }

        if($data != null){
            $holiday_type_id = $data->holiday_type_id;
            $type = $data->holiday_type_id;

            if($restday != 0 || $restday != '0'){
                if($type == 2){
                    $holiday_type_id = 3;
                }
                elseif ($type == 4) {
                    $holiday_type_id = 5;
                }
                elseif ($type == 6) {
                    $holiday_type_id = 7;
                }
            } 
        }

        return $holiday_type_id;
    }

    //check schedule 1st in SA
    public function checkSA($id,$days,$date){

       $data = DB::table('schedule_adjustments_request')
        ->join('schedule_adjustments', 'schedule_adjustments_request.SA_id', '=', 'schedule_adjustments.id')
        ->join('employee_company', 'schedule_adjustments_request.employee_id', '=', 'employee_company.employee_id')
        ->join('company_branch', 'employee_company.branch_id', '=', 'company_branch.id')
        ->select(
            DB::raw('\'0\' as restday'),
            DB::raw('schedule_adjustments_request.start_time AS time_in'),
            DB::raw('schedule_adjustments_request.end_time AS time_out'),
            'schedule_adjustments.id',
            'schedule_adjustments_request.date',
            'schedule_adjustments_request.break_start AS lunch_break_time',
            'schedule_adjustments_request.break_end AS lunch_mins_break',
            'employee_company.branch_id',
            'company_branch.branch_name'
            )
        ->where('schedule_adjustments_request.employee_id','=' ,$id)
        ->where('schedule_adjustments.status_id','=' ,4)
        ->where('schedule_adjustments_request.date','=' ,$date)
        ->where('schedule_adjustments.deleted_at','=',null)
        ->where('schedule_adjustments.deleted_by','=',null)
        ->first();

        
        //2nd if SA is null 
        if($data == null ){
            //check if user has a schedule for specific day
            $data = $this->getWorkingSched($id,$days);
        }

        //3rd if still null
        if($data == null ){
            //get employee default schedule
            $data = $this->getDefaultSchedule($id);
        }

        return $data;
    }

    public function getWorkingSched($id,$days){
        $data = DB::table('employee_working_schedule')
                 ->join('working_schedule', 'employee_working_schedule.working_schedule_id', '=', 'working_schedule.id')
                 ->join('employee_company', 'employee_working_schedule.employee_id', '=', 'employee_company.employee_id')
                 ->join('company_branch', 'employee_company.branch_id', '=', 'company_branch.id')
                 ->select(
                    DB::raw('\'0\' as restday'),
                    'working_schedule.id', 
                    'working_schedule.id AS working_schedule_id', 
                    'working_schedule.time_in', 
                    'working_schedule.time_out', 
                    'working_schedule.days',
                    'working_schedule.company_id',
                    'working_schedule.grace_period_mins',
                    'working_schedule.lunch_break_time',
                    'working_schedule.lunch_mins_break',
                    'employee_company.branch_id',
                    'company_branch.branch_name')
                ->where('employee_working_schedule.employee_id','=',$id)
                ->where('working_schedule.days', 'like', '%' . $days . '%')
                ->whereNull('working_schedule.deleted_at')
                ->whereNull('working_schedule.deleted_by')
                ->whereNull('employee_working_schedule.deleted_at')
                ->first();

        if ($data != null) {
            if ($data->lunch_break_time == null) {
                $data->lunch_break_time = '12:00:00';
            }
            if ($data->lunch_mins_break == null) {
                $data->lunch_mins_break = 60;
            }
            $data->break_start = $data->lunch_break_time;
            $hour  = Carbon::parse($data->lunch_break_time)->addMinutes($data->lunch_mins_break);
            $convert = $hour->toDateTimeString();
            $data->break_end  = substr($convert, 11);
        }

        return $data;
    }

    public function getDefaultSchedule($id){

        $data = DB::table('employee_working_schedule')
         ->join('working_schedule', 'employee_working_schedule.working_schedule_id', '=', 'working_schedule.id')
         ->join('employee_company', 'employee_working_schedule.employee_id', '=', 'employee_company.employee_id')
         ->join('company_branch', 'employee_company.branch_id', '=', 'company_branch.id')
         ->select(
            DB::raw('\'1\' as restday'),
            'working_schedule.id', 
            'working_schedule.time_in', 
            'working_schedule.time_out', 
            'working_schedule.days',
            'working_schedule.company_id',
            'working_schedule.grace_period_mins',
            'working_schedule.lunch_break_time',
            'working_schedule.lunch_mins_break',
            'employee_company.branch_id',
            'company_branch.branch_name')
        ->where('employee_working_schedule.employee_id','=',$id)
        ->whereNull('working_schedule.deleted_at')
        ->whereNull('working_schedule.deleted_by')
        ->whereNull('employee_working_schedule.deleted_at')
        ->first();

        if ($data != null) {
            if ($data->lunch_break_time == null) {
                $data->lunch_break_time = '12:00:00';
            }
            if ($data->lunch_mins_break == null) {
                $data->lunch_mins_break = 60;
            }
            $data->break_start = $data->lunch_break_time;
            $hour  = Carbon::parse($data->lunch_break_time)->addMinutes($data->lunch_mins_break);
            $convert = $hour->toDateTimeString();
            $data->break_end  = substr($convert, 11);
        }

        return $data;
    }


    public function getSchedules($data){

        $id = $data['id'];
        $date = $data['date'];
        $data = [];
        $lent = count($date);
        $len = count($id);

        for ($x=0; $x < $len ; $x++) { 

            $emp_id = $id[$x];
            
            for ($y=0; $y < $lent ; $y++) {

                $dt = strtotime($date[$y]);
                $e = date("l", $dt);
                $days=strtolower($e);

                $sched = $this->checkSA($emp_id,$days,$date[$y]);


                if (count($sched) == 0) {
                    $data[] = array(
                        'id'            => $emp_id,
                        'date'          => $date[$y],
                        'start_time'    => 0,
                        'end_time'      => 0,
                        'time_in'       => $date[$y],
                        'time_out'      => $date[$y],
                        'break_start'   => 0,
                        'break_end'     => 0,
                        'lunch_in'      => $date[$y],
                        'lunch_out'     => $date[$y],
                        'branch_id'     => 0,
                        'branch_name'   => '',
                        'days'          => $days,
                        'late'          => 0,
                        'undertime'     => 0,
                        'sa'            => 0,
                        'created_by'    => 0
                    );
                }else{

                    $data[] = array(
                        'id'            => $emp_id,
                        'date'          => $date[$y],
                        'start_time'    => $sched->time_in,
                        'end_time'      => $sched->time_out,
                        'time_in'       => $date[$y] .' '. $sched->time_in,
                        'time_out'      => $date[$y] .' '. $sched->time_out,
                        'break_start'   => $sched->break_start,
                        'break_end'     => $sched->break_end,
                        'lunch_in'      => $date[$y] .' '. $sched->break_start,
                        'lunch_out'     => $date[$y] .' '. $sched->break_end,
                        'branch_id'     => $sched->branch_id,
                        'branch_name'   => $sched->branch_name,
                        'days'          => $days,
                        'late'          => 0,
                        'undertime'     => 0,
                        'sa'            => 0,
                        'created_by'    => 0
                    );
                }

            }
        }

        return $data;
       
    }

    public function getBranch($data){
        $id = array_unique($data);
         

        $dat = DB::table('employee_company')
         ->join('employee', 'employee_company.employee_id', '=', 'employee.id')
         ->select(
            DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
            'employee_company.branch_id',
            'employee.id',
            'employee_company.company_id')
        ->whereIn('employee.id',$id)
        ->get();

        return $dat;
    }

    public function createDayAttendance2($data){

        $sched = [];
        $le = count($data);
        $ut = [];
        $user = $this->getUser();
       
        for ($i=0; $i < $le; $i++) { 

            $len = $data[$i]['days'] + $data[$i]['utdays'];

            for ($x=0; $x <$len ; $x++) { 

                $date = $data[$i]['date'][$x];
                $dt = strtotime($date);
                $e = date("l", $dt);
                $days=strtolower($e);

                $emp_id = $data[$i]['emp_id'];
                $dat = $this->checkSA($emp_id,$days,$date);

                if(count($dat) == 0){
                    $dat = $this->getDefaultSchedule($emp_id);
                }

                $time_out = $dat->time_in;

                //use for subtracting minutes of undertime to time_out
                if($data[$i]['utdays'] > 0){
                    $sub = 0;
                //if whole num , 30 min is to save in database
                //if not 480 - the remaining butal in undertime
                    if ($data[$i]['wholenum'] > 0) {
                        $sub = 30;
                        $data[$i]['ut']=$data[$i]['ut']-450;
                        $data[$i]['wholenum']--;
                    }else{
                        $sub = 480 - abs($data[$i]['ut']);
                    }

                    $undertime = $sub;

                    $in = Carbon::parse($dat->time_out)->subMinutes($undertime);

                    $convert = $in->toDateTimeString();
                    $dat->time_out = substr($convert, 11);


                    $data[$i]['utdays']--;
                }

                $leng = count($data[$i]['holiday']);
                $holiday_id = 0;
                for ($y=0; $y < $leng ; $y++) { 
                    if ($data[$i]['holiday'][$y]['date'] == $date) {
                       $holiday_id = $data[$i]['holiday'][$y]['type'];
                    }
                }


                $sched[] = array(
                    'id'            => $emp_id,
                    'date'          => $date,
                    'time_in'       => $date .' '. $dat->time_in,
                    'time_out'      => $date .' '. $dat->time_out,
                    'out'           => $date .' '. $time_out,
                    'break_start'   => $date .' '. $dat->break_start,
                    'break_end'     => $date .' '. $dat->break_end,
                    'holiday_id'    => $holiday_id,
                );

            }
        }

        $ids=[];
        $type = 1;
        $date = date('Y-m-d H:i:s');
        $undertime = 0;
        $ot_id = 0;

        foreach ($data as $d) {

            foreach ($sched as $counter => $sch) {

                if ($d['emp_id'] == $sch['id']) {

                    //check if the employee has an overtime
                    if($d['overtime'] > 0){

                        $time_in = substr($sch['time_out'], 11);
                        $time_out = Carbon::parse($sch['time_out'])->addMinutes($d['overtime']);
                        $ot_id=1;

                        DB::table('overtime')->insertGetId(
                            array(
                                'date'           => $d['date'][0],
                                'emp_id'         => $d['emp_id'],
                                'start_time'     => $time_in,
                                'end_time'       => $time_out,
                                'total_hours'    => $d['ot'],
                                'remarks'        => 'auto generated from day attendance',
                                'status_id'      => 4,
                                'date_applied'   => $date,
                                'created_by'     => $user->user_id,
                                'created_at'     => $date
                                )
                        );

                        $d['overtime'] = 0;
                    }else{
                        $ot_id = 0;
                    }

                    if($d['unddays'] > 0){
                        $sub = 0;
                        //use in subtracting time_out for ut database
                        if ($d['wholeno'] > 0) {
                            $sub = 30;
                            $d['undertime']=$d['undertime']-450;
                            $d['wholeno']--;

                            $ut = Carbon::parse($sch['out'])->subMinutes(30);


                            DB::table('official_undertime')->insertGetId(
                                array(
                                    'emp_id'         => $d['emp_id'],
                                    'date'           => $sch['date'],
                                    'start_time'     => $sch['time_in'],
                                    'end_time'       => $ut,
                                    'total_hours'    => 30,
                                    'remarks'        => 'auto generated from manual input attendance',
                                    'status_id'      => 4,
                                    'date_applied'   => $date,
                                    'created_by'     => $user->user_id,
                                    'created_at'     => $date
                                    )
                            );

                        }else{
                            $sub = 480-abs($d['undertime']);

                            $ut = Carbon::parse($sch['out'])->subMinutes($sub);

                            DB::table('official_undertime')->insertGetId(
                                array(
                                    'emp_id'         => $d['emp_id'],
                                    'date'           => $sch['date'],
                                    'start_time'     => $sch['time_in'],
                                    'end_time'       => $ut,
                                    'total_hours'    => $sub,
                                    'remarks'        => 'auto generated from manual input attendance',
                                    'status_id'      => 4,
                                    'date_applied'   => $date,
                                    'created_by'     => $user->user_id,
                                    'created_at'     => $date
                                    )
                            );
                        }
                        $undertime = $sub;
                        $d['unddays']--;
                    }else{
                        $undertime = 0;
                    }

                    $id = null;

                    // $sch['time_out'] = Carbon::parse($sch['time_out'])->subMinutes($d['ut']);

                    $id = DB::table('employees_logs')->insertGetId(
                            array(
                                'employee_id'         => $d['emp_id'],
                                'attendance_type_id'  => $type,
                                'time_in'             => $sch['time_in'],
                                'time_out'            => $sch['time_out'],
                                'lunch_in'            => $sch['break_start'],
                                'lunch_out'           => $sch['break_end'],
                                'undertime'           => $undertime,
                                'overtime'            => $ot_id,
                                'branch_id'           => $d['branch_id'],
                                'holiday_id'          => $sch['holiday_id'],
                                'created_by'          => $user->user_id,
                                'created_at'          => $date
                            )
                    );

                    // $d['ut'] = 0;
                }

                $ids[$counter]= $id;
            }
        }

        $table=DB::table('employees_logs')->whereIn('id',$ids)->select()->get();
        return $table;
    }

    public function createDayAttendanceDefault($data){


        $len = count($data);
        $sched = [];
        $user = $this->getUser();

        for ($i=0; $i < $len; $i++) { 

            $leng = $data[$i]['days'];

            for ($x=0; $x < $leng; $x++) { 

                $date = $data[$i]['date'][$x];
                
                $dt = strtotime($date);
                $e = date("l", $dt);
                $days=strtolower($e);

                $emp_id = $data[$i]['emp_id'];
                $dat = $this->checkSA($emp_id,$days,$date);

                if(count($dat) == 0){
                    $dat = $this->getDefaultSchedule($emp_id);
                }

                $lent = count($data[$i]['holiday']);
                $holiday_id = 0;
                for ($y=0; $y < $lent ; $y++) { 
                    if ($data[$i]['holiday'][$y]['date'] == $date) {
                       $holiday_id = $data[$i]['holiday'][$y]['type'];
                    }
                }

                $sched[] = array(
                        'emp_id'        => $emp_id,
                        'date'          => $date,
                        'time_in'       => $date .' '. $dat->time_in,
                        'time_out'      => $date .' '. $dat->time_out,
                        'break_start'   => $date .' '. $dat->break_start,
                        'break_end'     => $date .' '. $dat->break_end,
                        'holiday_id'    => $holiday_id,
                );
            }
        }

        $ids=[];
        $type = 1;
        $date = date('Y-m-d H:i:s');
        $undertime = 0;
        $ot_id = 0;


        foreach ($data as $d) {

            foreach ($sched as $counter => $sch) {

                if ($d['emp_id'] == $sch['emp_id']) {

                    if($d['overtime'] > 0){

                        $time_in = substr($sch['time_out'], 11);
                        $time_out = Carbon::parse($sch['time_out'])->addMinutes($d['overtime']);
                        $ot_id=1;

                        DB::table('overtime')->insertGetId(
                            array(
                                'date'           => $d['date'][0],
                                'emp_id'         => $d['emp_id'],
                                'start_time'     => $time_in,
                                'end_time'       => $time_out,
                                'total_hours'    => $d['overtime'],
                                'remarks'        => 'auto generated from day attendance',
                                'status_id'      => 4,
                                'date_applied'   => $date,
                                'created_by'     => $user->user_id,
                                'created_at'     => $date
                                )
                        );

                        $d['overtime'] = 0;
                    }else{
                        $ot_id = 0;
                    }

                    // if($d['undertime'] > 0){

                    //     $ut = Carbon::parse($sch['time_out'])->subMinutes($d['undertime']);
                    //     DB::table('official_undertime')->insertGetId(
                    //         array(
                    //             'emp_id'         => $d['emp_id'],
                    //             'date'           => $sch['date'],
                    //             'start_time'     => $sch['time_in'],
                    //             'end_time'       => $ut,
                    //             'total_hours'    => $d['undertime'],
                    //             'remarks'        => 'auto generated from manual input attendance',
                    //             'status_id'      => 4,
                    //             'date_applied'   => $date,
                    //             'created_by'     => $user->user_id,
                    //             'created_at'     => $date
                    //             )
                    //     );
                    // }

                    $id = null;

                    $sch['time_out'] = Carbon::parse($sch['time_out'])->subMinutes($d['undertime']);

                    $id = DB::table('employees_logs')->insertGetId(
                            array(
                                'employee_id'         => $d['emp_id'],
                                'attendance_type_id'  => $type,
                                'time_in'             => $sch['time_in'],
                                'time_out'            => $sch['time_out'],
                                'lunch_in'            => $sch['break_start'],
                                'lunch_out'           => $sch['break_end'],
                                'late'                => $d['undertime'],
                                'overtime'            => $ot_id,
                                'branch_id'           => $d['branch_id'],
                                'holiday_id'          => $sch['holiday_id'],
                                'night_differential'  => $d['night_diff'],
                                'created_by'          => $user->user_id,
                                'created_at'          => $date
                            )
                    );

                    $d['undertime']=0;
                    $d['night_diff']=0;
                    $ids[$counter]= $id;
                }

            }
        }

        $table=DB::table('employees_logs')->whereIn('id',$ids)->select()->get();
        return $table;
    }

    public function createDayAttendance($data){

        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();
        $ids = [];

        foreach ($data as $counter => $d) {
            DB::table('employee_logs2')->insertGetId(
                array(
                    'employee_id'         => $d['emp_id'],
                    'start_date'          => $d['date2'][0],
                    'end_date'            => $d['date2'][1],
                    'number_of_days'      => $d['days'],
                    'branch_id'           => $d['branch_id'],
                    'late'                => $d['undertime'],
                    'restday'             => $d['restday'],
                    'holiday_id'          => $d['holiday2'],
                    'holiday_ids'         => $d['holiday_ids'],
                    'overtime'            => $d['overtime'],
                    'night_differential'  => $d['night_diff'],
                    'created_by'          => $user->user_id,
                    'created_at'          => $date
                )
            );


            foreach ($d['date'] as $key => $da) {
               
                $date = $da;
                $dt = strtotime($date);
                $e = date("l", $dt);
                $days=strtolower($e);
                $holiday = 0;

                $emp_id = $d['emp_id'];
                $dat = $this->checkSA($emp_id,$days,$date);

                if ($d['holiday'] != null || count($d['holiday']) != 0) {
                    foreach ($d['holiday'] as $key => $ho) {
                       if ($ho['date'] == $date) {
                           $holiday = $ho['type'];
                        }
                    }
                }


                $id = DB::table('employees_logs')->insertGetId(
                        array(
                            'employee_id'         => $d['emp_id'],
                            'attendance_type_id'  => 1,
                            'time_in'             => $date .' '. $dat->time_in,
                            'time_out'            => $date .' '. $dat->time_out,
                            'lunch_in'            => $date .' '. $dat->break_start,
                            'lunch_out'           => $date .' '. $dat->break_end,
                            'late'                => $d['undertime'],
                            'overtime'            => $d['overtime'],
                            'branch_id'           => $d['branch_id'],
                            'holiday_id'          => $holiday,
                            'night_differential'  => $d['night_diff'],
                            'created_by'          => $user->user_id,
                            'created_at'          => $date
                        )
                    );

                $ids[$counter]= $id;
            }
        }

        $table=DB::table('employees_logs')->where('id',$ids)->select()->get();
        return $table;

    }

    public function show($id){

        return self::select(
            'employees_logs.id',
            'employees_logs.branch_id',
            'employees_logs.employee_id',
            'time_in', 
            'time_out', 
            'lunch_in', 
            'lunch_out', 
            'attendance_type_id',
            'employee_working_schedule.working_schedule_id',
            'employee_company.company_id')
        ->leftJoin('employee_company', 'employee_company.employee_id', '=', 'employees_logs.employee_id')
        ->leftJoin(DB::raw("(SELECT * FROM employee_working_schedule WHERE deleted_at IS NULL) AS employee_working_schedule"), 'employee_working_schedule.employee_id', '=', 'employees_logs.employee_id')
        ->where('employees_logs.id','=',$id)
        ->first();
    }

    public function getAttendanceNotDefaultbyId($id){

        $data = DB::table('employee_logs2')
        ->select(
            'id',
            'branch_id',
            'employee_id',
            'start_date', 
            'end_date', 
            'number_of_days', 
            'late', 
            'overtime', 
            'restday',
            'holiday_id',
            'holiday_ids',
            'night_differential')
        ->where('id','=',$id)
        ->first();

        return $data;
    }

    public function updateAttendanceNotDefault($id,$model){

        $d = $model;
        $user = $this->getUser();
        $date = date('Y-m-d H:i:s');

        $data = DB::table('employee_logs2')->where('id',$id)->update([
            'employee_id'       => $d['employee_id'][0],
            'number_of_days'    => $d['number_of_days'],
            'start_date'        => $d['start_date'],
            'end_date'          => $d['end_date'],
            'late'              => $d['late'],
            'overtime'          => $d['overtime'],
            'branch_id'         => $d['branch_id'],
            'holiday_id'        => $d['holiday_id'],
            'holiday_ids'       => $d['holiday_ids'],
            'restday'           => $d['restday'],
            'night_differential'=> $d['night_differential'],
            'updated_by'        => $user->user_id,
            'updated_at'        => $date
        ]);
        
        return $data;
    }

    public function getMyLogs(){

        $user = $this->getUser();

        $employee_id= $user->employee_id;

        return self::join('employee', 'employees_logs.employee_id', '=', 'employee.id')
        ->join('attendance_type', 'employees_logs.attendance_type_id', '=', 'attendance_type.id')
        ->join('employee_company', 'employees_logs.employee_id', '=', 'employee_company.employee_id')
        ->join('company_branch', 'employees_logs.branch_id', '=', 'company_branch.id')
        ->leftJoin('business_logs', 'employees_logs.id', '=', 'business_logs.employees_logs_id')
        ->leftJoin('business_logs_type', 'business_logs.type_id', '=', 'business_logs_type.id')
        ->select(
            DB::raw('attendance_type.name AS types'),
            DB::raw('company_branch.branch_name as location'),
            'employees_logs.id', 
            'employees_logs.employee_id', 
            'employees_logs.time_in', 
            'employees_logs.time_out',
            'employees_logs.lunch_in',
            'employees_logs.lunch_out',
            'business_logs.log_in',
            'business_logs.log_out',
            'business_logs_type.name as type',
            'business_logs.remarks'
        )
        ->where('employees_logs.deleted_at','=',null)
        ->where('employees_logs.employee_id','=',$employee_id)
        ->orderBy('employees_logs.time_in', 'desc')
        ->orderBy('business_logs.id', 'asc')
        ->limit(5)
        ->get();

    }

    public function duplicateLogIn($data){

        $date = $data['date'];
        $id = $data['employee_id'];

        return self::select(
            'employees_logs.id', 
            'employees_logs.employee_id', 
            'employees_logs.time_in', 
            'employees_logs.time_out')
        ->where('employees_logs.deleted_at','=',null)
        ->where('employees_logs.employee_id','=',$id)
        ->whereRaw(" CAST(employees_logs.time_in as date) = '$date'")
        ->first();
    }

    public function createLogIn($data,$type){

        $d = $data;
        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        $id = DB::table('employees_logs')->insertGetId(
                array(
                    'employee_id'         => $d['employee_id'],
                    'attendance_type_id'  => $type,
                    'time_in'             => $d['datetime'],
                    'overtime'            => 0,
                    'branch_id'           => $d['branch_id'],
                    'created_by'          => $user->user_id,
                    'created_at'          => $date
                )
            );

        $table=DB::table('employees_logs')->where('id',$id)->select()->get();
        return $table;
    }

    public function getTimeIn($data){

        $date = $data['date'];
        // $id = $data['employee_id'];
        $id = $data['id'];

        return self::select(
            'employees_logs.id', 
            'employees_logs.employee_id', 
            'employees_logs.time_in', 
            'employees_logs.time_out')
        ->where('employees_logs.deleted_at','=',null)
        ->where('employees_logs.id','=',$id)
        // ->where('employees_logs.employee_id','=',$id)
        // ->whereRaw(" CAST(employees_logs.time_in as date) = '$date'")
        ->first();
    }

    public function createLogOut($data){

        $d = $data;
        $user = $this->getUser();
        $date = date('Y-m-d H:i:s');
        $id=$d['id'];

        DB::table('employees_logs')->where('id',$id)->update([
            'employee_id'       => $d['emp_id'],
            'time_in'           => $d['time_in'],
            'time_out'          => $d['time_out'],
            'late'              => $d['late'],
            'undertime'         => $d['undertime'],
            'branch_id'         => $d['branch_id'],
            'holiday_id'        => $d['holiday_id'],
            'overtime'          => $d['overtime'],
            'halfday'           => $d['halfday'],
            'night_differential'=> $d['night_differential'],
            'updated_by'        => $user->user_id,
            'updated_at'        => $date
        ]);
        
        return $data;
    }

    public function identifyTypeEmpId($data,$val){

        $len = count($data);
        for ($i=0; $i < $len; $i++) { 
           
            $date = $data[$i]['date'];
            $id = $data[$i]['bio_id'];
            $deviceName = $data[$i]['device_name'];

            $dn = DB::table('biometrics_device')
            ->select('id','branch_id')
            ->where('deleted_at','=',null)
            ->where('name','=',$deviceName)
            ->first();

            $data[$i]['device_id'] = $dn->id;
            $data[$i]['branch_id'] = $dn->branch_id;

            $emp = DB::table('employee_biometrics_id')
            ->select( 
                'emp_id')
            ->where('deleted_at','=',null)
            ->where('bio_id','=',$id)
            ->where('device_id','=',$data[$i]['device_id'])
            ->first();

            $data[$i]['employee_id'] = 1;

            $cb = DB::table('employee_user')
            ->join('employee', 'employee_user.employee_id', '=', 'employee.id')
            ->select('employee_user.user_id')
            ->first();

            $data[$i]['created_by'] = 1;
            $emp_id =$data[$i]['employee_id'];

            if ($val == 1) {
                $dat = DB::table('employees_logs')
                ->select('id')
                ->where('deleted_at','=',null)
                ->where('employee_id','=',$emp_id)
                ->whereRaw(" CAST(time_in as date) = '$date'")
                ->first();

                if(count($dat) == 1){
                    $data[$i]['type'] = 'out';
                }else{
                    $data[$i]['type'] = 'in';
                }
            }
        }

        return $data;
    }

    public function getBioTimeIn($data){

        $date = $data['date'];
        $id = $data['employee_id'];

        return self::select(
            'employees_logs.id', 
            'employees_logs.employee_id', 
            'employees_logs.time_in', 
            'employees_logs.time_out')
        ->where('employees_logs.deleted_at','=',null)
        ->where('employees_logs.employee_id','=',$id)
        ->whereRaw(" CAST(employees_logs.time_in as date) = '$date'")
        ->first();
    }

    public function checkTypeOfLog($data){
        $date = $data['date'];
        $emp_id =$data['employee_id'];

        $dat = DB::table('employees_logs')
                ->select('id')
                ->where('deleted_at','=',null)
                ->where('employee_id','=',$emp_id)
                ->whereRaw(" CAST(time_in as date) = '$date'")
                ->first();
                
        return $dat;
    }

    public function fetchDataFromBioTable(){
        $date = date('Y-m-d H:i:s');

        $dat = DB::table('temp_stor_fr_bio')
                ->select('bio_id','device_name','uid','type','date','time','date_time')
                ->where('deleted_at','=',null)
                ->get();

        DB::table('temp_stor_fr_bio')
        ->where('deleted_at', '=', null)
        ->update([
            'deleted_by' => 1,
            'deleted_at' => $date
        ]);
                
        return $dat;
    }

    public function updateLogs($data){

        $user = $this->getUser();
        $d = $data;
        $date = date('Y-m-d H:i:s');
        $id=$d['id'];

        if ($data['type'] ==  'lunchIn') {
            DB::table('employees_logs')->where('id',$id)->update([
                'lunch_in'         => $d['datetime'],
                'updated_by'       => $user->user_id,
                'updated_at'       => $date
            ]);
        }
        else if($data['type'] ==  'lunchOut'){
            DB::table('employees_logs')->where('id',$id)->update([
                'lunch_out'         => $d['datetime'],
                'updated_by'        => $user->user_id,
                'updated_at'        => $date
            ]);
        }
        else if($data['type'] ==  'meetingIn'){
            DB::table('business_logs')->insert(
                array(
                    'employees_logs_id' => $id,
                    'remarks'           => $d['remarks'],
                    'log_in'            => $d['datetime'],
                    'type_id'           => 1,
                    'created_by'        => $user->user_id,
                    'created_at'        => $date
                    )
            );
        }
        else if($data['type'] ==  'meetingOut'){
            DB::table('business_logs')->where('employees_logs_id',$id)->update([
                'log_out'         => $d['datetime'],
                'updated_by'      => $user->user_id,
                'updated_at'      => $date
            ]);
        }
        else if($data['type'] ==  'seminarIn'){
            DB::table('business_logs')->insert(
                array(
                    'employees_logs_id' => $id,
                    'remarks'           => $d['remarks'],
                    'log_in'            => $d['datetime'],
                    'type_id'           => 2,
                    'created_by'        => $user->user_id,
                    'created_at'        => $date
                    )
            );
        }
        else if($data['type'] ==  'seminarOut'){
            DB::table('business_logs')->where('employees_logs_id',$id)->update([
                'log_out'         => $d['datetime'],
                'updated_by'      => $user->user_id,
                'updated_at'      => $date
            ]);
        }
        else if($data['type'] ==  'outOffice'){
            DB::table('business_logs')->insert(
                array(
                    'employees_logs_id' => $id,
                    'remarks'           => $d['remarks'],
                    'log_in'            => $d['datetime'],
                    'type_id'           => 3,
                    'created_by'        => $user->user_id,
                    'created_at'        => $date
                    )
            );
        }
        else if($data['type'] ==  'inOffice'){
            DB::table('business_logs')->where('employees_logs_id',$id)->update([
                'log_out'         => $d['datetime'],
                'updated_by'      => $user->user_id,
                'updated_at'      => $date
            ]);
        }

        return $data;
    }

    public function validateEmpId($id){

        $temp = array_unique($id);
        $new_id = array_values($temp);
        $not_exist = [];

        foreach ($new_id as $d) {
            $val = DB::table('employee')
                ->select('id')
                ->where('id','=',$d)
                ->where('deleted_at','=',null)
                ->where('deleted_by','=',null)
                ->first();

            if (count($val) == 0) {
                $not_exist[] = $d;
            }
            
        }

        return $not_exist;

    }

    public function logsValidation($ip_address,$mac_address,$ddns){ 

        $user = $this->getUser();

        $data = DB::table('user_time_setting')
            ->select('id')
            // ->where('ip_address','=',$ip_address)
            // ->where('ddns','=',$ddns)
            // ->where('mac_address','=',$mac_address)
            ->where('employee_id','=',$user->employee_id)
            ->get();

        return $data;

    }

    public function getEmployeeRate($data){
        $val = DB::table('employee_rate')
            ->join('employee', 'employee_rate.employee_id', '=', 'employee.id')
            ->select(
                'employee_rate.employee_id',
                'employee_rate.daily_rate',
                'employee_rate.allowance_amount',
                'employee.e_cola'
            )
            ->whereIn('employee_rate.employee_id',$data)
            ->where('employee_rate.deleted_at','=',null)
            ->where('employee_rate.deleted_by','=',null)
            ->where('employee.deleted_at','=',null)
            ->where('employee.deleted_by','=',null)
            ->get();
        return $val;
    }

    public function updateRate($data){ 

        $user = $this->getUser();
        $date = date('Y-m-d H:i:s');
        $created_by = $user->user_id;

        for ($i=0; $i < count($data); $i++) { 

            $emp_id = $data[$i]['emp_id'];
            $allowance = $data[$i]['allowance'];
            $daily_rate = $data[$i]['daily_rate'];
            $e_cola = $data[$i]['e_cola'];

            DB::table('employee_rate')->where('employee_id',$emp_id)->update([
                'daily_rate'        => $daily_rate,
                'allowance_amount'  => $allowance,
                'updated_by'        => $created_by,
                'updated_at'        => $date
            ]);

            DB::table('employee')->where('id',$emp_id)->update([
                'e_cola'        => $e_cola,
                'updated_by'    => $created_by,
                'updated_at'    => $date
            ]);
        }
    }

    public function archiveAttendanceNotDefault($id){

        $user = $this->getUser();
        $date = date('Y-m-d H:i:s');

        $data = DB::table('employee_logs2')->where('id',$id)->update([
            'deleted_by'        => $user->user_id,
            'deleted_at'        => $date
        ]);
        
        return $data;
    }

    public function createAttendanceFromBioFile($data){
        
        $user = $this->getUser();
        $ids=[];
        $upload_ids = [];
        $type = 3;
        $date = date('Y-m-d H:i:s');
        $branch_id = null;
        foreach ($data as $counter => $d) {
            
            $branch = $this->getEmployeeBranch($d['emp_id']);
            if ($branch == null) {
                $branch_id = 0;
            }else{
                $branch_id = $branch->branch_id;
            }

            $id = DB::table('employees_logs')->insertGetId(
                    array(
                        'employee_id'         => $d['emp_id'],
                        'attendance_type_id'  => $type,
                        'time_in'             => $d['time_in'],
                        'time_out'            => $d['time_out'],
                        'lunch_in'            => $d['lunch_in'],
                        'lunch_out'           => $d['lunch_out'],
                        'late'                => $d['late'],
                        'undertime'           => $d['undertime'],
                        'overtime'            => $d['overtime'],
                        'halfday'             => $d['halfday'],
                        'holiday_id'          => $d['holiday_id'],
                        'night_differential'  => $d['night_differential'],
                        'branch_id'           => $branch_id,
                        'created_by'          => $user->user_id,
                        'created_at'          => $date
                        )
                );
            $ids[$counter] = $id;
        }

        // upload history logs...
        
        

        $table = DB::table('employees_logs')->select('id')->whereIn('id',$ids)->select()->get();



        return $table;
    }

    public function createLoginOrLogout($d,$type){

        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();
        $branch = $this->getEmployeeBranch($d["id"]);
        $branch_id = null;
        if ($branch == null) {
            $branch_id = 0;
        }else{
            $branch_id = $branch->branch_id;
        }
        $id = null;

        if ($type == 'in') {
            $datetime  = $d["date"].' '.$d["in"];
            if (isset($d['lunch_in'])) {
                $lunch_in  = ($d['lunch_in'] != null ? $d["date"].' '.$d["lunch_in"] : null);
            } else {
                $lunch_in = null;
            }
            if (isset($d['lunch_out'])) {
                $lunch_out = ($d['lunch_out'] != null ? $d["date"].' '.$d["lunch_out"] : null);
            } else {
                $lunch_out = null;
            }

            $id = DB::table('employees_logs')->insertGetId(
                array(
                    'employee_id'         => $d['id'],
                    'attendance_type_id'  => 3,
                    'time_in'             => $datetime,
                    'lunch_in'            => $lunch_in,
                    'lunch_out'           => $lunch_out,
                    'overtime'            => 0,
                    'branch_id'           => $branch_id,
                    'created_by'          => $user->user_id,
                    'created_at'          => $date
                )
            );
        }else if($type == 'out'){
            $datetime = $d["date"].' '.$d["out"];
            if (isset($d['lunch_in'])) {
                $lunch_in  = ($d['lunch_in'] != null ? $d["date"].' '.$d["lunch_in"] : null);
            } else {
                $lunch_in = null;
            }
            if (isset($d['lunch_out'])) {
                $lunch_out = ($d['lunch_out'] != null ? $d["date"].' '.$d["lunch_out"] : null);
            } else {
                $lunch_out = null;
            }
            $id = DB::table('employees_logs')->insertGetId(
                array(
                    'employee_id'         => $d['id'],
                    'attendance_type_id'  => 3,
                    'lunch_in'            => $lunch_in,
                    'lunch_out'           => $lunch_out,
                    'time_out'            => $datetime,
                    'overtime'            => 0,
                    'branch_id'           => $branch_id,
                    'created_by'          => $user->user_id,
                    'created_at'          => $date
                )
            );
        }
        

        $table = DB::table('employees_logs')->select('id')->where('id',$id)->select()->get();

        return $table;
    }

    public function getAttendanceByIdAndLoginLogout($d){
        $date = $d['date'];
        $emp_id = $d['emp_id'];

        return self::whereRaw("(CAST(time_in AS date) = '$date' or CAST(time_out AS date)= '$date')")
        ->select(
            'id',
            'time_out',
            'time_in', 
            DB::raw("date_format(time_out,'%H:%i:%s') as end_time"), 
            DB::raw("date_format(time_in,'%H:%i:%s') as start_time")
        )
        ->where('employee_id',$emp_id)
        ->first();
    }

    public function updateAttendanceFromApproveCOA($d){

        $updated_at = date('Y-m-d H:i:s');
        $user = $this->getUser();
        $id = $d['employees_logs_id'];

        DB::table('employees_logs')
            ->where('id',$id)
            ->update([
                'time_in'             => $d['time_in'],
                'time_out'            => $d['time_out'],
                'late'                => $d['late'],
                'undertime'           => $d['undertime'],
                'overtime'            => $d['overtime'],
                'halfday'             => $d['halfday'],
                'holiday_id'          => $d['holiday_id'],
                'night_differential'  => $d['night_differential'],
                'updated_by'          => $user->user_id,
                'updated_at'          => $updated_at,
            ]);
    }

    public function updateAttendanceFromApproveOB($d){

        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();
        $type = 5 ;

        $id = DB::table('employees_logs')->insertGetId(
                array(
                    'employee_id'         => $d['emp_id'],
                    'attendance_type_id'  => $type,
                    'time_in'             => $d['time_in'],
                    'time_out'            => $d['time_out'],
                    // 'lunch_in'            => $d['lunch_in'],
                    // 'lunch_out'           => $d['lunch_out'],
                    'late'                => $d['late'],
                    'undertime'           => $d['undertime'],
                    'overtime'            => $d['overtime'],
                    'halfday'             => $d['halfday'],
                    'holiday_id'          => $d['holiday_id'],
                    'night_differential'  => $d['night_differential'],
                    'branch_id'           => $d['branch_id'],
                    'created_by'          => $user->user_id,
                    'created_at'          => $date
                    )
            );

        $table=DB::table('employees_logs')->where('id',$id)->select()->get();
        return $table;
    }

    public function validateCredentialOfCreator($id){

        $user = $this->getUser();
        $role_id = $user->role_id;
        $temp = array_unique($id);
        $new_id = array_values($temp);
        $not_empl = [];
        $data = null;

        foreach ($new_id as $d) {
            $tr = DB::table('employee')
                ->select('id')
                ->where('id','=',$d)
                ->where('deleted_at','=',null)
                ->where('deleted_by','=',null);

            if($role_id > 2 AND $role_id < 0) {
                $tr->where('supervisor_id', 'LIKE', '%' . $user->user_id . '%');
            }
            $val = $tr->first();

            if (count($val) == 0) {
                $not_empl[] = $d;
            }
        }

        $data = DB::table('employee')
                ->select(DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'))
                ->whereIn('id',$not_empl)
                ->where('deleted_at','=',null)
                ->where('deleted_by','=',null)
                ->get();

        return $data;
    }

    public function getUploadHistoryID($data) {
        $uploaded_ids = [];
        $user = $this->getUser();

        foreach ($data as $ids) {
            $uploaded_ids[] = $ids->id;
        }
        
        $uploaded_ids = implode(',', $uploaded_ids);

        if (count($uploaded_ids)) {
            $uploaded_ids = DB::table('upload_history')->insert([
                ['data' => $uploaded_ids, 'source' => 'attendace_upload', 'created_by' => $user->user_id, 'created_at' => date('Y-m-d H:i:s') ]
            ]);
        }

        return $data;            
    }


    public function getUploadHistoryFromAttendance() {

        $data = DB::table('upload_history')
            ->select('id', 'created_at', 'data')
            ->where('source', '=', 'attendace_upload')
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'asc')
            ->get();

        return $data;
    }

    public function deleteAttendanceByBatch($id) {
        // getUploadHistoryID
        $ids = DB::table('upload_history')
            ->select('data')
            ->where('source', '=', 'attendace_upload')
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'asc')
            ->where('id', "=", $id)
            ->pluck('data')
            ->first();
            
        $ids = explode(',', $ids);

        if (isset($id)) {
            DB::table('upload_history')->where('id', $id )->update([ 'deleted_at' => date('Y-m-d h:i:s'), 'deleted_by' => 1 ]);
        }
        if (is_array($ids)) {
            DB::table('employees_logs')->whereIn('id', $ids)->update([ 'deleted_at' => date('Y-m-d h:i:s'), 'deleted_by' => 1 ]);
        } 

        return $ids;
    }
              

    public function getWorkingScheduleListPerCompany($company_id) {
        $schedules = DB::table('working_schedule')
                        ->select('id', DB::raw('CONCAT(time_in, " - ",time_out, " | ", IFNULL(lunch_break_time, "N/A"), " - ", IFNULL(lunch_mins_break, "N/A"), " mins") AS `text`'), 'company_id', 'time_in', 'time_out', 'lunch_break_time', 'lunch_mins_break')
                        ->whereNull('deleted_at')
                        ->where('company_id', $company_id)
                        ->oldest('time_in')
                        ->get();

        return $schedules;
    }


    public function getWorkingScheduleListById($id) {
        $schedule = DB::table('working_schedule')
                        ->select('working_schedule.time_in', 'working_schedule.time_out', 'working_schedule.lunch_break_time', 'working_schedule.lunch_mins_break')
                        ->whereNull('working_schedule.deleted_at')
                        ->where('working_schedule.id', $id)
                        ->first();
        $data[] = array(
            'time_in' => $schedule->time_in,
            'time_out' => $schedule->time_out,
            'lunch_in' => $schedule->lunch_break_time,
            'lunch_out' => $schedule->lunch_mins_break,
            'restday' => 0,
            'overtime' => 0
        );
    
        return $data;
    }

    public function getScheduleId($employee_id) {
        
        $schedule_id = DB::table('employee_working_schedule')
                        ->select('id')
                        ->where('employee_id', $employee_id)
                        ->whereNull('deleted_at')
                        ->first();

        return $schedule_id;
        
    }

}
