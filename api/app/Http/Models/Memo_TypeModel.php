<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class Memo_TypeModel extends Model
{
    protected $table = "memo_type"; use SoftDeletes;
}
