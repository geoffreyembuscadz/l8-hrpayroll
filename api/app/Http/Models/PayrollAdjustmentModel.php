<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;

class PayrollAdjustmentModel extends Model
{
    protected $table = "payroll_adjustment"; use SoftDeletes;
}
