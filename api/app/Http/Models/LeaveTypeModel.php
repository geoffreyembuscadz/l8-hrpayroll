<?php
namespace App\Http\Models;
use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveTypeModel extends Model
{
    protected $table = "leave_type"; use SoftDeletes;

    public static function onLeave($date){
    	
            $onLeave = DB::table('leave')
                 ->join('status_type', 'leave.status_id', '=', 'status_type.id')
                 ->select(DB::raw('Count(leave.id) AS emp_on_leave'))
                 ->where('status_type.id', '=', '4')
                 ->where('date_from', '=', $date)
                 ->orWhere('date_to', '=', $date)
                 ->get();
            return $onLeave;
    }
    public function getList(){
        $data=DB::table('leave_type')
            ->select(
               'leave_type.id', 
               'leave_type.name', 
               'leave_type.description'
               )
            ->where('deleted_at','=',null)
            ->get();
        return $data;
    }
}
