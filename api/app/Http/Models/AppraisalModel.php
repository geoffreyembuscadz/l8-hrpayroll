<?php
namespace App\Http\Models;
use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Http\Request;
use App\Http\Models\Arrays;
use Carbon\Carbon;
use \stdClass;

class AppraisalModel extends Model
{
    protected $table = "appraisal"; use SoftDeletes;

    public function appraisal($data){
        $employee_id = $data['employee_id'];

    	return $query = DB::table('appraisal AS a')
    					->select('a.id','a.objective', 'a.achieve_obj', 'a.due', 'a.implement', 's.name As status')
                        ->leftJoin('status_type As s', 'a.status', '=', 's.id')
                        ->where('a.employee_id', '=', $employee_id)
    					->where('a.deleted_by', '=',  NULL)
    					->get();
    }

    public function updateObj($file){
        $d = $file;
        $date = date('Y-m-d H:i:s');
        $id = $d['id'];
        $data = [];

        DB::table('appraisal')
        ->where('id',$id)->update([
        	'emp_id'        => $d['implement'],
        	'date'          => $d['objective'],
        	'start_time'    => $d['achieve_obj'],
        	'end_time'      => $d['due'],
        	'updated_by'    => $d['created_by'],
        	'updated_at'    => $date
        ]);

        return $data;
    }


        public static function getEmployeeByAdmin(){

            return $query = DB::table('employee AS e')
                    ->join('position As p', 'e.position', '=', 'p.id')
                    ->select('e.id', 'p.name As position', 'e.date_hired', 'e.employment_status AS status',
                        DB::raw('CONCAT(e.firstname, " ",e.lastname) AS `employee_name`')
                        )
                    ->get();

        }

        public function getObjNow($data){
            $employee_id = $data['employee_id'];
            $date = date('Y-m-d');

            $query = DB::table('appraisal AS a')
                    ->select('a.id', 'a.implement', 'a.objective', 'a.achieve_obj', 'a.due',  'o_s.rate_value', 'o_s.feedback') 
                    ->join('obj_rate As o_s', 'a.id', '=', 'o_s.obj_id', 'left')
                    ->whereDate('a.due',  $date)
                    ->where('a.employee_id', '=', $employee_id);
            
            return $query->get();
        }

        public function rateObj($data){
            $id = $data['id'];
            $employee_id = $data['employee_id'];
            $date = date('Y-m-d');

            $query = DB::table('appraisal As a')
                    ->select('a.id', 'a.implement', 'a.objective', 'a.achieve_obj', 'a.due', 'o_s.rate_value', 'o_s.feedback') 
                    ->join('obj_rate As o_s', 'a.id', '=', 'o_s.obj_id', 'left')
                    ->whereDate('a.due',  $date)
                    ->where('a.employee_id', '=', $employee_id)
                    ->where('a.id', '=', $id)
                    ->where('a.status', '=', 4)
                    ->get();

            return $query;

        }

    public function saveRateObj($data){
        $date = date('Y-m-d H:i:s');
        $created_by = 1;

        $atabs =
        DB::table('obj_rate')->insertGetId
        ( array
            (   
                'obj_id'        => $data['obj_id'],
                'employee_id'   => $data['employee_id'],
                'rate_value'    => $data['rate_value'],
                'feedback'      => $data['feedback'],
                'created_by'    => $created_by,
                'created_at'    => $date
            )
        );

        $notif_id=DB::table('notification')->insertGetId(
                array(
                    'title'          => 'Appraisal',
                    'description'    => 'rate objectives',
                    'messenger_id'   => $created_by,
                    'table_from'     => 'obj_rate',
                    'table_from_id'  => $atabs,
                    'seen'           => 0,
                    'route'          => 'objective-approval',
                    'receiver_id'    => $data['employee_id'],
                    'icon'           => 'fa fa-check-square-o text-aqua',
                    'created_by'     => $created_by,
                    'created_at'     => $date
                    )
        );
        $notif[]= $notif_id;
        return $notif;

    }

    public function viewRate($data){
        $employee_id = $data['employee_id'];

        $query = DB::table('obj_rate As o_r')
                ->select('o_r.id', 'o_r.rate_value', 'o_r.feedback', 
                    'a.implement', 'a.objective', 'a.achieve_obj', 'a.due')
                ->join('appraisal As a', 'o_r.obj_id', '=', 'a.id')
                ->where('o_r.employee_id', '=', $employee_id)
                ->get();
        return $query;

    }

    public function pendingAppraisal($data){
        $employee_id = $data['employee_id'];

        return $query = DB::table('appraisal AS a')
                        ->select('a.id','a.objective', 'a.achieve_obj', 'a.due', 'a.implement', 's.name As status')
                        ->leftJoin('status_type As s', 'a.status', '=', 's.id')
                        ->where('a.employee_id', '=', $employee_id)
                        ->where('a.deleted_by', '=',  NULL)
                        ->where('a.status', '=', '1')
                        ->get();
    }

}
