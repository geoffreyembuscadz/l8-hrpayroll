<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class TripModel extends Model
{
    protected $table = "trip"; use SoftDeletes;
}
