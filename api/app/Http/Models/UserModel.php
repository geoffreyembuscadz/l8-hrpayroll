<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

use App\Http\Models\Role;
use App\User;
use Auth;

use \DB AS DB;

class UserModel extends Model
{
    protected $table = "users"; 

    use SoftDeletes;


    public function preListJoin($model) {
    	return $model->join('role_user', 'users.id', '=', 'role_user.user_id');
    }

    public function postListJoin($model) {
    	return $model->select('users.id', 'users.name', 'users.email', 'role_user.role_id');
    }

    // get all the user data 
    // left join roles and role_user table
    public function getList() {
        // hide the superAdmin from the query using skip and limit
        $count = self::count();
        $skip = 1;
        $limit = $count - $skip;
    	$query = 
            self::leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
    		->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
        ->leftJoin('users_company', 'users_company.user_id', '=', 'users.id')
        ->leftJoin('company', 'company.id', '=', 'users_company.company_id')
    		->select('users.id', 
    				 'users.name', 
    				 DB::raw('GROUP_CONCAT(\' \',roles.name) as role_name'), 
             'company.name as company_name',
    				 'users.email')
    		->groupBy('users.id')
    		->groupBy('users.name')
    		->groupBy('users.email')
            ->skip($skip)
            ->take($limit)
    		->get();

    	return $query;
    }

    // left join roles, role_user, permissions, permission_role, role_module, role_menu, employee_user, employee
    public function getListById($id) {
          $query = 
              self::leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
              ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
              ->leftJoin('permission_role', 'roles.id', '=', 'permission_role.role_id')
              ->leftJoin('permissions', 'permission_role.permission_id', '=', 'permissions.id')
              ->leftJoin('role_module', 'roles.id', '=', 'role_module.role_id')
              ->leftJoin('role_menu', 'roles.id', '=', 'role_menu.role_id')
              ->leftJoin('employee_user', 'users.id', '=', 'employee_user.user_id')
              ->leftJoin('employee', 'employee_user.employee_id', '=', 'employee.id')
              ->select('users.id',
                       'users.name',
                       DB::raw('GROUP_CONCAT(DISTINCT roles.id) as role_id'),
                       DB::raw('GROUP_CONCAT(DISTINCT permissions.id) as permission_id'),
                       DB::raw('GROUP_CONCAT(DISTINCT role_module.module_id) as role_module'),
                       DB::raw('GROUP_CONCAT(DISTINCT role_menu.menu_id) as role_menu'), 
                       'users.email',
                       'roles.name AS role_name',
                       'employee.id as employee_id',
                       'employee.supervisor_id as supervisor_id',
                       'employee_user.employee_id as employee_user')
              ->where('users.id', '=', $id)
              ->groupBy('users.id')
              ->groupBy('employee.employee_code')
              ->groupBy('users.name')
              ->groupBy('users.email')
              ->groupBy('employee_user.employee_id')
              ->first();
          return $query;
    }

    public function getListByIdLimited($id) {
          $query = 
              self::leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
              ->leftJoin('employee_user', 'users.id', '=', 'employee_user.user_id')
              ->leftJoin('employee', 'employee_user.employee_id', '=', 'employee.id')
              ->select('users.id as user_id',
                       'role_user.role_id',
                       'employee.id as employee_id',
                       'employee.supervisor_id')
              ->where('users.id', '=', $id)
              ->first();
          return $query;
    }

    public function getUserId($id) {
        $query = 
            self::leftJoin('employee_user', 'users.id', '=', 'employee_user.user_id')
            ->leftJoin('employee', 'employee_user.employee_id', '=', 'employee.id')
            ->select('users.id as user_id','employee.branch as branch_id')
            ->where('employee.id', '=', $id)
            ->first();
            
        return $query;
    }

    public function getEmployeeBranch($id) {
        $query = 
            DB::table('employee')
            ->select('branch as branch_id')
            ->where('id', '=', $id)
            ->first();
            
        return $query;
    }

    public function getPermissionId($id) {

      $query = 
              self::leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
              ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
              ->leftJoin('permission_role', 'roles.id', '=', 'permission_role.role_id')
              ->leftJoin('permissions', 'permission_role.permission_id', '=', 'permissions.id')
              ->select(
                DB::raw('GROUP_CONCAT(DISTINCT permissions.id) as permission_id')
              )
              ->where('users.id', '=', $id)
              ->groupBy('users.id')
              ->first();

      $query = explode(",",$query->permission_id);

      return $query;
    }



    // preStore only change the password to a hash value
    // postStore save the role_ids 
    public function postStore($id, $data) {
          $user = User::find($id);

          $role_ids = $data['data']['role_id']; // return as array or null

          DB::table('users_company')->insert(
            array(
              'user_id' => $id,
              'company_id' => $data['data']['company'],
              'created_at'  => date('Y-m-d H:i:s'),
              'created_by'  => Auth::User()->id

            )
          );

          if ($role_ids == null) {
              return $user;
          } else {
              // user foreach if multiple role
              // foreach ($role_ids as $role_id) {
              //     $user->attachRole($role_id);
              // }

              // attachRole is a function from Zizarco
              $user->attachRole($role_ids);

              return $data;
          }


    }


    // check if there's a data from $data['data']['passwords']['password'] 
    // AND check if the $data['data']['passwords']['password'] is NOT HASH
    // AND check if the $data['data']['passwords']['password'] is BLANK
    // THEN the password will be a new password
    // ELSE do not send $data['data']['passwords'] so it will not be updated
    public function preUpdate($id, $data) {
          // hash password
          if (isset($data['data']['passwords']['password']) 
              && !Hash::check($data['data']['passwords']['password'], User::find($id)->password)

              && !Hash::check('', $data['data']['passwords']['password'])) {

             $data['data']['password'] = Hash::make($data['data']['passwords']['password']);
             return $data;

          } else {
             unset($data['data']['password']);
             return $data;
          }
          
    }

    public function postUpdate($id, $data) {
          $user = User::find($id);

          if (isset($data['data']['company'])) {
            DB::table('users_company')->where('user_id', $id)->update(
              array(
                'company_id' => $data['data']['company'],
                'updated_at'  => date('Y-m-d H:i:s'),
                'updated_by'  => Auth::User()->id

              )
            );
          }

          $role_ids = explode(" ",$data['data']['role_id']); // return as array or null
      
          if ($role_ids == null) {
               // if the role_ids is null
               // remove all the existing role_ids from role_user
               $user->roles()->sync([]);
          } else {
              foreach ($role_ids as $role_id) {
                // add the newly submitted role_ids from form submit
                $user->roles()->sync($role_ids);
              }
              return $data;
          }
    }

    public function deleteUsersCompany($id) {
        DB::table('users_company')->where('user_id', $id)->update(
            array(
              'deleted_at'  => date('Y-m-d H:i:s'),
              'deleted_by'  => Auth::User()->id
            )
          );
    }


    public function changePassword($newPassword, $current_password) {
      // get the user current password
      $my_password = Auth::User()->password;
      // check if the current_password and my_password is equals           
      if(Hash::check($current_password, $my_password)) {           
        $user_id            = Auth::User()->id;                       
        $obj_user           = User::find($user_id);
        $obj_user->password = Hash::make($newPassword);;
        $obj_user->save(); 
        return $obj_user;
      }  else {           
        $error = array('current-password' => 'Please enter correct current password');
        return response()->json(array('error' => $error), 400);   
      }  
    }


}
