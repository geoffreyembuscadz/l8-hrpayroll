<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;


use App\Http\Models\PayslipExportModel as PayslipExport;
use App\Http\Models\PayrollListDataModel as PayrollList;
use App\Http\Models\AdjustmentModel as Adjustment;
use App\Http\Models\PayrollModel as Payroll;
use App\Http\Models\LoanModel as Loan;

use Carbon\Carbon;
use \DB as DB;

class PayslipExportModel extends Model
{


    protected $table = "payroll"; 

    use SoftDeletes;

    public function getPayroll($id) {
        $payroll = new Payroll;
        $payroll = DB::table('payroll')
                      ->whereNull('deleted_at')
                      ->where('id', '=', $id)
                      ->first();
        return $payroll;
    }

    public function getPayrollList($id, $emp_id) {
        $payrollList = new PayrollList;
        $payrollList = DB::table('payroll_list')
                         ->select('payroll_list.*', 
                            'employee.email', 
                            'company.name AS company', 
                            'company_branch.branch_name AS branch', 
                            'employee.firstname', 
                            'payroll.start_time AS start', 
                            'payroll.end_time AS end', 
                            'department.name AS department_name', 
                            'employee.employment_status', 
                            'employee.starting_date', 
                            'employee.employee_code', 
                            'employee.bank_account_number', 
                            DB::raw("(SELECT count(*) FROM payroll_list WHERE payroll_id = '$id') as count"),
                            'employee.sick_leave_credit',
                            'employee.use_sick_leave_credit', 
                            'employee.vacation_leave_credit',
                            'employee.use_vacation_leave_credit', 
                            'employee.emergency_leave_credit', 
                            'employee.use_emergency_leave_credit', 
                            'employee.maternity_leave_credit',
                            'employee.use_maternity_leave_credit', 
                            'employee.others_leave_credit', 
                            'employee.use_others_leave_credit',
                            'position.name AS position_name'
                        )
                         ->whereNull('payroll_list.deleted_at')
                         ->where('payroll_id', '=', $id)
                         ->join('employee_company', 'payroll_list.emp_id', '=', 'employee_company.employee_id')
                         ->join('company', 'employee_company.company_id', '=', 'company.id')
                         ->join('employee', 'payroll_list.emp_id', '=', 'employee.id')
                         ->join('payroll', 'payroll_list.payroll_id', '=', 'payroll.id')
                         ->join('company_branch', 'employee.branch', '=', 'company_branch.id')
                         ->join('position', 'employee.position', '=', 'position.id')
                         ->leftJoin('department', 'employee.department', '=', 'department.id');
        /**
         * 
         * If emp_id is not empty add this condition on the $payrollList
         * This is for the email for payslip
         * 
         */
        if ($emp_id != '' || $emp_id != NULL) {
          $payrollList->where('emp_id', '=', $emp_id);
        }

        $payrollList = $payrollList->get();

        return $payrollList;

    }


    public function getAdjustment($id) {
        $adjustment = new Adjustment;
        $adjustment = DB::table('payroll_adjustment')
                      ->select(
                        'payroll_adjustment.id', 
                        'payroll_adjustment.adjustment_type', 
                        'payroll_adjustment.employee_id', 
                        'adjustment_type.display_name as display_name', 
                        DB::raw("(select if(adjustment_type.taxable = 1, 'Yes', 'No' )) AS taxable"),
                        DB::raw("(select if(adjustment_type.billable = 1, 'Yes', 'No' )) AS billable"),
                        'adjustment_type.type as adjustment_type', 
                        'payroll_adjustment.type', 
                        'payroll_adjustment.created_at',
                        'payroll_adjustment.adjustment_amount'

                        )
                      ->join('adjustment_type', 'payroll_adjustment.adjustment_type', '=', 'adjustment_type.id')
                      ->whereNull('payroll_adjustment.deleted_at')
                      ->where('adjustment_type.include_payslip', 1)
                      ->where('payroll_id', '=', $id)
                      ->get();

        return $adjustment;
    }

    public function getLoan($id) { 
        $loans = new Loan;
        $loans = DB::table('payroll_loan')
                ->select(
                    'payroll_loan.id', 
                    'payroll_loan.loan_type', 
                    'payroll_loan.employee_id', 
                    'loan_type.display_name', 
                    'loan_type.type as loan_type', 
                    'payroll_loan.loan_amount', 
                    'payroll_loan.type', 
                    'payroll_loan.created_at'
                )
                ->join('loan_type', 'payroll_loan.loan_type', '=', 'loan_type.id')
                ->whereNull('payroll_loan.deleted_at')
                ->where('payroll_id', '=', $id)
                ->where('loan_type.include_payslip', '=', 1)
                ->get();

        return $loans;
    }

    public function sendPayslip($id) {

        $payroll = new PayrollList;
        $payroll = DB::table('payroll_list')
                         ->select('payroll_list.emp_id AS employee_id','payroll_list.payroll_id AS payroll_id', 'employee.email', 'company.name AS company', 'company_branch.branch_name AS branch', 'employee.firstname', 'payroll.start_time AS start', 'payroll.end_time AS end')
                         ->whereNull('payroll_list.deleted_at')
                         ->join('employee_company', 'payroll_list.emp_id', '=', 'employee_company.employee_id')
                         ->join('company', 'employee_company.company_id', '=', 'company.id')
                         ->join('employee', 'payroll_list.emp_id', '=', 'employee.id')
                         ->join('company_branch', 'employee.branch', '=', 'company_branch.id')
                         ->join('payroll', 'payroll_list.payroll_id', '=', 'payroll.id')
                         ->where('payroll_id', '=', $id)
                         ->get();

        return $payroll;
    }
    
}
