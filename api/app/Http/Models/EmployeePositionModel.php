<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeePositionModel extends Model
{
    protected $table = "employee_position"; use SoftDeletes;

 public function getRecords(){
        return self::select('employee_position.department_id', 'employee_position.position')->
        where('position', '=', 'maintenance')->get();
    }
}