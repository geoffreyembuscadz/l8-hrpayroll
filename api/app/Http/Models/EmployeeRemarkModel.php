<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeRemarkModel extends Model{
    protected $table = "employee_remarks"; use SoftDeletes;
}
