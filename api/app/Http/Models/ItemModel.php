<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class ItemModel extends Model
{
    protected $table = "item"; use SoftDeletes;
}
