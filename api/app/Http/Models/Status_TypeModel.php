<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class Status_TypeModel extends Model
{
    protected $table = "status_type"; use SoftDeletes;
}
