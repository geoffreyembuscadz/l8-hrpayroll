<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyPolicyModel extends Model
{
    protected $table = "company_policy"; use SoftDeletes;

    public function joinCompanyPolicyType($model){
    	return $model->join('company_policy_type', 'company_policy_type.id', '=', 'company_policy.company_policy_type_id');
    }

    public function functionSelect($model, $data = []){
    	if(isset($data['company_id'])){
    		$model->where('company_policy.company_id', $data['company_id']);
    	}

    	return $model->select('company_policy.id', 'company_policy.name', 'company_policy.default_value', 'company_policy.start_value', 'company_policy.end_value', 'company_policy_type.name AS company_policy_type');
    }
}
