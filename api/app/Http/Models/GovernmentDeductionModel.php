<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use \DB;

class GovernmentDeductionModel extends Model
{
   protected $table = 'philhealth';

   public static function computeThisDeduction($basic_pay){
   	$data = [
   		'philhealth' => self::computePhilhealth($basic_pay),
         'sss' => self::computeSSS($basic_pay),
   		'pagibig' => 100.00
   	];
   	
   	return $data;


   }

    public static function computePhilhealth($basicPay){
      
      $philhealth = self::select('employee_share')
         ->where('salary_base','<=',$basicPay)
         ->where('salary_ceiling','>=',$basicPay)
         ->first();
         
         return $philhealth;
    }

    public static function computeSSS($basicPay){

      $sss = DB::select('select ee from sss where salary_base <= '. $basicPay .' and salary_ceiling >= ' .$basicPay. ' LIMIT 1');
      return $sss;
    }

}
