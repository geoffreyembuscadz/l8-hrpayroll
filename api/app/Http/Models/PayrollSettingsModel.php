<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class PayrollSettingsModel extends Model
{
    protected $table = "payrollsettings"; use SoftDeletes;
}
