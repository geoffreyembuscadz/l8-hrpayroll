<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;
use \DB;
use App\Http\Controllers\UserController;


class OnboardingModel extends Model
{
    protected $table = "onboarding"; use SoftDeletes;

    public static function getUser(){
      $contr = new UserController;
      $data = $contr->getListByIdLimited();

      return $data;
    }

    public function getUserId($id){
        $contr = new UserController;
        $data = $contr->getUserId($id);

        return $data;
    }

    public function getOnboardingList($data){

        $user = $this->getUser();

        $id= $user->user_id;

        $data = DB::table('employee_onboarding')
                ->join('employee', 'employee_onboarding.employee_id', '=', 'employee.id')
                ->join('onboarding_list', 'employee_onboarding.id', '=', 'onboarding_list.employee_onboarding_id')
                ->join('onboarding', 'onboarding_list.onboarding_id', '=', 'onboarding.id')
                ->join('status_type', 'onboarding_list.status_id', '=', 'status_type.id')
                ->select(
                    DB::raw('CONCAT(employee.firstname, " ",employee.lastname) AS `name`'),
                    DB::raw('status_type.name AS status'), 
                    DB::raw('\'\' AS val'),
                    'employee_onboarding.checker_id',
                    'onboarding_list.status_id as status_id',
                    'onboarding.name as type',
                    'onboarding_list.id as onboarding_list_id',
                    'onboarding_list.remarks',
                    'employee_onboarding.id as employee_onboarding_id',
                    'employee_onboarding.id',
                    'employee_onboarding.employee_id as employee_id'
                )
                ->where('employee_onboarding.checker_id','=' ,$id)
                ->get();

        for ($i=0; $i < count($data); $i++) { 
            $data[$i]->val =  false;
        }

        return $data;
    }  


    public function updateStatus($data){

        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        $onboarding = $data['onboarding'];
        $employee_id = $data['employee_id'];
        $table_id = $data['table_id'];
        $created_by = $user->user_id;

        $dat = $this->getUserId($employee_id);

        foreach ($onboarding  as $key => $d) {

            DB::table('onboarding_list')->where('id',$d['id'])->update([
                'status_id'     => 8,
                'remarks'       => $d['remarks'],
                'updated_by'    => $created_by,
                'updated_at'    => $date
            ]);
        }

        $notif_id=DB::table('notification')->insertGetId(
                array(
                    'title'          => 'Onboarding',
                    'description'    => 'Done Onboarding',
                    'messenger_id'   => $created_by,
                    'table_from'     => 'employee_onboarding',
                    'table_from_id'  => $table_id,
                    'seen'           => 0,
                    'route'          => 'employee-dashboard',
                    'receiver_id'    => $dat->user_id,
                    'icon'           => 'fa fa-list-ol text-aqua',
                    'created_by'     => $created_by,
                    'created_at'     => $date
                    )
            );
            
        $notif[]= $notif_id;

        return $notif;
    }

    public function getMyOnboardingList($data){

        $user = $this->getUser();
        $id = $user->employee_id;

    	$data = DB::table('employee_onboarding')
                ->join('onboarding_list', 'employee_onboarding.id', '=', 'onboarding_list.employee_onboarding_id')
                ->join('onboarding', 'onboarding_list.onboarding_id', '=', 'onboarding.id')
                ->join('status_type', 'onboarding_list.status_id', '=', 'status_type.id')
                ->select(
                    DB::raw('status_type.name AS status'), 
                    DB::raw('\'\' AS val'),
                    'onboarding_list.id',
                    'employee_onboarding.checker_id',
                    'onboarding.name as type',
                    'employee_onboarding.employee_id',
                    'onboarding_list.remarks',
                    'employee_onboarding.id as employee_onboarding_id'
                )
                ->where('employee_onboarding.employee_id','=' ,$id)
                ->get();

        for ($i=0; $i < count($data); $i++) { 
            $data[$i]->val =  false;
        }

        return $data;
    }

    public function updateMyStatus($dat){

        $date = date('Y-m-d H:i:s');
        $user = $this->getUser();

        $created_by = $user->user_id;
        $receiver_id = $user->supervisor_id;
        $data = $dat['data'];
        $table_id = $dat['table_id'];

        foreach ($data as $key => $d) {

            DB::table('onboarding_list')->where('id',$d)->update([
                'status_id'     => 7,
                'updated_by'    => $created_by,
                'updated_at'    => $date
            ]);
        }

        $notif_id=DB::table('notification')->insertGetId(
                array(
                    'title'          => 'Onboarding',
                    'description'    => 'Check Onboarding',
                    'messenger_id'   => $created_by,
                    'table_from'     => 'employee_onboarding',
                    'table_from_id'  => $table_id,
                    'seen'           => 0,
                    'route'          => 'onboarding-checklist',
                    'receiver_id'    => $receiver_id,
                    'icon'           => 'fa fa-list-ol text-aqua',
                    'created_by'     => $created_by,
                    'created_at'     => $date
                    )
            );
            
        $notif[]= $notif_id;

        return $notif;
    }
}
