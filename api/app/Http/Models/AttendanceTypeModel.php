<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model; use Illuminate\Database\Eloquent\SoftDeletes;

class AttendanceTypeModel extends Model
{
    protected $table = "attendance_type"; use SoftDeletes;
}
