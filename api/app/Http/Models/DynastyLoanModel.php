<?php
namespace App\Http\Models;

use \DB;
use App\Http\Models\EmployeeModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DynastyLoanModel extends Model
{
    protected $table = "employee_dynasty_loan"; use SoftDeletes;

    public function preListJoin($model){
        return $model->select('employee_dynasty_loan.id', DB::raw('employee.id AS employee_id'), 'employee.employee_code', 'employee.firstname', 'employee.middlename', 'employee.lastname', DB::raw('concat(employee.firstname, " ", employee.middlename, " ", employee.lastname) as employee_fullname'), DB::raw('company.name as company_name'))
            ->whereRaw('employee_dynasty_loan.deleted_at IS NULL')
            ->join('employee', 'employee.id', '=', 'employee_dynasty_loan.company_id')
            ->join('employee_company', 'employee_company.employee_id', '=', 'employee.id')
            ->join('company', 'company.id', '=', 'employee_company.company_id')
            ->whereRaw('employee_company.deleted_at IS NULL')
            ->orderBy('employee.lastname');
    }

    public function postListJoin($model){
        return $model->select("employee.id", "employee.employee_code", "employee.firstname", "employee.middlename", "employee.lastname", DB::raw('concat("employee.lastname", ", ", "employee.firstname") as name'), DB::raw("company.name as company"));
    }
}
