<?php
namespace App\Http\Models;

use \DB;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Http\Request;
use App\Http\Models\Arrays;
use \stdClass;

class EvaluationModel extends Model
{
    protected $table = "evaluation"; use SoftDeletes;

    public static function segment(){
        $query = DB::table('segment AS s')                
                ->select('s.id', 's.name AS segment')
                ->get()->toArray();     
        return $query;
    }

    public function arrangeData($data){
        $rows = $this->segment();
        $rows = json_decode(json_encode($rows), 1);

        $response = [];

        foreach ($rows as $index => $row) {
            $response[$index]['s_id'] = $row['id'];
            $response[$index]['text_s'] = $row['segment'];

            $response[$index]['category'] = DB::table('category AS c')
                ->where('c.segment_id', $row['id'])               
                ->select( 
                    'c.id As c_id',
                    'c.name As cat_name',                   
                    'c.description As desc'                  
                )
                ->get()->toArray();
        }

        return $response;
    }

    public function getSubcat($data) {

        $id = $data['id'];

        $query = DB::table('category AS c')                
                ->select('c.id', 'c.name AS category')
                ->get()->toArray();
        $query = json_decode(json_encode($query), 1);

        $result = [];

        foreach($query AS $index => $row){
            $result[$index]['c_id'] = $row['id'];
            $result[$index]['text_c'] = $row['category'];

            $result[$index]['subcategory'] = DB::table('subcategory AS sc')
                ->where('sc.category_id', $row['id'])
                ->join('question AS q', 'q.id', '=', 'sc.question_id')
                ->join('category As c', 'sc.category_id', '=', 'c.id')
                ->select('sc.id As sc_id','sc.name AS sub_name','sc.description','q.question')
                ->where('c.id', '=', $id)
                ->get()->toArray();            
        }
        return $result;

    }

    public static function employmentStatus(){
        $query = DB::table('employee')
                ->distinct()
                ->get(['employment_status As text']);
        return $query;
    }

    public function getEmpByStatus($data){
        $emp_stat = $data['emp_stat'];

        $query = DB::table(
            DB::raw("(
               SELECT id,
               CONCAT(employee.firstname,' ',employee.lastname) AS `text`
               FROM employee
               WHERE employment_status = '$emp_stat'

            ) As `a`
            ")
        );
        $data = $query->get();
        return $data;
    }

    public static function addSegment(){
        $query = DB::table('segment')
                ->select('id', 'name As text')
                ->where('deleted_by', '=', NULL)
                ->get();
        return $query;
    } 

    public static function addCategory(){

        $query = DB::table(
            DB::raw("(
               SELECT 
               id,
               name AS text
               FROM category
               WHERE is_cat = 1
            ) As `a`
            ")
        );
        $data = $query->get();
        return $data;
    }

    public static function addSubCategory(){

        $query = DB::table(
            DB::raw("(
               SELECT 
               id,
               name AS text
               FROM category
               WHERE is_cat = 0
            ) As `a`
            ")
        );
        $data = $query->get();
        return $data;
    }

    public static function getEmployeePosition(){
        $query = DB::table('position')
                ->select('id', 'name As text')
                ->get();
        return $query;

    }

    public function postEvaluationTemplate($data){
        $event_ids=[];
        $created_by = 1;
        $date = date('Y-m-d H:i:s');

        $evaluation_id = 
        DB::table('evaluation')->insertGetId
        ( array
            (  
                'title'         => $data['name'],
                'position_id'   => $data['position'],
                'created_by'    => $created_by,
                'created_at'    => $date 
               
            )
        );
        foreach ($data['segments'] as $value) {
            
            DB::table('evaluation_details')->insert
            ( array
                (   
                    'evaluation_id'   => $evaluation_id,
                    'segment_id'      => $value['segment'],
                    'question_id'     => $value['question'],
                    'created_by'      => $created_by,
                    'created_at'      => $date
                )
            );
        }


         DB::table('evaluation_rating')->insert
        ( array
            (   
                'evaluation_id'      => $evaluation_id,
                'rating_count'       => $data['rating_count'],
                'created_by'         => $created_by,
                'created_at'         => $date
            )
        );

    }


    public function getEvaluation($data){
        $position_id = $data['position'];
        $evaluation_id = $data['id'];

        $query = DB::table(
            DB::raw("(
                SELECT e.id, e.title, p.name position,
                s.name segment, 
                q.question, e_r.rating_count, q.id As q_id, q.rating_value
                FROM evaluation_details AS e_d
                JOIN segment s ON e_d.segment_id = s.id
                JOIN question q ON e_d.question_id = q.id
                JOIN evaluation e ON e_d.evaluation_id = e.id
                JOIN position p ON e.position_id = p.id
                JOIN evaluation_rating e_r ON e_d.evaluation_id = e_r.evaluation_id
                WHERE e.position_id = '$position_id'
                AND e.id = '$evaluation_id'
            ) As `a`
            ")
        )->get();


        $query = json_decode(json_encode($query), 1);
        $array_value =[];
        
        foreach ($query AS $key => &$data) {
            $array_value['id'] = $data['id'];
            $array_value['title'] = $data['title'];
            $array_value['emp_position'] = $data['position'];
            $array_value['rating'] = $data['rating_count'];                                    

            if ( is_array($data) ) {
               $array_value['segments'][$key]['segment_name'] = $data['segment'];                                  
               $array_value['segments'][$key]['q_id'] = $data['q_id'];                                  
               $array_value['segments'][$key]['question'] = $data['question'];                                  
               $array_value['segments'][$key]['rating_value'] = $data['rating_value'];                                  
            } 

        }
        $query = json_encode($array_value);
        $query = json_decode($query, true);

        $segment_array = [];
        foreach($query['segments'] as $arr){
          $segment_array[$arr['segment_name']]['segment_name'] = $arr['segment_name'];
          $segment_array[$arr['segment_name']]['question_collection'][] = 
          array( 
                'q_id'=>$arr['q_id'],
                'question'=>$arr['question'], 
                'rating_value'=>$arr['rating_value']
            );
        }

        $query['segments'] = array_values($segment_array);

        $array = array(json_decode(json_encode($query), true));
        return $array;
        // echo '<pre>';
        // print_r($array);exit;
    }

    public function getEvaluationTitle($data){
        $position_id = $data['position'];

        $query = DB::table('evaluation')
                ->select('id','title AS text')
                ->where('position_id', '=', $position_id )
                ->get();
        return $query;
    }

     public function getEmployeeByPosition($data){
        $position_id = $data['position'];

        $query = DB::table('employee')
                ->select('id',
                    DB::raw('CONCAT(firstname, " ",lastname) AS `text`'))
                ->where('position', '=', $position_id )
                ->get();

        return $query;
    }

      public function postEvaluationForm($data){

        $created_by = 1;
        $date = date('Y-m-d H:i:s');
        $form = [];

        foreach ($data['employee'] as $counter => $value) {
            
            $id=DB::table('emp_evaluation_form')->insertGetId
            ( array
                (   
                    'evaluation_id'   => $data['evaluation_id'],
                    'employee_id'     => $value,
                    'date'            => $data['date'],
                    'created_by'      => $created_by,
                    'created_at'      => $date
                )
            );

            $form[$counter]= $id;
        }

        return $form;


    }


    public function getEmployeeEvaluation($data){
        $id = $data['id'];

        $query = DB::table(
            DB::raw("(

            SELECT eef.id, e.title, s.name segment, q.question, s.id As s_id,
            q.id q_id, emp.id As e_code, q.rating_value, 
            CONCAT(emp.firstname,' ',emp.lastname) AS employee_name, 
            p.name position, p.id AS p_id
            FROM emp_evaluation_form eef
            JOIN evaluation e ON eef.evaluation_id = e.id
            JOIN evaluation_details e_d ON e.id = e_d.evaluation_id
            JOIN segment s ON e_d.segment_id = s.id
            JOIN question q ON e_d.question_id = q.id
            JOIN employee emp ON eef.employee_id = emp.id
            JOIN position p On emp.position = p.id
            WHERE eef.employee_id = '$id'  
                
            ) As `a`
            ") /*AND eef.date = CURDATE()*/
                
        )->get();

        $query = json_decode(json_encode($query), 1);
        $array_value =[];
        
        foreach ($query AS $key => &$data) {
            $array_value['id'] = $data['id'];
            $array_value['title'] = $data['title'];
            $array_value['emp_id'] = $data['e_code'];                                  
            $array_value['emp_name'] = $data['employee_name'];                                  
            $array_value['position_id'] = $data['p_id'];                                  
            $array_value['position'] = $data['position'];                                  

            if ( is_array($data) ) {
               $array_value['segments'][$key]['segment_name'] = $data['segment'];                                  
               $array_value['segments'][$key]['q_id'] = $data['q_id'];                                  
               $array_value['segments'][$key]['s_id'] = $data['s_id'];                                  
               $array_value['segments'][$key]['question'] = $data['question'];                                  
               $array_value['segments'][$key]['rating_value'] = $data['rating_value'];                                  
            } 

        }
        $query = json_encode($array_value);
        $query = json_decode($query, true);


        $segment_array = [];
        foreach($query['segments'] as $arr){
          $segment_array[$arr['segment_name']]['segment_name'] = $arr['segment_name'];
          $segment_array[$arr['segment_name']]['s_id'] = $arr['s_id'];
          $segment_array[$arr['segment_name']]['question_collection'][] = 
          array( 
                'q_id'=>$arr['q_id'],
                'question'=>$arr['question'], 
                'rating_value'=> 0
            );
        }
        $query['segments'] = array_values($segment_array);

        $array = array(json_decode(json_encode($query), true));
        return $array;
    }



    public function postEvaluationAnswer($data){
        $date = date('Y-m-d H:i:s');
        $created_by = 1;

        for ($i = 0; $i < count($data); $i++) {
            for ($x = 0; $x < count($data[$i]['segments']); ++$x) {
                for ($y = 0; $y < count($data[$i]['segments'][$x]['question_collection']); ++$y) {
                    $employee_id   = $data[$i]['emp_id'];
                    $position_id   = $data[$i]['position_id'];
                    $position      = $data[$i]['position'];
                    $question_id   = $data[$i]['segments'][$x]['question_collection'][$y]['q_id'];
                    $question      = $data[$i]['segments'][$x]['question_collection'][$y]['question'];
                    $segment_id    = $data[$i]['segments'][$x]['s_id'];
                    $segment_name  = $data[$i]['segments'][$x]['segment_name'];
                    $rating_value  = $data[$i]['segments'][$x]['question_collection'][$y]['rating_value'];

                    $evaluation_id =
                    DB::table('employee_evaluation_rating')->insertGetId
                     ( array
                        (  
                            'employee_id'         =>  $employee_id,
                            'position_id'         =>  $position_id,
                            'created_by'          =>  $created_by,
                            'created_at'          =>  $date 

                        )
                    );

                        
                    DB::table('employee_evaluation_rating_details')->insert
                     ( array
                        (   
                            'employee_evaluation_rating_id'     =>  $evaluation_id,
                            'segment_id'                        =>  $segment_id,
                            'question_id'                       =>  $question_id,
                            'rating_value'                      =>  $rating_value,
                            'primary_strengths'                 =>  $rating_value,
                            'improvement_needs'                 =>  $rating_value,
                            'action_taken'                      =>  $rating_value,
                            'comments_recommendation'           =>  $rating_value,
                            'created_by'                        =>  $created_by,
                            'created_at'                        =>  $date
                        )
                    );
                
                }

            }
        }

        return $data;
    }

    public static function showAllEmployee(){

        $query = DB::table('employee')
                ->select('id',
                    DB::raw('CONCAT(firstname, " ",lastname) AS `text`'))
                ->get();
        return $query;
    }


    public static function showEmployeeRate(){
        $query =  DB::table(
            DB::raw("(                            
                    SELECT e.id,
                    CONCAT(e.firstname,' ',e.lastname) AS employee_name,
                    CASE WHEN eerd.rating_value IS NOT NULL THEN ROUND(AVG(eerd.rating_value), 2) 
                    ELSE 0.00 END AS rate
                    FROM employee e
                    LEFT JOIN employee_evaluation_rating eer ON e.id = eer.employee_id 
                    LEFT JOIN employee_evaluation_rating_details eerd ON eer.id = eerd.employee_evaluation_rating_id
                    GROUP BY id
                
            ) As `a`
            ")
                
        )->get();
        return $query;
    }

     // SELECT e.employee_code AS id, IFNULL(eer.rate, 0) AS rate, CONCAT(e.firstname,' ',e.lastname) AS employee_name
     //         FROM employee e
     //         LEFT JOIN (
     //            SELECT eer.employee_id, ROUND(AVG(eerd.rating_value), 2) AS rate
     //            FROM employee_evaluation_rating eer
     //            JOIN employee_evaluation_rating_details eerd ON eer.id = eerd.employee_evaluation_rating_id 
     //        ) eer ON e.employee_code = eer.employee_id

    public function filterEmployee($data){
        $type = $data['type'];

        $employee = DB::table('employee_evaluation_rating')
                    ->select('employee_id AS id')
                    ->get();
        $emp_id = [];
        for ($x=0, $y = count($employee); $x < $y; $x++) { 
            $emp_id[$x] = $employee[$x]->id;
        }
        
        $query =  DB::table('employee AS e')
                    ->select(
                        'id',
                        DB::raw('CASE WHEN eerd.rating_value IS NOT NULL THEN ROUND(AVG(eerd.rating_value), 2) ELSE 0.00 END AS rate'),
                        DB::raw('CONCAT(e.firstname, " ",e.lastname) AS `employee_name`')
                    )
                     ->leftJoin('employee_evaluation_rating AS eer', 'e.id', '=', 'eer.employee_id')
                     ->leftJoin('employee_evaluation_rating_details AS eerd', 'eer.id', '=', 'eerd.employee_evaluation_rating_id')
                     ->groupBy('id');

        if ($type == '0') {
            $query;
        } else if ($type == '1'){
            $query->whereNotIn('id', $emp_id);
        } else if ($type == '2') {
            $query->whereIn('id', $emp_id);
        }
        $query = $query->get();
        return $query;
    }

    public function viewEmployeeEvaluation($data){
       $id = $data['id'];

        $query = DB::table(
            DB::raw("(
                SELECT eer.employee_id,  CONCAT(e.firstname,' ',e.lastname) AS employee_name, 
                p.name As emp_position, eerd.question_id AS q_id, eerd.rating_value, 
                q.question, s.id s_id, s.name segment
                FROM employee_evaluation_rating_details eerd
                JOIN employee_evaluation_rating eer ON eerd.employee_evaluation_rating_id = eer.id
                JOIN question q ON eerd.question_id = q.id
                JOIN segment s ON eerd.segment_id = s.id   
                JOIN employee e On eer.employee_id = e.id
                JOIN position p ON eer.position_id = p.id       
                WHERE eer.employee_id = '$id'         
            ) As `a`
            ") 
                
        )->get();

        $query = json_decode(json_encode($query), 1);
        
        $array_value =[];        
        foreach ($query AS $key => &$data) {
            $array_value['employee_name'] = $data['employee_name'];
            $array_value['emp_position'] = $data['emp_position'];
            $array_value['rating_value'] = $data['rating_value'];                                    

            if ( is_array($data) ) {
               $array_value['segments'][$key]['segment_name'] = $data['segment'];                                  
               $array_value['segments'][$key]['q_id'] = $data['q_id'];                                  
               $array_value['segments'][$key]['question'] = $data['question'];                                  
               $array_value['segments'][$key]['rating_value'] = $data['rating_value'];                                  
            } 

        }
        $query = json_encode($array_value);
        $query = json_decode($query, true);

        $segment_array = [];
        foreach($query['segments'] as $arr){
          $segment_array[$arr['segment_name']]['segment_name'] = $arr['segment_name'];
          $segment_array[$arr['segment_name']]['question_collection'][] = 
          array( 
                'q_id'=>$arr['q_id'],
                'question'=>$arr['question'], 
                'rating_value'=>$arr['rating_value']
            );
        }

        $query['segments'] = array_values($segment_array);

        $array = array(json_decode(json_encode($query), true));
        return $array;

    }


    public function getEmployee($data){
        $supervisor_id = $data['supervisor_id'];

        return $query = DB::table('employee')
                ->join('position As p', 'employee.position', '=', 'p.id')
                ->leftJoin('appraisal AS a', 'employee.id', '=', 'a.employee_id')
                ->select('p.name AS position', 'employee.date_hired', 'employee.employment_status As status',
                  DB::raw('CONCAT(employee.lastname, ", ",employee.firstname) AS `name`'),
                  DB::raw('employee.id As id'),
                  DB::raw('COUNT(a.objective) As obj_count')
                  )
                ->where('employee.supervisor_id', 'LIKE', '%' . $supervisor_id . '%')
                ->groupBy('employee.id')
                ->get();





    }


}




