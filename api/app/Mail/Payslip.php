<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Payslip extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The payroll instance.
     *
     * @var Payroll
     */
    public $payroll;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($payroll)
    {
        $this->payroll = $payroll;

    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.payslip')
                ->from('no@reply.com', 'Logic8 HRboss')
                ->with([
                    'payroll_id' => $this->payroll->payroll_id,
                    'firstname'  => $this->payroll->firstname,
                    'start'      => $this->payroll->start,
                    'end'        => $this->payroll->end,
                    'company'    => $this->payroll->company,
                    'branch'     => $this->payroll->branch,
                    'emp_id'     => $this->payroll->employee_id
                ]);;
      
    }
}