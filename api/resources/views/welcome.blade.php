<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <script src="{!! asset('js/jquery.js') !!}"></script>
        <script>
        $.ajaxSetup({ headers: { 'x-my-custom-header': '{!! csrf_token() !!}' }});
        </script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>

                <button class="store">Store</button>
                <button class="update">Update</button>
                <button class="get">Get</button>
                <button class="list">List</button>
                <button class="login">Login</button>
            </div>
        </div>
        <script>
        var raw_token = '{{ csrf_token() }}';

        $('.login').click(function(){
          var raw_data = {
            _token: raw_token,
            email: 'admin2@yahoo.com',
            password: 'password'
          };

          $.ajax({
            url: '{!! url('login') !!}',
            type: 'POST',
            data: raw_data,
            complete: function(html){
              window.location.href = '{{ url('home') }}';
            }
          });
        });
        $('.store').click(function(){
          var raw_data = {
            _token: raw_token,
            name: 'Fruit Salad',
            description: 'Oriented mixed fruits for dessert',
            srp: 25.5
          };

          $.ajax({
            url: '{!! url("item") !!}',
            type: 'POST',
            data: raw_data
          });
        });
        $('.update').click(function(){
          var raw_data = {
            _token: '{{ csrf_token() }}',
            name: 'Buko Salad',
            description: 'Oriented mixed jelly and buko for dessert',
            srp: 12.5
          };

          $.ajax({
            url: '{!! url("item") !!}/1',
            type: 'PUT',
            data: raw_data
          });
        });
        $('.get').click(function(){
          var raw_data = { _token: '{{ csrf_token() }}' };
          $.ajax({
            url: '{!! url('item/1') !!}',
            type: 'GET'
          });
        });
        $('.list').click(function(){
          var raw_data = { _token: '{{ csrf_token()}}' };
          $.ajax({
            url: '{!! url('item') !!}'
          });
        });
        </script>
    </body>
</html>
