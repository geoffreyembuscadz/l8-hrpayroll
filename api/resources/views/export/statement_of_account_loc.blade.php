<!DOCTYPE html>
<html lang="en">
<head>
	<title>Statement Of Account</title>
	<style type="text/css">
		html {
			font-size: 13px;
		}
		table {
			font-family: Calibri, sans-serif;
		}
		.table {
			width: 100%;
			margin: 0 auto;
			border-collapse: collapse;
		}
		td {
			padding: 2px;
			vertical-align:top !important;
		}
		
		.page-break {
		    page-break-after: always;
		}

		body { word-wrap: break-word; }

		.hidden {display: none; height: 0; width: 0; color: #ffffff;}
	
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	@foreach($payroll_list as $payroll)
	<table border="" class="table" style="padding-bottom: 20px; padding-top: 80px;">
		<tr >
			<td colspan="5" >
				<table class="soa" border="" cellpadding="0" cellspacing="0" style="width:100%;float:left; ">
					
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" style="height: 30px;">
								<tr>
									<td class=""><strong>{{ date("d M Y", strtotime($data['date'])) }}</strong></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
		
							<table width="50%" border="0" cellpadding="0" cellspacing="0" style="height: 100px;">
								<tr>
									<td class=""><strong>{{ $payroll->name }}</strong></td>
									
								</tr>
								<tr>
									<td>{{ $payroll->address }}
									</td>
									
								</tr>
								<tr>
									<td>
										{{ $payroll->phone_number }}
									</td>
								</tr>
							</table>
						
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" style="height: 60px;">
								<tr>
									<td style="text-align:center;"><strong> STATEMENT OF ACCOUNT </strong></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" style="height: 80px;">
								<tr>

									<td class="">We hereby charge your account for the services <br /> rendered for the <strong>PAYROLL PERIOD: {{ date("d M Y", strtotime($payroll->start_time))}}  to {{ date("d M Y", strtotime($payroll->end_time)) }} </strong></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" style="height: 70px;">
								<tr>
									<td style="">Gross Pay</td>
									<td style="text-align: right;">{{ number_format($data['total_billable'], 2) }}</td>
								</tr>
								@if($payroll->billing_rate <= 0)
								<tr>
									<td style="">Admin Fee</td>
									<td style="text-align: right;">{{ number_format($data['admin_fee'],2) }}</td>
								</tr>
								@endif
								@if( $payroll->billing_rate <= 0)
								<tr>
									<td style="">12% VAT </td>
									<td style="text-align: right;">{{ number_format($data['vat'], 2) }}</td>
								</tr>
								@endif
								@if( $payroll->billing_rate <= 0)
								<tr>
									<td style="">Less 2%</td>
									<td style="text-align: right; ">({{ number_format($data['less2'], 2) }})</td>
								</tr>
								@endif
								<tr>
									<td style="padding-top: 5px; border-top: 1px solid black; border-bottom: 2px double black;"><strong>TOTAL AMOUNT DUE</strong> 
									</td>
									<td style="border-top: 1px solid black; border-bottom: 2px double black; text-align: right; font-weight: bold;">
										{{number_format($data['total_billable'] + ($payroll->billing_rate <= 0 ? $data['admin_fee'] + $data['vat'] - $data['less2'] : 0),2)}}										
								    </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" style="height: 70px; padding-top: 30px;">
								<tr>
									<td style="padding-top: 30px;">Prepared By: </td>
									<td style="padding-top: 30px; text-align: right; padding-right: 30px;">&nbsp;</td>
								</tr>
								<tr>
									<td style="padding-top: 30px;">{{ $payroll->prepared_by }}</td>
									<td style="padding-top: 30px; text-align: right; padding-right: 30px;"><span style="border-top: 1px solid black;">Signature Over Printed Name</span></td>
								</tr>
								<tr>
									<td style="padding-top: 30px; padding-bottom: 30px;">Received By: </td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>
							<table width="50%" border="0" cellpadding="0" cellspacing="0" style="height: 80px;">
								<tr>
									<td style=" border-top: 1px solid black; ">
										<strong>
											(DUE 15 DAYS UPON RECEIPT) <br /> 

											Please advise us of any revisions or errors w/in 24hours <br />

											or the next business day 

										</strong>
									</td>
								</tr>
							</table>
						</td>
					</tr>

				</table>
				
				<div style="clear:both"></div>
				
			</td>
		</tr>

	</table>
	@endforeach
</body>
</html>