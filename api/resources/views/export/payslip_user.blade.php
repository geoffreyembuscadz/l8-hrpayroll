<!DOCTYPE html>
<html lang="en">
<head>
	<title>Payslip</title>
	<style type="text/css">
		html {
			font-size: 12px;
		}
		table {
			font-family: Calibri, sans-serif;
		}
		.table {
			width: 100%;
			margin: 0 auto;
			border-collapse: collapse;
		}
		td {
			padding: 2px;
			vertical-align:top !important;
		}
		.payroll-details {
			font-size: 12px;
		}
		.payroll-details .td {
			text-align: center;
		}
		.date {
			text-align: right;
		}
		.page-break {
		    page-break-after: always;
		}

		body { word-wrap: break-word; }

		.hidden {display: none; height: 0; width: 0; color: #ffffff;}
	
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<div class="hidden">
	{{ $var = 0 }}
	</div>
    @foreach($payrollList as $key => $payroll)
	<table border="1" class="table" style="padding-bottom: 20px;">
		<tr>
			<td colspan="4" style="padding: 5px; background-color: #90CAF9"><strong>{{ $payroll->company_name }}</strong></td>
			<td class="date" colspan="1" style="padding: 5px; background-color: #90CAF9">{{ date("d M Y", strtotime($data['date'])) }}</td>
		</tr>
		<tr>
			<td>Station:</td>
			<td colspan="4">{{ $payroll->branch_name}}</td>
		</tr>
	    <tr>
		    <td>Payroll Period:</td>
		    <td colspan="4">{{ date("d M Y", strtotime($payroll->start_time)) }} to {{ date("d M Y", strtotime($payroll->end_time)) }}</td>
	    </tr>
		<tr>
			<td colspan="5"><strong>{{ $payroll->firstname }}  {{ $payroll->lastname }}</strong></td>
		</tr>
		<tr >
			<td colspan="5" >
				<table class="payroll-details" border="" cellpadding="0" cellspacing="0" style="width:58%;float:left; ">
		
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" style="height: 170px;">
								<tr>
									<td colspan="3"><strong>Gross Salary</strong></td>
									<td class="td"><strong>Particular</strong></td>
									<td class="td"><strong>Amount</strong></td>
								</tr>
								<tr>
									<td></td>
									<td colspan="2">Reg. Days</td>
									<td class="td">{{ $payroll->no_of_days }}</td>
									<td class="td">{{ number_format($payroll->ttl_basic_pay, 2) }}</td>
								</tr>
								@if($payroll->total_late != 0 || $payroll->total_undertime != 0)
								<tr>
									<td></td>
									<td colspan="2">Late/Undertime</td>
									<td class="td">({{ $payroll->total_late + $payroll->total_undertime }})</td>
									<td class="td">({{ number_format($payroll->late_amount + $payroll->undertime_amount, 2) }})</td>
								</tr>
								@endif
								@if($payroll->reg_ot != 0)
								<tr>
									<td></td>
									<td colspan="2">Reg. Overtime</td>
									<td class="td">{{ $payroll->reg_ot }}</td>
									<td class="td">{{ number_format($payroll->pay_reg_ot, 2) }}</td>
								</tr>
								@endif
								@if($payroll->restday != 0)
								<tr>
									<td></td>
									<td colspan="2">Restday</td>
									<td class="td">{{ $payroll->restday }}</td>
									<td class="td">{{ number_format($payroll->pay_restday, 2) }}</td>
								</tr>
								@endif
								@if($payroll->rd_ot != 0)
								<tr>
									<td></td>
									<td colspan="2">Restday Overtime</td>
									<td class="td">{{ $payroll->rd_ot }}</td>
									<td class="td">{{ number_format($payroll->pay_rd_ot, 2) }}</td>
								</tr>
								@endif
								@if($payroll->special_hol != 0)
								<tr>
									<td></td>
									<td colspan="2">Special Holiday</td>
									<td class="td">{{ $payroll->special_hol }}</td>
									<td class="td">{{ number_format($payroll->pay_special_hol, 2) }}</td>
								</tr>
								@endif
								@if($payroll->special_hol_ot != 0)
								<tr>
									<td></td>
									<td colspan="2">OT - Special Holiday</td>
									<td class="td">{{ $payroll->special_hol_ot }}</td>
									<td class="td">{{ number_format($payroll->pay_special_hol_ot, 2) }}</td>
								</tr>
								@endif
								@if($payroll->special_hol_rd != 0)
								<tr>
									<td></td>
									<td colspan="2">Restday - Special Holiday</td>
									<td class="td">{{ $payroll->special_hol_rd }}</td>
									<td class="td">{{ number_format($payroll->pay_special_hol_rd, 2) }}</td>
								</tr>
								@endif
								@if($payroll->special_hol_rd_ot != 0)
								<tr>
									<td></td>
									<td colspan="2">OT & RD - Special Holiday</td>
									<td class="td">{{ $payroll->special_hol_rd_ot }}</td>
									<td class="td">{{ number_format($payroll->pay_special_hol_rd_ot, 2) }}</td>
								</tr>
								@endif
								@if($payroll->legal_hol != 0)
								{{-- Legal Holiday  --}}
								<tr>
									<td></td>
									<td colspan="2">Legal Holiday</td>
									<td class="td">{{ $payroll->legal_hol }}</td>
									<td class="td">{{ number_format($payroll->pay_legal_hol, 2) }}</td>
								</tr>
								@endif
								@if($payroll->legal_hol_ot != 0)
								<tr>
									<td></td>
									<td colspan="2">OT - Legal Holiday</td>
									<td class="td">{{ $payroll->legal_hol_ot }}</td>
									<td class="td">{{ number_format($payroll->pay_legal_hol_ot, 2) }}</td>
								</tr>
								@endif
								@if($payroll->legal_hol_restday != 0)
								<tr>
									<td></td>
									<td colspan="2">Restday - Legal Holiday</td>
									<td class="td">{{ $payroll->legal_hol_restday }}</td>
									<td class="td">{{ number_format($payroll->pay_legal_hol_restday, 2) }}</td>
								</tr>
								@endif
								@if($payroll->legal_hol_rd_ot != 0)
								<tr>
									<td></td>
									<td colspan="2">OT & RD - Legal Holiday</td>
									<td class="td">{{ $payroll->legal_hol_rd_ot }}</td>
									<td class="td">{{ number_format($payroll->pay_legal_hol_rd_ot, 2) }}</td>
								</tr>
								@endif
								@if($payroll->double_hol != 0)
								{{-- Double Holiday  --}}
								<tr>
									<td></td>
									<td colspan="2">Double Holiday</td>
									<td class="td">{{ $payroll->double_hol }}</td>
									<td class="td">{{ number_format($payroll->pay_double_hol, 2) }}</td>
								</tr>
								@endif
								@if($payroll->dbl_hol_ot != 0) 
								<tr>
									<td></td>
									<td colspan="2">OT - Double Holiday</td>
									<td class="td">{{ $payroll->dbl_hol_ot }}</td>
									<td class="td">{{ number_format($payroll->pay_dbl_hol_ot, 2) }}</td>
								</tr>
								@endif
								@if($payroll->double_hol_rd != 0)
								<tr>
									<td></td>
									<td colspan="2">Restday - Double Holiday</td>
									<td class="td">{{ $payroll->double_hol_rd }}</td>
									<td class="td">{{ number_format($payroll->pay_double_hol_rd, 2) }}</td>
								</tr>
								@endif
								@if($payroll->dbl_hol_rd_ot != 0)
								<tr>
									<td></td>
									<td colspan="2">OT & RD - Double Holiday</td>
									<td class="td">{{ $payroll->dbl_hol_rd_ot }}</td>
									<td class="td">{{ number_format($payroll->pay_dbl_hol_rd_ot, 2) }}</td>
								</tr>
								@endif
								@if($payroll->reg_nd != 0)
								<tr>
									<td></td>
									<td colspan="2">Reg. Night Diff.</td>
									<td class="td">{{ $payroll->reg_nd }}</td>
									<td class="td">{{ number_format($payroll->pay_reg_nd, 2) }}</td>
								</tr>
								@endif
								@if($payroll->rd_nd != 0)
								<tr>
									<td></td>
									<td colspan="2">Restday Night Diff.</td>
									<td class="td">{{ $payroll->rd_nd }}</td>
									<td class="td">{{ number_format($payroll->pay_rd_nd, 2) }}</td>
								</tr>
								@endif
								@if($payroll->special_hol_nd != 0)
								<tr>
									<td></td>
									<td colspan="2">Special Holiday - ND</td>
									<td class="td">{{ $payroll->special_hol_nd }}</td>
									<td class="td">{{ number_format($payroll->pay_special_hol_nd, 2) }}</td>
								</tr>
								@endif
								@if($payroll->special_hol_rd_nd != 0)
								<tr>
									<td></td>
									<td colspan="2">Special Holiday - RD & ND</td>
									<td class="td">{{ $payroll->special_hol_rd_nd }}</td>
									<td class="td">{{ number_format($payroll->pay_special_hol_rd_nd, 2) }}</td>
								</tr>
								@endif
								@if($payroll->legal_hol_nd != 0)
								<tr>
									<td></td>
									<td colspan="2">Legal Holiday - ND</td>
									<td class="td">{{ $payroll->legal_hol_nd }}</td>
									<td class="td">{{ number_format($payroll->pay_legal_hol_nd, 2) }}</td>
								</tr>
								@endif
								@if($payroll->legal_hol_rd_nd != 0)
								<tr>
									<td></td>
									<td colspan="2">Legal Holiday ju RD & ND</td>
									<td class="td">{{ $payroll->legal_hol_rd_nd }}</td>
									<td class="td">{{ number_format($payroll->pay_legal_hol_rd_nd, 2) }}</td>
								</tr>
								@endif
								@if($payroll->dbl_hol_nd != 0)
								<tr>
									<td></td>
									<td colspan="2">Double Holiday - ND</td>
									<td class="td">{{ $payroll->dbl_hol_nd }}</td>
									<td class="td">{{ number_format($payroll->pay_dbl_hol_nd, 2) }}</td>
								</tr>
								@endif
								@if($payroll->dbl_hol_rd_nd != 0)
								<tr>
									<td></td>
									<td colspan="2">Double Holiday - RD & ND</td>
									<td class="td">{{ $payroll->dbl_hol_rd_nd }}</td>
									<td class="td">{{ number_format($payroll->pay_dbl_hol_rd_nd, 2) }}</td>
								</tr>
								@endif

								@if($payroll->total_adjustment != 0)
								<tr>
									<td></td>
									<td colspan="2">Adjustment</td>
									<td class="td"></td>
									<td class="td">{{ number_format($payroll->total_adjustment, 2) }}</td>
								</tr>
								@endif
					
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table class="payroll-details" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td colspan="3" style=""><strong>Gross Pay</strong></td>
									<td style="padding-left: 275px;"></td>
									<td class="td" style="border-top: 1px solid #000"><strong>{{ number_format($payroll->gross_pay, 2) }}</strong></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table class="payroll-details" border="0" cellpadding="0" cellspacing="0" style="width:38%;float:left;">
					<tr>
						<td>
							<table class="payroll-details" border="0" cellpadding="0" cellspacing="0" >
								<tr>
									<td colspan="3"><strong>Deduction</strong></td>
									<td style="padding-left: 160px;"></td>
									<td class="td"><strong>Amount</strong></td>
								</tr>
								<tr>
									<td colspan="3">SSS</td>
									<td style="padding-left: 160px;"></td>
									<td class="td">{{ number_format($payroll->total_sss, 2)}}</td>
								</tr>
								<tr>
									<td colspan="3">Philhealth</td>
									<td style="padding-left: 160px;"></td>
									<td class="td">{{ number_format($payroll->total_philhealth, 2)}}</td>
								</tr>
								<tr>
									<td colspan="3">Pagibig</td>
									<td style="padding-left: 160px;"></td>
									<td class="td">{{ number_format($payroll->total_pagibig, 2)}}</td>
								</tr>
								@foreach($adjustment as $key => $adj)
								@if($payroll->emp_id == $adj->employee_id && $adj->adjustment_type == 'DEDUCTION')
								<tr>
									<td colspan="4">{{ $adj->display_name}}</td>
									<td class="td">{{ number_format(abs($adj->adjustment_amount), 2)  }}</td>
								</tr>
								@endif
								@endforeach

								@foreach($loans as $key => $loan)
								@if($payroll->emp_id == $loan->employee_id && $loan->loan_type == 'DEDUCTION')
								<tr>
									<td colspan="4">{{ $loan->display_name}}</td>
									<td class="td">{{ number_format(abs($loan->loan_amount), 2)  }}</td>
								</tr>
								@endif
								@endforeach
								
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table class="payroll-details" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td colspan="3" style="width: 225px;">Total Deduction</td>
									<td >{{ number_format(abs($payroll->total_deduction) + $payroll->total_sss + $payroll->total_philhealth + $payroll->total_pagibig + abs($payroll->total_loan), 2)  }}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table class="payroll-details" border="0" cellpadding="0" cellspacing="0">

								<tr>
									<td colspan="3" style="width: 225px;"><strong>Net Pay</strong></td>
									<td class="td" ><strong>{{ number_format($payroll->net_pay, 2) }}</strong></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div style="clear:both"></div>
				<table  border="0" cellpadding="0" cellspacing="0" width="100%" >
					<tr>
						<td>
							<table border=""0 cellpadding="0" cellspacing="0" style="float:right; padding-right: 40px;">
								<tr>
									<td colspan="5" style="">Received by:</td>
								</tr>
								<tr>
									<td colspan="5" style="padding-top: 15px;">
										<table border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td colspan="5" >
													<hr style="padding-left: 10px;" width="180px;">
												</td>
												<td colspan="5"  style="padding-top: 15px; text-align: center;">
													<strong>Signature Over Printed Name</strong>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div style="clear:both"></div>

			</td>
		</tr>

	</table>
	<div class="hidden">{{ $var++ }}</div>
		@if($var % 2 == 0)
		
		<div class="page-break">
		</div>
		@endif
	@endforeach

</body>
</html>