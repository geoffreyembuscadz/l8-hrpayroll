<!DOCTYPE html>
<html lang="en">
<head>
	<title>Clearance</title>
	<style type="text/css">
		html {
			font-size: 12px;
		}
		table {
			font-family: Calibri, sans-serif;
		}
		.table {
			width: 100%;
			margin: 0 auto;
			border-collapse: collapse;
		}
		td {
			padding: 2px;
			vertical-align:top !important;
		}
		.payroll-details {
			font-size: 12px;
		}
		.payroll-details .td {
			text-align: center;
		}
		.date {
			text-align: right;
		}
		.page-break {
		    page-break-after: always;
		}

		.cntr {
			text-align: center;
			font-weight: bold;
		}

		.lft {
			font-weight: bold;
			text-align: left;
		}

		body { word-wrap: break-word; }

		.hidden {display: none; height: 0; width: 0; color: #ffffff;}
	
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>

	<table border="0" class="table" style="padding-bottom: 20px;">
		<tr>
			<td class="cntr"><h1>{{ $empInfo->company_name }}</h1></td>
		</tr>
		<tr>
			<td class="cntr"><h2>Clearance</h2></td>
		</tr>
	</table>
	<br>
	<table  border="0" class="table" style="padding-bottom: 20px;">
		<tr>
			<td class="lft">Name:</td>
			<td>{{ $empInfo->name }}</td>
			<td class="lft">Date:</td>
			<td>{{ $data['date'] }}</td>
		</tr>
		<tr>
			<td class="lft">Position:</td>
			<td>{{ $empInfo->position_name }}</td>
			<td class="lft">ID#:</td>
			<td>{{ $empInfo->employee_code }}</td>
		</tr>
		<tr>
			<td class="lft">Date Start:</td>
			<td>{{ $empInfo->starting_date }}</td>
			<td class="lft">Date End:</td>
			<td>{{ $empInfo->end_date }}</td>
		</tr>
	</table>
	<br>
	<table  border="1" class="table" style="padding-bottom: 20px;">
		<tr class="cntr">
			<td>Name</td>
			<td>Status</td>
			<td>Check By</td>
			<td>Remarks</td>
		</tr>
		@foreach($clearance as $key => $clear)
			@if($clear->status == 'PENDING' || $clear->status == 'CHECKING')
				<tr style="color: red;">
					<td>{{ $clear->type }}</td>
					<td>{{ $clear->status }}</td>
					<td>{{ $clear->name }}</td>
					<td>{{ $clear->remarks }}</td>
				</tr>
			@endif
			@if($clear->status == 'DONE')
				<tr>
					<td>{{ $clear->type }}</td>
					<td>{{ $clear->status }}</td>
					<td>{{ $clear->name }}</td>
					<td>{{ $clear->remarks }}</td>
				</tr>
			@endif
		@endforeach
	</table>
<br>
<br>
<br>
<br>
<br>
	<table  border="0" cellpadding="0" cellspacing="0" width="100%" >
		<tr>
			<td>
				<table border=""0 cellpadding="0" cellspacing="0" style="float:right; padding-right: 40px;">
					<tr>
						<td colspan="5" style="padding-top: 15px;">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td colspan="5" >
										<hr style="padding-left: 10px;" width="180px;">
									</td>
									<td colspan="5"  style="padding-top: 15px; text-align: center;">
										<strong>EMPLOYEE'S SIGNATURE</strong>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border=""0 cellpadding="0" cellspacing="0">
					<tr>
						<td colspan="5" style="padding-top: 15px;">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td colspan="5" >
										<hr style="padding-left: 10px;" width="180px;">
									</td>
									<td colspan="5"  style="padding-top: 15px; text-align: center;">
										<strong>Signature Over Printed Name</strong>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</body>
</html>