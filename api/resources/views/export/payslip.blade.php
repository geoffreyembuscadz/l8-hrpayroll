<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>MJM Payslip</title>

	<style type='text/css'>
		html {
			font-family: Calibri, sans-serif;
			font-size: 12px;
		}
		.main {
			border: 2px solid black;
			margin-bottom: 5px;
		}
		.header {
			width: 100%;
			display: block;
			border-bottom: 1px solid black;
		}
		.content {
			width: 100%;
			display: block;
		}
		.head-title {
			text-align: center;
			font-weight: bold;
		}

		.header .content .head-title {
			padding: 5px 0;
		}
		.payslip-details .content .head-title {
			padding: 5px 0;
			text-decoration: underline;
		}

		.column {
		    float: left;
		    width: 25%;
		}

		.column15 {
			float: left;
			width: 15%;
		}

		.column20 {
			float: left;
			width: 20%;
		}

		.column30 {
			float: left;
			width: 30%;
		}

		/* Clear floats after the columns */
		.row:after {
		    content: "";
		    display: table;
		    clear: both;
		}

		.blank {
			border-top: 1px solid black;
		}

		.footer {
			border-top: 2px solid black;
		}

		.footer .body, .payslip-details .bold {
			font-weight: bold;
		}

		.amount {
			text-align: right;
		}

		.particular {
			padding-right: 15px;
		}

		.payslip-details-2 {
			border-top: 2px solid black;
			padding: 20px 0;
		}
		.net-pay {
			border-top: 2px solid black;
			font-weight: bold;
			padding: 3px 0;
		}
		ul {
			padding: 0;
    		margin: 0;
    		list-style-type: none;
		}
		.logo {
			font-size: 30px;
		}

		.logo div {
			width: 190px;
			float: left;
			text-align: center;
			padding-top: 25px;
		}

		hr {
		    border: none;
		    height: 1px;
		    /* Set the hr color */
		    color: #333; /* old IE */
		    background-color: #333; /* Modern Browsers */
		}
	</style>
</head>
<body>

	@foreach ($payrollList as $payroll)
	<div class="main">
		<section class="header">
			<div class="content">
				<div class="row">
					<div class="head-title">
						MARVIN JOHNSON MANAGEMENT & HR SOLUTIONS <br />
						2ndFlr. Renaissance Tower, Meralco Ave. Pasig City <br />
						1605, Metro Manila <br />
						Philippiness
					</div>	
				</div>
			</div>
		</section>
		<section class="payslip-details" style="padding: 0 15px 15px;">
			<div class="content">
				<div class="head-title">
					<p> PAYSLIP FOR {{ strtoupper(date("M d, Y", strtotime($payroll_period->start_time))) }} TO {{ strtoupper(date("M d, Y", strtotime($payroll_period->end_time))) }}</p>				
				</div>
				<div class="body">
					<div class="row">
					  <div class="column20">
					  	<ul>
					  		<li>NAME :</li>
					  		<li>DEPARTMENT :</li>
					  		<li>DATE OF JOINING :</li>
					  		<li>DAYS WORKED :</li>
					  		<li>HOLIDAY :</li>
					  		<li>LEAVE CREDITS :</li>
					  	</ul>
					  </div>
					  <div class="column30 bold">
					  	<ul>
					  		<li> {{ strtoupper($payroll->firstname) }} {{ strtoupper($payroll->lastname) }} </li>
					  		<li> {{ strtoupper($payroll->department_name) }} </li>
					  		<li> {{ $payroll->starting_date }} </li>
					  		<li> {{ $payroll->no_of_days }} </li>
					  		<li> {{ $payroll->special_hol + $payroll->legal_hol + $payroll->double_hol }} </li>
					  		<li> 
					  			{{
						  			$payroll->sick_leave_credit +
									$payroll->vacation_leave_credit +
									$payroll->emergency_leave_credit +
									$payroll->maternity_leave_credit +
									$payroll->others_leave_credit 
								}} 
							</li>
					  	</ul>
					  </div>
					  <div class="column">
					  	<ul>
					  		<li>EMPLOYEE NUMBER :</li>
					  		<li>DESIGNATION :</li>
					  		<li>EMPLOYMENT STATUS :</li>
					  		<li>PAYROLL ATM :</li>
					  	</ul>
					  </div>
					  <div class="column bold">
					  	<ul>
					  		<li> {{ $payroll->employee_code }} </li>
					  		<li> {{ strtoupper($payroll->position_name) }} </li>
					  		<li> {{ strtoupper($payroll->employment_status) }} </li>
					  		<li> {{ $payroll->bank_account_number }} </li>
					  	</ul>
					  </div>
					</div>
				</div>
			</div>
		
		</section>
		<div class="content">
			<div class="blank">
				&nbsp;
			</div>
		</div>
		<section class="footer" >
			<div class="body">
				<div class="row">
				  <div class="column">
				 	GROSS SALARY
				  </div>
				  <div class="column amount">
				  	<div class="particular">
				  		AMOUNT
				  	</div>
				  </div>
				  <div class="column">
				 	DEDUCTIONS
				  </div>
				  <div class="column amount">
				  	<div class="particular">
				  		AMOUNT
				  	</div>
				  </div>
				</div>
			</div>
		</section>
		<section class="payslip-details-2" style="padding: 15px;">
			<div class="body">
				<div class="row">
					<div class="column20">
						<ul>
							<li>REG. DAYS :</li>
							<li>ECOLA :</li>
							@if($payroll->total_late != 0 || $payroll->total_undertime != 0)
							<li>LATE/UNDERTIME :</li>
							@endif
							@if($payroll->total_allowance != 0 || $payroll->total_allowance != 0)
							<li>TOTAL ALLOWANCE :</li>
							@endif
							@if($payroll->reg_ot != 0)
							<li>REG. OVERTIME :</li>
							@endif
							@if($payroll->restday != 0)
							<li>RESTDAY</li>
							@endif
							@if($payroll->rd_ot != 0)
							<li>RESTDAY OT</li>
							@endif
							@if($payroll->special_hol != 0)
							<li>SPECIAL HOL</li>
							@endif
							@if($payroll->special_hol_ot != 0)
							<li>OT - SPECIAL HOL</li>
							@endif
							@if($payroll->special_hol_rd != 0)
							<li>RD - SPECIAL HOL</li>
							@endif
							@if($payroll->special_hol_rd_ot != 0)
							<li>OT & RD - SPECIAL HOL</li>
							@endif
							@if($payroll->legal_hol != 0)
							<li>LEGAL HOL</li>
							@endif
							@if($payroll->legal_hol_ot != 0)
							<li>OT - LEGAL HOL</li>
							@endif
							@if($payroll->legal_hol_restday != 0)
							<li>RD - LEGAL HOL</li>
							@endif
							@if($payroll->legal_hol_rd_ot != 0)
							<li>OT & RD - LEGAL HOL</li>
							@endif
							@if($payroll->double_hol != 0)
							<li>DOUBLE HOL</li>
							@endif
							@if($payroll->dbl_hol_ot != 0)
							<li>OT - DOUBLE HOL</li>
							@endif
							@if($payroll->double_hol_rd != 0)
							<li>RD - DOUBLE HOL</li>
							@endif
							@if($payroll->dbl_hol_rd_ot != 0)
							<li>OT & RD - DOUBLE HOL</li>
							@endif
							@if($payroll->reg_nd != 0)
							<li>REG. NIGHTDIFF</li>
							@endif
							@if($payroll->rd_nd != 0)
							<li>RESTDAY NIGHTDIFF</li>
							@endif
							@if($payroll->special_hol_nd != 0)
							<li>SPECIAL HOL - ND</li>
							@endif
							@if($payroll->special_hol_rd_nd != 0)
							<li>SPECIAL HOL - RD & ND</li>
							@endif
							@if($payroll->legal_hol_nd != 0)
							<li>LEGAL HOL - ND</li>
							@endif
							@if($payroll->legal_hol_rd_nd != 0)
							<li>LEGAL HOL - RD & ND</li>
							@endif
							@if($payroll->dbl_hol_nd != 0)
							<li>DOUBLE HOL - ND</li>
							@endif
							@if($payroll->dbl_hol_rd_nd != 0)
							<li>DOUBLE HOL - RD & ND</li>
							@endif
							@if($payroll->total_adjustment != 0)
							<li>TOTAL ADJUSTMENT</li>
							@endif

						</ul>
					</div>
					<div class="column15 amount">
						<div class="particular">
							<ul>
								<li>{{ $payroll->no_of_days }}</li>
								<li>{{ $payroll->cola }}</li>
								@if($payroll->total_late != 0 || $payroll->total_undertime != 0)
								<li>({{ $payroll->total_late + $payroll->total_undertime }})</li>	
								@endif
								@if($payroll->total_allowance != 0 || $payroll->total_allowance != 0)
								<li>{{ number_format($payroll->allowance, 2) }}</li>
								@endif
								@if($payroll->reg_ot != 0)
								<li>{{ $payroll->reg_ot }}</li>
								@endif
								@if($payroll->restday != 0)
								<li>{{ $payroll->restday }}</li>
								@endif
								@if($payroll->rd_ot != 0)
								<li>{{ $payroll->rd_ot }}</li>
								@endif
								@if($payroll->special_hol != 0)
								<li>{{ $payroll->special_hol }}</li>
								@endif
								@if($payroll->special_hol_ot != 0)
								<li>{{ $payroll->special_hol_ot }}</li>
								@endif
								@if($payroll->special_hol_rd != 0)
								<li>{{ $payroll->special_hol_rd }}</li>
								@endif
								@if($payroll->special_hol_rd_ot != 0)
								<li>{{ $payroll->special_hol_rd_ot }}</li>
								@endif
								@if($payroll->legal_hol != 0)
								<li>{{ $payroll->legal_hol }}</li>
								@endif
								@if($payroll->legal_hol_ot != 0)
								<li>{{ $payroll->legal_hol_ot }}</li>
								@endif
								@if($payroll->legal_hol_restday != 0)
								<li>{{ $payroll->legal_hol_restday }}</li>
								@endif
								@if($payroll->legal_hol_rd_ot != 0)
								<li>{{ $payroll->legal_hol_rd_ot }}</li>
								@endif
								@if($payroll->double_hol != 0)
								<li>{{ $payroll->double_hol }}</li>
								@endif
								@if($payroll->dbl_hol_ot != 0)
								<li>{{ $payroll->dbl_hol_ot }}</li>
								@endif
								@if($payroll->double_hol_rd != 0)
								<li>{{ $payroll->double_hol_rd }}</li>
								@endif
								@if($payroll->dbl_hol_rd_ot != 0)
								<li>{{ $payroll->dbl_hol_rd_ot }}</li>
								@endif
								@if($payroll->reg_nd != 0)
								<li>{{ $payroll->reg_nd }}</li>
								@endif
								@if($payroll->rd_nd != 0)
								<li>{{ $payroll->rd_nd }}</li>
								@endif
								@if($payroll->special_hol_nd != 0)
								<li>{{ $payroll->special_hol_nd }}</li>
								@endif
								@if($payroll->special_hol_rd_nd != 0)
								<li>{{ $payroll->special_hol_rd_nd }}</li>
								@endif
								@if($payroll->legal_hol_nd != 0)
								<li>{{ $payroll->legal_hol_nd }}</li>
								@endif
								@if($payroll->legal_hol_rd_nd != 0)
								<li>{{ $payroll->legal_hol_rd_nd }}</li>
								@endif
								@if($payroll->dbl_hol_nd != 0)
								<li>{{ $payroll->dbl_hol_nd }}</li>
								@endif
								@if($payroll->dbl_hol_rd_nd != 0)
								<li>{{ $payroll->dbl_hol_rd_nd }}</li>
								@endif
								@if($payroll->total_adjustment != 0)
								<li>&nbsp;</li>
								@endif
							</ul>
						</div>
					</div>
					<div class="column15 amount">
						<div class="particular">
							<ul>
								<li> {{ number_format($payroll->ttl_basic_pay, 2) }} </li>
								<li>{{ $payroll->ecola}}</li>
								@if($payroll->total_late != 0 || $payroll->total_undertime != 0)
								<li> ({{ number_format($payroll->late_amount + $payroll->undertime_amount, 2) }}) </li>
								@endif
								@if($payroll->total_allowance != 0 || $payroll->total_allowance != 0)
								<li>{{ number_format($payroll->total_allowance, 2) }}</li>
								@endif
								@if($payroll->reg_ot != 0)
								<li>{{ number_format($payroll->pay_reg_ot, 2) }}</li>
								@endif
								@if($payroll->restday != 0)
								<li>{{ number_format($payroll->pay_restday, 2) }}</li>
								@endif
								@if($payroll->rd_ot != 0)
								<li>{{ number_format($payroll->pay_rd_ot, 2) }}</li>
								@endif
								@if($payroll->special_hol != 0)
								<li>{{ number_format($payroll->pay_special_hol, 2) }}</li>
								@endif
								@if($payroll->special_hol_ot != 0)
								<li>{{ number_format($payroll->pay_special_hol_ot, 2) }}</li>
								@endif
								@if($payroll->special_hol_rd != 0)
								<li>{{ number_format($payroll->pay_special_hol_rd, 2) }}</li>
								@endif
								@if($payroll->special_hol_rd_ot != 0)
								<li>{{ number_format($payroll->pay_special_hol_rd_ot, 2) }}</li>
								@endif
								@if($payroll->legal_hol != 0)
								<li>{{ number_format($payroll->pay_legal_hol, 2) }}</li>
								@endif
								@if($payroll->legal_hol_ot != 0)
								<li>{{ number_format($payroll->pay_legal_hol_ot, 2) }}</li>
								@endif
								@if($payroll->legal_hol_restday != 0)
								<li>{{ number_format($payroll->pay_legal_hol_restday, 2) }}</li>
								@endif
								@if($payroll->legal_hol_rd_ot != 0)
								<li>{{ number_format($payroll->pay_legal_hol_rd_ot, 2) }}</li>
								@endif
								@if($payroll->double_hol != 0)
								<li>{{ number_format($payroll->pay_double_hol, 2) }}</li>
								@endif
								@if($payroll->dbl_hol_ot != 0)
								<li>{{ number_format($payroll->pay_dbl_hol_ot, 2) }}</li>
								@endif
								@if($payroll->double_hol_rd != 0)
								<li>{{ number_format($payroll->pay_double_hol_rd, 2) }}</li>
								@endif
								@if($payroll->dbl_hol_rd_ot != 0)
								<li>{{ number_format($payroll->pay_dbl_hol_rd_ot, 2) }}</li>
								@endif
								@if($payroll->reg_nd != 0)
								<li>{{ number_format($payroll->pay_reg_nd, 2) }}</li>
								@endif
								@if($payroll->rd_nd != 0)
								<li>{{ number_format($payroll->pay_rd_nd, 2) }}</li>
								@endif
								@if($payroll->special_hol_nd != 0)
								<li>{{ number_format($payroll->pay_special_hol_nd, 2) }}</li>
								@endif
								@if($payroll->special_hol_rd_nd != 0)
								<li>{{ number_format($payroll->pay_special_hol_rd_nd, 2) }}</li>
								@endif
								@if($payroll->legal_hol_nd != 0)
								<li>{{ number_format($payroll->pay_legal_hol_nd, 2) }}</li>
								@endif
								@if($payroll->legal_hol_rd_nd != 0)
								<li>{{ number_format($payroll->pay_legal_hol_rd_nd, 2) }}</li>
								@endif
								@if($payroll->dbl_hol_nd != 0)
								<li>{{ number_format($payroll->pay_dbl_hol_nd, 2) }}</li>
								@endif
								@if($payroll->dbl_hol_rd_nd != 0)
								<li>{{ number_format($payroll->pay_dbl_hol_rd_nd, 2) }}</li>
								@endif
								@if($payroll->total_adjustment != 0)
								<li> {{ number_format($payroll->total_adjustment, 2) }} </li>
								@endif
							</ul>
						</div>
					</div>
					<div class="column">
						<ul>
							<li>SSS :</li>
							<li>PHILHEALTH :</li>
							<li>PAGIBIG :</li>
							<li>AUTHORIZED DEDUCTIONS :</li>
						</ul>
					</div>
					<div class="column amount">
						<div class="particular">
							<ul>
								<li>{{ number_format($payroll->total_sss, 2)}}</li>
								<li>{{ number_format($payroll->total_philhealth, 2)}}</li>
								<li>{{ number_format($payroll->total_pagibig, 2)}}</li>
								<li>-</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="footer">
			<div class="body">
				<div class="row">
				  <div class="column">
				 	GROSS PAY
				  </div>
				  <div class="column amount">
				  	<div class="particular">
				  		{{ number_format($payroll->gross_pay, 2) }}
				  	</div>
				  </div>
				  <div class="column">
				 	TOTAL DEDUCTIONS
				  </div>
				  <div class="column amount">
				  	<div class="particular">
				  		{{ number_format(abs($payroll->total_deduction) + $payroll->total_sss + $payroll->total_philhealth + $payroll->total_pagibig + abs($payroll->total_loan), 2)  }}
				  	</div>
				  </div>
				</div>
			</div>
		</section>
		<section class="net-pay">
			<div class="body">
				<div class="row">
					<div class="column">
						&nbsp;
					</div>
					<div class="column">
						&nbsp;
					</div>
					<div class="column">
						NET PAY :
					</div>
					<div class="column amount">
						<div class="particular">
							{{ number_format($payroll->net_pay, 2) }}
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<hr>
	@endforeach
</body>
</html>
