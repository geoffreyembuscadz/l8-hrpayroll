<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Payslip for [From] to [To]</title>
</head>
<body>
    <table>
    	<tr>
    		<td>
    			<p>
			    	Dear <strong>{{ $firstname }}</strong>, <br>
			    </p>
    		</td>
    	</tr>
    	<tr>
    		<td>
    			<p>
					Please see your payslip for the period <strong>{{ date("d M Y", strtotime($start)) }}</strong> to <strong>{{ date("d M Y", strtotime($end)) }}</strong> on this 
                    <a href="{{ url('/api/payslip?e='.str_random(68).'&id='.$payroll_id.'&emp_id='.$emp_id.'&1='.str_random(68).'') }}"><strong>link</strong></a>.  <br>
				</p>
    		</td>
    	</tr>
    	<tr>
    		<td>
    			<p>
			    	Regards,
			    </p>
    		</td>
    	</tr>
    </table>
</body>
</html>